package ca.gc.asc_csa.apogy.examples.rover.apogy;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rover Apogy System Api Adapter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * This class is the specialized Apogy API adapter, used for connecting
 * the existing rover example, located at
 * {@link ca.gc.asc_csa.apogy.examples.rover.Rover},
 * to Apogy; one can override the well-known callback functions to make
 * Apogy perform a variety of useful functions, including initialization,
 * disposal and other features.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.examples.rover.apogy.ApogyExamplesRoverApogyPackage#getRoverApogySystemApiAdapter()
 * @model
 * @generated
 */
public interface RoverApogySystemApiAdapter extends ApogySystemApiAdapter
{
} // RoverApogySystemApiAdapter

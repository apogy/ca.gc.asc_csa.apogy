package ca.gc.asc_csa.apogy.examples.rover;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rover Simulated</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * This is a simulated implementation of the rover, where all operations
 * are executed upon a simulated (i.e. virtual) rover. While there are no
 * physical components interacted with, it attempts to emulate, wherever
 * possible, the actions and results of its real world counterpart(s).
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.examples.rover.ApogyExamplesRoverPackage#getRoverSimulated()
 * @model
 * @generated
 */
public interface RoverSimulated extends Rover {
} // RoverSimulated

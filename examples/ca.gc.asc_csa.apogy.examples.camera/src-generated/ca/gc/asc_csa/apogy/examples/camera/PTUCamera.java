package ca.gc.asc_csa.apogy.examples.camera;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.addons.actuators.PanTiltUnit;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PTU Camera</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * The abstract definition of a basic camera mounted on a Pan-Tilt Unit (PTU).
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.examples.camera.ApogyExamplesCameraPackage#getPTUCamera()
 * @model abstract="true"
 * @generated
 */
public interface PTUCamera extends Camera, PanTiltUnit
{
} // PTUCamera

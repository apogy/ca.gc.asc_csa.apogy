package ca.gc.asc_csa.apogy.examples.lidar;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lidar Stub</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * This is a specific implementation of the Lidar unit, in which
 * all operations are stubs and hence, non-functional; the
 * operations should simply log a message, indicating that they
 * were performed.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.examples.lidar.ApogyExamplesLidarPackage#getLidarStub()
 * @model
 * @generated
 */
public interface LidarStub extends Lidar
{
} // LidarStub

package ca.gc.asc_csa.apogy.examples.lidar;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lidar Simulated</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * This is a simulated implementation of the Lidar unit, where all
 * operations are executed upon a simulated (i.e. virtual) Lidar.
 * The current version returns a point cloud of randomly generated points.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.examples.lidar.ApogyExamplesLidarPackage#getLidarSimulated()
 * @model
 * @generated
 */
public interface LidarSimulated extends Lidar {
} // LidarSimulated

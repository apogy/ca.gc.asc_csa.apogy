/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.examples.satellite.converters;

import ca.gc.asc_csa.apogy.common.converters.IConverter;
import ca.gc.asc_csa.apogy.examples.satellite.ConstellationRequestsListsContainer;
import ca.gc.asc_csa.apogy.examples.satellite.ConstellationState;

public class ConstellationStateToConstellationRequestsListsContainerConverter implements IConverter {

	@Override
	public boolean canConvert(Object arg0) {
		return ((ConstellationState)arg0).getConstellationRequestsListsContainer() != null;
	}

	@Override
	public Object convert(Object arg0) throws Exception {
		return ((ConstellationState)arg0).getConstellationRequestsListsContainer();
	}

	@Override
	public Class<?> getInputType() {
		return ConstellationState.class;
	}

	@Override
	public Class<?> getOutputType() {
		return ConstellationRequestsListsContainer.class;
	}	
}
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.examples.satellite.converters;

import ca.gc.asc_csa.apogy.common.converters.IConverter;
import ca.gc.asc_csa.apogy.core.environment.earth.EarthSurfaceLocation;
import ca.gc.asc_csa.apogy.examples.satellite.ObservationConstellationRequest;

public class ObservationConstellationRequestToEarthSurfaceLocationConverter implements IConverter {

	@Override
	public boolean canConvert(Object arg0) {
		return ((ObservationConstellationRequest)arg0).getLocation()!=null;
	}

	@Override
	public Object convert(Object arg0) throws Exception {
		return ((ObservationConstellationRequest)arg0).getLocation();
	}

	@Override
	public Class<?> getInputType() {
		return ObservationConstellationRequest.class;
	}

	@Override
	public Class<?> getOutputType() {
		return EarthSurfaceLocation.class;
	}
}
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
/**
 *
 * $Id$
 */
package ca.gc.asc_csa.apogy.examples.lander.validation;

import ca.gc.asc_csa.apogy.addons.vehicle.Thruster;

import ca.gc.asc_csa.apogy.examples.lander.LanderLegExtension;
import ca.gc.asc_csa.apogy.examples.lander.Position;

/**
 * A sample validator interface for {@link ca.gc.asc_csa.apogy.examples.lander.Lander}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface LanderValidator {
	boolean validate();

	boolean validatePosition(Position value);
	boolean validateThruster(Thruster value);
	boolean validateLegAExtension(LanderLegExtension value);
	boolean validateLegBExtension(LanderLegExtension value);
	boolean validateLegCExtension(LanderLegExtension value);
	boolean validateLegAPosition(double value);
	boolean validateLegBPosition(double value);
	boolean validateLegCPosition(double value);
	boolean validateXAngularVelocity(double value);
	boolean validateYAngularVelocity(double value);
	boolean validateMass(double value);
	boolean validateFuelMass(double value);
	boolean validateGravitationalPull(double value);
	boolean validateChangingLegs(boolean value);
	boolean validateChangingAttitude(boolean value);
	boolean validateChangingLocation(boolean value);
	boolean validateFlyingEnabled(boolean value);
	boolean validateInitialized(boolean value);
	boolean validateDisposed(boolean value);
}

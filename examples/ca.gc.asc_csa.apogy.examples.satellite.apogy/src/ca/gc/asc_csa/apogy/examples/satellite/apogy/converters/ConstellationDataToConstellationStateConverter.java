/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.examples.satellite.apogy.converters;

import ca.gc.asc_csa.apogy.common.converters.IConverter;
import ca.gc.asc_csa.apogy.examples.satellite.ConstellationState;
import ca.gc.asc_csa.apogy.examples.satellite.apogy.ConstellationData;

public class ConstellationDataToConstellationStateConverter implements IConverter {

	@Override
	public boolean canConvert(Object arg0) {
		return ((ConstellationData)arg0).getConstellationState() != null;
	}

	@Override
	public Object convert(Object arg0) throws Exception {
		return ((ConstellationData)arg0).getConstellationState();
	}

	@Override
	public Class<?> getInputType() {		
		return ConstellationData.class;
	}

	@Override
	public Class<?> getOutputType() {
		return ConstellationState.class;
	}
}
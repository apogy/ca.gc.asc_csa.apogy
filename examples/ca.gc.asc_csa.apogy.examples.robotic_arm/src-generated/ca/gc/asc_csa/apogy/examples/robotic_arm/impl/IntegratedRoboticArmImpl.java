/**
 * Copyright (c) 2015-2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.examples.robotic_arm.impl;

import ca.gc.asc_csa.apogy.examples.camera.PTUCamera;

import ca.gc.asc_csa.apogy.examples.robotic_arm.ApogyExamplesRoboticArmPackage;
import ca.gc.asc_csa.apogy.examples.robotic_arm.IntegratedRoboticArm;
import ca.gc.asc_csa.apogy.examples.robotic_arm.RoboticArm;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Integrated Robotic Arm</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.examples.robotic_arm.impl.IntegratedRoboticArmImpl#getRoboticArm <em>Robotic Arm</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.examples.robotic_arm.impl.IntegratedRoboticArmImpl#getArmCamera <em>Arm Camera</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IntegratedRoboticArmImpl extends MinimalEObjectImpl.Container implements IntegratedRoboticArm {
	/**
	 * The cached value of the '{@link #getRoboticArm() <em>Robotic Arm</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoboticArm()
	 * @generated
	 * @ordered
	 */
	protected RoboticArm roboticArm;

	/**
	 * The cached value of the '{@link #getArmCamera() <em>Arm Camera</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArmCamera()
	 * @generated
	 * @ordered
	 */
	protected PTUCamera armCamera;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntegratedRoboticArmImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyExamplesRoboticArmPackage.Literals.INTEGRATED_ROBOTIC_ARM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoboticArm getRoboticArm() {
		if (roboticArm != null && roboticArm.eIsProxy()) {
			InternalEObject oldRoboticArm = (InternalEObject)roboticArm;
			roboticArm = (RoboticArm)eResolveProxy(oldRoboticArm);
			if (roboticArm != oldRoboticArm) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ROBOTIC_ARM, oldRoboticArm, roboticArm));
			}
		}
		return roboticArm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoboticArm basicGetRoboticArm() {
		return roboticArm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoboticArm(RoboticArm newRoboticArm) {
		RoboticArm oldRoboticArm = roboticArm;
		roboticArm = newRoboticArm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ROBOTIC_ARM, oldRoboticArm, roboticArm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PTUCamera getArmCamera() {
		if (armCamera != null && armCamera.eIsProxy()) {
			InternalEObject oldArmCamera = (InternalEObject)armCamera;
			armCamera = (PTUCamera)eResolveProxy(oldArmCamera);
			if (armCamera != oldArmCamera) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ARM_CAMERA, oldArmCamera, armCamera));
			}
		}
		return armCamera;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PTUCamera basicGetArmCamera() {
		return armCamera;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArmCamera(PTUCamera newArmCamera) {
		PTUCamera oldArmCamera = armCamera;
		armCamera = newArmCamera;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ARM_CAMERA, oldArmCamera, armCamera));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public boolean init() 
	{
		boolean result = false;
		
		if(getRoboticArm() != null)
		{
			result = getRoboticArm().init();
		}
		
		if(result && getArmCamera() != null)
		{
			result = getArmCamera().init();
		}
		
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ROBOTIC_ARM:
				if (resolve) return getRoboticArm();
				return basicGetRoboticArm();
			case ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ARM_CAMERA:
				if (resolve) return getArmCamera();
				return basicGetArmCamera();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ROBOTIC_ARM:
				setRoboticArm((RoboticArm)newValue);
				return;
			case ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ARM_CAMERA:
				setArmCamera((PTUCamera)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ROBOTIC_ARM:
				setRoboticArm((RoboticArm)null);
				return;
			case ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ARM_CAMERA:
				setArmCamera((PTUCamera)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ROBOTIC_ARM:
				return roboticArm != null;
			case ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM__ARM_CAMERA:
				return armCamera != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyExamplesRoboticArmPackage.INTEGRATED_ROBOTIC_ARM___INIT:
				return init();
		}
		return super.eInvoke(operationID, arguments);
	}

} //IntegratedRoboticArmImpl

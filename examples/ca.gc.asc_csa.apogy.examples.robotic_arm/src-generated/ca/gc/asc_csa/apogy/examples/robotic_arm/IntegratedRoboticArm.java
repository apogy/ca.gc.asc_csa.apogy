/**
 * Copyright (c) 2015-2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.examples.robotic_arm;

import ca.gc.asc_csa.apogy.examples.camera.PTUCamera;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integrated Robotic Arm</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Class representing an integrated assembly of a robotic arm and a PTU camera.
 * The PTU Camera is mounted on the arm forearm platform.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.examples.robotic_arm.IntegratedRoboticArm#getRoboticArm <em>Robotic Arm</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.examples.robotic_arm.IntegratedRoboticArm#getArmCamera <em>Arm Camera</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.examples.robotic_arm.ApogyExamplesRoboticArmPackage#getIntegratedRoboticArm()
 * @model
 * @generated
 */
public interface IntegratedRoboticArm extends EObject {
	/**
	 * Returns the value of the '<em><b>Robotic Arm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The robotic arm.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Robotic Arm</em>' reference.
	 * @see #setRoboticArm(RoboticArm)
	 * @see ca.gc.asc_csa.apogy.examples.robotic_arm.ApogyExamplesRoboticArmPackage#getIntegratedRoboticArm_RoboticArm()
	 * @model required="true" transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' propertyCategory='Subcomponents' children='true'"
	 * @generated
	 */
	RoboticArm getRoboticArm();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.examples.robotic_arm.IntegratedRoboticArm#getRoboticArm <em>Robotic Arm</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Robotic Arm</em>' reference.
	 * @see #getRoboticArm()
	 * @generated
	 */
	void setRoboticArm(RoboticArm value);

	/**
	 * Returns the value of the '<em><b>Arm Camera</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * A camera mounted on the arm.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Arm Camera</em>' reference.
	 * @see #setArmCamera(PTUCamera)
	 * @see ca.gc.asc_csa.apogy.examples.robotic_arm.ApogyExamplesRoboticArmPackage#getIntegratedRoboticArm_ArmCamera()
	 * @model required="true" transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' propertyCategory='Subcomponents' children='true'"
	 * @generated
	 */
	PTUCamera getArmCamera();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.examples.robotic_arm.IntegratedRoboticArm#getArmCamera <em>Arm Camera</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arm Camera</em>' reference.
	 * @see #getArmCamera()
	 * @generated
	 */
	void setArmCamera(PTUCamera value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Performs the required operations required in order to initialize
	 * the robotic arm and the Camera.
	 * @return Whether or not the robotic arm was successfully initialized.
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 * @generated
	 */
	boolean init();

} // IntegratedRoboticArm

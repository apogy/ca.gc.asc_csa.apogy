package ca.gc.asc_csa.apogy.examples.mobile_platform.apogy.impl;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.EAttribute;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.examples.mobile_platform.apogy.MobilePlatformData;
import ca.gc.asc_csa.apogy.examples.mobile_platform.apogy.MobilePlatformApogySystemAnnotation;
import ca.gc.asc_csa.apogy.examples.mobile_platform.apogy.MobilePlatformApogySystemApiAdapter;
import ca.gc.asc_csa.apogy.examples.mobile_platform.apogy.ApogyExamplesMobilePlatformApogyFactory;
import ca.gc.asc_csa.apogy.examples.mobile_platform.apogy.ApogyExamplesMobilePlatformApogyPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyExamplesMobilePlatformApogyPackageImpl extends EPackageImpl implements ApogyExamplesMobilePlatformApogyPackage
{
  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mobilePlatformApogySystemApiAdapterEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mobilePlatformDataEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mobilePlatformApogySystemAnnotationEClass = null;
		/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.examples.mobile_platform.apogy.ApogyExamplesMobilePlatformApogyPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
  private ApogyExamplesMobilePlatformApogyPackageImpl()
  {
		super(eNS_URI, ApogyExamplesMobilePlatformApogyFactory.eINSTANCE);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private static boolean isInited = false;

  /**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyExamplesMobilePlatformApogyPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
  public static ApogyExamplesMobilePlatformApogyPackage init()
  {
		if (isInited) return (ApogyExamplesMobilePlatformApogyPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyExamplesMobilePlatformApogyPackage.eNS_URI);

		// Obtain or create and register package
		ApogyExamplesMobilePlatformApogyPackageImpl theApogyExamplesMobilePlatformApogyPackage = (ApogyExamplesMobilePlatformApogyPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyExamplesMobilePlatformApogyPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyExamplesMobilePlatformApogyPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogySurfaceEnvironmentUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyExamplesMobilePlatformApogyPackage.createPackageContents();

		// Initialize created meta-data
		theApogyExamplesMobilePlatformApogyPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyExamplesMobilePlatformApogyPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyExamplesMobilePlatformApogyPackage.eNS_URI, theApogyExamplesMobilePlatformApogyPackage);
		return theApogyExamplesMobilePlatformApogyPackage;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMobilePlatformApogySystemApiAdapter() {
		return mobilePlatformApogySystemApiAdapterEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMobilePlatformData() {
		return mobilePlatformDataEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMobilePlatformData_Initialized() {
		return (EAttribute)mobilePlatformDataEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMobilePlatformData_Disposed() {
		return (EAttribute)mobilePlatformDataEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMobilePlatformData_LinearVelocity() {
		return (EAttribute)mobilePlatformDataEClass.getEStructuralFeatures().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMobilePlatformData_AngularVelocity() {
		return (EAttribute)mobilePlatformDataEClass.getEStructuralFeatures().get(3);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMobilePlatformApogySystemAnnotation() {
		return mobilePlatformApogySystemAnnotationEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyExamplesMobilePlatformApogyFactory getApogyExamplesMobilePlatformApogyFactory() {
		return (ApogyExamplesMobilePlatformApogyFactory)getEFactoryInstance();
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isCreated = false;

  /**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void createPackageContents()
  {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		mobilePlatformApogySystemApiAdapterEClass = createEClass(MOBILE_PLATFORM_APOGY_SYSTEM_API_ADAPTER);

		mobilePlatformDataEClass = createEClass(MOBILE_PLATFORM_DATA);
		createEAttribute(mobilePlatformDataEClass, MOBILE_PLATFORM_DATA__INITIALIZED);
		createEAttribute(mobilePlatformDataEClass, MOBILE_PLATFORM_DATA__DISPOSED);
		createEAttribute(mobilePlatformDataEClass, MOBILE_PLATFORM_DATA__LINEAR_VELOCITY);
		createEAttribute(mobilePlatformDataEClass, MOBILE_PLATFORM_DATA__ANGULAR_VELOCITY);

		mobilePlatformApogySystemAnnotationEClass = createEClass(MOBILE_PLATFORM_APOGY_SYSTEM_ANNOTATION);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isInitialized = false;

  /**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void initializePackageContents()
  {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCorePackage theApogyCorePackage = (ApogyCorePackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCorePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogySurfaceEnvironmentUIPackage theApogySurfaceEnvironmentUIPackage = (ApogySurfaceEnvironmentUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogySurfaceEnvironmentUIPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		mobilePlatformApogySystemApiAdapterEClass.getESuperTypes().add(theApogyCorePackage.getApogySystemApiAdapter());
		mobilePlatformDataEClass.getESuperTypes().add(theApogyCorePackage.getApogyInitializationData());
		mobilePlatformApogySystemAnnotationEClass.getESuperTypes().add(theApogySurfaceEnvironmentUIPackage.getPoseVariableAnnotation());

		// Initialize classes, features, and operations; add parameters
		initEClass(mobilePlatformApogySystemApiAdapterEClass, MobilePlatformApogySystemApiAdapter.class, "MobilePlatformApogySystemApiAdapter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mobilePlatformDataEClass, MobilePlatformData.class, "MobilePlatformData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMobilePlatformData_Initialized(), theEcorePackage.getEBoolean(), "initialized", "false", 0, 1, MobilePlatformData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMobilePlatformData_Disposed(), theEcorePackage.getEBoolean(), "disposed", "false", 0, 1, MobilePlatformData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMobilePlatformData_LinearVelocity(), theEcorePackage.getEDouble(), "linearVelocity", "0.0", 0, 1, MobilePlatformData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMobilePlatformData_AngularVelocity(), theEcorePackage.getEDouble(), "angularVelocity", "0.0", 0, 1, MobilePlatformData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mobilePlatformApogySystemAnnotationEClass, MobilePlatformApogySystemAnnotation.class, "MobilePlatformApogySystemAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

		/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "prefix", "ApogyExamplesMobilePlatformApogy",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque \n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyExamplesMobilePlatformApogy",
			 "suppressGenModelAnnotations", "false",
			 "dynamicTemplates", "true",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.examples.mobile_platform.apogy/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.examples.mobile_platform.apogy.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.examples.mobile_platform"
		   });	
		addAnnotation
		  (mobilePlatformApogySystemApiAdapterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nThis class is the specialized Apogy API adapter, used for connecting\nthe existing Lidar unit example, located at\n{@link ca.gc.asc_csa.apogy.common.emf.examples.lidar.Lidar},\nto Apogy; one can override the well-known callback functions to make\nApogy perform a variety of useful functions, including initialization,\ndisposal and other features."
		   });	
		addAnnotation
		  (mobilePlatformDataEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nThis class specifies the initialization data that Apogy\nshould be interested in when (re)initializing the Mobile\nplatform with the API Adapter (which in this case is\n{@link ca.gc.asc_csa.apogy.examples.mobile_platform.apogy.MobilePlatformApogySystemApiAdapter})"
		   });	
		addAnnotation
		  (getMobilePlatformData_Initialized(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThis is whether or not the mobile platform should be\ninitialized.",
			 "children", "false",
			 "notify", "true",
			 "property", "Editable",
			 "propertyCategory", "Status"
		   });	
		addAnnotation
		  (getMobilePlatformData_Disposed(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThis is whether or not the mobile platform should be\ndisposed.",
			 "children", "false",
			 "notify", "true",
			 "property", "Editable",
			 "propertyCategory", "Status"
		   });	
		addAnnotation
		  (getMobilePlatformData_LinearVelocity(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThis is the linear velocity that the mobile platform\nshould have.",
			 "apogy_units", "m/s",
			 "children", "false",
			 "notify", "true",
			 "property", "Editable",
			 "propertyCategory", "Velocities"
		   });	
		addAnnotation
		  (getMobilePlatformData_AngularVelocity(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThis is the angular velocity that the mobile platform\nshould have.",
			 "apogy_units", "rad/s",
			 "children", "false",
			 "notify", "true",
			 "property", "Editable",
			 "propertyCategory", "Velocities"
		   });	
		addAnnotation
		  (mobilePlatformApogySystemAnnotationEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nThis class is used to specify an annotation on the\nMap View of Apogy, corresponding to the position of\nthe mobile platform; in addition, the annotation is\ncapable of representing the platform\'s pose on the map."
		   });
	}

} //ApogyExamplesMobilePlatformApogyPackageImpl

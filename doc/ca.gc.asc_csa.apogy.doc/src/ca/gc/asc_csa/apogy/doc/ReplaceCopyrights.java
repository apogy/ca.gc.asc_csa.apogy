/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard 	- initial API and implementation
 *     Regent L'Archeveque, 
 *     Sebastien Gemme 
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.doc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;

public class ReplaceCopyrights 
{	
	public static int fileCount = 1;	
	public static String original = null;
	public static String replacement = null;
	
	public static void list(File folder, final String fileExtension)
	{		
		File[] listOfFiles = folder.listFiles();
		if(listOfFiles != null)
		{
			for (File file : listOfFiles) 
			{
			    if (file.isFile()) 
			    {
			    	if(file.getAbsolutePath().endsWith(fileExtension))
		        	{
		        		replace(file);
		        	}
			    }
			    else
			    {
			    	list(file, fileExtension);
			    }
			}
		}
		else
		{
			if (folder.isFile()) 
		    {
		    	if(folder.getAbsolutePath().endsWith(fileExtension))
	        	{
	        		replace(folder);
	        	}
		    }
		}
	}
		
	public static void replace(File file)
	{
		try
		{			
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fis.read(data);			
	
			System.out.println("Processing file (" + fileCount + ") : " + file.getAbsolutePath());

			
			String str = new String(data, "UTF-8");
						
			String start = original.substring(0, 50);
			String end  = original.substring(original.length() - 50);
								
			int beginIndex = str.indexOf(start);
			int endIndex = str.indexOf(end) + end.length();
			
			// System.out.println(str.substring(beginIndex, endIndex));
			
			String before = str.substring(0, beginIndex);
			String after = str.substring(endIndex);
			str = before + replacement + after;						

			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(str);
			writer.close();
			fis.close();
			
			fileCount ++;
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			System.out.println("---------------------> No matches found in file " + file.getAbsolutePath());
		}
	}
	
	public static void configureOriginalString(String originalStringFilePath)
	{
		try
		{
			File file = new File(originalStringFilePath);
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
							
			fis.read(data);
			fis.close();
	
			original = new String(data, "UTF-8");
			original = original.trim();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void configureReplacementString(String replacementStringFilePath)
	{
		try
		{
			File file = new File(replacementStringFilePath);
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fis.read(data);
			fis.close();
	
			replacement = new String(data, "UTF-8");
			replacement = replacement.trim();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) 
	{		
		String folderName = "/home/pallard/git/ca.gc.asc_csa.apogy/bundles/ca.gc.asc_csa.apogy.addons";		
		String fileExtension = ".java";
		
		String originalStringFilePath = "/home/pallard/original.txt";
		String replacementStringFilePath = "/home/pallard/replacement.txt";
		
		// Configure original and replacement.		
		configureOriginalString(originalStringFilePath);
		configureReplacementString(replacementStringFilePath);
		
		File folder = new File(folderName);
		File[] listOfFiles = folder.listFiles();
						
		for (File file : listOfFiles) 
		{
		    list(file, fileExtension);
		}
	}
}

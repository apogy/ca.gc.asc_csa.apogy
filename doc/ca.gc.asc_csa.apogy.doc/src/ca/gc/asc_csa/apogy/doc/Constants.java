/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.doc;

/*
 * The following class contains Apogy Documentation Constants.
 */
public class Constants {
	private Constants(){		
	}
	
	public static final String APOGY_DOC_TOC_LOCATION = "/" + Activator.ID + "wiki/main.html"; 
	public static final String APOGY_DOC_TUTORIALS_LOCATION = "/" + Activator.ID + "wiki/tutorials.html";
}
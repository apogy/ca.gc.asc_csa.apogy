package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite;

public class EarthAtmosphereWorksiteDetailsComposite extends Composite 
{
	private EarthAtmosphereWorksite earthAtmosphereWorksite;
			
	private Composite compositeDetails;
	
	public EarthAtmosphereWorksiteDetailsComposite(Composite parent, int style) 
	{
		super(parent, SWT.NONE);
		setLayout(new GridLayout(1,false));
				
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.widthHint = 318;
		gd_compositeDetails.minimumWidth = 318;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));	
	}

	public EarthAtmosphereWorksite getEarthAtmosphereWorksite() {
		return earthAtmosphereWorksite;
	}

	public void setEarthAtmosphereWorksite(EarthAtmosphereWorksite earthAtmosphereWorksite) 
	{
		this.earthAtmosphereWorksite = earthAtmosphereWorksite;
			
		// Update Details EMFForm.
		if(earthAtmosphereWorksite != null)
		{
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, earthAtmosphereWorksite);
		}
	}		
}

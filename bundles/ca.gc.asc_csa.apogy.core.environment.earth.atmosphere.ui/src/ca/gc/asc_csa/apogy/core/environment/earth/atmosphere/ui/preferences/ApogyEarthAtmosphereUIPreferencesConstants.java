package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/


public class ApogyEarthAtmosphereUIPreferencesConstants 
{
	public static final String DEFAULT_WMS_URL_ID = "DEFAULT_WMS_URL_ID";
	public static final String DEFAULT_WMS_URL = "https://geogratis.gc.ca/maps/CBMT?service"; 
	// "https://ows.mundialis.de/services/service";
	
	
	public static final String DEFAULT_WMS_LAYERS_ID = "DEFAULT_WMS_LAYERS_ID";
	public static final String DEFAULT_WMS_LAYERS = "CBMT";
	// "OSM-WMS"
	
	// Presentation Preferences.
	
	public static final String DEFAULT_ATMOSPHERE_WORKSITE_AXIS_LENGTH_ID = "DEFAULT_ATMOSPHERE_WORKSITE_AXIS_LENGTH_ID";
	public static final double DEFAULT_ATMOSPHERE_WORKSITE_AXIS_LENGTH = 5.0;

	public static final String DEFAULT_ATMOSPHERE_WORKSITE_AXIS_VISIBLE_ID = "DEFAULT_ATMOSPHERE_WORKSITE_AXIS_VISIBLE_ID";
	public static final boolean DEFAULT_ATMOSPHERE_WORKSITE_AXIS_VISIBLE = false;
	
	public static final String DEFAULT_ATMOSPHERE_WORKSITE_AZIMUTH_VISIBLE_ID = "DEFAULT_ATMOSPHERE_WORKSITE_AZIMUTH_VISIBLE_ID";
	public static final boolean DEFAULT_ATMOSPHERE_WORKSITE_AZIMUTH_VISIBLE = true;
	
	public static final String DEFAULT_ATMOSPHERE_WORKSITE_AZIMUTH_LINES_VISIBLE_ID = "DEFAULT_ATMOSPHERE_WORKSITE_AZIMUTH_LINES_VISIBLE_ID";
	public static final boolean DEFAULT_ATMOSPHERE_WORKSITE_AZIMUTH_LINES_VISIBLE = true;
	
	public static final String DEFAULT_ATMOSPHERE_WORKSITE_ELEVATION_LINES_VISIBLE_ID = "DEFAULT_ATMOSPHERE_WORKSITE_ELEVATION_LINES_VISIBLE_ID";
	public static final boolean DEFAULT_ATMOSPHERE_WORKSITE_ELEVATION_LINES_VISIBLE = true;

	public static final String DEFAULT_ATMOSPHERE_WORKSITE_PLANE_VISIBLE_ID = "DEFAULT_ATMOSPHERE_WORKSITE_PLANE_VISIBLE_ID";	
	public static final boolean DEFAULT_ATMOSPHERE_WORKSITE_PLANE_VISIBLE = false;
	
	public static final String DEFAULT_ATMOSPHERE_WORKSITE_PLANE_GRID_SIZE_ID = "DEFAULT_ATMOSPHERE_WORKSITE_PLANE_GRID_SIZE_ID";
	public static final double DEFAULT_ATMOSPHERE_WORKSITE_PLANE_GRID_SIZE = 1.0;
	
	public static final String DEFAULT_ATMOSPHERE_WORKSITE_PLANE_SIZE_ID = "DEFAULT_ATMOSPHERE_WORKSITE_PLANE_SIZE_ID";
	public static final double DEFAULT_ATMOSPHERE_WORKSITE_PLANE_SIZE = 20.0;

}

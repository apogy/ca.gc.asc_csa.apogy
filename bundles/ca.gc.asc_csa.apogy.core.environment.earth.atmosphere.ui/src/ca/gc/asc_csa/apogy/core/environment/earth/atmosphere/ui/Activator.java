package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.preferences.ApogyEarthAtmosphereUIPreferencesConstants;

public class Activator extends AbstractUIPlugin implements BundleActivator 
{
	public static String ID = "ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui";
	
	// The shared instance
	private static Activator plugin;	

	private static BundleContext context;
	
	public Activator()
	{
		plugin = this;
	}
	
	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	static BundleContext getContext() {
		return context;
	}
	
	@Override
	public void start(BundleContext bundleContext) throws Exception 
	{
		Activator.context = bundleContext;
	}

	@Override
	public void stop(BundleContext arg0) throws Exception 
	{
		Activator.context = null;
	}

	public static String getWMSUrl()
	{
		return Activator.getDefault().getPreferenceStore().getString(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_WMS_URL_ID);
	}
	
	public static String[] getWMSLayers()
	{
		String layersString = Activator.getDefault().getPreferenceStore().getString(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_WMS_LAYERS_ID);
		
		if(layersString != null && layersString.length() > 0)
		{
			String[] layers = layersString.split(",");
			for(int i = 0; i < layers.length; i++)
			{
				layers[i] = layers[i].trim();
			}
			
			return layers;
		}
		else
		{
			return null;
		}
	}
}

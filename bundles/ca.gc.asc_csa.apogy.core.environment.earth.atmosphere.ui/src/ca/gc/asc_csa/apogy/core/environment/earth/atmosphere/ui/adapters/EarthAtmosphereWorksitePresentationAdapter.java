package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.adapters;

import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentationAdapter;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.ApogyEarthAtmosphereEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation;

public class EarthAtmosphereWorksitePresentationAdapter implements NodePresentationAdapter
{

	@Override
	public boolean isAdapterFor(Node obj) 
	{		
		boolean isAdapter = obj instanceof EarthAtmosphereWorksiteNode;	
		return isAdapter;
	}

	@Override
	public NodePresentation getAdapter(Node obj, Object context) 
	{
		if (!isAdapterFor(obj)) 
		{
			throw new IllegalArgumentException();
		}

		EarthAtmosphereWorksitePresentation presentationNode = ApogyEarthAtmosphereEnvironmentUIFactory.eINSTANCE.createEarthAtmosphereWorksitePresentation();
		presentationNode.setNode(obj);			
						
		return presentationNode;
	}

	@Override
	public Class<?> getAdaptedClass() 
	{
		return EarthAtmosphereWorksiteNode.class;
	}

}

package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.Activator;

public class ApogyEarthAtmosphereUIPreferencesInitializer extends AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() 
	{			
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
				
		// Initialize the WMS Url.
		store.setDefault(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_WMS_URL_ID, ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_WMS_URL);
		
		// Initialize the WMS Layers to use..
		store.setDefault(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_WMS_LAYERS_ID, ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_WMS_LAYERS);

		// Initialize default value for presentation.
		store.setDefault(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_AXIS_LENGTH_ID, ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_AXIS_LENGTH);
		store.setDefault(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_AXIS_VISIBLE_ID, ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_AXIS_VISIBLE);
		store.setDefault(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_AZIMUTH_VISIBLE_ID, ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_AZIMUTH_VISIBLE);
		store.setDefault(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_AZIMUTH_LINES_VISIBLE_ID, ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_AZIMUTH_LINES_VISIBLE);
		store.setDefault(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_ELEVATION_LINES_VISIBLE_ID, ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_ELEVATION_LINES_VISIBLE);
		store.setDefault(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_ELEVATION_LINES_VISIBLE_ID, ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_ELEVATION_LINES_VISIBLE);		
		store.setDefault(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_PLANE_VISIBLE_ID, ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_PLANE_VISIBLE);		
		store.setDefault(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_PLANE_GRID_SIZE_ID, ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_PLANE_GRID_SIZE);
		store.setDefault(ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_PLANE_SIZE_ID, ApogyEarthAtmosphereUIPreferencesConstants.DEFAULT_ATMOSPHERE_WORKSITE_PLANE_SIZE);
	}
}

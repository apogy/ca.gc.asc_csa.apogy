package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.parts;

import javax.inject.Inject;

import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.composites.EarthAtmosphereWorksitesComposite;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;

public class EarthAtmosphereWorksitesPart extends AbstractSessionBasedPart
{
	@Inject
	protected EPartService ePartService;
	
	private EarthAtmosphereWorksitesComposite earthAtmosphereWorksitesComposite;
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{
		if(invocatorSession != null)
		{
			if(invocatorSession.getEnvironment() instanceof ApogyEnvironment)
			{
				ApogyEnvironment apogyEnvironment = (ApogyEnvironment) invocatorSession.getEnvironment();
				earthAtmosphereWorksitesComposite.setApogyEnvironment(apogyEnvironment);
			}
			else
			{
				earthAtmosphereWorksitesComposite.setApogyEnvironment(null);
			}
		}			
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{
		parent.setLayout(new FillLayout());
		earthAtmosphereWorksitesComposite = new EarthAtmosphereWorksitesComposite(parent, SWT.NONE)
		{			
			@Override
			protected void newEarthAtmosphereWorksiteSelected(EarthAtmosphereWorksite earthAtmosphereWorksite)
			{								
				selectionService.setSelection(earthAtmosphereWorksite);
			}
		};				
	}
}

package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.composites;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.AbstractWorksite;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.WorksitesList;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.Activator;


public class EarthAtmosphereWorksiteListComposite extends Composite 
{	
	private WorksitesList worksitesList;
	private EarthAtmosphereWorksite selectedEarthSurfaceWorksite;
	
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;
	private Button btnActivate;
	private Button btnImport;
	private Button btnExport;
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;
	
	public EarthAtmosphereWorksiteListComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(2, false));
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CompositeFilterContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				selectedEarthSurfaceWorksite = (EarthAtmosphereWorksite)((IStructuredSelection) event.getSelection()).getFirstElement();
				newSelection(selectedEarthSurfaceWorksite);					
			}
		});
		
		// Buttons.
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(composite, SWT.NONE);
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{
				if (event.type == SWT.Selection) 
				{
					// Create EarthSurfaceWorksiteSettings.
					MapBasedEClassSettings settings = ApogyCommonEMFUIFactory.eINSTANCE.createMapBasedEClassSettings();
					String name = ApogyCommonEMFFacade.INSTANCE.getDefaultName(getWorksitesList(), null, ApogyCoreEnvironmentPackage.Literals.WORKSITES_LIST__WORKSITES);
					settings.getUserDataMap().put("name", name);
					
					Wizard wizard = new ApogyEObjectWizard(ApogyCoreEnvironmentPackage.Literals.WORKSITES_LIST__WORKSITES, getWorksitesList(), settings, ApogyEarthAtmosphereEnvironmentPackage.Literals.EARTH_ATMOSPHERE_WORKSITE);
					WizardDialog dialog = new WizardDialog(getShell(), wizard);
					dialog.open();
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(getWorksitesList());
					}
				}
			}
		});
				
		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				String worksitesToDeleteMessage = "";

				Iterator<EarthAtmosphereWorksite> worksites = getSelectedEarthAtmosphereWorksites().iterator();
				while (worksites.hasNext()) 
				{
					EarthAtmosphereWorksite worksite = worksites.next();
					worksitesToDeleteMessage = worksitesToDeleteMessage + worksite.getName();

					if (worksites.hasNext()) 
					{
						worksitesToDeleteMessage = worksitesToDeleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected Earth Surface Worksites", null,
						"Are you sure to delete these Earth Surface Worksites: " + worksitesToDeleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					for (EarthAtmosphereWorksite worksite :  getSelectedEarthAtmosphereWorksites()) 
					{
						try 
						{
							// Checks if the worksite to be removed is the Environment active worksite.
							if(worksite.getWorksitesList().eContainer() instanceof ApogyEnvironment)
							{
								ApogyEnvironment apogyEnvironment = (ApogyEnvironment) worksite.getWorksitesList().eContainer();
								
								if(apogyEnvironment.getActiveWorksite() == worksite)
								{								
									ApogyCommonTransactionFacade.INSTANCE.basicSet(apogyEnvironment, ApogyCoreEnvironmentPackage.Literals.APOGY_ENVIRONMENT__ACTIVE_WORKSITE, null);
								}
							}
							
							// Removes the worksite from the list.
							ApogyCommonTransactionFacade.INSTANCE.basicRemove(worksite.getWorksitesList(), ApogyCoreEnvironmentPackage.Literals.WORKSITES_LIST__WORKSITES, worksite);														
						} 
						catch (Exception e)	
						{
							Logger.INSTANCE.log(Activator.ID,
									"Unable to delete the Earth Surface Worksite <"+ worksite.getName() + ">",
									EventSeverity.ERROR, e);
						}
					}
				}
				
				// Forces the viewer to refresh its input.
				if(!treeViewer.isBusy())
				{					
					treeViewer.setInput(getWorksitesList());
				}					
			}
		});
		
		btnActivate = new Button(composite, SWT.NONE);
		btnActivate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnActivate.setSize(74, 29);
		btnActivate.setText("Activate");
		btnActivate.setEnabled(false);
		btnActivate.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{	
				ApogyEnvironment env = ApogyCoreEnvironmentFacade.INSTANCE.getActiveApogyEnvironment();				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(env, ApogyCoreEnvironmentPackage.Literals.APOGY_ENVIRONMENT__ACTIVE_WORKSITE, getSelectedEarthAtmosphereWorksite());
			}
		});
		
		btnImport = new Button(composite, SWT.NONE);
		btnImport.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnImport.setSize(74, 29);
		btnImport.setText("Import Worksite");
		btnImport.setToolTipText("Import an existing Earth Atmosphere Worksite.");
		btnImport.setEnabled(false);
		btnImport.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{	
				// TODO
			}
		});				
		
		btnExport = new Button(composite, SWT.NONE);
		btnExport.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnExport.setSize(74, 29);
		btnExport.setText("Export Worksite");
		btnExport.setToolTipText("Export the selected Earth Atmosphere Worksite to file.");
		btnExport.setEnabled(false);
		btnExport.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{	
				if(getSelectedEarthAtmosphereWorksites().size() > 0)
				{
					EarthAtmosphereWorksite tmp = getSelectedEarthAtmosphereWorksites().get(0);
					EarthAtmosphereWorksite worksite = EcoreUtil.copy(tmp);
					
					try
					{												
						FileDialog fileChooser = new FileDialog(Display.getDefault().getActiveShell(), SWT.SAVE);
						
						String defaultFilename = worksite.getName();
						if(defaultFilename != null)
						{
							fileChooser.setFileName(defaultFilename);
						}
						
						String currentDir = System.getProperty("user.dir");
						
						fileChooser.setText("Save Worksite to File");
						fileChooser.setFilterPath(currentDir);
						fileChooser.setFilterExtensions(new String[] { "*.ws"});		
						String filename = fileChooser.open();
						
						if (filename != null)
						{						
							Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
							Map<String, Object> m = reg.getExtensionToFactoryMap();
							m.put("key", new XMIResourceFactoryImpl());
							ResourceSet resSet = new ResourceSetImpl();
							Resource resource = resSet.createResource(URI.createFileURI(filename));
							resource.getContents().add(worksite);
							resource.save(Collections.EMPTY_MAP);
						}
					}
					catch (Exception ex) 
					{
						ex.printStackTrace();
					}					
				}
			}
		});
				
		
		this.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	public WorksitesList getWorksitesList() {
		return worksitesList;
	}

	public void setWorksitesList(WorksitesList worksitesList) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.worksitesList = worksitesList;
		
		if(worksitesList != null)
		{
			m_bindingContext = customInitDataBindings();
			treeViewer.setInput(worksitesList);
				
			// Selects the first EarthSurfaceWorksite found.
			EarthAtmosphereWorksite earthAtmosphereWorksite = getFirstEarthAtmosphereWorksite(worksitesList);			
			treeViewer.setSelection(new StructuredSelection(earthAtmosphereWorksite), true);
		}
	}
	
	public EarthAtmosphereWorksite getSelectedEarthAtmosphereWorksite()
	{
		return selectedEarthSurfaceWorksite;
	}
	
	@SuppressWarnings("unchecked")
	public List<EarthAtmosphereWorksite> getSelectedEarthAtmosphereWorksites()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	

	protected EarthAtmosphereWorksite getFirstEarthAtmosphereWorksite(WorksitesList worksitesList)
	{
		List<EarthAtmosphereWorksite> earthAtmosphereWorksites = filterEarthSurfaceWorksite(worksitesList);
		if(!earthAtmosphereWorksites.isEmpty())
		{
			return earthAtmosphereWorksites.get(0);
		}
		else
		{
			return null;
		}
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		/* Activate Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnActivateObserveWidget = WidgetProperties.enabled().observe(btnActivate);
		bindingContext.bindValue(observeEnabledBtnActivateObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		
		/* Export Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnExportObserveWidget = WidgetProperties.enabled().observe(btnExport);
		bindingContext.bindValue(observeEnabledBtnExportObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		return bindingContext;
	}
	
	protected void newSelection(EarthAtmosphereWorksite earthAtmosphereWorksite)
	{		
	}

	private List<EarthAtmosphereWorksite> filterEarthSurfaceWorksite(WorksitesList worksitesList)
	{
		List<EarthAtmosphereWorksite> earthSurfaceWorksites = new ArrayList<EarthAtmosphereWorksite>();
		for(AbstractWorksite worksite : worksitesList.getWorksites())
		{
			if(worksite instanceof EarthAtmosphereWorksite)
			{
				earthSurfaceWorksites.add(((EarthAtmosphereWorksite) worksite));
			}
		}
		
		return earthSurfaceWorksites;
	}
	
	private class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof WorksitesList)
			{								
				WorksitesList worksitesList = (WorksitesList) inputElement;
								
				// Keeps only EarthSurfaceWorksite.			
				return filterEarthSurfaceWorksite(worksitesList).toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof WorksitesList)
			{								
				WorksitesList worksitesList = (WorksitesList) parentElement;
				
				// Keeps only EarthSurfaceWorksite.			
				return filterEarthSurfaceWorksite(worksitesList).toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof WorksitesList)
			{
				WorksitesList worksitesList = (WorksitesList) element;				
				return !filterEarthSurfaceWorksite(worksitesList).isEmpty();
			}		
			else
			{
				return false;
			}
		}
	}
}

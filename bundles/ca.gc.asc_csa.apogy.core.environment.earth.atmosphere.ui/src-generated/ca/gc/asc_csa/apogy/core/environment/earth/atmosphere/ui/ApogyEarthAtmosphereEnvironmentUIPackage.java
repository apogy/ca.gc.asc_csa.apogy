/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;

import ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.ApogyEarthAtmosphereEnvironmentUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyEarthAtmosphereEnvironmentUI' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' modelName='ApogyEarthAtmosphereEnvironmentUI' complianceLevel='6.0' suppressGenModelAnnotations='false' dynamicTemplates='true' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' modelDirectory='/ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.core.environment.earth.atmosphere'"
 * @generated
 */
public interface ApogyEarthAtmosphereEnvironmentUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyEarthAtmosphereEnvironmentUIPackage eINSTANCE = ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.ApogyEarthAtmosphereEnvironmentUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.EarthAtmosphereWorksitePresentationImpl <em>Earth Atmosphere Worksite Presentation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.EarthAtmosphereWorksitePresentationImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.ApogyEarthAtmosphereEnvironmentUIPackageImpl#getEarthAtmosphereWorksitePresentation()
	 * @generated
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION = 0;

	/**
	 * The feature id for the '<em><b>Topology Presentation Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__TOPOLOGY_PRESENTATION_SET = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__TOPOLOGY_PRESENTATION_SET;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__NODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__NODE;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__COLOR = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__COLOR;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__VISIBLE;

	/**
	 * The feature id for the '<em><b>Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__SELECTED = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SELECTED;

	/**
	 * The feature id for the '<em><b>Shadow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__SHADOW_MODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SHADOW_MODE;

	/**
	 * The feature id for the '<em><b>Use In Bounding Calculation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__USE_IN_BOUNDING_CALCULATION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__USE_IN_BOUNDING_CALCULATION;

	/**
	 * The feature id for the '<em><b>Id Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__ID_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ID_VISIBLE;

	/**
	 * The feature id for the '<em><b>Enable Texture Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__ENABLE_TEXTURE_PROJECTION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ENABLE_TEXTURE_PROJECTION;

	/**
	 * The feature id for the '<em><b>Centroid</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__CENTROID = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__CENTROID;

	/**
	 * The feature id for the '<em><b>Min</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__MIN = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MIN;

	/**
	 * The feature id for the '<em><b>Max</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__MAX = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MAX;

	/**
	 * The feature id for the '<em><b>XRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__XRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__XRANGE;

	/**
	 * The feature id for the '<em><b>YRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__YRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__YRANGE;

	/**
	 * The feature id for the '<em><b>ZRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__ZRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ZRANGE;

	/**
	 * The feature id for the '<em><b>Scene Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__SCENE_OBJECT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SCENE_OBJECT;

	/**
	 * The feature id for the '<em><b>Axis Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__AXIS_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Axis Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__AXIS_LENGTH = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Azimuth Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__AZIMUTH_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Elevation Lines Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__ELEVATION_LINES_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Azimuth Lines Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__AZIMUTH_LINES_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Plane Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__PLANE_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Plane Grid Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__PLANE_GRID_SIZE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Plane Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__PLANE_SIZE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Earth Atmosphere Worksite Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION_FEATURE_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Earth Atmosphere Worksite Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_PRESENTATION_OPERATION_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.EarthAtmosphereWorksiteWizardPagesProviderImpl <em>Earth Atmosphere Worksite Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.EarthAtmosphereWorksiteWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.ApogyEarthAtmosphereEnvironmentUIPackageImpl#getEarthAtmosphereWorksiteWizardPagesProvider()
	 * @generated
	 */
	int EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER = 1;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Earth Atmosphere Worksite Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Earth Atmosphere Worksite Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation <em>Earth Atmosphere Worksite Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth Atmosphere Worksite Presentation</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation
	 * @generated
	 */
	EClass getEarthAtmosphereWorksitePresentation();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#isAxisVisible <em>Axis Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Axis Visible</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#isAxisVisible()
	 * @see #getEarthAtmosphereWorksitePresentation()
	 * @generated
	 */
	EAttribute getEarthAtmosphereWorksitePresentation_AxisVisible();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#getAxisLength <em>Axis Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Axis Length</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#getAxisLength()
	 * @see #getEarthAtmosphereWorksitePresentation()
	 * @generated
	 */
	EAttribute getEarthAtmosphereWorksitePresentation_AxisLength();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#isAzimuthVisible <em>Azimuth Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Azimuth Visible</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#isAzimuthVisible()
	 * @see #getEarthAtmosphereWorksitePresentation()
	 * @generated
	 */
	EAttribute getEarthAtmosphereWorksitePresentation_AzimuthVisible();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#isElevationLinesVisible <em>Elevation Lines Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Elevation Lines Visible</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#isElevationLinesVisible()
	 * @see #getEarthAtmosphereWorksitePresentation()
	 * @generated
	 */
	EAttribute getEarthAtmosphereWorksitePresentation_ElevationLinesVisible();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#isAzimuthLinesVisible <em>Azimuth Lines Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Azimuth Lines Visible</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#isAzimuthLinesVisible()
	 * @see #getEarthAtmosphereWorksitePresentation()
	 * @generated
	 */
	EAttribute getEarthAtmosphereWorksitePresentation_AzimuthLinesVisible();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#isPlaneVisible <em>Plane Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Plane Visible</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#isPlaneVisible()
	 * @see #getEarthAtmosphereWorksitePresentation()
	 * @generated
	 */
	EAttribute getEarthAtmosphereWorksitePresentation_PlaneVisible();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#getPlaneGridSize <em>Plane Grid Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Plane Grid Size</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#getPlaneGridSize()
	 * @see #getEarthAtmosphereWorksitePresentation()
	 * @generated
	 */
	EAttribute getEarthAtmosphereWorksitePresentation_PlaneGridSize();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#getPlaneSize <em>Plane Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Plane Size</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksitePresentation#getPlaneSize()
	 * @see #getEarthAtmosphereWorksitePresentation()
	 * @generated
	 */
	EAttribute getEarthAtmosphereWorksitePresentation_PlaneSize();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksiteWizardPagesProvider <em>Earth Atmosphere Worksite Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth Atmosphere Worksite Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksiteWizardPagesProvider
	 * @generated
	 */
	EClass getEarthAtmosphereWorksiteWizardPagesProvider();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyEarthAtmosphereEnvironmentUIFactory getApogyEarthAtmosphereEnvironmentUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.EarthAtmosphereWorksitePresentationImpl <em>Earth Atmosphere Worksite Presentation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.EarthAtmosphereWorksitePresentationImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.ApogyEarthAtmosphereEnvironmentUIPackageImpl#getEarthAtmosphereWorksitePresentation()
		 * @generated
		 */
		EClass EARTH_ATMOSPHERE_WORKSITE_PRESENTATION = eINSTANCE.getEarthAtmosphereWorksitePresentation();
		/**
		 * The meta object literal for the '<em><b>Axis Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__AXIS_VISIBLE = eINSTANCE.getEarthAtmosphereWorksitePresentation_AxisVisible();
		/**
		 * The meta object literal for the '<em><b>Axis Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__AXIS_LENGTH = eINSTANCE.getEarthAtmosphereWorksitePresentation_AxisLength();
		/**
		 * The meta object literal for the '<em><b>Azimuth Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__AZIMUTH_VISIBLE = eINSTANCE.getEarthAtmosphereWorksitePresentation_AzimuthVisible();
		/**
		 * The meta object literal for the '<em><b>Elevation Lines Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__ELEVATION_LINES_VISIBLE = eINSTANCE.getEarthAtmosphereWorksitePresentation_ElevationLinesVisible();
		/**
		 * The meta object literal for the '<em><b>Azimuth Lines Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__AZIMUTH_LINES_VISIBLE = eINSTANCE.getEarthAtmosphereWorksitePresentation_AzimuthLinesVisible();
		/**
		 * The meta object literal for the '<em><b>Plane Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__PLANE_VISIBLE = eINSTANCE.getEarthAtmosphereWorksitePresentation_PlaneVisible();
		/**
		 * The meta object literal for the '<em><b>Plane Grid Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__PLANE_GRID_SIZE = eINSTANCE.getEarthAtmosphereWorksitePresentation_PlaneGridSize();
		/**
		 * The meta object literal for the '<em><b>Plane Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_ATMOSPHERE_WORKSITE_PRESENTATION__PLANE_SIZE = eINSTANCE.getEarthAtmosphereWorksitePresentation_PlaneSize();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.EarthAtmosphereWorksiteWizardPagesProviderImpl <em>Earth Atmosphere Worksite Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.EarthAtmosphereWorksiteWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl.ApogyEarthAtmosphereEnvironmentUIPackageImpl#getEarthAtmosphereWorksiteWizardPagesProvider()
		 * @generated
		 */
		EClass EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER = eINSTANCE.getEarthAtmosphereWorksiteWizardPagesProvider();

	}

} //ApogyEarthAtmosphereEnvironmentUIPackage

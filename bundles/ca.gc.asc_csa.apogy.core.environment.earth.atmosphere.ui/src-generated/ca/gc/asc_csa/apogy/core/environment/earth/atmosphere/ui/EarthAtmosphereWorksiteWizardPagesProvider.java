/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui;

import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earth Atmosphere Worksite Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.ApogyEarthAtmosphereEnvironmentUIPackage#getEarthAtmosphereWorksiteWizardPagesProvider()
 * @model
 * @generated
 */
public interface EarthAtmosphereWorksiteWizardPagesProvider extends NamedDescribedWizardPagesProvider {
} // EarthAtmosphereWorksiteWizardPagesProvider

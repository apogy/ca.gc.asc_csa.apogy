/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl;

import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyEarthAtmosphereEnvironmentUIFactoryImpl extends EFactoryImpl implements ApogyEarthAtmosphereEnvironmentUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyEarthAtmosphereEnvironmentUIFactory init() {
		try {
			ApogyEarthAtmosphereEnvironmentUIFactory theApogyEarthAtmosphereEnvironmentUIFactory = (ApogyEarthAtmosphereEnvironmentUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyEarthAtmosphereEnvironmentUIPackage.eNS_URI);
			if (theApogyEarthAtmosphereEnvironmentUIFactory != null) {
				return theApogyEarthAtmosphereEnvironmentUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyEarthAtmosphereEnvironmentUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthAtmosphereEnvironmentUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyEarthAtmosphereEnvironmentUIPackage.EARTH_ATMOSPHERE_WORKSITE_PRESENTATION: return createEarthAtmosphereWorksitePresentation();
			case ApogyEarthAtmosphereEnvironmentUIPackage.EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER: return createEarthAtmosphereWorksiteWizardPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthAtmosphereWorksitePresentation createEarthAtmosphereWorksitePresentation() {
		EarthAtmosphereWorksitePresentationImpl earthAtmosphereWorksitePresentation = new EarthAtmosphereWorksitePresentationImpl();
		return earthAtmosphereWorksitePresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthAtmosphereWorksiteWizardPagesProvider createEarthAtmosphereWorksiteWizardPagesProvider() {
		EarthAtmosphereWorksiteWizardPagesProviderImpl earthAtmosphereWorksiteWizardPagesProvider = new EarthAtmosphereWorksiteWizardPagesProviderImpl();
		return earthAtmosphereWorksiteWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthAtmosphereEnvironmentUIPackage getApogyEarthAtmosphereEnvironmentUIPackage() {
		return (ApogyEarthAtmosphereEnvironmentUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyEarthAtmosphereEnvironmentUIPackage getPackage() {
		return ApogyEarthAtmosphereEnvironmentUIPackage.eINSTANCE;
	}

} //ApogyEarthAtmosphereEnvironmentUIFactoryImpl

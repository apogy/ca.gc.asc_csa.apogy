package ca.gc.asc_csa.apogy.common.databinding.converters;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.text.NumberFormat;
import java.text.ParseException;

public class StringToIntegerConverter extends AbstractNumberConverter {

	public StringToIntegerConverter() {
		super(String.class, Integer.class);
	}

	public StringToIntegerConverter(NumberFormat numberFormat) {
		super(String.class, Integer.class);
		setNumberFormat(numberFormat);
	}
	
	public Object convert(Object fromObject) 
	{
		if(getNumberFormat() != null)
		{				
			try 
			{
				return new Integer(getNumberFormat().parse( (String) fromObject).intValue());
			} 
			catch (ParseException e) 
			{			
				e.printStackTrace();
			}
		}
		return new Integer((String)fromObject);	
	}
}

package ca.gc.asc_csa.apogy.common.databinding.converters;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.core.databinding.conversion.Converter;


public class PercentToFloatConverter extends Converter 
{
	public PercentToFloatConverter()
	{
		super(Integer.class, Float.class);
	}
	
	public Object convert(Object fromObject) 
	{
		Integer value = (Integer) fromObject;
		if(value > 100)
			value = 100;
		else if(value < 0)
			value = 0;
		return  value / 100.0f;
	}
}

package ca.gc.asc_csa.apogy.common.databinding.converters;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.text.NumberFormat;

public class FloatToStringAbsoluteConverter extends AbstractNumberConverter 
{
	public FloatToStringAbsoluteConverter()
	{
		super(Float.class, String.class);
	}
	
	public FloatToStringAbsoluteConverter(NumberFormat numberFormat)
	{
		super(Float.class, String.class);
		setNumberFormat(numberFormat);
	}
		
	public Object convert(Object fromObject) 
	{
		if(getNumberFormat() != null)
		{
			try
			{
				Float value = (Float) Math.abs((Float) fromObject);
				return getNumberFormat().format(value);
			}
			catch(Exception e)
			{
				return fromObject.toString();
			}
		}
		else
		{
			return fromObject.toString();
		}
	}
}

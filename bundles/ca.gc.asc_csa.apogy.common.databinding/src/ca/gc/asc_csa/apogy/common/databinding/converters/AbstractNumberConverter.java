package ca.gc.asc_csa.apogy.common.databinding.converters;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.text.NumberFormat;

import org.eclipse.core.databinding.conversion.Converter;

public abstract class AbstractNumberConverter extends Converter 
{	
	private NumberFormat numberFormat = null;
	
	public AbstractNumberConverter() 
	{
		super(Number.class, String.class);
	}

	public AbstractNumberConverter(Object fromType, Object toType)
	{
		super(fromType, toType);
	}
	
	public NumberFormat getNumberFormat() {
		return numberFormat;
	}

	public void setNumberFormat(NumberFormat numberFormat) {
		this.numberFormat = numberFormat;
	}
}

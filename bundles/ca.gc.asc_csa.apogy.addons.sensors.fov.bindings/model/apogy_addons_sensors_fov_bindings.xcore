// *****************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
//     Pierre Allard - initial API and implementation
//     Regent L'Archeveque
//     Sebastien Gemme
//        
// SPDX-License-Identifier: EPL-1.0
// *****************************************************************************
@GenModel(prefix="ApogyAddonsSensorsFOVBindings",
 copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Pierre Allard - initial API and implementation
     Regent L'Archeveque
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",	
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  multipleEditorPages="false",
		  modelName="ApogyAddonsSensorsFOVBindings",
		  complianceLevel="6.0",
		  dynamicTemplates="true", 
		  suppressGenModelAnnotations="false",
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.addons.sensors.fov.bindings/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.edit/src-generated")

package ca.gc.asc_csa.apogy.addons.sensors.fov.bindings

import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding
import ca.gc.asc_csa.apogy.addons.sensors.fov.CircularSectorFieldOfView
import ca.gc.asc_csa.apogy.addons.sensors.fov.ConicalFieldOfView
import ca.gc.asc_csa.apogy.addons.sensors.fov.RectangularFrustrumFieldOfView

/**
 * Binding that binds a CircularSectorFieldOfView to another CircularSectorFieldOfView.
 */
class CircularSectorFieldOfViewBinding extends AbstractTopologyBinding
{
	/**
	 * The CircularSectorFieldOfView that the binding updates.
	 */
	@GenModel(propertyCategory="DESTINATION")
	refers CircularSectorFieldOfView fov
}

/**
 * Binding that binds a ConicalFieldOfViewBinding to another ConicalFieldOfViewBinding.
 */
class ConicalFieldOfViewBinding extends AbstractTopologyBinding
{
	/**
	 * The ConicalFieldOfView that binding updates,
	 */
	@GenModel(propertyCategory="DESTINATION")
	refers ConicalFieldOfView fov
}

/**
 * Binding that binds a RectangularFrustrumFieldOfViewBinding to another RectangularFrustrumFieldOfViewBinding.
 */
class RectangularFrustrumFieldOfViewBinding extends AbstractTopologyBinding
{
	/**
	 * The RectangularFrustrumFieldOfViewBinding that binding updates,
	 */
	@GenModel(propertyCategory="DESTINATION")
	refers RectangularFrustrumFieldOfView fov	
}

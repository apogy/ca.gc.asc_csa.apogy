/**
 * Copyright (c) 2015-2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraFactory;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EMFFeatureOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EMFFeatureOverlayPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.EMFFeatureOverlayWizardPage;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EMF Feature Overlay Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EMFFeatureOverlayPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements EMFFeatureOverlayPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EMFFeatureOverlayPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsSensorsImagingCameraPackage.Literals.EMF_FEATURE_OVERLAY_PAGES_PROVIDER;
	}
	
	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		EMFFeatureOverlay overlay = ApogyAddonsSensorsImagingCameraFactory.eINSTANCE.createEMFFeatureOverlay();
		overlay.setVariableFeatureReference(ApogyCoreInvocatorFactory.eINSTANCE.createVariableFeatureReference());	
		
		return overlay;
	}

	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> pages = super.instantiateWizardPages(eObject, settings);

		pages.add(new EMFFeatureOverlayWizardPage((EMFFeatureOverlay) eObject));
		
		return pages;
	}

} //EMFFeatureOverlayPagesProviderImpl

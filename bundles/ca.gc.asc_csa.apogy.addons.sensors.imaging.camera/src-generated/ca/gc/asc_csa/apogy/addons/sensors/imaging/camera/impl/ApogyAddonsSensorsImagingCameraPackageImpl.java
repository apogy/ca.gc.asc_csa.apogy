package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.impl;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.util.List;

import javax.vecmath.Color3f;

import javax.vecmath.Point2d;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ApogyAddonsSensorsImagingPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.AbstractTextOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.AbstractTextOverlayOverlayPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraFactory;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyLogoOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.AzimuthElevationFOVOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.AzimuthFeatureReference;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraImageAnnotation;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraNameOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraNameOverlayPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraOverlayList;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraTool;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraToolList;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfiguration;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfigurationList;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfigurationPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfigurationReference;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewDisplayRotation;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewUtilities;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ContrastAndBrightnessFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ContrastAndBrightnessFilterPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.DrawnCameraOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EMFFeatureAzimuthElevationFOVOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EMFFeatureOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EMFFeatureOverlayPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EdgeFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EdgeFilterPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ElevationFeatureReference;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ExposureFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ExposureFilterPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.FOVOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.FOVOverlayPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.FilterList;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.GainFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.GainFilterPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.GrayScaleFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.GrayScaleFilterPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageCameraOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageCountOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageCountOverlayOverlayPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageFrozenOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageFrozenOverlayPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageSizePolicy;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.InvertFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.InvertFilterPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.OverlayAlignment;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PTZCameraTool;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PTZCameraToolPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PointerCameraTool;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PointerCameraToolPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.RescaleFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.RescaleFilterPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ToolTipTextProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.URLImageOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.URLImageOverlayPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.images.ApogyCommonImagesPackage;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyAddonsSensorsImagingCameraPackageImpl extends EPackageImpl implements ApogyAddonsSensorsImagingCameraPackage
{
  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cameraViewUtilitiesEClass = null;
		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass cameraViewConfigurationListEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass cameraViewConfigurationEClass = null;

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cameraViewConfigurationReferenceEClass = null;
		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass filterListEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass imageFilterEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass grayScaleFilterEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass edgeFilterEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass contrastAndBrightnessFilterEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass exposureFilterEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass gainFilterEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass invertFilterEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass rescaleFilterEClass = null;
  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cameraImageAnnotationEClass = null;
		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass cameraOverlayListEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass cameraOverlayEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass drawnCameraOverlayEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass abstractTextOverlayEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass emfFeatureOverlayEClass = null;
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass cameraNameOverlayEClass = null;
  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass imageCountOverlayEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass imageFrozenOverlayEClass = null;
		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass imageCameraOverlayEClass = null;
  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass urlImageOverlayEClass = null;
		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass apogyLogoOverlayEClass = null;
  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fovOverlayEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass azimuthElevationFOVOverlayEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass emfFeatureAzimuthElevationFOVOverlayEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass azimuthFeatureReferenceEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass elevationFeatureReferenceEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass toolTipTextProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cameraToolEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cameraToolListEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pointerCameraToolEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ptzCameraToolEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cameraViewConfigurationPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractTextOverlayOverlayPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cameraNameOverlayPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass imageFrozenOverlayPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass imageCountOverlayOverlayPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass emfFeatureOverlayPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass urlImageOverlayPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fovOverlayPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pointerCameraToolPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ptzCameraToolPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass grayScaleFilterPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass edgeFilterPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass invertFilterPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contrastAndBrightnessFilterPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gainFilterPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exposureFilterPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rescaleFilterPagesProviderEClass = null;
		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EEnum overlayAlignmentEEnum = null;
  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum cameraViewDisplayRotationEEnum = null;
		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum imageSizePolicyEEnum = null;
		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EDataType color3fEDataType = null;

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType listEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType point2dEDataType = null;

		/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
  private ApogyAddonsSensorsImagingCameraPackageImpl()
  {
		super(eNS_URI, ApogyAddonsSensorsImagingCameraFactory.eINSTANCE);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private static boolean isInited = false;

  /**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyAddonsSensorsImagingCameraPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
  public static ApogyAddonsSensorsImagingCameraPackage init()
  {
		if (isInited) return (ApogyAddonsSensorsImagingCameraPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsSensorsImagingCameraPackage.eNS_URI);

		// Obtain or create and register package
		ApogyAddonsSensorsImagingCameraPackageImpl theApogyAddonsSensorsImagingCameraPackage = (ApogyAddonsSensorsImagingCameraPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyAddonsSensorsImagingCameraPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyAddonsSensorsImagingCameraPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyAddonsPackage.eINSTANCE.eClass();
		ApogyAddonsSensorsImagingPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyAddonsSensorsImagingCameraPackage.createPackageContents();

		// Initialize created meta-data
		theApogyAddonsSensorsImagingCameraPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyAddonsSensorsImagingCameraPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyAddonsSensorsImagingCameraPackage.eNS_URI, theApogyAddonsSensorsImagingCameraPackage);
		return theApogyAddonsSensorsImagingCameraPackage;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCameraViewUtilities() {
		return cameraViewUtilitiesEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraViewUtilities__GetCameraViewConfigurationIdentifier__CameraViewConfiguration() {
		return cameraViewUtilitiesEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraViewUtilities__GetActiveCameraViewConfiguration__String() {
		return cameraViewUtilitiesEClass.getEOperations().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraViewUtilities__GetActiveCameraViewConfigurationList() {
		return cameraViewUtilitiesEClass.getEOperations().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraViewUtilities__AddCameraTool__CameraViewConfiguration_CameraTool() {
		return cameraViewUtilitiesEClass.getEOperations().get(3);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraViewUtilities__RemoveCameraTool__CameraViewConfiguration_CameraTool() {
		return cameraViewUtilitiesEClass.getEOperations().get(4);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getCameraViewConfigurationList()
  {
		return cameraViewConfigurationListEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EReference getCameraViewConfigurationList_CameraViewConfigurations()
  {
		return (EReference)cameraViewConfigurationListEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getCameraViewConfiguration()
  {
		return cameraViewConfigurationEClass;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCameraViewConfiguration_CameraViewDisplayRotation() {
		return (EAttribute)cameraViewConfigurationEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCameraViewConfiguration_CameraViewConfigurationList() {
		return (EReference)cameraViewConfigurationEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EReference getCameraViewConfiguration_Camera()
  {
		return (EReference)cameraViewConfigurationEClass.getEStructuralFeatures().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getCameraViewConfiguration_ImageWidth()
  {
		return (EAttribute)cameraViewConfigurationEClass.getEStructuralFeatures().get(3);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getCameraViewConfiguration_ImageHeight()
  {
		return (EAttribute)cameraViewConfigurationEClass.getEStructuralFeatures().get(4);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCameraViewConfiguration_ImageCount() {
		return (EAttribute)cameraViewConfigurationEClass.getEStructuralFeatures().get(5);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getCameraViewConfiguration_DisplayRectifiedImage()
  {
		return (EAttribute)cameraViewConfigurationEClass.getEStructuralFeatures().get(6);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EReference getCameraViewConfiguration_OverlayList()
  {
		return (EReference)cameraViewConfigurationEClass.getEStructuralFeatures().get(7);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCameraViewConfiguration_ToolList() {
		return (EReference)cameraViewConfigurationEClass.getEStructuralFeatures().get(8);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EReference getCameraViewConfiguration_FilterList()
  {
		return (EReference)cameraViewConfigurationEClass.getEStructuralFeatures().get(9);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCameraViewConfiguration_ImageAutoSaveEnable() {
		return (EAttribute)cameraViewConfigurationEClass.getEStructuralFeatures().get(10);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCameraViewConfiguration_SaveImageWithOverlays() {
		return (EAttribute)cameraViewConfigurationEClass.getEStructuralFeatures().get(11);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCameraViewConfiguration_ImageAutoSaveFolderPath() {
		return (EAttribute)cameraViewConfigurationEClass.getEStructuralFeatures().get(12);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraViewConfiguration__GetCameraImageAnnotations() {
		return cameraViewConfigurationEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCameraViewConfigurationReference() {
		return cameraViewConfigurationReferenceEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCameraViewConfigurationReference_CameraViewConfiguration() {
		return (EReference)cameraViewConfigurationReferenceEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getFilterList()
  {
		return filterListEClass;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFilterList_CameraViewConfiguration() {
		return (EReference)filterListEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EReference getFilterList_ImageFilters()
  {
		return (EReference)filterListEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getImageFilter()
  {
		return imageFilterEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getImageFilter_Enabled()
  {
		return (EAttribute)imageFilterEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EOperation getImageFilter__Filter__AbstractCamera_AbstractEImage()
  {
		return imageFilterEClass.getEOperations().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getImageFilter__Dispose() {
		return imageFilterEClass.getEOperations().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getGrayScaleFilter()
  {
		return grayScaleFilterEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getEdgeFilter()
  {
		return edgeFilterEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getContrastAndBrightnessFilter()
  {
		return contrastAndBrightnessFilterEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getContrastAndBrightnessFilter_Contrast()
  {
		return (EAttribute)contrastAndBrightnessFilterEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getContrastAndBrightnessFilter_Brightness()
  {
		return (EAttribute)contrastAndBrightnessFilterEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getExposureFilter()
  {
		return exposureFilterEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getExposureFilter_Exposure()
  {
		return (EAttribute)exposureFilterEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getGainFilter()
  {
		return gainFilterEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getGainFilter_Gain()
  {
		return (EAttribute)gainFilterEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getGainFilter_Bias()
  {
		return (EAttribute)gainFilterEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getInvertFilter()
  {
		return invertFilterEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getRescaleFilter()
  {
		return rescaleFilterEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getRescaleFilter_Scale()
  {
		return (EAttribute)rescaleFilterEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCameraImageAnnotation() {
		return cameraImageAnnotationEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCameraImageAnnotation_Visible() {
		return (EAttribute)cameraImageAnnotationEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraImageAnnotation__Dispose() {
		return cameraImageAnnotationEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getCameraOverlayList()
  {
		return cameraOverlayListEClass;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCameraOverlayList_CameraViewConfiguration() {
		return (EReference)cameraOverlayListEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EReference getCameraOverlayList_Overlays()
  {
		return (EReference)cameraOverlayListEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getCameraOverlay()
  {
		return cameraOverlayEClass;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCameraOverlay_CameraOverlayList() {
		return (EReference)cameraOverlayEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getCameraOverlay_OverlayAlignment()
  {
		return (EAttribute)cameraOverlayEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getDrawnCameraOverlay()
  {
		return drawnCameraOverlayEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EOperation getDrawnCameraOverlay__ApplyOverlay__AbstractCamera_AbstractEImage_OverlayAlignment_int_int()
  {
		return drawnCameraOverlayEClass.getEOperations().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getAbstractTextOverlay()
  {
		return abstractTextOverlayEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getAbstractTextOverlay_FontName()
  {
		return (EAttribute)abstractTextOverlayEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getAbstractTextOverlay_FontSize()
  {
		return (EAttribute)abstractTextOverlayEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getAbstractTextOverlay_HorizontalOffset()
  {
		return (EAttribute)abstractTextOverlayEClass.getEStructuralFeatures().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getAbstractTextOverlay_VerticalOffset()
  {
		return (EAttribute)abstractTextOverlayEClass.getEStructuralFeatures().get(3);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getAbstractTextOverlay_TextColor()
  {
		return (EAttribute)abstractTextOverlayEClass.getEStructuralFeatures().get(4);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EOperation getAbstractTextOverlay__GetDisplayedString()
  {
		return abstractTextOverlayEClass.getEOperations().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getEMFFeatureOverlay()
  {
		return emfFeatureOverlayEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EReference getEMFFeatureOverlay_VariableFeatureReference()
  {
		return (EReference)emfFeatureOverlayEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EAttribute getEMFFeatureOverlay_NumberFormat()
  {
		return (EAttribute)emfFeatureOverlayEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEMFFeatureOverlay_VariableFeatureReferenceChangeCount() {
		return (EAttribute)emfFeatureOverlayEClass.getEStructuralFeatures().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	public EAttribute getEMFFeatureOverlay_DisplayUnits() {
		return (EAttribute)emfFeatureOverlayEClass.getEStructuralFeatures().get(3);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEMFFeatureOverlay_EnableRangeColoring() {
		return (EAttribute)emfFeatureOverlayEClass.getEStructuralFeatures().get(4);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getCameraNameOverlay()
  {
		return cameraNameOverlayEClass;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImageCountOverlay() {
		return imageCountOverlayEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImageCountOverlay_IndicatorVisible() {
		return (EAttribute)imageCountOverlayEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImageCountOverlay_CountVisible() {
		return (EAttribute)imageCountOverlayEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImageFrozenOverlay() {
		return imageFrozenOverlayEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImageFrozenOverlay_Frozen() {
		return (EAttribute)imageFrozenOverlayEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImageFrozenOverlay_ExpectedImageUpdatePeriod() {
		return (EAttribute)imageFrozenOverlayEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImageFrozenOverlay_FrozenMessage() {
		return (EAttribute)imageFrozenOverlayEClass.getEStructuralFeatures().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getImageCameraOverlay()
  {
		return imageCameraOverlayEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EOperation getImageCameraOverlay__GetOverlay__AbstractCamera_OverlayAlignment_int_int()
  {
		return imageCameraOverlayEClass.getEOperations().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getURLImageOverlay() {
		return urlImageOverlayEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getURLImageOverlay_Url() {
		return (EAttribute)urlImageOverlayEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getURLImageOverlay_ImageSizePolicy() {
		return (EAttribute)urlImageOverlayEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getURLImageOverlay_Image() {
		return (EReference)urlImageOverlayEClass.getEStructuralFeatures().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getApogyLogoOverlay()
  {
		return apogyLogoOverlayEClass;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFOVOverlay() {
		return fovOverlayEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFOVOverlay_LineWidth() {
		return (EAttribute)fovOverlayEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFOVOverlay_PositiveValuesColor() {
		return (EAttribute)fovOverlayEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFOVOverlay_NegativeValueColor() {
		return (EAttribute)fovOverlayEClass.getEStructuralFeatures().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFOVOverlay_AngleInterval() {
		return (EAttribute)fovOverlayEClass.getEStructuralFeatures().get(3);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFOVOverlay_FontName() {
		return (EAttribute)fovOverlayEClass.getEStructuralFeatures().get(4);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFOVOverlay_FontSize() {
		return (EAttribute)fovOverlayEClass.getEStructuralFeatures().get(5);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFOVOverlay_AzimuthDirection() {
		return (EAttribute)fovOverlayEClass.getEStructuralFeatures().get(6);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFOVOverlay_ElevationDirection() {
		return (EAttribute)fovOverlayEClass.getEStructuralFeatures().get(7);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAzimuthElevationFOVOverlay() {
		return azimuthElevationFOVOverlayEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAzimuthElevationFOVOverlay__ChangeAzimuth__double() {
		return azimuthElevationFOVOverlayEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAzimuthElevationFOVOverlay__ChangeElevation__double() {
		return azimuthElevationFOVOverlayEClass.getEOperations().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEMFFeatureAzimuthElevationFOVOverlay() {
		return emfFeatureAzimuthElevationFOVOverlayEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEMFFeatureAzimuthElevationFOVOverlay_AzimuthFeatureReference() {
		return (EReference)emfFeatureAzimuthElevationFOVOverlayEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEMFFeatureAzimuthElevationFOVOverlay_ElevationFeatureReference() {
		return (EReference)emfFeatureAzimuthElevationFOVOverlayEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAzimuthFeatureReference() {
		return azimuthFeatureReferenceEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElevationFeatureReference() {
		return elevationFeatureReferenceEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getToolTipTextProvider() {
		return toolTipTextProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getToolTipTextProvider__GetToolTipText__AbstractCamera_ImageSnapshot_int_int_int() {
		return toolTipTextProviderEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCameraTool() {
		return cameraToolEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCameraTool_CameraToolList() {
		return (EReference)cameraToolEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraTool__InitializeCamera__AbstractCamera() {
		return cameraToolEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraTool__UpdateImageSnapshot__ImageSnapshot() {
		return cameraToolEClass.getEOperations().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraTool__Dispose() {
		return cameraToolEClass.getEOperations().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraTool__MouseMoved__AbstractEImage_int_int_int() {
		return cameraToolEClass.getEOperations().get(3);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCameraTool__PositionSelected__AbstractEImage_int_int_int() {
		return cameraToolEClass.getEOperations().get(4);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCameraToolList() {
		return cameraToolListEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCameraToolList_CameraViewConfiguration() {
		return (EReference)cameraToolListEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCameraToolList_Tools() {
		return (EReference)cameraToolListEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPointerCameraTool() {
		return pointerCameraToolEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPointerCameraTool_VectorColor() {
		return (EAttribute)pointerCameraToolEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPointerCameraTool_IntersectionDistance() {
		return (EAttribute)pointerCameraToolEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPTZCameraTool() {
		return ptzCameraToolEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPTZCameraTool_SelectionBoxColor() {
		return (EAttribute)ptzCameraToolEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPTZCameraTool_UserSelectionCorner0() {
		return (EAttribute)ptzCameraToolEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPTZCameraTool_UserSelectionCorner1() {
		return (EAttribute)ptzCameraToolEClass.getEStructuralFeatures().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPTZCameraTool__CommandPTZ__double_double_double_double() {
		return ptzCameraToolEClass.getEOperations().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPTZCameraTool__ClearUserSelection() {
		return ptzCameraToolEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCameraViewConfigurationPagesProvider() {
		return cameraViewConfigurationPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractTextOverlayOverlayPagesProvider() {
		return abstractTextOverlayOverlayPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCameraNameOverlayPagesProvider() {
		return cameraNameOverlayPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImageFrozenOverlayPagesProvider() {
		return imageFrozenOverlayPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImageCountOverlayOverlayPagesProvider() {
		return imageCountOverlayOverlayPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEMFFeatureOverlayPagesProvider() {
		return emfFeatureOverlayPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getURLImageOverlayPagesProvider() {
		return urlImageOverlayPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFOVOverlayPagesProvider() {
		return fovOverlayPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPointerCameraToolPagesProvider() {
		return pointerCameraToolPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPTZCameraToolPagesProvider() {
		return ptzCameraToolPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGrayScaleFilterPagesProvider() {
		return grayScaleFilterPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEdgeFilterPagesProvider() {
		return edgeFilterPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInvertFilterPagesProvider() {
		return invertFilterPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContrastAndBrightnessFilterPagesProvider() {
		return contrastAndBrightnessFilterPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGainFilterPagesProvider() {
		return gainFilterPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExposureFilterPagesProvider() {
		return exposureFilterPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRescaleFilterPagesProvider() {
		return rescaleFilterPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EEnum getOverlayAlignment()
  {
		return overlayAlignmentEEnum;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCameraViewDisplayRotation() {
		return cameraViewDisplayRotationEEnum;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getImageSizePolicy() {
		return imageSizePolicyEEnum;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EDataType getColor3f()
  {
		return color3fEDataType;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getList() {
		return listEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPoint2d() {
		return point2dEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsSensorsImagingCameraFactory getApogyAddonsSensorsImagingCameraFactory() {
		return (ApogyAddonsSensorsImagingCameraFactory)getEFactoryInstance();
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isCreated = false;

  /**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void createPackageContents()
  {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		cameraViewUtilitiesEClass = createEClass(CAMERA_VIEW_UTILITIES);
		createEOperation(cameraViewUtilitiesEClass, CAMERA_VIEW_UTILITIES___GET_CAMERA_VIEW_CONFIGURATION_IDENTIFIER__CAMERAVIEWCONFIGURATION);
		createEOperation(cameraViewUtilitiesEClass, CAMERA_VIEW_UTILITIES___GET_ACTIVE_CAMERA_VIEW_CONFIGURATION__STRING);
		createEOperation(cameraViewUtilitiesEClass, CAMERA_VIEW_UTILITIES___GET_ACTIVE_CAMERA_VIEW_CONFIGURATION_LIST);
		createEOperation(cameraViewUtilitiesEClass, CAMERA_VIEW_UTILITIES___ADD_CAMERA_TOOL__CAMERAVIEWCONFIGURATION_CAMERATOOL);
		createEOperation(cameraViewUtilitiesEClass, CAMERA_VIEW_UTILITIES___REMOVE_CAMERA_TOOL__CAMERAVIEWCONFIGURATION_CAMERATOOL);

		cameraViewConfigurationListEClass = createEClass(CAMERA_VIEW_CONFIGURATION_LIST);
		createEReference(cameraViewConfigurationListEClass, CAMERA_VIEW_CONFIGURATION_LIST__CAMERA_VIEW_CONFIGURATIONS);

		cameraViewConfigurationEClass = createEClass(CAMERA_VIEW_CONFIGURATION);
		createEAttribute(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__CAMERA_VIEW_DISPLAY_ROTATION);
		createEReference(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__CAMERA_VIEW_CONFIGURATION_LIST);
		createEReference(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__CAMERA);
		createEAttribute(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__IMAGE_WIDTH);
		createEAttribute(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__IMAGE_HEIGHT);
		createEAttribute(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__IMAGE_COUNT);
		createEAttribute(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__DISPLAY_RECTIFIED_IMAGE);
		createEReference(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__OVERLAY_LIST);
		createEReference(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__TOOL_LIST);
		createEReference(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__FILTER_LIST);
		createEAttribute(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__IMAGE_AUTO_SAVE_ENABLE);
		createEAttribute(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__SAVE_IMAGE_WITH_OVERLAYS);
		createEAttribute(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION__IMAGE_AUTO_SAVE_FOLDER_PATH);
		createEOperation(cameraViewConfigurationEClass, CAMERA_VIEW_CONFIGURATION___GET_CAMERA_IMAGE_ANNOTATIONS);

		cameraViewConfigurationReferenceEClass = createEClass(CAMERA_VIEW_CONFIGURATION_REFERENCE);
		createEReference(cameraViewConfigurationReferenceEClass, CAMERA_VIEW_CONFIGURATION_REFERENCE__CAMERA_VIEW_CONFIGURATION);

		filterListEClass = createEClass(FILTER_LIST);
		createEReference(filterListEClass, FILTER_LIST__CAMERA_VIEW_CONFIGURATION);
		createEReference(filterListEClass, FILTER_LIST__IMAGE_FILTERS);

		imageFilterEClass = createEClass(IMAGE_FILTER);
		createEAttribute(imageFilterEClass, IMAGE_FILTER__ENABLED);
		createEOperation(imageFilterEClass, IMAGE_FILTER___FILTER__ABSTRACTCAMERA_ABSTRACTEIMAGE);
		createEOperation(imageFilterEClass, IMAGE_FILTER___DISPOSE);

		grayScaleFilterEClass = createEClass(GRAY_SCALE_FILTER);

		edgeFilterEClass = createEClass(EDGE_FILTER);

		contrastAndBrightnessFilterEClass = createEClass(CONTRAST_AND_BRIGHTNESS_FILTER);
		createEAttribute(contrastAndBrightnessFilterEClass, CONTRAST_AND_BRIGHTNESS_FILTER__CONTRAST);
		createEAttribute(contrastAndBrightnessFilterEClass, CONTRAST_AND_BRIGHTNESS_FILTER__BRIGHTNESS);

		exposureFilterEClass = createEClass(EXPOSURE_FILTER);
		createEAttribute(exposureFilterEClass, EXPOSURE_FILTER__EXPOSURE);

		gainFilterEClass = createEClass(GAIN_FILTER);
		createEAttribute(gainFilterEClass, GAIN_FILTER__GAIN);
		createEAttribute(gainFilterEClass, GAIN_FILTER__BIAS);

		invertFilterEClass = createEClass(INVERT_FILTER);

		rescaleFilterEClass = createEClass(RESCALE_FILTER);
		createEAttribute(rescaleFilterEClass, RESCALE_FILTER__SCALE);

		cameraImageAnnotationEClass = createEClass(CAMERA_IMAGE_ANNOTATION);
		createEAttribute(cameraImageAnnotationEClass, CAMERA_IMAGE_ANNOTATION__VISIBLE);
		createEOperation(cameraImageAnnotationEClass, CAMERA_IMAGE_ANNOTATION___DISPOSE);

		cameraOverlayListEClass = createEClass(CAMERA_OVERLAY_LIST);
		createEReference(cameraOverlayListEClass, CAMERA_OVERLAY_LIST__CAMERA_VIEW_CONFIGURATION);
		createEReference(cameraOverlayListEClass, CAMERA_OVERLAY_LIST__OVERLAYS);

		cameraOverlayEClass = createEClass(CAMERA_OVERLAY);
		createEReference(cameraOverlayEClass, CAMERA_OVERLAY__CAMERA_OVERLAY_LIST);
		createEAttribute(cameraOverlayEClass, CAMERA_OVERLAY__OVERLAY_ALIGNMENT);

		drawnCameraOverlayEClass = createEClass(DRAWN_CAMERA_OVERLAY);
		createEOperation(drawnCameraOverlayEClass, DRAWN_CAMERA_OVERLAY___APPLY_OVERLAY__ABSTRACTCAMERA_ABSTRACTEIMAGE_OVERLAYALIGNMENT_INT_INT);

		abstractTextOverlayEClass = createEClass(ABSTRACT_TEXT_OVERLAY);
		createEAttribute(abstractTextOverlayEClass, ABSTRACT_TEXT_OVERLAY__FONT_NAME);
		createEAttribute(abstractTextOverlayEClass, ABSTRACT_TEXT_OVERLAY__FONT_SIZE);
		createEAttribute(abstractTextOverlayEClass, ABSTRACT_TEXT_OVERLAY__HORIZONTAL_OFFSET);
		createEAttribute(abstractTextOverlayEClass, ABSTRACT_TEXT_OVERLAY__VERTICAL_OFFSET);
		createEAttribute(abstractTextOverlayEClass, ABSTRACT_TEXT_OVERLAY__TEXT_COLOR);
		createEOperation(abstractTextOverlayEClass, ABSTRACT_TEXT_OVERLAY___GET_DISPLAYED_STRING);

		emfFeatureOverlayEClass = createEClass(EMF_FEATURE_OVERLAY);
		createEReference(emfFeatureOverlayEClass, EMF_FEATURE_OVERLAY__VARIABLE_FEATURE_REFERENCE);
		createEAttribute(emfFeatureOverlayEClass, EMF_FEATURE_OVERLAY__NUMBER_FORMAT);
		createEAttribute(emfFeatureOverlayEClass, EMF_FEATURE_OVERLAY__VARIABLE_FEATURE_REFERENCE_CHANGE_COUNT);
		createEAttribute(emfFeatureOverlayEClass, EMF_FEATURE_OVERLAY__DISPLAY_UNITS);
		createEAttribute(emfFeatureOverlayEClass, EMF_FEATURE_OVERLAY__ENABLE_RANGE_COLORING);

		cameraNameOverlayEClass = createEClass(CAMERA_NAME_OVERLAY);

		imageCountOverlayEClass = createEClass(IMAGE_COUNT_OVERLAY);
		createEAttribute(imageCountOverlayEClass, IMAGE_COUNT_OVERLAY__INDICATOR_VISIBLE);
		createEAttribute(imageCountOverlayEClass, IMAGE_COUNT_OVERLAY__COUNT_VISIBLE);

		imageFrozenOverlayEClass = createEClass(IMAGE_FROZEN_OVERLAY);
		createEAttribute(imageFrozenOverlayEClass, IMAGE_FROZEN_OVERLAY__FROZEN);
		createEAttribute(imageFrozenOverlayEClass, IMAGE_FROZEN_OVERLAY__EXPECTED_IMAGE_UPDATE_PERIOD);
		createEAttribute(imageFrozenOverlayEClass, IMAGE_FROZEN_OVERLAY__FROZEN_MESSAGE);

		imageCameraOverlayEClass = createEClass(IMAGE_CAMERA_OVERLAY);
		createEOperation(imageCameraOverlayEClass, IMAGE_CAMERA_OVERLAY___GET_OVERLAY__ABSTRACTCAMERA_OVERLAYALIGNMENT_INT_INT);

		urlImageOverlayEClass = createEClass(URL_IMAGE_OVERLAY);
		createEAttribute(urlImageOverlayEClass, URL_IMAGE_OVERLAY__URL);
		createEAttribute(urlImageOverlayEClass, URL_IMAGE_OVERLAY__IMAGE_SIZE_POLICY);
		createEReference(urlImageOverlayEClass, URL_IMAGE_OVERLAY__IMAGE);

		apogyLogoOverlayEClass = createEClass(APOGY_LOGO_OVERLAY);

		fovOverlayEClass = createEClass(FOV_OVERLAY);
		createEAttribute(fovOverlayEClass, FOV_OVERLAY__LINE_WIDTH);
		createEAttribute(fovOverlayEClass, FOV_OVERLAY__POSITIVE_VALUES_COLOR);
		createEAttribute(fovOverlayEClass, FOV_OVERLAY__NEGATIVE_VALUE_COLOR);
		createEAttribute(fovOverlayEClass, FOV_OVERLAY__ANGLE_INTERVAL);
		createEAttribute(fovOverlayEClass, FOV_OVERLAY__FONT_NAME);
		createEAttribute(fovOverlayEClass, FOV_OVERLAY__FONT_SIZE);
		createEAttribute(fovOverlayEClass, FOV_OVERLAY__AZIMUTH_DIRECTION);
		createEAttribute(fovOverlayEClass, FOV_OVERLAY__ELEVATION_DIRECTION);

		azimuthElevationFOVOverlayEClass = createEClass(AZIMUTH_ELEVATION_FOV_OVERLAY);
		createEOperation(azimuthElevationFOVOverlayEClass, AZIMUTH_ELEVATION_FOV_OVERLAY___CHANGE_AZIMUTH__DOUBLE);
		createEOperation(azimuthElevationFOVOverlayEClass, AZIMUTH_ELEVATION_FOV_OVERLAY___CHANGE_ELEVATION__DOUBLE);

		emfFeatureAzimuthElevationFOVOverlayEClass = createEClass(EMF_FEATURE_AZIMUTH_ELEVATION_FOV_OVERLAY);
		createEReference(emfFeatureAzimuthElevationFOVOverlayEClass, EMF_FEATURE_AZIMUTH_ELEVATION_FOV_OVERLAY__AZIMUTH_FEATURE_REFERENCE);
		createEReference(emfFeatureAzimuthElevationFOVOverlayEClass, EMF_FEATURE_AZIMUTH_ELEVATION_FOV_OVERLAY__ELEVATION_FEATURE_REFERENCE);

		azimuthFeatureReferenceEClass = createEClass(AZIMUTH_FEATURE_REFERENCE);

		elevationFeatureReferenceEClass = createEClass(ELEVATION_FEATURE_REFERENCE);

		toolTipTextProviderEClass = createEClass(TOOL_TIP_TEXT_PROVIDER);
		createEOperation(toolTipTextProviderEClass, TOOL_TIP_TEXT_PROVIDER___GET_TOOL_TIP_TEXT__ABSTRACTCAMERA_IMAGESNAPSHOT_INT_INT_INT);

		cameraToolEClass = createEClass(CAMERA_TOOL);
		createEReference(cameraToolEClass, CAMERA_TOOL__CAMERA_TOOL_LIST);
		createEOperation(cameraToolEClass, CAMERA_TOOL___INITIALIZE_CAMERA__ABSTRACTCAMERA);
		createEOperation(cameraToolEClass, CAMERA_TOOL___UPDATE_IMAGE_SNAPSHOT__IMAGESNAPSHOT);
		createEOperation(cameraToolEClass, CAMERA_TOOL___DISPOSE);
		createEOperation(cameraToolEClass, CAMERA_TOOL___MOUSE_MOVED__ABSTRACTEIMAGE_INT_INT_INT);
		createEOperation(cameraToolEClass, CAMERA_TOOL___POSITION_SELECTED__ABSTRACTEIMAGE_INT_INT_INT);

		cameraToolListEClass = createEClass(CAMERA_TOOL_LIST);
		createEReference(cameraToolListEClass, CAMERA_TOOL_LIST__CAMERA_VIEW_CONFIGURATION);
		createEReference(cameraToolListEClass, CAMERA_TOOL_LIST__TOOLS);

		pointerCameraToolEClass = createEClass(POINTER_CAMERA_TOOL);
		createEAttribute(pointerCameraToolEClass, POINTER_CAMERA_TOOL__VECTOR_COLOR);
		createEAttribute(pointerCameraToolEClass, POINTER_CAMERA_TOOL__INTERSECTION_DISTANCE);

		ptzCameraToolEClass = createEClass(PTZ_CAMERA_TOOL);
		createEAttribute(ptzCameraToolEClass, PTZ_CAMERA_TOOL__SELECTION_BOX_COLOR);
		createEAttribute(ptzCameraToolEClass, PTZ_CAMERA_TOOL__USER_SELECTION_CORNER0);
		createEAttribute(ptzCameraToolEClass, PTZ_CAMERA_TOOL__USER_SELECTION_CORNER1);
		createEOperation(ptzCameraToolEClass, PTZ_CAMERA_TOOL___CLEAR_USER_SELECTION);
		createEOperation(ptzCameraToolEClass, PTZ_CAMERA_TOOL___COMMAND_PTZ__DOUBLE_DOUBLE_DOUBLE_DOUBLE);

		cameraViewConfigurationPagesProviderEClass = createEClass(CAMERA_VIEW_CONFIGURATION_PAGES_PROVIDER);

		abstractTextOverlayOverlayPagesProviderEClass = createEClass(ABSTRACT_TEXT_OVERLAY_OVERLAY_PAGES_PROVIDER);

		cameraNameOverlayPagesProviderEClass = createEClass(CAMERA_NAME_OVERLAY_PAGES_PROVIDER);

		imageFrozenOverlayPagesProviderEClass = createEClass(IMAGE_FROZEN_OVERLAY_PAGES_PROVIDER);

		imageCountOverlayOverlayPagesProviderEClass = createEClass(IMAGE_COUNT_OVERLAY_OVERLAY_PAGES_PROVIDER);

		emfFeatureOverlayPagesProviderEClass = createEClass(EMF_FEATURE_OVERLAY_PAGES_PROVIDER);

		urlImageOverlayPagesProviderEClass = createEClass(URL_IMAGE_OVERLAY_PAGES_PROVIDER);

		fovOverlayPagesProviderEClass = createEClass(FOV_OVERLAY_PAGES_PROVIDER);

		pointerCameraToolPagesProviderEClass = createEClass(POINTER_CAMERA_TOOL_PAGES_PROVIDER);

		ptzCameraToolPagesProviderEClass = createEClass(PTZ_CAMERA_TOOL_PAGES_PROVIDER);

		grayScaleFilterPagesProviderEClass = createEClass(GRAY_SCALE_FILTER_PAGES_PROVIDER);

		edgeFilterPagesProviderEClass = createEClass(EDGE_FILTER_PAGES_PROVIDER);

		invertFilterPagesProviderEClass = createEClass(INVERT_FILTER_PAGES_PROVIDER);

		contrastAndBrightnessFilterPagesProviderEClass = createEClass(CONTRAST_AND_BRIGHTNESS_FILTER_PAGES_PROVIDER);

		gainFilterPagesProviderEClass = createEClass(GAIN_FILTER_PAGES_PROVIDER);

		exposureFilterPagesProviderEClass = createEClass(EXPOSURE_FILTER_PAGES_PROVIDER);

		rescaleFilterPagesProviderEClass = createEClass(RESCALE_FILTER_PAGES_PROVIDER);

		// Create enums
		overlayAlignmentEEnum = createEEnum(OVERLAY_ALIGNMENT);
		cameraViewDisplayRotationEEnum = createEEnum(CAMERA_VIEW_DISPLAY_ROTATION);
		imageSizePolicyEEnum = createEEnum(IMAGE_SIZE_POLICY);

		// Create data types
		color3fEDataType = createEDataType(COLOR3F);
		listEDataType = createEDataType(LIST);
		point2dEDataType = createEDataType(POINT2D);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isInitialized = false;

  /**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void initializePackageContents()
  {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogyCoreInvocatorPackage theApogyCoreInvocatorPackage = (ApogyCoreInvocatorPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCoreInvocatorPackage.eNS_URI);
		ApogyAddonsPackage theApogyAddonsPackage = (ApogyAddonsPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsPackage.eNS_URI);
		ApogyAddonsSensorsImagingPackage theApogyAddonsSensorsImagingPackage = (ApogyAddonsSensorsImagingPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsSensorsImagingPackage.eNS_URI);
		ApogyCommonEMFPackage theApogyCommonEMFPackage = (ApogyCommonEMFPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFPackage.eNS_URI);
		ApogyCommonImagesPackage theApogyCommonImagesPackage = (ApogyCommonImagesPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonImagesPackage.eNS_URI);
		ApogyCorePackage theApogyCorePackage = (ApogyCorePackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCorePackage.eNS_URI);
		ApogyCommonEMFUIPackage theApogyCommonEMFUIPackage = (ApogyCommonEMFUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFUIPackage.eNS_URI);

		// Create type parameters
		addETypeParameter(listEDataType, "T");

		// Set bounds for type parameters

		// Add supertypes to classes
		cameraViewConfigurationListEClass.getESuperTypes().add(theApogyCoreInvocatorPackage.getAbstractToolsListContainer());
		cameraViewConfigurationEClass.getESuperTypes().add(theApogyCoreInvocatorPackage.getVariableFeatureReference());
		cameraViewConfigurationEClass.getESuperTypes().add(theApogyAddonsPackage.getAbstractTool());
		imageFilterEClass.getESuperTypes().add(theApogyCommonEMFPackage.getNamed());
		imageFilterEClass.getESuperTypes().add(theApogyCommonEMFPackage.getDescribed());
		grayScaleFilterEClass.getESuperTypes().add(this.getImageFilter());
		edgeFilterEClass.getESuperTypes().add(this.getImageFilter());
		contrastAndBrightnessFilterEClass.getESuperTypes().add(this.getImageFilter());
		exposureFilterEClass.getESuperTypes().add(this.getImageFilter());
		gainFilterEClass.getESuperTypes().add(this.getImageFilter());
		invertFilterEClass.getESuperTypes().add(this.getImageFilter());
		rescaleFilterEClass.getESuperTypes().add(this.getImageFilter());
		cameraImageAnnotationEClass.getESuperTypes().add(theApogyCommonEMFPackage.getNamed());
		cameraImageAnnotationEClass.getESuperTypes().add(theApogyCommonEMFPackage.getDescribed());
		cameraOverlayEClass.getESuperTypes().add(this.getCameraImageAnnotation());
		drawnCameraOverlayEClass.getESuperTypes().add(this.getCameraOverlay());
		abstractTextOverlayEClass.getESuperTypes().add(this.getDrawnCameraOverlay());
		emfFeatureOverlayEClass.getESuperTypes().add(this.getAbstractTextOverlay());
		cameraNameOverlayEClass.getESuperTypes().add(this.getAbstractTextOverlay());
		imageCountOverlayEClass.getESuperTypes().add(this.getAbstractTextOverlay());
		imageFrozenOverlayEClass.getESuperTypes().add(this.getAbstractTextOverlay());
		imageCameraOverlayEClass.getESuperTypes().add(this.getCameraOverlay());
		urlImageOverlayEClass.getESuperTypes().add(this.getImageCameraOverlay());
		apogyLogoOverlayEClass.getESuperTypes().add(this.getImageCameraOverlay());
		fovOverlayEClass.getESuperTypes().add(this.getDrawnCameraOverlay());
		fovOverlayEClass.getESuperTypes().add(this.getToolTipTextProvider());
		azimuthElevationFOVOverlayEClass.getESuperTypes().add(this.getFOVOverlay());
		emfFeatureAzimuthElevationFOVOverlayEClass.getESuperTypes().add(this.getAzimuthElevationFOVOverlay());
		azimuthFeatureReferenceEClass.getESuperTypes().add(theApogyCoreInvocatorPackage.getVariableFeatureReference());
		elevationFeatureReferenceEClass.getESuperTypes().add(theApogyCoreInvocatorPackage.getVariableFeatureReference());
		cameraToolEClass.getESuperTypes().add(this.getCameraImageAnnotation());
		pointerCameraToolEClass.getESuperTypes().add(this.getCameraTool());
		pointerCameraToolEClass.getESuperTypes().add(this.getToolTipTextProvider());
		pointerCameraToolEClass.getESuperTypes().add(theApogyCorePackage.getAbsolutePoseProvider());
		ptzCameraToolEClass.getESuperTypes().add(this.getFOVOverlay());
		ptzCameraToolEClass.getESuperTypes().add(this.getCameraTool());
		cameraViewConfigurationPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		abstractTextOverlayOverlayPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		cameraNameOverlayPagesProviderEClass.getESuperTypes().add(this.getAbstractTextOverlayOverlayPagesProvider());
		imageFrozenOverlayPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		imageCountOverlayOverlayPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		emfFeatureOverlayPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		urlImageOverlayPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		fovOverlayPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		pointerCameraToolPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		ptzCameraToolPagesProviderEClass.getESuperTypes().add(this.getFOVOverlayPagesProvider());
		grayScaleFilterPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		edgeFilterPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		invertFilterPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		contrastAndBrightnessFilterPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		gainFilterPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		exposureFilterPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		rescaleFilterPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(cameraViewUtilitiesEClass, CameraViewUtilities.class, "CameraViewUtilities", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getCameraViewUtilities__GetCameraViewConfigurationIdentifier__CameraViewConfiguration(), theEcorePackage.getEString(), "getCameraViewConfigurationIdentifier", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCameraViewConfiguration(), "cameraViewConfiguration", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCameraViewUtilities__GetActiveCameraViewConfiguration__String(), this.getCameraViewConfiguration(), "getActiveCameraViewConfiguration", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "identifier", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getCameraViewUtilities__GetActiveCameraViewConfigurationList(), this.getCameraViewConfigurationList(), "getActiveCameraViewConfigurationList", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCameraViewUtilities__AddCameraTool__CameraViewConfiguration_CameraTool(), null, "addCameraTool", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCameraViewConfiguration(), "cameraViewConfiguration", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCameraTool(), "cameraTool", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCameraViewUtilities__RemoveCameraTool__CameraViewConfiguration_CameraTool(), null, "removeCameraTool", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCameraViewConfiguration(), "cameraViewConfiguration", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCameraTool(), "cameraTool", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(cameraViewConfigurationListEClass, CameraViewConfigurationList.class, "CameraViewConfigurationList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCameraViewConfigurationList_CameraViewConfigurations(), this.getCameraViewConfiguration(), this.getCameraViewConfiguration_CameraViewConfigurationList(), "cameraViewConfigurations", null, 0, -1, CameraViewConfigurationList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cameraViewConfigurationEClass, CameraViewConfiguration.class, "CameraViewConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCameraViewConfiguration_CameraViewDisplayRotation(), this.getCameraViewDisplayRotation(), "cameraViewDisplayRotation", "None", 0, 1, CameraViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCameraViewConfiguration_CameraViewConfigurationList(), this.getCameraViewConfigurationList(), this.getCameraViewConfigurationList_CameraViewConfigurations(), "cameraViewConfigurationList", null, 0, 1, CameraViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCameraViewConfiguration_Camera(), theApogyAddonsSensorsImagingPackage.getAbstractCamera(), null, "camera", null, 0, 1, CameraViewConfiguration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCameraViewConfiguration_ImageWidth(), theEcorePackage.getEInt(), "imageWidth", "640", 0, 1, CameraViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCameraViewConfiguration_ImageHeight(), theEcorePackage.getEInt(), "imageHeight", "480", 0, 1, CameraViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCameraViewConfiguration_ImageCount(), theEcorePackage.getEInt(), "imageCount", "0", 0, 1, CameraViewConfiguration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCameraViewConfiguration_DisplayRectifiedImage(), theEcorePackage.getEBoolean(), "displayRectifiedImage", "true", 0, 1, CameraViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCameraViewConfiguration_OverlayList(), this.getCameraOverlayList(), this.getCameraOverlayList_CameraViewConfiguration(), "overlayList", null, 1, 1, CameraViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCameraViewConfiguration_ToolList(), this.getCameraToolList(), this.getCameraToolList_CameraViewConfiguration(), "toolList", null, 1, 1, CameraViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCameraViewConfiguration_FilterList(), this.getFilterList(), this.getFilterList_CameraViewConfiguration(), "filterList", null, 1, 1, CameraViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCameraViewConfiguration_ImageAutoSaveEnable(), theEcorePackage.getEBoolean(), "imageAutoSaveEnable", "false", 0, 1, CameraViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCameraViewConfiguration_SaveImageWithOverlays(), theEcorePackage.getEBoolean(), "saveImageWithOverlays", "false", 0, 1, CameraViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCameraViewConfiguration_ImageAutoSaveFolderPath(), theEcorePackage.getEString(), "imageAutoSaveFolderPath", null, 0, 1, CameraViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getCameraViewConfiguration__GetCameraImageAnnotations(), null, "getCameraImageAnnotations", 0, 1, !IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(this.getList());
		EGenericType g2 = createEGenericType(this.getCameraImageAnnotation());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		initEClass(cameraViewConfigurationReferenceEClass, CameraViewConfigurationReference.class, "CameraViewConfigurationReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCameraViewConfigurationReference_CameraViewConfiguration(), this.getCameraViewConfiguration(), null, "cameraViewConfiguration", null, 0, 1, CameraViewConfigurationReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(filterListEClass, FilterList.class, "FilterList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFilterList_CameraViewConfiguration(), this.getCameraViewConfiguration(), this.getCameraViewConfiguration_FilterList(), "cameraViewConfiguration", null, 0, 1, FilterList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFilterList_ImageFilters(), this.getImageFilter(), null, "imageFilters", null, 0, -1, FilterList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(imageFilterEClass, ImageFilter.class, "ImageFilter", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImageFilter_Enabled(), theEcorePackage.getEBoolean(), "enabled", "true", 0, 1, ImageFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getImageFilter__Filter__AbstractCamera_AbstractEImage(), theApogyCommonImagesPackage.getAbstractEImage(), "filter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyAddonsSensorsImagingPackage.getAbstractCamera(), "camera", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonImagesPackage.getAbstractEImage(), "cameraImage", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getImageFilter__Dispose(), null, "dispose", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(grayScaleFilterEClass, GrayScaleFilter.class, "GrayScaleFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(edgeFilterEClass, EdgeFilter.class, "EdgeFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(contrastAndBrightnessFilterEClass, ContrastAndBrightnessFilter.class, "ContrastAndBrightnessFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getContrastAndBrightnessFilter_Contrast(), theEcorePackage.getEDouble(), "contrast", "1.0", 0, 1, ContrastAndBrightnessFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getContrastAndBrightnessFilter_Brightness(), theEcorePackage.getEDouble(), "brightness", "1.0", 0, 1, ContrastAndBrightnessFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(exposureFilterEClass, ExposureFilter.class, "ExposureFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExposureFilter_Exposure(), theEcorePackage.getEDouble(), "exposure", "2.5", 0, 1, ExposureFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gainFilterEClass, GainFilter.class, "GainFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGainFilter_Gain(), theEcorePackage.getEDouble(), "gain", "50", 0, 1, GainFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGainFilter_Bias(), theEcorePackage.getEDouble(), "bias", "50", 0, 1, GainFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(invertFilterEClass, InvertFilter.class, "InvertFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(rescaleFilterEClass, RescaleFilter.class, "RescaleFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRescaleFilter_Scale(), theEcorePackage.getEDouble(), "scale", "1.0", 0, 1, RescaleFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cameraImageAnnotationEClass, CameraImageAnnotation.class, "CameraImageAnnotation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCameraImageAnnotation_Visible(), theEcorePackage.getEBoolean(), "visible", "true", 0, 1, CameraImageAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getCameraImageAnnotation__Dispose(), null, "dispose", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(cameraOverlayListEClass, CameraOverlayList.class, "CameraOverlayList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCameraOverlayList_CameraViewConfiguration(), this.getCameraViewConfiguration(), this.getCameraViewConfiguration_OverlayList(), "cameraViewConfiguration", null, 0, 1, CameraOverlayList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCameraOverlayList_Overlays(), this.getCameraOverlay(), this.getCameraOverlay_CameraOverlayList(), "overlays", null, 0, -1, CameraOverlayList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cameraOverlayEClass, CameraOverlay.class, "CameraOverlay", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCameraOverlay_CameraOverlayList(), this.getCameraOverlayList(), this.getCameraOverlayList_Overlays(), "cameraOverlayList", null, 0, 1, CameraOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCameraOverlay_OverlayAlignment(), this.getOverlayAlignment(), "overlayAlignment", null, 0, 1, CameraOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(drawnCameraOverlayEClass, DrawnCameraOverlay.class, "DrawnCameraOverlay", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getDrawnCameraOverlay__ApplyOverlay__AbstractCamera_AbstractEImage_OverlayAlignment_int_int(), theApogyCommonImagesPackage.getAbstractEImage(), "applyOverlay", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyAddonsSensorsImagingPackage.getAbstractCamera(), "camera", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonImagesPackage.getAbstractEImage(), "cameraImage", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOverlayAlignment(), "overlayAlignment", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "overlayWidth", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "overlayHeight", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(abstractTextOverlayEClass, AbstractTextOverlay.class, "AbstractTextOverlay", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractTextOverlay_FontName(), theEcorePackage.getEString(), "fontName", "SansSerif", 0, 1, AbstractTextOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractTextOverlay_FontSize(), theEcorePackage.getEInt(), "fontSize", "10", 0, 1, AbstractTextOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractTextOverlay_HorizontalOffset(), theEcorePackage.getEInt(), "horizontalOffset", "10", 0, 1, AbstractTextOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractTextOverlay_VerticalOffset(), theEcorePackage.getEInt(), "verticalOffset", "10", 0, 1, AbstractTextOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractTextOverlay_TextColor(), this.getColor3f(), "textColor", "0.0,1.0,0.0", 0, 1, AbstractTextOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getAbstractTextOverlay__GetDisplayedString(), theEcorePackage.getEString(), "getDisplayedString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(emfFeatureOverlayEClass, EMFFeatureOverlay.class, "EMFFeatureOverlay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEMFFeatureOverlay_VariableFeatureReference(), theApogyCoreInvocatorPackage.getVariableFeatureReference(), null, "variableFeatureReference", null, 1, 1, EMFFeatureOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEMFFeatureOverlay_NumberFormat(), theEcorePackage.getEString(), "numberFormat", "0.00", 0, 1, EMFFeatureOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEMFFeatureOverlay_VariableFeatureReferenceChangeCount(), theEcorePackage.getELong(), "variableFeatureReferenceChangeCount", "0", 0, 1, EMFFeatureOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEMFFeatureOverlay_DisplayUnits(), theEcorePackage.getEString(), "displayUnits", null, 0, 1, EMFFeatureOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEMFFeatureOverlay_EnableRangeColoring(), theEcorePackage.getEBoolean(), "enableRangeColoring", "true", 0, 1, EMFFeatureOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cameraNameOverlayEClass, CameraNameOverlay.class, "CameraNameOverlay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(imageCountOverlayEClass, ImageCountOverlay.class, "ImageCountOverlay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImageCountOverlay_IndicatorVisible(), theEcorePackage.getEBoolean(), "indicatorVisible", "true", 0, 1, ImageCountOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImageCountOverlay_CountVisible(), theEcorePackage.getEBoolean(), "countVisible", "true", 0, 1, ImageCountOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(imageFrozenOverlayEClass, ImageFrozenOverlay.class, "ImageFrozenOverlay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImageFrozenOverlay_Frozen(), theEcorePackage.getEBoolean(), "frozen", "false", 0, 1, ImageFrozenOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImageFrozenOverlay_ExpectedImageUpdatePeriod(), theEcorePackage.getEDouble(), "expectedImageUpdatePeriod", "1.0", 0, 1, ImageFrozenOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImageFrozenOverlay_FrozenMessage(), theEcorePackage.getEString(), "frozenMessage", "Frozen", 0, 1, ImageFrozenOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(imageCameraOverlayEClass, ImageCameraOverlay.class, "ImageCameraOverlay", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getImageCameraOverlay__GetOverlay__AbstractCamera_OverlayAlignment_int_int(), theApogyCommonImagesPackage.getAbstractEImage(), "getOverlay", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyAddonsSensorsImagingPackage.getAbstractCamera(), "camera", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOverlayAlignment(), "overlayAlignment", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "overlayWidth", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "overlayHeight", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(urlImageOverlayEClass, URLImageOverlay.class, "URLImageOverlay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getURLImageOverlay_Url(), theEcorePackage.getEString(), "url", null, 0, 1, URLImageOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getURLImageOverlay_ImageSizePolicy(), this.getImageSizePolicy(), "imageSizePolicy", null, 0, 1, URLImageOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getURLImageOverlay_Image(), theApogyCommonImagesPackage.getAbstractEImage(), null, "image", null, 0, 1, URLImageOverlay.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(apogyLogoOverlayEClass, ApogyLogoOverlay.class, "ApogyLogoOverlay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fovOverlayEClass, FOVOverlay.class, "FOVOverlay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFOVOverlay_LineWidth(), theEcorePackage.getEInt(), "lineWidth", "2", 0, 1, FOVOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFOVOverlay_PositiveValuesColor(), this.getColor3f(), "positiveValuesColor", "0.0,1.0,0.0", 0, 1, FOVOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFOVOverlay_NegativeValueColor(), this.getColor3f(), "negativeValueColor", "1.0,0.0,0.0", 0, 1, FOVOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFOVOverlay_AngleInterval(), theEcorePackage.getEInt(), "angleInterval", "5", 0, 1, FOVOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFOVOverlay_FontName(), theEcorePackage.getEString(), "fontName", "SansSerif", 0, 1, FOVOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFOVOverlay_FontSize(), theEcorePackage.getEInt(), "fontSize", "10", 0, 1, FOVOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFOVOverlay_AzimuthDirection(), theApogyAddonsSensorsImagingPackage.getAzimuthDirection(), "azimuthDirection", null, 0, 1, FOVOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFOVOverlay_ElevationDirection(), theApogyAddonsSensorsImagingPackage.getElevationDirection(), "elevationDirection", null, 0, 1, FOVOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(azimuthElevationFOVOverlayEClass, AzimuthElevationFOVOverlay.class, "AzimuthElevationFOVOverlay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getAzimuthElevationFOVOverlay__ChangeAzimuth__double(), null, "changeAzimuth", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "azimuth", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAzimuthElevationFOVOverlay__ChangeElevation__double(), null, "changeElevation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "elevation", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(emfFeatureAzimuthElevationFOVOverlayEClass, EMFFeatureAzimuthElevationFOVOverlay.class, "EMFFeatureAzimuthElevationFOVOverlay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEMFFeatureAzimuthElevationFOVOverlay_AzimuthFeatureReference(), this.getAzimuthFeatureReference(), null, "azimuthFeatureReference", null, 1, 1, EMFFeatureAzimuthElevationFOVOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEMFFeatureAzimuthElevationFOVOverlay_ElevationFeatureReference(), this.getElevationFeatureReference(), null, "elevationFeatureReference", null, 1, 1, EMFFeatureAzimuthElevationFOVOverlay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(azimuthFeatureReferenceEClass, AzimuthFeatureReference.class, "AzimuthFeatureReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(elevationFeatureReferenceEClass, ElevationFeatureReference.class, "ElevationFeatureReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(toolTipTextProviderEClass, ToolTipTextProvider.class, "ToolTipTextProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getToolTipTextProvider__GetToolTipText__AbstractCamera_ImageSnapshot_int_int_int(), theEcorePackage.getEString(), "getToolTipText", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyAddonsSensorsImagingPackage.getAbstractCamera(), "camera", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyAddonsSensorsImagingPackage.getImageSnapshot(), "imageSnapshot", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "mouseButton", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "x", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "y", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(cameraToolEClass, CameraTool.class, "CameraTool", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCameraTool_CameraToolList(), this.getCameraToolList(), this.getCameraToolList_Tools(), "cameraToolList", null, 0, 1, CameraTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getCameraTool__InitializeCamera__AbstractCamera(), null, "initializeCamera", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyAddonsSensorsImagingPackage.getAbstractCamera(), "camera", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCameraTool__UpdateImageSnapshot__ImageSnapshot(), null, "updateImageSnapshot", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyAddonsSensorsImagingPackage.getImageSnapshot(), "imageSnapshot", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getCameraTool__Dispose(), null, "dispose", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCameraTool__MouseMoved__AbstractEImage_int_int_int(), null, "mouseMoved", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonImagesPackage.getAbstractEImage(), "cameraImage", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "mouseButton", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "x", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "y", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCameraTool__PositionSelected__AbstractEImage_int_int_int(), null, "positionSelected", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonImagesPackage.getAbstractEImage(), "cameraImage", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "mouseButton", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "x", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "y", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(cameraToolListEClass, CameraToolList.class, "CameraToolList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCameraToolList_CameraViewConfiguration(), this.getCameraViewConfiguration(), this.getCameraViewConfiguration_ToolList(), "cameraViewConfiguration", null, 0, 1, CameraToolList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCameraToolList_Tools(), this.getCameraTool(), this.getCameraTool_CameraToolList(), "tools", null, 0, -1, CameraToolList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pointerCameraToolEClass, PointerCameraTool.class, "PointerCameraTool", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPointerCameraTool_VectorColor(), this.getColor3f(), "vectorColor", "0.0,1.0,0.0", 0, 1, PointerCameraTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPointerCameraTool_IntersectionDistance(), theEcorePackage.getEDouble(), "intersectionDistance", "0.0", 0, 1, PointerCameraTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ptzCameraToolEClass, PTZCameraTool.class, "PTZCameraTool", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPTZCameraTool_SelectionBoxColor(), this.getColor3f(), "selectionBoxColor", "0.0,0.0,1.0", 0, 1, PTZCameraTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPTZCameraTool_UserSelectionCorner0(), this.getPoint2d(), "userSelectionCorner0", null, 0, 1, PTZCameraTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPTZCameraTool_UserSelectionCorner1(), this.getPoint2d(), "userSelectionCorner1", null, 0, 1, PTZCameraTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getPTZCameraTool__ClearUserSelection(), null, "clearUserSelection", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getPTZCameraTool__CommandPTZ__double_double_double_double(), null, "commandPTZ", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "panIncrement", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "tiltIncrement", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "newHorizontalFOV", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "newVerticalFOV", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(cameraViewConfigurationPagesProviderEClass, CameraViewConfigurationPagesProvider.class, "CameraViewConfigurationPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractTextOverlayOverlayPagesProviderEClass, AbstractTextOverlayOverlayPagesProvider.class, "AbstractTextOverlayOverlayPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cameraNameOverlayPagesProviderEClass, CameraNameOverlayPagesProvider.class, "CameraNameOverlayPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(imageFrozenOverlayPagesProviderEClass, ImageFrozenOverlayPagesProvider.class, "ImageFrozenOverlayPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(imageCountOverlayOverlayPagesProviderEClass, ImageCountOverlayOverlayPagesProvider.class, "ImageCountOverlayOverlayPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(emfFeatureOverlayPagesProviderEClass, EMFFeatureOverlayPagesProvider.class, "EMFFeatureOverlayPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(urlImageOverlayPagesProviderEClass, URLImageOverlayPagesProvider.class, "URLImageOverlayPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fovOverlayPagesProviderEClass, FOVOverlayPagesProvider.class, "FOVOverlayPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pointerCameraToolPagesProviderEClass, PointerCameraToolPagesProvider.class, "PointerCameraToolPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ptzCameraToolPagesProviderEClass, PTZCameraToolPagesProvider.class, "PTZCameraToolPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(grayScaleFilterPagesProviderEClass, GrayScaleFilterPagesProvider.class, "GrayScaleFilterPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(edgeFilterPagesProviderEClass, EdgeFilterPagesProvider.class, "EdgeFilterPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(invertFilterPagesProviderEClass, InvertFilterPagesProvider.class, "InvertFilterPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(contrastAndBrightnessFilterPagesProviderEClass, ContrastAndBrightnessFilterPagesProvider.class, "ContrastAndBrightnessFilterPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(gainFilterPagesProviderEClass, GainFilterPagesProvider.class, "GainFilterPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(exposureFilterPagesProviderEClass, ExposureFilterPagesProvider.class, "ExposureFilterPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(rescaleFilterPagesProviderEClass, RescaleFilterPagesProvider.class, "RescaleFilterPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(overlayAlignmentEEnum, OverlayAlignment.class, "OverlayAlignment");
		addEEnumLiteral(overlayAlignmentEEnum, OverlayAlignment.CENTER);
		addEEnumLiteral(overlayAlignmentEEnum, OverlayAlignment.LOWER_LEFT_CORNER);
		addEEnumLiteral(overlayAlignmentEEnum, OverlayAlignment.UPPER_LEFT_CORNER);
		addEEnumLiteral(overlayAlignmentEEnum, OverlayAlignment.LOWER_RIGHT_CORNER);
		addEEnumLiteral(overlayAlignmentEEnum, OverlayAlignment.UPPER_RIGHT_CORNER);

		initEEnum(cameraViewDisplayRotationEEnum, CameraViewDisplayRotation.class, "CameraViewDisplayRotation");
		addEEnumLiteral(cameraViewDisplayRotationEEnum, CameraViewDisplayRotation.NONE);
		addEEnumLiteral(cameraViewDisplayRotationEEnum, CameraViewDisplayRotation.CW_90_DEG);
		addEEnumLiteral(cameraViewDisplayRotationEEnum, CameraViewDisplayRotation.CCW_90_DEG);
		addEEnumLiteral(cameraViewDisplayRotationEEnum, CameraViewDisplayRotation.CW_180_DEG);

		initEEnum(imageSizePolicyEEnum, ImageSizePolicy.class, "ImageSizePolicy");
		addEEnumLiteral(imageSizePolicyEEnum, ImageSizePolicy.FIXED_SIZE);
		addEEnumLiteral(imageSizePolicyEEnum, ImageSizePolicy.ALLOW_RESIZE_VERTICAL);
		addEEnumLiteral(imageSizePolicyEEnum, ImageSizePolicy.ALLOW_RESIZE_HORIZONTAL);
		addEEnumLiteral(imageSizePolicyEEnum, ImageSizePolicy.ALLOW_RESIZE_BOTH);

		// Initialize data types
		initEDataType(color3fEDataType, Color3f.class, "Color3f", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(listEDataType, List.class, "List", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(point2dEDataType, Point2d.class, "Point2d", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

		/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "prefix", "ApogyAddonsSensorsImagingCamera",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyAddonsSensorsImagingCamera",
			 "complianceLevel", "6.0",
			 "suppressGenModelAnnotations", "false",
			 "dynamicTemplates", "true",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.addons.sensors.imaging.camera/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.addons.sensors.imaging"
		   });	
		addAnnotation
		  (overlayAlignmentEEnum, 
		   source, 
		   new String[] {
			 "documentation", "*\nOverlay alignment position."
		   });	
		addAnnotation
		  (cameraViewUtilitiesEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nUtilities class for camera views."
		   });	
		addAnnotation
		  (getCameraViewUtilities__GetCameraViewConfigurationIdentifier__CameraViewConfiguration(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturn the identifier associated with a given CameraViewConfiguration.\n@param cameraViewConfiguration The given CameraViewConfiguration.\n@return The identifier, null if none is found."
		   });	
		addAnnotation
		  (getCameraViewUtilities__GetActiveCameraViewConfiguration__String(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturn the CameraViewConfiguration (in the Active Session) with the specified identifier.\n@param identifier The CameraViewConfiguration identifier.\n@return The CameraViewConfiguration with the specified identifier, null if non is found."
		   });	
		addAnnotation
		  (getCameraViewUtilities__GetActiveCameraViewConfigurationList(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturn the CameraViewConfigurationList in the Active Session.\n@return The CameraViewConfigurationList in the Active Session, null if none is found."
		   });	
		addAnnotation
		  (getCameraViewUtilities__AddCameraTool__CameraViewConfiguration_CameraTool(), 
		   source, 
		   new String[] {
			 "documentation", "*\nAdds a Camera Tool from a given Camera View Configuration.\n@param cameraViewConfiguration The CameraViewConfiguration to which to add the CameraTool.\n@param cameraTool The camera Tool to add."
		   });	
		addAnnotation
		  (getCameraViewUtilities__RemoveCameraTool__CameraViewConfiguration_CameraTool(), 
		   source, 
		   new String[] {
			 "documentation", "*\nRemoves cleanly a Camera Tool from a given Camera View Configuration.\n@param cameraViewConfiguration The CameraViewConfiguration from which to remove the CameraTool.\n@param cameraTool The camera Tool to remove."
		   });	
		addAnnotation
		  (cameraViewConfigurationListEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA list of CameraViewConfigurations."
		   });	
		addAnnotation
		  (getCameraViewConfigurationList_CameraViewConfigurations(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of CameraViewConfiguration.",
			 "property", "None"
		   });	
		addAnnotation
		  (cameraViewDisplayRotationEEnum, 
		   source, 
		   new String[] {
			 "documentation", "*\nThe image rotations available."
		   });	
		addAnnotation
		  (cameraViewConfigurationEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nDefines a configuration of to be used when displaying a camera imagery. This includes the camera, overlays, filters and tools."
		   });	
		addAnnotation
		  (getCameraViewConfiguration__GetCameraImageAnnotations(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns the list of all CameraImageAnnotation.\n@return The list of CameraImageAnnotation."
		   });	
		addAnnotation
		  (getCameraViewConfiguration_CameraViewDisplayRotation(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe display rotation to use."
		   });	
		addAnnotation
		  (getCameraViewConfiguration_CameraViewConfigurationList(), 
		   source, 
		   new String[] {
			 "documentation", "*\n The  CameraViewConfiguration containing this CameraViewConfiguration.",
			 "property", "None"
		   });	
		addAnnotation
		  (getCameraViewConfiguration_Camera(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe Camera being displayed.",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (getCameraViewConfiguration_ImageWidth(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWidth of the image being displayed, in pixels.",
			 "apogy_units", "pixel",
			 "propertyCategory", "IMAGE_DISPLAY_SETTINGS"
		   });	
		addAnnotation
		  (getCameraViewConfiguration_ImageHeight(), 
		   source, 
		   new String[] {
			 "documentation", "*\nHeight of the image being displayed, in pixels.",
			 "apogy_units", "pixel",
			 "propertyCategory", "IMAGE_DISPLAY_SETTINGS"
		   });	
		addAnnotation
		  (getCameraViewConfiguration_ImageCount(), 
		   source, 
		   new String[] {
			 "documentation", "*\nNumber of image received."
		   });	
		addAnnotation
		  (getCameraViewConfiguration_DisplayRectifiedImage(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not to display the rectified image is available.",
			 "propertyCategory", "IMAGE_DISPLAY_SETTINGS"
		   });	
		addAnnotation
		  (getCameraViewConfiguration_OverlayList(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of overlays shown onto the camera image."
		   });	
		addAnnotation
		  (getCameraViewConfiguration_ToolList(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of tools used with the camera."
		   });	
		addAnnotation
		  (getCameraViewConfiguration_FilterList(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of image filters to apply to the camera image."
		   });	
		addAnnotation
		  (getCameraViewConfiguration_ImageAutoSaveEnable(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not to automatically save the image received.",
			 "propertyCategory", "IMAGE_AUTOSAVE_SETTINGS"
		   });	
		addAnnotation
		  (getCameraViewConfiguration_SaveImageWithOverlays(), 
		   source, 
		   new String[] {
			 "documentation", "*\nIf auto saving image, whether or not to save the image with overlays.",
			 "propertyCategory", "IMAGE_AUTOSAVE_SETTINGS"
		   });	
		addAnnotation
		  (getCameraViewConfiguration_ImageAutoSaveFolderPath(), 
		   source, 
		   new String[] {
			 "documentation", "*\nPath of the folder where to save images.",
			 "propertyCategory", "IMAGE_AUTOSAVE_SETTINGS"
		   });	
		addAnnotation
		  (cameraViewConfigurationReferenceEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nClass implementing a reference to a CameraViewConfiguration."
		   });	
		addAnnotation
		  (getCameraViewConfigurationReference_CameraViewConfiguration(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe CameraViewConfiguration refered to."
		   });	
		addAnnotation
		  (filterListEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\n\nFilters.\n\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (getFilterList_CameraViewConfiguration(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe CameraViewConfiguration containing this FilterList.",
			 "property", "None"
		   });	
		addAnnotation
		  (getFilterList_ImageFilters(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of image filters."
		   });	
		addAnnotation
		  (imageFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBase class for all image filters."
		   });	
		addAnnotation
		  (getImageFilter__Filter__AbstractCamera_AbstractEImage(), 
		   source, 
		   new String[] {
			 "documentation", "*\nCreates a filtered image using an image from a specified camera.\n@param camera The camera that produced the image.\n@param cameraImage The image to filter.\n@return The filtered image."
		   });	
		addAnnotation
		  (getImageFilter__Dispose(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called to dispose of the ImageFilter. This method should be overridden by sub-classes\nto release resources, unregister listeners, etc."
		   });	
		addAnnotation
		  (getImageFilter_Enabled(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not the filter is enabled.",
			 "propertyCategory", "FILTER_SETTING"
		   });	
		addAnnotation
		  (grayScaleFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn Image Filter which converts an image to grayscale using the NTSC brightness calculation."
		   });	
		addAnnotation
		  (edgeFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn image Filter that implements edge-detection."
		   });	
		addAnnotation
		  (contrastAndBrightnessFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn Image Filter to change the brightness and contrast of an image."
		   });	
		addAnnotation
		  (getContrastAndBrightnessFilter_Contrast(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe contrast setting.",
			 "propertyCategory", "FILTER_SETTING"
		   });	
		addAnnotation
		  (getContrastAndBrightnessFilter_Brightness(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe brightness setting.",
			 "propertyCategory", "FILTER_SETTING"
		   });	
		addAnnotation
		  (exposureFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn Image Filter which changes the exposure of an image."
		   });	
		addAnnotation
		  (getExposureFilter_Exposure(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe exposure setting.",
			 "propertyCategory", "FILTER_SETTING"
		   });	
		addAnnotation
		  (gainFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn Image Filter which changes the gain and bias of an image."
		   });	
		addAnnotation
		  (getGainFilter_Gain(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe gain setting.",
			 "propertyCategory", "FILTER_SETTING"
		   });	
		addAnnotation
		  (getGainFilter_Bias(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe bias setting.",
			 "propertyCategory", "FILTER_SETTING"
		   });	
		addAnnotation
		  (invertFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn Image Filter which inverts the RGB channels of an image."
		   });	
		addAnnotation
		  (rescaleFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn Image Filter which simply multiplies pixel values by a given scale factor."
		   });	
		addAnnotation
		  (getRescaleFilter_Scale(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe scalling setting.",
			 "propertyCategory", "FILTER_SETTING"
		   });	
		addAnnotation
		  (cameraImageAnnotationEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBase class for annotation over images."
		   });	
		addAnnotation
		  (getCameraImageAnnotation__Dispose(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called to dispose of the CameraImageAnnotation. This method should be overridden by sub-classes\nto release resources, unregister listeners, etc."
		   });	
		addAnnotation
		  (getCameraImageAnnotation_Visible(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not the camera image annotation is visible."
		   });	
		addAnnotation
		  (cameraOverlayListEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\n\nOverlays.\n\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (getCameraOverlayList_CameraViewConfiguration(), 
		   source, 
		   new String[] {
			 "documentation", "*\n The  CameraViewConfiguration containing this CameraOverlayList.",
			 "property", "None"
		   });	
		addAnnotation
		  (getCameraOverlayList_Overlays(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of overlay shown onto the camera image."
		   });	
		addAnnotation
		  (cameraOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay displayed on top of a camera image."
		   });	
		addAnnotation
		  (getCameraOverlay_CameraOverlayList(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe  CameraOverlayList containing this CameraOverlay.",
			 "property", "None"
		   });	
		addAnnotation
		  (getCameraOverlay_OverlayAlignment(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhere the overlay should be shown.",
			 "propertyCategory", "OVERLAY_POSITION_PROPERTIES"
		   });	
		addAnnotation
		  (drawnCameraOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay that has to be drawn onto a camera image.\nSuch overlays are responsible for drawing their representation onto the provided image."
		   });	
		addAnnotation
		  (getDrawnCameraOverlay__ApplyOverlay__AbstractCamera_AbstractEImage_OverlayAlignment_int_int(), 
		   source, 
		   new String[] {
			 "documentation", "*\nDraws the overlay onto the image provided by the camera.\n@param camera The camera that provided the image.\n@param cameraImage The image onto which the overlay must be drawn.\n@param overlayAlignment The alignment of the overlay\n@param overlayWidth The width of the overlay.\n@param overlayHeight The height of the overlay.\n@return The camera image with the overlay drawn on top of it."
		   });	
		addAnnotation
		  (abstractTextOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay that displays text onto a camera image."
		   });	
		addAnnotation
		  (getAbstractTextOverlay__GetDisplayedString(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns the string that will be displayed in the screen.\nSub-classes should overload this method.\n@return The string to display."
		   });	
		addAnnotation
		  (getAbstractTextOverlay_FontName(), 
		   source, 
		   new String[] {
			 "documentation", "*\nName of the font to use.",
			 "propertyCategory", "FONT_PROPERTIES"
		   });	
		addAnnotation
		  (getAbstractTextOverlay_FontSize(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe font size.",
			 "propertyCategory", "FONT_PROPERTIES"
		   });	
		addAnnotation
		  (getAbstractTextOverlay_HorizontalOffset(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe minimum distance of text to image left or right edge, in pixel.",
			 "apogy_units", "pixel",
			 "propertyCategory", "OVERLAY_POSITION_PROPERTIES"
		   });	
		addAnnotation
		  (getAbstractTextOverlay_VerticalOffset(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe minimum distance of text to image upper or lower edge, in pixel.",
			 "apogy_units", "pixel",
			 "propertyCategory", "OVERLAY_POSITION_PROPERTIES"
		   });	
		addAnnotation
		  (getAbstractTextOverlay_TextColor(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe text color.",
			 "propertyCategory", "FONT_PROPERTIES"
		   });	
		addAnnotation
		  (emfFeatureOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay that displays the value of an Variable feature as text onto an image."
		   });	
		addAnnotation
		  (getEMFFeatureOverlay_VariableFeatureReference(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe VariableFeatureReference pointing to the value to display.",
			 "property", "None"
		   });	
		addAnnotation
		  (getEMFFeatureOverlay_NumberFormat(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe number format to use when displaying a Number.",
			 "propertyCategory", "FONT_PROPERTIES"
		   });	
		addAnnotation
		  (getEMFFeatureOverlay_VariableFeatureReferenceChangeCount(), 
		   source, 
		   new String[] {
			 "documentation", "*\n Attribute used to trigger an overlay update when changes in the VariableFeatureReference are made.",
			 "property", "None"
		   });	
		addAnnotation
		  (getEMFFeatureOverlay_DisplayUnits(), 
		   source, 
		   new String[] {
			 "documentation", "*\nUnits to be used for display."
		   });	
		addAnnotation
		  (getEMFFeatureOverlay_EnableRangeColoring(), 
		   source, 
		   new String[] {
			 "documentation", "*\nEnables the range checking of the value and changes the background color of the text to reflect the range."
		   });	
		addAnnotation
		  (cameraNameOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay used to display the name of the camera."
		   });	
		addAnnotation
		  (imageCountOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay used to display the number of images."
		   });	
		addAnnotation
		  (getImageCountOverlay_IndicatorVisible(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not the indicator should be shown."
		   });	
		addAnnotation
		  (getImageCountOverlay_CountVisible(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not the image count should be shown."
		   });	
		addAnnotation
		  (imageFrozenOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay that displays a message when a specific period without a new image is exceeded."
		   });	
		addAnnotation
		  (getImageFrozenOverlay_Frozen(), 
		   source, 
		   new String[] {
			 "documentation", "*\nIndicates whether or not the image is frozen.",
			 "propertyCategory", "FROZEN_PROPERTIES",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (getImageFrozenOverlay_ExpectedImageUpdatePeriod(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe expected image update period, in seconds.",
			 "propertyCategory", "FROZEN_PROPERTIES",
			 "apogy_units", "s"
		   });	
		addAnnotation
		  (getImageFrozenOverlay_FrozenMessage(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe message to display when the image freezes.",
			 "propertyCategory", "FROZEN_PROPERTIES"
		   });	
		addAnnotation
		  (imageCameraOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "An overlay that produces a transparent image that will be superimposed\non the camera image by the CameraView."
		   });	
		addAnnotation
		  (getImageCameraOverlay__GetOverlay__AbstractCamera_OverlayAlignment_int_int(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns the overlay image to superimpose on the camera image.\n@param overlayWidth The width of the overlay.\n@param overlayHeight The height of the overlay.\n@return The image (with transparent background) to be superimposed on the camera image."
		   });	
		addAnnotation
		  (urlImageOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay that put an image found at a specified URL onto an image."
		   });	
		addAnnotation
		  (getURLImageOverlay_Url(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe URL to the file containing the image to overlay."
		   });	
		addAnnotation
		  (getURLImageOverlay_ImageSizePolicy(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe image resize policy."
		   });	
		addAnnotation
		  (getURLImageOverlay_Image(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe Image read from the url.",
			 "child", "true",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (imageSizePolicyEEnum, 
		   source, 
		   new String[] {
			 "documentation", "*\nImage sizing policy."
		   });	
		addAnnotation
		  (apogyLogoOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay that displays the Apogy Logo."
		   });	
		addAnnotation
		  (fovOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay that displays a graduated cross-hair representing the horizontal and vertical Field Of View (FOV) of the camera."
		   });	
		addAnnotation
		  (getFOVOverlay_LineWidth(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWidth of lines displayed, in pixels.",
			 "apogy_units", "pixel",
			 "propertyCategory", "OVERLAY_PROPERTIES"
		   });	
		addAnnotation
		  (getFOVOverlay_PositiveValuesColor(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe color to be used for drawing positive values.",
			 "propertyCategory", "OVERLAY_PROPERTIES"
		   });	
		addAnnotation
		  (getFOVOverlay_NegativeValueColor(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe color to be used for drawing positive values.",
			 "propertyCategory", "OVERLAY_PROPERTIES"
		   });	
		addAnnotation
		  (getFOVOverlay_AngleInterval(), 
		   source, 
		   new String[] {
			 "documentation", "The interval, in degrees, for which to display numerics.",
			 "propertyCategory", "OVERLAY_PROPERTIES",
			 "apogy_units", "deg"
		   });	
		addAnnotation
		  (getFOVOverlay_FontName(), 
		   source, 
		   new String[] {
			 "documentation", "*\n The name of the font to use.",
			 "propertyCategory", "FONT_PROPERTIES"
		   });	
		addAnnotation
		  (getFOVOverlay_FontSize(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe font size.",
			 "propertyCategory", "FONT_PROPERTIES"
		   });	
		addAnnotation
		  (getFOVOverlay_AzimuthDirection(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe direction of the positive azimuth (i.e. is left or right the positive direction).",
			 "propertyCategory", "DIRECTIONS_PROPERTIES"
		   });	
		addAnnotation
		  (getFOVOverlay_ElevationDirection(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe direction of the positive elevation (i.e. is up or down the positive direction).",
			 "propertyCategory", "DIRECTIONS_PROPERTIES"
		   });	
		addAnnotation
		  (azimuthElevationFOVOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay that displays a graduated cross-hair representing absolute horizontal and vertical orientation."
		   });	
		addAnnotation
		  (getAzimuthElevationFOVOverlay__ChangeAzimuth__double(), 
		   source, 
		   new String[] {
			 "documentation", "*\nChange the azimuth of the center of the image.\n@param azimuth The azimuth of the center of the image, in radians."
		   });	
		addAnnotation
		  ((getAzimuthElevationFOVOverlay__ChangeAzimuth__double()).getEParameters().get(0), 
		   source, 
		   new String[] {
			 "apogy_units", "rad"
		   });	
		addAnnotation
		  (getAzimuthElevationFOVOverlay__ChangeElevation__double(), 
		   source, 
		   new String[] {
			 "documentation", "*\nChange the elevation of the center of the image.\n@param elevation The elevation of the center of the image, in radians."
		   });	
		addAnnotation
		  ((getAzimuthElevationFOVOverlay__ChangeElevation__double()).getEParameters().get(0), 
		   source, 
		   new String[] {
			 "apogy_units", "rad"
		   });	
		addAnnotation
		  (emfFeatureAzimuthElevationFOVOverlayEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay that displays a graduated cross-hair representing absolute horizontal and vertical orientation.\nThe values of azimuth and elevation of the center of the cross-hair are driven by the specified VariableFeatureReference."
		   });	
		addAnnotation
		  (getEMFFeatureAzimuthElevationFOVOverlay_AzimuthFeatureReference(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe reference to use for azimuth.",
			 "property", "None",
			 "propertyCategory", "FEATURE_CATEGORY"
		   });	
		addAnnotation
		  (getEMFFeatureAzimuthElevationFOVOverlay_ElevationFeatureReference(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe reference to use for elevation.",
			 "property", "None",
			 "propertyCategory", "FEATURE_CATEGORY"
		   });	
		addAnnotation
		  (azimuthFeatureReferenceEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA specialization of VariableFeatureReference for absolute azimuth reference."
		   });	
		addAnnotation
		  (elevationFeatureReferenceEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA specialization of VariableFeatureReference for absolute elevation reference."
		   });	
		addAnnotation
		  (toolTipTextProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA provider of tooltip text to be displayed on top of an image."
		   });	
		addAnnotation
		  (getToolTipTextProvider__GetToolTipText__AbstractCamera_ImageSnapshot_int_int_int(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns the text to display as the result of the user clicking on the image.\n@param camera The camera that produced the image.\n@param imageSnapshot The image snaphot displayed.\n@param mouseButton The mouse button clicked.\n@param x The position (from left to right) of the click of the mouse, in pixels.\n@param y The position (from up to down) of the click of the mouse, in pixels.\n@return The string to display, can be null."
		   });	
		addAnnotation
		  ((getToolTipTextProvider__GetToolTipText__AbstractCamera_ImageSnapshot_int_int_int()).getEParameters().get(3), 
		   source, 
		   new String[] {
			 "apogy_units", "pixel"
		   });	
		addAnnotation
		  ((getToolTipTextProvider__GetToolTipText__AbstractCamera_ImageSnapshot_int_int_int()).getEParameters().get(4), 
		   source, 
		   new String[] {
			 "apogy_units", "pixel"
		   });	
		addAnnotation
		  (cameraToolEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBase class for camera tools."
		   });	
		addAnnotation
		  (getCameraTool__InitializeCamera__AbstractCamera(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called when the camera is resolved by the CameraViewConfiguration.\n@param camera The AbstractCamera resolved, can be null."
		   });	
		addAnnotation
		  (getCameraTool__UpdateImageSnapshot__ImageSnapshot(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called when a new ImageSnaphot is received.\n@param imageSnapshot The new image snapshot, can be null."
		   });	
		addAnnotation
		  (getCameraTool__Dispose(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called when the tool is no longer needed."
		   });	
		addAnnotation
		  (getCameraTool__MouseMoved__AbstractEImage_int_int_int(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called when the mouse is moved inside the camera image.\n@param cameraImage The current image being displayed.\n@param x The absolute position x coordinates of the mouse.\n@param y The absolute position y coordinates of the mouse."
		   });	
		addAnnotation
		  ((getCameraTool__MouseMoved__AbstractEImage_int_int_int()).getEParameters().get(2), 
		   source, 
		   new String[] {
			 "apogy_units", "pixel"
		   });	
		addAnnotation
		  ((getCameraTool__MouseMoved__AbstractEImage_int_int_int()).getEParameters().get(3), 
		   source, 
		   new String[] {
			 "apogy_units", "pixel"
		   });	
		addAnnotation
		  (getCameraTool__PositionSelected__AbstractEImage_int_int_int(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called when the user clicks on the image with the mouse.\n@param cameraImage The current image being displayed.\n@param mouseButton The mouse button clicked.\n@param x The absolute position x coordinates of the pixel selected.\n@param y The absolute position y coordinates of the pixel selected."
		   });	
		addAnnotation
		  ((getCameraTool__PositionSelected__AbstractEImage_int_int_int()).getEParameters().get(2), 
		   source, 
		   new String[] {
			 "apogy_units", "pixel"
		   });	
		addAnnotation
		  ((getCameraTool__PositionSelected__AbstractEImage_int_int_int()).getEParameters().get(3), 
		   source, 
		   new String[] {
			 "apogy_units", "pixel"
		   });	
		addAnnotation
		  (getCameraTool_CameraToolList(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe CameraToolList containing this tool.",
			 "property", "None"
		   });	
		addAnnotation
		  (cameraToolListEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA list of CameraTool."
		   });	
		addAnnotation
		  (getCameraToolList_CameraViewConfiguration(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe CameraViewConfiguration containing this CameraToolList.",
			 "property", "None"
		   });	
		addAnnotation
		  (getCameraToolList_Tools(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of overlay shown onto the camera image."
		   });	
		addAnnotation
		  (pointerCameraToolEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA tool that projects a ray going from the FOV origin through the point selected on the Camera View Image. The ray is made visible as a line in the 3D topology."
		   });	
		addAnnotation
		  (getPointerCameraTool_VectorColor(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe color of the vector.",
			 "propertyCategory", "TOOL_PROPERTIES"
		   });	
		addAnnotation
		  (getPointerCameraTool_IntersectionDistance(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe current intersection distance.",
			 "notify", "true",
			 "property", "Readonly",
			 "propertyCategory", "TOOL_PROPERTIES",
			 "sca_units", "m"
		   });	
		addAnnotation
		  (ptzCameraToolEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn overlay that displays a graduated cross-hair representing the horizontal and vertical Field Of View (FOV) of the\ncamera and allows the user to specified an area in the image for the camera to pan + tilt + zoom to."
		   });	
		addAnnotation
		  (getPTZCameraTool__ClearUserSelection(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod that clears the current user selection."
		   });	
		addAnnotation
		  (getPTZCameraTool__CommandPTZ__double_double_double_double(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called when the user completes it selection in the image. This method should be overriden by sub classes.\n@param panIncrement The pan increment to center the camera to the user selected area.\n@param tiltIncrement The tilt increment to center the camera to the user selected area.\n@param newHorizontalFOV The horizontal FOV angle represented by the user selected area.\n@param newVerticalFOV The vertical FOV angle represented by the user selected area."
		   });	
		addAnnotation
		  ((getPTZCameraTool__CommandPTZ__double_double_double_double()).getEParameters().get(0), 
		   source, 
		   new String[] {
			 "apogy_units", "rad"
		   });	
		addAnnotation
		  ((getPTZCameraTool__CommandPTZ__double_double_double_double()).getEParameters().get(1), 
		   source, 
		   new String[] {
			 "apogy_units", "rad"
		   });	
		addAnnotation
		  ((getPTZCameraTool__CommandPTZ__double_double_double_double()).getEParameters().get(2), 
		   source, 
		   new String[] {
			 "apogy_units", "rad"
		   });	
		addAnnotation
		  ((getPTZCameraTool__CommandPTZ__double_double_double_double()).getEParameters().get(3), 
		   source, 
		   new String[] {
			 "apogy_units", "rad"
		   });	
		addAnnotation
		  (getPTZCameraTool_SelectionBoxColor(), 
		   source, 
		   new String[] {
			 "documentation", "*\nColor to be used to represent the user selection.",
			 "propertyCategory", "OVERLAY_PROPERTIES"
		   });	
		addAnnotation
		  (getPTZCameraTool_UserSelectionCorner0(), 
		   source, 
		   new String[] {
			 "documentation", "*\nFirst corner of the user selection. This represent the azimuth, elevation of the corner, using the FOV coordinates defined in the overlay."
		   });	
		addAnnotation
		  (getPTZCameraTool_UserSelectionCorner1(), 
		   source, 
		   new String[] {
			 "documentation", "*\nSecond corner of the user selection. This represent the azimuth, elevation of the corner, using the FOV coordinates defined in the overlay."
		   });	
		addAnnotation
		  (cameraViewConfigurationPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard"
		   });	
		addAnnotation
		  (abstractTextOverlayOverlayPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for AbstractTextOverlay"
		   });	
		addAnnotation
		  (cameraNameOverlayPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for CameraNameOverlay."
		   });	
		addAnnotation
		  (imageFrozenOverlayPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for ImageFrozenOverlay."
		   });	
		addAnnotation
		  (imageCountOverlayOverlayPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for ImageCountOverlay."
		   });	
		addAnnotation
		  (emfFeatureOverlayPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for EMFFeatureOverlay."
		   });	
		addAnnotation
		  (urlImageOverlayPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for URLImageOverlay."
		   });	
		addAnnotation
		  (fovOverlayPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for FOVOverlay."
		   });	
		addAnnotation
		  (pointerCameraToolPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for PointerCameraTool."
		   });	
		addAnnotation
		  (ptzCameraToolPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for FOVOverlay."
		   });	
		addAnnotation
		  (grayScaleFilterPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for GrayScaleFilter."
		   });	
		addAnnotation
		  (edgeFilterPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for EdgeFilter."
		   });	
		addAnnotation
		  (invertFilterPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for InvertFilter."
		   });	
		addAnnotation
		  (contrastAndBrightnessFilterPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for ContrastAndBrightnessFilter."
		   });	
		addAnnotation
		  (gainFilterPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for GainFilter."
		   });	
		addAnnotation
		  (exposureFilterPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for GainFilter."
		   });	
		addAnnotation
		  (rescaleFilterPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for RescaleFilter"
		   });
	}

} //ApogyAddonsSensorsImagingCameraPackageImpl

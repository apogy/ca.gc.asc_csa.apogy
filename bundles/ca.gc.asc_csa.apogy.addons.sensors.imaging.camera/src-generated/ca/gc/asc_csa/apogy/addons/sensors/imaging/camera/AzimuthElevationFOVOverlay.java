package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Azimuth Elevation FOV Overlay</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * An overlay that displays a graduated cross-hair representing absolute horizontal and vertical orientation.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage#getAzimuthElevationFOVOverlay()
 * @model
 * @generated
 */
public interface AzimuthElevationFOVOverlay extends FOVOverlay {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Change the azimuth of the center of the image.
	 * @param azimuth The azimuth of the center of the image, in radians.
	 * <!-- end-model-doc -->
	 * @model azimuthUnique="false"
	 *        azimuthAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'"
	 * @generated
	 */
	void changeAzimuth(double azimuth);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Change the elevation of the center of the image.
	 * @param elevation The elevation of the center of the image, in radians.
	 * <!-- end-model-doc -->
	 * @model elevationUnique="false"
	 *        elevationAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'"
	 * @generated
	 */
	void changeElevation(double elevation);

} // AzimuthElevationFOVOverlay

package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.util;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import ca.gc.asc_csa.apogy.addons.AbstractTool;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.*;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.AbstractTextOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.AbstractTextOverlayOverlayPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyLogoOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.AzimuthElevationFOVOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.AzimuthFeatureReference;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraImageAnnotation;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraNameOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraOverlayList;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraTool;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraToolList;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfiguration;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfigurationList;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfigurationPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfigurationReference;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewUtilities;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ContrastAndBrightnessFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.DrawnCameraOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EMFFeatureAzimuthElevationFOVOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EMFFeatureOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EMFFeatureOverlayPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EdgeFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ElevationFeatureReference;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ExposureFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.FOVOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.FilterList;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.GainFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.GrayScaleFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageCameraOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageCountOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageCountOverlayOverlayPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageFrozenOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.InvertFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PointerCameraTool;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PointerCameraToolPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.RescaleFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ToolTipTextProvider;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.URLImageOverlay;
import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.core.AbsolutePoseProvider;
import ca.gc.asc_csa.apogy.core.PoseProvider;
import ca.gc.asc_csa.apogy.core.invocator.AbstractToolsListContainer;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage
 * @generated
 */
public class ApogyAddonsSensorsImagingCameraSwitch<T> extends Switch<T>
{
  /**
	 * The cached model package
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected static ApogyAddonsSensorsImagingCameraPackage modelPackage;

  /**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public ApogyAddonsSensorsImagingCameraSwitch()
  {
		if (modelPackage == null) {
			modelPackage = ApogyAddonsSensorsImagingCameraPackage.eINSTANCE;
		}
	}

  /**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
		return ePackage == modelPackage;
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
		switch (classifierID) {
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_VIEW_UTILITIES: {
				CameraViewUtilities cameraViewUtilities = (CameraViewUtilities)theEObject;
				T result = caseCameraViewUtilities(cameraViewUtilities);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_VIEW_CONFIGURATION_LIST: {
				CameraViewConfigurationList cameraViewConfigurationList = (CameraViewConfigurationList)theEObject;
				T result = caseCameraViewConfigurationList(cameraViewConfigurationList);
				if (result == null) result = caseAbstractToolsListContainer(cameraViewConfigurationList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_VIEW_CONFIGURATION: {
				CameraViewConfiguration cameraViewConfiguration = (CameraViewConfiguration)theEObject;
				T result = caseCameraViewConfiguration(cameraViewConfiguration);
				if (result == null) result = caseVariableFeatureReference(cameraViewConfiguration);
				if (result == null) result = caseAbstractTool(cameraViewConfiguration);
				if (result == null) result = caseNamed(cameraViewConfiguration);
				if (result == null) result = caseDescribed(cameraViewConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_VIEW_CONFIGURATION_REFERENCE: {
				CameraViewConfigurationReference cameraViewConfigurationReference = (CameraViewConfigurationReference)theEObject;
				T result = caseCameraViewConfigurationReference(cameraViewConfigurationReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.FILTER_LIST: {
				FilterList filterList = (FilterList)theEObject;
				T result = caseFilterList(filterList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.IMAGE_FILTER: {
				ImageFilter imageFilter = (ImageFilter)theEObject;
				T result = caseImageFilter(imageFilter);
				if (result == null) result = caseNamed(imageFilter);
				if (result == null) result = caseDescribed(imageFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.GRAY_SCALE_FILTER: {
				GrayScaleFilter grayScaleFilter = (GrayScaleFilter)theEObject;
				T result = caseGrayScaleFilter(grayScaleFilter);
				if (result == null) result = caseImageFilter(grayScaleFilter);
				if (result == null) result = caseNamed(grayScaleFilter);
				if (result == null) result = caseDescribed(grayScaleFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.EDGE_FILTER: {
				EdgeFilter edgeFilter = (EdgeFilter)theEObject;
				T result = caseEdgeFilter(edgeFilter);
				if (result == null) result = caseImageFilter(edgeFilter);
				if (result == null) result = caseNamed(edgeFilter);
				if (result == null) result = caseDescribed(edgeFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CONTRAST_AND_BRIGHTNESS_FILTER: {
				ContrastAndBrightnessFilter contrastAndBrightnessFilter = (ContrastAndBrightnessFilter)theEObject;
				T result = caseContrastAndBrightnessFilter(contrastAndBrightnessFilter);
				if (result == null) result = caseImageFilter(contrastAndBrightnessFilter);
				if (result == null) result = caseNamed(contrastAndBrightnessFilter);
				if (result == null) result = caseDescribed(contrastAndBrightnessFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.EXPOSURE_FILTER: {
				ExposureFilter exposureFilter = (ExposureFilter)theEObject;
				T result = caseExposureFilter(exposureFilter);
				if (result == null) result = caseImageFilter(exposureFilter);
				if (result == null) result = caseNamed(exposureFilter);
				if (result == null) result = caseDescribed(exposureFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.GAIN_FILTER: {
				GainFilter gainFilter = (GainFilter)theEObject;
				T result = caseGainFilter(gainFilter);
				if (result == null) result = caseImageFilter(gainFilter);
				if (result == null) result = caseNamed(gainFilter);
				if (result == null) result = caseDescribed(gainFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.INVERT_FILTER: {
				InvertFilter invertFilter = (InvertFilter)theEObject;
				T result = caseInvertFilter(invertFilter);
				if (result == null) result = caseImageFilter(invertFilter);
				if (result == null) result = caseNamed(invertFilter);
				if (result == null) result = caseDescribed(invertFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.RESCALE_FILTER: {
				RescaleFilter rescaleFilter = (RescaleFilter)theEObject;
				T result = caseRescaleFilter(rescaleFilter);
				if (result == null) result = caseImageFilter(rescaleFilter);
				if (result == null) result = caseNamed(rescaleFilter);
				if (result == null) result = caseDescribed(rescaleFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_IMAGE_ANNOTATION: {
				CameraImageAnnotation cameraImageAnnotation = (CameraImageAnnotation)theEObject;
				T result = caseCameraImageAnnotation(cameraImageAnnotation);
				if (result == null) result = caseNamed(cameraImageAnnotation);
				if (result == null) result = caseDescribed(cameraImageAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_OVERLAY_LIST: {
				CameraOverlayList cameraOverlayList = (CameraOverlayList)theEObject;
				T result = caseCameraOverlayList(cameraOverlayList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_OVERLAY: {
				CameraOverlay cameraOverlay = (CameraOverlay)theEObject;
				T result = caseCameraOverlay(cameraOverlay);
				if (result == null) result = caseCameraImageAnnotation(cameraOverlay);
				if (result == null) result = caseNamed(cameraOverlay);
				if (result == null) result = caseDescribed(cameraOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.DRAWN_CAMERA_OVERLAY: {
				DrawnCameraOverlay drawnCameraOverlay = (DrawnCameraOverlay)theEObject;
				T result = caseDrawnCameraOverlay(drawnCameraOverlay);
				if (result == null) result = caseCameraOverlay(drawnCameraOverlay);
				if (result == null) result = caseCameraImageAnnotation(drawnCameraOverlay);
				if (result == null) result = caseNamed(drawnCameraOverlay);
				if (result == null) result = caseDescribed(drawnCameraOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.ABSTRACT_TEXT_OVERLAY: {
				AbstractTextOverlay abstractTextOverlay = (AbstractTextOverlay)theEObject;
				T result = caseAbstractTextOverlay(abstractTextOverlay);
				if (result == null) result = caseDrawnCameraOverlay(abstractTextOverlay);
				if (result == null) result = caseCameraOverlay(abstractTextOverlay);
				if (result == null) result = caseCameraImageAnnotation(abstractTextOverlay);
				if (result == null) result = caseNamed(abstractTextOverlay);
				if (result == null) result = caseDescribed(abstractTextOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.EMF_FEATURE_OVERLAY: {
				EMFFeatureOverlay emfFeatureOverlay = (EMFFeatureOverlay)theEObject;
				T result = caseEMFFeatureOverlay(emfFeatureOverlay);
				if (result == null) result = caseAbstractTextOverlay(emfFeatureOverlay);
				if (result == null) result = caseDrawnCameraOverlay(emfFeatureOverlay);
				if (result == null) result = caseCameraOverlay(emfFeatureOverlay);
				if (result == null) result = caseCameraImageAnnotation(emfFeatureOverlay);
				if (result == null) result = caseNamed(emfFeatureOverlay);
				if (result == null) result = caseDescribed(emfFeatureOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_NAME_OVERLAY: {
				CameraNameOverlay cameraNameOverlay = (CameraNameOverlay)theEObject;
				T result = caseCameraNameOverlay(cameraNameOverlay);
				if (result == null) result = caseAbstractTextOverlay(cameraNameOverlay);
				if (result == null) result = caseDrawnCameraOverlay(cameraNameOverlay);
				if (result == null) result = caseCameraOverlay(cameraNameOverlay);
				if (result == null) result = caseCameraImageAnnotation(cameraNameOverlay);
				if (result == null) result = caseNamed(cameraNameOverlay);
				if (result == null) result = caseDescribed(cameraNameOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.IMAGE_COUNT_OVERLAY: {
				ImageCountOverlay imageCountOverlay = (ImageCountOverlay)theEObject;
				T result = caseImageCountOverlay(imageCountOverlay);
				if (result == null) result = caseAbstractTextOverlay(imageCountOverlay);
				if (result == null) result = caseDrawnCameraOverlay(imageCountOverlay);
				if (result == null) result = caseCameraOverlay(imageCountOverlay);
				if (result == null) result = caseCameraImageAnnotation(imageCountOverlay);
				if (result == null) result = caseNamed(imageCountOverlay);
				if (result == null) result = caseDescribed(imageCountOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.IMAGE_FROZEN_OVERLAY: {
				ImageFrozenOverlay imageFrozenOverlay = (ImageFrozenOverlay)theEObject;
				T result = caseImageFrozenOverlay(imageFrozenOverlay);
				if (result == null) result = caseAbstractTextOverlay(imageFrozenOverlay);
				if (result == null) result = caseDrawnCameraOverlay(imageFrozenOverlay);
				if (result == null) result = caseCameraOverlay(imageFrozenOverlay);
				if (result == null) result = caseCameraImageAnnotation(imageFrozenOverlay);
				if (result == null) result = caseNamed(imageFrozenOverlay);
				if (result == null) result = caseDescribed(imageFrozenOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.IMAGE_CAMERA_OVERLAY: {
				ImageCameraOverlay imageCameraOverlay = (ImageCameraOverlay)theEObject;
				T result = caseImageCameraOverlay(imageCameraOverlay);
				if (result == null) result = caseCameraOverlay(imageCameraOverlay);
				if (result == null) result = caseCameraImageAnnotation(imageCameraOverlay);
				if (result == null) result = caseNamed(imageCameraOverlay);
				if (result == null) result = caseDescribed(imageCameraOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.URL_IMAGE_OVERLAY: {
				URLImageOverlay urlImageOverlay = (URLImageOverlay)theEObject;
				T result = caseURLImageOverlay(urlImageOverlay);
				if (result == null) result = caseImageCameraOverlay(urlImageOverlay);
				if (result == null) result = caseCameraOverlay(urlImageOverlay);
				if (result == null) result = caseCameraImageAnnotation(urlImageOverlay);
				if (result == null) result = caseNamed(urlImageOverlay);
				if (result == null) result = caseDescribed(urlImageOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.APOGY_LOGO_OVERLAY: {
				ApogyLogoOverlay apogyLogoOverlay = (ApogyLogoOverlay)theEObject;
				T result = caseApogyLogoOverlay(apogyLogoOverlay);
				if (result == null) result = caseImageCameraOverlay(apogyLogoOverlay);
				if (result == null) result = caseCameraOverlay(apogyLogoOverlay);
				if (result == null) result = caseCameraImageAnnotation(apogyLogoOverlay);
				if (result == null) result = caseNamed(apogyLogoOverlay);
				if (result == null) result = caseDescribed(apogyLogoOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.FOV_OVERLAY: {
				FOVOverlay fovOverlay = (FOVOverlay)theEObject;
				T result = caseFOVOverlay(fovOverlay);
				if (result == null) result = caseDrawnCameraOverlay(fovOverlay);
				if (result == null) result = caseToolTipTextProvider(fovOverlay);
				if (result == null) result = caseCameraOverlay(fovOverlay);
				if (result == null) result = caseCameraImageAnnotation(fovOverlay);
				if (result == null) result = caseNamed(fovOverlay);
				if (result == null) result = caseDescribed(fovOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.AZIMUTH_ELEVATION_FOV_OVERLAY: {
				AzimuthElevationFOVOverlay azimuthElevationFOVOverlay = (AzimuthElevationFOVOverlay)theEObject;
				T result = caseAzimuthElevationFOVOverlay(azimuthElevationFOVOverlay);
				if (result == null) result = caseFOVOverlay(azimuthElevationFOVOverlay);
				if (result == null) result = caseDrawnCameraOverlay(azimuthElevationFOVOverlay);
				if (result == null) result = caseToolTipTextProvider(azimuthElevationFOVOverlay);
				if (result == null) result = caseCameraOverlay(azimuthElevationFOVOverlay);
				if (result == null) result = caseCameraImageAnnotation(azimuthElevationFOVOverlay);
				if (result == null) result = caseNamed(azimuthElevationFOVOverlay);
				if (result == null) result = caseDescribed(azimuthElevationFOVOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.EMF_FEATURE_AZIMUTH_ELEVATION_FOV_OVERLAY: {
				EMFFeatureAzimuthElevationFOVOverlay emfFeatureAzimuthElevationFOVOverlay = (EMFFeatureAzimuthElevationFOVOverlay)theEObject;
				T result = caseEMFFeatureAzimuthElevationFOVOverlay(emfFeatureAzimuthElevationFOVOverlay);
				if (result == null) result = caseAzimuthElevationFOVOverlay(emfFeatureAzimuthElevationFOVOverlay);
				if (result == null) result = caseFOVOverlay(emfFeatureAzimuthElevationFOVOverlay);
				if (result == null) result = caseDrawnCameraOverlay(emfFeatureAzimuthElevationFOVOverlay);
				if (result == null) result = caseToolTipTextProvider(emfFeatureAzimuthElevationFOVOverlay);
				if (result == null) result = caseCameraOverlay(emfFeatureAzimuthElevationFOVOverlay);
				if (result == null) result = caseCameraImageAnnotation(emfFeatureAzimuthElevationFOVOverlay);
				if (result == null) result = caseNamed(emfFeatureAzimuthElevationFOVOverlay);
				if (result == null) result = caseDescribed(emfFeatureAzimuthElevationFOVOverlay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.AZIMUTH_FEATURE_REFERENCE: {
				AzimuthFeatureReference azimuthFeatureReference = (AzimuthFeatureReference)theEObject;
				T result = caseAzimuthFeatureReference(azimuthFeatureReference);
				if (result == null) result = caseVariableFeatureReference(azimuthFeatureReference);
				if (result == null) result = caseNamed(azimuthFeatureReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.ELEVATION_FEATURE_REFERENCE: {
				ElevationFeatureReference elevationFeatureReference = (ElevationFeatureReference)theEObject;
				T result = caseElevationFeatureReference(elevationFeatureReference);
				if (result == null) result = caseVariableFeatureReference(elevationFeatureReference);
				if (result == null) result = caseNamed(elevationFeatureReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.TOOL_TIP_TEXT_PROVIDER: {
				ToolTipTextProvider toolTipTextProvider = (ToolTipTextProvider)theEObject;
				T result = caseToolTipTextProvider(toolTipTextProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_TOOL: {
				CameraTool cameraTool = (CameraTool)theEObject;
				T result = caseCameraTool(cameraTool);
				if (result == null) result = caseCameraImageAnnotation(cameraTool);
				if (result == null) result = caseNamed(cameraTool);
				if (result == null) result = caseDescribed(cameraTool);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_TOOL_LIST: {
				CameraToolList cameraToolList = (CameraToolList)theEObject;
				T result = caseCameraToolList(cameraToolList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.POINTER_CAMERA_TOOL: {
				PointerCameraTool pointerCameraTool = (PointerCameraTool)theEObject;
				T result = casePointerCameraTool(pointerCameraTool);
				if (result == null) result = caseCameraTool(pointerCameraTool);
				if (result == null) result = caseToolTipTextProvider(pointerCameraTool);
				if (result == null) result = caseAbsolutePoseProvider(pointerCameraTool);
				if (result == null) result = caseCameraImageAnnotation(pointerCameraTool);
				if (result == null) result = casePoseProvider(pointerCameraTool);
				if (result == null) result = caseNamed(pointerCameraTool);
				if (result == null) result = caseDescribed(pointerCameraTool);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL: {
				PTZCameraTool ptzCameraTool = (PTZCameraTool)theEObject;
				T result = casePTZCameraTool(ptzCameraTool);
				if (result == null) result = caseFOVOverlay(ptzCameraTool);
				if (result == null) result = caseCameraTool(ptzCameraTool);
				if (result == null) result = caseDrawnCameraOverlay(ptzCameraTool);
				if (result == null) result = caseToolTipTextProvider(ptzCameraTool);
				if (result == null) result = caseCameraOverlay(ptzCameraTool);
				if (result == null) result = caseCameraImageAnnotation(ptzCameraTool);
				if (result == null) result = caseNamed(ptzCameraTool);
				if (result == null) result = caseDescribed(ptzCameraTool);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_VIEW_CONFIGURATION_PAGES_PROVIDER: {
				CameraViewConfigurationPagesProvider cameraViewConfigurationPagesProvider = (CameraViewConfigurationPagesProvider)theEObject;
				T result = caseCameraViewConfigurationPagesProvider(cameraViewConfigurationPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(cameraViewConfigurationPagesProvider);
				if (result == null) result = caseWizardPagesProvider(cameraViewConfigurationPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.ABSTRACT_TEXT_OVERLAY_OVERLAY_PAGES_PROVIDER: {
				AbstractTextOverlayOverlayPagesProvider abstractTextOverlayOverlayPagesProvider = (AbstractTextOverlayOverlayPagesProvider)theEObject;
				T result = caseAbstractTextOverlayOverlayPagesProvider(abstractTextOverlayOverlayPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(abstractTextOverlayOverlayPagesProvider);
				if (result == null) result = caseWizardPagesProvider(abstractTextOverlayOverlayPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CAMERA_NAME_OVERLAY_PAGES_PROVIDER: {
				CameraNameOverlayPagesProvider cameraNameOverlayPagesProvider = (CameraNameOverlayPagesProvider)theEObject;
				T result = caseCameraNameOverlayPagesProvider(cameraNameOverlayPagesProvider);
				if (result == null) result = caseAbstractTextOverlayOverlayPagesProvider(cameraNameOverlayPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(cameraNameOverlayPagesProvider);
				if (result == null) result = caseWizardPagesProvider(cameraNameOverlayPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.IMAGE_FROZEN_OVERLAY_PAGES_PROVIDER: {
				ImageFrozenOverlayPagesProvider imageFrozenOverlayPagesProvider = (ImageFrozenOverlayPagesProvider)theEObject;
				T result = caseImageFrozenOverlayPagesProvider(imageFrozenOverlayPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(imageFrozenOverlayPagesProvider);
				if (result == null) result = caseWizardPagesProvider(imageFrozenOverlayPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.IMAGE_COUNT_OVERLAY_OVERLAY_PAGES_PROVIDER: {
				ImageCountOverlayOverlayPagesProvider imageCountOverlayOverlayPagesProvider = (ImageCountOverlayOverlayPagesProvider)theEObject;
				T result = caseImageCountOverlayOverlayPagesProvider(imageCountOverlayOverlayPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(imageCountOverlayOverlayPagesProvider);
				if (result == null) result = caseWizardPagesProvider(imageCountOverlayOverlayPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.EMF_FEATURE_OVERLAY_PAGES_PROVIDER: {
				EMFFeatureOverlayPagesProvider emfFeatureOverlayPagesProvider = (EMFFeatureOverlayPagesProvider)theEObject;
				T result = caseEMFFeatureOverlayPagesProvider(emfFeatureOverlayPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(emfFeatureOverlayPagesProvider);
				if (result == null) result = caseWizardPagesProvider(emfFeatureOverlayPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.URL_IMAGE_OVERLAY_PAGES_PROVIDER: {
				URLImageOverlayPagesProvider urlImageOverlayPagesProvider = (URLImageOverlayPagesProvider)theEObject;
				T result = caseURLImageOverlayPagesProvider(urlImageOverlayPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(urlImageOverlayPagesProvider);
				if (result == null) result = caseWizardPagesProvider(urlImageOverlayPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.FOV_OVERLAY_PAGES_PROVIDER: {
				FOVOverlayPagesProvider fovOverlayPagesProvider = (FOVOverlayPagesProvider)theEObject;
				T result = caseFOVOverlayPagesProvider(fovOverlayPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(fovOverlayPagesProvider);
				if (result == null) result = caseWizardPagesProvider(fovOverlayPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.POINTER_CAMERA_TOOL_PAGES_PROVIDER: {
				PointerCameraToolPagesProvider pointerCameraToolPagesProvider = (PointerCameraToolPagesProvider)theEObject;
				T result = casePointerCameraToolPagesProvider(pointerCameraToolPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(pointerCameraToolPagesProvider);
				if (result == null) result = caseWizardPagesProvider(pointerCameraToolPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL_PAGES_PROVIDER: {
				PTZCameraToolPagesProvider ptzCameraToolPagesProvider = (PTZCameraToolPagesProvider)theEObject;
				T result = casePTZCameraToolPagesProvider(ptzCameraToolPagesProvider);
				if (result == null) result = caseFOVOverlayPagesProvider(ptzCameraToolPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(ptzCameraToolPagesProvider);
				if (result == null) result = caseWizardPagesProvider(ptzCameraToolPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.GRAY_SCALE_FILTER_PAGES_PROVIDER: {
				GrayScaleFilterPagesProvider grayScaleFilterPagesProvider = (GrayScaleFilterPagesProvider)theEObject;
				T result = caseGrayScaleFilterPagesProvider(grayScaleFilterPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(grayScaleFilterPagesProvider);
				if (result == null) result = caseWizardPagesProvider(grayScaleFilterPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.EDGE_FILTER_PAGES_PROVIDER: {
				EdgeFilterPagesProvider edgeFilterPagesProvider = (EdgeFilterPagesProvider)theEObject;
				T result = caseEdgeFilterPagesProvider(edgeFilterPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(edgeFilterPagesProvider);
				if (result == null) result = caseWizardPagesProvider(edgeFilterPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.INVERT_FILTER_PAGES_PROVIDER: {
				InvertFilterPagesProvider invertFilterPagesProvider = (InvertFilterPagesProvider)theEObject;
				T result = caseInvertFilterPagesProvider(invertFilterPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(invertFilterPagesProvider);
				if (result == null) result = caseWizardPagesProvider(invertFilterPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.CONTRAST_AND_BRIGHTNESS_FILTER_PAGES_PROVIDER: {
				ContrastAndBrightnessFilterPagesProvider contrastAndBrightnessFilterPagesProvider = (ContrastAndBrightnessFilterPagesProvider)theEObject;
				T result = caseContrastAndBrightnessFilterPagesProvider(contrastAndBrightnessFilterPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(contrastAndBrightnessFilterPagesProvider);
				if (result == null) result = caseWizardPagesProvider(contrastAndBrightnessFilterPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.GAIN_FILTER_PAGES_PROVIDER: {
				GainFilterPagesProvider gainFilterPagesProvider = (GainFilterPagesProvider)theEObject;
				T result = caseGainFilterPagesProvider(gainFilterPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(gainFilterPagesProvider);
				if (result == null) result = caseWizardPagesProvider(gainFilterPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.EXPOSURE_FILTER_PAGES_PROVIDER: {
				ExposureFilterPagesProvider exposureFilterPagesProvider = (ExposureFilterPagesProvider)theEObject;
				T result = caseExposureFilterPagesProvider(exposureFilterPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(exposureFilterPagesProvider);
				if (result == null) result = caseWizardPagesProvider(exposureFilterPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsImagingCameraPackage.RESCALE_FILTER_PAGES_PROVIDER: {
				RescaleFilterPagesProvider rescaleFilterPagesProvider = (RescaleFilterPagesProvider)theEObject;
				T result = caseRescaleFilterPagesProvider(rescaleFilterPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(rescaleFilterPagesProvider);
				if (result == null) result = caseWizardPagesProvider(rescaleFilterPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Camera View Utilities</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera View Utilities</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCameraViewUtilities(CameraViewUtilities object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Camera View Configuration List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera View Configuration List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseCameraViewConfigurationList(CameraViewConfigurationList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Camera View Configuration</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera View Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseCameraViewConfiguration(CameraViewConfiguration object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Camera View Configuration Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera View Configuration Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCameraViewConfigurationReference(CameraViewConfigurationReference object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Filter List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Filter List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseFilterList(FilterList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Image Filter</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseImageFilter(ImageFilter object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Gray Scale Filter</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gray Scale Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseGrayScaleFilter(GrayScaleFilter object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Edge Filter</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Edge Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseEdgeFilter(EdgeFilter object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Contrast And Brightness Filter</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contrast And Brightness Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseContrastAndBrightnessFilter(ContrastAndBrightnessFilter object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Exposure Filter</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exposure Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseExposureFilter(ExposureFilter object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Gain Filter</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gain Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseGainFilter(GainFilter object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Invert Filter</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Invert Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseInvertFilter(InvertFilter object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Rescale Filter</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rescale Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRescaleFilter(RescaleFilter object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Camera Image Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera Image Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCameraImageAnnotation(CameraImageAnnotation object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Camera Overlay List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera Overlay List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseCameraOverlayList(CameraOverlayList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Camera Overlay</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseCameraOverlay(CameraOverlay object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Drawn Camera Overlay</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Drawn Camera Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseDrawnCameraOverlay(DrawnCameraOverlay object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Text Overlay</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Text Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseAbstractTextOverlay(AbstractTextOverlay object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>EMF Feature Overlay</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EMF Feature Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseEMFFeatureOverlay(EMFFeatureOverlay object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Camera Name Overlay</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera Name Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseCameraNameOverlay(CameraNameOverlay object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Image Count Overlay</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image Count Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImageCountOverlay(ImageCountOverlay object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Image Frozen Overlay</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image Frozen Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImageFrozenOverlay(ImageFrozenOverlay object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Image Camera Overlay</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image Camera Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseImageCameraOverlay(ImageCameraOverlay object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>URL Image Overlay</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>URL Image Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseURLImageOverlay(URLImageOverlay object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Apogy Logo Overlay</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Apogy Logo Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseApogyLogoOverlay(ApogyLogoOverlay object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>FOV Overlay</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FOV Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFOVOverlay(FOVOverlay object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Azimuth Elevation FOV Overlay</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Azimuth Elevation FOV Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAzimuthElevationFOVOverlay(AzimuthElevationFOVOverlay object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EMF Feature Azimuth Elevation FOV Overlay</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EMF Feature Azimuth Elevation FOV Overlay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEMFFeatureAzimuthElevationFOVOverlay(EMFFeatureAzimuthElevationFOVOverlay object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Azimuth Feature Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Azimuth Feature Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAzimuthFeatureReference(AzimuthFeatureReference object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Elevation Feature Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Elevation Feature Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElevationFeatureReference(ElevationFeatureReference object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Tool Tip Text Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tool Tip Text Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseToolTipTextProvider(ToolTipTextProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Camera Tool</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera Tool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCameraTool(CameraTool object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Camera Tool List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera Tool List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCameraToolList(CameraToolList object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Pointer Camera Tool</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pointer Camera Tool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePointerCameraTool(PointerCameraTool object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>PTZ Camera Tool</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PTZ Camera Tool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePTZCameraTool(PTZCameraTool object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Camera View Configuration Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera View Configuration Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCameraViewConfigurationPagesProvider(CameraViewConfigurationPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Text Overlay Overlay Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Text Overlay Overlay Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractTextOverlayOverlayPagesProvider(AbstractTextOverlayOverlayPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Camera Name Overlay Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Camera Name Overlay Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCameraNameOverlayPagesProvider(CameraNameOverlayPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Image Frozen Overlay Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image Frozen Overlay Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImageFrozenOverlayPagesProvider(ImageFrozenOverlayPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Image Count Overlay Overlay Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image Count Overlay Overlay Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImageCountOverlayOverlayPagesProvider(ImageCountOverlayOverlayPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EMF Feature Overlay Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EMF Feature Overlay Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEMFFeatureOverlayPagesProvider(EMFFeatureOverlayPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>URL Image Overlay Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>URL Image Overlay Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseURLImageOverlayPagesProvider(URLImageOverlayPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>FOV Overlay Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FOV Overlay Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFOVOverlayPagesProvider(FOVOverlayPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Pointer Camera Tool Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pointer Camera Tool Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePointerCameraToolPagesProvider(PointerCameraToolPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>PTZ Camera Tool Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PTZ Camera Tool Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePTZCameraToolPagesProvider(PTZCameraToolPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Gray Scale Filter Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gray Scale Filter Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGrayScaleFilterPagesProvider(GrayScaleFilterPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Edge Filter Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Edge Filter Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEdgeFilterPagesProvider(EdgeFilterPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Invert Filter Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Invert Filter Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInvertFilterPagesProvider(InvertFilterPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Contrast And Brightness Filter Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contrast And Brightness Filter Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContrastAndBrightnessFilterPagesProvider(ContrastAndBrightnessFilterPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Gain Filter Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gain Filter Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGainFilterPagesProvider(GainFilterPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Exposure Filter Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exposure Filter Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExposureFilterPagesProvider(ExposureFilterPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Rescale Filter Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rescale Filter Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRescaleFilterPagesProvider(RescaleFilterPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Tools List Container</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Tools List Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseAbstractToolsListContainer(AbstractToolsListContainer object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Feature Reference</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Feature Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseVariableFeatureReference(VariableFeatureReference object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseNamed(Named object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Described</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Described</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseDescribed(Described object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Tool</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Tool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseAbstractTool(AbstractTool object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Pose Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pose Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePoseProvider(PoseProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Absolute Pose Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Absolute Pose Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbsolutePoseProvider(AbsolutePoseProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWizardPagesProvider(WizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedDescribedWizardPagesProvider(NamedDescribedWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
  @Override
  public T defaultCase(EObject object)
  {
		return null;
	}

} //ApogyAddonsSensorsImagingCameraSwitch

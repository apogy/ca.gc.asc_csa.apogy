package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.AbstractCamera;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ImageSnapshot;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tool Tip Text Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A provider of tooltip text to be displayed on top of an image.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage#getToolTipTextProvider()
 * @model
 * @generated
 */
public interface ToolTipTextProvider extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the text to display as the result of the user clicking on the image.
	 * @param camera The camera that produced the image.
	 * @param imageSnapshot The image snaphot displayed.
	 * @param mouseButton The mouse button clicked.
	 * @param x The position (from left to right) of the click of the mouse, in pixels.
	 * @param y The position (from up to down) of the click of the mouse, in pixels.
	 * @return The string to display, can be null.
	 * <!-- end-model-doc -->
	 * @model unique="false" cameraUnique="false" imageSnapshotUnique="false" mouseButtonUnique="false" xUnique="false"
	 *        xAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'" yUnique="false"
	 *        yAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'"
	 * @generated
	 */
	String getToolTipText(AbstractCamera camera, ImageSnapshot imageSnapshot, int mouseButton, int x, int y);

} // ToolTipTextProvider

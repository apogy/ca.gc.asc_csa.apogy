/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.internal;

import java.util.Date;

import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVFacade;
import ca.gc.asc_csa.apogy.addons.sensors.fov.RectangularFrustrumFieldOfView;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ApogyAddonsSensorsImagingFactory;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ImageSnapshot;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.impl.AbstractCameraImpl;
import ca.gc.asc_csa.apogy.common.images.ApogyCommonImagesFactory;
import ca.gc.asc_csa.apogy.common.images.URLEImage;

public class AbstractCameraStub extends AbstractCameraImpl 
{
	private String imageURL;
	private RectangularFrustrumFieldOfView fov;
	
	public AbstractCameraStub(String imageURL, double horizontalFieldOfViewAngle, double verticalFieldOfViewAngle)
	{
		this.imageURL = imageURL;
		
		fov = ApogyAddonsSensorsFOVFacade.INSTANCE.createRectangularFrustrumFieldOfView(0, 100, horizontalFieldOfViewAngle, verticalFieldOfViewAngle);
	}
	
	@Override
	public RectangularFrustrumFieldOfView getFieldOfView() 
	{
		return fov;
	}
	
	@Override
	public ImageSnapshot takeSnapshot() 
	{
		ImageSnapshot imageSnapshot = ApogyAddonsSensorsImagingFactory.eINSTANCE.createImageSnapshot();
		imageSnapshot.setFieldOfView(ApogyAddonsSensorsFOVFacade.INSTANCE.createRectangularFrustrumFieldOfView(fov));
		imageSnapshot.setTime(new Date());
		
		// Adds the image.
		URLEImage urlImage = ApogyCommonImagesFactory.eINSTANCE.createURLEImage();
		urlImage.setUrl(imageURL);				
		imageSnapshot.setImage(urlImage);
		
		setLatestImageSnapshot(imageSnapshot);
		
		return imageSnapshot;
	}
}

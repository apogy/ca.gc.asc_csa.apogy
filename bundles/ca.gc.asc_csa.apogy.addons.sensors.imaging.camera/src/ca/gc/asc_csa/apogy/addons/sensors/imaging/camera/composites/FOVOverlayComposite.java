/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.AzimuthDirection;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ElevationDirection;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.FOVOverlay;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.Color3fComposite;

public class FOVOverlayComposite extends Composite 
{
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
			
	private FOVOverlay fovOverlay;
		
	private	ComboViewer fontComboViewer;
	private Combo fontCombo;
	private Spinner fontSizeSpinner;	
	
	private Color3fComposite positiveColorComposite;
	private Color3fComposite negativeColorComposite;
	
	private ComboViewer azimuthDirectionComboViewer;
	private ComboViewer elevationDirectionComboViewer;
	private Spinner angleIntervalSpinner;	
	private Spinner lineWidthSpinner;
	
	private DrawnCameraOverlayPreviewComposite drawnCameraOverlayPreviewComposite;
	
	private DataBindingContext m_bindingContext;
	
	public FOVOverlayComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new GridLayout(2, false));
		
		Group grpFontSettings = new Group(this, SWT.NONE);
		grpFontSettings.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpFontSettings.setText("Font Settings");
		grpFontSettings.setLayout(new GridLayout(4, false));
		
		Label lblFont = new Label(grpFontSettings, SWT.NONE);
		lblFont.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFont.setText("Font:");
		
		fontComboViewer = new ComboViewer(grpFontSettings, SWT.NONE);
		fontCombo = fontComboViewer.getCombo();
		GridData gd_fontCombo = new GridData(SWT.LEFT, SWT.CENTER, true, false, 3, 1);
		gd_fontCombo.widthHint = 250;
		gd_fontCombo.minimumWidth = 250;
		fontCombo.setLayoutData(gd_fontCombo);
		fontComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		fontComboViewer.setInput(getAvailableFonts());
		fontComboViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				if(event.getSelection().isEmpty())
				{					
				}
				else if(event.getSelection() instanceof IStructuredSelection)
				{
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					if(iStructuredSelection.getFirstElement() instanceof String)
					{
						String fontName = (String) iStructuredSelection.getFirstElement();
						if(fovOverlay != null)
						{
							if(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getFovOverlay()) == null)
							{
								ApogyCommonTransactionFacade.INSTANCE.addInTempTransactionalEditingDomain(getFovOverlay());
							}							
								
							ApogyCommonTransactionFacade.INSTANCE.basicSet(getFovOverlay(), ApogyAddonsSensorsImagingCameraPackage.Literals.FOV_OVERLAY__FONT_NAME, fontName);
						}
						
						fontNameSelected(fontName);						
					}
					else
					{						
					}
				}									
			}
		});
		
		Label lblFontSize = new Label(grpFontSettings, SWT.NONE);
		lblFontSize.setText("Font Size:");
		
		fontSizeSpinner = new Spinner(grpFontSettings, SWT.BORDER);
		GridData gd_fontSizeSpinner = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_fontSizeSpinner.minimumWidth = 50;
		gd_fontSizeSpinner.widthHint = 50;
		fontSizeSpinner.setLayoutData(gd_fontSizeSpinner);
			
		Group grpColorSettings = new Group(this, SWT.NONE);
		grpColorSettings.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpColorSettings.setText("Color Settings");
		grpColorSettings.setLayout(new GridLayout(2, false));
		
		Label lblPositiveValuesColors = new Label(grpColorSettings, SWT.NONE);
		lblPositiveValuesColors.setText("Positive Values Color:");
		
		positiveColorComposite = new Color3fComposite(grpColorSettings, SWT.NONE);
		positiveColorComposite.setFeature(ApogyAddonsSensorsImagingCameraPackage.Literals.FOV_OVERLAY__POSITIVE_VALUES_COLOR);	
		
		Label lblNegativeValuesColor = new Label(grpColorSettings, SWT.NONE);
		lblNegativeValuesColor.setText("Negative Values Color:");
		
		negativeColorComposite = new Color3fComposite(grpColorSettings, SWT.NONE);		
		negativeColorComposite.setFeature(ApogyAddonsSensorsImagingCameraPackage.Literals.FOV_OVERLAY__NEGATIVE_VALUE_COLOR);	
		
		Group grpDirectionSettings = new Group(this, SWT.NONE);
		grpDirectionSettings.setLayout(new GridLayout(2, false));
		grpDirectionSettings.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpDirectionSettings.setText("Direction Settings");
		
		Label lblAzimuthDirection = new Label(grpDirectionSettings, SWT.NONE);
		lblAzimuthDirection.setText("Azimuth Direction:");
				 
		azimuthDirectionComboViewer = new ComboViewer(grpDirectionSettings, SWT.NONE);				
		GridData gd_imageSizePolicyComboViewer = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_imageSizePolicyComboViewer.minimumWidth = 200;
		gd_imageSizePolicyComboViewer.widthHint = 200;
		azimuthDirectionComboViewer.getCombo().setLayoutData(gd_imageSizePolicyComboViewer);
		azimuthDirectionComboViewer.setContentProvider(new ArrayContentProvider());
		azimuthDirectionComboViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		azimuthDirectionComboViewer.setInput(AzimuthDirection.values());
		
		Label lblElevationDirection = new Label(grpDirectionSettings, SWT.NONE);
		lblElevationDirection.setText("Elevation Direction:");

		elevationDirectionComboViewer = new ComboViewer(grpDirectionSettings, SWT.NONE);				
		GridData gd_elevationDirectionComboViewer = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_elevationDirectionComboViewer.minimumWidth = 200;
		gd_elevationDirectionComboViewer.widthHint = 200;
		elevationDirectionComboViewer.getCombo().setLayoutData(gd_elevationDirectionComboViewer);
		elevationDirectionComboViewer.setContentProvider(new ArrayContentProvider());
		elevationDirectionComboViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		elevationDirectionComboViewer.setInput(ElevationDirection.values());
		
		// Cross-Hair
		Group grpCrosshairSettings = new Group(this, SWT.NONE);
		grpCrosshairSettings.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpCrosshairSettings.setText("Cross-Hair Settings");
		grpCrosshairSettings.setLayout(new GridLayout(2, false));
		
		Label lblAngleIntervaldeg = new Label(grpCrosshairSettings, SWT.NONE);
		lblAngleIntervaldeg.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAngleIntervaldeg.setText("Angle Interval (deg):");
		
		angleIntervalSpinner = new Spinner(grpCrosshairSettings, SWT.BORDER);
		angleIntervalSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		angleIntervalSpinner.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				angleIntervalSelected(angleIntervalSpinner.getSelection());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {				
			}
		});
		
		Label lblLineWidthpixels = new Label(grpCrosshairSettings, SWT.NONE);
		lblLineWidthpixels.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLineWidthpixels.setText("Line Width (pixels):");
		
		lineWidthSpinner = new Spinner(grpCrosshairSettings, SWT.BORDER);
		lineWidthSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lineWidthSpinner.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				lineWidthSelected(lineWidthSpinner.getSelection());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
		Group grpPreview = new Group(this, SWT.NONE);
		grpPreview.setLayout(new GridLayout(1, false));
		grpPreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		grpPreview.setText("Preview");
		
		drawnCameraOverlayPreviewComposite = new DrawnCameraOverlayPreviewComposite(grpPreview, SWT.BORDER);
		GridData gd_imagePreviewComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_imagePreviewComposite.minimumWidth = 160;
		gd_imagePreviewComposite.minimumHeight = 320;
		drawnCameraOverlayPreviewComposite.setLayoutData(gd_imagePreviewComposite);		
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public FOVOverlay getFovOverlay() {
		return fovOverlay;
	}

	public void setFovOverlay(FOVOverlay fovOverlay) 
	{
		if (m_bindingContext != null) m_bindingContext.dispose();
		
		this.fovOverlay = fovOverlay;
		
		if(fovOverlay != null)
		{
			drawnCameraOverlayPreviewComposite.setDrawnCameraOverlay(fovOverlay);
			m_bindingContext = customInitDataBindings();
		}
	}
	
	protected void lineWidthSelected(int lineWidth)
	{		
	}
	
	protected void angleIntervalSelected(int angleInterval)
	{		
	}
	
	protected void fontNameSelected(String fontName)
	{		
	}
	
	protected List<String> getAvailableFonts()
	{
		List<String> fonts = new ArrayList<String>();
		
		String[] fontNames = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
	   	for ( int i = 0; i < fontNames.length; i++ )
	   	{
	   		fonts.add(fontNames[i]);
	    }
		
		return fonts;
	}
	
	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		/* Font name */
		
		IObservableValue<Double> observeFontName = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(fovOverlay), 
				  										FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.FOV_OVERLAY__FONT_NAME)).observe(fovOverlay);
		IObservableValue<?> observeFontComboViewer = WidgetProperties.selection().observe(fontComboViewer.getCombo());
		
		bindingContext.bindValue(observeFontComboViewer,
								 observeFontName, 
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		
		/* Font size Value. */
		IObservableValue<Double> observeFontSize = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(fovOverlay), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.FOV_OVERLAY__FONT_SIZE)).observe(fovOverlay);
		IObservableValue<String> observeFontSizeSpinner = WidgetProperties.selection().observe(fontSizeSpinner);

		bindingContext.bindValue(observeFontSizeSpinner,
								 observeFontSize, 
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());

		
		/* Positive color.*/
		positiveColorComposite.setOwner(fovOverlay);
		
		/* Negative color.*/
		negativeColorComposite.setOwner(fovOverlay);
		
		/* Azimuth Direction.*/
		IObservableValue<Double> observeAzimuthDirection = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(fovOverlay),
																				  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.FOV_OVERLAY__AZIMUTH_DIRECTION)).observe(fovOverlay);
		
		IObservableValue<?> observeAzimuthDirectionCombViewer = ViewerProperties.singleSelection().observe(azimuthDirectionComboViewer);
		bindingContext.bindValue(observeAzimuthDirectionCombViewer, 
								 observeAzimuthDirection,
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		/* ElevationDirection Direction.*/
		IObservableValue<Double> observeElevationDirection = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(fovOverlay),
																				  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.FOV_OVERLAY__ELEVATION_DIRECTION)).observe(fovOverlay);
		
		IObservableValue<?> observeElevationDirectionCombViewer = ViewerProperties.singleSelection().observe(elevationDirectionComboViewer);
		bindingContext.bindValue(observeElevationDirectionCombViewer, 
								 observeElevationDirection,
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		/* Line Width Value. */
		IObservableValue<Double> observeLineWidth = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(fovOverlay), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.FOV_OVERLAY__LINE_WIDTH)).observe(fovOverlay);
		IObservableValue<String> observeLineWidthSpinner = WidgetProperties.selection().observe(lineWidthSpinner);

		bindingContext.bindValue(observeLineWidthSpinner,
								 observeLineWidth, 
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		/* Contrast Value. */
		IObservableValue<Double> observeAngleInterval = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(fovOverlay), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.FOV_OVERLAY__ANGLE_INTERVAL)).observe(fovOverlay);
		IObservableValue<String> observeAngleIntervalText = WidgetProperties.selection().observe(angleIntervalSpinner);

		bindingContext.bindValue(observeAngleIntervalText,
								 observeAngleInterval, 
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		return bindingContext;
	}
}

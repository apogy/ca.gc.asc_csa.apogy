/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectMenuItem;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfiguration;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.CameraViewComposite;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.parts.CameraViewPart;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;

public class SetActiveCameraViewConfigurationHandler 
{
	@CanExecute
	public boolean canExecute(MPart part)
	{
		return ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() != null;
	}
	
	@Execute
	public void execute(MDirectMenuItem selectedItem, MPart part)
	{		
		if (part.getObject() instanceof CameraViewPart)
		{
			CameraViewPart viewPart = (CameraViewPart) part.getObject();						
			if(viewPart.getActualComposite() instanceof CameraViewComposite)
			{
				CameraViewComposite composite = (CameraViewComposite)viewPart.getActualComposite();
				
				if(composite != null && !composite.isDisposed())
				{
					CameraViewConfiguration cameraViewConfiguration = (CameraViewConfiguration) selectedItem.getTransientData().get("cameraViewConfiguration");				
					composite.setCameraViewConfiguration(cameraViewConfiguration);
				}
			}
		}		
	}	
}
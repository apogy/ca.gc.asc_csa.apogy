/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera;

public class ApogyAddonsSensorsImagingCameraRCPConstants {

	/**
	 * Camera view Part
	 */
	public static final String PART__CAMERA_VIEW__ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.part.cameraView";
	public static final String TOOL_BAR__CAMERA_VIEW__ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.toolbar.cameraView";
	
	/** ToolItems */
	public static final String HANDLED_TOOL_ITEM__DISPLAY_RECTIFIED_IMAGE__ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.handledtoolitem.displayRectifiedImage";
	public static final String HANDLED_TOOL_ITEM__IMAGE_AUTO_SAVE__ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.handledtoolitem.imageAutoSaveEnable";
	public static final String HANDLED_TOOL_ITEM__SAVE_IMAGE_WITH_OVERLAYS__ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.handledtoolitem.saveImageWithOverlays";
	public static final String HANDLED_TOOL_ITEM__DELETE_CAMERA_VIEW_CONFIGURATION__ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.handledtoolitem.deleteCameraViewConfiguration";

	/**
	 * Commands
	 */
	public static final String COMMAND__UPDATE_CAMERA_VIEW_TOOLBAR__ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.command.updateCameraViewToolbar";

}

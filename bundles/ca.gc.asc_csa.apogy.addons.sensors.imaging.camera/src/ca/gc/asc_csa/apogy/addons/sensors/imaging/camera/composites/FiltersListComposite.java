/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfiguration;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageFilter;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.impl.FeaturePathAdapterImpl;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class FiltersListComposite extends Composite {

	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
			ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	private TreeViewer treeViewer;
	private Composite emfFormsComposite;
	private ScrolledComposite scrolledComposite;
	private Button btnNew;
	private Button btnDelete;

	private Button btnUp;
	private Button btnDown;

	private Button btnVisible;

	private DataBindingContext m_bindingContext;

	private CameraViewConfiguration cameraViewConfiguration;
	private List<FeaturePathAdapter> featurePathAdapters;

	public FiltersListComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(4, false));
		this.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		this.setBackgroundMode(SWT.INHERIT_FORCE);
		addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				dispose();
			}
		});

		treeViewer = new TreeViewer(this);
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				FiltersListComposite.this.newSelection();
				updateEMFForms();

				if (btnVisible != null) {
					btnVisible.setVisible(getSelectedFilter() != null);
				}
			}

		});
		treeViewer.setLabelProvider(getLabelProvider());
		treeViewer.setContentProvider(getContentProvider());
		treeViewer.getTree().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));

		Composite buttonComposite = new Composite(this, SWT.None);
		buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 2));
		buttonComposite.setLayout(new GridLayout(1, false));

		btnNew = new Button(buttonComposite, SWT.None);
		btnNew.setText("New");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				NamedSetting namedSettings = ApogyCommonEMFUIFactory.eINSTANCE.createNamedSetting();
				namedSettings.setParent(getCameraViewConfiguration().getFilterList());
				namedSettings.setContainingFeature(
						ApogyAddonsSensorsImagingCameraPackage.Literals.FILTER_LIST__IMAGE_FILTERS);

				Wizard wizard = new ApogyEObjectWizard(
						ApogyAddonsSensorsImagingCameraPackage.Literals.FILTER_LIST__IMAGE_FILTERS,
						getCameraViewConfiguration().getFilterList(), namedSettings, null);
				WizardDialog dialog = new WizardDialog(getShell(), wizard);
				dialog.open();

				treeViewer.refresh();
				updateEMFForms();
			}
		});

		btnDelete = new Button(buttonComposite, SWT.None);
		btnDelete.setText("Delete");
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (getSelectedFilter() != null) {
					ApogyCommonTransactionFacade.INSTANCE.basicRemove(getCameraViewConfiguration().getFilterList(),
							ApogyAddonsSensorsImagingCameraPackage.Literals.FILTER_LIST__IMAGE_FILTERS,
							getSelectedFilter());
				}
			}
		});

		btnUp = new Button(buttonComposite, SWT.NONE);
		btnUp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnUp.setSize(74, 29);
		btnUp.setText("Up");
		btnUp.setEnabled(false);
		btnUp.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (getSelectedFilter() != null) {
					ImageFilter imageFilter = getSelectedFilter();

					int index = getCameraViewConfiguration().getFilterList().getImageFilters().indexOf(imageFilter);

					if (index > 0) {
						getCameraViewConfiguration().getFilterList().getImageFilters().move(index - 1, index);

						// Forces the viewer to refresh its input.
						if (!treeViewer.isBusy()) {
							treeViewer.refresh();
						}
					}
				}
			}
		});

		btnDown = new Button(buttonComposite, SWT.NONE);
		btnDown.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDown.setSize(74, 29);
		btnDown.setText("Down");
		btnDown.setEnabled(false);
		btnDown.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (getSelectedFilter() != null) {
					ImageFilter imageFilter = getSelectedFilter();

					int listSize = getCameraViewConfiguration().getFilterList().getImageFilters().size();
					int index = getCameraViewConfiguration().getFilterList().getImageFilters().indexOf(imageFilter);

					if (index < (listSize - 1)) {
						getCameraViewConfiguration().getFilterList().getImageFilters().move(index + 1, index);

						// Forces the viewer to refresh its input.
						if (!treeViewer.isBusy()) {
							treeViewer.refresh();
						}
					}
				}
			}
		});

		Label separator = new Label(this, SWT.SEPARATOR);
		separator.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 2));

		Composite toggleVisibleComposite = new Composite(this, SWT.None);
		toggleVisibleComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		toggleVisibleComposite.setLayout(new GridLayout(1, false));

		btnVisible = new Button(toggleVisibleComposite, SWT.None);
		btnVisible.setText("Toggle Active");
		btnVisible.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1));
		btnVisible.setVisible(false);
		btnVisible.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (getSelectedFilter() != null) {
					ApogyCommonTransactionFacade.INSTANCE.basicSet(getSelectedFilter(),
							ApogyAddonsSensorsImagingCameraPackage.Literals.IMAGE_FILTER__ENABLED,
							!getSelectedFilter().isEnabled());
				}
			}
		});

		scrolledComposite = new ScrolledComposite(this, SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		emfFormsComposite = new Composite(scrolledComposite, SWT.None);
		scrolledComposite.setContent(emfFormsComposite);
		scrolledComposite.setMinSize(emfFormsComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		updateEMFForms();

		addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (cameraViewConfiguration != null) 
				{
					for (FeaturePathAdapter adapter : getFeaturePathAdapter()) 
					{
						adapter.dispose();
					}
				}

				if (m_bindingContext != null) m_bindingContext.dispose();
			}
		});

	}

	/**
	 * Updates the EMFForms displaying the image annotation selected.
	 */
	private void updateEMFForms() 
	{
		if(!emfFormsComposite.isDisposed())
		{
			for (Control control : emfFormsComposite.getChildren()) 
			{
				if(! control.isDisposed()) control.dispose();
			}
	
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(emfFormsComposite, getSelectedFilter(),"No compatible selection");

			if(!scrolledComposite.isDisposed()) scrolledComposite.setMinSize(emfFormsComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		}
	}

	protected void newSelection() {

	}

	public ImageFilter getSelectedFilter() 
	{
		if (treeViewer.getStructuredSelection() != null && treeViewer.getStructuredSelection().getFirstElement() != null) 
		{
			Object object = treeViewer.getStructuredSelection().getFirstElement();

			if (object instanceof ImageFilter) 
			{
				return (ImageFilter) object;
			}
		}
		return null;
	}

	public CameraViewConfiguration getCameraViewConfiguration() 
	{
		return cameraViewConfiguration;
	}

	public void setCameraViewConfiguration(CameraViewConfiguration cameraViewConfiguration) 
	{
		if (m_bindingContext != null) m_bindingContext.dispose();

		if (this.cameraViewConfiguration != null) 
		{
			for (FeaturePathAdapter adapter : getFeaturePathAdapter()) 
			{
				adapter.dispose();
			}
		}
		this.cameraViewConfiguration = cameraViewConfiguration;

		if (cameraViewConfiguration != null) {
			treeViewer.setInput(this.cameraViewConfiguration.getFilterList());

			for (FeaturePathAdapter adapter : getFeaturePathAdapter()) {
				adapter.init(this.cameraViewConfiguration);
			}

			m_bindingContext = customInitDataBindings();

			btnNew.setEnabled(true);
			btnDelete.setEnabled(true);
		} else {
			treeViewer.setInput(null);

			btnNew.setEnabled(false);
			btnDelete.setEnabled(false);
		}
	}

	private List<FeaturePathAdapter> getFeaturePathAdapter() {
		if (this.featurePathAdapters == null) {
			this.featurePathAdapters = new ArrayList<FeaturePathAdapter>();

			this.featurePathAdapters.add(new FeaturePathAdapterImpl() {

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> list = new ArrayList<EStructuralFeature>();

					list.add(ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_VIEW_CONFIGURATION__FILTER_LIST);
					list.add(ApogyAddonsSensorsImagingCameraPackage.Literals.FILTER_LIST__IMAGE_FILTERS);
					list.add(ApogyCommonEMFPackage.Literals.NAMED__NAME);

					return list;
				}

				@Override
				public void notifyChanged(Notification msg) {
					treeViewer.refresh();
				}
			});
			this.featurePathAdapters.add(new FeaturePathAdapterImpl() {

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> list = new ArrayList<EStructuralFeature>();

					list.add(ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_VIEW_CONFIGURATION__FILTER_LIST);
					list.add(ApogyAddonsSensorsImagingCameraPackage.Literals.FILTER_LIST__IMAGE_FILTERS);
					list.add(ApogyAddonsSensorsImagingCameraPackage.Literals.IMAGE_FILTER__ENABLED);

					return list;
				}

				@Override
				public void notifyChanged(Notification msg) {
					treeViewer.refresh();
				}
			});
		}
		return featurePathAdapters;
	}

	protected AdapterFactoryContentProvider getContentProvider() {
		return new AdapterFactoryContentProvider(adapterFactory) {
			@Override
			public Object[] getElements(Object object) {
				if (cameraViewConfiguration != null && cameraViewConfiguration.getFilterList() != null
						&& cameraViewConfiguration.getFilterList().getImageFilters() != null) {

					return cameraViewConfiguration.getFilterList().getImageFilters().toArray();
				}
				return null;
			}

			@Override
			public Object[] getChildren(Object object) {
				return null;
			}
		};
	}

	protected AdapterFactoryLabelProvider getLabelProvider() {
		return new AdapterFactoryLabelProvider(adapterFactory) {
			@Override
			public String getText(Object object) {
				if (object instanceof ImageFilter) {
					ImageFilter filter = ((ImageFilter) object);
					String label = filter.getName();

					if (filter.isEnabled()) {
						label += " <enabled>";
					}
					return label;
				}
				return "";
			}

			@Override
			public Image getImage(Object object) {
				return null;
			}
		};
	}

	protected DataBindingContext customInitDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();

		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);

		/* Up Button Enabled Binding. */
		IObservableValue<?> observeBtnUpObserveWidget = WidgetProperties.enabled().observe(btnUp);
		bindingContext.bindValue(observeBtnUpObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));

		/* Down Button Enabled Binding. */
		IObservableValue<?> observeBtnDownObserveWidget = WidgetProperties.enabled().observe(btnDown);
		bindingContext.bindValue(observeBtnDownObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));

		return bindingContext;
	}

}

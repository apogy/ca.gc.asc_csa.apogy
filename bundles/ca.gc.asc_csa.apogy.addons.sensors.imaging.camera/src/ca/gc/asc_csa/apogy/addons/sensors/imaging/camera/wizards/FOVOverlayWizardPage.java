/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.FOVOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.FOVOverlayComposite;

public class FOVOverlayWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.FOVOverlayWizardPage";
		
	private FOVOverlay fovOverlay;
	
	private FOVOverlayComposite fovOverlayComposite; 
	private DataBindingContext m_bindingContext;
	
	public FOVOverlayWizardPage(FOVOverlay fovOverlay) 
	{
		super(WIZARD_PAGE_ID);
		this.fovOverlay = fovOverlay;
			
		setTitle("Filed Of View (FOV) Overlay.");
		setDescription("Select the FOV settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		fovOverlayComposite = new FOVOverlayComposite(top, SWT.BORDER)
		{
			@Override
			protected void lineWidthSelected(int lineWidth)
			{		
				validate();
			}
			
			@Override
			protected void angleIntervalSelected(int angleInterval)
			{	
				validate();
			}
			
		};
		fovOverlayComposite.setFovOverlay(fovOverlay);
		fovOverlayComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);		
						
		if(fovOverlay.getFontName() == null)
		{
			setErrorMessage("No font selected !");
		}
		
		if(fovOverlay.getAngleInterval() <= 0)
		{
			setErrorMessage("Invalid Angle Interval specified ! Must be greater than zero.");
		}
		
		if(fovOverlay.getLineWidth() <= 0)
		{
			setErrorMessage("Invalid Line Width specified ! Must be greater than zero.");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}

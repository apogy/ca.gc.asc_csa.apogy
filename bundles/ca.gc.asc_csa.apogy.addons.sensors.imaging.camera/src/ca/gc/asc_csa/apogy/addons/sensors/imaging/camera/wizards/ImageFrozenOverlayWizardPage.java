/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageFrozenOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.ImageFrozenOverlayComposite;

public class ImageFrozenOverlayWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.EMFFeatureOverlayWizardPage";
		
	private ImageFrozenOverlay imageFrozenOverlay;
	private ImageFrozenOverlayComposite imageFrozenOverlayComposite;
		
	private DataBindingContext m_bindingContext;
	
	public ImageFrozenOverlayWizardPage(ImageFrozenOverlay imageFrozenOverlay) 
	{
		super(WIZARD_PAGE_ID);
		this.imageFrozenOverlay = imageFrozenOverlay;
			
		setTitle("Image Frozen Overlay.");
		setDescription("Select Expected Update Period and Frozen message.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		imageFrozenOverlayComposite = new ImageFrozenOverlayComposite(top, SWT.BORDER)
		{
			@Override
			protected void newExpectedImageUpdatePeriodSelected(double expectedImageUpdatePeriod)
			{		
				validate();
			}
			
			@Override
			protected void newFrozenMessageSelected(String frozenMessage)
			{	
				validate();
			}
		};
		imageFrozenOverlayComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		imageFrozenOverlayComposite.setImageFrozenOverlay(imageFrozenOverlay);
				
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
		
	protected void validate()
	{
		setErrorMessage(null);		
						
		if(imageFrozenOverlay.getExpectedImageUpdatePeriod() <= 0)
		{
			setErrorMessage("The Expected Image Refresh Period specified is invalid ! It should be greater tahn zero.");
		}
		
		if(imageFrozenOverlay.getFrozenMessage() == null || imageFrozenOverlay.getFrozenMessage().length() == 0)
		{
			setErrorMessage("The Frozen Message is empty !");
		}		
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}

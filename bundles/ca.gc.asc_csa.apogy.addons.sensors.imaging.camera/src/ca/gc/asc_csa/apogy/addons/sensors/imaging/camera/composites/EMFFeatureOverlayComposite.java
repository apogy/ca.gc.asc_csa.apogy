/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EMFFeatureOverlay;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.VariableFeatureReferenceComposite;

public class EMFFeatureOverlayComposite extends Composite 
{
	private EMFFeatureOverlay emfFeatureOverlay;
	
	private AbstractTextOverlayComposite abstractTextOverlayComposite;
	private Text numberFormatTxt;	
	private VariableFeatureReferenceComposite variableFeatureReferenceComposite; 
	private DrawnCameraOverlayPreviewComposite drawnCameraOverlayPreviewComposite;
	
	private DataBindingContext m_bindingContext;
	
	public EMFFeatureOverlayComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new GridLayout(2, false));
		
		Group grpGeneral = new Group(this, SWT.NONE);
		grpGeneral.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpGeneral.setText("General");
		grpGeneral.setLayout(new GridLayout(2, false));
		
		abstractTextOverlayComposite = new AbstractTextOverlayComposite(grpGeneral, SWT.NONE);				
		abstractTextOverlayComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
	
		Label lblNumberFormat = new Label(grpGeneral, SWT.NONE);
		lblNumberFormat.setText("Number Format :");
		
		numberFormatTxt = new Text(grpGeneral, SWT.BORDER);
		GridData gd_numberFormatTxt = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_numberFormatTxt.minimumWidth = 200;
		gd_numberFormatTxt.widthHint = 200;
		numberFormatTxt.setLayoutData(gd_numberFormatTxt);
		new Label(grpGeneral, SWT.NONE);
		numberFormatTxt.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				newNumberFormatSelected(numberFormatTxt.getText());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {				
			}
		});
		new Label(this, SWT.NONE);
		
		
		variableFeatureReferenceComposite = new VariableFeatureReferenceComposite(this, SWT.BORDER)
		{
			protected void newSelection(ISelection selection) 
			{
				newVariableFeatureReferenceSelected(selection);
			}
		};
		
		GridData gd_variableFeatureReferenceComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_variableFeatureReferenceComposite.heightHint = 200;
		gd_variableFeatureReferenceComposite.minimumHeight = 200;
		gd_variableFeatureReferenceComposite.widthHint = 300;
		gd_variableFeatureReferenceComposite.minimumWidth = 300;
		gd_variableFeatureReferenceComposite.horizontalSpan = 2;
		variableFeatureReferenceComposite.setLayoutData(gd_variableFeatureReferenceComposite);	
		
		// Preview
		Group grpPreview = new Group(this, SWT.NONE);
		grpPreview.setLayout(new GridLayout(1, false));
		grpPreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		grpPreview.setText("Preview");
		
		drawnCameraOverlayPreviewComposite = new DrawnCameraOverlayPreviewComposite(grpPreview, SWT.BORDER);
		GridData gd_imagePreviewComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_imagePreviewComposite.minimumWidth = 160;
		gd_imagePreviewComposite.minimumHeight = 375;
		drawnCameraOverlayPreviewComposite.setLayoutData(gd_imagePreviewComposite);		
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public EMFFeatureOverlay getEmfFeatureOverlay() 
	{
		return emfFeatureOverlay;
	}

	public void setEmfFeatureOverlay(EMFFeatureOverlay emfFeatureOverlay) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.emfFeatureOverlay = emfFeatureOverlay;
		
		if(emfFeatureOverlay != null)
		{
			abstractTextOverlayComposite.setAbstractTextOverlay(emfFeatureOverlay);
			drawnCameraOverlayPreviewComposite.setDrawnCameraOverlay(emfFeatureOverlay);
			m_bindingContext = initDataBindingsCustom();						
		}
	}
	
	protected void newVariableFeatureReferenceSelected(ISelection selection)
	{	
	}
	
	protected void newNumberFormatSelected(String numberFormat)
	{	
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
		variableFeatureReferenceComposite.set(ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment().getVariablesList(), emfFeatureOverlay.getVariableFeatureReference());
		
		/* Name Value. */
		IObservableValue<Double> observeNumberFormat = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(emfFeatureOverlay), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.EMF_FEATURE_OVERLAY__NUMBER_FORMAT)).observe(emfFeatureOverlay);
		IObservableValue<String> observeNumberFormatText = WidgetProperties.text(SWT.Modify).observe(numberFormatTxt);

		bindingContext.bindValue(observeNumberFormatText,
								observeNumberFormat, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
	
		return bindingContext;
	}
}

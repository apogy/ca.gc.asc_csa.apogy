/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ContrastAndBrightnessFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.ContrastAndBrightnessFilterComposite;

public class ContrastAndBrightnessFilterWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.ContrastAndBrightnessFilterWizardPage";
		
	private ContrastAndBrightnessFilter contrastAndBrightnessFilter;
	private ContrastAndBrightnessFilterComposite contrastAndBrightnessFilterComposite;
		
	private DataBindingContext m_bindingContext;
	
	public ContrastAndBrightnessFilterWizardPage(ContrastAndBrightnessFilter contrastAndBrightnessFilter) 
	{
		super(WIZARD_PAGE_ID);
		this.contrastAndBrightnessFilter = contrastAndBrightnessFilter;
			
		setTitle("Contrast and Brightness Filter.");
		setDescription("Select filter settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		contrastAndBrightnessFilterComposite = new ContrastAndBrightnessFilterComposite(top, SWT.NONE)
		{
			@Override
			protected void newContrastSelected(double contrast)
			{
				validate();
			}
			@Override
			protected void newBrightnessSelected(double brightness)
			{
				validate();
			}
		};
		contrastAndBrightnessFilterComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		contrastAndBrightnessFilterComposite.setContrastAndBrightnessFilter(contrastAndBrightnessFilter);
				
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);		
						
		if(contrastAndBrightnessFilter.getBrightness() < 0)
		{
			setErrorMessage("Invalid brightness specified ! Must be greater than zero.");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}

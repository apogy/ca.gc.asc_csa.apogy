/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.ImageSnapshot;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.DrawnCameraOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.internal.AbstractCameraStub;
import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.images.ui.composites.ImageDisplayComposite;

public class DrawnCameraOverlayPreviewComposite extends Composite 
{
	private DrawnCameraOverlay drawnCameraOverlay;
	
	private Adapter drawnCameraOverlayAdapter;
	
	private AbstractCameraStub abstractCameraStub = new AbstractCameraStub("platform:/plugin/ca.gc.asc_csa.apogy.addons.sensors.imaging.camera/images/unfiltered_image.jpg", Math.toRadians(45), Math.toRadians(25.3125));
	
	private ImageDisplayComposite imagePreviewComposite; 
	
	public DrawnCameraOverlayPreviewComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new FillLayout());
						
		imagePreviewComposite = new ImageDisplayComposite(this, SWT.NONE);	
	}

	public DrawnCameraOverlay getDrawnCameraOverlay() 
	{
		return drawnCameraOverlay;
	}

	public void setDrawnCameraOverlay(DrawnCameraOverlay drawnCameraOverlay) 
	{
		if(this.drawnCameraOverlay != null)
		{
			this.drawnCameraOverlay.eAdapters().remove(getDrawnCameraOverlayAdapter());
		}
		
		this.drawnCameraOverlay = drawnCameraOverlay;
		
		if(this.drawnCameraOverlay != null)
		{
			this.drawnCameraOverlay.eAdapters().add(getDrawnCameraOverlayAdapter());
			updatePreview();
		}
	}	
	
	protected void updatePreview()
	{	
		if(!isDisposed())
		{
			getDisplay().asyncExec(new Runnable() 
			{			
				@Override
				public void run() 
				{	
					try
					{
						if(getDrawnCameraOverlay() != null)
						{
							abstractCameraStub.setName("Test Camera");
							abstractCameraStub.setDescription("A test camera.");
							
							ImageSnapshot imageSnapshot = abstractCameraStub.takeSnapshot();
							AbstractEImage abstractEImage = imageSnapshot.getImage();												
							AbstractEImage resultImage = getDrawnCameraOverlay().applyOverlay(abstractCameraStub, abstractEImage, getDrawnCameraOverlay().getOverlayAlignment(), abstractEImage.getWidth(), abstractEImage.getHeight());
							
							ImageData imageData = EImagesUtilities.INSTANCE.convertToImageData(resultImage.asBufferedImage());
							
							if(imagePreviewComposite != null && !imagePreviewComposite.isDisposed() && imageData != null)
							{			
								imagePreviewComposite.setImageData(imageData);
								imagePreviewComposite.fitImage();
							}	
						}
						else
						{
							// TODO
						}										
					}
					catch (Exception e) 
					{
						e.printStackTrace();
					}									
				}
			});	
		}
	}

	private Adapter getDrawnCameraOverlayAdapter() 
	{
		if(drawnCameraOverlayAdapter == null)
		{
			drawnCameraOverlayAdapter = new EContentAdapter()
			{
				@Override
				public void notifyChanged(Notification notification) 
				{
					updatePreview();
					super.notifyChanged(notification);
				}
			};
		}
		return drawnCameraOverlayAdapter;
	}
	
	
	
}

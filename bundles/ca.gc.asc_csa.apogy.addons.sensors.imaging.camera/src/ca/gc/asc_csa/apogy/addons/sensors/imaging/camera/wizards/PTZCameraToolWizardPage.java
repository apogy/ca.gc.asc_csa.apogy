/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PTZCameraTool;
import ca.gc.asc_csa.apogy.common.ui.composites.Color3fComposite;

public class PTZCameraToolWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.PTZCameraToolWizardPage";
		
	private PTZCameraTool ptzCameraTool;		
	
	private Color3fComposite selectionBoxColorComposite;
	
	private DataBindingContext m_bindingContext;
	
	public PTZCameraToolWizardPage(PTZCameraTool ptzCameraTool) 
	{
		super(WIZARD_PAGE_ID);
		this.ptzCameraTool = ptzCameraTool;
			
		setTitle("Pan Tilt Zoom (PTZ) Camera Tool Overlay.");
		setDescription("Select the Selection Box Color.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(2, false));		
		
		Label lblNegativeValuesColor = new Label(top, SWT.NONE);
		lblNegativeValuesColor.setText("Selection Box Color:");
		
		selectionBoxColorComposite = new Color3fComposite(top, SWT.NONE);		
		selectionBoxColorComposite.setFeature(ApogyAddonsSensorsImagingCameraPackage.Literals.PTZ_CAMERA_TOOL__SELECTION_BOX_COLOR);	
		selectionBoxColorComposite.setOwner(ptzCameraTool);
		
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);										
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}

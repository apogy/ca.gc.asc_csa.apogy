/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraImageAnnotation;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraTool;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfiguration;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewUtilities;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.impl.FeaturePathAdapterImpl;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class CameraImageAnnotationListComposite extends Composite {

	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
			ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	private TreeViewer treeViewer;
	private Composite emfFormsComposite;
	private Button btnNew;
	private Button btnDelete;

	private Button btnVisible;

	private CameraViewConfiguration cameraViewConfiguration;
	private EStructuralFeature feature;
	private EStructuralFeature listFeature;
	private List<FeaturePathAdapter> featurePathAdapters;

	public CameraImageAnnotationListComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(4, false));
		this.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		this.setBackgroundMode(SWT.INHERIT_FORCE);

		treeViewer = new TreeViewer(this);
		treeViewer.setLabelProvider(getLabelProvider());
		treeViewer.setContentProvider(getContentProvider());
		treeViewer.getTree().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				CameraImageAnnotationListComposite.this.newSelection();
				updateEMFForms();

				if (btnVisible != null) {
					btnVisible.setVisible(getSelectedCameraImageAnnotation() != null);
				}
			}

		});

		Composite buttonComposite = new Composite(this, SWT.None);
		buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 2));
		buttonComposite.setLayout(new GridLayout(1, false));

		btnNew = new Button(buttonComposite, SWT.None);
		btnNew.setText("New");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				/** NamedSettings */
				NamedSetting namedSettings = ApogyCommonEMFUIFactory.eINSTANCE.createNamedSetting();
				namedSettings.setParent((EObject) getCameraViewConfiguration().eGet(feature));
				namedSettings.setContainingFeature((EReference) listFeature);

				ApogyEObjectWizard wizard = new ApogyEObjectWizard((EReference) listFeature,
						(EObject) getCameraViewConfiguration().eGet(feature), namedSettings, null);
				WizardDialog dialog = new WizardDialog(getShell(), wizard);
				dialog.open();

				/** Initialize the Camera of the CameraTool just created. */
				if (wizard.getCreatedEObject() instanceof CameraTool) {
					CameraTool cameraTool = (CameraTool) wizard.getCreatedEObject();
					cameraTool.initializeCamera(getCameraViewConfiguration().getCamera());
				}

				treeViewer.refresh();
				updateEMFForms();
			}
		});

		btnDelete = new Button(buttonComposite, SWT.None);
		btnDelete.setText("Delete");
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (getSelectedCameraImageAnnotation() != null) {
					CameraImageAnnotation cameraImageAnnotation = getSelectedCameraImageAnnotation();
					if (cameraImageAnnotation instanceof CameraTool) {
						CameraTool cameraTool = (CameraTool) cameraImageAnnotation;
						CameraViewUtilities.INSTANCE.removeCameraTool(getCameraViewConfiguration(), cameraTool);
					} else {

						ApogyCommonTransactionFacade.INSTANCE.basicRemove(
								(EObject) getCameraViewConfiguration().eGet(feature), listFeature,
								getSelectedCameraImageAnnotation());

					}
				}
			}
		});

		Label separator = new Label(this, SWT.SEPARATOR);
		separator.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 2));

		Composite toggleVisibleComposite = new Composite(this, SWT.None);
		toggleVisibleComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		toggleVisibleComposite.setLayout(new GridLayout(1, false));

		btnVisible = new Button(toggleVisibleComposite, SWT.None);
		btnVisible.setText("Toggle visible");
		btnVisible.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1));
		btnVisible.setVisible(false);
		btnVisible.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (getSelectedCameraImageAnnotation() != null) {
					ApogyCommonTransactionFacade.INSTANCE.basicSet(getSelectedCameraImageAnnotation(),
							ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_IMAGE_ANNOTATION__VISIBLE,
							!getSelectedCameraImageAnnotation().isVisible());
				}
			}
		});

		ScrolledComposite scrolledComposite = new ScrolledComposite(this, SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		emfFormsComposite = new Composite(scrolledComposite, SWT.None);
		scrolledComposite.setContent(emfFormsComposite);
		scrolledComposite.setMinSize(emfFormsComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		updateEMFForms();

		// Dispose
		addDisposeListener(new DisposeListener() 
		{
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (cameraViewConfiguration != null) 
				{
					for (FeaturePathAdapter adapter : getFeaturePathAdapter()) 
					{
						adapter.dispose();
					}
				}

				CameraImageAnnotationListComposite.this.newSelection();
				updateEMFForms();
				if (btnVisible != null) 
				{
					btnVisible.setVisible(getSelectedCameraImageAnnotation() != null);
				}
			}
		});

	}

	/**
	 * Updates the EMFForms displaying the image annotation selected.
	 */
	private void updateEMFForms() {
		for (Control control : emfFormsComposite.getChildren()) {
			control.dispose();
		}

		ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(emfFormsComposite, getSelectedCameraImageAnnotation(),
				"No compatible selection");

		((ScrolledComposite) emfFormsComposite.getParent())
				.setMinSize(emfFormsComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	protected void newSelection() {

	}

	public CameraImageAnnotation getSelectedCameraImageAnnotation() {
		if (treeViewer.getStructuredSelection() != null
				&& treeViewer.getStructuredSelection().getFirstElement() != null) {
			Object object = treeViewer.getStructuredSelection().getFirstElement();

			if (object instanceof CameraImageAnnotation) {
				return (CameraImageAnnotation) object;
			}
		}
		return null;
	}

	public CameraViewConfiguration getCameraViewConfiguration() {
		return cameraViewConfiguration;
	}

	public void setCameraViewConfiguration(CameraViewConfiguration cameraViewConfiguration,
			EStructuralFeature feature) {
		if (this.cameraViewConfiguration != null) {
			for (FeaturePathAdapter adapter : getFeaturePathAdapter()) {
				adapter.dispose();
			}
		}
		this.cameraViewConfiguration = cameraViewConfiguration;
		this.feature = feature;

		if (this.cameraViewConfiguration != null && this.feature != null) {
			EList<EReference> eRefs = ApogyCommonEMFFacade.INSTANCE
					.getSettableEReferences((EObject) this.cameraViewConfiguration.eGet(feature));
			for (EReference reference : eRefs) {
				if (reference.isMany()) {
					this.listFeature = reference;
				}
			}

			treeViewer.setInput(this.cameraViewConfiguration.eGet(this.feature));

			for (FeaturePathAdapter adapter : getFeaturePathAdapter()) {
				adapter.init(this.cameraViewConfiguration);
			}
			btnNew.setEnabled(true);
			btnDelete.setEnabled(true);
		} else {
			treeViewer.setInput(null);
			btnNew.setEnabled(false);
			btnDelete.setEnabled(false);
		}
	}

	private List<FeaturePathAdapter> getFeaturePathAdapter() {
		if (this.featurePathAdapters == null) {
			this.featurePathAdapters = new ArrayList<FeaturePathAdapter>();

			this.featurePathAdapters.add(new FeaturePathAdapterImpl() {

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> list = new ArrayList<EStructuralFeature>();

					list.add(CameraImageAnnotationListComposite.this.feature);
					list.add(CameraImageAnnotationListComposite.this.listFeature);
					list.add(ApogyCommonEMFPackage.Literals.NAMED__NAME);

					return list;
				}

				@Override
				public void notifyChanged(Notification msg) {
					treeViewer.refresh();
				}
			});
			this.featurePathAdapters.add(new FeaturePathAdapterImpl() {

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> list = new ArrayList<EStructuralFeature>();

					list.add(CameraImageAnnotationListComposite.this.feature);
					list.add(CameraImageAnnotationListComposite.this.listFeature);
					list.add(ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_IMAGE_ANNOTATION__VISIBLE);

					return list;
				}

				@Override
				public void notifyChanged(Notification msg) {
					treeViewer.refresh();
				}
			});
		}
		return featurePathAdapters;
	}

	@SuppressWarnings("unchecked")
	protected AdapterFactoryContentProvider getContentProvider() {
		return new AdapterFactoryContentProvider(adapterFactory) {
			@Override
			public Object[] getElements(Object object) {
				if (cameraViewConfiguration != null
						&& cameraViewConfiguration.eGet(CameraImageAnnotationListComposite.this.feature) != null
						&& ((EObject) cameraViewConfiguration.eGet(feature)).eGet(listFeature) != null) {

					return ((EList<CameraImageAnnotation>) ((EObject) cameraViewConfiguration.eGet(feature))
							.eGet(listFeature)).toArray();
				}
				return null;
			}

			@Override
			public Object[] getChildren(Object object) {
				return null;
			}
		};
	}

	protected AdapterFactoryLabelProvider getLabelProvider() {
		return new AdapterFactoryLabelProvider(adapterFactory) {
			@Override
			public String getText(Object object) {
				if (object instanceof CameraImageAnnotation) {
					CameraImageAnnotation annotation = ((CameraImageAnnotation) object);
					String label = annotation.getName();

					if (annotation.isVisible()) {
						label += " <visible>";
					}
					return label;
				}
				return "";
			}

			@Override
			public Image getImage(Object object) {
				return super.getImage(object);
			}
		};
	}

}

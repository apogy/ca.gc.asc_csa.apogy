/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ExposureFilter;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class ExposureFilterComposite extends Composite 
{
	private ExposureFilter exposureFilter;
	
	private ImageFilterComposite imageFilterComposite;
	private Text exposureText;
	
	private DataBindingContext m_bindingContext;
	
	public ExposureFilterComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Group grpSettings = new Group(this, SWT.NONE);
		grpSettings.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpSettings.setText("Settings");
		grpSettings.setLayout(new GridLayout(2, false));
		
		Label lblContrast = new Label(grpSettings, SWT.NONE);
		lblContrast.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblContrast.setText("Exposure :");
		
		exposureText = new Text(grpSettings, SWT.BORDER);
		exposureText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		exposureText.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) 
			{				
				try
				{
					newExposureSelected(Double.parseDouble(exposureText.getText()));
				}
				catch (Exception ex) 
				{					
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{				
				try
				{
					newExposureSelected(Double.parseDouble(exposureText.getText()));
				}
				catch (Exception ex) 
				{					
				}
			}
		});	
		
		Group grpPreview = new Group(this, SWT.NONE);
		grpPreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpPreview.setText("Preview");
		grpPreview.setLayout(new GridLayout(1, false));
		
		imageFilterComposite = new ImageFilterComposite(grpPreview, SWT.NONE);		
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public ExposureFilter getExposureFilter() 
	{
		return exposureFilter;
	}

	public void setExposureFilter(ExposureFilter exposureFilter) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.exposureFilter = exposureFilter;
		
		if(exposureFilter != null)
		{
			m_bindingContext = customInitDataBindings();
		}
		
		imageFilterComposite.setImageFilter(exposureFilter);
	}	
	
	protected void newExposureSelected(double contrast)
	{	
	}
	
	
	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Contrast Value. */
		IObservableValue<Double> observeExposure = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(exposureFilter), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.EXPOSURE_FILTER__EXPOSURE)).observe(exposureFilter);
		IObservableValue<String> observeExposureText = WidgetProperties.text(SWT.Modify).observe(exposureText);

		bindingContext.bindValue(observeExposureText,
								 observeExposure, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{						
										double tmp  =  exposureFilter.getExposure();
										try
										{
											tmp  = Double.parseDouble((String) fromObject);										
										}
										catch(Throwable t)
										{											
										}	
										
										return tmp;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
	
		return bindingContext;
	}
}

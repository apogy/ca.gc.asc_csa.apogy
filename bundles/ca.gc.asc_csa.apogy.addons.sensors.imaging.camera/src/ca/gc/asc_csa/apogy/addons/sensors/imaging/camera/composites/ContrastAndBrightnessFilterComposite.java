/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ContrastAndBrightnessFilter;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class ContrastAndBrightnessFilterComposite extends Composite 
{
	private ContrastAndBrightnessFilter contrastAndBrightnessFilter;
	
	private ImageFilterComposite imageFilterComposite;
	private Text contrastText;
	private Text brightnessText;
	
	private DataBindingContext m_bindingContext;
	
	public ContrastAndBrightnessFilterComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Group grpSettings = new Group(this, SWT.NONE);
		grpSettings.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpSettings.setText("Settings");
		grpSettings.setLayout(new GridLayout(4, false));
		
		Label lblContrast = new Label(grpSettings, SWT.NONE);
		lblContrast.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblContrast.setText("Contrast:");
		
		contrastText = new Text(grpSettings, SWT.BORDER);
		contrastText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		contrastText.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				try
				{
					newContrastSelected(Double.parseDouble(contrastText.getText()));
				}
				catch (Exception ex) 
				{					
				}				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				try
				{
					newContrastSelected(Double.parseDouble(contrastText.getText()));
				}
				catch (Exception ex) 
				{					
				}				
			}
		});
	
		Label lblBrightness = new Label(grpSettings, SWT.NONE);
		lblBrightness.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblBrightness.setText("Brightness:");
		
		brightnessText = new Text(grpSettings, SWT.NONE);
		brightnessText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		brightnessText.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyReleased(KeyEvent e) 
			{
				try
				{
					newBrightnessSelected(Double.parseDouble(brightnessText.getText()));
				}
				catch (Exception ex) 
				{					
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{
				try
				{
					newBrightnessSelected(Double.parseDouble(brightnessText.getText()));
				}
				catch (Exception ex) 
				{					
				}
			}
		});
		
		Group grpPreview = new Group(this, SWT.NONE);
		grpPreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpPreview.setText("Preview");
		grpPreview.setLayout(new GridLayout(1, false));
		
		imageFilterComposite = new ImageFilterComposite(grpPreview, SWT.NONE);	
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public ContrastAndBrightnessFilter getContrastAndBrightnessFilter() 
	{
		return contrastAndBrightnessFilter;
	}

	public void setContrastAndBrightnessFilter(ContrastAndBrightnessFilter contrastAndBrightnessFilter) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.contrastAndBrightnessFilter = contrastAndBrightnessFilter;
		
		if(contrastAndBrightnessFilter != null)
		{
			m_bindingContext = customInitDataBindings();
		}
		
		imageFilterComposite.setImageFilter(contrastAndBrightnessFilter);
	}	
	
	protected void newContrastSelected(double contrast)
	{
		
	}
	
	protected void newBrightnessSelected(double brightness)
	{
		
	}
	
	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Contrast Value. */
		IObservableValue<Double> observeContrast = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(contrastAndBrightnessFilter), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.CONTRAST_AND_BRIGHTNESS_FILTER__CONTRAST)).observe(contrastAndBrightnessFilter);
		IObservableValue<String> observeContrastText = WidgetProperties.text(SWT.Modify).observe(contrastText);

		bindingContext.bindValue(observeContrastText,
								 observeContrast, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{				
										double tmp = contrastAndBrightnessFilter.getContrast();
										try
										{
											tmp = Double.parseDouble((String) fromObject); 
										}
										catch (Throwable t) 
										{
										}
										
										return tmp;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		/* Brightness Value. */
		IObservableValue<Double> observeBrightness = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(contrastAndBrightnessFilter), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.CONTRAST_AND_BRIGHTNESS_FILTER__BRIGHTNESS)).observe(contrastAndBrightnessFilter);
		IObservableValue<String> observeBrightnessText = WidgetProperties.text(SWT.Modify).observe(brightnessText);

		bindingContext.bindValue(observeBrightnessText,
								 observeBrightness, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{		
										double tmp = contrastAndBrightnessFilter.getBrightness();
										try
										{
											tmp = Double.parseDouble((String) fromObject); 
										}
										catch (Throwable t) 
										{
										}
										
										return tmp;										
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		return bindingContext;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.CameraViewComposite;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.parts.CameraViewPart;

public class SetImageAutoSaveHandler {

	@CanExecute
	public boolean canExecute(MPart part){
		if (part.getObject() instanceof CameraViewPart){
			CameraViewPart viewPart = (CameraViewPart) part.getObject();
			if(viewPart.getActualComposite() instanceof CameraViewComposite){
				CameraViewComposite composite = (CameraViewComposite)viewPart.getActualComposite();
				return composite.getSelectedCameraViewConfiguration() != null;
			}
		}
		return false;
	}
	
	@Execute
	public void execute(MPart part, final MToolItem item){
		if (part.getObject() instanceof CameraViewPart){
			CameraViewPart viewPart = (CameraViewPart) part.getObject();
			viewPart.setImageAutoSaveEnable(item.isSelected());
		}
	}
}
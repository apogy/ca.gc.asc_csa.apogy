/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards;

import java.text.DecimalFormat;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.EMFFeatureOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.EMFFeatureOverlayComposite;

public class EMFFeatureOverlayWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.EMFFeatureOverlayWizardPage";
		
	private EMFFeatureOverlay emfFeatureOverlay;
	private EMFFeatureOverlayComposite emfFeatureOverlayComposite;
		
	private DataBindingContext m_bindingContext;
	
	public EMFFeatureOverlayWizardPage(EMFFeatureOverlay emfFeatureOverlay) 
	{
		super(WIZARD_PAGE_ID);
		this.emfFeatureOverlay = emfFeatureOverlay;
			
		setTitle("EMF Feature Overlay.");
		setDescription("Select the feature to display.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		emfFeatureOverlayComposite = new EMFFeatureOverlayComposite(top, SWT.BORDER)
		{
			@Override
			protected void newVariableFeatureReferenceSelected(ISelection selection) 
			{
				validate();
			}
			
			@Override
			protected void newNumberFormatSelected(String numberFormat) 
			{
				validate();
			}
		};
		emfFeatureOverlayComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		emfFeatureOverlayComposite.setEmfFeatureOverlay(emfFeatureOverlay);
				
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);		
						
		if(emfFeatureOverlay.getVariableFeatureReference().getVariable() == null)
		{
			setErrorMessage("No variable selected !");
		}
		
		if(emfFeatureOverlay.getNumberFormat() == null)
		{
			setErrorMessage("Number format is not defined !");
		}
		else
		{
			try
			{
				DecimalFormat format = new DecimalFormat(emfFeatureOverlay.getNumberFormat());
				format.format(1.234);
			}
			catch (Throwable t) 
			{
				setErrorMessage("Invalid number format specified !");
			}
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageCountOverlay;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class ImageCountOverlayComposite extends Composite 
{
	private ImageCountOverlay imageCountOverlay;
	
	private Button indicatorVisibleBtn;
	private AbstractTextOverlayComposite abstractTextOverlayComposite;
	private DrawnCameraOverlayPreviewComposite drawnCameraOverlayPreviewComposite;
	
	private DataBindingContext m_bindingContext;
	
	public ImageCountOverlayComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new GridLayout(1, false));
		
		Group grpGeneral = new Group(this, SWT.NONE);
		grpGeneral.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpGeneral.setText("General");
		grpGeneral.setLayout(new GridLayout(2, false));
		
		abstractTextOverlayComposite = new AbstractTextOverlayComposite(grpGeneral, SWT.NONE);				
		abstractTextOverlayComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		
		Group grpFrozen = new Group(this, SWT.NONE);
		grpFrozen.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpFrozen.setText("Count Settings");
		grpFrozen.setLayout(new GridLayout(2, false));
		
		Label lblNumberFormat = new Label(grpFrozen, SWT.NONE);
		lblNumberFormat.setText("Indicator Visible:");
		
		indicatorVisibleBtn = new Button(grpFrozen, SWT.CHECK);		
		
		// Preview
		Group grpPreview = new Group(this, SWT.NONE);
		grpPreview.setLayout(new GridLayout(1, false));
		grpPreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		grpPreview.setText("Preview");
		
		drawnCameraOverlayPreviewComposite = new DrawnCameraOverlayPreviewComposite(grpPreview, SWT.BORDER);
		GridData gd_imagePreviewComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_imagePreviewComposite.minimumWidth = 160;
		gd_imagePreviewComposite.minimumHeight = 320;
		drawnCameraOverlayPreviewComposite.setLayoutData(gd_imagePreviewComposite);	
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public ImageCountOverlay getImageCountOverlay() 
	{
		return imageCountOverlay;
	}

	public void setImageCountOverlay(ImageCountOverlay imageCountOverlay) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.imageCountOverlay = imageCountOverlay;
		
		if(imageCountOverlay != null)
		{
			abstractTextOverlayComposite.setAbstractTextOverlay(imageCountOverlay);
			drawnCameraOverlayPreviewComposite.setDrawnCameraOverlay(imageCountOverlay);
			m_bindingContext = initDataBindingsCustom();
		}
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
				
		/* Name Value. */
		IObservableValue<Boolean> observeIndicatorVisible = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(imageCountOverlay), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.IMAGE_COUNT_OVERLAY__INDICATOR_VISIBLE)).observe(imageCountOverlay);
	
		IObservableValue<String> observeIndicatorVisibleButtton = WidgetProperties.selection().observe(indicatorVisibleBtn);

		bindingContext.bindValue(observeIndicatorVisibleButtton,
								observeIndicatorVisible, 
								new UpdateValueStrategy(), 
								new UpdateValueStrategy());
	
		return bindingContext;
	}
}

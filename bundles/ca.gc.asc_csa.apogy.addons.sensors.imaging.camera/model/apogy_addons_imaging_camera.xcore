// *****************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
//     Pierre Allard - initial API and implementation
//     Regent L'Archeveque
//     Sebastien Gemme
//        
// SPDX-License-Identifier: EPL-1.0
// *****************************************************************************
@GenModel(prefix="ApogyAddonsSensorsImagingCamera",
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  multipleEditorPages="false",
copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Pierre Allard - initial API and implementation
     Regent L'Archeveque
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",
		  modelName="ApogyAddonsSensorsImagingCamera",
		  complianceLevel="6.0",
		  suppressGenModelAnnotations="false",
		  dynamicTemplates="true", 
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.addons.sensors.imaging.camera/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.edit/src-generated")

package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera

import ca.gc.asc_csa.apogy.addons.AbstractTool
import ca.gc.asc_csa.apogy.addons.sensors.imaging.AbstractCamera
import ca.gc.asc_csa.apogy.addons.sensors.imaging.AzimuthDirection
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ElevationDirection
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ImageSnapshot
import ca.gc.asc_csa.apogy.common.emf.Described
import ca.gc.asc_csa.apogy.common.emf.Named
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider
import ca.gc.asc_csa.apogy.common.images.AbstractEImage
import ca.gc.asc_csa.apogy.core.AbsolutePoseProvider
import ca.gc.asc_csa.apogy.core.invocator.AbstractToolsListContainer
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference

// Types

type Color3f wraps javax.vecmath.Color3f
type List<T> wraps java.util.List
type Point2d wraps javax.vecmath.Point2d

/**
 * Overlay alignment position.
 */
enum OverlayAlignment
{	
	CENTER as "Center" = 1,
	LOWER_LEFT_CORNER as "Lower Left" = 2,
	UPPER_LEFT_CORNER as "Upper Left" = 3,
	LOWER_RIGHT_CORNER as "Lower Right" = 4,
	UPPER_RIGHT_CORNER as "Upper Right" = 5
}

/**
 * Utilities class for camera views.
 */
class CameraViewUtilities
{
	/**
	 * Return the identifier associated with a given CameraViewConfiguration.
	 * @param cameraViewConfiguration The given CameraViewConfiguration.
	 * @return The identifier, null if none is found.
	 */
	op String getCameraViewConfigurationIdentifier(CameraViewConfiguration cameraViewConfiguration)
	
	/**
	 * Return the CameraViewConfiguration (in the Active Session) with the specified identifier.
	 * @param identifier The CameraViewConfiguration identifier.
	 * @return The CameraViewConfiguration with the specified identifier, null if non is found.
	 */
	op CameraViewConfiguration getActiveCameraViewConfiguration(String identifier)
	
	/**
	 * Return the CameraViewConfigurationList in the Active Session.
	 * @return The CameraViewConfigurationList in the Active Session, null if none is found.
	 */
	op CameraViewConfigurationList getActiveCameraViewConfigurationList()	
	
	/**
	 * Adds a Camera Tool from a given Camera View Configuration.
	 * @param cameraViewConfiguration The CameraViewConfiguration to which to add the CameraTool.
	 * @param cameraTool The camera Tool to add.
	 */
	op void addCameraTool(CameraViewConfiguration cameraViewConfiguration, CameraTool cameraTool)
	
	/**
	 * Removes cleanly a Camera Tool from a given Camera View Configuration.
	 * @param cameraViewConfiguration The CameraViewConfiguration from which to remove the CameraTool.
	 * @param cameraTool The camera Tool to remove.
	 */
	op void removeCameraTool(CameraViewConfiguration cameraViewConfiguration, CameraTool cameraTool)
}

/**
 * A list of CameraViewConfigurations.
 */
class CameraViewConfigurationList extends AbstractToolsListContainer
{
	/**
	 * The list of CameraViewConfiguration.
	 */
	@GenModel(property="None")	
	contains CameraViewConfiguration[0..*] cameraViewConfigurations opposite cameraViewConfigurationList
}

/**
 * The image rotations available.
 */
enum CameraViewDisplayRotation
{	
	NONE as "None" = 0,
	CW_90_DEG as "Clockwise 90 deg" = 1,
	CCW_90_DEG as "Counter Clockwise 90 deg" = 2,
	CW_180_DEG as "Clockwise 180 deg" = 3	
}

/**
 * Defines a configuration of to be used when displaying a camera imagery. This includes the camera, overlays, filters and tools. 
 */
class CameraViewConfiguration extends VariableFeatureReference, AbstractTool
{			
	/**
	 * The display rotation to use.
	 */
	CameraViewDisplayRotation cameraViewDisplayRotation = "None"
	
	/**
	 *  The  CameraViewConfiguration containing this CameraViewConfiguration.
	 */	
    @GenModel(property="None")
	refers CameraViewConfigurationList cameraViewConfigurationList opposite cameraViewConfigurations
	
	/**
	 * The Camera being displayed.
	 */	
	@GenModel(property="Readonly")
	refers transient AbstractCamera camera
	
	/**
	 * Width of the image being displayed, in pixels.
	 */
	@GenModel(apogy_units = "pixel", propertyCategory="IMAGE_DISPLAY_SETTINGS")	
	int imageWidth = "640"
	
	/**
	 * Height of the image being displayed, in pixels.
	 */
	@GenModel(apogy_units = "pixel",propertyCategory="IMAGE_DISPLAY_SETTINGS")
	int imageHeight = "480"
	
	/**
	 * Number of image received.
	 */
	transient int imageCount = "0"
	
	/**
	 * Whether or not to display the rectified image is available.
	 */
	@GenModel(propertyCategory="IMAGE_DISPLAY_SETTINGS")
	boolean displayRectifiedImage = "true"
	
	/**
	 * The list of overlays shown onto the camera image.
	 */	
	contains CameraOverlayList[1] overlayList opposite cameraViewConfiguration
	
	/**
	 * The list of tools used with the camera.
	 */
	contains CameraToolList[1] toolList opposite cameraViewConfiguration
	
	/**
	 * The list of image filters to apply to the camera image.
	 */
	contains FilterList[1] filterList opposite cameraViewConfiguration
	
	/**
	 * Whether or not to automatically save the image received.
	 */
	@GenModel(propertyCategory="IMAGE_AUTOSAVE_SETTINGS")	
	boolean imageAutoSaveEnable = "false"
	
	/**
	 * If auto saving image, whether or not to save the image with overlays.
	 */
	@GenModel(propertyCategory="IMAGE_AUTOSAVE_SETTINGS")
	boolean saveImageWithOverlays = "false"
	
	/**
	 * Path of the folder where to save images.
	 */
	@GenModel(propertyCategory="IMAGE_AUTOSAVE_SETTINGS")
	String imageAutoSaveFolderPath
	
	/** 
	 * Returns the list of all CameraImageAnnotation.
	 * @return The list of CameraImageAnnotation.
	 */
	op List<CameraImageAnnotation> getCameraImageAnnotations()
}

/**
 * Class implementing a reference to a CameraViewConfiguration.
 */
class CameraViewConfigurationReference
{
	/**
	 * The CameraViewConfiguration refered to.
	 */
	refers CameraViewConfiguration cameraViewConfiguration
}

/* -------------------------------------------------------------------------
 * 
 * Filters.
 * 
 * ------------------------------------------------------------------------- */ 

/**
 * A list of Image Filter.
 */
class FilterList
{
	/** 
	 * The CameraViewConfiguration containing this FilterList.
	 */
	@GenModel(property="None")
	refers CameraViewConfiguration cameraViewConfiguration opposite filterList
	
	/**
	 * The list of image filters.
	 */
	contains ImageFilter[0..*] imageFilters
}

/**
 * Base class for all image filters.
 */
abstract class ImageFilter extends Named, Described
{
	/**
	 * Whether or not the filter is enabled.
	 */
	@GenModel(propertyCategory="FILTER_SETTING")
	boolean enabled = "true"
	
	/**
	 * Creates a filtered image using an image from a specified camera.
	 * @param camera The camera that produced the image.
	 * @param cameraImage The image to filter.
	 * @return The filtered image.
	 */
	op AbstractEImage filter(AbstractCamera camera, AbstractEImage cameraImage)
	
	/**
	 * Method called to dispose of the ImageFilter. This method should be overridden by sub-classes
	 * to release resources, unregister listeners, etc. 
	 */
	op void dispose() 
}

/**
 * An Image Filter which converts an image to grayscale using the NTSC brightness calculation.
 */
class GrayScaleFilter extends ImageFilter
{	
}

/**
 * An image Filter that implements edge-detection.
 */
class EdgeFilter extends ImageFilter
{	
}

/**
 * An Image Filter to change the brightness and contrast of an image.
 */
class ContrastAndBrightnessFilter extends ImageFilter
{
	/**
	 * The contrast setting.
	 */
	@GenModel(propertyCategory="FILTER_SETTING")	
	double contrast = "1.0"	
	
	/**
	 * The brightness setting.
	 */
	@GenModel(propertyCategory="FILTER_SETTING")
	double brightness = "1.0"
}

/**
 * An Image Filter which changes the exposure of an image.
 */
class ExposureFilter extends ImageFilter
{
	/**
	 * The exposure setting.
	 */
	@GenModel(propertyCategory="FILTER_SETTING")	
	double exposure = "2.5"
}

/**
 * An Image Filter which changes the gain and bias of an image.
 */
class GainFilter extends ImageFilter
{
	/**
	 * The gain setting.
	 */
	@GenModel(propertyCategory="FILTER_SETTING")	
	double gain = "50"
	
	/**
	 * The bias setting.
	 */
	@GenModel(propertyCategory="FILTER_SETTING")
	double bias = "50"
}

/**
 * An Image Filter which inverts the RGB channels of an image.
 */
class InvertFilter extends ImageFilter
{
}

/**
 * An Image Filter which simply multiplies pixel values by a given scale factor.
 */
class RescaleFilter extends ImageFilter
{
	/**
	 * The scalling setting.
	 */
	@GenModel(propertyCategory="FILTER_SETTING")	
	double scale = "1.0"
}

/**
 * Base class for annotation over images.
 */
abstract class CameraImageAnnotation extends Named, Described
{	
	/**
	 * Whether or not the camera image annotation is visible.
	 */
	boolean visible = "true"		
	
	/**
	 * Method called to dispose of the CameraImageAnnotation. This method should be overridden by sub-classes
	 * to release resources, unregister listeners, etc. 
	 */
	op void dispose()
}


/* -------------------------------------------------------------------------
 * 
 * Overlays.
 * 
 * ------------------------------------------------------------------------- */ 

/**
 * List of CameraOverlay.
 */
class CameraOverlayList
{
	/**
	 *  The  CameraViewConfiguration containing this CameraOverlayList.
	 */
	@GenModel(property="None")
	refers CameraViewConfiguration cameraViewConfiguration opposite overlayList
	
	/**
	 * The list of overlay shown onto the camera image.
	 */	
	contains CameraOverlay[0..*] overlays opposite cameraOverlayList
}

/**
 * An overlay displayed on top of a camera image.
 */
abstract class CameraOverlay extends CameraImageAnnotation
{			
	/** 
	 * The  CameraOverlayList containing this CameraOverlay.
	 */
	@GenModel(property="None")
	refers CameraOverlayList cameraOverlayList opposite overlays
		
	/**
	 * Where the overlay should be shown.
	 */
	@GenModel(propertyCategory="OVERLAY_POSITION_PROPERTIES")	
	OverlayAlignment overlayAlignment	
}

/** 
 * An overlay that has to be drawn onto a camera image.  
 * Such overlays are responsible for drawing their representation onto the provided image.
 */
abstract class DrawnCameraOverlay extends CameraOverlay
{
	/**
	 * Draws the overlay onto the image provided by the camera. 
	 * @param camera The camera that provided the image.
	 * @param cameraImage The image onto which the overlay must be drawn.
	 * @param overlayAlignment The alignment of the overlay
	 * @param overlayWidth The width of the overlay.
	 * @param overlayHeight The height of the overlay.
	 * @return The camera image with the overlay drawn on top of it.
	 */
	op AbstractEImage applyOverlay(AbstractCamera camera, AbstractEImage cameraImage, OverlayAlignment overlayAlignment, int overlayWidth, int overlayHeight)	
}

/**
 * An overlay that displays text onto a camera image.
 */
abstract class AbstractTextOverlay extends DrawnCameraOverlay
{
	/**
	 * Name of the font to use.
	 */
	@GenModel(propertyCategory="FONT_PROPERTIES")
	String fontName = "SansSerif"
		
	/**
	 * The font size.
	 */
	@GenModel(propertyCategory="FONT_PROPERTIES")
	int fontSize = "10"
	
	/**
	 * The minimum distance of text to image left or right edge, in pixel.
	 */
	@GenModel(apogy_units = "pixel", propertyCategory="OVERLAY_POSITION_PROPERTIES")	
	int horizontalOffset = "10"
	
	/**
	 * The minimum distance of text to image upper or lower edge, in pixel.
	 */
	@GenModel(apogy_units = "pixel", propertyCategory="OVERLAY_POSITION_PROPERTIES")	
	int verticalOffset = "10"
	
	/**
	 * The text color.
	 */
	@GenModel(propertyCategory="FONT_PROPERTIES")
	Color3f textColor = "0.0,1.0,0.0"
	
	/** 
	 * Returns the string that will be displayed in the screen.
	 * Sub-classes should overload this method.
	 * @return The string to display.
	 */
	op String getDisplayedString()
}

/**
 * An overlay that displays the value of an Variable feature as text onto an image.
 */
class EMFFeatureOverlay extends AbstractTextOverlay
{
	/**
	 * The VariableFeatureReference pointing to the value to display.
	 */
	@GenModel(property="None")
	contains VariableFeatureReference[1] variableFeatureReference
	
	/**
	 * The number format to use when displaying a Number.
	 */
	@GenModel(propertyCategory="FONT_PROPERTIES")
	String numberFormat = "0.00"
	
	/**
	 *  Attribute used to trigger an overlay update when changes in the VariableFeatureReference are made.
	 */
	@GenModel(property="None")
	long variableFeatureReferenceChangeCount="0"	
	
	/**
	 * Units to be used for display.
	 */
	String displayUnits
	
	/**
	 * Enables the range checking of the value and changes the background color of the text to reflect the range.
	 */
	boolean enableRangeColoring = "true"
}

/**
 * An overlay used to display the name of the camera.
 */
class CameraNameOverlay extends AbstractTextOverlay
{			
}

/**
 * An overlay used to display the number of images.
 */
class ImageCountOverlay extends AbstractTextOverlay
{
	/**
	 * Whether or not the indicator should be shown.
	 */
	boolean indicatorVisible = "true"
	
	/**
	 * Whether or not the image count should be shown.
	 */
	boolean countVisible = "true"			
}

/**
 * An overlay that displays a message when a specific period without a new image is exceeded.
 */
class ImageFrozenOverlay extends AbstractTextOverlay
{
	/**
	 * Indicates whether or not the image is frozen.
	 */
	@GenModel(propertyCategory="FROZEN_PROPERTIES", property="Readonly")
	boolean frozen = "false"
	
	/**
	 * The expected image update period, in seconds.
	 */
	@GenModel(propertyCategory="FROZEN_PROPERTIES", apogy_units="s")
	double expectedImageUpdatePeriod = "1.0"
	
	/**
	 * The message to display when the image freezes.
	 */
	@GenModel(propertyCategory="FROZEN_PROPERTIES")
	String frozenMessage = "Frozen"
}


/*
 * An overlay that produces a transparent image that will be superimposed 
 * on the camera image by the CameraView.
 */
abstract class ImageCameraOverlay extends CameraOverlay
{
	/**
	 * Returns the overlay image to superimpose on the camera image.
	 * @param overlayWidth The width of the overlay.
	 * @param overlayHeight The height of the overlay.
	 * @return The image (with transparent background) to be superimposed on the camera image.
	 */
	op AbstractEImage getOverlay(AbstractCamera camera, OverlayAlignment overlayAlignment, int overlayWidth, int overlayHeight)	
}

/**
 * An overlay that put an image found at a specified URL onto an image.
 */
class URLImageOverlay extends ImageCameraOverlay
{
	/**
	 * The URL to the file containing the image to overlay.
	 */	
	String url		
	
	/**
	 * The image resize policy.
	 */	
	ImageSizePolicy imageSizePolicy
	
	/**
	 * The Image read from the url.
	 */
	@GenModel(child="true", property="Readonly")
	refers transient AbstractEImage image		
}

/**
 * Image sizing policy.
 */
enum ImageSizePolicy
{	
	FIXED_SIZE as "Fixed Size" = 1,
	ALLOW_RESIZE_VERTICAL as "Allow Resize Vertical" = 2,
	ALLOW_RESIZE_HORIZONTAL as "Allow Resize Horizontal" = 3,
	ALLOW_RESIZE_BOTH as "Allow Resize Both" = 4	
}

/**
 * An overlay that displays the Apogy Logo.
 */
class ApogyLogoOverlay extends ImageCameraOverlay
{	
}

/**
 * An overlay that displays a graduated cross-hair representing the horizontal and vertical Field Of View (FOV) of the camera.
 */
class FOVOverlay extends DrawnCameraOverlay, ToolTipTextProvider
{			
	/**
	 * Width of lines displayed, in pixels.
	 */
	@GenModel(apogy_units = "pixel", propertyCategory="OVERLAY_PROPERTIES")	
	int lineWidth = "2"
	
	/**
	 * The color to be used for drawing positive values.
	 */
	@GenModel(propertyCategory="OVERLAY_PROPERTIES")	
	Color3f positiveValuesColor = "0.0,1.0,0.0"

	/**
	 * The color to be used for drawing positive values.
	 */
	@GenModel(propertyCategory="OVERLAY_PROPERTIES")	
	Color3f negativeValueColor = "1.0,0.0,0.0"

	/* 
	 * The interval, in degrees, for which to display numerics.
	 */
	@GenModel(propertyCategory="OVERLAY_PROPERTIES", apogy_units = "deg")	
	int angleInterval = "5"

	/**
	 *  The name of the font to use.
	 */
	@GenModel(propertyCategory="FONT_PROPERTIES")
	String fontName = "SansSerif"
		
	/**
	 * The font size.
	 */
	@GenModel(propertyCategory="FONT_PROPERTIES")
	int fontSize = "10"
	
	/**
	 * The direction of the positive azimuth (i.e. is left or right the positive direction).
	 */
	@GenModel(propertyCategory="DIRECTIONS_PROPERTIES")
	AzimuthDirection azimuthDirection
	
	/**
	 * The direction of the positive elevation (i.e. is up or down the positive direction).
	 */
	@GenModel(propertyCategory="DIRECTIONS_PROPERTIES")
	ElevationDirection elevationDirection			
}

/**
 * An overlay that displays a graduated cross-hair representing absolute horizontal and vertical orientation. 
 */
class AzimuthElevationFOVOverlay extends FOVOverlay
{
	/**
	 * Change the azimuth of the center of the image.
	 * @param azimuth The azimuth of the center of the image, in radians.
	 */
	op void changeAzimuth(@GenModel(apogy_units="rad") double azimuth)
	
	/**
	 * Change the elevation of the center of the image.
	 * @param elevation The elevation of the center of the image, in radians.
	 */
	op void changeElevation(@GenModel(apogy_units="rad") double elevation)
}

/**
 * An overlay that displays a graduated cross-hair representing absolute horizontal and vertical orientation. 
 * The values of azimuth and elevation of the center of the cross-hair are driven by the specified VariableFeatureReference.
 */
class EMFFeatureAzimuthElevationFOVOverlay extends AzimuthElevationFOVOverlay
{
	/**
	 * The reference to use for azimuth.
	 */
	@GenModel(property="None", propertyCategory="FEATURE_CATEGORY")
	contains AzimuthFeatureReference[1] azimuthFeatureReference
	
	/**
	 * The reference to use for elevation.
	 */
	@GenModel(property="None", propertyCategory="FEATURE_CATEGORY")
	contains ElevationFeatureReference[1] elevationFeatureReference	
}

/**
 * A specialization of VariableFeatureReference for absolute azimuth reference.
 */
class AzimuthFeatureReference extends VariableFeatureReference
{	
}

/**
 * A specialization of VariableFeatureReference for absolute elevation reference.
 */
class ElevationFeatureReference extends VariableFeatureReference
{	
}

/**
 * A provider of tooltip text to be displayed on top of an image.
 */
class ToolTipTextProvider
{
	/**
	 * Returns the text to display as the result of the user clicking on the image.
	 * @param camera The camera that produced the image.
	 * @param imageSnapshot The image snaphot displayed.
	 * @param mouseButton The mouse button clicked.
	 * @param x The position (from left to right) of the click of the mouse, in pixels.
	 * @param y The position (from up to down) of the click of the mouse, in pixels.
	 * @return The string to display, can be null.
	 */
	op String getToolTipText(AbstractCamera camera, 
							 ImageSnapshot imageSnapshot, 
							 int mouseButton, 
							 @GenModel(apogy_units = "pixel") int x, 
							 @GenModel(apogy_units = "pixel") int y)
}

/**
 * Base class for camera tools.
 */
abstract class CameraTool extends CameraImageAnnotation
{
	/**
	 * The CameraToolList containing this tool.
	 */
	@GenModel(property="None")
	refers CameraToolList cameraToolList opposite tools
	
	/**
	 * Method called when the camera is resolved by the CameraViewConfiguration.
	 * @param camera The AbstractCamera resolved, can be null.
	 */
	op void initializeCamera(AbstractCamera camera)
	
	/**
	 * Method called when a new ImageSnaphot is received.
	 * @param imageSnapshot The new image snapshot, can be null.
	 */
	op void updateImageSnapshot(ImageSnapshot imageSnapshot)
	
	/**
	 * Method called when the tool is no longer needed.
	 */
	op void dispose()
	
	/**
	 * Method called when the mouse is moved inside the camera image.
	 * @param cameraImage The current image being displayed.
	 * @param x The absolute position x coordinates of the mouse.
	 * @param y The absolute position y coordinates of the mouse.
	 */
	op void mouseMoved(AbstractEImage cameraImage, int mouseButton,  @GenModel(apogy_units = "pixel") int x,  @GenModel(apogy_units = "pixel") int y)
	
	/**
	 * Method called when the user clicks on the image with the mouse.
	 * @param cameraImage The current image being displayed.
	 * @param mouseButton The mouse button clicked.
	 * @param x The absolute position x coordinates of the pixel selected.
	 * @param y The absolute position y coordinates of the pixel selected.
	 */
	op void positionSelected(AbstractEImage cameraImage, int mouseButton,  @GenModel(apogy_units = "pixel") int x,  @GenModel(apogy_units = "pixel") int y)
	
}

/**
 * A list of CameraTool.
 */
class CameraToolList
{
	/** 
	 * The CameraViewConfiguration containing this CameraToolList.
	 */
	@GenModel(property="None")
	refers CameraViewConfiguration cameraViewConfiguration opposite toolList
	
	/**
	 * The list of overlay shown onto the camera image.
	 */	
	contains CameraTool[0..*] tools	opposite cameraToolList
}

/**
 * A tool that projects a ray going from the FOV origin through the point selected on the Camera View Image. The ray is made visible as a line in the 3D topology.
 */
class PointerCameraTool extends CameraTool, ToolTipTextProvider, AbsolutePoseProvider
{		
 	/**
	 * The color of the vector.
	 */
	@GenModel(propertyCategory="TOOL_PROPERTIES")	
	Color3f vectorColor = "0.0,1.0,0.0"
	
	/**
	 * The current intersection distance.
	 */
	@GenModel(notify="true", property="Readonly", propertyCategory="TOOL_PROPERTIES", sca_units="m")
	transient double intersectionDistance = "0.0"
}


/**
 * An overlay that displays a graduated cross-hair representing the horizontal and vertical Field Of View (FOV) of the 
 * camera and allows the user to specified an area in the image for the camera to pan + tilt + zoom to. 
 */
abstract class PTZCameraTool extends FOVOverlay, CameraTool
{
	/**
	 * Color to be used to represent the user selection.
	 */
	@GenModel(propertyCategory="OVERLAY_PROPERTIES")	
	Color3f selectionBoxColor = "0.0,0.0,1.0"
		
	/**
	 * First corner of the user selection. This represent the azimuth, elevation of the corner, using the FOV coordinates defined in the overlay.
	 */
	transient Point2d userSelectionCorner0
	
	/**
	 * Second corner of the user selection. This represent the azimuth, elevation of the corner, using the FOV coordinates defined in the overlay.
	 */
	transient Point2d userSelectionCorner1
		
	/**
	 * Method that clears the current user selection.
	 */
	op void clearUserSelection()	
		
	/**
	 * Method called when the user completes it selection in the image. This method should be overriden by sub classes.
	 * @param panIncrement The pan increment to center the camera to the user selected area.
	 * @param tiltIncrement The tilt increment to center the camera to the user selected area.
	 * @param newHorizontalFOV The horizontal FOV angle represented by the user selected area.
	 * @param newVerticalFOV The vertical FOV angle represented by the user selected area.
	 */
	op void commandPTZ(@GenModel(apogy_units="rad") double panIncrement, @GenModel(apogy_units="rad") double tiltIncrement, @GenModel(apogy_units="rad") double newHorizontalFOV, @GenModel(apogy_units="rad") double newVerticalFOV)	
}

/**
 * Wizard
 */
class CameraViewConfigurationPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for AbstractTextOverlay
 */
class AbstractTextOverlayOverlayPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for CameraNameOverlay.
 */
class CameraNameOverlayPagesProvider extends AbstractTextOverlayOverlayPagesProvider
{	
}

/**
 * Wizard support for ImageFrozenOverlay.
 */
class ImageFrozenOverlayPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for ImageCountOverlay.
 */
class ImageCountOverlayOverlayPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for EMFFeatureOverlay.
 */
class EMFFeatureOverlayPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for URLImageOverlay.
 */
class URLImageOverlayPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for FOVOverlay.
 */
class FOVOverlayPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for PointerCameraTool.
 */
class PointerCameraToolPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for FOVOverlay.
 */
class PTZCameraToolPagesProvider extends FOVOverlayPagesProvider
{	
}

/**
 * Wizard support for GrayScaleFilter.
 */
class GrayScaleFilterPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for EdgeFilter.
 */
class EdgeFilterPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for InvertFilter.
 */
class InvertFilterPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for ContrastAndBrightnessFilter.
 */
class ContrastAndBrightnessFilterPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for GainFilter.
 */
class GainFilterPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for GainFilter.
 */
class ExposureFilterPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

/**
 * Wizard support for RescaleFilter
 */
class RescaleFilterPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

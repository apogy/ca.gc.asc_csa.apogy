package ca.gc.asc_csa.apogy.common.geometry.data;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mesh</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A mesh composed of vertices and polygons.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data.Mesh#getPolygons <em>Polygons</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.geometry.data.ApogyCommonGeometryDataPackage#getMesh()
 * @model
 * @generated
 */
public interface Mesh<CoordinatesType extends Coordinates, PolygonType extends Polygon<CoordinatesType>> extends CoordinatesSet<CoordinatesType> {
	/**
	 * Returns the value of the '<em><b>Polygons</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polygons</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 *  The polygons composing the mesh.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Polygons</em>' containment reference list.
	 * @see ca.gc.asc_csa.apogy.common.geometry.data.ApogyCommonGeometryDataPackage#getMesh_Polygons()
	 * @model containment="true"
	 * @generated
	 */
	EList<PolygonType> getPolygons();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets a polygon's neighbours. Neighbours are defined a polygons sharing at least one vertex with the specified polygon.
	 * @param polygon The polygon.
	 * @return The list of neighbours.
	 * <!-- end-model-doc -->
	 * @model polygonUnique="false"
	 * @generated
	 */
	EList<PolygonType> getPolygonNeighbours(PolygonType polygon);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets a vertices's neighbours. Neighbours are defined as all vertices on all polygons sharing the specified vertex.
	 * @param point The vertex.
	 * @return The list of neighbours.
	 * <!-- end-model-doc -->
	 * @model pointUnique="false"
	 * @generated
	 */
	EList<CoordinatesType> getPointNeighbours(CoordinatesType point);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the list of polygons sharing a specified vertex.
	 * @param point The vertex.
	 * @return The list of polygons.
	 * <!-- end-model-doc -->
	 * @model pointUnique="false"
	 * @generated
	 */
	EList<PolygonType> getPolygonsSharingPoint(CoordinatesType point);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * <!-- begin-model-doc -->
	 * *
	 * Returns the total surface of all the polygons composing this mesh.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	double getSurface();

} // Mesh

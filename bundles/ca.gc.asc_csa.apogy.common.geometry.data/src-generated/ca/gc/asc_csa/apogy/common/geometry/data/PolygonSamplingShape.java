package ca.gc.asc_csa.apogy.common.geometry.data;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Polygon Sampling Shape</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Defines a sampling shape that is used to sample Polygons.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.geometry.data.ApogyCommonGeometryDataPackage#getPolygonSamplingShape()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface PolygonSamplingShape<CoordinatesType extends Coordinates, PolygonType extends Polygon<CoordinatesType>> extends SamplingShape {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns whether a given polygon falls inside the sampling shape.
	 * @param polygon The polygon.
	 * @return True if the specified polygon falls inside the shape, false otherwise.
	 * <!-- end-model-doc -->
	 * @model unique="false" polygonUnique="false"
	 * @generated
	 */
	boolean isPolygonInside(PolygonType polygon);

} // PolygonSamplingShape

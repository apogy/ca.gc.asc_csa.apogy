/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.converters;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.ImageSnapshot;
import ca.gc.asc_csa.apogy.common.converters.FileExporterUtilities;
import ca.gc.asc_csa.apogy.common.converters.IFileExporter;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.images.converters.AbstractImageToFileConverter;

public class ImageSnapshotToIFileConverter implements IFileExporter {

	public boolean canConvert(Object arg0) 
	{		
		if(arg0 instanceof ImageSnapshot)
		{
			return (((ImageSnapshot) arg0).getImage() != null);
		}
		return false;
	}

	public Object convert(Object arg0) throws Exception 
	{		
		ImageSnapshot imageSnapshot = (ImageSnapshot) arg0;
		
		// Saves the image in a file in the tmp folder .
		String tmpFolder = System.getProperty("user.home") + File.separator + System.getProperty("java.io.tmpdir");
		Date now = new Date();				
				
		// Save as .jpg
		String fileName = null;
		try
		{			
			fileName = tmpFolder + File.separator + now.getTime() + "." + AbstractImageToFileConverter.JPEG_FILE_EXTENSION;											
			EImagesUtilities.INSTANCE.saveImageAsJPEG(fileName, imageSnapshot.getImage());				
			
			return new File(fileName);	
		}
		catch(Exception e)
		{
			File toDelete = new File(fileName);
			toDelete.delete();			
			e.printStackTrace();
			
			return null;
		}
	}
	
	public Class<?> getInputType() {
		return ImageSnapshot.class;
	}

	public Class<?> getOutputType() { 
		return  File.class;
	}

	@Override
	public void exportToFile(Object input, String filePath, List<String> extensions) throws Exception 
	{		
		ImageSnapshot imageSnapshot = (ImageSnapshot) input;
		for(String extension : extensions)
		{
			String fullPathString = filePath + "." + extension;		
			if(extension.equalsIgnoreCase(AbstractImageToFileConverter.JPEG_FILE_EXTENSION))
			{										
				EImagesUtilities.INSTANCE.saveImageAsJPEG(fullPathString, imageSnapshot.getImage());
			}
			else if(extension.equalsIgnoreCase(AbstractImageToFileConverter.PNG_FILE_EXTENSION))
			{										
				EImagesUtilities.INSTANCE.saveImageAsPNG(fullPathString, imageSnapshot.getImage());				
			}
			else if(extension.equalsIgnoreCase(METADATA_FILE_EXTENSION))
			{								
				FileExporterUtilities.saveMetaDataToFile(fullPathString, getMetaData(imageSnapshot));
			}
		}
	}

	@Override
	public List<String> getSupportedFileExtensions() 
	{
		List<String> extensions = new ArrayList<String>();
		extensions.add(AbstractImageToFileConverter.JPEG_FILE_EXTENSION);
		extensions.add(AbstractImageToFileConverter.PNG_FILE_EXTENSION);
		extensions.add(METADATA_FILE_EXTENSION);
		return extensions;
	}

	@Override
	public String getDescription(String fileExtension) 
	{
		if(fileExtension.contains(AbstractImageToFileConverter.JPEG_FILE_EXTENSION))
		{
			return "The ImageSnapshot's image to file in JPEG format.";
		}
		else if(fileExtension.contains(AbstractImageToFileConverter.PNG_FILE_EXTENSION))
		{
			return "The ImageSnapshot's image to file in PNG format.";
		}
		else if(fileExtension.contains(METADATA_FILE_EXTENSION))
		{
			return "The ImageSnapshot metadata (FOV,time stamp).";
		}
		return null;		
	}
	
	/**
	 * Gets the metadata as a String for a given ImageSnapshot.
	 * @param imageSnapshot The ImageSnapshot.
	 * @return The metadata string.
	 */
	public static String getMetaData(ImageSnapshot imageSnapshot)
	{
		String metadata ="";
		
		if(imageSnapshot.getTime() != null)
		{
			metadata += "Time = " + ApogyCommonEMFFacade.INSTANCE.format(imageSnapshot.getTime()) + "\n";
		}
		
		if(imageSnapshot.getFieldOfView() != null)
		{
			metadata += "FOV Horizontal (deg) = " + Math.toDegrees(imageSnapshot.getFieldOfView().getHorizontalFieldOfViewAngle()) + "\n";
			metadata += "FOV Vertical (deg) = " + Math.toDegrees(imageSnapshot.getFieldOfView().getVerticalFieldOfViewAngle()) + "\n";
		}
		
		if(imageSnapshot.getImage() != null)
		{
			metadata += AbstractImageToFileConverter.getMetaData(imageSnapshot.getImage());
		}
		
		return metadata;
	}
}

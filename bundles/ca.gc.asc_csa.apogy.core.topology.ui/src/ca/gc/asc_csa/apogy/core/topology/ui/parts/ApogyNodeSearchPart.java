/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.parts;

import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.NodeSearchComposite;
import ca.gc.asc_csa.apogy.core.ApogyTopology;

public class ApogyNodeSearchPart extends AbstractApogyTopologyBasedPart
{
	private NodeSearchComposite nodeSelectionComposite;
	
	@Override
	protected void newTopology(ApogyTopology apogyTopology) 
	{		
		if(apogyTopology != null)
		{
			nodeSelectionComposite.setTopologyRoot(apogyTopology.getRootNode());
		}
		else
		{
			nodeSelectionComposite.setTopologyRoot(null);
		}
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{
		nodeSelectionComposite = new NodeSearchComposite(parent, style)
		{
			@Override
			public void nodeSelectedChanged(Node nodeSelected) 
			{
				selectionService.setSelection(nodeSelected);
			}
		};
	}
}

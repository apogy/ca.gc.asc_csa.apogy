/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.NodePath;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.NodeSearchComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyTopology;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyFacade;

public class AttachedViewPointComposite extends Composite 
{	
	private AttachedViewPoint attachedViewPoint;		
	
	private Button enableRotationButton;
	private Button enableTranslationButton;
	
	
	private DataBindingContext m_bindingContext;
		
	private NodeSearchComposite nodeSearchComposite;
	
	public AttachedViewPointComposite(Composite parent, int style) 
	{
		this(parent, style, null);
	}
	
	public AttachedViewPointComposite(Composite parent, int style,  AttachedViewPoint attachedViewPoint) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Composite top = new Composite(this, SWT.BORDER);		
		top.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		top.setLayout(new GridLayout(4,true));
		
		// Rotation Enable.
		Label lblEnableRotationLabel = new Label(top, SWT.NONE);
		lblEnableRotationLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEnableRotationLabel.setText("Enable Rotation:");
		
		enableRotationButton = new Button(top, SWT.CHECK);
		
		// Translation Enable.
		Label lblEnableTranslationLabel = new Label(top, SWT.NONE);
		lblEnableTranslationLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEnableTranslationLabel.setText("Enable Translation:");
		
		enableTranslationButton = new Button(top, SWT.CHECK);
		
		Composite bottom = new Composite(this, SWT.None);
		bottom.setLayout(new GridLayout(1, false));
		bottom.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));		
		
		nodeSearchComposite = new NodeSearchComposite(bottom, SWT.BORDER)
		{
			@Override
			public void nodeSelectedChanged(Node nodeSelected) 
			{
				// Updates the nodePath.
				Node root = ApogyCoreTopologyFacade.INSTANCE.getApogyTopology().getRootNode();
				NodePath nodePath = ApogyCommonTopologyFacade.INSTANCE.createNodePath(root, nodeSelected);	
				ApogyCommonTransactionFacade.INSTANCE.basicSet(attachedViewPoint, ApogyCommonTopologyPackage.Literals.ATTACHED_VIEW_POINT__NODE_PATH, nodePath);					
			}
		};
		nodeSearchComposite.setTopologyRoot(resolveTopologyRoot());		
		
		setAttachedViewPoint(attachedViewPoint);
		
		// Adds dispose cleanup.
		this.addDisposeListener(new DisposeListener() 
		{		
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{				
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}
	
	public AttachedViewPoint getAttachedViewPoint() 
	{
		return attachedViewPoint;
	}

	public void setAttachedViewPoint(AttachedViewPoint attachedViewPoint) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.attachedViewPoint = attachedViewPoint;
		
		if(attachedViewPoint != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}

	private Node resolveTopologyRoot()
	{
		ApogyTopology apogyTopology = ApogyCoreTopologyFacade.INSTANCE.getApogyTopology(); 
		if(apogyTopology != null)
		{
			return apogyTopology.getRootNode();
		}
		return null;
	}
		
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Enable Rotation.*/
		IObservableValue<Double> observeAllowRotation = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(attachedViewPoint), 
				  FeaturePath.fromList(ApogyCommonTopologyPackage.Literals.ATTACHED_VIEW_POINT__ALLOW_ROTATION)).observe(attachedViewPoint);
		
		IObservableValue<String> observeEnableRotationButton = WidgetProperties.selection().observe(enableRotationButton);
		
		bindingContext.bindValue(observeEnableRotationButton,
									observeAllowRotation, 
									new UpdateValueStrategy(),	
									new UpdateValueStrategy());		
		
		/* Enable Translation.*/
		IObservableValue<Double> observeAllowTranslation = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(attachedViewPoint), 
				  FeaturePath.fromList(ApogyCommonTopologyPackage.Literals.ATTACHED_VIEW_POINT__ALLOW_TRANSLATION)).observe(attachedViewPoint);
		
		IObservableValue<String> observeEnableTranslationButton = WidgetProperties.selection().observe(enableTranslationButton);
		
		bindingContext.bindValue(observeEnableTranslationButton,
								observeAllowTranslation, 
								new UpdateValueStrategy(),	
								new UpdateValueStrategy());		
		
		return bindingContext;
	}
	
}

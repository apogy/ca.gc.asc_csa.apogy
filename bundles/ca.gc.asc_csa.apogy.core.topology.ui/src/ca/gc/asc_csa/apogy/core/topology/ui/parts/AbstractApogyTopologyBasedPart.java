/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 		 Regent L'Archeveque 
 * 		 Olivier L. Larouche
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.parts;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractPart;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;
import ca.gc.asc_csa.apogy.core.ApogyTopology;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyFacade;

abstract public class AbstractApogyTopologyBasedPart extends AbstractPart{
	
	private Adapter adapter;	
	
	@Override
	protected void createNoContentComposite(Composite parent, int style) {
		new NoContentComposite(parent, SWT.None)
		{			
			@Override
			protected String getMessage() {
				return "No active topology";
			}
		};	
	}
			
	@Override
	protected EObject getInitializeObject() {
		ApogyCoreTopologyFacade.INSTANCE.eAdapters().add(getAdapter());
		return ApogyCoreTopologyFacade.INSTANCE.getApogyTopology();
	} 
	
	abstract protected void newTopology(ApogyTopology apogyTopology);

	@Override
	protected void setCompositeContent(EObject eObject) 
	{
		newTopology((ApogyTopology) eObject); 
	}
	
	/**
	 * Gets an adapter that sets the part's parentComposite to a
	 * {@link NoActiveSessionComposite} if there is no active session.
	 * 
	 * @return the {@link Adapter}
	 */
	private Adapter getAdapter() {
		if (adapter == null) {
			adapter = new AdapterImpl() {
				@Override
				public void notifyChanged(Notification msg) {
					setEObject(ApogyCoreTopologyFacade.INSTANCE.getApogyTopology());
				}
			};
		}
		return adapter;
	}

	@Override
	protected void dispose() {
		ApogyCoreTopologyFacade.INSTANCE.eAdapters().remove(getAdapter());
		super.dispose();
	}
}
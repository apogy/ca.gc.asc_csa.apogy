package ca.gc.asc_csa.apogy.core.topology.ui;

public class ApogyCoreTopologyUIRCPConstants 
{
	/**
	 * Part IDs for selectionBasedParts.
	 */
	public static final String PART__APOGY_SYSTEM_FILE_EDITOR_ID = "ca.gc.asc_csa.apogy.core.topology.ui.parts.apogysystemfileeditor";	
	public static final String PART__VIEWER_3D__ID = "ca.gc.asc_csa.apogy.core.topology.ui.part.3D";
	public static final String PART__NODE_SEARCH__ID = "ca.gc.asc_csa.apogy.core.topology.ui.nodeSearch";
	public static final String PART__TOPOLOGY_TREE_EDITOR__ID = "ca.gc.asc_csa.apogy.core.topology.ui.topoTreeEditor";
	
	// Apogy System Editing
	public static final String PART__APOGY_SYSTEM_3D__ID = "ca.gc.asc_csa.apogy.core.topology.ui.part.apogysystem3dviewer";
	public static final String PART_APOGY_SYSTEM_TOPOLOGY_EDITOR_ID = "ca.gc.asc_csa.apogy.core.topology.ui.part.apogysystemtopology"; 																	   
	public static final String APOGY_NODE_PRESENTATION_SHOW_NODE_DETAILS_PERSISTED_STATE_ID = "ShowNodeOverviewSection";
}

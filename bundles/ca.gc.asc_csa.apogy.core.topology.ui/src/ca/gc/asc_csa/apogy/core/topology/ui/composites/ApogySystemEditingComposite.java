/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.ApogySystem;

public class ApogySystemEditingComposite extends Composite 
{
	private ApogySystem apogySystem;
	
	private ApogySystemFileSelectionComposite apogySystemFileSelectionComposite;
	private ApogySystemDetailsComposite apogySystemDetailsComposite;
	
	public ApogySystemEditingComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		apogySystemFileSelectionComposite = new ApogySystemFileSelectionComposite(this, SWT.NONE)
		{
			@Override
			protected void apogySystemOpened(ApogySystem newApogySystem) 
			{
				setApogySystem(newApogySystem);
			}
			
			@Override
			protected void apogySystemPathSet(String newApogySystemPath) 
			{
				ApogySystemEditingComposite.this.apogySystemPathSet(newApogySystemPath);
			}
			
			@Override
			protected void setFileIsDirty(boolean dirty) 
			{
				ApogySystemEditingComposite.this.setFileIsDirty(dirty);
			}
		};
		GridData gd_apogySystemFileSelectionComposite = new GridData(SWT.FILL, SWT.TOP, true, false);
		apogySystemFileSelectionComposite.setLayoutData(gd_apogySystemFileSelectionComposite);
		
		
		apogySystemDetailsComposite = new ApogySystemDetailsComposite(this, SWT.NONE);
		GridData gd_apogySystemDetailsComposite = new GridData(SWT.FILL, SWT.TOP, true, false);
		apogySystemDetailsComposite.setLayoutData(gd_apogySystemDetailsComposite);
	}

	public ApogySystem getApogySystem() 
	{
		return apogySystem;
	}

	public void setApogySystem(final ApogySystem apogySystem) 
	{
		this.apogySystem = apogySystem;
		
		getDisplay().asyncExec(new Runnable() 
		{			
			@Override
			public void run() 
			{
				if(apogySystemDetailsComposite != null && !apogySystemDetailsComposite.isDisposed())
				{
					apogySystemDetailsComposite.setApogySystem(apogySystem);
				}
			}
		});
	}	
	
	public void save() throws Exception
	{
		apogySystemFileSelectionComposite.save();
	}
	
	/**
	 * @param dirty
	 */
	protected void setFileIsDirty(boolean dirty)
	{		
	}
	
	protected void apogySystemPathSet(String newApogySystemPath) 
	{
	}
}

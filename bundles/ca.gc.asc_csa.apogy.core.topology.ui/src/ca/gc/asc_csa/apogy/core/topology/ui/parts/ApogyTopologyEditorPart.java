/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 		 Regent L'Archeveque
 * 	     Olivier L. Larouche
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

package ca.gc.asc_csa.apogy.core.topology.ui.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.TopologyTreeEditingComposite;
import ca.gc.asc_csa.apogy.core.ApogyTopology;

public class ApogyTopologyEditorPart extends AbstractApogyTopologyBasedPart 
{
	private TopologyTreeEditingComposite topologyTreeEditingComposite;
	
	@Override
	protected void createContentComposite(Composite parent, int style) 
	{		
		topologyTreeEditingComposite = new TopologyTreeEditingComposite(parent, SWT.BORDER, true)
		{
			@Override
			protected void nodeSelected(Node node) 
			{
				selectionService.setSelection(node);
			}
		};	
	}

	@Override
	protected void newTopology(ApogyTopology apogyTopology) 
	{
		if(topologyTreeEditingComposite != null && !topologyTreeEditingComposite.isDisposed())
		{
			if(apogyTopology != null)
			{
				topologyTreeEditingComposite.setRoot(apogyTopology.getRootNode());
			}
			else
			{
				topologyTreeEditingComposite.setRoot(null);
			}
		}
	}
}
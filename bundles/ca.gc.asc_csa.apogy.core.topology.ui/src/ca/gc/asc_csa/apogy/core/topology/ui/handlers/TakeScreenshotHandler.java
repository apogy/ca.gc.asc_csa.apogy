package ca.gc.asc_csa.apogy.core.topology.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.topology.ui.parts.AbstractApogy3dPart;
import ca.gc.asc_csa.apogy.core.topology.ui.parts.ApogySystem3dPart;

public class TakeScreenshotHandler {

	@CanExecute
	public boolean canExecute(MPart part)
	{
		if (part.getObject() instanceof AbstractApogy3dPart )
		{
			return ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() != null;	
		}
		else
		{
			return false;
		}		
	}
	
	@Execute
	public void execute(MPart part)
	{
		if (part.getObject() instanceof AbstractApogy3dPart )
		{
			AbstractApogy3dPart apogy3dPart = (AbstractApogy3dPart) part.getObject();
			apogy3dPart.takeScreenshot();			
		}
		else if(part.getObject() instanceof ApogySystem3dPart)
		{
			ApogySystem3dPart apogySystem3dPart = (ApogySystem3dPart) part.getObject();
			apogySystem3dPart.takeScreenshot();
		}
	}
}
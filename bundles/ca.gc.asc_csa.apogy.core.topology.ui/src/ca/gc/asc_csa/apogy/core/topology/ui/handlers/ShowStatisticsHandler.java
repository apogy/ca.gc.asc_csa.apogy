package ca.gc.asc_csa.apogy.core.topology.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;

import ca.gc.asc_csa.apogy.common.topology.ui.viewer.ITopologyViewer;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.TopologyViewerProvider;

public class ShowStatisticsHandler {

	@CanExecute
	public boolean canExecute(MPart part)
	{
		if (part.getObject() instanceof TopologyViewerProvider)
		{			
			TopologyViewerProvider provider = (TopologyViewerProvider) part.getObject();
			ITopologyViewer topologyViewer = provider.getTopologyViewer();
			return (topologyViewer != null);				
		}	
		else
		{
			return false;
		}
	}
	
	@Execute
	public void execute(MPart part, final MToolItem item)
	{
		if (part.getObject() instanceof TopologyViewerProvider)
		{			
			TopologyViewerProvider provider = (TopologyViewerProvider) part.getObject();
			ITopologyViewer topologyViewer = provider.getTopologyViewer();
			if(topologyViewer != null) topologyViewer.setShowStatisticsEnabled(item.isSelected());			
		}					
	}
}
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.parts;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecp.view.spi.model.VView;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.converters.ApogyCommonConvertersFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import ca.gc.asc_csa.apogy.common.topology.ui.NodeSelection;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIRCPConstants;

public class ApogyNodePresentationPart extends AbstractEObjectSelectionPart
{	
	private boolean showNodeDetails = true;
	
	private Composite nodePresentationComposite;
	private ScrolledComposite nodePresentationCompositeScrolledComposite;
	
	private ScrolledComposite nodeDetailsScrolledComposite;
	private Composite nodeDetailsComposite;

	
	@Override
	public void userPostConstruct(MPart mPart) 
	{
		Map<String, String> persistedStateMap = mPart.getPersistedState();
		if(persistedStateMap.containsKey(ApogyCoreTopologyUIRCPConstants.APOGY_NODE_PRESENTATION_SHOW_NODE_DETAILS_PERSISTED_STATE_ID))
		{
			showNodeDetails = Boolean.valueOf(persistedStateMap.get(ApogyCoreTopologyUIRCPConstants.APOGY_NODE_PRESENTATION_SHOW_NODE_DETAILS_PERSISTED_STATE_ID));
		}
	}
	
	@Override
	protected void createContentComposite(Composite parent, int style) 
	{
		System.out.println("Show Details : " + showNodeDetails);
		
		if(showNodeDetails)
		{
			Composite top =  new Composite(parent, SWT.NONE);
			top.setLayout(new GridLayout(2,true));
			
			Label labelPresentation = new Label(top, SWT.NONE);
			labelPresentation.setText("Presentation Details");
	
			Label labelNode = new Label(top, SWT.NONE);
			labelNode.setText("Node Details");
			
			// Node Presentation
			nodePresentationCompositeScrolledComposite = new ScrolledComposite(top, SWT.H_SCROLL | SWT.V_SCROLL);
			nodePresentationCompositeScrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			nodePresentationCompositeScrolledComposite.setExpandHorizontal(true);
			nodePresentationCompositeScrolledComposite.setExpandVertical(true);
			
			nodePresentationComposite = new Composite(nodePresentationCompositeScrolledComposite, SWT.NONE);	
			nodePresentationCompositeScrolledComposite.setContent(nodePresentationComposite);
			nodePresentationCompositeScrolledComposite.setMinSize(nodePresentationComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
					
			// Node Details
			nodeDetailsScrolledComposite = new ScrolledComposite(top, SWT.H_SCROLL | SWT.V_SCROLL);
			nodeDetailsScrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			nodeDetailsScrolledComposite.setExpandHorizontal(true);
			nodeDetailsScrolledComposite.setExpandVertical(true);
					
			nodeDetailsComposite = new Composite(nodeDetailsScrolledComposite, SWT.NONE);		
			nodeDetailsScrolledComposite.setContent(nodeDetailsComposite);
			nodeDetailsScrolledComposite.setMinSize(nodeDetailsComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			
			top.layout();
		}
		else
		{
			Composite top =  new Composite(parent, SWT.NONE);
			top.setLayout(new GridLayout(1,true));
			
			Label labelPresentation = new Label(top, SWT.NONE);
			labelPresentation.setText("Presentation Details");
	
			
			// Node Presentation
			nodePresentationCompositeScrolledComposite = new ScrolledComposite(top, SWT.H_SCROLL | SWT.V_SCROLL);
			nodePresentationCompositeScrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			nodePresentationCompositeScrolledComposite.setExpandHorizontal(true);
			nodePresentationCompositeScrolledComposite.setExpandVertical(true);
			
			nodePresentationComposite = new Composite(nodePresentationCompositeScrolledComposite, SWT.NONE);	
			nodePresentationCompositeScrolledComposite.setContent(nodePresentationComposite);
			nodePresentationCompositeScrolledComposite.setMinSize(nodePresentationComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));		
			
			top.layout();
		}
	}

	@Override
	protected void setCompositeContents(EObject eObject) 
	{		
		if(eObject instanceof NodePresentation)
		{
			NodePresentation nodePresentation = (NodePresentation) eObject;
			
			if(!nodePresentationComposite.isDisposed())
			{
				if(showNodeDetails)
				{
					// Update the EMFForm.			
					ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(nodePresentationComposite, eObject);
					nodePresentationCompositeScrolledComposite.setMinSize(nodePresentationComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
					
					VView viewModel = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createDefaultViewModel(nodePresentation.getNode());			
					ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(nodeDetailsComposite, nodePresentation.getNode(), viewModel);				
					nodeDetailsScrolledComposite.setMinSize(nodeDetailsComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				}
				else
				{
					// Update the EMFForm.			
					ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(nodePresentationComposite, eObject);
					nodePresentationCompositeScrolledComposite.setMinSize(nodePresentationComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				}
			}
		}		
	}
	
	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() {
		HashMap<String, ISelectionListener> map = new HashMap<>();
		
		map.put(ApogyCoreTopologyUIRCPConstants.PART__VIEWER_3D__ID, create3DViewerSelectionListener());
		map.put(ApogyCoreTopologyUIRCPConstants.PART__NODE_SEARCH__ID, createNodeSearchListener());
		map.put(ApogyCoreTopologyUIRCPConstants.PART__TOPOLOGY_TREE_EDITOR__ID, createNodeSearchListener());
		map.put(ApogyCoreTopologyUIRCPConstants.PART_APOGY_SYSTEM_TOPOLOGY_EDITOR_ID, createNodeSearchListener());
		
		return map;
	}
	
	private ISelectionListener create3DViewerSelectionListener()
	{
		return new ISelectionListener() 
		{
			@Override
			public void selectionChanged(MPart part, Object selection) 
			{
				if(selection instanceof NodeSelection)
				{
					NodeSelection nodeSelection = (NodeSelection) selection;
					Node selectedNode = nodeSelection.getSelectedNode();
					
					if(selectedNode != null)
					{
						NodePresentation selectedNodePresentation = (NodePresentation) ApogyCommonConvertersFacade.INSTANCE.convert(selectedNode, NodePresentation.class);
						setEObject(selectedNodePresentation);
					}
					else
					{
						setEObject(null);
					}
				}												
			}
		};
	}
	
	private ISelectionListener createNodeSearchListener()
	{
		return new ISelectionListener() 
		{
			@Override
			public void selectionChanged(MPart part, Object selection) 
			{
				if (selection instanceof Node) 
				{
					Node selectedNode = (Node) selection;
					NodePresentation selectedNodePresentation = (NodePresentation) ApogyCommonConvertersFacade.INSTANCE.convert(selectedNode, NodePresentation.class);
					setEObject(selectedNodePresentation);
				}
				else
				{
					setEObject(null);
				}
			}
		};
	}	
}

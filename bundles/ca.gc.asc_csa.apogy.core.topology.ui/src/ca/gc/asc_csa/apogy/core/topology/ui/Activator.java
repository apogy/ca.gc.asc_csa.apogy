package ca.gc.asc_csa.apogy.core.topology.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIFacade;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyFacade;
import ca.gc.asc_csa.apogy.core.topology.ui.preferences.ApogyTopologyUIPreferencesConstants;



public class Activator extends AbstractUIPlugin 
{		
	// The plug-in ID
	public static final String ID = "ca.gc.asc_csa.apogy.core.topology.ui"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	private static BundleContext context;
	
	private Adapter adapter;	
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception 
	{
		super.start(context);
		Activator.context = context;
		plugin = this;
			
		// Force the update of the TopologyPresentationRegistry
		forceTopologyPresentationRegistryUpdate();
		
		ApogyCoreTopologyFacade.INSTANCE.eAdapters().add(getAdapter());	
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		Activator.context = null;
		
		ApogyCoreTopologyFacade.INSTANCE.eAdapters().remove(getAdapter());
		
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	static BundleContext getContext() {
		return context;
	}
	
	public static RGB getApogySystem3DViewBackgroundColor()
	{
		return PreferenceConverter.getColor(getDefault().getPreferenceStore(), ApogyTopologyUIPreferencesConstants.DEFAULT_APOGY_SYSTEM_VIEW_BACKGROUND_COLOR_ID);		
	}
	
	/**
	 * Forces the update of the TopologyPresentationRegistry.
	 */
	private static void forceTopologyPresentationRegistryUpdate()
	{
		if(ApogyCoreTopologyFacade.INSTANCE.getApogyTopology() != null)
		{
			// Forces the creation of the NodePresentation Registry.					
			ApogyCommonTopologyUIFacade.INSTANCE.createGraphicsContext(ApogyCoreTopologyFacade.INSTANCE.getApogyTopology().getRootNode());					
		}
	}
	
	/**
	 * Returns the adapter used to force the update of the TopologyPresentationRegistry when the session root node changes.
	 * @return The Adapter.
	 */
	private Adapter getAdapter() 
	{
		if (adapter == null) 
		{
			adapter = new AdapterImpl() 
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					forceTopologyPresentationRegistryUpdate();
				}
			};
		}
		return adapter;
	}
}

package ca.gc.asc_csa.apogy.core.topology.ui.wizards;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint;
import ca.gc.asc_csa.apogy.core.topology.ui.composites.AttachedViewPointComposite;

public class AttachedViewPointWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.ui.wizards.TransformNodeWizardPage";
		
	private AttachedViewPoint attachedViewPoint;
	private Adapter adapter;
	private AttachedViewPointComposite attachedViewPointComposite;
			
	public AttachedViewPointWizardPage(AttachedViewPoint attachedViewPoint) 
	{
		super(WIZARD_PAGE_ID);
		this.attachedViewPoint = attachedViewPoint;
				
		setTitle("Attached View Point");
		setDescription("Sets the Attached View Point attachment node.");
		
		if(attachedViewPoint != null) attachedViewPoint.eAdapters().add(getAdapter());
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
		
		attachedViewPointComposite = new AttachedViewPointComposite(top, SWT.NONE, attachedViewPoint);
		attachedViewPointComposite.setAttachedViewPoint(attachedViewPoint);
		attachedViewPointComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		setControl(top);
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if(attachedViewPointComposite != null && !attachedViewPointComposite.isDisposed()) attachedViewPointComposite.dispose();
				if(attachedViewPoint != null) attachedViewPoint.eAdapters().remove(getAdapter());							
			}
		});
		
	}			
		
	protected void validate()
	{
		setErrorMessage(null);			
			
		if(attachedViewPoint.getName() == null)
		{
			setErrorMessage("Name is not set !");
		}
		else if(attachedViewPoint.getNodePath() == null)
		{
			setErrorMessage("Node is not set !");
		}
				
		setPageComplete(getErrorMessage() == null);
	}			
	
	private Adapter getAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					validate();
				}
			};
		}
		
		return adapter;
	}
}

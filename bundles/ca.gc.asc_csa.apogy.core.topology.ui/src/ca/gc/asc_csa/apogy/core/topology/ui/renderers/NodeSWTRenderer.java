package ca.gc.asc_csa.apogy.core.topology.ui.renderers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.core.swt.SimpleControlSWTControlSWTRenderer;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.editsupport.EMFFormsEditSupport;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.impl.FeaturePathAdapterImpl;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.dialogs.NodeSelectionDialog;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyFacade;

public class NodeSWTRenderer extends SimpleControlSWTControlSWTRenderer {
	private Text txtNodeId;
	private Button btnSelect;

	private FeaturePathAdapter adapter;

	@Inject
	public NodeSWTRenderer(VControl vElement, ViewModelContext viewContext, ReportService reportService,
			EMFFormsDatabinding emfFormsDatabinding, EMFFormsLabelProvider emfFormsLabelProvider,
			VTViewTemplateProvider vtViewTemplateProvider, EMFFormsEditSupport emfFormsEditSupport) {
		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);
	}

	@Override
	protected Control createSWTControl(Composite parent) {
		final Composite main = new Composite(parent, SWT.NONE);
		main.setLayout(GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(false).create());
		main.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		main.setBackground(parent.getBackground());
		main.setBackgroundMode(SWT.INHERIT_FORCE);

		txtNodeId = new Text(main, SWT.BORDER);
		txtNodeId.setEditable(false);
		Object obj = getValue();

		if (obj instanceof Node) 
		{
			Node node = (Node) obj;
			if(node.getNodeId() != null)
			{
				txtNodeId.setText(node.getNodeId());
			}
			else
			{
				txtNodeId.setText("");
			}
		} else {
			txtNodeId.setText("");
		}

		GridData gd_txtNodeId = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_txtNodeId.minimumWidth = 100;
		gd_txtNodeId.widthHint = 100;
		txtNodeId.setLayoutData(gd_txtNodeId);

		btnSelect = new Button(main, SWT.None);
		btnSelect.setText("Select...");
		btnSelect.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		btnSelect.addSelectionListener(new SelectionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Opens the Node Selection Dialog.
				NodeSelectionDialog nodeSelectionDialog = new NodeSelectionDialog(parent.getShell(), getRootNode());

				if (nodeSelectionDialog.open() == Window.OK) {
					Node node = nodeSelectionDialog.getSelectedNode();

					try {
						getModelValue().setValue(node);
					} catch (DatabindingFailedException e1) {
						e1.printStackTrace();
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		btnSelect.setEnabled(true);

		return main;
	}

	@Override
	protected Binding[] createBindings(Control control) throws DatabindingFailedException {
		getAdapter().init(getViewModelContext().getDomainModel());
		return new Binding[0];
	}

	private Node getRootNode() {
		if (ApogyCoreTopologyFacade.INSTANCE.getApogyTopology() != null) {
			return ApogyCoreTopologyFacade.INSTANCE.getApogyTopology().getRootNode();
		}
		return null;
	}

	public FeaturePathAdapter getAdapter() {
		if (adapter == null) {
			adapter = new FeaturePathAdapterImpl() {

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> features = new ArrayList<EStructuralFeature>();

					features.add(getEStructuralFeature());
					features.add(ApogyCommonTopologyPackage.Literals.NODE__NODE_ID);

					return features;
				}

				@Override
				public void notifyChanged(Notification msg) {
					Object obj = getValue();

					if (obj instanceof Node) {
						txtNodeId.setText(((Node) obj).getNodeId());
					} else {
						txtNodeId.setText("");
					}
				}
			};
		}
		return adapter;
	}

	private EStructuralFeature getEStructuralFeature() {
		try {
			return (EStructuralFeature) getModelValue().getValueType();
		} catch (DatabindingFailedException e) {
			return null;
		}
	}

	private Object getValue() {
		try {
			return getModelValue().getValue();
		} catch (DatabindingFailedException e) {
			return null;
		}

	}

	@Override
	protected void dispose() {
		getAdapter().dispose();
		super.dispose();
	}

	@Override
	protected String getUnsetText() {
		return "unset value";
	}
}

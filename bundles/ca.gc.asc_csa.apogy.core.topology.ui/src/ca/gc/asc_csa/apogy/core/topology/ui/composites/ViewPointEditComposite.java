/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.AbstractViewPointComposite;
import ca.gc.asc_csa.apogy.core.environment.ViewPointList;
import ca.gc.asc_csa.apogy.core.environment.ui.composites.ViewPointListComposite;

public class ViewPointEditComposite extends Composite 
{
	private ViewPointList viewPointList;
	
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());	
	private ViewPointListComposite viewPointListComposite;
	private AbstractViewPointComposite abstractViewPointComposite;
	private AbstractViewPointDetailsComposite abstractViewPointDetailsComposite;
	private AbstractViewPoint currentActiveViewPoint;
	
	public ViewPointEditComposite(Composite parent, int style, AbstractViewPoint currentActiveViewPoint) 
	{	
		super(parent, style);
		
		this.currentActiveViewPoint = currentActiveViewPoint;
		
		setLayout(new GridLayout(1, false));
		ScrolledForm scrldfrm = formToolkit.createScrolledForm(this);
		scrldfrm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrldfrm);
		scrldfrm.setText("View Points");
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.numColumns = 2;
			tableWrapLayout.makeColumnsEqualWidth = true;
			scrldfrm.getBody().setLayout(tableWrapLayout);
		}
		
		// View Point List
		Section sctnViewPoints = formToolkit.createSection(scrldfrm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnViewPoints = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 2, 1);
		twd_sctnViewPoints.valign = TableWrapData.FILL;
		twd_sctnViewPoints.grabVertical = true;
		twd_sctnViewPoints.grabHorizontal = true;
		twd_sctnViewPoints.align = TableWrapData.FILL;
		sctnViewPoints.setLayoutData(twd_sctnViewPoints);
		formToolkit.paintBordersFor(sctnViewPoints);
		sctnViewPoints.setText("View Points");
		
	    viewPointListComposite = new ViewPointListComposite(sctnViewPoints, SWT.NONE)
	    {
	    	@Override
	    	protected void newViewPointSelected(AbstractViewPoint viewPoint) 
	    	{
	    		ViewPointEditComposite.this.currentActiveViewPoint = viewPoint;
	    		
	    		abstractViewPointComposite.setAbstractViewPoint(viewPoint);
	    		abstractViewPointDetailsComposite.setAbstractViewPoint(viewPoint);
	    		
	    		ViewPointEditComposite.this.newViewPointSelected(viewPoint);
	    	}
	    };			
	    
	    formToolkit.adapt(viewPointListComposite);
	    formToolkit.paintBordersFor(viewPointListComposite);
	    sctnViewPoints.setClient(viewPointListComposite);
	
		// View Point Overview.
		Section sctnOverview = formToolkit.createSection(scrldfrm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnOverview = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 1, 1);
		twd_sctnOverview.grabVertical = true;
		twd_sctnOverview.valign = TableWrapData.FILL;
		twd_sctnOverview.grabHorizontal = true;
		twd_sctnOverview.align = TableWrapData.FILL;
		sctnOverview.setLayoutData(twd_sctnOverview);
		formToolkit.paintBordersFor(sctnOverview);
		sctnOverview.setText("Overview");
				
	    abstractViewPointComposite = new AbstractViewPointComposite(sctnOverview, SWT.NONE, null);	    
	    formToolkit.adapt(abstractViewPointComposite);
	    formToolkit.paintBordersFor(abstractViewPointComposite);
	    sctnOverview.setClient(abstractViewPointComposite);
	    
		// View Point Details.
		Section sctnDetails = formToolkit.createSection(scrldfrm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnDetails = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 1, 1);
		twd_sctnDetails.grabVertical = true;
		twd_sctnDetails.valign = TableWrapData.FILL;
		twd_sctnDetails.grabHorizontal = true;
		twd_sctnDetails.align = TableWrapData.FILL;
		sctnDetails.setLayoutData(twd_sctnDetails);
		formToolkit.paintBordersFor(sctnOverview);
		sctnDetails.setText("Details");
				
		abstractViewPointDetailsComposite = new AbstractViewPointDetailsComposite(sctnDetails, SWT.NONE);
		formToolkit.adapt(abstractViewPointDetailsComposite);
		formToolkit.paintBordersFor(abstractViewPointDetailsComposite);
		sctnDetails.setClient(abstractViewPointDetailsComposite);	
		
		addDisposeListener(new DisposeListener() 
		{
			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{		
				formToolkit.dispose();
			}
		});
	}

	public ViewPointList getViewPointList() {
		return viewPointList;
	}

	public void setViewPointList(ViewPointList viewPointList) 
	{		
		this.viewPointList = viewPointList;					
		
		if(viewPointListComposite != null && !viewPointListComposite.isDisposed())
		{
			viewPointListComposite.setViewPointList(viewPointList);			
			
			if(viewPointList != null)
			{
				viewPointListComposite.setSelectedViewPoint(currentActiveViewPoint);
			}
		}
		
		if(abstractViewPointComposite != null && !abstractViewPointComposite.isDisposed())
		{
			abstractViewPointComposite.setAbstractViewPoint(currentActiveViewPoint);
		}
	}
	
	protected void newViewPointSelected(AbstractViewPoint viewPoint) 
	{
		
	}
}

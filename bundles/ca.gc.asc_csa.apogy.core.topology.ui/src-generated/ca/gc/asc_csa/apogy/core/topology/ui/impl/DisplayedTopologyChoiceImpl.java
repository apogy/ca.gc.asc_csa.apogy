/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.topology.ui.impl;

import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage;
import ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopology;
import ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopologyChoice;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Displayed Topology Choice</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.topology.ui.impl.DisplayedTopologyChoiceImpl#getDisplayedTopology <em>Displayed Topology</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DisplayedTopologyChoiceImpl extends MinimalEObjectImpl.Container implements DisplayedTopologyChoice {
	/**
	 * The default value of the '{@link #getDisplayedTopology() <em>Displayed Topology</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedTopology()
	 * @generated
	 * @ordered
	 */
	protected static final DisplayedTopology DISPLAYED_TOPOLOGY_EDEFAULT = DisplayedTopology.ASSEMBLY;

	/**
	 * The cached value of the '{@link #getDisplayedTopology() <em>Displayed Topology</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedTopology()
	 * @generated
	 * @ordered
	 */
	protected DisplayedTopology displayedTopology = DISPLAYED_TOPOLOGY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DisplayedTopologyChoiceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreTopologyUIPackage.Literals.DISPLAYED_TOPOLOGY_CHOICE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisplayedTopology getDisplayedTopology() {
		return displayedTopology;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisplayedTopology(DisplayedTopology newDisplayedTopology) {
		DisplayedTopology oldDisplayedTopology = displayedTopology;
		displayedTopology = newDisplayedTopology == null ? DISPLAYED_TOPOLOGY_EDEFAULT : newDisplayedTopology;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreTopologyUIPackage.DISPLAYED_TOPOLOGY_CHOICE__DISPLAYED_TOPOLOGY, oldDisplayedTopology, displayedTopology));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreTopologyUIPackage.DISPLAYED_TOPOLOGY_CHOICE__DISPLAYED_TOPOLOGY:
				return getDisplayedTopology();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreTopologyUIPackage.DISPLAYED_TOPOLOGY_CHOICE__DISPLAYED_TOPOLOGY:
				setDisplayedTopology((DisplayedTopology)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreTopologyUIPackage.DISPLAYED_TOPOLOGY_CHOICE__DISPLAYED_TOPOLOGY:
				setDisplayedTopology(DISPLAYED_TOPOLOGY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreTopologyUIPackage.DISPLAYED_TOPOLOGY_CHOICE__DISPLAYED_TOPOLOGY:
				return displayedTopology != DISPLAYED_TOPOLOGY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (displayedTopology: ");
		result.append(displayedTopology);
		result.append(')');
		return result.toString();
	}

} //DisplayedTopologyChoiceImpl

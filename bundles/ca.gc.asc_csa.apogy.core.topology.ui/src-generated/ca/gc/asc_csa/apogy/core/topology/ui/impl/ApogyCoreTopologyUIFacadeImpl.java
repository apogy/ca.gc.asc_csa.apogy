/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.topology.ui.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.topology.AbstractNodeVisitor;
import ca.gc.asc_csa.apogy.common.topology.INodeVisitor;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.URLNode;
import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import ca.gc.asc_csa.apogy.common.topology.ui.URLNodePresentation;
import ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation;
import ca.gc.asc_csa.apogy.core.invocator.Variable;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyFacade;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIFacadeImpl#getEditedApogySystemAssemblyRoot <em>Edited Apogy System Assembly Root</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ApogyCoreTopologyUIFacadeImpl extends MinimalEObjectImpl.Container implements ApogyCoreTopologyUIFacade 
{
	/**
	 * The cached value of the '{@link #getEditedApogySystemAssemblyRoot() <em>Edited Apogy System Assembly Root</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditedApogySystemAssemblyRoot()
	 * @generated
	 * @ordered
	 */
	protected Node editedApogySystemAssemblyRoot;
	private static ApogyCoreTopologyUIFacade instance = null;
	
	public static ApogyCoreTopologyUIFacade getInstance()
	{
		if(instance == null)
		{
			instance = new ApogyCoreTopologyUIFacadeImpl();
		}
		
		return instance;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApogyCoreTopologyUIFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreTopologyUIPackage.Literals.APOGY_CORE_TOPOLOGY_UI_FACADE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getEditedApogySystemAssemblyRoot() {
		if (editedApogySystemAssemblyRoot != null && editedApogySystemAssemblyRoot.eIsProxy()) {
			InternalEObject oldEditedApogySystemAssemblyRoot = (InternalEObject)editedApogySystemAssemblyRoot;
			editedApogySystemAssemblyRoot = (Node)eResolveProxy(oldEditedApogySystemAssemblyRoot);
			if (editedApogySystemAssemblyRoot != oldEditedApogySystemAssemblyRoot) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE__EDITED_APOGY_SYSTEM_ASSEMBLY_ROOT, oldEditedApogySystemAssemblyRoot, editedApogySystemAssemblyRoot));
			}
		}
		return editedApogySystemAssemblyRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetEditedApogySystemAssemblyRoot() {
		return editedApogySystemAssemblyRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEditedApogySystemAssemblyRoot(Node newEditedApogySystemAssemblyRoot) {
		Node oldEditedApogySystemAssemblyRoot = editedApogySystemAssemblyRoot;
		editedApogySystemAssemblyRoot = newEditedApogySystemAssemblyRoot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE__EDITED_APOGY_SYSTEM_ASSEMBLY_ROOT, oldEditedApogySystemAssemblyRoot, editedApogySystemAssemblyRoot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setVisibility(Variable variable, boolean visible) 
	{
		// Gets the variable's root node.
		Node root = ApogyCoreTopologyFacade.INSTANCE.getVariableOriginNode(variable); 
		if(root != null)
		{
			// Gets the presentation node associated with the root node.
			NodePresentation nodePresentation = ca.gc.asc_csa.apogy.common.topology.ui.Activator.getTopologyPresentationRegistry().getPresentationNode(root);
			if(nodePresentation != null)
			{
				nodePresentation.setVisible(visible);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void toggleVisibility(Variable variable) 
	{
		// Gets the variable's root node.
		Node root = ApogyCoreTopologyFacade.INSTANCE.getVariableOriginNode(variable); 
		if(root != null)
		{
			// Gets the presentation node associated with the root node.
			NodePresentation nodePresentation = ca.gc.asc_csa.apogy.common.topology.ui.Activator.getTopologyPresentationRegistry().getPresentationNode(root);
			if(nodePresentation != null)
			{
				// Toggle visibility flag.
				nodePresentation.setVisible(!nodePresentation.isVisible());
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setPresentationMode(Variable variable, MeshPresentationMode presentationMode) 
	{
		// Gets the variable's root node.
		Node root = ApogyCoreTopologyFacade.INSTANCE.getVariableOriginNode(variable); 
		if(root != null)
		{
			INodeVisitor visitor = new AbstractNodeVisitor() 
			{
				@Override
				public void visit(Node node) 
				{
					if(node instanceof URLNode)
					{
						// Gets the presentation node associated with the node.
						NodePresentation nodePresentation = ca.gc.asc_csa.apogy.common.topology.ui.Activator.getTopologyPresentationRegistry().getPresentationNode(node);

						if(nodePresentation instanceof URLNodePresentation)
						{
							URLNodePresentation urlNodePresentation = (URLNodePresentation) nodePresentation;
							urlNodePresentation.setPresentationMode(presentationMode);
						}
					}
				}
			};

			root.accept(visitor);
		}		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setVisibility(AbstractTypeImplementation abstractTypeImplementation, boolean visible) 
	{
		// Gets the abstractTypeImplementation root node.
		Node root = ApogyCoreTopologyFacade.INSTANCE.getAbstractTypeImplementationOriginNode(abstractTypeImplementation); 
		if(root != null)
		{
			// Gets the presentation node associated with the root node.
			NodePresentation nodePresentation = ca.gc.asc_csa.apogy.common.topology.ui.Activator.getTopologyPresentationRegistry().getPresentationNode(root);
			if(nodePresentation != null)
			{
				nodePresentation.setVisible(visible);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void toggleVisibility(AbstractTypeImplementation abstractTypeImplementation) 
	{
		// Gets the variable's root node.
		Node root = ApogyCoreTopologyFacade.INSTANCE.getAbstractTypeImplementationOriginNode(abstractTypeImplementation); 
		if(root != null)
		{
			// Gets the presentation node associated with the root node.
			NodePresentation nodePresentation = ca.gc.asc_csa.apogy.common.topology.ui.Activator.getTopologyPresentationRegistry().getPresentationNode(root);
			if(nodePresentation != null)
			{
				// Toggle visibility flag.
				nodePresentation.setVisible(!nodePresentation.isVisible());
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setPresentationMode(AbstractTypeImplementation abstractTypeImplementation, MeshPresentationMode presentationMode) 
	{
		// Gets the abstractTypeImplementation root node.
		Node root = ApogyCoreTopologyFacade.INSTANCE.getAbstractTypeImplementationOriginNode(abstractTypeImplementation); 
		if(root != null)
		{
			setPresentationMode(root, presentationMode);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setPresentationMode(Node root, MeshPresentationMode presentationMode) 
	{
		if(root != null)
		{
			INodeVisitor visitor = new AbstractNodeVisitor() 
			{
				@Override
				public void visit(Node node) 
				{
					if(node instanceof URLNode)
					{
						// Gets the presentation node associated with the node.
						NodePresentation nodePresentation = ca.gc.asc_csa.apogy.common.topology.ui.Activator.getTopologyPresentationRegistry().getPresentationNode(node);

						if(nodePresentation instanceof URLNodePresentation)
						{
							URLNodePresentation urlNodePresentation = (URLNodePresentation) nodePresentation;
							urlNodePresentation.setPresentationMode(presentationMode);
						}
					}
				}
			};

			root.accept(visitor);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE__EDITED_APOGY_SYSTEM_ASSEMBLY_ROOT:
				if (resolve) return getEditedApogySystemAssemblyRoot();
				return basicGetEditedApogySystemAssemblyRoot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE__EDITED_APOGY_SYSTEM_ASSEMBLY_ROOT:
				setEditedApogySystemAssemblyRoot((Node)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE__EDITED_APOGY_SYSTEM_ASSEMBLY_ROOT:
				setEditedApogySystemAssemblyRoot((Node)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE__EDITED_APOGY_SYSTEM_ASSEMBLY_ROOT:
				return editedApogySystemAssemblyRoot != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE___SET_VISIBILITY__VARIABLE_BOOLEAN:
				setVisibility((Variable)arguments.get(0), (Boolean)arguments.get(1));
				return null;
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE___TOGGLE_VISIBILITY__VARIABLE:
				toggleVisibility((Variable)arguments.get(0));
				return null;
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__VARIABLE_MESHPRESENTATIONMODE:
				setPresentationMode((Variable)arguments.get(0), (MeshPresentationMode)arguments.get(1));
				return null;
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE___SET_VISIBILITY__ABSTRACTTYPEIMPLEMENTATION_BOOLEAN:
				setVisibility((AbstractTypeImplementation)arguments.get(0), (Boolean)arguments.get(1));
				return null;
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE___TOGGLE_VISIBILITY__ABSTRACTTYPEIMPLEMENTATION:
				toggleVisibility((AbstractTypeImplementation)arguments.get(0));
				return null;
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__ABSTRACTTYPEIMPLEMENTATION_MESHPRESENTATIONMODE:
				setPresentationMode((AbstractTypeImplementation)arguments.get(0), (MeshPresentationMode)arguments.get(1));
				return null;
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__NODE_MESHPRESENTATIONMODE:
				setPresentationMode((Node)arguments.get(0), (MeshPresentationMode)arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //ApogyCoreTopologyUIFacadeImpl

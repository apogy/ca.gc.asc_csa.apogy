/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.topology.ui.impl;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIPackage;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFactory;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage;
import ca.gc.asc_csa.apogy.core.topology.ui.AttachedViewPointPagesProvider;
import ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopology;
import ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopologyChoice;
import org.eclipse.emf.ecore.EAttribute;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCoreTopologyUIPackageImpl extends EPackageImpl implements ApogyCoreTopologyUIPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass apogyCoreTopologyUIFacadeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass displayedTopologyChoiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachedViewPointPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum displayedTopologyEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyCoreTopologyUIPackageImpl() {
		super(eNS_URI, ApogyCoreTopologyUIFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCoreTopologyUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyCoreTopologyUIPackage init() {
		if (isInited) return (ApogyCoreTopologyUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCoreTopologyUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCoreTopologyUIPackageImpl theApogyCoreTopologyUIPackage = (ApogyCoreTopologyUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCoreTopologyUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCoreTopologyUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCoreInvocatorPackage.eINSTANCE.eClass();
		ApogyCommonTopologyUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCoreTopologyUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCoreTopologyUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCoreTopologyUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCoreTopologyUIPackage.eNS_URI, theApogyCoreTopologyUIPackage);
		return theApogyCoreTopologyUIPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApogyCoreTopologyUIFacade() {
		return apogyCoreTopologyUIFacadeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApogyCoreTopologyUIFacade_EditedApogySystemAssemblyRoot() {
		return (EReference)apogyCoreTopologyUIFacadeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCoreTopologyUIFacade__SetVisibility__Variable_boolean() {
		return apogyCoreTopologyUIFacadeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCoreTopologyUIFacade__ToggleVisibility__Variable() {
		return apogyCoreTopologyUIFacadeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCoreTopologyUIFacade__SetPresentationMode__Variable_MeshPresentationMode() {
		return apogyCoreTopologyUIFacadeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCoreTopologyUIFacade__SetVisibility__AbstractTypeImplementation_boolean() {
		return apogyCoreTopologyUIFacadeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCoreTopologyUIFacade__ToggleVisibility__AbstractTypeImplementation() {
		return apogyCoreTopologyUIFacadeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCoreTopologyUIFacade__SetPresentationMode__AbstractTypeImplementation_MeshPresentationMode() {
		return apogyCoreTopologyUIFacadeEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCoreTopologyUIFacade__SetPresentationMode__Node_MeshPresentationMode() {
		return apogyCoreTopologyUIFacadeEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachedViewPointPagesProvider() {
		return attachedViewPointPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDisplayedTopology() {
		return displayedTopologyEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDisplayedTopologyChoice() {
		return displayedTopologyChoiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDisplayedTopologyChoice_DisplayedTopology() {
		return (EAttribute)displayedTopologyChoiceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCoreTopologyUIFactory getApogyCoreTopologyUIFactory() {
		return (ApogyCoreTopologyUIFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		apogyCoreTopologyUIFacadeEClass = createEClass(APOGY_CORE_TOPOLOGY_UI_FACADE);
		createEReference(apogyCoreTopologyUIFacadeEClass, APOGY_CORE_TOPOLOGY_UI_FACADE__EDITED_APOGY_SYSTEM_ASSEMBLY_ROOT);
		createEOperation(apogyCoreTopologyUIFacadeEClass, APOGY_CORE_TOPOLOGY_UI_FACADE___SET_VISIBILITY__VARIABLE_BOOLEAN);
		createEOperation(apogyCoreTopologyUIFacadeEClass, APOGY_CORE_TOPOLOGY_UI_FACADE___TOGGLE_VISIBILITY__VARIABLE);
		createEOperation(apogyCoreTopologyUIFacadeEClass, APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__VARIABLE_MESHPRESENTATIONMODE);
		createEOperation(apogyCoreTopologyUIFacadeEClass, APOGY_CORE_TOPOLOGY_UI_FACADE___SET_VISIBILITY__ABSTRACTTYPEIMPLEMENTATION_BOOLEAN);
		createEOperation(apogyCoreTopologyUIFacadeEClass, APOGY_CORE_TOPOLOGY_UI_FACADE___TOGGLE_VISIBILITY__ABSTRACTTYPEIMPLEMENTATION);
		createEOperation(apogyCoreTopologyUIFacadeEClass, APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__ABSTRACTTYPEIMPLEMENTATION_MESHPRESENTATIONMODE);
		createEOperation(apogyCoreTopologyUIFacadeEClass, APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__NODE_MESHPRESENTATIONMODE);

		displayedTopologyChoiceEClass = createEClass(DISPLAYED_TOPOLOGY_CHOICE);
		createEAttribute(displayedTopologyChoiceEClass, DISPLAYED_TOPOLOGY_CHOICE__DISPLAYED_TOPOLOGY);

		attachedViewPointPagesProviderEClass = createEClass(ATTACHED_VIEW_POINT_PAGES_PROVIDER);

		// Create enums
		displayedTopologyEEnum = createEEnum(DISPLAYED_TOPOLOGY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCommonTopologyPackage theApogyCommonTopologyPackage = (ApogyCommonTopologyPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyPackage.eNS_URI);
		ApogyCoreInvocatorPackage theApogyCoreInvocatorPackage = (ApogyCoreInvocatorPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCoreInvocatorPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogyCommonTopologyUIPackage theApogyCommonTopologyUIPackage = (ApogyCommonTopologyUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyUIPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		attachedViewPointPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyUIPackage.getAbstractViewPointPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(apogyCoreTopologyUIFacadeEClass, ApogyCoreTopologyUIFacade.class, "ApogyCoreTopologyUIFacade", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getApogyCoreTopologyUIFacade_EditedApogySystemAssemblyRoot(), theApogyCommonTopologyPackage.getNode(), null, "editedApogySystemAssemblyRoot", null, 0, 1, ApogyCoreTopologyUIFacade.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getApogyCoreTopologyUIFacade__SetVisibility__Variable_boolean(), null, "setVisibility", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCoreInvocatorPackage.getVariable(), "variable", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "visible", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCoreTopologyUIFacade__ToggleVisibility__Variable(), null, "toggleVisibility", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCoreInvocatorPackage.getVariable(), "variable", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCoreTopologyUIFacade__SetPresentationMode__Variable_MeshPresentationMode(), null, "setPresentationMode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCoreInvocatorPackage.getVariable(), "variable", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonTopologyUIPackage.getMeshPresentationMode(), "presentationMode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCoreTopologyUIFacade__SetVisibility__AbstractTypeImplementation_boolean(), null, "setVisibility", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCoreInvocatorPackage.getAbstractTypeImplementation(), "abstractTypeImplementation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "visible", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCoreTopologyUIFacade__ToggleVisibility__AbstractTypeImplementation(), null, "toggleVisibility", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCoreInvocatorPackage.getAbstractTypeImplementation(), "abstractTypeImplementation", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCoreTopologyUIFacade__SetPresentationMode__AbstractTypeImplementation_MeshPresentationMode(), null, "setPresentationMode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCoreInvocatorPackage.getAbstractTypeImplementation(), "abstractTypeImplementation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonTopologyUIPackage.getMeshPresentationMode(), "presentationMode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCoreTopologyUIFacade__SetPresentationMode__Node_MeshPresentationMode(), null, "setPresentationMode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonTopologyPackage.getNode(), "root", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonTopologyUIPackage.getMeshPresentationMode(), "presentationMode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(displayedTopologyChoiceEClass, DisplayedTopologyChoice.class, "DisplayedTopologyChoice", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDisplayedTopologyChoice_DisplayedTopology(), this.getDisplayedTopology(), "displayedTopology", null, 0, 1, DisplayedTopologyChoice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachedViewPointPagesProviderEClass, AttachedViewPointPagesProvider.class, "AttachedViewPointPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(displayedTopologyEEnum, DisplayedTopology.class, "DisplayedTopology");
		addEEnumLiteral(displayedTopologyEEnum, DisplayedTopology.ASSEMBLY);
		addEEnumLiteral(displayedTopologyEEnum, DisplayedTopology.SYSTEM);

		// Create resource
		createResource(eNS_URI);
	}

} //ApogyCoreTopologyUIPackageImpl

/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.topology.ui.impl;

import javax.vecmath.Matrix3d;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.GeometricUtils;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFactory;
import ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint;
import ca.gc.asc_csa.apogy.common.topology.ui.impl.AbstractViewPointPagesProviderImpl;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage;
import ca.gc.asc_csa.apogy.core.topology.ui.AttachedViewPointPagesProvider;
import ca.gc.asc_csa.apogy.core.topology.ui.wizards.AttachedViewPointWizardPage;

/**
 * <!-- begin-user-doc -->
 *er</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AttachedViewPointPagesProviderImpl extends AbstractViewPointPagesProviderImpl implements AttachedViewPointPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttachedViewPointPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreTopologyUIPackage.Literals.ATTACHED_VIEW_POINT_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{		
		AttachedViewPoint attachedViewPoint = ApogyCommonTopologyFactory.eINSTANCE.createAttachedViewPoint();
		attachedViewPoint.setPosition(ApogyCommonMathFacade.INSTANCE.createTuple3d(0, 0, 0));
		attachedViewPoint.setAllowRotation(true);
		
		Matrix3d rotationMatrix = GeometricUtils.packXYZ(Math.toRadians(90), Math.toRadians(90), 0);		
		attachedViewPoint.setRotationMatrix(ApogyCommonMathFacade.INSTANCE.createMatrix3x3(rotationMatrix));
		
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings = (MapBasedEClassSettings) settings;
			attachedViewPoint.setName((String) mapBasedEClassSettings.getUserDataMap().get("name"));						
		}
			
		return attachedViewPoint;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	

		list.add(new AttachedViewPointWizardPage((AttachedViewPoint) eObject));
		
		return list;
	}
} //AttachedViewPointPagesProviderImpl

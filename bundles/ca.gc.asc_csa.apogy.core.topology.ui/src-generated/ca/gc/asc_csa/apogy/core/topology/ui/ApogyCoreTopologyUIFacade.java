/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.topology.ui;

import ca.gc.asc_csa.apogy.common.topology.Node;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;
import ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation;
import ca.gc.asc_csa.apogy.core.invocator.Variable;
import ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#getEditedApogySystemAssemblyRoot <em>Edited Apogy System Assembly Root</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage#getApogyCoreTopologyUIFacade()
 * @model
 * @generated
 */
public interface ApogyCoreTopologyUIFacade extends EObject {

	/**
	 * Returns the value of the '<em><b>Edited Apogy System Assembly Root</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edited Apogy System Assembly Root</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The root node of the Apogy System full assembly being edited.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Edited Apogy System Assembly Root</em>' reference.
	 * @see #setEditedApogySystemAssemblyRoot(Node)
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage#getApogyCoreTopologyUIFacade_EditedApogySystemAssemblyRoot()
	 * @model transient="true"
	 * @generated
	 */
	Node getEditedApogySystemAssemblyRoot();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#getEditedApogySystemAssemblyRoot <em>Edited Apogy System Assembly Root</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Edited Apogy System Assembly Root</em>' reference.
	 * @see #getEditedApogySystemAssemblyRoot()
	 * @generated
	 */
	void setEditedApogySystemAssemblyRoot(Node value);

	/**
	 * @generated_NOT
	*/
	public static ApogyCoreTopologyUIFacade INSTANCE = ApogyCoreTopologyUIFacadeImpl.getInstance();

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Sets a specified Variable's topology visibility.
	 * @param variable The specified Variable.
	 * @param visible True to show the variable topology, false otherwise.
	 * <!-- end-model-doc -->
	 * @model variableUnique="false" visibleUnique="false"
	 * @generated
	 */
	void setVisibility(Variable variable, boolean visible);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Toggles the visibility of the topology of a specified Variable.
	 * @param variable The specified variable.
	 * <!-- end-model-doc -->
	 * @model variableUnique="false"
	 * @generated
	 */
	void toggleVisibility(Variable variable);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Sets the presentation mode (Point, Wireframe, Surface) of all URLNode in the topology associated with a specified Variable.
	 * @param variable The specified Variable.
	 * @param presentationMode The presentation mode of the geometry.
	 * <!-- end-model-doc -->
	 * @model variableUnique="false" presentationModeUnique="false"
	 * @generated
	 */
	void setPresentationMode(Variable variable, MeshPresentationMode presentationMode);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Sets a specified AbstractTypeImplementation topology visibility.
	 * @param variable The specified AbstractTypeImplementation.
	 * @param visible True to show the AbstractTypeImplementation topology, false otherwise.
	 * <!-- end-model-doc -->
	 * @model abstractTypeImplementationUnique="false" visibleUnique="false"
	 * @generated
	 */
	void setVisibility(AbstractTypeImplementation abstractTypeImplementation, boolean visible);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Toggles the visibility of the topology of a specified AbstractTypeImplementation.
	 * @param abstractTypeImplementation The specified AbstractTypeImplementation.
	 * <!-- end-model-doc -->
	 * @model abstractTypeImplementationUnique="false"
	 * @generated
	 */
	void toggleVisibility(AbstractTypeImplementation abstractTypeImplementation);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Sets the presentation mode (Point, Wireframe, Surface) of all URLNode in the topology associated with a specified AbstractTypeImplementation.
	 * @param variable The specified AbstractTypeImplementation.
	 * @param presentationMode The presentation mode of the geometry.
	 * <!-- end-model-doc -->
	 * @model abstractTypeImplementationUnique="false" presentationModeUnique="false"
	 * @generated
	 */
	void setPresentationMode(AbstractTypeImplementation abstractTypeImplementation, MeshPresentationMode presentationMode);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Sets the presentation mode (Point, Wireframe, Surface) of all URLNode in the topology specified by its root.
	 * @param root The specified root.
	 * @param presentationMode The presentation mode of the geometry.
	 * <!-- end-model-doc -->
	 * @model rootUnique="false" presentationModeUnique="false"
	 * @generated
	 */
	void setPresentationMode(Node root, MeshPresentationMode presentationMode);
} // ApogyCoreTopologyUIFacade

package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.jme3.scene_objects;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;

import com.jme3.asset.AssetManager;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.util.BufferUtils;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects.DefaultJME3SceneObject;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.scene_objects.EarthOrbitModelPassToolNodeSceneObject;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.VisibilityPass;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.VisibilityPassSpacecraftPosition;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.VisibilityPassSpacecraftPositionHistory;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.jme3.SurfaceEnvironmentJMEConstants;

public class EarthOrbitModelPassToolNodeJME3Object extends DefaultJME3SceneObject<EarthOrbitModelPassToolNode> implements IPropertyChangeListener, EarthOrbitModelPassToolNodeSceneObject
{							
	public static final ColorRGBA DEFAULT_GRID_COLOR = new ColorRGBA(1.0f, 0.0f, 0.0f, 1.0f);
	
	public static final float DISPLAY_RADIUS  = (float) (SurfaceEnvironmentJMEConstants.CELESTIAL_SPHERE_RADIUS * 0.75f);
	
	private EarthOrbitModelPassTool earthOrbitModelPassTool;
	
	private Adapter adapter;		
	private AssetManager assetManager;	
	
	private Geometry trajectoryGeometry = null;
	
	public EarthOrbitModelPassToolNodeJME3Object(EarthOrbitModelPassToolNode node, JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{
		super(node, jme3RenderEngineDelegate);		
				
		this.assetManager = jme3Application.getAssetManager();
		assetManager.registerLocator("/", FileLocator.class);
		
		earthOrbitModelPassTool = node.getEarthOrbitModelPassTool();
							
		Job job = new Job("EarthOrbitModelPassToolNodeJME3Object initialize.")
		{
			@Override
			protected IStatus run(IProgressMonitor monitor) 
			{														
				jme3Application.enqueue(new Callable<Object>() 
				{
					@Override
					public Object call() throws Exception 
					{			
						createGeometry();	
						getTopologyNode().getEarthOrbitModelPassTool().eAdapters().add(EarthOrbitModelPassToolNodeJME3Object.this.getAdapter());
						return null;
					}
				});
				return Status.OK_STATUS;				
			}
		};
		job.schedule();
		
		// Activator.getDefault().getPreferenceStore().addPropertyChangeListener(this);		
	}
	
	@Override
	public void updateGeometry(float tpf) 
	{
		// Detach previous trajectory geometry
		if(trajectoryGeometry != null) getAttachmentNode().detachChild(trajectoryGeometry);				
		
		// Adds the trajectory
		trajectoryGeometry = createPassTrajectoryGeometry();		
		getAttachmentNode().attachChild(trajectoryGeometry);
	}
	
	@Override
	public void dispose() 
	{		
		if(getTopologyNode() != null && getTopologyNode().getEarthOrbitModelPassTool() != null)
		{
			getTopologyNode().getEarthOrbitModelPassTool().eAdapters().remove(EarthOrbitModelPassToolNodeJME3Object.this.getAdapter());
		}
		super.dispose();
	}	
	
	@Override
	public void propertyChange(final PropertyChangeEvent event) 
	{				
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{
				// TODO
				return null;
			}
		});		
	}
							
	@Override
	public List<Geometry> getGeometries() 
	{		
		List<Geometry> geometries = new ArrayList<Geometry>();
		
		geometries.add(trajectoryGeometry);	
		
		return geometries;
	}
	
	private void createGeometry()
	{
		// Adds the trajectory
		trajectoryGeometry = createPassTrajectoryGeometry();		
		getAttachmentNode().attachChild(trajectoryGeometry);
	}
	
	private Geometry createPassTrajectoryGeometry()
	{
		VisibilityPass visibilityPass = earthOrbitModelPassTool.getDisplayedPass(); 
		
		Geometry geometry = null;
		
		if(visibilityPass == null)
		{
			Mesh mesh = new Mesh();
			mesh.setMode(Mesh.Mode.Lines);	
			geometry = new Geometry("PassTrajectory", mesh);			
		}
		else
		{
			Mesh mesh = new Mesh();
			mesh.setMode(Mesh.Mode.Lines);				
			geometry = new Geometry("PassTrajectory", mesh);
			
			List<Vector3f> verticesList = new ArrayList<Vector3f>();
			List<Integer> indexesList = new ArrayList<Integer>();
			
			VisibilityPassSpacecraftPositionHistory history = visibilityPass.getPositionHistory();
			int index = 0;
			
			double azimuth = 0;
			double elevation = 0;
			
			for(VisibilityPassSpacecraftPosition position : history.getPositions())
			{
				azimuth = position.getAzimuth();				
				elevation = position.getElevation();
				
				double r = Math.cos(elevation) * DISPLAY_RADIUS;				
				
				float x = (float) ( r * Math.cos(-azimuth));
				float y = (float) ( r * Math.sin(-azimuth));
				float z = (float) ( DISPLAY_RADIUS * Math.sin(elevation));
					
				Vector3f p0 = new Vector3f(x, y, z);				
				
				// Adds the point
				verticesList.add(p0);				
				
				if(index > 0)
				{
					indexesList.add(index - 1);
					indexesList.add(index);
				}
				index++;
			}
			
			mesh.setBuffer( com.jme3.scene.VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(JME3Utilities.convertToFloatArray(verticesList)));
			mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.Index, 2, BufferUtils.createIntBuffer(JME3Utilities.convertToIntArray(indexesList)));								
			mesh.updateBound();
			mesh.updateCounts();						
		}		
		
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", DEFAULT_GRID_COLOR.clone());
        geometry.setMaterial(mat);
        geometry.setShadowMode(ShadowMode.Off);
        
        return geometry;
	}	
	
	private Adapter getAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{					
					if(msg.getNotifier() instanceof EarthOrbitModelPassTool)
					{
						int featureId = msg.getFeatureID(EarthOrbitModelPassTool.class);
						switch (featureId) 
						{
							case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS:
								requestUpdate();
							break;
							
							case ApogyAddonsPackage.SIMPLE3_DTOOL__VISIBLE:
							{
								boolean newVisible = msg.getNewBooleanValue();
								setVisible(newVisible);
							}

							default:
							break;
						}												
					}
				}
			};
		}
		
		return adapter;
	}
}

// *****************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
//     Pierre Allard - initial API and implementation
//     Sebastien Gemme
//        
// SPDX-License-Identifier: EPL-1.0
// *****************************************************************************
@GenModel(prefix="ApogyCoreTopology",
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  multipleEditorPages="false",
copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Pierre Allard - initial API and implementation
     Sebastien Gemme
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",	
		  modelName="ApogyCoreTopology")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.core.topology/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.core.topology.edit/src-generated")

package ca.gc.asc_csa.apogy.core.topology


import ca.gc.asc_csa.apogy.common.topology.Node
import ca.gc.asc_csa.apogy.common.topology.ReferencedGroupNode
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment
import ca.gc.asc_csa.apogy.core.ResultsListNode
import ca.gc.asc_csa.apogy.core.environment.WorksiteNode
import ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation
import ca.gc.asc_csa.apogy.core.invocator.Variable
import ca.gc.asc_csa.apogy.core.ApogyTopology
import ca.gc.asc_csa.apogy.core.FeatureOfInterestNode
import ca.gc.asc_csa.apogy.core.FeatureOfInterest
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession

// Types

type SortedSet<T> wraps java.util.SortedSet 


/* 
 * 
 * ApogyCoreTopologyFacade Singleton.
 * 
 */ 
class ApogyCoreTopologyFacade
{
	/*
	 * Returns the topology root associated with a specified Variable.
	 * @param variable The specified variable.
	 * @return The topology root associated with the variable, null if none is found.
	 */
	op Node getVariableOriginNode(Variable variable) 
		
	/*
	 * Return the topology root associated with a specified AbstractTypeImplementation.
	 * @param abstractTypeImplementation The specified AbstractTypeImplementation.
	 * @return The topology root associated with the AbstractTypeImplementation, null if none is found.
	 */
	op Node getAbstractTypeImplementationOriginNode(AbstractTypeImplementation abstractTypeImplementation)
	
	/*
	 * Creates the ApogyEnvironmentNode associated with a specified ApogyEnvironment.
	 * @param apogyEnvironment The specified ApogyEnvironment.
	 * @return The ApogyEnvironmentNode.
	 */
	op ApogyEnvironmentNode createApogyEnvironmentNode(ApogyEnvironment apogyEnvironment)
	
	/*
	 * Apogy Topology Singleton.
	 */
	@GenModel(children="true", notify="true", property="None")
	refers derived transient ApogyTopology[0..1] apogyTopology
	
	/* 
	 * Initializes the {@link ApogyTopology}.
	 * @param environment Refers the loaded environment.
	 */
	op void initApogyTopology(ApogyEnvironment environment)

	/* 
	 * Disposes the {@link ApogyTopology}.
	 */	
	op void disposeApogyTopology()
	
	/*
	 * Finds the FeatureOfInterestNode associated with a specified FeatureOfInterest in the active session.
	 * @param featureOfInterest The specified FeatureOfInterest.
	 * @return The FeatureOfInterestNode associated with the specified FeatureOfInterest, null if none is found.
	 */
	op FeatureOfInterestNode getFeatureOfInterestNode(FeatureOfInterest featureOfInterest)			 
}

/*
 * This controller listens the {@link ApogyCoreInvocatorFacade#activeInvocatorSession} and 
 * sets the {@link ApogyTopology}
 */
class ApogyTopologyController{
	/*
	 * Initializes the {@link ApogyCoreInvocatorFacade#activeInvocatorSession} listener.
	 */
	op void init()
	
	/*
	 * Disposes the {@link ApogyCoreInvocatorFacade#activeInvocatorSession} listener.
	 */	
	op void dispose()
	
	/*
	 * Initializes or clear the active Apogy Topology
	 * @param session Reference to the session.  If not null, the topology will be initialized.  
	 * Otherwise, the topology will be disposed. 
	 */
	op void initApogyTopology(InvocatorSession session) 
}

class SystemsTopologyAdapter
{
	/*
	 * The Deployment to monitor.
	 */
	refers transient ApogyEnvironment deployment	
	
	/*
	 * The topology representing the Systems found in the Deployment.
	 */
	refers transient ReferencedGroupNode systemsGroup	
}


/* -------------------------------------------------------------------------
 * Specialized Nodes.
 * ------------------------------------------------------------------------- */ 

/**
 * A specialized Topology node representing the environment.
 */
class ApogyEnvironmentNode extends ReferencedGroupNode
{
	refers transient ApogyEnvironment apogyEnvironment
		
	refers derived transient WorksiteNode[1] worksiteNode
	
	refers derived transient readonly ResultsListNode[1] resultsListNode
		
	refers derived transient readonly ApogySystemAPIsNode[1] apogySystemAPIsNode
	
	op void dispose()
}

/**
 * A specialized Topology node referring to all Systems.
 */
class ApogySystemAPIsNode extends ReferencedGroupNode
{
	refers transient ApogyEnvironment apogyEnvironment
}

/**
 * A specialized Node that represent a temporary node used for assemble of system and sub-systems.
 */
class TemporaryAssemblyNode extends ReferencedGroupNode
{	
}
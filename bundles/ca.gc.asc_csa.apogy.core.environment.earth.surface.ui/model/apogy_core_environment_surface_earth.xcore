// *****************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
//     Pierre Allard - initial API and implementation
//     Regent L'Archeveque
//     Sebastien Gemme
//        
// SPDX-License-Identifier: EPL-1.0
// *****************************************************************************
@GenModel(prefix="ApogyCoreEnvironmentSurfaceEarthUI",
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  multipleEditorPages="false",
copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Pierre Allard - initial API and implementation
     Regent L'Archeveque 
     Sebastien Gemme
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",	
		  modelName="ApogyCoreEnvironmentUI",
		  complianceLevel="6.0",
		  suppressGenModelAnnotations="false",
		  dynamicTemplates="true", 
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.core.environment.earth.surface.ui/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.edit/src-generated")

package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider
import ca.gc.asc_csa.apogy.core.environment.surface.ui.AbstractSurfaceWorksitePresentation
import ca.gc.asc_csa.apogy.core.environment.surface.ui.AbstractSurfaceWorksiteSkyPresentation
import ca.gc.asc_csa.apogy.common.topology.Node
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation
import ca.gc.asc_csa.apogy.addons.AbstractTwoPoints3DTool


// Types
type Color3f wraps javax.vecmath.Color3f
type Point3f wraps javax.vecmath.Point3f

/**
 * Defines the display properties associated with an EarthSurfaceWorksite. 
 */
class EarthSurfaceWorksitePresentation extends AbstractSurfaceWorksitePresentation
{
}


/**
 * Defines the display properties associated with an EarthSky. 
 */
class EarthSkyPresentation extends AbstractSurfaceWorksiteSkyPresentation
{
}

/**
 * Defines the display properties associated with the Moon in the EarthSky. 
 */
class MoonPresentation extends NodePresentation
{
	
}

// Tools
/**
 * A 3D tool used to show the direction and intensity of the Sun.
 */

class SunVector3DTool extends AbstractTwoPoints3DTool
{
	/* The vector color. */
	@GenModel(propertyCategory="SUN_VECTOR_PROPERTIES")
	Color3f vectorColor = "1.0,1.0,0.0"

	/* The color of the sun intensity level. */
	@GenModel(propertyCategory="SUN_VECTOR_PROPERTIES")
	Color3f sunIntensityLevelColor = "0.0,1.0,0.0"
		
	/* Length of the vector. */
	@GenModel(notify="true", apogy_units = "m", propertyCategory="SUN_VECTOR_PROPERTIES")
	double vectorLength = "10.0"
	
	/* Radius of sphere used as end point to vector. */
	@GenModel(notify="true", apogy_units = "m", propertyCategory="SUN_VECTOR_PROPERTIES")	
	double endPointRadius = "0.01"
	
	/* The current sun intensity. */
	@GenModel(notify="true", apogy_units = "W/m*m", property="Readonly", propertyCategory="SUN_VECTOR_LIGHT_PROPERTIES")	
	double sunIntensity = "0.0"
	
	/* The current day maximum sun intensity. */
	@GenModel(notify="true", apogy_units = "W/m*m", property="Readonly", propertyCategory="SUN_VECTOR_LIGHT_PROPERTIES")	
	double currentDayMaximumSunIntensity = "0.0"
	
	/* The current sun intensity. */
	@GenModel(notify="true", apogy_units = "%", property="Readonly", propertyCategory="SUN_VECTOR_LIGHT_PROPERTIES")	
	double currentSunIntensityPercentage = "0.0"
		
	/* Topology Node associated with the tool. */
	@GenModel(notify="true", children="true", property="Readonly", propertyCategory="SUN_VECTOR_PROPERTIES")	
	refers transient SunVector3DToolNode sunVector3DToolNode opposite sunVector3DTool
	
}


class SunVector3DToolNode extends Node
{	
	refers transient SunVector3DTool sunVector3DTool opposite sunVector3DToolNode	
}

class SunVector3DToolNodePresentation extends NodePresentation
{
	
}

// Utilities

class EarthSurfaceUIUtilities
{
	/*
	 * Return the sun color for a given altitude above the horizon.
	 * @param sunAltitude The altitude above the horizon, in radians.
	 */
	op Color3f getSunLightColor(@GenModel(apogy_units="rad") double sunAltitude)
	
	/*
	 * Return the transparency value (between no transparency 0 and full transparency 1.0)
	 * to be used for the blus sky as a function of the sun altitude angle.
	 * @param sunAltitude The altitude above the horizon, in radians.
	 */
	op double getSkyTransparency(@GenModel(apogy_units="rad") double sunAltitude)
	
	/*
	 * Return the sky color for a given altitude above the horizon.
	 * @param sunAltitude The altitude above the horizon, in radians.
	 */
	op Color3f getSkyColor(@GenModel(apogy_units="rad") double sunAltitude)
}

class EarthSurfaceWorksiteSettings extends EClassSettings
{
	/*
	 * Name to give the EarthSurfaceWorksite.
	 */
	String name
}

class EarthSurfaceWorksiteWizardPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

// Wizard Support for SunVectorTool.
class SunVector3DToolWizardPagesProvider extends NamedDescribedWizardPagesProvider
{	
}


package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.parts;

import java.util.HashMap;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.ApogyCoreEnvironmentSurfaceEarthUIRCPConstants;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.DEMsComposite;

public class DEMsPart extends AbstractEObjectSelectionPart 
{
	private DEMsComposite demsComposite;
	private Map displayedMap = null;
	
	public Map getDisplayedMap() {
		return displayedMap;
	}

	@Override
	protected void setCompositeContents(EObject eObject) 
	{
		if(eObject instanceof Map)
		{
			displayedMap = (Map) eObject;
			demsComposite.setMap(displayedMap);
		}
	}

	@Override
	protected void createContentComposite(Composite parent, int style) {
		
		demsComposite = new DEMsComposite(parent, SWT.NONE);
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() 
	{	
		HashMap<String, ISelectionListener> map = new HashMap<>();

		map.put(ApogyCoreEnvironmentSurfaceEarthUIRCPConstants.PART__MAPS__ID, new ISelectionListener() {
			@Override
			public void selectionChanged(MPart part, Object selection) 
			{
				if (selection instanceof Map) 
				{
					Map map = (Map) selection;										
					setEObject(map);
				}
			}
		});

		return map;
	}

}

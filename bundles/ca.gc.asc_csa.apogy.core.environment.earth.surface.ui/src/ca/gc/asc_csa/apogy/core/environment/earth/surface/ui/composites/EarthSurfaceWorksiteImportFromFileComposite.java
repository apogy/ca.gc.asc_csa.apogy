package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites;

import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.Activator;

public class EarthSurfaceWorksiteImportFromFileComposite extends Composite 
{
	public static String path = System.getProperty("user.home");
	
	private EarthSurfaceWorksite selectedEarthSurfaceWorksite;
	
	private Button btnBrowse;
	private Text text;
	
	public EarthSurfaceWorksiteImportFromFileComposite(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(3, false));
		
		Label lblFile = new Label(this, SWT.NONE);
		lblFile.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFile.setText("File:");
		
		text = new Text(this, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		text.addSelectionListener(new SelectionListener() 
		{		
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				try
				{
					loadEarthSurfaceWorksite(text.getText());															
				}
				catch(Throwable t)
				{
					String errorMessage = "Failed to import Earth Surface Worksite from file<" + text.getText() + ">!";
					Logger.INSTANCE.log(Activator.ID, this, errorMessage, EventSeverity.ERROR, t);					
					MessageDialog.openError(getShell(), "Error", errorMessage);				
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {								
			}
		});
		
		btnBrowse = new Button(this, SWT.NONE);
		btnBrowse.setText("Browse...");	
		
		btnBrowse.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				FileDialog fd = new FileDialog(getShell(), SWT.OPEN);
				fd.setFilterPath(path);
				fd.setFilterExtensions(new String[]{"*.ws", "*.sws"});				
				fd.setOverwrite(true);
				
				String filePath = fd.open();		
				
				if(filePath != null && filePath.length() > 0)
				{
					try
					{
						loadEarthSurfaceWorksite(filePath);					
						text.setText(filePath);
					}
					catch(Throwable t)
					{				
						setSelectedEarthSurfaceWorksite(null);		
						
						String errorMessage = "Failed to import Earth Surface Worksite from file<" + filePath + ">!";
						Logger.INSTANCE.log(Activator.ID, this, errorMessage, EventSeverity.ERROR, t);					
						MessageDialog.openError(getShell(), "Error", errorMessage);
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) 
			{				
			}
		});
	}
	
	private void setSelectedEarthSurfaceWorksite(EarthSurfaceWorksite selectedEarthSurfaceWorksite)
	{
		this.selectedEarthSurfaceWorksite = selectedEarthSurfaceWorksite;
		newEarthSurfaceWorksiteSelected(selectedEarthSurfaceWorksite);
	}
	
	public EarthSurfaceWorksite getSelectedEarthSurfaceWorksite()
	{
		return selectedEarthSurfaceWorksite;
	}	
	
	private void loadEarthSurfaceWorksite(String filePath) throws Throwable
	{
		if(filePath != null)
		{
			String urlString = filePath; 	
			
			Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
			Map<String, Object> m = reg.getExtensionToFactoryMap();
			m.put("key", new XMIResourceFactoryImpl());
			ResourceSet resSet = new ResourceSetImpl();
			
			Resource resource = resSet.createResource(URI.createFileURI(urlString));
			resource.load(m);
			EarthSurfaceWorksite earthSurfaceWorksite = (EarthSurfaceWorksite) resource.getContents().get(0);
															
			setSelectedEarthSurfaceWorksite(earthSurfaceWorksite);			
		}	
	}
	
	/**
	 * Method called when a EarthSurfaceWorksite is selected. 
	 * @param earthSurfaceWorksite The selected EarthSurfaceWorksite, can be null.
	 */
	protected void newEarthSurfaceWorksiteSelected(EarthSurfaceWorksite earthSurfaceWorksite)
	{		
	}
}

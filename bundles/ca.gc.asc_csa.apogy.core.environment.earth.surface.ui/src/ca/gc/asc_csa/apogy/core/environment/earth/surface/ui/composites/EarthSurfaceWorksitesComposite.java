package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;

public class EarthSurfaceWorksitesComposite extends Composite 
{
	private ApogyEnvironment apogyEnvironment;
	
	private FormToolkit formToolkit = new FormToolkit(Display.getCurrent());	
	private EarthSurfaceWorksiteListComposite earthSurfaceWorksiteListComposite;
	private EarthSurfaceWorksiteOriginComposite earthSurfaceWorksiteOriginComposite;
	private EarthSurfaceWorksiteDetailsComposite earthSurfaceWorksiteDetailsComposite;
	
	public EarthSurfaceWorksitesComposite(Composite parent, int style) 
	{
		super(parent, style);	
		
		addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				formToolkit.dispose();
			}
		});
		setLayout(new GridLayout(1, true));
		
		ScrolledForm scrldfrmNewScrolledform = formToolkit.createScrolledForm(this);
		scrldfrmNewScrolledform.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrldfrmNewScrolledform.setShowFocusedControl(true);
		formToolkit.paintBordersFor(scrldfrmNewScrolledform);
		scrldfrmNewScrolledform.setText(null);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			tableWrapLayout.numColumns = 2;
			scrldfrmNewScrolledform.getBody().setLayout(tableWrapLayout);
		}
		
		
		// Worksites Section.
		Section sctnWorksites = formToolkit.createSection(scrldfrmNewScrolledform.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnWorksites = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 2, 1);
		twd_sctnWorksites.valign = TableWrapData.FILL;		
		twd_sctnWorksites.grabVertical = true;
		sctnWorksites.setLayoutData(twd_sctnWorksites);
		formToolkit.paintBordersFor(sctnWorksites);
		sctnWorksites.setText("Earth Surface Worksites");
		
		earthSurfaceWorksiteListComposite = new EarthSurfaceWorksiteListComposite(sctnWorksites, SWT.NONE) 
		{
			@Override
			protected void newSelection(EarthSurfaceWorksite earthSurfaceWorksite) 
			{
				earthSurfaceWorksiteOriginComposite.setEarthSurfaceWorksite(earthSurfaceWorksite);
				earthSurfaceWorksiteDetailsComposite.setEarthSurfaceWorksite(earthSurfaceWorksite);				
				newEarthSurfaceWorksiteSelected(earthSurfaceWorksite);
			}
		};
		formToolkit.adapt(earthSurfaceWorksiteListComposite);
		formToolkit.paintBordersFor(earthSurfaceWorksiteListComposite);
		sctnWorksites.setClient(earthSurfaceWorksiteListComposite);
	
		// Worksite Origin.
		Section sctnOrigin = formToolkit.createSection(scrldfrmNewScrolledform.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnOrigin = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_sctnOrigin.align = TableWrapData.LEFT;
		sctnOrigin.setLayoutData(twd_sctnOrigin);
		formToolkit.paintBordersFor(sctnOrigin);
		sctnOrigin.setText("Worksite Origin");
		sctnOrigin.setExpanded(true);
		
		earthSurfaceWorksiteOriginComposite = new EarthSurfaceWorksiteOriginComposite(sctnOrigin, SWT.NONE);
		formToolkit.adapt(earthSurfaceWorksiteOriginComposite);
		formToolkit.paintBordersFor(earthSurfaceWorksiteOriginComposite);
		sctnOrigin.setClient(earthSurfaceWorksiteOriginComposite);
		
		
		// Worksite Details.
		Section sctnDetails = formToolkit.createSection(scrldfrmNewScrolledform.getBody(),Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnDetails = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_sctnDetails.align = TableWrapData.LEFT;
		sctnDetails.setLayoutData(twd_sctnDetails);
		formToolkit.paintBordersFor(sctnDetails);
		sctnDetails.setText("Worksite Details");
		sctnDetails.setExpanded(false);
		
		earthSurfaceWorksiteDetailsComposite = new EarthSurfaceWorksiteDetailsComposite(sctnDetails, SWT.NONE);	
		formToolkit.adapt(earthSurfaceWorksiteDetailsComposite);
		formToolkit.paintBordersFor(earthSurfaceWorksiteDetailsComposite);
		sctnDetails.setClient(earthSurfaceWorksiteDetailsComposite);		
		
	}

	public ApogyEnvironment getApogyEnvironment() 
	{
		return apogyEnvironment;
	}

	public void setApogyEnvironment(ApogyEnvironment apogyEnvironment) 
	{
		this.apogyEnvironment = apogyEnvironment;
		
		if(apogyEnvironment != null)
		{
			earthSurfaceWorksiteListComposite.setWorksitesList(apogyEnvironment.getWorksitesList());
		}
		else
		{
			earthSurfaceWorksiteListComposite.setWorksitesList(null);
		}
	}
	
	protected void newEarthSurfaceWorksiteSelected(EarthSurfaceWorksite earthSurfaceWorksite)
	{		
	}
	
}

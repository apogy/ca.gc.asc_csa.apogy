package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.SunVector3DTool;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites.SunVector3DToolComposite;

public class SunVector3DToolWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.wizards.SunVector3DToolWizardPage";
	
	private SunVector3DTool sunVector3DTool;
	private SunVector3DToolComposite sunVector3DToolComposite;
	
	public SunVector3DToolWizardPage(SunVector3DTool sunVector3DTool) 
	{
		super(WIZARD_PAGE_ID);
		this.sunVector3DTool = sunVector3DTool;
		
		setTitle("Sun Vector 3D Tool");
		setDescription("Configure the Sun Vector Tool settings.");
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(1, false));
		
		sunVector3DToolComposite = new SunVector3DToolComposite(container, SWT.None);
		sunVector3DToolComposite.setSunVector3DTool(sunVector3DTool);
		sunVector3DToolComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		
		setControl(container);
		sunVector3DToolComposite.setFocus();
		
		setPageComplete(true);
	}	
}

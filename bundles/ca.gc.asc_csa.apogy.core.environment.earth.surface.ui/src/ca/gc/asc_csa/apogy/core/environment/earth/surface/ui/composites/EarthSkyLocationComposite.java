package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.text.DecimalFormat;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.EarthWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky;

public class EarthSkyLocationComposite extends Composite 
{
	public static String NO_VALUE_AVAILABLE_STRING = "N/A";
	
	public static final String LONGITUDE_FORMAT_STRING = "0.000";
	public static final String LATITUDE_FORMAT_STRING = "0.000";
	public static final String ALTITUDE_FORMAT_STRING = "0.0";
		
	private static int LABEL_WIDTH = 100; 
	private static int VALUE_WIDTH = 75; 
	private static int BUTTON_WIDTH = 30; 		

	
	// Earth Sky
	private EarthSky earthSky= null;

	// Location	
	private TypedElementSimpleUnitsComposite latitudeValueLabel = null;
	private TypedElementSimpleUnitsComposite longitudeValueLabel = null;	
	private TypedElementSimpleUnitsComposite altitudeValueLabel = null;	
	
	private DataBindingContext m_bindingContext;		
	
	public EarthSkyLocationComposite(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(1, true));
		
		EarthWorksite worksite = null;
		if(getEarthSky() != null)
		{
			worksite = (EarthWorksite) getEarthSky().getWorksite();
		}	
		
		// Latitude
		GridData gridDataLatitude = new GridData();
		gridDataLatitude.grabExcessHorizontalSpace = false;		
		gridDataLatitude.horizontalAlignment = SWT.LEFT;
		latitudeValueLabel = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat(LATITUDE_FORMAT_STRING);			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Latitude:";
			}
			
			@Override
			protected boolean isFeatureEditable() 
			{
				return false;
			}
		};			
				
		latitudeValueLabel.setTypedElement(FeaturePath.fromList(ApogyEarthEnvironmentPackage.Literals.EARTH_WORKSITE__GEOGRAPHICAL_COORDINATES, ApogyEarthEnvironmentPackage.Literals.GEOGRAPHIC_COORDINATES__LATITUDE), worksite);
		latitudeValueLabel.setLayoutData(gridDataLatitude);								
		
		// Longitude		
		GridData gridDataLongitude = new GridData();
		gridDataLongitude.grabExcessHorizontalSpace = false;		
		gridDataLongitude.horizontalAlignment = SWT.LEFT;
		longitudeValueLabel = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat(LONGITUDE_FORMAT_STRING);			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Longitude:";
			}
			
			@Override
			protected boolean isFeatureEditable() 
			{
				return false;
			}
		};
		longitudeValueLabel.setTypedElement(FeaturePath.fromList(ApogyEarthEnvironmentPackage.Literals.EARTH_WORKSITE__GEOGRAPHICAL_COORDINATES, ApogyEarthEnvironmentPackage.Literals.GEOGRAPHIC_COORDINATES__LONGITUDE), worksite);
		longitudeValueLabel.setLayoutData(gridDataLongitude);
		
		// Altitude		
		GridData gridDataAltitude = new GridData();
		gridDataAltitude.grabExcessHorizontalSpace = false;
		gridDataAltitude.horizontalAlignment = SWT.LEFT;
		altitudeValueLabel = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat(ALTITUDE_FORMAT_STRING);			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Altitude:";
			}
			
			@Override
			protected boolean isFeatureEditable() 
			{
				return false;
			}
		};
		altitudeValueLabel.setLayoutData(gridDataAltitude);
		altitudeValueLabel.setTypedElement(FeaturePath.fromList(ApogyEarthEnvironmentPackage.Literals.EARTH_WORKSITE__GEOGRAPHICAL_COORDINATES, ApogyEarthEnvironmentPackage.Literals.GEOGRAPHIC_COORDINATES__ELEVATION), worksite);
		
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
	
	public EarthSky getEarthSky() {
		return earthSky;
	}
	
	public void setEarthSky(EarthSky earthSky) 
	{		
		setEarthSky(earthSky, true);		
	}
	
	public void setEarthSky(EarthSky newEarthSky, boolean update) 
	{
		// Updates EarthSky
		this.earthSky = newEarthSky;
		
		if (update) 
		{
			if (m_bindingContext != null) {
				m_bindingContext.dispose();
				m_bindingContext = null;
			}
			if (newEarthSky != null) {
				m_bindingContext = initDataBindings();
			}
		}
	}
		
	protected DataBindingContext initDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		EarthWorksite worksite = (EarthWorksite) getEarthSky().getWorksite();
		
		latitudeValueLabel.setInstance(worksite);
		longitudeValueLabel.setInstance(worksite);
		altitudeValueLabel.setInstance(worksite);
		
		return bindingContext;
	}
}

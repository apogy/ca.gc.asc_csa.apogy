package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites.EarthSurfaceWorksiteOriginComposite;

public class EarthSurfaceWorksiteOriginWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.wizards.EarthSurfaceWorksiteOriginWizardPage";
	
	private EarthSurfaceWorksite earthSurfaceWorksite;
	private EarthSurfaceWorksiteOriginComposite earthSurfaceWorksiteOriginComposite;
	
	public EarthSurfaceWorksiteOriginWizardPage(EarthSurfaceWorksite earthSurfaceWorksite) 
	{
		super(WIZARD_PAGE_ID);
		this.earthSurfaceWorksite = earthSurfaceWorksite;
		
		setTitle("Earth Surface Worksite Origin");
		setDescription("Set the coordinates and orientation of the worksite origin.");
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(1, false));
		
		earthSurfaceWorksiteOriginComposite = new EarthSurfaceWorksiteOriginComposite(container, SWT.None);
		earthSurfaceWorksiteOriginComposite.setEarthSurfaceWorksite(earthSurfaceWorksite);
		earthSurfaceWorksiteOriginComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		
		setControl(container);
		earthSurfaceWorksiteOriginComposite.setFocus();
		
		setPageComplete(true);
	}	
}

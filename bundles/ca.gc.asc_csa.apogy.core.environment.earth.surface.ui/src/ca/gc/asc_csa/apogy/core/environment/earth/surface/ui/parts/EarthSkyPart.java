package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.parts;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractPart;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.EarthWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites.EarthSkyComposite;

public class EarthSkyPart extends AbstractPart
{
	private Adapter adapter;
	private EarthSkyComposite earthSkyComposite = null;

	public EarthSkyPart() 
	{
		// Register to changes in the active Earth Surface Worksite.
		ApogyEarthFacade.INSTANCE.eAdapters().add(getApogyEarthSurfaceEnvironmentFacadeAdapter());
	}
	
	@Override
	protected EObject getInitializeObject() 
	{			
		EarthSky earthSky = null;
		
		EarthWorksite earthSurfaceWorksite = ApogyEarthFacade.INSTANCE.getActiveEarthWorksite();
		if(earthSurfaceWorksite != null && earthSurfaceWorksite.getSky() instanceof EarthSky)
		{
			earthSky = (EarthSky) earthSurfaceWorksite.getSky();
		}
		
		return earthSky;
	}
	
	@Override
	protected void setCompositeContent(EObject eObject) 
	{	
		if(eObject instanceof EarthSky)
		{			
			earthSkyComposite.setEarthSky((EarthSky) eObject);
		}
		else
		{
			setEObject(null);
		}
	}

	@Override
	protected void createNoContentComposite(Composite parent, int style) 
	{		
		new NoContentComposite(parent, SWT.None){
			@Override
			protected String getMessage() {
				return "No active Earth Sky.";
			}		
		};	
	}	

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{						
		parent.setLayout(new GridLayout(1,false));
		
		ScrolledComposite scrolledComposite = new ScrolledComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
						
		earthSkyComposite = new EarthSkyComposite(scrolledComposite, style);				
		GridData gridData = new GridData(SWT.FILL, SWT.TOP, true, false);
		earthSkyComposite.setLayoutData(gridData);		
		
		scrolledComposite.setContent(earthSkyComposite);
		scrolledComposite.setMinSize(earthSkyComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	@Override
	public void dispose() 
	{
		// Un-Register from changes in the active Earth Surface Worksite.
		ApogyEarthFacade.INSTANCE.eAdapters().remove(getApogyEarthSurfaceEnvironmentFacadeAdapter());	
		super.dispose();
	}
	
	private Adapter getApogyEarthSurfaceEnvironmentFacadeAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{			
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof ApogyEarthFacade)
					{
						int featureID = msg.getFeatureID(ApogyEarthFacade.class);
						switch (featureID) 
						{
							case ApogyEarthEnvironmentPackage.APOGY_EARTH_FACADE__ACTIVE_EARTH_WORKSITE:
								if(msg.getNewValue() instanceof EarthWorksite)
								{
									EarthWorksite earthSurfaceWorksite = (EarthWorksite) msg.getNewValue();
									
									if(earthSurfaceWorksite != null && (earthSurfaceWorksite.getSky() instanceof EarthSky))
									{
										setEObject((EarthSky) earthSurfaceWorksite.getSky());
									}
									else
									{
										setEObject(null);
									}																		
								}
								else
								{
									setEObject(null);
								}
								
							break;

						default:
							break;
						}
					}
				}	
			};
		}
		
		return adapter;
		
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites;



import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.Duration;
import ca.gc.asc_csa.apogy.core.environment.earth.EarthWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.AstronomyUtils;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky;

public class EarthSkyDayComposite extends Composite 
{
	private EarthSky earthSky = null;
	private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private DecimalFormat durationFormat = new DecimalFormat("00");

	private Text sunRiseTime;
	private Text sunSetTime;
	private Text dayDuration;
	
	private DataBindingContext m_bindingContext;		
	
	public EarthSkyDayComposite(Composite parent, int style) 
	{
		super(parent, style);			
		setLayout(new GridLayout(2, false));
		
		Label sunRiseLabel = new Label(this, SWT.NONE);
		sunRiseLabel.setText("Sunrise Time (local) :");
		
		GridData gridData1 = new GridData();
		gridData1.grabExcessHorizontalSpace = true;
		gridData1.widthHint = 70;
		gridData1.minimumWidth = 70;
		gridData1.horizontalAlignment = SWT.LEFT;
		sunRiseTime = new Text(this, SWT.NONE | SWT.RIGHT);
		sunRiseTime.setLayoutData(gridData1);
		sunRiseTime.setText("HH:MM:ss");		
		sunRiseTime.setEditable(false);
		sunRiseTime.setToolTipText("Time, in local time, when the sun will cross the horizon at sunrise (does not include refraction).");
		
		Label sunSetLabel = new Label(this, SWT.NONE);
		sunSetLabel.setText("Sunset Time (local) :");
		
		GridData gridData2 = new GridData();
		gridData2.grabExcessHorizontalSpace = true;
		gridData2.widthHint = 70;
		gridData2.minimumWidth = 70;
		gridData2.horizontalAlignment = SWT.LEFT;
		sunSetTime = new Text(this, SWT.NONE | SWT.RIGHT);
		sunSetTime.setLayoutData(gridData2);
		sunSetTime.setText("HH:MM:ss");		
		sunSetTime.setEditable(false);
		sunSetTime.setToolTipText("Time, in local time, when the sun will cross the horizon at sunset (does not include refraction).");
		
		Label sunHoursDurationLabel = new Label(this, SWT.NONE);
		sunHoursDurationLabel.setText("Duration :");
		
		GridData gridData3 = new GridData();
		gridData3.grabExcessHorizontalSpace = true;
		gridData3.widthHint = 70;
		gridData3.minimumWidth = 70;
		gridData3.horizontalAlignment = SWT.LEFT;
		dayDuration = new Text(this, SWT.NONE | SWT.RIGHT);
		dayDuration.setLayoutData(gridData3);
		dayDuration.setText("HH:MM:ss");		
		dayDuration.setEditable(false);
		dayDuration.setToolTipText("Duration of day, from sunrise to sunset (does not include refraction) in hh:mm:ss format.");
		
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
	
	public EarthSky getEarthSky() {
		return earthSky;
	}
	
	public void setEarthSky(EarthSky earthSky) 
	{		
		setEarthSky(earthSky, true);		
	}
	
	public void setEarthSky(EarthSky newEarthSky, boolean update) 
	{
		// Updates EarthSky
		this.earthSky = newEarthSky;
		
		if (update) 
		{
			if (m_bindingContext != null) 
			{
				m_bindingContext.dispose();
				m_bindingContext = null;
			}
			
			if (newEarthSky != null) 
			{
				m_bindingContext = initDataBindings();
			}
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected DataBindingContext initDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		// Local Time
		IObservableValue localTimeObserveValue = EMFProperties.value(FeaturePath.fromList(ApogyCommonEMFPackage.Literals.TIMED__TIME)).observe(getEarthSky());

		// Sun Rise Time
		IObservableValue sunRiseTimeText = PojoProperties.value("text").observe(sunRiseTime);	
		bindingContext.bindValue(sunRiseTimeText, 
								 localTimeObserveValue, 
								 new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), 
								 new UpdateValueStrategy().setConverter(new Converter(Date.class, String.class) 
								 {									 
									@Override
									public Object convert(Object arg0) 
									{
										if(arg0 instanceof Date && getEarthSky().getWorksite() instanceof EarthWorksite)
										{
											EarthWorksite earthWorksite = (EarthWorksite) getEarthSky().getWorksite();																						
											Date date = (Date) arg0;
											Date sunRise = AstronomyUtils.INSTANCE.getSunRiseTime(date, earthWorksite.getGeographicalCoordinates().getLongitude(), earthWorksite.getGeographicalCoordinates().getLatitude());
											return timeFormat.format(sunRise);											
										}
										else
										{
											return "?";
										}
									}
								}));
		// Sun Set Time
		IObservableValue sunSetTimeText = PojoProperties.value("text").observe(sunSetTime);	
		bindingContext.bindValue(sunSetTimeText, 
								 localTimeObserveValue, 
								 new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), 
								 new UpdateValueStrategy().setConverter(new Converter(Date.class, String.class) 
								 {									 
									@Override
									public Object convert(Object arg0) 
									{
										if(arg0 instanceof Date && getEarthSky().getWorksite() instanceof EarthWorksite)
										{
											EarthWorksite earthWorksite = (EarthWorksite) getEarthSky().getWorksite();																						
											Date date = (Date) arg0;
											Date sunSet = AstronomyUtils.INSTANCE.getSunSetTime(date, earthWorksite.getGeographicalCoordinates().getLongitude(), earthWorksite.getGeographicalCoordinates().getLatitude());
											return timeFormat.format(sunSet);											
										}
										else
										{
											return "?";
										}
									}
								}));		
		
		// Duration
		IObservableValue dayDurationText = PojoProperties.value("text").observe(dayDuration);	
		bindingContext.bindValue(dayDurationText, 
				 localTimeObserveValue, 
				 new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), 
				 new UpdateValueStrategy().setConverter(new Converter(Date.class, String.class) 
				 {									 
					@Override
					public Object convert(Object arg0) 
					{
						if(arg0 instanceof Date && getEarthSky().getWorksite() instanceof EarthWorksite)
						{
							EarthWorksite earthWorksite = (EarthWorksite) getEarthSky().getWorksite();																						
							Date date = (Date) arg0;
							
							Date sunRise = AstronomyUtils.INSTANCE.getSunRiseTime(date, earthWorksite.getGeographicalCoordinates().getLongitude(), earthWorksite.getGeographicalCoordinates().getLatitude());
							Date sunSet = AstronomyUtils.INSTANCE.getSunSetTime(date, earthWorksite.getGeographicalCoordinates().getLongitude(), earthWorksite.getGeographicalCoordinates().getLatitude());

							long delta = sunSet.getTime() - sunRise.getTime();
							Duration duration = ApogyCommonEMFFactory.eINSTANCE.createDuration();
							duration.setValue(delta);							
														
							return durationFormat.format(duration.getHours()) + ":" + durationFormat.format(duration.getMinutes()) + ":" + durationFormat.format(duration.getSeconds());
						}
						else
						{
							return "?";
						}
					}
				}));	
		
		return bindingContext;
	}
}

package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.core.environment.AbstractWorksite;
import ca.gc.asc_csa.apogy.core.environment.WorksitesRegistry;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;

public class EarthSurfaceWorksitesRegistryComposite extends Composite 
{
	private Tree tree;
	private TreeViewer treeViewer;
	
	private EarthSurfaceWorksite selectedEarthSurfaceWorksite;

	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	public EarthSurfaceWorksitesRegistryComposite(Composite parent, int style) 
	{
		super(parent, style);					
		setLayout(new GridLayout(2, false));
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CompositeFilterContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				selectedEarthSurfaceWorksite = (EarthSurfaceWorksite)((IStructuredSelection) event.getSelection()).getFirstElement();
				newEarthSurfaceWorksiteSelected(selectedEarthSurfaceWorksite);					
			}
		});
		
		treeViewer.setInput(WorksitesRegistry.INSTANCE);
	}
	
	public EarthSurfaceWorksite getSelectedEarthSurfaceWorksite()
	{
		return selectedEarthSurfaceWorksite;
	}
	
	/**
	 * Method called when a EarthSurfaceWorksite is selected. 
	 * @param earthSurfaceWorksite The selected EarthSurfaceWorksite, can be null.
	 */
	protected void newEarthSurfaceWorksiteSelected(EarthSurfaceWorksite earthSurfaceWorksite)
	{		
	}
	
	private List<EarthSurfaceWorksite> filterEarthSurfaceWorksite(WorksitesRegistry worksitesRegistry)
	{
		List<EarthSurfaceWorksite> earthSurfaceWorksites = new ArrayList<EarthSurfaceWorksite>();
		for(AbstractWorksite worksite : worksitesRegistry.getWorksites())
		{
			if(worksite instanceof EarthSurfaceWorksite)
			{
				earthSurfaceWorksites.add(((EarthSurfaceWorksite) worksite));
			}
		}
		
		return earthSurfaceWorksites;
	}
	
	private class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof WorksitesRegistry)
			{								
				WorksitesRegistry worksitesRegistry = (WorksitesRegistry) inputElement;
								
				// Keeps only EarthSurfaceWorksite.			
				return filterEarthSurfaceWorksite(worksitesRegistry).toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof WorksitesRegistry)
			{								
				WorksitesRegistry worksitesRegistry = (WorksitesRegistry) parentElement;
				
				// Keeps only EarthSurfaceWorksite.			
				return filterEarthSurfaceWorksite(worksitesRegistry).toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof WorksitesRegistry)
			{
				WorksitesRegistry worksitesRegistry = (WorksitesRegistry) element;			
				return !filterEarthSurfaceWorksite(worksitesRegistry).isEmpty();
			}		
			else
			{
				return false;
			}
		}
	}
}

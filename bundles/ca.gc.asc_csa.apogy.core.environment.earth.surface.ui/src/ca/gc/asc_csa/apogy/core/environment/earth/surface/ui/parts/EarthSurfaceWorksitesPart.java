package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.parts;

import javax.inject.Inject;

import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites.EarthSurfaceWorksitesComposite;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;

public class EarthSurfaceWorksitesPart extends AbstractSessionBasedPart
{
	@Inject
	protected EPartService ePartService;
	
	private EarthSurfaceWorksitesComposite earthSurfaceWorksitesComposite;
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{
		if(invocatorSession != null)
		{
			if(invocatorSession.getEnvironment() instanceof ApogyEnvironment)
			{
				ApogyEnvironment apogyEnvironment = (ApogyEnvironment) invocatorSession.getEnvironment();
				earthSurfaceWorksitesComposite.setApogyEnvironment(apogyEnvironment);
			}
			else
			{
				earthSurfaceWorksitesComposite.setApogyEnvironment(null);
			}
		}			
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{
		parent.setLayout(new FillLayout());
		earthSurfaceWorksitesComposite = new EarthSurfaceWorksitesComposite(parent, SWT.NONE)
		{			
			@Override
			protected void newEarthSurfaceWorksiteSelected(EarthSurfaceWorksite earthSurfaceWorksite) 
			{								
				selectionService.setSelection(earthSurfaceWorksite);
			}
		};				
	}
}

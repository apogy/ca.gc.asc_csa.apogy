package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.parts;

import java.util.HashMap;

import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.ApogyCoreEnvironmentSurfaceEarthUIRCPConstants;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.SurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.MapsComposite;

public class MapsPart extends AbstractEObjectSelectionPart 
{
	@Inject
	protected EPartService ePartService;

	private Map selectedMap = null;
	private MapsComposite mapsComposite;
	
	public Map getSelectedMap() {
		return selectedMap;
	}

	@Override
	protected void setCompositeContents(EObject eObject) 
	{
		if (eObject instanceof SurfaceWorksite) 
		{
			SurfaceWorksite surfaceWorksite = (SurfaceWorksite) eObject;
			mapsComposite.setSurfaceWorksite(surfaceWorksite);
		}
	}

	@Override
	protected void createContentComposite(Composite parent, int style) {
		mapsComposite = new MapsComposite(parent, SWT.None)
		{
			@Override
			protected void newMapSelected(Map map) 
			{			
				selectedMap = map;
				selectionService.setSelection(map);
			}
		};
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() {
		HashMap<String, ISelectionListener> map = new HashMap<>();

		map.put(ApogyCoreEnvironmentSurfaceEarthUIRCPConstants.PART__EARTH_SURFACE_WORKSITES__ID, new ISelectionListener() {
			@Override
			public void selectionChanged(MPart part, Object selection) 
			{
				if (selection instanceof SurfaceWorksite) 
				{
					SurfaceWorksite surfaceWorksite = (SurfaceWorksite) selection;
					setEObject(surfaceWorksite);
				}
			}
		});

		return map;
	}
}

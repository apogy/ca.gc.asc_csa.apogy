package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.wizards;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.WorksitesList;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.Activator;

public class ImportEarthSurfaceWorksiteWizard extends Wizard
{
	private WorksitesList worksitesList;
	private EarthSurfaceWorksite importedEarthSurfaceWorksite = null;
	
	private ImportEarthSurfaceWorksiteWizardPage importEarthSurfaceWorksiteWizardPage;
	
	public ImportEarthSurfaceWorksiteWizard(WorksitesList worksitesList)
	{
		super();
		this.worksitesList = worksitesList;
		
		setWindowTitle("Import Earth Surface Worksite");
		
		importEarthSurfaceWorksiteWizardPage = new ImportEarthSurfaceWorksiteWizardPage(worksitesList)
		{
			@Override
			protected void newSelectedEarthSurfaceWorksite(EarthSurfaceWorksite selectedEarthSurfaceWorksite) 
			{
				importedEarthSurfaceWorksite = selectedEarthSurfaceWorksite;
				setPageComplete(importedEarthSurfaceWorksite != null);
			}
		};
		addPage(importEarthSurfaceWorksiteWizardPage);
	}
	
	public void setImportedEarthSurfaceWorksite(EarthSurfaceWorksite importedEarthSurfaceWorksite)
	{
		this.importedEarthSurfaceWorksite = importedEarthSurfaceWorksite;
	}
	
	@Override
	public boolean canFinish() 
	{
		return (importedEarthSurfaceWorksite != null);
	}
	
	@Override
	public boolean performFinish() 
	{		
		if(importedEarthSurfaceWorksite != null)
		{
			try 
			{
				ApogyCommonTransactionFacade.INSTANCE.basicAdd(worksitesList, ApogyCoreEnvironmentPackage.Literals.WORKSITES_LIST__WORKSITES, importedEarthSurfaceWorksite);
				return true;
			} 
			catch (Exception e) 
			{
				String errorMessage = "Failed to import Earth Surface Worksite !!";
				Logger.INSTANCE.log(Activator.ID, this, errorMessage, EventSeverity.ERROR, e);					
				
				MessageDialog.openError(getShell(), "Error", errorMessage);							
												
				return false;
			}
		}
		else
		{
			return false;
		}
	}
		
}

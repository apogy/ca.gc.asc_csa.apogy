package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky;

public class EarthSkyComposite extends Composite 
{
			
	// Earth Sky
	private EarthSky earthSky= null;
	
	private EarthSkyLocationComposite earthSkyLocationComposite = null;
	private EarthSkySunComposite earthSkySunComposite = null;
	private EarthSkyMoonComposite earthSkyMoonComposite = null;
	private EarthSkyTimeComposite earthSkyTimeComposite = null;
	private EarthSkyDayComposite earthSkyDayComposite = null;
	
	public EarthSkyComposite(Composite parent, int style) 
	{
		super(parent, style);	
		
		setLayout(new GridLayout(2, false));
				
		Group locationGroup = new Group(this, SWT.NONE);
		GridData locationGroupGridData = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1); 
		locationGroupGridData.minimumHeight = 200;
		locationGroupGridData.minimumWidth = 600;
		locationGroup.setLayout(new FillLayout(SWT.HORIZONTAL));
		locationGroup.setLayoutData(locationGroupGridData);
		locationGroup.setText("Location");
		earthSkyLocationComposite = new EarthSkyLocationComposite(locationGroup, SWT.NONE);
		
		Group timeGroup = new Group(this, SWT.NONE);
		GridData timeGroupGridData = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		timeGroupGridData.minimumHeight = 200;
		timeGroupGridData.minimumWidth = 400;
		timeGroup.setLayoutData(timeGroupGridData);
		timeGroup.setText("Time");		
		timeGroup.setLayout(new FillLayout(SWT.HORIZONTAL));		
		earthSkyTimeComposite = new EarthSkyTimeComposite(timeGroup, SWT.NONE);
		
		Group sunGroup = new Group(this, SWT.NONE);
		GridData sunGroupGridData = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1); 
		sunGroupGridData.minimumHeight = 200;
		sunGroupGridData.minimumWidth = 200;
		sunGroup.setLayoutData(sunGroupGridData);
		sunGroup.setText("Sun");		
		sunGroup.setLayout(new FillLayout(SWT.HORIZONTAL));								
		earthSkySunComposite = new EarthSkySunComposite(sunGroup, SWT.NONE);
								
		Group moonGroup = new Group(this, SWT.NONE);
		GridData moonGroupGridData = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		moonGroupGridData.minimumHeight = 200;
		moonGroupGridData.minimumWidth = 200;
		moonGroup.setLayoutData(moonGroupGridData);
		moonGroup.setText("Moon");		
		moonGroup.setLayout(new FillLayout(SWT.HORIZONTAL));		
		earthSkyMoonComposite = new EarthSkyMoonComposite(moonGroup, SWT.NONE);
		
		Group dayGroup = new Group(this, SWT.NONE);
		GridData dayGroupGridData = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		dayGroupGridData.minimumHeight = 200;
		dayGroupGridData.minimumWidth = 200;
		dayGroup.setLayoutData(dayGroupGridData);
		dayGroup.setText("Day");		
		dayGroup.setLayout(new FillLayout(SWT.HORIZONTAL));		
		earthSkyDayComposite = new EarthSkyDayComposite(dayGroup, SWT.NONE);

		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent arg0) 
			{				
				earthSky = null;
			}
		});
	}
		
	public EarthSky getEarthSky() {
		return earthSky;
	}

	public void setEarthSky(EarthSky earthSky) 
	{		
		setEarthSky(earthSky, true);		
	}
	
	public void setEarthSky(EarthSky newEarthSky, boolean update) 
	{
		earthSkyLocationComposite.setEarthSky(newEarthSky);
		earthSkySunComposite.setEarthSky(newEarthSky);
		earthSkyTimeComposite.setEarthSky(newEarthSky);
		earthSkyMoonComposite.setEarthSky(newEarthSky);	
		earthSkyDayComposite.setEarthSky(newEarthSky);
		
		// Updates EarthSky
		this.earthSky = newEarthSky;			
	}
}
package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.text.DecimalFormat;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky;

public class EarthSkyMoonComposite extends Composite 
{
	public static String NO_VALUE_AVAILABLE_STRING = "N/A";

	public static final String AZIMUTH_FORMAT_STRING = "0.000";
	public static final String ELEVATION_FORMAT_STRING = "0.000";

	private static int LABEL_WIDTH = 100; 
	private static int VALUE_WIDTH = 75; 
	private static int BUTTON_WIDTH = 30; 		

	
	// Earth Sky
	private EarthSky earthSky= null;

	// Moon Displays.
	private TypedElementSimpleUnitsComposite moonAzimuthValueLabel = null;
	private TypedElementSimpleUnitsComposite moonElevationValueLabel = null;
	
	private DataBindingContext m_bindingContext;		
	
	public EarthSkyMoonComposite(Composite parent, int style) 
	{
		super(parent, style);
		
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginBottom = 0;
		layout.marginTop = 0;
		layout.marginLeft = 0;
		layout.marginRight = 0;
		setLayout(layout);
		
		// Moon Azimuth
		GridData gridDataAzimuth = new GridData();
		gridDataAzimuth.grabExcessHorizontalSpace = false;		
		gridDataAzimuth.horizontalAlignment = SWT.LEFT;
		moonAzimuthValueLabel = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat(AZIMUTH_FORMAT_STRING);			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Azimuth:";
			}
			
			@Override
			protected boolean isFeatureEditable() 
			{
				return false;
			}
		};
		moonAzimuthValueLabel.setTypedElement(FeaturePath.fromList(ApogyEarthSurfaceEnvironmentPackage.Literals.EARTH_SKY__MOON_HORIZONTAL_COORDINATES, ApogyEarthEnvironmentPackage.Literals.HORIZONTAL_COORDINATES__AZIMUTH), getEarthSky());
		moonAzimuthValueLabel.setLayoutData(gridDataAzimuth);
		
		
		// Moon Elevation
		GridData gridDataElevation = new GridData();
		gridDataElevation.grabExcessHorizontalSpace = false;		
		gridDataElevation.horizontalAlignment = SWT.LEFT;
		moonElevationValueLabel = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat(ELEVATION_FORMAT_STRING);			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Elevation:";
			}
			
			@Override
			protected boolean isFeatureEditable() 
			{
				return false;
			}
		};
		moonElevationValueLabel.setLayoutData(gridDataElevation);
		moonElevationValueLabel.setTypedElement(FeaturePath.fromList(ApogyEarthSurfaceEnvironmentPackage.Literals.EARTH_SKY__MOON_HORIZONTAL_COORDINATES, ApogyEarthEnvironmentPackage.Literals.HORIZONTAL_COORDINATES__ALTITUDE), getEarthSky());
		
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
	
	public EarthSky getEarthSky() {
		return earthSky;
	}
	
	public void setEarthSky(EarthSky earthSky) 
	{		
		setEarthSky(earthSky, true);		
	}
	
	public void setEarthSky(EarthSky newEarthSky, boolean update) 
	{
		// Updates EarthSky
		this.earthSky = newEarthSky;
		
		if (update) 
		{
			if (m_bindingContext != null) {
				m_bindingContext.dispose();
				m_bindingContext = null;
			}
			if (newEarthSky != null) {
				m_bindingContext = initDataBindings();
			}
		}
	}
	
	protected DataBindingContext initDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		moonAzimuthValueLabel.setInstance(getEarthSky());
		moonElevationValueLabel.setInstance(getEarthSky());
		
		return bindingContext;
	}
}

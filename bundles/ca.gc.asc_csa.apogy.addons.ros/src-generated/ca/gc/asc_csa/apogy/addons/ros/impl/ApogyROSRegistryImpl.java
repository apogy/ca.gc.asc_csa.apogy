/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ros.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import ca.gc.asc_csa.apogy.addons.ros.ApogyAddonsROSPackage;
import ca.gc.asc_csa.apogy.addons.ros.ApogyROSRegistry;
import ca.gc.asc_csa.apogy.addons.ros.ROSInterface;
import java.util.Collection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Apogy ROS Registry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.impl.ApogyROSRegistryImpl#getRosInterfaceList <em>Ros Interface List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ApogyROSRegistryImpl extends MinimalEObjectImpl.Container implements ApogyROSRegistry 
{
	private static ApogyROSRegistry instance;
	
	/**
	 * Returns the ApogyROSRegistry singleton. 
	 * @return
	 */
	public static ApogyROSRegistry getInstance()
	{
		if( instance == null )
		{
			instance = new ApogyROSRegistryImpl(); 
		}
		return instance; 
	}
	
	/**
	 * The cached value of the '{@link #getRosInterfaceList() <em>Ros Interface List</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRosInterfaceList()
	 * @generated
	 * @ordered
	 */
	protected EList<ROSInterface> rosInterfaceList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApogyROSRegistryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsROSPackage.Literals.APOGY_ROS_REGISTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ROSInterface> getRosInterfaceList() {
		if (rosInterfaceList == null) {
			rosInterfaceList = new EObjectResolvingEList<ROSInterface>(ROSInterface.class, this, ApogyAddonsROSPackage.APOGY_ROS_REGISTRY__ROS_INTERFACE_LIST);
		}
		return rosInterfaceList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyAddonsROSPackage.APOGY_ROS_REGISTRY__ROS_INTERFACE_LIST:
				return getRosInterfaceList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyAddonsROSPackage.APOGY_ROS_REGISTRY__ROS_INTERFACE_LIST:
				getRosInterfaceList().clear();
				getRosInterfaceList().addAll((Collection<? extends ROSInterface>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyAddonsROSPackage.APOGY_ROS_REGISTRY__ROS_INTERFACE_LIST:
				getRosInterfaceList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyAddonsROSPackage.APOGY_ROS_REGISTRY__ROS_INTERFACE_LIST:
				return rosInterfaceList != null && !rosInterfaceList.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ApogyROSRegistryImpl

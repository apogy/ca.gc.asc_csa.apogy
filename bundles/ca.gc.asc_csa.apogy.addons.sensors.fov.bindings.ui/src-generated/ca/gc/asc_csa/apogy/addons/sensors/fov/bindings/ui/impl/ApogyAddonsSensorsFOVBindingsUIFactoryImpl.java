/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl;

import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyAddonsSensorsFOVBindingsUIFactoryImpl extends EFactoryImpl implements ApogyAddonsSensorsFOVBindingsUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyAddonsSensorsFOVBindingsUIFactory init() {
		try {
			ApogyAddonsSensorsFOVBindingsUIFactory theApogyAddonsSensorsFOVBindingsUIFactory = (ApogyAddonsSensorsFOVBindingsUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyAddonsSensorsFOVBindingsUIPackage.eNS_URI);
			if (theApogyAddonsSensorsFOVBindingsUIFactory != null) {
				return theApogyAddonsSensorsFOVBindingsUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyAddonsSensorsFOVBindingsUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsSensorsFOVBindingsUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyAddonsSensorsFOVBindingsUIPackage.CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER: return createCircularSectorFieldOfViewBindingWizardPagesProvider();
			case ApogyAddonsSensorsFOVBindingsUIPackage.CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER: return createConicalFieldOfViewBindingWizardPagesProvider();
			case ApogyAddonsSensorsFOVBindingsUIPackage.RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER: return createRectangularFrustrumFieldOfViewBindingWizardPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CircularSectorFieldOfViewBindingWizardPagesProvider createCircularSectorFieldOfViewBindingWizardPagesProvider() {
		CircularSectorFieldOfViewBindingWizardPagesProviderImpl circularSectorFieldOfViewBindingWizardPagesProvider = new CircularSectorFieldOfViewBindingWizardPagesProviderImpl();
		return circularSectorFieldOfViewBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConicalFieldOfViewBindingWizardPagesProvider createConicalFieldOfViewBindingWizardPagesProvider() {
		ConicalFieldOfViewBindingWizardPagesProviderImpl conicalFieldOfViewBindingWizardPagesProvider = new ConicalFieldOfViewBindingWizardPagesProviderImpl();
		return conicalFieldOfViewBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RectangularFrustrumFieldOfViewBindingWizardPagesProvider createRectangularFrustrumFieldOfViewBindingWizardPagesProvider() {
		RectangularFrustrumFieldOfViewBindingWizardPagesProviderImpl rectangularFrustrumFieldOfViewBindingWizardPagesProvider = new RectangularFrustrumFieldOfViewBindingWizardPagesProviderImpl();
		return rectangularFrustrumFieldOfViewBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsSensorsFOVBindingsUIPackage getApogyAddonsSensorsFOVBindingsUIPackage() {
		return (ApogyAddonsSensorsFOVBindingsUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyAddonsSensorsFOVBindingsUIPackage getPackage() {
		return ApogyAddonsSensorsFOVBindingsUIPackage.eINSTANCE;
	}

} //ApogyAddonsSensorsFOVBindingsUIFactoryImpl

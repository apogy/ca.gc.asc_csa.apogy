/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl;

import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ApogyAddonsSensorsFOVBindingsUIFactory;
import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ApogyAddonsSensorsFOVBindingsUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.CircularSectorFieldOfViewBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ConicalFieldOfViewBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.RectangularFrustrumFieldOfViewBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyAddonsSensorsFOVBindingsUIPackageImpl extends EPackageImpl implements ApogyAddonsSensorsFOVBindingsUIPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass circularSectorFieldOfViewBindingWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conicalFieldOfViewBindingWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rectangularFrustrumFieldOfViewBindingWizardPagesProviderEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ApogyAddonsSensorsFOVBindingsUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyAddonsSensorsFOVBindingsUIPackageImpl() {
		super(eNS_URI, ApogyAddonsSensorsFOVBindingsUIFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyAddonsSensorsFOVBindingsUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyAddonsSensorsFOVBindingsUIPackage init() {
		if (isInited) return (ApogyAddonsSensorsFOVBindingsUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsSensorsFOVBindingsUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyAddonsSensorsFOVBindingsUIPackageImpl theApogyAddonsSensorsFOVBindingsUIPackage = (ApogyAddonsSensorsFOVBindingsUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyAddonsSensorsFOVBindingsUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyAddonsSensorsFOVBindingsUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCommonTopologyBindingsUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyAddonsSensorsFOVBindingsUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyAddonsSensorsFOVBindingsUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyAddonsSensorsFOVBindingsUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyAddonsSensorsFOVBindingsUIPackage.eNS_URI, theApogyAddonsSensorsFOVBindingsUIPackage);
		return theApogyAddonsSensorsFOVBindingsUIPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCircularSectorFieldOfViewBindingWizardPagesProvider() {
		return circularSectorFieldOfViewBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConicalFieldOfViewBindingWizardPagesProvider() {
		return conicalFieldOfViewBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRectangularFrustrumFieldOfViewBindingWizardPagesProvider() {
		return rectangularFrustrumFieldOfViewBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsSensorsFOVBindingsUIFactory getApogyAddonsSensorsFOVBindingsUIFactory() {
		return (ApogyAddonsSensorsFOVBindingsUIFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		circularSectorFieldOfViewBindingWizardPagesProviderEClass = createEClass(CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER);

		conicalFieldOfViewBindingWizardPagesProviderEClass = createEClass(CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER);

		rectangularFrustrumFieldOfViewBindingWizardPagesProviderEClass = createEClass(RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCommonTopologyBindingsUIPackage theApogyCommonTopologyBindingsUIPackage = (ApogyCommonTopologyBindingsUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyBindingsUIPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		circularSectorFieldOfViewBindingWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyBindingsUIPackage.getAbstractTopologyBindingWizardPagesProvider());
		conicalFieldOfViewBindingWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyBindingsUIPackage.getAbstractTopologyBindingWizardPagesProvider());
		rectangularFrustrumFieldOfViewBindingWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyBindingsUIPackage.getAbstractTopologyBindingWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(circularSectorFieldOfViewBindingWizardPagesProviderEClass, CircularSectorFieldOfViewBindingWizardPagesProvider.class, "CircularSectorFieldOfViewBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(conicalFieldOfViewBindingWizardPagesProviderEClass, ConicalFieldOfViewBindingWizardPagesProvider.class, "ConicalFieldOfViewBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(rectangularFrustrumFieldOfViewBindingWizardPagesProviderEClass, RectangularFrustrumFieldOfViewBindingWizardPagesProvider.class, "RectangularFrustrumFieldOfViewBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "prefix", "ApogyAddonsSensorsFOVBindingsUI",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque, \n     Sebastien Gemme \n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyAddonsSensorsFOVBindingsUI",
			 "publicConstructors", "true",
			 "suppressGenModelAnnotations", "false",
			 "complianceLevel", "6.0",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.addons.sensors.fov.bindings"
		   });	
		addAnnotation
		  (circularSectorFieldOfViewBindingWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard page provider used to create a CircularSectorFieldOfViewBinding."
		   });	
		addAnnotation
		  (conicalFieldOfViewBindingWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard page provider used to create a ConicalFieldOfViewBinding."
		   });	
		addAnnotation
		  (rectangularFrustrumFieldOfViewBindingWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard page provider used to create a RectangularFrustrumFieldOfViewBinding."
		   });
	}

} //ApogyAddonsSensorsFOVBindingsUIPackageImpl

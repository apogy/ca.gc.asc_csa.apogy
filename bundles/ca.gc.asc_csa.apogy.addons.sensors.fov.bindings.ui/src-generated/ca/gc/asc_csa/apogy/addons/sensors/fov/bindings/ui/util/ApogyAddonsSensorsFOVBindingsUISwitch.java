/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.util;

import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.*;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ApogyAddonsSensorsFOVBindingsUIPackage
 * @generated
 */
public class ApogyAddonsSensorsFOVBindingsUISwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ApogyAddonsSensorsFOVBindingsUIPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsSensorsFOVBindingsUISwitch() {
		if (modelPackage == null) {
			modelPackage = ApogyAddonsSensorsFOVBindingsUIPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ApogyAddonsSensorsFOVBindingsUIPackage.CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER: {
				CircularSectorFieldOfViewBindingWizardPagesProvider circularSectorFieldOfViewBindingWizardPagesProvider = (CircularSectorFieldOfViewBindingWizardPagesProvider)theEObject;
				T result = caseCircularSectorFieldOfViewBindingWizardPagesProvider(circularSectorFieldOfViewBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(circularSectorFieldOfViewBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(circularSectorFieldOfViewBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsFOVBindingsUIPackage.CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER: {
				ConicalFieldOfViewBindingWizardPagesProvider conicalFieldOfViewBindingWizardPagesProvider = (ConicalFieldOfViewBindingWizardPagesProvider)theEObject;
				T result = caseConicalFieldOfViewBindingWizardPagesProvider(conicalFieldOfViewBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(conicalFieldOfViewBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(conicalFieldOfViewBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsSensorsFOVBindingsUIPackage.RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER: {
				RectangularFrustrumFieldOfViewBindingWizardPagesProvider rectangularFrustrumFieldOfViewBindingWizardPagesProvider = (RectangularFrustrumFieldOfViewBindingWizardPagesProvider)theEObject;
				T result = caseRectangularFrustrumFieldOfViewBindingWizardPagesProvider(rectangularFrustrumFieldOfViewBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(rectangularFrustrumFieldOfViewBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(rectangularFrustrumFieldOfViewBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Circular Sector Field Of View Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Circular Sector Field Of View Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCircularSectorFieldOfViewBindingWizardPagesProvider(CircularSectorFieldOfViewBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conical Field Of View Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conical Field Of View Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConicalFieldOfViewBindingWizardPagesProvider(ConicalFieldOfViewBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rectangular Frustrum Field Of View Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rectangular Frustrum Field Of View Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRectangularFrustrumFieldOfViewBindingWizardPagesProvider(RectangularFrustrumFieldOfViewBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWizardPagesProvider(WizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Topology Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Topology Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractTopologyBindingWizardPagesProvider(AbstractTopologyBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ApogyAddonsSensorsFOVBindingsUISwitch

package ca.gc.asc_csa.apogy.addons.vehicle.ui.util;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.TrajectoryPickingToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.*;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIPackage;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.PathPlannerToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.PhysicalWheelPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.VehiclePathPlannerToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import ca.gc.asc_csa.apogy.common.topology.ui.NodeWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIPackage
 * @generated
 */
public class ApogyAddonsVehicleUISwitch<T> extends Switch<T>
{
  /**
	 * The cached model package
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected static ApogyAddonsVehicleUIPackage modelPackage;

  /**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public ApogyAddonsVehicleUISwitch()
  {
		if (modelPackage == null) {
			modelPackage = ApogyAddonsVehicleUIPackage.eINSTANCE;
		}
	}

  /**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
		return ePackage == modelPackage;
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
		switch (classifierID) {
			case ApogyAddonsVehicleUIPackage.PHYSICAL_WHEEL_PRESENTATION: {
				PhysicalWheelPresentation physicalWheelPresentation = (PhysicalWheelPresentation)theEObject;
				T result = casePhysicalWheelPresentation(physicalWheelPresentation);
				if (result == null) result = caseNodePresentation(physicalWheelPresentation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsVehicleUIPackage.LANDER_SPHERICAL_FOOT_PRESENTATION: {
				LanderSphericalFootPresentation landerSphericalFootPresentation = (LanderSphericalFootPresentation)theEObject;
				T result = caseLanderSphericalFootPresentation(landerSphericalFootPresentation);
				if (result == null) result = caseNodePresentation(landerSphericalFootPresentation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsVehicleUIPackage.THRUSTER_PRESENTATION: {
				ThrusterPresentation thrusterPresentation = (ThrusterPresentation)theEObject;
				T result = caseThrusterPresentation(thrusterPresentation);
				if (result == null) result = caseNodePresentation(thrusterPresentation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsVehicleUIPackage.WHEEL_WIZARD_PAGES_PROVIDER: {
				WheelWizardPagesProvider wheelWizardPagesProvider = (WheelWizardPagesProvider)theEObject;
				T result = caseWheelWizardPagesProvider(wheelWizardPagesProvider);
				if (result == null) result = caseNodeWizardPagesProvider(wheelWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(wheelWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsVehicleUIPackage.LANDER_SPHERICAL_FOOT_PAGES_PROVIDER: {
				LanderSphericalFootPagesProvider landerSphericalFootPagesProvider = (LanderSphericalFootPagesProvider)theEObject;
				T result = caseLanderSphericalFootPagesProvider(landerSphericalFootPagesProvider);
				if (result == null) result = caseNodeWizardPagesProvider(landerSphericalFootPagesProvider);
				if (result == null) result = caseWizardPagesProvider(landerSphericalFootPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsVehicleUIPackage.THRUSTER_WIZARD_PAGES_PROVIDER: {
				ThrusterWizardPagesProvider thrusterWizardPagesProvider = (ThrusterWizardPagesProvider)theEObject;
				T result = caseThrusterWizardPagesProvider(thrusterWizardPagesProvider);
				if (result == null) result = caseNodeWizardPagesProvider(thrusterWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(thrusterWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsVehicleUIPackage.PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER: {
				PathPlannerToolWizardPagesProvider pathPlannerToolWizardPagesProvider = (PathPlannerToolWizardPagesProvider)theEObject;
				T result = casePathPlannerToolWizardPagesProvider(pathPlannerToolWizardPagesProvider);
				if (result == null) result = caseSimple3DToolWizardPagesProvider(pathPlannerToolWizardPagesProvider);
				if (result == null) result = caseSimpleToolWizardPagesProvider(pathPlannerToolWizardPagesProvider);
				if (result == null) result = caseAbstractToolWizardPagesProvider(pathPlannerToolWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(pathPlannerToolWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(pathPlannerToolWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsVehicleUIPackage.VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER: {
				VehiclePathPlannerToolWizardPagesProvider vehiclePathPlannerToolWizardPagesProvider = (VehiclePathPlannerToolWizardPagesProvider)theEObject;
				T result = caseVehiclePathPlannerToolWizardPagesProvider(vehiclePathPlannerToolWizardPagesProvider);
				if (result == null) result = casePathPlannerToolWizardPagesProvider(vehiclePathPlannerToolWizardPagesProvider);
				if (result == null) result = caseSimple3DToolWizardPagesProvider(vehiclePathPlannerToolWizardPagesProvider);
				if (result == null) result = caseSimpleToolWizardPagesProvider(vehiclePathPlannerToolWizardPagesProvider);
				if (result == null) result = caseAbstractToolWizardPagesProvider(vehiclePathPlannerToolWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(vehiclePathPlannerToolWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(vehiclePathPlannerToolWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsVehicleUIPackage.VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER: {
				VehicleTrajectoryPickingToolWizardPagesProvider vehicleTrajectoryPickingToolWizardPagesProvider = (VehicleTrajectoryPickingToolWizardPagesProvider)theEObject;
				T result = caseVehicleTrajectoryPickingToolWizardPagesProvider(vehicleTrajectoryPickingToolWizardPagesProvider);
				if (result == null) result = caseTrajectoryPickingToolWizardPagesProvider(vehicleTrajectoryPickingToolWizardPagesProvider);
				if (result == null) result = caseSimple3DToolWizardPagesProvider(vehicleTrajectoryPickingToolWizardPagesProvider);
				if (result == null) result = caseSimpleToolWizardPagesProvider(vehicleTrajectoryPickingToolWizardPagesProvider);
				if (result == null) result = caseAbstractToolWizardPagesProvider(vehicleTrajectoryPickingToolWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(vehicleTrajectoryPickingToolWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(vehicleTrajectoryPickingToolWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsVehicleUIPackage.THRUSTER_BINDING_WIZARD_PAGES_PROVIDER: {
				ThrusterBindingWizardPagesProvider thrusterBindingWizardPagesProvider = (ThrusterBindingWizardPagesProvider)theEObject;
				T result = caseThrusterBindingWizardPagesProvider(thrusterBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(thrusterBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(thrusterBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Wheel Presentation</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Wheel Presentation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T casePhysicalWheelPresentation(PhysicalWheelPresentation object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Lander Spherical Foot Presentation</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lander Spherical Foot Presentation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseLanderSphericalFootPresentation(LanderSphericalFootPresentation object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Thruster Presentation</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Thruster Presentation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseThrusterPresentation(ThrusterPresentation object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Wheel Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->

	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wheel Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWheelWizardPagesProvider(WheelWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Lander Spherical Foot Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->

	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lander Spherical Foot Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLanderSphericalFootPagesProvider(LanderSphericalFootPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Thruster Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->

	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Thruster Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseThrusterWizardPagesProvider(ThrusterWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Path Planner Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path Planner Tool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathPlannerToolWizardPagesProvider(PathPlannerToolWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Vehicle Path Planner Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vehicle Path Planner Tool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVehiclePathPlannerToolWizardPagesProvider(VehiclePathPlannerToolWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Vehicle Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vehicle Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVehicleTrajectoryPickingToolWizardPagesProvider(VehicleTrajectoryPickingToolWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Thruster Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Thruster Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseThrusterBindingWizardPagesProvider(ThrusterBindingWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Presentation</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Presentation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseNodePresentation(NodePresentation object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWizardPagesProvider(WizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->

	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNodeWizardPagesProvider(NodeWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedDescribedWizardPagesProvider(NamedDescribedWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Tool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractToolWizardPagesProvider(AbstractToolWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Tool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleToolWizardPagesProvider(SimpleToolWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple3 DTool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple3 DTool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimple3DToolWizardPagesProvider(Simple3DToolWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTrajectoryPickingToolWizardPagesProvider(TrajectoryPickingToolWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Topology Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Topology Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractTopologyBindingWizardPagesProvider(AbstractTopologyBindingWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
  @Override
  public T defaultCase(EObject object)
  {
		return null;
	}

} //ApogyAddonsVehicleUISwitch

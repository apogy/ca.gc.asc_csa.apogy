package ca.gc.asc_csa.apogy.addons.vehicle.ui;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIPackage
 * @generated
 */
public interface ApogyAddonsVehicleUIFactory extends EFactory
{
  /**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  ApogyAddonsVehicleUIFactory eINSTANCE = ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIFactoryImpl.init();

  /**
	 * Returns a new object of class '<em>Physical Wheel Presentation</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Wheel Presentation</em>'.
	 * @generated
	 */
  PhysicalWheelPresentation createPhysicalWheelPresentation();

  /**
	 * Returns a new object of class '<em>Lander Spherical Foot Presentation</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lander Spherical Foot Presentation</em>'.
	 * @generated
	 */
  LanderSphericalFootPresentation createLanderSphericalFootPresentation();

  /**
	 * Returns a new object of class '<em>Thruster Presentation</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Thruster Presentation</em>'.
	 * @generated
	 */
  ThrusterPresentation createThrusterPresentation();

  /**
	 * Returns a new object of class '<em>Wheel Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return a new object of class '<em>Wheel Wizard Pages Provider</em>'.
	 * @generated
	 */
	WheelWizardPagesProvider createWheelWizardPagesProvider();

		/**
	 * Returns a new object of class '<em>Lander Spherical Foot Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return a new object of class '<em>Lander Spherical Foot Pages Provider</em>'.
	 * @generated
	 */
	LanderSphericalFootPagesProvider createLanderSphericalFootPagesProvider();

		/**
	 * Returns a new object of class '<em>Thruster Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return a new object of class '<em>Thruster Wizard Pages Provider</em>'.
	 * @generated
	 */
	ThrusterWizardPagesProvider createThrusterWizardPagesProvider();

		/**
	 * Returns a new object of class '<em>Path Planner Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Path Planner Tool Wizard Pages Provider</em>'.
	 * @generated
	 */
	PathPlannerToolWizardPagesProvider createPathPlannerToolWizardPagesProvider();

		/**
	 * Returns a new object of class '<em>Vehicle Path Planner Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vehicle Path Planner Tool Wizard Pages Provider</em>'.
	 * @generated
	 */
	VehiclePathPlannerToolWizardPagesProvider createVehiclePathPlannerToolWizardPagesProvider();

		/**
	 * Returns a new object of class '<em>Vehicle Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vehicle Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * @generated
	 */
	VehicleTrajectoryPickingToolWizardPagesProvider createVehicleTrajectoryPickingToolWizardPagesProvider();

		/**
	 * Returns a new object of class '<em>Thruster Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Thruster Binding Wizard Pages Provider</em>'.
	 * @generated
	 */
	ThrusterBindingWizardPagesProvider createThrusterBindingWizardPagesProvider();

		/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyAddonsVehicleUIPackage getApogyAddonsVehicleUIPackage();

} //ApogyAddonsVehicleUIFactory

package ca.gc.asc_csa.apogy.addons.vehicle.ui.util;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.TrajectoryPickingToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.*;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIPackage;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.PathPlannerToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.PhysicalWheelPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.VehiclePathPlannerToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import ca.gc.asc_csa.apogy.common.topology.ui.NodeWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIPackage
 * @generated
 */
public class ApogyAddonsVehicleUIAdapterFactory extends AdapterFactoryImpl
{
  /**
	 * The cached model package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected static ApogyAddonsVehicleUIPackage modelPackage;

  /**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public ApogyAddonsVehicleUIAdapterFactory()
  {
		if (modelPackage == null) {
			modelPackage = ApogyAddonsVehicleUIPackage.eINSTANCE;
		}
	}

  /**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
  @Override
  public boolean isFactoryForType(Object object)
  {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

  /**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected ApogyAddonsVehicleUISwitch<Adapter> modelSwitch =
    new ApogyAddonsVehicleUISwitch<Adapter>() {
			@Override
			public Adapter casePhysicalWheelPresentation(PhysicalWheelPresentation object) {
				return createPhysicalWheelPresentationAdapter();
			}
			@Override
			public Adapter caseLanderSphericalFootPresentation(LanderSphericalFootPresentation object) {
				return createLanderSphericalFootPresentationAdapter();
			}
			@Override
			public Adapter caseThrusterPresentation(ThrusterPresentation object) {
				return createThrusterPresentationAdapter();
			}
			@Override
			public Adapter caseWheelWizardPagesProvider(WheelWizardPagesProvider object) {
				return createWheelWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseLanderSphericalFootPagesProvider(LanderSphericalFootPagesProvider object) {
				return createLanderSphericalFootPagesProviderAdapter();
			}
			@Override
			public Adapter caseThrusterWizardPagesProvider(ThrusterWizardPagesProvider object) {
				return createThrusterWizardPagesProviderAdapter();
			}
			@Override
			public Adapter casePathPlannerToolWizardPagesProvider(PathPlannerToolWizardPagesProvider object) {
				return createPathPlannerToolWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseVehiclePathPlannerToolWizardPagesProvider(VehiclePathPlannerToolWizardPagesProvider object) {
				return createVehiclePathPlannerToolWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseVehicleTrajectoryPickingToolWizardPagesProvider(VehicleTrajectoryPickingToolWizardPagesProvider object) {
				return createVehicleTrajectoryPickingToolWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseThrusterBindingWizardPagesProvider(ThrusterBindingWizardPagesProvider object) {
				return createThrusterBindingWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseNodePresentation(NodePresentation object) {
				return createNodePresentationAdapter();
			}
			@Override
			public Adapter caseWizardPagesProvider(WizardPagesProvider object) {
				return createWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseNodeWizardPagesProvider(NodeWizardPagesProvider object) {
				return createNodeWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseNamedDescribedWizardPagesProvider(NamedDescribedWizardPagesProvider object) {
				return createNamedDescribedWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseAbstractToolWizardPagesProvider(AbstractToolWizardPagesProvider object) {
				return createAbstractToolWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseSimpleToolWizardPagesProvider(SimpleToolWizardPagesProvider object) {
				return createSimpleToolWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseSimple3DToolWizardPagesProvider(Simple3DToolWizardPagesProvider object) {
				return createSimple3DToolWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseTrajectoryPickingToolWizardPagesProvider(TrajectoryPickingToolWizardPagesProvider object) {
				return createTrajectoryPickingToolWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseAbstractTopologyBindingWizardPagesProvider(AbstractTopologyBindingWizardPagesProvider object) {
				return createAbstractTopologyBindingWizardPagesProviderAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

  /**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
  @Override
  public Adapter createAdapter(Notifier target)
  {
		return modelSwitch.doSwitch((EObject)target);
	}


  /**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.PhysicalWheelPresentation <em>Physical Wheel Presentation</em>}'.
	 * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.PhysicalWheelPresentation
	 * @generated
	 */
  public Adapter createPhysicalWheelPresentationAdapter()
  {
		return null;
	}

  /**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPresentation <em>Lander Spherical Foot Presentation</em>}'.
	 * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPresentation
	 * @generated
	 */
  public Adapter createLanderSphericalFootPresentationAdapter()
  {
		return null;
	}

  /**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation <em>Thruster Presentation</em>}'.
	 * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation
	 * @generated
	 */
  public Adapter createThrusterPresentationAdapter()
  {
		return null;
	}

  /**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.WheelWizardPagesProvider <em>Wheel Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->

	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.WheelWizardPagesProvider
	 * @generated
	 */
	public Adapter createWheelWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPagesProvider <em>Lander Spherical Foot Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->

	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPagesProvider
	 * @generated
	 */
	public Adapter createLanderSphericalFootPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterWizardPagesProvider <em>Thruster Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->

	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterWizardPagesProvider
	 * @generated
	 */
	public Adapter createThrusterWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.PathPlannerToolWizardPagesProvider <em>Path Planner Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.PathPlannerToolWizardPagesProvider
	 * @generated
	 */
	public Adapter createPathPlannerToolWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.VehiclePathPlannerToolWizardPagesProvider <em>Vehicle Path Planner Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.VehiclePathPlannerToolWizardPagesProvider
	 * @generated
	 */
	public Adapter createVehiclePathPlannerToolWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.VehicleTrajectoryPickingToolWizardPagesProvider <em>Vehicle Trajectory Picking Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.VehicleTrajectoryPickingToolWizardPagesProvider
	 * @generated
	 */
	public Adapter createVehicleTrajectoryPickingToolWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterBindingWizardPagesProvider <em>Thruster Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterBindingWizardPagesProvider
	 * @generated
	 */
	public Adapter createThrusterBindingWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation <em>Node Presentation</em>}'.
	 * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation
	 * @generated
	 */
  public Adapter createNodePresentationAdapter()
  {
		return null;
	}

  /**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider <em>Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider
	 * @generated
	 */
	public Adapter createWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.topology.ui.NodeWizardPagesProvider <em>Node Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->

	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.topology.ui.NodeWizardPagesProvider
	 * @generated
	 */
	public Adapter createNodeWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider <em>Named Described Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider
	 * @generated
	 */
	public Adapter createNamedDescribedWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider <em>Abstract Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider
	 * @generated
	 */
	public Adapter createAbstractToolWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider <em>Simple Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider
	 * @generated
	 */
	public Adapter createSimpleToolWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider <em>Simple3 DTool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider
	 * @generated
	 */
	public Adapter createSimple3DToolWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.ui.TrajectoryPickingToolWizardPagesProvider <em>Trajectory Picking Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.ui.TrajectoryPickingToolWizardPagesProvider
	 * @generated
	 */
	public Adapter createTrajectoryPickingToolWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider <em>Abstract Topology Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider
	 * @generated
	 */
	public Adapter createAbstractTopologyBindingWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
  public Adapter createEObjectAdapter()
  {
		return null;
	}

} //ApogyAddonsVehicleUIAdapterFactory

package ca.gc.asc_csa.apogy.addons.vehicle.ui.impl;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIFactory;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIPackage;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.PathPlannerToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.PhysicalWheelPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.VehiclePathPlannerToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.VehicleTrajectoryPickingToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.WheelWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyAddonsVehicleUIPackageImpl extends EPackageImpl implements ApogyAddonsVehicleUIPackage
{
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass physicalWheelPresentationEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass landerSphericalFootPresentationEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass thrusterPresentationEClass = null;

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	private EClass wheelWizardPagesProviderEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	private EClass landerSphericalFootPagesProviderEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	private EClass thrusterWizardPagesProviderEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pathPlannerToolWizardPagesProviderEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehiclePathPlannerToolWizardPagesProviderEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehicleTrajectoryPickingToolWizardPagesProviderEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass thrusterBindingWizardPagesProviderEClass = null;

		/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
  private ApogyAddonsVehicleUIPackageImpl()
  {
		super(eNS_URI, ApogyAddonsVehicleUIFactory.eINSTANCE);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private static boolean isInited = false;

  /**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyAddonsVehicleUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
  public static ApogyAddonsVehicleUIPackage init()
  {
		if (isInited) return (ApogyAddonsVehicleUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsVehicleUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyAddonsVehicleUIPackageImpl theApogyAddonsVehicleUIPackage = (ApogyAddonsVehicleUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyAddonsVehicleUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyAddonsVehicleUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyAddonsUIPackage.eINSTANCE.eClass();
		ApogyCommonTopologyBindingsUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyAddonsVehicleUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyAddonsVehicleUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyAddonsVehicleUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyAddonsVehicleUIPackage.eNS_URI, theApogyAddonsVehicleUIPackage);
		return theApogyAddonsVehicleUIPackage;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getPhysicalWheelPresentation()
  {
		return physicalWheelPresentationEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getLanderSphericalFootPresentation()
  {
		return landerSphericalFootPresentationEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public EClass getThrusterPresentation()
  {
		return thrusterPresentationEClass;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThrusterPresentation_PlumeEnvelopeVisible() {
		return (EAttribute)thrusterPresentationEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThrusterPresentation_PlumeEnvelopeLength() {
		return (EAttribute)thrusterPresentationEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	public EClass getWheelWizardPagesProvider() {
		return wheelWizardPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	public EClass getLanderSphericalFootPagesProvider() {
		return landerSphericalFootPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	public EClass getThrusterWizardPagesProvider() {
		return thrusterWizardPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPathPlannerToolWizardPagesProvider() {
		return pathPlannerToolWizardPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVehiclePathPlannerToolWizardPagesProvider() {
		return vehiclePathPlannerToolWizardPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVehicleTrajectoryPickingToolWizardPagesProvider() {
		return vehicleTrajectoryPickingToolWizardPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getThrusterBindingWizardPagesProvider() {
		return thrusterBindingWizardPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsVehicleUIFactory getApogyAddonsVehicleUIFactory() {
		return (ApogyAddonsVehicleUIFactory)getEFactoryInstance();
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isCreated = false;

  /**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void createPackageContents()
  {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		physicalWheelPresentationEClass = createEClass(PHYSICAL_WHEEL_PRESENTATION);

		landerSphericalFootPresentationEClass = createEClass(LANDER_SPHERICAL_FOOT_PRESENTATION);

		thrusterPresentationEClass = createEClass(THRUSTER_PRESENTATION);
		createEAttribute(thrusterPresentationEClass, THRUSTER_PRESENTATION__PLUME_ENVELOPE_VISIBLE);
		createEAttribute(thrusterPresentationEClass, THRUSTER_PRESENTATION__PLUME_ENVELOPE_LENGTH);

		wheelWizardPagesProviderEClass = createEClass(WHEEL_WIZARD_PAGES_PROVIDER);

		landerSphericalFootPagesProviderEClass = createEClass(LANDER_SPHERICAL_FOOT_PAGES_PROVIDER);

		thrusterWizardPagesProviderEClass = createEClass(THRUSTER_WIZARD_PAGES_PROVIDER);

		pathPlannerToolWizardPagesProviderEClass = createEClass(PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER);

		vehiclePathPlannerToolWizardPagesProviderEClass = createEClass(VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER);

		vehicleTrajectoryPickingToolWizardPagesProviderEClass = createEClass(VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER);

		thrusterBindingWizardPagesProviderEClass = createEClass(THRUSTER_BINDING_WIZARD_PAGES_PROVIDER);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isInitialized = false;

  /**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void initializePackageContents()
  {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCommonTopologyUIPackage theApogyCommonTopologyUIPackage = (ApogyCommonTopologyUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyUIPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogyAddonsUIPackage theApogyAddonsUIPackage = (ApogyAddonsUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsUIPackage.eNS_URI);
		ApogyCommonTopologyBindingsUIPackage theApogyCommonTopologyBindingsUIPackage = (ApogyCommonTopologyBindingsUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyBindingsUIPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		physicalWheelPresentationEClass.getESuperTypes().add(theApogyCommonTopologyUIPackage.getNodePresentation());
		landerSphericalFootPresentationEClass.getESuperTypes().add(theApogyCommonTopologyUIPackage.getNodePresentation());
		thrusterPresentationEClass.getESuperTypes().add(theApogyCommonTopologyUIPackage.getNodePresentation());
		wheelWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyUIPackage.getNodeWizardPagesProvider());
		landerSphericalFootPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyUIPackage.getNodeWizardPagesProvider());
		thrusterWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyUIPackage.getNodeWizardPagesProvider());
		pathPlannerToolWizardPagesProviderEClass.getESuperTypes().add(theApogyAddonsUIPackage.getSimple3DToolWizardPagesProvider());
		vehiclePathPlannerToolWizardPagesProviderEClass.getESuperTypes().add(this.getPathPlannerToolWizardPagesProvider());
		vehicleTrajectoryPickingToolWizardPagesProviderEClass.getESuperTypes().add(theApogyAddonsUIPackage.getTrajectoryPickingToolWizardPagesProvider());
		thrusterBindingWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyBindingsUIPackage.getAbstractTopologyBindingWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(physicalWheelPresentationEClass, PhysicalWheelPresentation.class, "PhysicalWheelPresentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(landerSphericalFootPresentationEClass, LanderSphericalFootPresentation.class, "LanderSphericalFootPresentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(thrusterPresentationEClass, ThrusterPresentation.class, "ThrusterPresentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getThrusterPresentation_PlumeEnvelopeVisible(), theEcorePackage.getEBoolean(), "plumeEnvelopeVisible", "false", 0, 1, ThrusterPresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getThrusterPresentation_PlumeEnvelopeLength(), theEcorePackage.getEDouble(), "plumeEnvelopeLength", "1.0", 0, 1, ThrusterPresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(wheelWizardPagesProviderEClass, WheelWizardPagesProvider.class, "WheelWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(landerSphericalFootPagesProviderEClass, LanderSphericalFootPagesProvider.class, "LanderSphericalFootPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(thrusterWizardPagesProviderEClass, ThrusterWizardPagesProvider.class, "ThrusterWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pathPlannerToolWizardPagesProviderEClass, PathPlannerToolWizardPagesProvider.class, "PathPlannerToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vehiclePathPlannerToolWizardPagesProviderEClass, VehiclePathPlannerToolWizardPagesProvider.class, "VehiclePathPlannerToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vehicleTrajectoryPickingToolWizardPagesProviderEClass, VehicleTrajectoryPickingToolWizardPagesProvider.class, "VehicleTrajectoryPickingToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(thrusterBindingWizardPagesProviderEClass, ThrusterBindingWizardPagesProvider.class, "ThrusterBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

		/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "prefix", "ApogyAddonsVehicleUI",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyAddonsVehicleUI",
			 "dynamicTemplates", "true",
			 "suppressGenModelAnnotations", "false",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.addons.vehicle.ui/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.addons.vehicle.ui.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.addons.vehicle"
		   });	
		addAnnotation
		  (physicalWheelPresentationEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nNodePresentation for Physical Wheel."
		   });	
		addAnnotation
		  (landerSphericalFootPresentationEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\n NodePresentation for Lander Spherical Foot."
		   });	
		addAnnotation
		  (thrusterPresentationEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\n NodePresentation for Thruster."
		   });	
		addAnnotation
		  (getThrusterPresentation_PlumeEnvelopeVisible(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not the plume envelope is visible.",
			 "notify", "true",
			 "child", "false",
			 "propertyCategory", "PLUME_INFO"
		   });	
		addAnnotation
		  (getThrusterPresentation_PlumeEnvelopeLength(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe length of the cone used to represent the plume envelope, in meters.",
			 "notify", "true",
			 "child", "false",
			 "apogy_units", "m",
			 "propertyCategory", "PLUME_INFO"
		   });	
		addAnnotation
		  (wheelWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for Wheel creation."
		   });	
		addAnnotation
		  (landerSphericalFootPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for LanderSphericalFoot creation."
		   });	
		addAnnotation
		  (thrusterWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for Thruster creation."
		   });	
		addAnnotation
		  (pathPlannerToolWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for PathPlannerTool."
		   });	
		addAnnotation
		  (vehiclePathPlannerToolWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for VehiclePathPlannerTool."
		   });	
		addAnnotation
		  (vehicleTrajectoryPickingToolWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for VehicleTrajectoryPickingTool."
		   });	
		addAnnotation
		  (thrusterBindingWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\n Wizard support for ThrusterBinding"
		   });
	}

} //ApogyAddonsVehicleUIPackageImpl

package ca.gc.asc_csa.apogy.addons.vehicle.ui;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyAddonsVehicleUI' childCreationExtenders='true' extensibleProviderFactory='true' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' modelName='ApogyAddonsVehicleUI' dynamicTemplates='true' suppressGenModelAnnotations='false' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' modelDirectory='/ca.gc.asc_csa.apogy.addons.vehicle.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.addons.vehicle.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.addons.vehicle'"
 * @generated
 */
public interface ApogyAddonsVehicleUIPackage extends EPackage
{
  /**
	 * The package name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNAME = "ui";

  /**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_URI = "ca.gc.asc_csa.apogy.addons.vehicle.ui";

  /**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_PREFIX = "ui";

  /**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  ApogyAddonsVehicleUIPackage eINSTANCE = ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl.init();

  /**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.PhysicalWheelPresentationImpl <em>Physical Wheel Presentation</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.PhysicalWheelPresentationImpl
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getPhysicalWheelPresentation()
	 * @generated
	 */
  int PHYSICAL_WHEEL_PRESENTATION = 0;

  /**
	 * The feature id for the '<em><b>Topology Presentation Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_WHEEL_PRESENTATION__TOPOLOGY_PRESENTATION_SET = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__TOPOLOGY_PRESENTATION_SET;

		/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION__NODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__NODE;

  /**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION__COLOR = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__COLOR;

  /**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION__VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__VISIBLE;

  /**
	 * The feature id for the '<em><b>Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_WHEEL_PRESENTATION__SELECTED = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SELECTED;

		/**
	 * The feature id for the '<em><b>Shadow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_WHEEL_PRESENTATION__SHADOW_MODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SHADOW_MODE;

		/**
	 * The feature id for the '<em><b>Use In Bounding Calculation</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION__USE_IN_BOUNDING_CALCULATION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__USE_IN_BOUNDING_CALCULATION;

		/**
	 * The feature id for the '<em><b>Id Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int PHYSICAL_WHEEL_PRESENTATION__ID_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ID_VISIBLE;

		/**
	 * The feature id for the '<em><b>Enable Texture Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_WHEEL_PRESENTATION__ENABLE_TEXTURE_PROJECTION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ENABLE_TEXTURE_PROJECTION;

		/**
	 * The feature id for the '<em><b>Centroid</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION__CENTROID = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__CENTROID;

		/**
	 * The feature id for the '<em><b>Min</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION__MIN = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MIN;

		/**
	 * The feature id for the '<em><b>Max</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION__MAX = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MAX;

		/**
	 * The feature id for the '<em><b>XRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION__XRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__XRANGE;

		/**
	 * The feature id for the '<em><b>YRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION__YRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__YRANGE;

		/**
	 * The feature id for the '<em><b>ZRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION__ZRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ZRANGE;

		/**
	 * The feature id for the '<em><b>Scene Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION__SCENE_OBJECT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SCENE_OBJECT;

		/**
	 * The number of structural features of the '<em>Physical Wheel Presentation</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION_FEATURE_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 0;

  /**
	 * The number of operations of the '<em>Physical Wheel Presentation</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PHYSICAL_WHEEL_PRESENTATION_OPERATION_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_OPERATION_COUNT + 0;


  /**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.LanderSphericalFootPresentationImpl <em>Lander Spherical Foot Presentation</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.LanderSphericalFootPresentationImpl
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getLanderSphericalFootPresentation()
	 * @generated
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION = 1;

  /**
	 * The feature id for the '<em><b>Topology Presentation Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PRESENTATION__TOPOLOGY_PRESENTATION_SET = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__TOPOLOGY_PRESENTATION_SET;

		/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION__NODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__NODE;

  /**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION__COLOR = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__COLOR;

  /**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION__VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__VISIBLE;

  /**
	 * The feature id for the '<em><b>Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PRESENTATION__SELECTED = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SELECTED;

		/**
	 * The feature id for the '<em><b>Shadow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PRESENTATION__SHADOW_MODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SHADOW_MODE;

		/**
	 * The feature id for the '<em><b>Use In Bounding Calculation</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION__USE_IN_BOUNDING_CALCULATION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__USE_IN_BOUNDING_CALCULATION;

		/**
	 * The feature id for the '<em><b>Id Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PRESENTATION__ID_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ID_VISIBLE;

		/**
	 * The feature id for the '<em><b>Enable Texture Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PRESENTATION__ENABLE_TEXTURE_PROJECTION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ENABLE_TEXTURE_PROJECTION;

		/**
	 * The feature id for the '<em><b>Centroid</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION__CENTROID = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__CENTROID;

		/**
	 * The feature id for the '<em><b>Min</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION__MIN = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MIN;

		/**
	 * The feature id for the '<em><b>Max</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION__MAX = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MAX;

		/**
	 * The feature id for the '<em><b>XRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION__XRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__XRANGE;

		/**
	 * The feature id for the '<em><b>YRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION__YRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__YRANGE;

		/**
	 * The feature id for the '<em><b>ZRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION__ZRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ZRANGE;

		/**
	 * The feature id for the '<em><b>Scene Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION__SCENE_OBJECT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SCENE_OBJECT;

		/**
	 * The number of structural features of the '<em>Lander Spherical Foot Presentation</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION_FEATURE_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 0;

  /**
	 * The number of operations of the '<em>Lander Spherical Foot Presentation</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LANDER_SPHERICAL_FOOT_PRESENTATION_OPERATION_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_OPERATION_COUNT + 0;


  /**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterPresentationImpl <em>Thruster Presentation</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterPresentationImpl
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getThrusterPresentation()
	 * @generated
	 */
  int THRUSTER_PRESENTATION = 2;

  /**
	 * The feature id for the '<em><b>Topology Presentation Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRUSTER_PRESENTATION__TOPOLOGY_PRESENTATION_SET = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__TOPOLOGY_PRESENTATION_SET;

		/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION__NODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__NODE;

  /**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION__COLOR = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__COLOR;

  /**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION__VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__VISIBLE;

  /**
	 * The feature id for the '<em><b>Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRUSTER_PRESENTATION__SELECTED = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SELECTED;

		/**
	 * The feature id for the '<em><b>Shadow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRUSTER_PRESENTATION__SHADOW_MODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SHADOW_MODE;

		/**
	 * The feature id for the '<em><b>Use In Bounding Calculation</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION__USE_IN_BOUNDING_CALCULATION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__USE_IN_BOUNDING_CALCULATION;

		/**
	 * The feature id for the '<em><b>Id Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_PRESENTATION__ID_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ID_VISIBLE;

		/**
	 * The feature id for the '<em><b>Enable Texture Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRUSTER_PRESENTATION__ENABLE_TEXTURE_PROJECTION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ENABLE_TEXTURE_PROJECTION;

		/**
	 * The feature id for the '<em><b>Centroid</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION__CENTROID = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__CENTROID;

		/**
	 * The feature id for the '<em><b>Min</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION__MIN = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MIN;

		/**
	 * The feature id for the '<em><b>Max</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION__MAX = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MAX;

		/**
	 * The feature id for the '<em><b>XRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION__XRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__XRANGE;

		/**
	 * The feature id for the '<em><b>YRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION__YRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__YRANGE;

		/**
	 * The feature id for the '<em><b>ZRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION__ZRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ZRANGE;

		/**
	 * The feature id for the '<em><b>Scene Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION__SCENE_OBJECT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SCENE_OBJECT;

		/**
	 * The feature id for the '<em><b>Plume Envelope Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRUSTER_PRESENTATION__PLUME_ENVELOPE_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Plume Envelope Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRUSTER_PRESENTATION__PLUME_ENVELOPE_LENGTH = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Thruster Presentation</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION_FEATURE_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 2;

  /**
	 * The number of operations of the '<em>Thruster Presentation</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int THRUSTER_PRESENTATION_OPERATION_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_OPERATION_COUNT + 0;


  /**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.WheelWizardPagesProviderImpl <em>Wheel Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.WheelWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getWheelWizardPagesProvider()
	 * @generated
	 */
	int WHEEL_WIZARD_PAGES_PROVIDER = 3;

		/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int WHEEL_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER__PAGES;

		/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int WHEEL_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER__EOBJECT;

		/**
	 * The number of structural features of the '<em>Wheel Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int WHEEL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int WHEEL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int WHEEL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int WHEEL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int WHEEL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

		/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int WHEEL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

		/**
	 * The number of operations of the '<em>Wheel Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int WHEEL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.LanderSphericalFootPagesProviderImpl <em>Lander Spherical Foot Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.LanderSphericalFootPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getLanderSphericalFootPagesProvider()
	 * @generated
	 */
	int LANDER_SPHERICAL_FOOT_PAGES_PROVIDER = 4;

		/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PAGES_PROVIDER__PAGES = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER__PAGES;

		/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER__EOBJECT;

		/**
	 * The number of structural features of the '<em>Lander Spherical Foot Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

		/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

		/**
	 * The number of operations of the '<em>Lander Spherical Foot Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int LANDER_SPHERICAL_FOOT_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterWizardPagesProviderImpl <em>Thruster Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getThrusterWizardPagesProvider()
	 * @generated
	 */
	int THRUSTER_WIZARD_PAGES_PROVIDER = 5;

		/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER__PAGES;

		/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER__EOBJECT;

		/**
	 * The number of structural features of the '<em>Thruster Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

		/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

		/**
	 * The number of operations of the '<em>Thruster Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.PathPlannerToolWizardPagesProviderImpl <em>Path Planner Tool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.PathPlannerToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getPathPlannerToolWizardPagesProvider()
	 * @generated
	 */
	int PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER = 6;

		/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER__PAGES = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES;

		/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

		/**
	 * The number of structural features of the '<em>Path Planner Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

		/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

		/**
	 * The number of operations of the '<em>Path Planner Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.VehiclePathPlannerToolWizardPagesProviderImpl <em>Vehicle Path Planner Tool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.VehiclePathPlannerToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getVehiclePathPlannerToolWizardPagesProvider()
	 * @generated
	 */
	int VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER = 7;

		/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER__PAGES = PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER__PAGES;

		/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER__EOBJECT = PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

		/**
	 * The number of structural features of the '<em>Vehicle Path Planner Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

		/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

		/**
	 * The number of operations of the '<em>Vehicle Path Planner Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.VehicleTrajectoryPickingToolWizardPagesProviderImpl <em>Vehicle Trajectory Picking Tool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.VehicleTrajectoryPickingToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getVehicleTrajectoryPickingToolWizardPagesProvider()
	 * @generated
	 */
	int VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER = 8;

		/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER__PAGES = ApogyAddonsUIPackage.TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER__PAGES;

		/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyAddonsUIPackage.TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

		/**
	 * The number of structural features of the '<em>Vehicle Trajectory Picking Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyAddonsUIPackage.TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyAddonsUIPackage.TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyAddonsUIPackage.TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyAddonsUIPackage.TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyAddonsUIPackage.TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

		/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyAddonsUIPackage.TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

		/**
	 * The number of operations of the '<em>Vehicle Trajectory Picking Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyAddonsUIPackage.TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterBindingWizardPagesProviderImpl <em>Thruster Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getThrusterBindingWizardPagesProvider()
	 * @generated
	 */
	int THRUSTER_BINDING_WIZARD_PAGES_PROVIDER = 9;

		/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

		/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

		/**
	 * The number of structural features of the '<em>Thruster Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRUSTER_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

		/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int THRUSTER_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

		/**
	 * The number of operations of the '<em>Thruster Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRUSTER_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.PhysicalWheelPresentation <em>Physical Wheel Presentation</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Wheel Presentation</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.PhysicalWheelPresentation
	 * @generated
	 */
  EClass getPhysicalWheelPresentation();

  /**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPresentation <em>Lander Spherical Foot Presentation</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lander Spherical Foot Presentation</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPresentation
	 * @generated
	 */
  EClass getLanderSphericalFootPresentation();

  /**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation <em>Thruster Presentation</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Thruster Presentation</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation
	 * @generated
	 */
  EClass getThrusterPresentation();

  /**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation#isPlumeEnvelopeVisible <em>Plume Envelope Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Plume Envelope Visible</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation#isPlumeEnvelopeVisible()
	 * @see #getThrusterPresentation()
	 * @generated
	 */
	EAttribute getThrusterPresentation_PlumeEnvelopeVisible();

		/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation#getPlumeEnvelopeLength <em>Plume Envelope Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Plume Envelope Length</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation#getPlumeEnvelopeLength()
	 * @see #getThrusterPresentation()
	 * @generated
	 */
	EAttribute getThrusterPresentation_PlumeEnvelopeLength();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.WheelWizardPagesProvider <em>Wheel Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return the meta object for class '<em>Wheel Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.WheelWizardPagesProvider
	 * @generated
	 */
	EClass getWheelWizardPagesProvider();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPagesProvider <em>Lander Spherical Foot Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return the meta object for class '<em>Lander Spherical Foot Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPagesProvider
	 * @generated
	 */
	EClass getLanderSphericalFootPagesProvider();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterWizardPagesProvider <em>Thruster Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return the meta object for class '<em>Thruster Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterWizardPagesProvider
	 * @generated
	 */
	EClass getThrusterWizardPagesProvider();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.PathPlannerToolWizardPagesProvider <em>Path Planner Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path Planner Tool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.PathPlannerToolWizardPagesProvider
	 * @generated
	 */
	EClass getPathPlannerToolWizardPagesProvider();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.VehiclePathPlannerToolWizardPagesProvider <em>Vehicle Path Planner Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vehicle Path Planner Tool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.VehiclePathPlannerToolWizardPagesProvider
	 * @generated
	 */
	EClass getVehiclePathPlannerToolWizardPagesProvider();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.VehicleTrajectoryPickingToolWizardPagesProvider <em>Vehicle Trajectory Picking Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vehicle Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.VehicleTrajectoryPickingToolWizardPagesProvider
	 * @generated
	 */
	EClass getVehicleTrajectoryPickingToolWizardPagesProvider();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterBindingWizardPagesProvider <em>Thruster Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Thruster Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterBindingWizardPagesProvider
	 * @generated
	 */
	EClass getThrusterBindingWizardPagesProvider();

		/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyAddonsVehicleUIFactory getApogyAddonsVehicleUIFactory();

		/**
	 * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each operation of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
	 * @generated
	 */
  interface Literals
  {
    /**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.PhysicalWheelPresentationImpl <em>Physical Wheel Presentation</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.PhysicalWheelPresentationImpl
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getPhysicalWheelPresentation()
		 * @generated
		 */
    EClass PHYSICAL_WHEEL_PRESENTATION = eINSTANCE.getPhysicalWheelPresentation();
    /**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.LanderSphericalFootPresentationImpl <em>Lander Spherical Foot Presentation</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.LanderSphericalFootPresentationImpl
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getLanderSphericalFootPresentation()
		 * @generated
		 */
    EClass LANDER_SPHERICAL_FOOT_PRESENTATION = eINSTANCE.getLanderSphericalFootPresentation();
    /**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterPresentationImpl <em>Thruster Presentation</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterPresentationImpl
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getThrusterPresentation()
		 * @generated
		 */
    EClass THRUSTER_PRESENTATION = eINSTANCE.getThrusterPresentation();
				/**
		 * The meta object literal for the '<em><b>Plume Envelope Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THRUSTER_PRESENTATION__PLUME_ENVELOPE_VISIBLE = eINSTANCE.getThrusterPresentation_PlumeEnvelopeVisible();
				/**
		 * The meta object literal for the '<em><b>Plume Envelope Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THRUSTER_PRESENTATION__PLUME_ENVELOPE_LENGTH = eINSTANCE.getThrusterPresentation_PlumeEnvelopeLength();
				/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.WheelWizardPagesProviderImpl <em>Wheel Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->

		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.WheelWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getWheelWizardPagesProvider()
		 * @generated
		 */
		EClass WHEEL_WIZARD_PAGES_PROVIDER = eINSTANCE.getWheelWizardPagesProvider();
				/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.LanderSphericalFootPagesProviderImpl <em>Lander Spherical Foot Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->

		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.LanderSphericalFootPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getLanderSphericalFootPagesProvider()
		 * @generated
		 */
		EClass LANDER_SPHERICAL_FOOT_PAGES_PROVIDER = eINSTANCE.getLanderSphericalFootPagesProvider();
				/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterWizardPagesProviderImpl <em>Thruster Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->

		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getThrusterWizardPagesProvider()
		 * @generated
		 */
		EClass THRUSTER_WIZARD_PAGES_PROVIDER = eINSTANCE.getThrusterWizardPagesProvider();
				/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.PathPlannerToolWizardPagesProviderImpl <em>Path Planner Tool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.PathPlannerToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getPathPlannerToolWizardPagesProvider()
		 * @generated
		 */
		EClass PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getPathPlannerToolWizardPagesProvider();
				/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.VehiclePathPlannerToolWizardPagesProviderImpl <em>Vehicle Path Planner Tool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.VehiclePathPlannerToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getVehiclePathPlannerToolWizardPagesProvider()
		 * @generated
		 */
		EClass VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getVehiclePathPlannerToolWizardPagesProvider();
				/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.VehicleTrajectoryPickingToolWizardPagesProviderImpl <em>Vehicle Trajectory Picking Tool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.VehicleTrajectoryPickingToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getVehicleTrajectoryPickingToolWizardPagesProvider()
		 * @generated
		 */
		EClass VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getVehicleTrajectoryPickingToolWizardPagesProvider();
				/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterBindingWizardPagesProviderImpl <em>Thruster Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ThrusterBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.vehicle.ui.impl.ApogyAddonsVehicleUIPackageImpl#getThrusterBindingWizardPagesProvider()
		 * @generated
		 */
		EClass THRUSTER_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getThrusterBindingWizardPagesProvider();

  }

} //ApogyAddonsVehicleUIPackage

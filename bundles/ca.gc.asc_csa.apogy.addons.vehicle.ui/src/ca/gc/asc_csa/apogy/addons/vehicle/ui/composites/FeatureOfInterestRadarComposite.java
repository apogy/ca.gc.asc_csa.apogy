/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.vehicle.ui.composites;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.PolarPlot;
import org.jfree.chart.renderer.DefaultPolarItemRenderer;
import org.jfree.data.Range;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.experimental.chart.swt.ChartComposite;

import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter;
import ca.gc.asc_csa.apogy.core.FeatureOfInterest;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;
import ca.gc.asc_csa.apogy.core.invocator.VariablesList;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.VariableFeatureReferenceComposite;
import ca.gc.asc_csa.apogy.core.ui.composites.ApogySystemApiAdapterBasedComposite;

public class FeatureOfInterestRadarComposite extends ApogySystemApiAdapterBasedComposite 
{	
	public static final String DEGREE_STRING = "\u00b0";
	
	private Adapter poseAdapter;
	private VariablesList variablesList;
	
	private ApogySystemApiAdapter apogySystemApiAdapter;
	private Matrix4d variablePose;
	
	private Text txtVariableFeatureReference;
	private Spinner maximumRadiusSpinner;
	
	private JFreeChart chart;
	private XYSeriesCollection xySeriesCollection = null;
	private XYSeries currentDataSeries = null;

	private DecimalFormat angleDecimalFormat = new DecimalFormat("0");
	private DecimalFormat distanceDecimalFormat = new DecimalFormat("0.0");
	
	private double maximumRadius = 50;
	
	public FeatureOfInterestRadarComposite(Composite parent, int style)	
	{
		this(parent, style, null, null);
	}
	
	public FeatureOfInterestRadarComposite(Composite parent, int style, VariablesList variablesList, VariableFeatureReference variableFeatureReference) 
	{
		super(parent, style);
		
		setVariablesList(variablesList);
		setVariableFeatureReference(variableFeatureReference);
				
		GridLayout layout = new GridLayout(3, false);		
		setLayout(layout);		
		
		Label variableFeatureLabel = new Label(this, SWT.NONE);
		variableFeatureLabel.setText("Variable Feature :");
		
		txtVariableFeatureReference = new Text(this, SWT.BORDER);
		GridData gd_txtVariableFeatureReference = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtVariableFeatureReference.widthHint = 300;
		txtVariableFeatureReference.setLayoutData(gd_txtVariableFeatureReference);
		
		Button btnSelectVariableFeatureReference = new Button(this, SWT.PUSH);
		btnSelectVariableFeatureReference.setText("Select...");
		btnSelectVariableFeatureReference.addSelectionListener(new SelectionListener() 
		{
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				final Shell shell = Display.getCurrent().getActiveShell();
				Dialog dialog = new Dialog(shell) 
				{
					VariableFeatureReferenceComposite vfrComposite = null;
					
					@Override
					protected Control createDialogArea(Composite parent) 
					{
						Composite area = (Composite) super.createDialogArea(parent);
						
						vfrComposite = new VariableFeatureReferenceComposite(area, SWT.NONE);
						vfrComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));		
						vfrComposite.set(FeatureOfInterestRadarComposite.this.variablesList, FeatureOfInterestRadarComposite.this.getVariableFeatureReference());											
						
						return area;
					}				
					
					@Override
					public boolean close() 
					{
						if(vfrComposite != null && !vfrComposite.isDisposed())
						{
							vfrComposite.set(null, null);
							vfrComposite.dispose();
						}
						
						return super.close();
					}
				};		
				
				int result  = dialog.open();
				if(result == Dialog.OK)
				{
					// Update displayed VFR.
					updateDisplayedVariableFeatureReference();
				}									
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {				
			}
		});
		
		Label maxRadiusLabel = new Label(this, SWT.NONE);
		maxRadiusLabel.setText("Maximum Radius (m) :");

		maximumRadiusSpinner = new Spinner(this, SWT.NONE);
		maximumRadiusSpinner.setMinimum(1);
		maximumRadiusSpinner.setMaximum(1000);
		maximumRadiusSpinner.setSelection((int) Math.round(getMaximumRadius()));
		maximumRadiusSpinner.addSelectionListener(new SelectionListener() {			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				setMaximumRadius(maximumRadiusSpinner.getSelection(), false);			
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) 
			{				
			}
		});
		
		GridData gd_maximumRadiusSpinner = new GridData(SWT.LEFT,SWT.CENTER, false, false, 1, 1);
		gd_maximumRadiusSpinner.widthHint = 100;
		gd_maximumRadiusSpinner.minimumWidth = 100;		
		maximumRadiusSpinner.setLayoutData(gd_maximumRadiusSpinner);
		
		// Filler.
		new Label(this, SWT.NONE);
		
		// Adds the chart parentComposite.
		ChartComposite chartComposite = new ChartComposite(this, SWT.NONE, getChart(), true);
		GridData gd_chartComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
		chartComposite.setLayoutData(gd_chartComposite);				
		
		addDisposeListener(new DisposeListener() 
		{		
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(apogySystemApiAdapter != null)
				{
					apogySystemApiAdapter.eAdapters().remove(getPoseAdapter());
				}
			}
		});
	}	
	
	public void setVariablesList(VariablesList newVariablesList)
	{
		this.variablesList = newVariablesList;				
	}
	
	@Override
	public void setVariableFeatureReference(VariableFeatureReference newVariableFeatureReference) 
	{
		super.setVariableFeatureReference(newVariableFeatureReference);
		
		getDisplay().asyncExec(new Runnable() 
		{									
			@Override
			public void run() 
			{										
				updateDisplayedVariableFeatureReference();
			}
		});
	}
	
	public double getMaximumRadius() 
	{
		return maximumRadius;
	}

	public void setMaximumRadius(double maximumRadius) 
	{
		setMaximumRadius(maximumRadius, true);
	}
	
	private void setMaximumRadius(double maximumRadius, boolean updateSpinner) 
	{
		if(maximumRadius > 0.0)
		{
			this.maximumRadius = maximumRadius;
			
			PolarPlot plot = (PolarPlot) chart.getPlot();
			plot.getAxis().setRange(new Range(0, maximumRadius));	
			
			double tickSize = (maximumRadius / 5.0);
		    NumberAxis numberAxis = (NumberAxis) plot.getAxis();
		    numberAxis.setTickUnit(new NumberTickUnit(tickSize));
		  			
		    if(maximumRadiusSpinner != null && !maximumRadiusSpinner.isDisposed())
		    {
		    	maximumRadiusSpinner.setSelection((int) Math.round(maximumRadius));
		    }
		    
			updateDisplay();
		}
	}

	protected void updateDisplayedVariableFeatureReference()
	{
		if(txtVariableFeatureReference != null && !txtVariableFeatureReference.isDisposed())
		{
			String text = ApogyCoreInvocatorFacade.INSTANCE.getVariableFeatureReferenceString(this.getVariableFeatureReference());
			txtVariableFeatureReference.setText(text);
		}
	}
		
	protected List<FeatureOfInterest> getFeatureOfInterest()
	{
		List<FeatureOfInterest> list = new ArrayList<FeatureOfInterest>();
			
		list.addAll(ApogyCoreEnvironmentFacade.INSTANCE.getAllFeatureOfInterestInActiveSession());
						
		return list;
	}
	
	protected void updateDisplay()
	{		
		getDisplay().asyncExec(new Runnable() {
			
			@Override
			public void run() 
			{
				if(!isDisposed())
				{
					try
					{
						// First, gets the list of FeatureOfInterest to display.
						List<FeatureOfInterest> fois = getFeatureOfInterest();
						
						// Gets the current position of the variable.
						if(apogySystemApiAdapter != null && apogySystemApiAdapter.getPoseTransform() != null)
						{
							variablePose = apogySystemApiAdapter.getPoseTransform().asMatrix4d();		
							getCurrentDataSeries().clear();
							populateDataSeries(getCurrentDataSeries(), fois, variablePose);
						}
					}
					catch(Throwable t)
					{
						t.printStackTrace();
					}
				}
			}
		});		
	}
	
	private class LabeledXYDataItem extends XYDataItem
	{		
		private static final long serialVersionUID = 1808722594301757066L;
		
		public String label;
		
		public LabeledXYDataItem(double x, double y) {
			super(x, y);			
		}
		
		public LabeledXYDataItem(double x, double y, String label) 
		{
			super(x, y);	
			this.label = label;
		}
		
	}

	@Override
	protected void apogySystemApiAdapterChanged(ApogySystemApiAdapter oldApogySystemApiAdapter,	ApogySystemApiAdapter newApogySystemApiAdapter) 
	{
		if(oldApogySystemApiAdapter != null)
		{
			oldApogySystemApiAdapter.eAdapters().remove(getPoseAdapter());
		}
		this.apogySystemApiAdapter = newApogySystemApiAdapter;
		
		if(newApogySystemApiAdapter != null)
		{
			updateDisplay();
			newApogySystemApiAdapter.eAdapters().add(getPoseAdapter());			
		}
	}
	
	protected XYSeriesCollection getXYSeriesCollection()
	{
		if(xySeriesCollection == null)
		{
			xySeriesCollection = new XYSeriesCollection();		
			xySeriesCollection.addSeries(getCurrentDataSeries());			
		}
		
		return xySeriesCollection;
	}
	
	protected XYSeries getCurrentDataSeries()
	{
		if(currentDataSeries == null)
		{
			currentDataSeries = new XYSeries("FOIs", false);					
		}
		
		return currentDataSeries;
	}
	
	protected void populateDataSeries(XYSeries xySeries, List<FeatureOfInterest> foiToDisplay, Matrix4d variablePose)
	{				
		Vector3d v = new Vector3d();
		variablePose.get(v);
		
		Matrix4d inv = new Matrix4d(variablePose);
		inv.invert();
		
		for(FeatureOfInterest foi : foiToDisplay)
		{
			Vector3d foiPosition = new Vector3d();
			foi.getPose().asMatrix4d().get(foiPosition);
			
			Matrix4d foiPose = new Matrix4d();
			foiPose.setIdentity();
			foiPose.setTranslation(foiPosition);
			foiPose.invert();			
			foiPose.mul(variablePose);
			foiPose.invert();	
			
			Vector3d foiRelativePosition = new Vector3d();
			foiPose.get(foiRelativePosition);
			
			double radius = foiRelativePosition.length();
				
			// Adds item that are inside the plot.
			if(radius <= getMaximumRadius())
			{
				double theta = Math.toRadians(-90) + Math.atan2(foiRelativePosition.x, foiRelativePosition.y);								
				double displayedTheta = -theta;
						
				// Keeps displayed angle between -180 and + 180 deg.
				if(displayedTheta > Math.PI)
				{
					displayedTheta -= 2*Math.PI;
				}
				else if(displayedTheta < -Math.PI)
				{
					displayedTheta += 2*Math.PI; 
				}
									
				String foiLabel = "?";
				if(foi.getName() != null && foi.getName().length() > 0)
				{
					foiLabel = foi.getName();
				}
				String label = foiLabel + " (" + angleDecimalFormat.format(Math.toDegrees(displayedTheta)) + DEGREE_STRING + ", " + distanceDecimalFormat.format(radius) + "m)";		
				LabeledXYDataItem item = new LabeledXYDataItem(Math.toDegrees(theta), radius, label);
				xySeries.add(item);
			}
		}		
	}
			
	protected JFreeChart getChart()
	{
		if(chart == null)
		{
			chart = ChartFactory.createPolarChart("", getXYSeriesCollection(), false, false, false); 
			
			final PolarPlot plot = (PolarPlot) chart.getPlot();
			plot.setBackgroundPaint(Color.black);
			plot.setAngleGridlinePaint(Color.green);		
			plot.setAngleLabelsVisible(false);
			plot.setAngleLabelPaint(Color.green);
			plot.setRadiusGridlinePaint(Color.green);
			plot.setRadiusGridlinesVisible(true);
			plot.setRadiusGridlinePaint(Color.green);
											
	        plot.getAxis().setAutoRange(false);
	        plot.getAxis().setInverted(false);
	        plot.getAxis().setRange(new Range(0, getMaximumRadius()));	        
	        
	        Font ticksFont = plot.getAxis().getLabelFont().deriveFont(16);
	        
	        plot.getAxis().setTickLabelsVisible(true);  
	        plot.getAxis().setTickLabelFont(ticksFont);
	        plot.getAxis().setTickLabelPaint(Color.green);  
	        plot.getAxis().setAxisLineStroke(new BasicStroke(2.0f));
	        plot.getAxis().setAxisLinePaint(Color.green);
	       
	        	        
	        DefaultPolarItemRenderer defaultPolarItemRenderer = new DefaultPolarItemRenderer()
	        {
	        	private static final long serialVersionUID = 3822995919130258873L;
	
				@Override
				public void drawSeries(Graphics2D g2, Rectangle2D dataArea, PlotRenderingInfo info, PolarPlot plot, XYDataset dataset, int seriesIndex) 
				{
					Font font = plot.getAxis().getLabelFont().deriveFont(12.0f);
					g2.setFont(font);					
					g2.setColor(Color.yellow);
					
					int numPoints = dataset.getItemCount(seriesIndex);				
															
			        for (int i = 0; i < numPoints; i++) 
			        {			        				        	
			            double theta = dataset.getXValue(seriesIndex, i);
			            double radius = dataset.getYValue(seriesIndex, i);
			            
			            Point p = plot.translateValueThetaRadiusToJava2D(theta, radius, dataArea);			            			            		            			           
			            Ellipse2D el = new Ellipse2D.Double(p.x, p.y, 5, 5);
						            
			            g2.fill(el);
			            g2.draw(el);	
			            		
			        
			            // Assume we have only our series !
			            LabeledXYDataItem item = (LabeledXYDataItem) getCurrentDataSeries().getItems().get(i);
			            if(item != null && item.label != null)
			            {
			            	g2.drawString(item.label ,p.x, p.y);
			            }
			        }
				}
	        };
	        defaultPolarItemRenderer.setBaseItemLabelsVisible(true);
	        
	        plot.setRenderer(defaultPolarItemRenderer);	        
	        	        
		}
		return chart;
	}
	
	private Adapter getPoseAdapter() 
	{
		if(poseAdapter == null)
		{
			poseAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof ApogySystemApiAdapter)
					{
						int featureID = msg.getFeatureID(ApogySystemApiAdapter.class);
						switch (featureID) 
						{
							case ApogyCorePackage.APOGY_SYSTEM_API_ADAPTER__POSE_TRANSFORM:								
								updateDisplay();
							break;

						default:
							break;
						}
					}
				}
			};
		}
		return poseAdapter;
	}		
}

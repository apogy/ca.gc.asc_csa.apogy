/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards;

import java.text.DecimalFormat;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage;
import ca.gc.asc_csa.apogy.addons.vehicle.LanderSphericalFoot;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;

public class LanderSphericalFootWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards.LanderSphericalFootWizardPage";
	public static String NO_VALUE_AVAILABLE_STRING = "N/A";
	private int LABEL_WIDTH = 200; 
	private int VALUE_WIDTH = 100; 
	private int BUTTON_WIDTH = 50; 		
	
	private LanderSphericalFoot landerSphericalFoot;
	private Adapter adapter;
	
	private TypedElementSimpleUnitsComposite radiusComposite;
	
	
	public LanderSphericalFootWizardPage(LanderSphericalFoot landerSphericalFoot)
	{
		super(WIZARD_PAGE_ID);
		this.landerSphericalFoot = landerSphericalFoot;
				
		setTitle("Lander Spherical Foot");
		setDescription("Sets the Lander Spherical Foot parameters.");			
	}
	
	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
		setControl(top);

		radiusComposite = new TypedElementSimpleUnitsComposite(top, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.000");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Radius :";
			}
		};
		radiusComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		radiusComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsVehiclePackage.Literals.LANDER_SPHERICAL_FOOT__RADIUS), getLanderSphericalFoot());
		
		if(landerSphericalFoot != null)  landerSphericalFoot.eAdapters().add(getAdapter());		
		top.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent arg0) {				
				if(landerSphericalFoot != null) landerSphericalFoot.eAdapters().remove(getAdapter());
			}
		});
		
		validate();
	}

	public LanderSphericalFoot getLanderSphericalFoot() {
		return landerSphericalFoot;
	}

	public void setLanderSphericalFoot(LanderSphericalFoot landerSphericalFoot) 
	{
		this.landerSphericalFoot = landerSphericalFoot;
		
		if(radiusComposite != null && !radiusComposite.isDisposed())
		{
			radiusComposite.setInstance(landerSphericalFoot);
		}	
	}

	protected void validate()
	{
		setErrorMessage(null);
		
		if(landerSphericalFoot.getRadius() <= 0)
		{
			setErrorMessage("The specified radius must be greater or equal to zero !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	
	protected Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof LanderSphericalFoot)
					{
						validate();
					}
				}
			};
		}
		return adapter;
	}
}

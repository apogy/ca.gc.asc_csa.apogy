/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.vehicle.ui.parts;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIRCPConstants;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.composites.VehiculePoseComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFactory;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;
import ca.gc.asc_csa.apogy.core.invocator.VariablesList;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;

public class VehiclePosePart extends AbstractSessionBasedPart 
{
	private double tripDistance = 0.0;
	
	private double alarmMinPitch = Math.toRadians(-20);
	private double alarmMaxPitch = Math.toRadians(20);
	
	private double warningMinPitch = Math.toRadians(-10);
	private double warningMaxPitch = Math.toRadians(10);
	
	private double alarmMinRoll = Math.toRadians(-20);
	private double alarmMaxRoll = Math.toRadians(20);
	
	private double warningMinRoll = Math.toRadians(-10);
	private double warningMaxRoll = Math.toRadians(10);
	
	private VehiculePoseComposite vehiculePoseComposite;	
	private VariableFeatureReference variableFeatureReference = ApogyCoreInvocatorFactory.eINSTANCE.createVariableFeatureReference();
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{	
		if(invocatorSession != null)
		{
			if(vehiculePoseComposite != null)
			{
				readAllValuesFromPersistedState();
				
				vehiculePoseComposite.setVariableFeatureReference(variableFeatureReference);		
				vehiculePoseComposite.setTripDistance(tripDistance);
								
				vehiculePoseComposite.getAttitudeIndicatorComposite().setPitchAlarmValues(alarmMinPitch, alarmMaxPitch);
				vehiculePoseComposite.getAttitudeIndicatorComposite().setPitchWarningValues(warningMinPitch, warningMaxPitch);
				vehiculePoseComposite.getAttitudeIndicatorComposite().setRollAlarmValues(alarmMinRoll, alarmMaxRoll);
				vehiculePoseComposite.getAttitudeIndicatorComposite().setRollWarningValues(warningMinRoll, warningMaxRoll);
				
				vehiculePoseComposite.setVariableList(invocatorSession.getEnvironment().getVariablesList());
			}
		}
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{		
		parent.setLayout(new FillLayout());
		
		VariablesList variables = null;
		
		if(ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() != null &&
		   ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment() != null)
		{
			ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment().getVariablesList();
		}
				
		ApogyCommonTransactionFacade.INSTANCE.addInTempTransactionalEditingDomain(variableFeatureReference);
		vehiculePoseComposite = new VehiculePoseComposite(parent, style, variables, variableFeatureReference, tripDistance);
		
		vehiculePoseComposite.getAttitudeIndicatorComposite().setPitchAlarmValues(alarmMinPitch, alarmMaxPitch);
		vehiculePoseComposite.getAttitudeIndicatorComposite().setPitchWarningValues(warningMinPitch, warningMaxPitch);
		vehiculePoseComposite.getAttitudeIndicatorComposite().setRollAlarmValues(alarmMinRoll, alarmMaxRoll);
		vehiculePoseComposite.getAttitudeIndicatorComposite().setRollWarningValues(warningMinRoll, warningMaxRoll);		
	}
	
	@Override
	public void userPostConstruct(MPart mPart) 
	{	
		try
		{	
			readAllValuesFromPersistedState();	
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}				
	}
	
	@Override
	public void userPersistState(MPart mPart)
	{		
		if(vehiculePoseComposite != null)
		{			
			ApogyCoreInvocatorUIFacade.INSTANCE.saveToPersistedState(mPart, ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__VARIABLE_FEATURE_REFERENCE_ID, variableFeatureReference);
		
			mPart.getPersistedState().put(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__DISTANCE, Double.toString(vehiculePoseComposite.getTripDistance()));	
			mPart.getPersistedState().put(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MIN_PITCH, Double.toString(vehiculePoseComposite.getAttitudeIndicatorComposite().getPitchAlarmMinLimit()));
			mPart.getPersistedState().put(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MAX_PITCH, Double.toString(vehiculePoseComposite.getAttitudeIndicatorComposite().getPitchAlarmMaxLimit()));
			mPart.getPersistedState().put(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MIN_PITCH, Double.toString(vehiculePoseComposite.getAttitudeIndicatorComposite().getPitchWarningMinLimit()));
			mPart.getPersistedState().put(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MAX_PITCH, Double.toString(vehiculePoseComposite.getAttitudeIndicatorComposite().getPitchWarningMaxLimit()));
			
			mPart.getPersistedState().put(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MIN_ROLL, Double.toString(vehiculePoseComposite.getAttitudeIndicatorComposite().getRollAlarmMinLimit()));
			mPart.getPersistedState().put(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MAX_ROLL, Double.toString(vehiculePoseComposite.getAttitudeIndicatorComposite().getRollAlarmMaxLimit()));
			mPart.getPersistedState().put(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MIN_ROLL, Double.toString(vehiculePoseComposite.getAttitudeIndicatorComposite().getRollWarningMinLimit()));
			mPart.getPersistedState().put(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MAX_ROLL, Double.toString(vehiculePoseComposite.getAttitudeIndicatorComposite().getRollWarningMaxLimit()));
		}		
	}
	
	private void readAllValuesFromPersistedState()
	{
		double value = getDoubleValueFromPersistedState(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__DISTANCE);
		if(!Double.isNaN(value)) tripDistance = value;			
		
		value = getDoubleValueFromPersistedState(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MIN_PITCH);
		if(!Double.isNaN(value)) alarmMinPitch = value;
		
		value = getDoubleValueFromPersistedState(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MAX_PITCH);
		if(!Double.isNaN(value)) alarmMaxPitch = value;
				
		value = getDoubleValueFromPersistedState(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MIN_PITCH);
		if(!Double.isNaN(value)) warningMinPitch = value;
		
		value = getDoubleValueFromPersistedState(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MAX_PITCH);
		if(!Double.isNaN(value)) warningMaxPitch = value;
								
		value = getDoubleValueFromPersistedState(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MIN_ROLL);
		if(!Double.isNaN(value)) alarmMinRoll = value;
		
		value = getDoubleValueFromPersistedState(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MAX_ROLL);
		if(!Double.isNaN(value)) alarmMaxRoll = value;
				
		value = getDoubleValueFromPersistedState(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MIN_ROLL);
		if(!Double.isNaN(value)) warningMinRoll = value;
		
		value = getDoubleValueFromPersistedState(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MAX_ROLL);
		if(!Double.isNaN(value)) warningMaxRoll = value;
				
		VariableFeatureReference vfr = (VariableFeatureReference) ApogyCoreInvocatorUIFacade.INSTANCE.readFromPersistedState(mPart, ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__VEHICLE_POSE_PART__VARIABLE_FEATURE_REFERENCE_ID);
		if(vfr != null) this.variableFeatureReference = vfr;		
	}
	
	private double getDoubleValueFromPersistedState(String key)
	{
		double value = Double.NaN;
		
		String valueString = mPart.getPersistedState().get(key);
		if(valueString != null)
		{
			try
			{
				value = Double.parseDouble(valueString);
			}
			catch (Exception e) 
			{
			}
		}
		
		return value;
	}
}

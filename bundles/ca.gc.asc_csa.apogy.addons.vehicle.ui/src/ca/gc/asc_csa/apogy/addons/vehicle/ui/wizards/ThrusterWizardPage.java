/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards;

import java.text.DecimalFormat;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage;
import ca.gc.asc_csa.apogy.addons.vehicle.Thruster;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;

public class ThrusterWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards.ThrusterWizardPage";
	public static String NO_VALUE_AVAILABLE_STRING = "N/A";
	private int LABEL_WIDTH = 200; 
	private int VALUE_WIDTH = 100; 
	private int BUTTON_WIDTH = 50; 		
	
	private Thruster thruster;
	private Adapter adapter;
	
	private TypedElementSimpleUnitsComposite minimumThrustComposite;
	private TypedElementSimpleUnitsComposite maximumThrustComposite;
	private TypedElementSimpleUnitsComposite plumeAngleComposite;
	
	public ThrusterWizardPage(Thruster thruster)
	{
		super(WIZARD_PAGE_ID);
		this.thruster = thruster;
				
		setTitle("Thruster");
		setDescription("Sets the thuster parameters.");			
	}
	
	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
		setControl(top);

		minimumThrustComposite = new TypedElementSimpleUnitsComposite(top, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.000");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Minimum Thrust :";
			}
		};
		minimumThrustComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		minimumThrustComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsVehiclePackage.Literals.THRUSTER__MINIMUM_THRUST), getThruster());


		maximumThrustComposite = new TypedElementSimpleUnitsComposite(top, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.000");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Maximum Thrust :";
			}
		};
		maximumThrustComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		maximumThrustComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsVehiclePackage.Literals.THRUSTER__MAXIMUM_THRUST), getThruster());

		
		plumeAngleComposite = new TypedElementSimpleUnitsComposite(top, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.0");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Plume Angle :";
			}
		};
		plumeAngleComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		plumeAngleComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsVehiclePackage.Literals.THRUSTER__PLUME_ANGLE), getThruster());

		
		if(thruster != null)  thruster.eAdapters().add(getAdapter());		
		top.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent arg0) {				
				if(thruster != null) thruster.eAdapters().remove(getAdapter());
			}
		});
		
		validate();
	}

	public Thruster getThruster() {
		return thruster;
	}

	public void setThruster(Thruster thruster) 
	{
		this.thruster = thruster;
		
		if(minimumThrustComposite != null && !minimumThrustComposite.isDisposed())
		{
			minimumThrustComposite.setInstance(thruster);
		}	
		
		if(maximumThrustComposite != null && !maximumThrustComposite.isDisposed())
		{
			maximumThrustComposite.setInstance(thruster);
		}	
		
		if(plumeAngleComposite != null && !plumeAngleComposite.isDisposed())
		{
			plumeAngleComposite.setInstance(thruster);
		}	
	}

	protected void validate()
	{
		setErrorMessage(null);
		
		if(thruster.getMinimumThrust() < 0)
		{
			setErrorMessage("The specified minimum thrust must be greater or equal to zero !");
		}
		
		if(thruster.getMaximumThrust() < 0)
		{
			setErrorMessage("The specified maximum thrust must be greater or equal to zero !");
		}
		
		if(thruster.getMaximumThrust() < thruster.getMinimumThrust())
		{
			setErrorMessage("The specified maximum thrust must be greater or equal than the minimum thrust !");
		}
		
		if(thruster.getPlumeAngle() <= 0)
		{
			setErrorMessage("The specified plume angle must be greater or equal than zero !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	
	protected Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof Thruster)
					{
						validate();
					}
				}
			};
		}
		return adapter;
	}
}

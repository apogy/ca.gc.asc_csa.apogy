/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage;
import ca.gc.asc_csa.apogy.addons.vehicle.VehicleTrajectoryPickingTool;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.VariablesListComposite;

public class VehicleTrajectoryPickingToolWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards.VehicleTrajectoryPickingToolWizardPage";
		
	private VehicleTrajectoryPickingTool vehicleTrajectoryPickingTool;
	
	private VariablesListComposite variablesListComposite;
	
	private DataBindingContext m_bindingContext;
	
	public VehicleTrajectoryPickingToolWizardPage(VehicleTrajectoryPickingTool vehicleTrajectoryPickingTool) 
	{
		super(WIZARD_PAGE_ID);
		this.vehicleTrajectoryPickingTool = vehicleTrajectoryPickingTool;
			
		setTitle("Vehicule Trajectory Picking Tool.");
		setDescription("Configure the Vehicle.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		variablesListComposite =  new VariablesListComposite(top, SWT.NONE)
		{
			@Override
			protected void newSelection(ISelection selection) 
			{
				VariableFeatureReference variableFeatureReference = variablesListComposite.getSelectedVariableFeatureReference();
				
				if(ApogyCommonTransactionFacade.INSTANCE.areEditingDomainsValid(vehicleTrajectoryPickingTool, ApogyAddonsVehiclePackage.Literals.VEHICLE_TRAJECTORY_PICKING_TOOL__VEHICULE_VARIABLE_FEATURE_REFERENCE, variableFeatureReference, true) == ApogyCommonTransactionFacade.EXECUTE_COMMAND_ON_OWNER_DOMAIN)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet(vehicleTrajectoryPickingTool, ApogyAddonsVehiclePackage.Literals.VEHICLE_TRAJECTORY_PICKING_TOOL__VEHICULE_VARIABLE_FEATURE_REFERENCE, variableFeatureReference);
				}
				else
				{				
					vehicleTrajectoryPickingTool.setVehiculeVariableFeatureReference(variableFeatureReference);
				}
			}
		};
		
		InvocatorSession invocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();		
		if(invocatorSession != null && invocatorSession.getEnvironment() != null)
		{
			variablesListComposite.setVariablesList(invocatorSession.getEnvironment().getVariablesList());
		}
		
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		top.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{				
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	

	protected void validate()
	{
		setErrorMessage(null);		
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.vehicle.ui.composites;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.vehicle.ui.utils.ImageUtils;
import ca.gc.asc_csa.apogy.common.emf.Ranges;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Matrix4x4;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter;
import ca.gc.asc_csa.apogy.core.PoseProvider;
import ca.gc.asc_csa.apogy.core.ui.composites.ApogySystemApiAdapterBasedComposite;

public class AttitudeIndicatorComposite extends ApogySystemApiAdapterBasedComposite 
{
	public static final String DEGREE_STRING = "\u00b0";
	public static final int MAX_ROLL = 60;	
	public static final int ROLL_INCREMENT = 10;
	
	private ApogySystemApiAdapter apogySystemApiAdapter;
	private Adapter poseAdapter;
		
	private boolean busy = false;
	private Label label;
	private Image currentImage = null;
	
	private int width = 300;	
	private int height = 300;
	
	private Color sky = new Color(0, 0, 255);
	private Color ground = new Color(150, 75, 0);
		
	private double pitchAngleIncrement = 10;
	
	private double alarmMinPitch = Math.toRadians(-20);
	private double alarmMaxPitch = Math.toRadians(20);
	
	private double warningMinPitch = Math.toRadians(-10);
	private double warningMaxPitch = Math.toRadians(10);
	
	private double alarmMinRoll = Math.toRadians(-20);
	private double alarmMaxRoll = Math.toRadians(20);
	
	private double warningMinRoll = Math.toRadians(-10);
	private double warningMaxRoll = Math.toRadians(10);
	
	/**
	 * Pitch orientation. A positive pitch angle means the vehicule is pitching UP.
	 */
	public static int PITCH_UP_POSITIVE = 0;
	
	
	/**
	 * Pitch orientation. A negative pitch angle means the vehicule is pitching UP.
	 */
	public static int PITCH_UP_NEGATIVE = 1;

	
	/**
	 * The actual picth up orientation used.
	 */
	private int pitchOrientation = PITCH_UP_NEGATIVE;

	public static int ROLL_CW_POSITIVE = 0;
	public static int ROLL_CW_NEGATIVE = 1;
	
	private int rollOrientation = ROLL_CW_POSITIVE;
			
	private double pitch = Math.toRadians(0);
	private double roll = Math.toRadians(0);
	private Text txtPitch;
	private Text txtRoll;
	private Button btnSettings;
	
	public AttitudeIndicatorComposite(Composite parent, int style) 
	{
		this(parent, style, PITCH_UP_NEGATIVE, ROLL_CW_POSITIVE, 300, 300);						
	}
	
	public AttitudeIndicatorComposite(Composite parent, int style, int indicatorWidth, int indicatorHeight) 
	{
		this(parent, style, PITCH_UP_NEGATIVE, ROLL_CW_POSITIVE, indicatorWidth, indicatorHeight);						
	}
	
	/**
	 * Creates an AttitudeIndicatorComposite, specifying the direction of pitch and roll.
	 * @param parent The parent Composite.
	 * @param style The SWT style to use.
	 * @param pitchOrientation The pitch orientation : PITCH_UP_POSITIVE or PITCH_UP_NEGATIVE.
	 * @param rollOrientation The roll orientation : ROLL_CW_POSITIVE or ROLL_CW_NEGATIVE.
	 * @param indicatorWidth The width, in pixel, of the indicator part of the composite.
	 * @param indicatorHeight The height, in pixel, of the indicator part of the composite.
	 */
	public AttitudeIndicatorComposite(Composite parent, int style, int pitchOrientation, int rollOrientation, int indicatorWidth, int indicatorHeight) 
	{
		super(parent, style);	
		
		this.pitchOrientation = pitchOrientation;
		this.rollOrientation = rollOrientation;
		this.width = indicatorWidth;
		this.height = indicatorHeight;
					
		this.setLayout(new GridLayout(2, true));
		
		Composite top = new Composite(this, SWT.NONE);
		top.setLayout(new GridLayout(1, false));
		top.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
				
		label = new Label(top, SWT.NONE);
		GridData gd_label = new GridData(SWT.CENTER, SWT.TOP, true, true, 1, 1);
		gd_label.minimumHeight = indicatorHeight;
		gd_label.heightHint = indicatorHeight;
		gd_label.widthHint = indicatorWidth;
		gd_label.minimumWidth = indicatorWidth;
		label.setLayoutData(gd_label);
		new Label(this, SWT.NONE);
		
		Composite bottom = new Composite(this, SWT.NONE);
		bottom.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1));
		bottom.setLayout(new GridLayout(5, false));
		
		Label lblPitchdeg = new Label(bottom, SWT.NONE);
		lblPitchdeg.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPitchdeg.setText("Pitch (" + DEGREE_STRING + "):");
		
		txtPitch = new Text(bottom, SWT.BORDER);
		txtPitch.setEditable(false);
		GridData gd_txtPitch = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtPitch.widthHint = 70;
		gd_txtPitch.minimumWidth = 70;
		txtPitch.setLayoutData(gd_txtPitch);
		
		Label lblRolldeg = new Label(bottom, SWT.NONE);
		lblRolldeg.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRolldeg.setText("Roll (" + DEGREE_STRING + "):");
		
		txtRoll = new Text(bottom, SWT.BORDER);
		txtRoll.setEditable(false);
		GridData gd_txtRoll = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtRoll.widthHint = 70;
		gd_txtRoll.minimumWidth = 70;
		txtRoll.setLayoutData(gd_txtRoll);
		
		btnSettings = new Button(bottom, SWT.NONE);
		btnSettings.setText("Settings...");
		btnSettings.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{	
				final Shell shell = Display.getCurrent().getActiveShell();
				Dialog dialog = new Dialog(shell) 
				{
					AttitudeIndicatorSettingComposite settings = null;
					
					@Override
					protected Control createDialogArea(Composite parent) 
					{
						Composite area = (Composite) super.createDialogArea(parent);
						
						settings = new AttitudeIndicatorSettingComposite(area, SWT.NONE, AttitudeIndicatorComposite.this);						
						
						return area;
					}
					
					@Override
					protected void configureShell(Shell newShell) 
					{					
						super.configureShell(newShell);

						newShell.setText("Attitude Indicator Settings.");
					}
					
					@Override
					protected void buttonPressed(int buttonId) 
					{
						if(buttonId == Dialog.OK)
						{
							settings.applySettings();
						}
						
						super.buttonPressed(buttonId);
					}
				};		
				
				dialog.open();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) 
			{			
			}
		});
		
		
		addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{				
				// Unregister pose adapter.
				if(apogySystemApiAdapter != null)
				{
					apogySystemApiAdapter.eAdapters().remove(getPoseAdapter());
				}
			}
		});
	}
	
	@Override
	protected void apogySystemApiAdapterChanged(ApogySystemApiAdapter oldApogySystemApiAdapter, ApogySystemApiAdapter newApogySystemApiAdapter) 
	{		
		if(oldApogySystemApiAdapter != null) 
		{
			oldApogySystemApiAdapter.eAdapters().remove(getPoseAdapter());
		}
		
		if(newApogySystemApiAdapter != null) 
		{
			newApogySystemApiAdapter.eAdapters().add(getPoseAdapter());
		}
		
		apogySystemApiAdapter = newApogySystemApiAdapter;
		
		forceUpdate();
	}
	
	/**
	 * Sets the current pitch to be displayed
	 * @param pitch The pitch, in radians.
	 */
	public void setPitch(double pitch)
	{
		this.pitch = pitch;				
	}
	
	/**
	 * Sets the current roll to be displayed
	 * @param roll The roll, in radians.
	 */
	public void setRoll(double roll)
	{
		this.roll = roll;
		
	}
	
	/**
	 * Sets both pitch and roll to be displayed
	 * @param pitch The pitch, in radians.
	 * @param roll The roll, in radians.
	 */
	public void setAttitude(double pitch, double roll)
	{
		this.pitch = pitch;
		this.roll = roll;		
		forceUpdate();
	}
	

	/**
	 * Set Pitch Alarm Limits.
	 * @param alarmMinPitch The Pitch Alarm Minimum value, in radians.
	 * @param alarmMaxPitch The Pitch Alarm Maximum value, in radians.
	 */
	public void setPitchAlarmValues(double alarmMinPitch, double alarmMaxPitch)
	{
		this.alarmMinPitch = alarmMinPitch;
		this.alarmMaxPitch = alarmMaxPitch;		
		forceUpdate();
	}
	
	/**
	 * Gets the current Pitch Alarm Minimum value.
	 * @return The angle, in radians.
	 */
	public double getPitchAlarmMinLimit()
	{
		return this.alarmMinPitch;
	}

	/**
	 * Gets the current Pitch Alarm Maximum value.
	 * @return The angle, in radians.
	 */
	public double getPitchAlarmMaxLimit()
	{
		return this.alarmMaxPitch;
	}
	
	/**
	 * Set Pitch Warning Limits.
	 * @param warningMinPitch The Pitch Warning Minimum value, in radians.
	 * @param warningMaxPitch The Pitch Warning Maximum value, in radians.
	 */
	public void setPitchWarningValues(double warningMinPitch, double warningMaxPitch)
	{
		this.warningMinPitch = warningMinPitch;
		this.warningMaxPitch = warningMaxPitch;		
		forceUpdate();
	}
	
	/**
	 * Gets the current Pitch Warning Minimum value.
	 * @return The angle, in radians.
	 */
	public double getPitchWarningMinLimit()
	{
		return this.warningMinPitch;
	}
	
	/**
	 * Gets the current Pitch Warning Maximum value.
	 * @return The angle, in radians.
	 */
	public double getPitchWarningMaxLimit()
	{
		return this.warningMaxPitch;
	}
	
	/**
	 * Set Roll Alarm Limits.
	 * @param alarmMinRoll The Roll Alarm Minimum value, in radians.
	 * @param alarmMaxRoll The Roll Alarm Maximum value, in radians.
	 */
	public void setRollAlarmValues(double alarmMinRoll, double alarmMaxRoll)
	{
		this.alarmMinRoll = alarmMinRoll;
		this.alarmMaxRoll = alarmMaxRoll;
		forceUpdate();
	}
	
	/**
	 * Gets the current Roll Warning Minimum value.
	 * @return The angle, in radians.
	 */
	public double getRollAlarmMinLimit()
	{
		return this.alarmMinRoll;
	}
	
	/**
	 * Gets the current Roll Warning Maximum value.
	 * @return The angle, in radians.
	 */
	public double getRollAlarmMaxLimit()
	{
		return this.alarmMaxRoll;
	}
	
	/**
	 * Set Roll Warning Limits.
	 * @param warningMinRoll The Roll Warning Minimum value, in radians.
	 * @param warningMaxRoll The Roll Warning Maximum value, in radians.
	 */
	public void setRollWarningValues(double warningMinRoll, double warningMaxRoll)
	{
		this.warningMinRoll = warningMinRoll;
		this.warningMaxRoll = warningMaxRoll;
		forceUpdate();
	}
	
	/**
	 * Gets the current Roll Warning Minimum value.
	 * @return The angle, in radians.
	 */
	public double getRollWarningMinLimit()
	{
		return this.warningMinRoll;
	}
	
	/**
	 * Gets the current Roll Warning Maximum value.
	 * @return The angle, in radians.
	 */
	public double getRollWarningMaxLimit()
	{
		return this.warningMaxRoll;
	}	
	
	private void forceUpdate()
	{
		if(!busy && !isDisposed()) 
		{
			busy = true;
									
			Job job = new Job("Attitude Indicator Update")
			{
				@Override
				protected IStatus run(IProgressMonitor monitor) 
				{
					// Gets the update image.
					final Image image = getImage();
					
					// Update the displayed image in the UI thread.
					Display.getDefault().asyncExec(new Runnable() 
					{			
						@Override
						public void run() 
						{
							if(!label.isDisposed())
							{
								label.setImage(image);
								
								txtRoll.setText(formatAngle(Math.toDegrees(roll), 4));
								txtRoll.setBackground(getRollColor(roll));
								
								txtPitch.setText(formatAngle(Math.toDegrees(pitch), 4));
								txtPitch.setBackground(getPitchColor(pitch));
								
								
							}
							busy = false;
						}
					});	
					
					return Status.OK_STATUS;
				}
			};
			job.schedule();			
		}				
	}
				
	protected void updatePose()
	{
		Matrix4x4 pose = ApogyCommonMathFacade.INSTANCE.createIdentityMatrix4x4();
		if(getApogySystemApiAdapter() != null)
		{
			pose = getApogySystemApiAdapter().getPoseTransform();
			if(pose == null) pose = ApogyCommonMathFacade.INSTANCE.createIdentityMatrix4x4();
		}
		
		// Computes the pitch and roll
		Vector3d xAxis = new Vector3d(1,0,0);
		Vector3d yAxis = new Vector3d(0,1,0);
		Vector3d zAxis = new Vector3d(0,0,1);
		
		// Extract the rotation matrix.
		Matrix4d poseMatrix = pose.asMatrix4d();
		Matrix3d rotationMatrix = new Matrix3d();
		poseMatrix.get(rotationMatrix);
		
		// Gets the new orientation of the x and Y axis of the rover.
		rotationMatrix.transform(xAxis);
		rotationMatrix.transform(yAxis);
		
		// Computes the angle between the Z axis and the rotated X.
		double pitch = -(Math.PI / 2.0 - zAxis.angle(xAxis));
		
		// Computes the angle between the Z axis and the rotated Y.	
		double roll =  Math.PI / 2.0 - zAxis.angle(yAxis);
		
		setAttitude(pitch, roll);
		
	}
	
	protected Adapter getPoseAdapter()
	{
		if(poseAdapter == null)
		{
			poseAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{					
					if(msg.getNotifier() instanceof PoseProvider)
					{
						int featureID = msg.getFeatureID(PoseProvider.class);
						switch (featureID) 
						{
							case ApogyCorePackage.POSE_PROVIDER__POSE_TRANSFORM:
								if(msg.getNewValue() instanceof Matrix4x4)
								{																
									updatePose();
								}																								
							break;

							default:
							break;
						}												
					}					
				}
			};
		}
		
		return poseAdapter;
	}
	
	private int getHorizontalCenter()
	{
		return Math.round(width / 2.0f);		
	}
	
	private int getVerticalCenter()
	{
		return Math.round(height / 2.0f);
	}
	
	private int getPitchLineIntervalPixel()
	{
		return Math.round(getRadius() / 3.7f);
	}
	
	private int getRadius()
	{
		return Math.round((width / 2.0f) * 0.6f);	
	}
	
	private Image getImage()
	{
		int radius = getRadius();	
		int vehiculeWidth = radius;
		
		Image horizonImg = new Image(getDisplay(), ImageUtils.convertToSWT(drawHorizon(width, height, pitch , roll)));		
		Image pitchImg = new Image(getDisplay(), ImageUtils.convertToSWT(drawPitchOverlay(width, height, radius, pitch , roll)));		
		Image vehiculeImg = new Image(getDisplay(), ImageUtils.convertToSWT(drawVehicule( width, height, vehiculeWidth, pitch , roll)));		
		Image foregroundImg = new Image(getDisplay(), ImageUtils.convertToSWT(drawForeground( width, height, radius, pitch, roll)));
				
		Image img1 = ImageUtils.applyOverlay(horizonImg, pitchImg);
		horizonImg.dispose();
		pitchImg.dispose();
		
		Image img2 = ImageUtils.applyOverlay(img1, vehiculeImg);
		img1.dispose();
		vehiculeImg.dispose();
		
		Image img3 = ImageUtils.applyOverlay(img2, foregroundImg);
		img2.dispose();
		foregroundImg.dispose();
		
		if(currentImage != null && !currentImage.isDisposed()) currentImage.dispose();
		currentImage = img3;
				
		return currentImage;
	}
	
	private BufferedImage drawVehicule(int width, int height, int vehiculeWidth, double pitch, double roll)
	{
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	    Graphics2D graphics = img.createGraphics();
	    graphics.setTransform(new AffineTransform());
	    graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
	    int x1 = getHorizontalCenter() - Math.round(vehiculeWidth / 2.0f);
	    int x2 = getHorizontalCenter() + Math.round(vehiculeWidth / 2.0f);
	    
		graphics.setTransform( new AffineTransform());
		graphics.setColor(Color.BLACK);	
		graphics.setStroke(new BasicStroke(3));
		graphics.drawLine(x1, getVerticalCenter() + 1, x2, getVerticalCenter() + 1);
		graphics.drawLine(getHorizontalCenter(), getVerticalCenter() - 10, getHorizontalCenter(), getVerticalCenter() + 10);
		graphics.dispose();
		
		return img;
	}
	
	private BufferedImage drawHorizon(int width, int height, double pitch, double roll)
	{
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	    Graphics2D graphics = img.createGraphics();
	    graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		graphics.setTransform(new AffineTransform());
		
		// Fills the display with sky.		
		graphics.setColor(sky);		
		graphics.fillRect(0,0, width, height);
				
		AffineTransform r = new AffineTransform();			
		r.translate(getHorizontalCenter(), getVerticalCenter());
		
		if(rollOrientation == ROLL_CW_POSITIVE)
		{
			r.rotate(-roll);
		}
		else
		{
			r.rotate(roll);
		}
		
		AffineTransform t = new AffineTransform();		
		t.translate(-width, 0);
		
		r.concatenate(t);
		
		graphics.setTransform(r);
		
		graphics.setColor(getHorizonColor(pitch, roll));
		
		int pitchInducedDelta = (int) Math.round((pitch / Math.toRadians(pitchAngleIncrement)) * getPitchLineIntervalPixel());
		
		if(pitchOrientation == PITCH_UP_POSITIVE)
		{
			graphics.fillRect(0, pitchInducedDelta, 4 * width,  2 * height);	
		}
		else
		{
			graphics.fillRect(0, -pitchInducedDelta, 4 * width,  2 * height);	
		}
					
		graphics.dispose();
		
		return img;
	}
	
	private BufferedImage drawPitchOverlay(int width, int height, int radius, double pitch, double roll)
	{
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	    Graphics2D graphics = img.createGraphics();
	    graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		DecimalFormat decimalFormat =  new DecimalFormat("###0");
						
		int textWidth = graphics.getFontMetrics().stringWidth("-10");
		int textHeight = graphics.getFontMetrics().getHeight();
		int lineWidth = Math.round(radius / 2.0f);
		int textLineHorizontalSpacing = 10;		
		int numberOfPitchLines = 13;
		
		int overlayWidth = textWidth + textLineHorizontalSpacing + lineWidth + textLineHorizontalSpacing + textWidth;
		int overlayHeight = (numberOfPitchLines - 1) * getPitchLineIntervalPixel() + textHeight + 1;
		
		AffineTransform r = new AffineTransform();			
		r.translate(getHorizontalCenter() - (int) Math.round(overlayWidth / 2.0f), getVerticalCenter() - (int) Math.round(overlayHeight / 2.0f));
		
		if(rollOrientation == ROLL_CW_POSITIVE)
		{
			r.rotate(-roll, (int) Math.round(overlayWidth / 2.0f), (int) Math.round(overlayHeight / 2.0f));
		}
		else
		{
			r.rotate(roll, (int) Math.round(overlayWidth / 2.0f), (int) Math.round(overlayHeight / 2.0f));
		}
			
		graphics.setTransform(r);		
		graphics.setColor(Color.GREEN);
		
		double pitchAngle = 0;
		
		if(pitchOrientation == PITCH_UP_POSITIVE)
		{
			pitchAngle = -60;
		}
		else
		{
			pitchAngle = 60;
		}
		
		int vPosition = textHeight;
		int zeroPitchPosition = 0;
		
		for(int pitchLine = 0; pitchLine < numberOfPitchLines; pitchLine++)
		{
			String pitchAngleString = decimalFormat.format(pitchAngle);
			if(pitchAngle >= 0) pitchAngleString = "+" + pitchAngleString;
			
			graphics.drawString(pitchAngleString, 0, vPosition);
			
			graphics.setStroke(new BasicStroke(2));
			int lineY = vPosition - (int) Math.floor(textHeight / 2.0) + 2;
			graphics.drawLine(textWidth + textLineHorizontalSpacing, lineY, textWidth + textLineHorizontalSpacing + lineWidth, lineY);			
			
			graphics.drawString(pitchAngleString, textWidth + textLineHorizontalSpacing + lineWidth + textLineHorizontalSpacing, vPosition);
			
			if(pitchAngle == 0.0) zeroPitchPosition = lineY;
			
			if(pitchOrientation == PITCH_UP_POSITIVE)
			{
				pitchAngle += pitchAngleIncrement;
			}
			else
			{
				pitchAngle -= pitchAngleIncrement;
			}
									
			vPosition += getPitchLineIntervalPixel();
		}
		
		graphics.setStroke(new BasicStroke(2));
		graphics.setColor(Color.WHITE);
		int xRightArrow = (int) Math.round(overlayWidth / 2.0) + radius;
		int xLeftArrow = (int) Math.round(overlayWidth / 2.0) - radius;
		
		graphics.drawLine(xRightArrow, zeroPitchPosition, xRightArrow - 5, zeroPitchPosition - 5);
		graphics.drawLine(xRightArrow, zeroPitchPosition, xRightArrow - 5,  zeroPitchPosition + 5);
		
		graphics.drawLine(xLeftArrow, zeroPitchPosition, xLeftArrow + 5, zeroPitchPosition - 5);
		graphics.drawLine(xLeftArrow, zeroPitchPosition, xLeftArrow + 5,  zeroPitchPosition + 5);		
		
		return img;
	}
 
	private BufferedImage drawForeground(int width, int height, int radius, double pitch, double roll)
	{
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = img.createGraphics();
	    graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		int textHeight = graphics.getFontMetrics().getHeight();
		
		DecimalFormat decimalFormat =  new DecimalFormat("###0");
									    	     	      
	    graphics.setColor(Color.BLACK);	
	    graphics.fillRect(0, 0, width, height);
	      
	    graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.DST_OUT, 1f));
	      
	    graphics.setColor(Color.blue);	
	      
	    int x = (int) Math.round(((width / 2.0) - radius));
	    int y = (int) Math.round(((height / 2.0) - radius));
	      	       
	    graphics.fillOval(x, y, radius * 2, radius * 2);
	    
	    // Adds the roll graduation mark.
	    graphics.setColor(Color.WHITE);	
	    graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC, 1f));
	    
	    double rollAngleIncrement = ROLL_INCREMENT;
	   
	    double rollAngle = 0;
	    if(rollOrientation == ROLL_CW_POSITIVE)
		{
	    	rollAngle = MAX_ROLL;
		}
	    else
	    {
	    	rollAngle = -MAX_ROLL;
	    }
	   
	    int markLenght = 10; 
	    
	    int iterations = (int) Math.round(2.0 * MAX_ROLL / rollAngleIncrement) + 1;
	    for(int i = 0; i < iterations; i++)
	    {	    	
		    int x1 = (int) Math.round(radius * Math.cos(Math.toRadians(rollAngle)));
		    int y1 = (int) Math.round(radius * Math.sin(Math.toRadians(rollAngle)));
		    
		    int x2 = (int) Math.round((radius + markLenght) * Math.cos(Math.toRadians(rollAngle)));
		    int y2 = (int) Math.round((radius + markLenght) * Math.sin(Math.toRadians(rollAngle)));
	
		    AffineTransform t = new AffineTransform();	
		    t.translate((int) Math.round(width / 2.0f), (int) Math.round(height / 2.0f) );
		    graphics.setTransform(t);
		    graphics.setStroke(new BasicStroke(2));
		    graphics.drawLine(x1,y1,x2,y2);
		    		    		    
		    if(rollOrientation == ROLL_CW_POSITIVE)
		    {
		    	String rollString = decimalFormat.format(-rollAngle);
			    graphics.drawString(rollString, x2 + 2, y2 + Math.round(textHeight / 2.0f) - 2);
		    	rollAngle -= rollAngleIncrement;
		    }
		    else
		    {
		    	String rollString = decimalFormat.format(rollAngle);
			    graphics.drawString(rollString, x2 + 2, y2 + Math.round(textHeight / 2.0f) - 2);
		    	rollAngle += rollAngleIncrement;
		    }
	    }
	    
	    if(rollOrientation == ROLL_CW_POSITIVE)
		{
	    	rollAngle = MAX_ROLL - 180;
		}
	    else
	    {
	    	rollAngle = -MAX_ROLL + 180;
	    }	    
	    	    	    
	    for(int i = 0; i < iterations; i++)
	    {
		    int x1 = (int) Math.round(radius * Math.cos(Math.toRadians(rollAngle)));
		    int y1 = (int) Math.round(radius * Math.sin(Math.toRadians(rollAngle)));
		    
		    int x2 = (int) Math.round((radius + markLenght) * Math.cos(Math.toRadians(rollAngle)));
		    int y2 = (int) Math.round((radius + markLenght) * Math.sin(Math.toRadians(rollAngle)));
	
		    AffineTransform t = new AffineTransform();	
		    t.translate((int) Math.round(width / 2.0f), (int) Math.round(height / 2.0f) );
		    graphics.setTransform(t);
		    graphics.setStroke(new BasicStroke(2));
		    graphics.drawLine(x1,y1,x2,y2);
		    
		    if(rollOrientation == ROLL_CW_POSITIVE)
			{
		    	String rollString = decimalFormat.format(-(rollAngle + 180));		    
		    	int textWidth = graphics.getFontMetrics().stringWidth(rollString);		    
		    	graphics.drawString(rollString, x2 - (textWidth + 2), y2 + Math.round(textHeight / 2.0f) - 2);
		    	rollAngle -= rollAngleIncrement;
			}
		    else
		    {
		    	String rollString = decimalFormat.format(rollAngle - 180);		    
			    int textWidth = graphics.getFontMetrics().stringWidth(rollString);		    
			    graphics.drawString(rollString, x2 - (textWidth + 2), y2 + Math.round(textHeight / 2.0f) - 2);
			    rollAngle += rollAngleIncrement;
		    }		    		   
	    }
	    
	    // Adds Pitch and Roll values.
//	    AffineTransform t = new AffineTransform();		    
//	    graphics.setTransform(t);
//
//	    int yValues = height - 100;	    	   
//	    
//	    String rollValue = "Roll : " + formatAngle(Math.toDegrees(roll), 4);	    	   	    
//	    String pitchValue = "Pitch : " + formatAngle(Math.toDegrees(pitch), 4);
//	    
//	    int rollWidth = graphics.getFontMetrics().stringWidth(rollValue);	    
//	    
//	    int xRoll = getHorizontalCenter() - getRadius() - rollWidth;
//	    int xPitch = getHorizontalCenter() + getRadius();
//	    
//	    graphics.drawString(rollValue, xRoll, yValues);
//	    graphics.drawString(pitchValue, xPitch, yValues);
	    	    
	    graphics.dispose();
	    		
	    return img;
	}
	
	private Color getHorizonColor(double pitch, double roll)
	{
		Ranges range = Ranges.NOMINAL;
		
		if((pitch <= alarmMinPitch) || (pitch >= alarmMaxPitch))
		{
			range = Ranges.ALARM;
		}
		else if((roll <= alarmMinRoll) || (roll >= alarmMaxRoll))
		{
			range = Ranges.ALARM;
		}
		else if((pitch <= warningMinPitch) || (pitch >= warningMaxPitch))
		{
			range = Ranges.WARNING;
		}
		else if((roll <= warningMinRoll) || (roll >= warningMaxRoll))
		{
			range = Ranges.WARNING;
		}		
		
		if(range.getValue() == Ranges.ALARM_VALUE || range.getValue() == Ranges.WARNING_VALUE)
		{
			org.eclipse.swt.graphics.Color rangeColor = ApogyCommonEMFUIFacade.INSTANCE.getColorForRange(range);
			
			return new Color(rangeColor.getRed(), rangeColor.getGreen(), rangeColor.getBlue()); 
		}
		else
		{
			return ground;
		}				
	}
	
	private org.eclipse.swt.graphics.Color getPitchColor(double pitch)
	{
		Ranges range = Ranges.NOMINAL;
		
		if((pitch <= alarmMinPitch) || (pitch >= alarmMaxPitch))
		{
			range = Ranges.ALARM;
		}
		else if((pitch <= warningMinPitch) || (pitch >= warningMaxPitch))
		{
			range = Ranges.WARNING;
		}
		
		org.eclipse.swt.graphics.Color rangeColor = ApogyCommonEMFUIFacade.INSTANCE.getColorForRange(range);		
		
		if(rangeColor != null)
		{
			return new org.eclipse.swt.graphics.Color(getDisplay(), rangeColor.getRed(), rangeColor.getGreen(), rangeColor.getBlue());  			
		}
		else
		{
			return new org.eclipse.swt.graphics.Color(getDisplay(), 255,255,255); 
		}
	}
	
	private org.eclipse.swt.graphics.Color getRollColor(double roll)
	{
		Ranges range = Ranges.NOMINAL;
		
		if((roll <= alarmMinRoll) || (roll >= alarmMaxRoll))
		{
			range = Ranges.ALARM;
		}
		else if((roll <= warningMinRoll) || (roll >= warningMaxRoll))
		{
			range = Ranges.WARNING;
		}	
		
		org.eclipse.swt.graphics.Color rangeColor = ApogyCommonEMFUIFacade.INSTANCE.getColorForRange(range);		
		
		if(rangeColor != null)
		{
			return new org.eclipse.swt.graphics.Color(getDisplay(), rangeColor.getRed(), rangeColor.getGreen(), rangeColor.getBlue());  			
		}
		else
		{
			return new org.eclipse.swt.graphics.Color(getDisplay(), 255,255,255); 
		}

	}
	
	private String formatAngle(double value, int lenght)
	{
		DecimalFormat angleValueFormat = new DecimalFormat("0.0"); 
		String s = angleValueFormat.format(value);
		if(value >= 0)
		{
			s = "+" + s;
		}
		
		while(s.length() < lenght)
		{
			s = " " + s;
		}
		
		return s;
	}

}

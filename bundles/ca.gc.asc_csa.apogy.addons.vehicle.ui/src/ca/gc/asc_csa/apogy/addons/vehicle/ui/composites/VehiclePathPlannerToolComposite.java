/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.vehicle.ui.composites;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage;
import ca.gc.asc_csa.apogy.addons.vehicle.VehiclePathPlannerTool;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.Variable;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.VariablesListComposite;

public class VehiclePathPlannerToolComposite extends Composite 
{
	private VehiclePathPlannerTool vehiclePathPlannerTool;

	private VariablesListComposite variablesListComposite;	
	
	public VehiclePathPlannerToolComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Group grpVariable = new Group(this, SWT.NONE);
		grpVariable.setLayout(new GridLayout(1, false));
		grpVariable.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpVariable.setText("Variable");
		
		variablesListComposite =  new VariablesListComposite(grpVariable, SWT.NONE)
		{
			@Override
			protected void newSelection(ISelection selection) 
			{								
				Variable variable = variablesListComposite.getSelectedVariable();
				
				if(ApogyCommonTransactionFacade.INSTANCE.areEditingDomainsValid(vehiclePathPlannerTool, ApogyAddonsVehiclePackage.Literals.VEHICLE_PATH_PLANNER_TOOL__VARIABLE, variable, false) == ApogyCommonTransactionFacade.EXECUTE_COMMAND_ON_OWNER_DOMAIN)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet(vehiclePathPlannerTool, ApogyAddonsVehiclePackage.Literals.VEHICLE_PATH_PLANNER_TOOL__VARIABLE, variable);
				}
				else
				{				
					vehiclePathPlannerTool.setVariable(variable);
				}				
				
				newVariableSelected(variable);
			}
		};
		
		InvocatorSession invocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();		
		if(invocatorSession != null && invocatorSession.getEnvironment() != null)
		{
			variablesListComposite.setVariablesList(invocatorSession.getEnvironment().getVariablesList());
		}			
	}

	public VehiclePathPlannerTool getVehiclePathPlannerTool() 
	{
		return vehiclePathPlannerTool;
	}

	public void setVehiclePathPlannerTool(VehiclePathPlannerTool vehiclePathPlannerTool) 
	{				
		this.vehiclePathPlannerTool = vehiclePathPlannerTool;
		
		if(variablesListComposite != null && !variablesListComposite.isDisposed())
		{
			// TODO : Select the variable			
		}		
	}	
	
	/**
	 * Method called when a variable is selected.
	 * @param variable The variable just selected.
	 */
	protected void newVariableSelected(Variable variable)
	{		
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.vehicle.ui.composites;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage;
import ca.gc.asc_csa.apogy.addons.vehicle.PathPlannerTool;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.SurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.DEMListComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.MapsListComposite;

public class PathPlannerToolComposite extends Composite 
{
	private SurfaceWorksite surfaceWorksite;
	private PathPlannerTool pathPlannerTool;
	private DataBindingContext m_bindingContext;	
		
	private Button btnAutoPathPlanEnabled;
	private Button btnPlanPath;
	
	private Group grpMesh;
	private MapsListComposite mapsListComposite;
	private DEMListComposite demListComposite;
	
	public PathPlannerToolComposite(Composite parent, int style) {
		
		super(parent, style);
		setLayout(new GridLayout(1, false));
			
		// Mesh Selection
		Group grpPathPlanningSettings = new Group(this, SWT.BORDER);
		grpPathPlanningSettings.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		grpPathPlanningSettings.setText("Path Planning");
		grpPathPlanningSettings.setLayout(new GridLayout(2, false));
		
		Label lblNewLabel = new Label(grpPathPlanningSettings, SWT.NONE);
		lblNewLabel.setText("Enable Auto Re-Plan:");
		
		btnAutoPathPlanEnabled = new Button(grpPathPlanningSettings, SWT.CHECK);
		btnAutoPathPlanEnabled.setToolTipText("Whether or not automatic path planning is enabled. When enabled, changing the destination will automatically start a replan of the path");
				
		btnPlanPath = new Button(grpPathPlanningSettings, SWT.NONE);
		btnPlanPath.setText("Pan Path");
		btnPlanPath.setToolTipText("Initiate an update of the path.");								
		btnPlanPath.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				IRunnableWithProgress runnable = new IRunnableWithProgress() {
					
					@Override
					public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException 
					{					
						if(getPathPlannerTool()  != null)
						{
							getPathPlannerTool().getPathPlanner().setProgressMonitor(monitor);
							getPathPlannerTool().planPath();
						}
					}
				};
				
				try 
				{
					new ProgressMonitorDialog(getShell()).run(true, true, runnable);
				} 
				catch (Throwable t) 
				{
					t.printStackTrace();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {						
			}
		});
		
		// Mesh Selection
		grpMesh = new Group(this, SWT.BORDER);
		grpMesh.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		grpMesh.setText("Mesh");
		grpMesh.setLayout(new GridLayout(2, false));
		
		Label lblMapsLabel = new Label(grpMesh, SWT.NONE);
		lblMapsLabel.setText("Maps");
		
		Label lblMeshesLabel = new Label(grpMesh, SWT.NONE);
		lblMeshesLabel.setText("Meshes");
		
		mapsListComposite = new MapsListComposite(grpMesh, SWT.BORDER, false)
		{
			protected void newMapSelected(Map map)
			{	
				demListComposite.setMap(map);				
			}
		};
		mapsListComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true));		
				
		demListComposite = new DEMListComposite(grpMesh, SWT.BORDER, false)
		{
			protected void newCartesianTriangularMeshMapLayerSelected(CartesianTriangularMeshMapLayer cartesianTriangularMeshMapLayer)
			{
				if(getPathPlannerTool() != null)
				{
					if(ApogyCommonTransactionFacade.INSTANCE.areEditingDomainsValid(getPathPlannerTool(), ApogyAddonsVehiclePackage.Literals.PATH_PLANNER_TOOL__MESH_LAYER, cartesianTriangularMeshMapLayer, false) == ApogyCommonTransactionFacade.EXECUTE_COMMAND_ON_OWNER_DOMAIN)
					{
						ApogyCommonTransactionFacade.INSTANCE.basicSet(getPathPlannerTool(), ApogyAddonsVehiclePackage.Literals.PATH_PLANNER_TOOL__MESH_LAYER, cartesianTriangularMeshMapLayer);
					}
					else
					{
						getPathPlannerTool().setMeshLayer(cartesianTriangularMeshMapLayer);
					}
				}
				
				newMeshSelected(cartesianTriangularMeshMapLayer);
			}			
		};
		demListComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true));
		demListComposite.setSelectedCartesianTriangularMeshMapLayers(null);
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public PathPlannerTool getPathPlannerTool() {
		return pathPlannerTool;
	}

	public void setPathPlannerTool(PathPlannerTool pathPlannerTool) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.pathPlannerTool = pathPlannerTool;
		
		if(pathPlannerTool != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}	
	
	public SurfaceWorksite getSurfaceWorksite() 
	{
		return surfaceWorksite;
	}

	public void setSurfaceWorksite(SurfaceWorksite newSurfaceWorksite) 
	{
		this.surfaceWorksite = newSurfaceWorksite;
		
		if(newSurfaceWorksite != null)
		{
			mapsListComposite.setMapsList(newSurfaceWorksite.getMapsList());
		}
		else
		{
			mapsListComposite.setMapsList(null);
		}	
	}
	
	protected void newMeshSelected(CartesianTriangularMeshMapLayer layer)
	{
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
	
		// Gets the Map
		if(pathPlannerTool.getMeshLayer() != null)
		{
			Map map = pathPlannerTool.getMeshLayer().getMap();
			mapsListComposite.setSelectedMap(map);
		}
		
		// Selects the mesh
		demListComposite.setSelectedCartesianTriangularMeshMapLayers(pathPlannerTool.getMeshLayer());		
				
		
		// TODO Does not compile !!!!!!
//		IEMFEditValueProperty property = EMFEditProperties.value(ApogyCommonEmfTransactionFacade.INSTANCE.getTransactionalEditingDomain(pathPlannerTool), 
//											ApogyAddonsVehiclePackage.Literals.PATH_PLANNER_TOOL__AUTO_PATH_PLAN_ENABLED);
//		
//		????
//		
//		// AutoPathPlanEnabled
//		IObservableValue<Boolean> observeAutoPathPlanEnabled =  EMFEditProperties.value(ApogyCommonEmfTransactionFacade.INSTANCE.getTransactionalEditingDomain(pathPlannerTool), 
//																ApogyAddonsVehiclePackage.Literals.PATH_PLANNER_TOOL__AUTO_PATH_PLAN_ENABLED).observe(pathPlannerTool);
//	
//		IObservableValue<Boolean> observeBtnAutoPathPlanEnabled = WidgetProperties.selection().observe(btnAutoPathPlanEnabled);
//		
//		bindingContext.bindValue(observeBtnAutoPathPlanEnabled,
//								 observeAutoPathPlanEnabled, 
//								 new UpdateValueStrategy(),	
//								 new UpdateValueStrategy());
//		
		return bindingContext;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.vehicle.ui;

public class ApogyAddonsVehicleUIRCPConstants 
{
	public static final String PERSISTED_STATE__VEHICLE_POSE_PART__DISTANCE = "distance";
	public static final String PERSISTED_STATE__VEHICLE_POSE_PART__VARIABLE_FEATURE_REFERENCE_ID = "variableFeatureReference";
	
	public static final String PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MIN_PITCH = "alarmMinPitch";
	public static final String PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MAX_PITCH = "alarmMaxPitch";
	
	public static final String PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MIN_PITCH = "warningMinPitch";
	public static final String PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MAX_PITCH = "warningMaxPitch";
	

	public static final String PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MIN_ROLL = "alarmMinRoll";
	public static final String PERSISTED_STATE__VEHICLE_POSE_PART__ALARM_MAX_ROLL = "alarmMaxRoll";
	
	public static final String PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MIN_ROLL = "warningMinRoll";
	public static final String PERSISTED_STATE__VEHICLE_POSE_PART__WARNING_MAX_ROLL = "warningMaxRoll";
		
	public static final String PERSISTED_STATE__FOI_RADAR_PART__MAXIMUM_RADIUS = "maxRadius";
	public static final String PERSISTED_STATE__FOI_RADAR_PART__VARIABLE_FEATURE_REFERENCE_ID = "variableFeatureReference";
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.vehicle.PathPlannerTool;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.composites.PathPlannerToolComposite;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.SurfaceWorksite;

public class PathPlannerToolWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards.PathPlannerToolWizardPage";
	
	private PathPlannerTool pathPlannerTool;
	private PathPlannerToolComposite pathPlannerToolComposite;
	
	public PathPlannerToolWizardPage(PathPlannerTool pathPlannerTool) 
	{
		super(WIZARD_PAGE_ID);
		this.pathPlannerTool = pathPlannerTool;
		
		setTitle("Path Planner Tool");
		setDescription("Configure the update beahviou and mesh used.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(1, false));
		
		pathPlannerToolComposite = new PathPlannerToolComposite(container, SWT.None)
		{
			protected void newMeshSelected(CartesianTriangularMeshMapLayer layer)
			{
				validate();
			}
		};
		pathPlannerToolComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		pathPlannerToolComposite.setPathPlannerTool(pathPlannerTool);
		
		setControl(container);
		pathPlannerToolComposite.setFocus();		
		
		if(ApogyCoreEnvironmentFacade.INSTANCE.getActiveWorksite() instanceof SurfaceWorksite)
		{
			pathPlannerToolComposite.setSurfaceWorksite((SurfaceWorksite) ApogyCoreEnvironmentFacade.INSTANCE.getActiveWorksite());	
		}		
		
		validate();
	}	
	
	protected void validate()
	{				
		setErrorMessage(null);		
		
		if(pathPlannerTool.getMeshLayer() == null)
		{
			setErrorMessage("No mesh selected !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
}

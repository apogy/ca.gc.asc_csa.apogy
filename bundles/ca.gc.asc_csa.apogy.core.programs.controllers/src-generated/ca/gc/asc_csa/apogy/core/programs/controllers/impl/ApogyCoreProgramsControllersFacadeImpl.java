package ca.gc.asc_csa.apogy.core.programs.controllers.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.io.jinput.ApogyCommonIOJInputFactory;
import ca.gc.asc_csa.apogy.common.io.jinput.EComponentQualifier;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFactory;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.ArgumentsList;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ProgramsGroup;
import ca.gc.asc_csa.apogy.core.invocator.ProgramsList;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersFacade;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersFactory;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.BindedEDataTypeArgument;
import ca.gc.asc_csa.apogy.core.programs.controllers.CenteredLinearInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerValueSource;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllersConfiguration;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllersGroup;
import ca.gc.asc_csa.apogy.core.programs.controllers.CustomInputConditioningPoint;
import ca.gc.asc_csa.apogy.core.programs.controllers.OperationCallControllerBinding;
import ca.gc.asc_csa.apogy.core.programs.controllers.ToggleValueSource;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Controller Facade</b></em>'. <!-- end-user-doc --> *
 * @generated
 */
public class ApogyCoreProgramsControllersFacadeImpl extends MinimalEObjectImpl.Container
		implements ApogyCoreProgramsControllersFacade {
	private static ApogyCoreProgramsControllersFacade instance = null;

	public static ApogyCoreProgramsControllersFacade getInstance() {
		if (instance == null) {
			instance = new ApogyCoreProgramsControllersFacadeImpl();
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	protected ApogyCoreProgramsControllersFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreProgramsControllersPackage.Literals.APOGY_CORE_PROGRAMS_CONTROLLERS_FACADE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public Object createValue(EParameter eParameter, float value) {
		Object object = null;

		if (eParameter.getEType().getInstanceClass() == double.class) {
			object = new Double(value);
		} else if (eParameter.getEType().getInstanceClass() == float.class) {
			object = new Float(value);
		} else if (eParameter.getEType().getInstanceClass() == int.class) {
			int intValue = Math.round(value);
			object = new Integer(intValue);
		} else if (eParameter.getEType().getInstanceClass() == long.class) {
			long longValue = Math.round(value);
			object = new Long(longValue);
		} else if (eParameter.getEType().getInstanceClass() == String.class) {
			object = new Float(value).toString();
		} else if (eParameter.getEType().getInstanceClass() == boolean.class) {
			if (value == 0) {
				object = new Boolean(false);
			} else {
				object = new Boolean(true);
			}
		}

		return object;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public TreeSet<CustomInputConditioningPoint> sortCustomInputConditioningPoint(
			List<CustomInputConditioningPoint> points) {
		TreeSet<CustomInputConditioningPoint> sortedSet = new TreeSet<CustomInputConditioningPoint>(
				new CustomInputConditionningPointComparator());

		sortedSet.addAll(points);

		return sortedSet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public CustomInputConditioningPoint createCustomInputConditioningPoint(double inputValue, double outputValue) {
		CustomInputConditioningPoint point = ApogyCoreProgramsControllersFactory.eINSTANCE
				.createCustomInputConditioningPoint();

		point.setInputValue(inputValue);
		point.setOutputValue(outputValue);

		return point;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void setActiveControllersConfiguration(ControllersConfiguration controllersConfiguration, Boolean active) {
		// If the controllerConfig is in a valid session
		if (active && controllersConfiguration.eContainer() instanceof ControllersGroup
				&& controllersConfiguration.eContainer().eContainer() instanceof ProgramsList) {
			ProgramsList programsList = (ProgramsList) controllersConfiguration.eContainer().eContainer();

			// Stop all controllers configurations.
			for (ProgramsGroup<? extends Program> group : programsList.getProgramsGroups()) {
				if (group instanceof ControllersGroup) {
					for (Program program : group.getPrograms()) {
						if (program instanceof ControllersConfiguration) {
							if (program != controllersConfiguration) {
								ApogyCommonTransactionFacade.INSTANCE.basicSet(program,
										ApogyCommonEMFPackage.Literals.STARTABLE__STARTED, false);
							}
						}
					}
				}
			}
		}

		ApogyCommonTransactionFacade.INSTANCE.basicSet(controllersConfiguration,
				ApogyCommonEMFPackage.Literals.STARTABLE__STARTED, active);
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public String getToggleValueSourceString(ToggleValueSource toggleValueSource) {
		String str = "";

		if (toggleValueSource.isCurrentValue()) {
			str += "Current";
		}
		if (toggleValueSource.isInitialValue()) {
			if (str != "") {
				str += " & ";
			}
			str += "Initial";
		}
		return str;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void initOperationCallControllerBindingArguments(
			OperationCallControllerBinding operationCallControllerBinding) {
		ArgumentsList argumentsList = ApogyCoreInvocatorFactory.eINSTANCE.createArgumentsList();

		/** Create an argument for each EParameter. */
		for (Iterator<EParameter> ite = operationCallControllerBinding.getEOperation().getEParameters().iterator(); ite
				.hasNext();) {
			ite.next();
			BindedEDataTypeArgument bindedArgument = ApogyCoreProgramsControllersFactory.eINSTANCE
					.createBindedEDataTypeArgument();
			initBindedEDataTypeArgument(bindedArgument);
			argumentsList.getArguments().add(bindedArgument);
		}

		/** Set the argumentsList. */
		if (ApogyCommonTransactionFacade.INSTANCE
				.getTransactionalEditingDomain(operationCallControllerBinding) != null) {
			ApogyCommonTransactionFacade.INSTANCE.basicSet(operationCallControllerBinding,
					ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__ARGUMENTS_LIST, argumentsList);
		} else {
			operationCallControllerBinding.setArgumentsList(argumentsList);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void initBindedEDataTypeArgument(BindedEDataTypeArgument bindedEDataTypeArgument) {
		/** Create a valueSource. */
		ControllerValueSource valueSource = ApogyCoreProgramsControllersFactory.eINSTANCE.createControllerValueSource();
		/** Create a componentQualifier. */
		EComponentQualifier qualifier = ApogyCommonIOJInputFactory.eINSTANCE.createEComponentQualifier();
		valueSource.setEComponentQualifier(qualifier);
		
		/** Create a conditioning. */
		CenteredLinearInputConditioning conditioning = ApogyCoreProgramsControllersFactory.eINSTANCE
				.createCenteredLinearInputConditioning();
		conditioning.setDeadBand((float) 0.1);
		valueSource.setConditioning(conditioning);
		
		/** Set the valueSource. */
		if (ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(bindedEDataTypeArgument) != null) {
			ApogyCommonTransactionFacade.INSTANCE.basicSet(bindedEDataTypeArgument,
					ApogyCoreProgramsControllersPackage.Literals.BINDED_EDATA_TYPE_ARGUMENT__VALUE_SOURCE, valueSource);
		} else {
			bindedEDataTypeArgument.setValueSource(valueSource);
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public ControllersGroup getControllersGroup() {
		ProgramsList list = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getProgramsList();
		if (list != null) {
			for (Iterator<ProgramsGroup<Program>> ite = list.getProgramsGroups().iterator(); ite.hasNext();) {
				ProgramsGroup<Program> group = ite.next();
				if((ProgramsGroup<? extends Program>)group instanceof ControllersGroup){
					return (ControllersGroup)(ProgramsGroup<? extends Program>)group;
				}
			}
			/** If no ControllersGroup is found, create one. */
			ControllersGroup group = ApogyCoreProgramsControllersFactory.eINSTANCE.createControllersGroup();
			group.setName("Controllers Group");
			ApogyCommonTransactionFacade.INSTANCE.basicAdd(list,
					ApogyCoreInvocatorPackage.Literals.PROGRAMS_LIST__PROGRAMS_GROUPS, group);

			return group;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCoreProgramsControllersPackage.APOGY_CORE_PROGRAMS_CONTROLLERS_FACADE___CREATE_VALUE__EPARAMETER_FLOAT:
				return createValue((EParameter)arguments.get(0), (Float)arguments.get(1));
			case ApogyCoreProgramsControllersPackage.APOGY_CORE_PROGRAMS_CONTROLLERS_FACADE___SORT_CUSTOM_INPUT_CONDITIONING_POINT__LIST:
				return sortCustomInputConditioningPoint((List<CustomInputConditioningPoint>)arguments.get(0));
			case ApogyCoreProgramsControllersPackage.APOGY_CORE_PROGRAMS_CONTROLLERS_FACADE___CREATE_CUSTOM_INPUT_CONDITIONING_POINT__DOUBLE_DOUBLE:
				return createCustomInputConditioningPoint((Double)arguments.get(0), (Double)arguments.get(1));
			case ApogyCoreProgramsControllersPackage.APOGY_CORE_PROGRAMS_CONTROLLERS_FACADE___SET_ACTIVE_CONTROLLERS_CONFIGURATION__CONTROLLERSCONFIGURATION_BOOLEAN:
				setActiveControllersConfiguration((ControllersConfiguration)arguments.get(0), (Boolean)arguments.get(1));
				return null;
			case ApogyCoreProgramsControllersPackage.APOGY_CORE_PROGRAMS_CONTROLLERS_FACADE___GET_TOGGLE_VALUE_SOURCE_STRING__TOGGLEVALUESOURCE:
				return getToggleValueSourceString((ToggleValueSource)arguments.get(0));
			case ApogyCoreProgramsControllersPackage.APOGY_CORE_PROGRAMS_CONTROLLERS_FACADE___INIT_OPERATION_CALL_CONTROLLER_BINDING_ARGUMENTS__OPERATIONCALLCONTROLLERBINDING:
				initOperationCallControllerBindingArguments((OperationCallControllerBinding)arguments.get(0));
				return null;
			case ApogyCoreProgramsControllersPackage.APOGY_CORE_PROGRAMS_CONTROLLERS_FACADE___INIT_BINDED_EDATA_TYPE_ARGUMENT__BINDEDEDATATYPEARGUMENT:
				initBindedEDataTypeArgument((BindedEDataTypeArgument)arguments.get(0));
				return null;
			case ApogyCoreProgramsControllersPackage.APOGY_CORE_PROGRAMS_CONTROLLERS_FACADE___GET_CONTROLLERS_GROUP:
				return getControllersGroup();
		}
		return super.eInvoke(operationID, arguments);
	}

	public class CustomInputConditionningPointComparator implements Comparator<CustomInputConditioningPoint> {
		@Override
		public int compare(CustomInputConditioningPoint arg0, CustomInputConditioningPoint arg1) {
			double difference = arg0.getInputValue() - arg1.getInputValue();

			if (difference > 0) {
				return 1;
			} else if (difference < 0) {
				return -1;
			} else {
				return 0;
			}
		}
	}

} // ApogyCoreProgramsControllersFacadeImpl

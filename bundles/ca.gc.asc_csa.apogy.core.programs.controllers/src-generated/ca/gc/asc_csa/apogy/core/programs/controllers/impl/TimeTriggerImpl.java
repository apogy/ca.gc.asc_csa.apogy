package ca.gc.asc_csa.apogy.core.programs.controllers.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.io.jinput.EControllerEnvironment;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.core.programs.controllers.Activator;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.TimeTrigger;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Time
 * Trigger</b></em>'. <!-- end-user-doc --> * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.programs.controllers.impl.TimeTriggerImpl#getRefreshPeriod <em>Refresh Period</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimeTriggerImpl extends TriggerImpl implements TimeTrigger {

	/**
	 * The default value of the '{@link #getRefreshPeriod() <em>Refresh Period</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @see #getRefreshPeriod()
	 * @generated
	 * @ordered
	 */
	protected static final long REFRESH_PERIOD_EDEFAULT = 100L;

	/**
	 * The cached value of the '{@link #getRefreshPeriod() <em>Refresh Period</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @see #getRefreshPeriod()
	 * @generated
	 * @ordered
	 */
	protected long refreshPeriod = REFRESH_PERIOD_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	protected TimeTriggerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreProgramsControllersPackage.Literals.TIME_TRIGGER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	public long getRefreshPeriod() {
		return refreshPeriod;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	public void setRefreshPeriod(long newRefreshPeriod) {
		long oldRefreshPeriod = refreshPeriod;
		refreshPeriod = newRefreshPeriod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreProgramsControllersPackage.TIME_TRIGGER__REFRESH_PERIOD, oldRefreshPeriod, refreshPeriod));
	}
	
//	/**
//	 * @generated_NOT
//	 */
//	@Override
//	public void setStarted(boolean newStarted) {
//		if (isStarted() != newStarted) {
//			super.setStarted(newStarted);
//
//			if (newStarted) {
//				new Thread() {
//					@Override
//					public void run() {
//						while (isStarted()) {
//							try {
//								// Update
//								getOperationCallControllerBinding().update();
//
//								// Wait
//								if(getRefreshPeriod() >= EControllerEnvironment.MILLIS_BEFORE_NEXT_POLLING){
//									Thread.sleep(getRefreshPeriod());
//								}else{
//									Thread.sleep(EControllerEnvironment.MILLIS_BEFORE_NEXT_POLLING);
//								}
//							} catch (Throwable t) {
//								Logger.INSTANCE.log(Activator.ID, this,
//										" error waiting the specified refresh period for the timeTrigger",
//										EventSeverity.INFO);
//							}
//						}
//					}
//				}.start();
//			}
//		}
//	}
	
	@Override
	public void start() 
	{
		if(!isStarted())
		{
			setStarted(true);
			
			// Start thread for update.
			new Thread() {
				@Override
				public void run() {
					while (isStarted()) {
						try {
							// Update
							getOperationCallControllerBinding().update();

							// Wait
							if(getRefreshPeriod() >= EControllerEnvironment.MILLIS_BEFORE_NEXT_POLLING){
								Thread.sleep(getRefreshPeriod());
							}else{
								Thread.sleep(EControllerEnvironment.MILLIS_BEFORE_NEXT_POLLING);
							}
						} catch (Throwable t) {
							Logger.INSTANCE.log(Activator.ID, this,
									" error waiting the specified refresh period for the timeTrigger",
									EventSeverity.INFO);
						}
					}
				}
			}.start();
		}
	}
	
	@Override
	public void stop() {
		setStarted(true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.TIME_TRIGGER__REFRESH_PERIOD:
				return getRefreshPeriod();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.TIME_TRIGGER__REFRESH_PERIOD:
				setRefreshPeriod((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.TIME_TRIGGER__REFRESH_PERIOD:
				setRefreshPeriod(REFRESH_PERIOD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.TIME_TRIGGER__REFRESH_PERIOD:
				return refreshPeriod != REFRESH_PERIOD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (refreshPeriod: ");
		result.append(refreshPeriod);
		result.append(')');
		return result.toString();
	}

} // TimeTriggerImpl

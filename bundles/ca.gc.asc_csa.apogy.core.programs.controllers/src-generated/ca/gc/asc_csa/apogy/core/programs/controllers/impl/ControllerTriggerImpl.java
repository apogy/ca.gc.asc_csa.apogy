package ca.gc.asc_csa.apogy.core.programs.controllers.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.io.jinput.Activator;
import ca.gc.asc_csa.apogy.common.io.jinput.ApogyCommonIOJInputFactory;
import ca.gc.asc_csa.apogy.common.io.jinput.ApogyCommonIOJInputPackage;
import ca.gc.asc_csa.apogy.common.io.jinput.EComponent;
import ca.gc.asc_csa.apogy.common.io.jinput.EComponentQualifier;
import ca.gc.asc_csa.apogy.common.io.jinput.EControllerEnvironment;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerTrigger;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Controller Trigger</b></em>'. <!-- end-user-doc --> * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.programs.controllers.impl.ControllerTriggerImpl#getComponentQualifier <em>Component Qualifier</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.programs.controllers.impl.ControllerTriggerImpl#getAdapter <em>Adapter</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ControllerTriggerImpl extends TriggerImpl implements ControllerTrigger 
{
	boolean busy = false;
	
	
	/**
	 * The cached value of the '{@link #getComponentQualifier() <em>Component Qualifier</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->	 * @see #getComponentQualifier()
	 * @generated
	 * @ordered
	 */
	protected EComponentQualifier componentQualifier;

	/**
	 * The default value of the '{@link #getAdapter() <em>Adapter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getAdapter()
	 * @generated
	 * @ordered
	 */
	protected static final Adapter ADAPTER_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getAdapter() <em>Adapter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getAdapter()
	 * @generated
	 * @ordered
	 */
	protected Adapter adapter = ADAPTER_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	protected ControllerTriggerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreProgramsControllersPackage.Literals.CONTROLLER_TRIGGER;
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EComponentQualifier getComponentQualifier() {
		if (getComponentQualifierGen() == null) {
			if (ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(this) == null) 
			{
				setComponentQualifier(ApogyCommonIOJInputFactory.eINSTANCE.createEComponentQualifier());
			} 
			else 
			{
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
						ApogyCoreProgramsControllersPackage.Literals.CONTROLLER_TRIGGER__COMPONENT_QUALIFIER,
						ApogyCommonIOJInputFactory.eINSTANCE.createEComponentQualifier());
			}
		}
		return getComponentQualifierGen();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	public EComponentQualifier getComponentQualifierGen() {
		return componentQualifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	public NotificationChain basicSetComponentQualifier(EComponentQualifier newComponentQualifier,
			NotificationChain msgs) {
		EComponentQualifier oldComponentQualifier = componentQualifier;
		componentQualifier = newComponentQualifier;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__COMPONENT_QUALIFIER, oldComponentQualifier, newComponentQualifier);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void setComponentQualifier(EComponentQualifier newComponentQualifier) {
		setComponentQualifierGen(newComponentQualifier);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	public void setComponentQualifierGen(EComponentQualifier newComponentQualifier) {
		if (newComponentQualifier != componentQualifier) {
			NotificationChain msgs = null;
			if (componentQualifier != null)
				msgs = ((InternalEObject)componentQualifier).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__COMPONENT_QUALIFIER, null, msgs);
			if (newComponentQualifier != null)
				msgs = ((InternalEObject)newComponentQualifier).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__COMPONENT_QUALIFIER, null, msgs);
			msgs = basicSetComponentQualifier(newComponentQualifier, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__COMPONENT_QUALIFIER, newComponentQualifier, newComponentQualifier));
	}
	
//	/**
//	 * @generated_NOT
//	 */
//	@Override
//	public void setStarted(boolean newStarted) 
//	{
//		super.setStarted(newStarted);		
//		if (newStarted) 
//		{			
//			Activator.getEControllerEnvironment().eAdapters().add(getAdapter());
//		} 
//		else 
//		{
//			Activator.getEControllerEnvironment().eAdapters().remove(getAdapter());
//		}
//	}

	@Override
	public void start() 
	{
		setStarted(true);
		Activator.getEControllerEnvironment().eAdapters().add(getAdapter());		
	}
	
	@Override
	public void stop() 
	{
		setStarted(false);
		Activator.getEControllerEnvironment().eAdapters().remove(getAdapter());
	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public Adapter getAdapter() 
	{
		Adapter adapter = getAdapterGen();
		if (adapter == null) 
		{
			adapter = new AdapterImpl()
			{				
				boolean oldValue = false;	
				
				@Override
				synchronized public void notifyChanged(Notification msg) 
				{									
					if(getOperationCallControllerBinding().isStarted())
					{
						/** If pollingCount */
						if (msg.getFeatureID(EControllerEnvironment.class) == ApogyCommonIOJInputPackage.ECONTROLLER_ENVIRONMENT__POLLING_COUNT) 
						{
							EComponent component = Activator.getEControllerEnvironment().resolveEComponent(getComponentQualifier());
							
							
							
							/** If there is a component */
							if (component != null && !busy) 
							{
								busy = true;
								float pollData = component.getPollData();
								boolean latest = convert(pollData);
																	 								
								if (Boolean.logicalXor(oldValue, latest)) 
								{	
									boolean prev = new Boolean(oldValue);
									boolean now = new Boolean(latest);
									
									oldValue = new Boolean(latest);																		
									update(prev, now);																									
								}		
								busy = false;
							}
						}						
						
					}
				}
			};
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCoreProgramsControllersPackage.Literals.CONTROLLER_TRIGGER__ADAPTER, adapter);
		}
		return getAdapterGen();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Adapter getAdapterGen() {
		return adapter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setAdapter(Adapter newAdapter) {
		Adapter oldAdapter = adapter;
		adapter = newAdapter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__ADAPTER, oldAdapter, adapter));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__COMPONENT_QUALIFIER:
				return basicSetComponentQualifier(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__COMPONENT_QUALIFIER:
				return getComponentQualifier();
			case ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__ADAPTER:
				return getAdapter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__COMPONENT_QUALIFIER:
				setComponentQualifier((EComponentQualifier)newValue);
				return;
			case ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__ADAPTER:
				setAdapter((Adapter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__COMPONENT_QUALIFIER:
				setComponentQualifier((EComponentQualifier)null);
				return;
			case ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__ADAPTER:
				setAdapter(ADAPTER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__COMPONENT_QUALIFIER:
				return componentQualifier != null;
			case ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER__ADAPTER:
				return ADAPTER_EDEFAULT == null ? adapter != null : !ADAPTER_EDEFAULT.equals(adapter);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (adapter: ");
		result.append(adapter);
		result.append(')');
		return result.toString();
	}

	/**
	 * Method that is called when a change on the Controller component is
	 * detected.
	 * 
	 * @param oldValue
	 *            The previous value of the Button.
	 * @param newValue
	 *            The current value of the Button.
	 */
	abstract protected void update(boolean oldValue, boolean newValue);

	protected boolean convert(float value) {
		if (value <= 0)
		{
			return false;
		} 
		else {
			return true;
		}
	}

} // ControllerTriggerImpl

package ca.gc.asc_csa.apogy.common.topology.ui;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;

import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.impl.ApogyCommonTopologyUIFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Facade for Topology UI.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIPackage#getApogyCommonTopologyUIFacade()
 * @model
 * @generated
 */
public interface ApogyCommonTopologyUIFacade extends EObject 
{
	public static final ApogyCommonTopologyUIFacade INSTANCE = ApogyCommonTopologyUIFacadeImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a GraphicsContext for a given topology tree.
	 * @param topologyRoot The root node of the topology.
	 * @return The GraphicsContext.
	 * <!-- end-model-doc -->
	 * @model unique="false" topologyRootUnique="false"
	 * @generated
	 */
	GraphicsContext createGraphicsContext(Node topologyRoot);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Finds the bounding box for a given topology tree.
	 * @param root The root node of the topology.
	 * @param topologyPresentationSet The presentation set containing all the nodes presentations for the topology tree.
	 * @param min The coordinates where the minimum values of the bounding box will be written to.
	 * @param max The coordinates where the maximum values of the bounding box will be written to.
	 * <!-- end-model-doc -->
	 * @model rootUnique="false" topologyPresentationSetUnique="false" minUnique="false" maxUnique="false"
	 * @generated
	 */
	void findExtent(Node root, TopologyPresentationSet topologyPresentationSet, Tuple3d min, Tuple3d max);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a NodeSelection from the selected point relative position.
	 * @param topologyPresentationSet The TopologyPresentationSet containing the node presentation for the selected node.
	 * @param nodePresentation The NodePresentation for the selected Node.
	 * @param relativePosition The relative position of the selected point.
	 * @return The NodeSelection.
	 * <!-- end-model-doc -->
	 * @model unique="false" topologyPresentationSetUnique="false" nodePresentationUnique="false" relativePositionDataType="ca.gc.asc_csa.apogy.common.topology.ui.Point3d" relativePositionUnique="false"
	 * @generated
	 */
	NodeSelection createNodeSelection(TopologyPresentationSet topologyPresentationSet, NodePresentation nodePresentation, Point3d relativePosition);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a NodeSelection from the selected point relative position and normal.
	 * @param topologyPresentationSet The TopologyPresentationSet containing the node presentation for the selected node.
	 * @param node The selected Node.
	 * @param relativePosition The relative position of the selected point.
	 * @param relativeNormal The relative normal at the selected point.
	 * @return The NodeSelection.
	 * <!-- end-model-doc -->
	 * @model unique="false" topologyPresentationSetUnique="false" nodeUnique="false" relativePositionDataType="ca.gc.asc_csa.apogy.common.topology.ui.Point3d" relativePositionUnique="false" relativeNormalDataType="ca.gc.asc_csa.apogy.common.topology.ui.Vector3f" relativeNormalUnique="false"
	 * @generated
	 */
	NodeSelection createNodeSelection(TopologyPresentationSet topologyPresentationSet, Node node, Point3d relativePosition, Vector3f relativeNormal);

} // ApogyCommonTopologyUIFacade

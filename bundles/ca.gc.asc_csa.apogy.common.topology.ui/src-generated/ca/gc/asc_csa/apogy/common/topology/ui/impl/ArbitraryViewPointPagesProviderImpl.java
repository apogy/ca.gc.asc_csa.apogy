/**
 * Copyright (c) 2015-2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.ui.impl;

import javax.vecmath.Matrix3d;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.GeometricUtils;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFactory;
import ca.gc.asc_csa.apogy.common.topology.ArbitraryViewPoint;
import ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIPackage;
import ca.gc.asc_csa.apogy.common.topology.ui.ArbitraryViewPointPagesProvider;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Arbitrary View Point Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ArbitraryViewPointPagesProviderImpl extends AbstractViewPointPagesProviderImpl implements ArbitraryViewPointPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArbitraryViewPointPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyUIPackage.Literals.ARBITRARY_VIEW_POINT_PAGES_PROVIDER;
	}
	
	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		ArbitraryViewPoint arbitraryViewPoint = ApogyCommonTopologyFactory.eINSTANCE.createArbitraryViewPoint();
		arbitraryViewPoint.setPosition(ApogyCommonMathFacade.INSTANCE.createTuple3d(0, 0, 0));
		
		Matrix3d rotationMatrix = GeometricUtils.packXYZ(Math.toRadians(90), Math.toRadians(90), 0);		
		arbitraryViewPoint.setRotationMatrix(ApogyCommonMathFacade.INSTANCE.createMatrix3x3(rotationMatrix));
		
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings = (MapBasedEClassSettings) settings;
			arbitraryViewPoint.setName((String) mapBasedEClassSettings.getUserDataMap().get("name"));						
		}
			
		return arbitraryViewPoint;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	

		// TODO
		
		return list;
	}
} //ArbitraryViewPointPagesProviderImpl

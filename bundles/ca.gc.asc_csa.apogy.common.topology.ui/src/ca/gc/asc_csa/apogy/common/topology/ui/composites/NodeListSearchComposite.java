/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.ui.composites;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.Node;

public class NodeListSearchComposite extends AbstractNodeSearchComposite 
{		
	// The root of the topology.
	protected List<Node> nodeList = null;
				
	public NodeListSearchComposite(Composite parent, int style) 
	{
		super(parent, style);	
	}
	
	public NodeListSearchComposite(Composite parent, int style, List<Node> nodes) 
	{
		super(parent, style);
		setNodeList(nodes);
	}
	
	@Override
	protected AdapterFactoryContentProvider createContentProvider() 
	{
		return new NodeContentProvider(adapterFactory);
	}

	@Override
	protected AdapterFactoryLabelProvider createLabelProvider() 
	{		
		return new NodeLabelProvider(adapterFactory);
	}
	
	/**
	 * Sets the list of Nodes for which to display and filter available Nodes.
	 * @param nodes The list of nodes..
	 */
	public void setNodeList(List<Node> nodes)
	{
		this.nodeList = nodes;
		
		// Clears the selected Node(s)
		this.selectedNode = null;
		
		nodeSelectedChanged(null);
		
		// Forces the filters to be applied and the displays updated.
		applyFilters();
	}
	
	@Override
	protected Set<Node> getUnfilteredNodes()
	{
		Set<Node> nodes = new HashSet<Node>();
		
		if(nodeList != null)
		{
			nodes.addAll(nodeList);
		}
		
		return nodes;
	}
	
	/**
	 * Content provider for the arguments.
	 * 
	 */
	private class NodeContentProvider extends AdapterFactoryContentProvider {

		public NodeContentProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		@Override
		public Object[] getElements(Object object) 
		{
			if(object instanceof Collection)
			{
				return ((Collection<?>) object).toArray();
			}
			return null;
		}

		@Override
		public Object[] getChildren(Object object) 
		{
			return null;
		}

		@Override
		public boolean hasChildren(Object object) 
		{
			return false;
		}
	}

	/**
	 * Label provider for the arguments.
	 * 
	 */
	private class NodeLabelProvider extends AdapterFactoryLabelProvider 
	{
		public NodeLabelProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		@Override
		public String getColumnText(Object object, int columnIndex) {
			String str = "<undefined>";

			switch (columnIndex) 
			{
				case ID_COLUMN_INDEX:
					if(object instanceof Node)
					{
						str = ((Node) object).getNodeId();
					}				
				break;
				
				case DESCRIPTION_COLUMN_INDEX:
					if(object instanceof Node)
					{
						str = ((Node) object).getDescription();
					}
				break;
			default:
				break;
			}
			return str;
		}
		
		@Override
		public Image getColumnImage(Object object, int columnIndex) 
		{
			if(columnIndex == ID_COLUMN_INDEX)
			{
				return super.getColumnImage(object, columnIndex);
			}
			else
			{
				return null;
			}
		}
	}
}

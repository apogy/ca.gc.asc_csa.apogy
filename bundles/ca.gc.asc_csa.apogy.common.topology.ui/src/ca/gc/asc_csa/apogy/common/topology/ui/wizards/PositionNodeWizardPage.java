/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dComposite;
import ca.gc.asc_csa.apogy.common.topology.PositionNode;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class PositionNodeWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.ui.wizards.PositionNodeWizardPage";
		
	private PositionNode positionNode;
	
	private Tuple3dComposite positionComposite;
	
	private DataBindingContext m_bindingContext;
		
	
	public PositionNodeWizardPage(PositionNode positionNode) 
	{
		super(WIZARD_PAGE_ID);
		this.positionNode = positionNode;
				
		setTitle("Position Node");
		setDescription("Sets the Node position.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(2, false));
							
		Label lblPosition = new Label(top, SWT.NONE);
		lblPosition.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPosition.setText("Position (m):");
		
		positionComposite = new Tuple3dComposite(top, SWT.NONE, ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(positionNode), "0.000");
		positionComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		positionComposite.setTuple3d(positionNode.getPosition());
							
		setControl(top);
		
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if(m_bindingContext != null) m_bindingContext.dispose();				
			}
		});
	}			
		
	protected void validate()
	{
		setErrorMessage(null);			
		
		setPageComplete(getErrorMessage() == null);
	}
		
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
						
		return bindingContext;
	}
}

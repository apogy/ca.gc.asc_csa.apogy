/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.math.ui.composites.RotationMatrixComposite;
import ca.gc.asc_csa.apogy.common.topology.RotationNode;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class RotationNodeWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.ui.wizards.PositionNodeWizardPage";
		
	private RotationNode rotationNode;
	
	private RotationMatrixComposite rotationComposite;
	
	private DataBindingContext m_bindingContext;
		
	
	public RotationNodeWizardPage(RotationNode positionNode) 
	{
		super(WIZARD_PAGE_ID);
		this.rotationNode = positionNode;
				
		setTitle("Rotation Node");
		setDescription("Sets the Node orientation.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
		
		rotationComposite = new RotationMatrixComposite(top, SWT.NONE, ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(rotationNode));
		rotationComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		rotationComposite.setMatrix3x3(rotationNode.getRotationMatrix());
							
		setControl(top);
		
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {				
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}			
		
	protected void validate()
	{
		setErrorMessage(null);			
		
		setPageComplete(getErrorMessage() == null);
	}
		
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
						
		return bindingContext;
	}
}

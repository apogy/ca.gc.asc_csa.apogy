package ca.gc.asc_csa.apogy.common.topology.ui.dialogs;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.NodeSearchComposite;

public class NodeSelectionDialog extends Dialog 
{
	private Node root;
	private Node selectedNode;
	
	
	@SuppressWarnings("unused")
	private NodeSearchComposite nodeSearchComposite;
	
	public NodeSelectionDialog(Shell parentShell) 
	{
		super(parentShell);
	}
	
	/**
	 * @wbp.parser.constructor
	 */
	public NodeSelectionDialog(Shell parentShell, Node root) 
	{
		this(parentShell);
		this.root = root;
	}
	
	public Node getSelectedNode() {
		return selectedNode;
	}

	/**
	 * Return the NodeSearchComposite used by the Dialog.
	 * @return The NodeSearchComposite.
	 */
	public NodeSearchComposite getNodeSearchComposite()
	{
		return nodeSearchComposite;
	}
	
	
	protected void setSelectedNode(Node newNodeSelected)
	{
		this.selectedNode = newNodeSelected;
	}
	
	@Override
	protected void configureShell(Shell newShell) 
	{	
		super.configureShell(newShell);
		newShell.setText("Select a Node");
	}
	
	@Override
	protected Control createDialogArea(Composite parent) 
	{	
		Composite area = (Composite) super.createDialogArea(parent);
		
		area.setLayout(new GridLayout(1,false));
		
		nodeSearchComposite = createNodeSearchComposite(area);
	    	   	    	 	    	    
	    return area;
	}	
	
	/**
	 * Method called to create the NodeSearchComposite used in the composite. Overload to customize the NodeSearchComposite.
	 * @param parent The parent composite,
	 * @return The NodeSearchComposite.
	 */
	protected NodeSearchComposite createNodeSearchComposite(Composite parent)
	{
		return new NodeSearchComposite(parent, SWT.NONE, root)
		{
			@Override
			public void nodeSelectedChanged(Node nodeSelected) 
			{
				NodeSelectionDialog.this.setSelectedNode(nodeSelected);				
			}
		};
	}
}

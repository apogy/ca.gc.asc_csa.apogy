/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.URLNode;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.URLSelectionComposite;

public class URLNodeWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.ui.wizards.URLNodeWizardPage";
		
	private URLNode urlNode;	
	private URLSelectionComposite urlSelectionComposite;				
	private DataBindingContext m_bindingContext;
	
	private String urlString = null;
	
	public URLNodeWizardPage(URLNode urlNode) 
	{
		super(WIZARD_PAGE_ID);
		this.urlNode = urlNode;
		
		this.urlString = urlNode.getUrl();
		
		setTitle("URL Node");
		setDescription("Sets the Node URL refering to the gometry file.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
						
		// URL Selection
		urlSelectionComposite = new URLSelectionComposite(top, SWT.None, new String[]{"*.obj"}, true, true, true)
		{
			@Override
			protected void urlStringSelected(String newURLString) 
			{		
				URLNodeWizardPage.this.urlString = newURLString;
				
				validate();
				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(urlNode, ApogyCommonTopologyPackage.Literals.URL_NODE__URL, newURLString);
			}
		};
		urlSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		if(urlNode != null && urlNode.getUrl() != null)
		{
			urlSelectionComposite.setUrlString(urlNode.getUrl());
		}
		
				
		setControl(top);
		urlSelectionComposite.setFocus();	
		
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {				
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);
		
		
		// Checks the URL.
		boolean urlValid = (urlString!= null) && (urlString.length() > 0);		
		if(!urlValid)
		{
			setErrorMessage("Invalid URL specified !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
		
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		return bindingContext;
	}
}

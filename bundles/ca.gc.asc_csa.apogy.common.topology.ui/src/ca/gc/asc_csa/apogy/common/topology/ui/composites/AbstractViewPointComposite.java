/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.math.ui.composites.RotationMatrixComposite;
import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dComposite;
import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class AbstractViewPointComposite extends Composite 
{	
	private AbstractViewPoint abstractViewPoint;
	
	private Text txtName;
	private Tuple3dComposite positionComposite;
	private RotationMatrixComposite rotationComposite;
	
	private DataBindingContext m_bindingContext;
	
	public AbstractViewPointComposite(Composite parent, int style, AbstractViewPoint abstractViewPoint) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, false) );
		
		Label lblName = new Label(this, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name:");
		
		txtName = new Text(this, SWT.BORDER);
		txtName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		Label lblPosition = new Label(this, SWT.NONE);
		lblPosition.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPosition.setText("Position (m):");
				
		EditingDomain editingDomain = ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(abstractViewPoint);
		if(editingDomain == null) editingDomain = ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain();
		
		positionComposite = new Tuple3dComposite(this, SWT.NONE, editingDomain, "0.000");
		positionComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));		
	
		Label lblNodeRotation = new Label(this, SWT.NONE);
		lblNodeRotation.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblNodeRotation.setText("Rotation (deg):");
		
		rotationComposite = new RotationMatrixComposite(this, SWT.NONE, editingDomain);
		rotationComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));		
							
		setAbstractViewPoint(abstractViewPoint);
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {				
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}
	
	public AbstractViewPoint getAbstractViewPoint() 
	{
		return abstractViewPoint;
	}

	public void setAbstractViewPoint(AbstractViewPoint abstractViewPoint) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.abstractViewPoint = abstractViewPoint;
		
		if(abstractViewPoint != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}

	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
					
		positionComposite.setTuple3d(abstractViewPoint.getPosition());
		rotationComposite.setMatrix3x3(abstractViewPoint.getRotationMatrix());
		
		IObservableValue<Double> observeName = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(abstractViewPoint), 
				  FeaturePath.fromList(ApogyCommonTopologyPackage.Literals.ABSTRACT_VIEW_POINT__NAME)).observe(abstractViewPoint);
		
		IObservableValue<?> nameObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtName);
			
		bindingContext.bindValue(nameObserveWidget, observeName, null, null);
		
		return bindingContext;
	}
	
}

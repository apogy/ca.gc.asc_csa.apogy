/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.ui.composites;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFactory;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.NodeDescriptionFilter;
import ca.gc.asc_csa.apogy.common.topology.NodeFilterChain;
import ca.gc.asc_csa.apogy.common.topology.NodeIdFilter;
import ca.gc.asc_csa.apogy.common.topology.NodeTypeFilter;

public abstract class AbstractNodeSearchComposite extends Composite 
{	
	protected ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	public static final int ID_COLUMN_INDEX = 0;
	public static final int DESCRIPTION_COLUMN_INDEX = 1;
		
	protected List<Node> filteredNodes = new ArrayList<Node>();
	
	protected Node selectedNode = null;
	protected List<Node> selectedNodes = new ArrayList<Node>();
	
	// The filter class
	protected EClass nodeFilterType = null;
	
	private NodeTypeComboComposite nodeClassFilterCombo;
	private Button btnClearTypeFilter;
	
	private Text nodeIDFilterText;
	private Button btnClearIDFilter;
	
	private Text nodeDescriptionFilterText;
	private Button btnClearDescriptionFilter;
	private Table filteredNodesTable;
	private TableViewer filteredNodesTableViewer;
	private Button btnApply;
			
	public AbstractNodeSearchComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new org.eclipse.swt.layout.GridLayout(3, false));
		
		// Node Type Filter.
		Label lblTypeFilter = new Label(this, SWT.NONE);
		lblTypeFilter.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTypeFilter.setText("Type Filter");
		nodeClassFilterCombo = new NodeTypeComboComposite(this, SWT.NONE)
		{
			@Override
			public void typeSelected(EClass eClass) 
			{				
				setTypeFilter(eClass);
			}
		};
		nodeClassFilterCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		btnClearTypeFilter = new Button(this, SWT.NONE);
		btnClearTypeFilter.setText("Clear");
		btnClearTypeFilter.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				nodeClassFilterCombo.clearSelection();
				//setTypeFilter(null);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {	
			}
		});
		
		// Node Id Filter.
		Label lblIdFilter = new Label(this, SWT.NONE);
		lblIdFilter.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblIdFilter.setText("ID Filter");
		
		nodeIDFilterText = new Text(this, SWT.BORDER);
		nodeIDFilterText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		nodeIDFilterText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) 
			{				
				applyFilters();
			}
		});
		
		btnClearIDFilter = new Button(this, SWT.NONE);
		btnClearIDFilter.setText("Clear");
		btnClearIDFilter.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				nodeIDFilterText.setText("");
				applyFilters();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {	
			}
		});
		
		// Node Description Filter.
		Label lblDescriptionFilter = new Label(this, SWT.NONE);
		lblDescriptionFilter.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDescriptionFilter.setText("Description Filter");
		
		nodeDescriptionFilterText = new Text(this, SWT.BORDER);
		nodeDescriptionFilterText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));	
		nodeDescriptionFilterText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) 
			{				
				applyFilters();
			}
		});
		
				
		btnClearDescriptionFilter = new Button(this, SWT.NONE);
		btnClearDescriptionFilter.setText("Clear");
		btnClearDescriptionFilter.addSelectionListener(new SelectionListener() 
		{
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{				
				nodeDescriptionFilterText.setText("");
				applyFilters();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {								
			}
		});
		
		// Apply filters
		btnApply = new Button(this, SWT.NONE);
		btnApply.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnApply.setText("Update");
		btnApply.setToolTipText("Force the topology to be updated and the filters re-applied.");
		btnApply.addSelectionListener(new SelectionListener() 
		{
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{								
				applyFilters();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {								
			}
		});
		
		
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		
		// Filtered table viewer.
		filteredNodesTableViewer = new TableViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);		
		filteredNodesTable = filteredNodesTableViewer.getTable();
		filteredNodesTable.setHeaderVisible(true);
		filteredNodesTable.setLinesVisible(true);
		GridData gd_filteredNodesTable = new GridData(SWT.FILL, SWT.TOP, true, false, 3, 1);
		gd_filteredNodesTable.heightHint = 300;
		gd_filteredNodesTable.minimumHeight = 300;
		filteredNodesTable.setLayoutData(gd_filteredNodesTable);
		
		// First Column : NodeId
		TableColumn tblclmnNodeId = new TableColumn(filteredNodesTable, SWT.NONE);
		tblclmnNodeId.setWidth(200);
		tblclmnNodeId.setText("Id");

		// Second Column : Node Description
		TableColumn tblclmnNodeDescription = new TableColumn(filteredNodesTable, SWT.NONE);
		tblclmnNodeDescription.setWidth(400);
		tblclmnNodeDescription.setText("Description");		
		
		filteredNodesTableViewer.setContentProvider(createContentProvider());
		filteredNodesTableViewer.setLabelProvider(createLabelProvider());
		filteredNodesTableViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{			
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				if(event.getSelection() instanceof IStructuredSelection)
				{
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					
					// Clears the selected Nodes list.					
					selectedNodes.clear();
					
					if(!iStructuredSelection.isEmpty())
					{					
						nodeSelectedChanged((Node) iStructuredSelection.getFirstElement());
						
						// Process the list of selected nodes.
						Collection<Node> nodesSelected = new ArrayList<Node>();		
						
						@SuppressWarnings("unchecked")
						List<Object> selectedObjects = iStructuredSelection.toList();
						for(Object object : selectedObjects)
						{
							if(object instanceof Node)	nodesSelected.add((Node) object);						
						}
						
						// Update selected nodes.
						selectedNodes.addAll(nodesSelected);
						
						nodesSelectedChanged(nodesSelected);
					}
					else
					{
						nodeSelectedChanged(null);
						nodesSelectedChanged(new ArrayList<Node>());
					}				
				}
			}
		});
		
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
	}
	
	/**
	 * Method that gets called when a node is selected by the user. This method should be overloaded to get notified of a node selection.
	 * @param nodeSelected The node selected by the user, can be null.
	 */
	public void nodeSelectedChanged(Node nodeSelected)
	{	
	}	
	
	/**
	 * Method that gets called when 1 or more nodes are selected by the user. This method should be overloaded to get notified of a node selection.
	 * @param nodesSelected The list of nodes selected. Can be empty, but never null.
	 */
	protected void nodesSelectedChanged(Collection<Node> nodesSelected)
	{	
	}	
	
	public void initializeTypeFilter(EClass eClass)
	{
		getDisplay().asyncExec(new Runnable() 
		{			
			@Override
			public void run() 
			{
				if(nodeClassFilterCombo != null && !nodeClassFilterCombo.isDisposed())
				{
					nodeClassFilterCombo.selectType(eClass);
				}				
			}
		});
	}
	
	/**
	 * Sets the EClass to be used to filter available Nodes. Use null to disable this filter.
	 * @param eClass The EClass used to filter, null to disable this filter.
	 */
	public void setTypeFilter(EClass eClass)
	{
		this.nodeFilterType = eClass;
	
		// Forces the filters to be applied and the displays updated.
		applyFilters();
	}		

	/**
	 * Returns the node currently selected.
	 * @return The node currently selected. Can be null. If multiples nodes are selected, the first one is returned.
	 */
	public Node getSelectedNode()
	{
		return selectedNode;
	}
	
	/**
	 * Returns the list of nodes currently selected.
	 * @return The list of nodes currently selected. Can be empty, never null;
	 */
	public Collection<Node> getSelectedNodes()
	{	
		return selectedNodes;
	}
		
	public void applyFilters()
	{				
		// Creates a NodeFilterChain
		NodeFilterChain nodeFilterChain = ApogyCommonTopologyFactory.eINSTANCE.createNodeFilterChain();
		
		if(nodeFilterType != null)
		{
			NodeTypeFilter nodeTypeFilter =  ApogyCommonTopologyFactory.eINSTANCE.createNodeTypeFilter();
			nodeTypeFilter.setClazz(nodeFilterType);
			nodeFilterChain.getFilters().add(nodeTypeFilter);
		}
		
		if(nodeIDFilterText.getText() != null && nodeIDFilterText.getText().length() > 0)
		{
			NodeIdFilter nodeIdFilter = ApogyCommonTopologyFactory.eINSTANCE.createNodeIdFilter();
			nodeIdFilter.setRegex(convertFilterStringToRegex(nodeIDFilterText.getText()));
			nodeFilterChain.getFilters().add(nodeIdFilter);
		}
		
		if(nodeDescriptionFilterText.getText() != null && nodeDescriptionFilterText.getText().length() > 0)
		{
			NodeDescriptionFilter nodeDescriptionFilter = ApogyCommonTopologyFactory.eINSTANCE.createNodeDescriptionFilter();
			nodeDescriptionFilter.setRegex(convertFilterStringToRegex(nodeDescriptionFilterText.getText() ));
			nodeFilterChain.getFilters().add(nodeDescriptionFilter);
		}
		
		// Applies the filters.
		Collection<Node> matches = nodeFilterChain.filter(getUnfilteredNodes());				
				
		// Updates the selected Nodes		
		filteredNodes.clear();
		filteredNodes.addAll(matches);
								
		// Updates the displayed nodes.
		filteredNodesTableViewer.setInput(filteredNodes);		
	}
	
	/**
	 * Method that return all nodes, before filtering.
	 * @return A set containing all Nodes.
	 */
	abstract protected Set<Node> getUnfilteredNodes();
	
	
	protected String convertFilterStringToRegex(String filterString)
	{
		String regex = new String(filterString);
		
		if(filterString.indexOf("*") >= 0)
		{
			// Replace all * by .*
			regex = regex.replace("*", ".*");								
		}
		
		// If the filter string does not start with .*, adds a ^
		if(!regex.startsWith(".*"))
		{
			regex = "^" + regex;			
		}
		
		if(!regex.endsWith(".*"))
		{
			regex = regex + ".*";
		}
		
		return regex; 
	}
		
	protected abstract AdapterFactoryContentProvider createContentProvider();

	protected abstract AdapterFactoryLabelProvider createLabelProvider();
}

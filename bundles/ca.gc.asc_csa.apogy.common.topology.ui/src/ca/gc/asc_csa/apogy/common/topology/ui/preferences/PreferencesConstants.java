package ca.gc.asc_csa.apogy.common.topology.ui.preferences;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/


public class PreferencesConstants 
{
	// Visibility Constants.
	public static final String DEFAULT_POSITION_NODE_AXIS_VISIBILITY_ID = "DEFAULT_POSITION_NODE_AXIS_VISIBILITY";	
	public static final Boolean DEFAULT_POSITION_NODE_AXIS_VISIBILITY = new Boolean(false);	
	public static final String DEFAULT_POSITION_NODE_AXIS_LENGTH_ID = "DEFAULT_POSITION_NODE_AXIS_LENGTH_ID";	
	public static final Double DEFAULT_POSITION_NODE_AXIS_LENGTH = new Double(1.0);	

	public static final String DEFAULT_ROTATION_NODE_AXIS_VISIBILITY_ID = "DEFAULT_ROTATION_NODE_AXIS_VISIBILITY";	
	public static final Boolean DEFAULT_ROTATION_NODE_AXIS_VISIBILITY = new Boolean(false);	
	public static final String DEFAULT_ROTATION_NODE_AXIS_LENGTH_ID = "DEFAULT_ROTATION_NODE_AXIS_LENGTH_ID";	
	public static final Double DEFAULT_ROTATION_NODE_AXIS_LENGTH = new Double(1.0);	
		
	public static final String DEFAULT_TRANSFORM_NODE_AXIS_VISIBILITY_ID = "DEFAULT_TRANSFORM_NODE_AXIS_VISIBILITY";	
	public static final Boolean DEFAULT_TRANSFORM_NODE_AXIS_VISIBILITY = new Boolean(false);
	public static final String DEFAULT_TRANSFORM_NODE_AXIS_LENGTH_ID = "DEFAULT_TRANSFORM_NODE_AXIS_LENGTH";	
	public static final Double DEFAULT_TRANSFORM_NODE_AXIS_LENGTH = new Double(1.0);	
		
	public static final String DEFAULT_URL_NODE_AXIS_VISIBILITY_ID = "DEFAULT_URL_NODE_AXIS_VISIBILITY_ID";	
	public static final Boolean DEFAULT_URL_NODE_AXIS_VISIBILITY = new Boolean(false);
	public static final String DEFAULT_URL_NODE_AXIS_LENGTH_ID = "DEFAULT_URL_NODE_AXIS_LENGTH_ID";	
	public static final Double DEFAULT_URL_NODE_AXIS_LENGTH = new Double(0.5);	

	public static final String DEFAULT_CAD_NODE_VISIBILITY_ID = "DEFAULT_CAD_NODE_VISIBILITY_ID";	
	public static final Boolean DEFAULT_CAD_NODE_VISIBILITY = new Boolean(true);
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.ui.parts;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.converters.ApogyCommonConvertersFacade;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.NodeSelection;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.NodeToNodeDistanceComposite;

public class NodesDistancePart
{
	private Composite parentComposite; 
	
	@Inject
	protected ESelectionService selectionService;
	
	private ISelectionListener selectionListener;
	
	private NodeToNodeDistanceComposite nodeToNodeDistanceComposite = null;

	@PostConstruct
	public void createContent(Composite parent)
	{
		parentComposite = parent;
		parentComposite.setLayout(new FillLayout());		
		
		nodeToNodeDistanceComposite = new NodeToNodeDistanceComposite(parentComposite, SWT.BORDER);		
		parentComposite.layout();
		
		// Register to the selection service.
		selectionService.addSelectionListener(getISelectionListener());				
	}
	
	@PreDestroy
	public void dispose()
	{
		selectionService.removeSelectionListener(getISelectionListener());
	}
	
	private ISelectionListener getISelectionListener()
	{
		if(selectionListener == null)
		{			
			selectionListener = new ISelectionListener() 
			{			
				@Override
				public void selectionChanged(MPart part, Object selection) 
				{
					if(selection instanceof Node)
					{
						Node node = (Node) selection;
						nodeToNodeDistanceComposite.selectNode(node);
					}
					else if(selection instanceof NodeSelection)
					{
						NodeSelection nodeSelection = (NodeSelection) selection;
						Node node = nodeSelection.getSelectedNode();	
						nodeToNodeDistanceComposite.selectNode(node);
					}
					else
					{
						// Call Converters.
						Node node = (Node) ApogyCommonConvertersFacade.INSTANCE.convert(selection, Node.class);
						nodeToNodeDistanceComposite.selectNode(node);
					}
				}
			};				
		}
		return selectionListener;
	}
}

/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth;

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.environment.earth.impl.ApogyEarthFacadeImpl;
import ca.gc.asc_csa.apogy.core.invocator.Environment;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apogy Earth Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Facade for Earth Environment.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthFacade#getActiveEarthWorksite <em>Active Earth Worksite</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage#getApogyEarthFacade()
 * @model
 * @generated
 */
public interface ApogyEarthFacade extends EObject 
{
	/**
	 * Returns the value of the '<em><b>Active Earth Worksite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Refers to the active EarthWorksite. May be null.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Active Earth Worksite</em>' reference.
	 * @see #setActiveEarthWorksite(EarthWorksite)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage#getApogyEarthFacade_ActiveEarthWorksite()
	 * @model transient="true"
	 * @generated
	 */
	EarthWorksite getActiveEarthWorksite();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthFacade#getActiveEarthWorksite <em>Active Earth Worksite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active Earth Worksite</em>' reference.
	 * @see #getActiveEarthWorksite()
	 * @generated
	 */
	void setActiveEarthWorksite(EarthWorksite value);

	public static ApogyEarthFacade INSTANCE = ApogyEarthFacadeImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return a unit vector pointing toward the Moon. The vector is defined for a specified topology node within a specified ApogySystem.
	 * @param apogySystem The ApogySystem containing the specified Node.
	 * @param nodeID The ID of the specified Node.
	 * @return The unit vector pointing toward the Moon, defined in the specified node coordinates, null if none is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" apogySystemUnique="false" nodeIDUnique="false" environmentUnique="false"
	 * @generated
	 */
	Tuple3d getMoonVector(ApogySystem apogySystem, String nodeID, Environment environment);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return a unit vector pointing toward the Moon. The vector is defined for a specified topology node.
	 * @param nodeID The ID of the specified Node.
	 * @return The unit vector pointing toward the Moon, defined in the specified node coordinates, null if none is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" nodeUnique="false" environmentUnique="false"
	 * @generated
	 */
	Tuple3d getMoonVector(Node node, Environment environment);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create a GeographicCoordinates.
	 * @param longitude The longitude, in radians.
	 * @param latitude The latitude, in radians.
	 * @param altitude The altitude, in meters.
	 * <!-- end-model-doc -->
	 * @model unique="false" longitudeUnique="false" latitudeUnique="false" altitudeUnique="false"
	 * @generated
	 */
	GeographicCoordinates createGeographicCoordinates(double longitude, double latitude, double altitude);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Creates an EarthSurfaceLocation from parameters.
	 * @param name The name of the location.
	 * @param description The description of the location.
	 * @param longitude The longitude of the location, in radians.
	 * @param latitude The latitude of the location, in radians.
	 * @param elevation The elevation of the location, in meters.
	 * @return The EarthSurfaceLocation.
	 * <!-- end-model-doc -->
	 * @model unique="false" nameUnique="false" descriptionUnique="false" longitudeUnique="false"
	 *        longitudeAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" latitudeUnique="false"
	 *        latitudeAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" elevationUnique="false"
	 *        elevationAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'"
	 * @generated
	 */
	EarthSurfaceLocation createEarthSurfaceLocation(String name, String description, double longitude, double latitude, double elevation);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Loads a list of GeographicCoordinates from a CSV formated file (Longitude, Latitude, Altitude). Longitude, Latitude are expected in degrees, Altitude in meters.
	 * @param url File URL.
	 * @return The list of GeographicCoordinates. Never null, but can be empty.
	 * @throws An exception if a probelm occures during load.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.core.environment.earth.List<ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates>" unique="false" many="false" exceptions="ca.gc.asc_csa.apogy.core.environment.earth.Exception" urlUnique="false"
	 * @generated
	 */
	List<GeographicCoordinates> loadGeographicCoordinatesFromURL(String url) throws Exception;
} // ApogyEarthFacade

/**
 * ***********************************************************************
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 	SPDX-License-Identifier: EPL-1.0
 * ***********************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth;

import ca.gc.asc_csa.apogy.core.environment.Worksite;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earth Worksite</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Worksite defined for Earth
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.EarthWorksite#getGeographicalCoordinates <em>Geographical Coordinates</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.EarthWorksite#getSunIntensity <em>Sun Intensity</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage#getEarthWorksite()
 * @model abstract="true"
 * @generated
 */
public interface EarthWorksite extends Worksite {
	/**
	 * Returns the value of the '<em><b>Geographical Coordinates</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The geographical coordinates of the origin of the worksite, in the WS84 datum.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Geographical Coordinates</em>' containment reference.
	 * @see #setGeographicalCoordinates(GeographicCoordinates)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage#getEarthWorksite_GeographicalCoordinates()
	 * @model containment="true" required="true"
	 * @generated
	 */
	GeographicCoordinates getGeographicalCoordinates();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.EarthWorksite#getGeographicalCoordinates <em>Geographical Coordinates</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Geographical Coordinates</em>' containment reference.
	 * @see #getGeographicalCoordinates()
	 * @generated
	 */
	void setGeographicalCoordinates(GeographicCoordinates value);

	/**
	 * Returns the value of the '<em><b>Sun Intensity</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The current sun intensity at the surface of the worksite.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sun Intensity</em>' attribute.
	 * @see #setSunIntensity(double)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage#getEarthWorksite_SunIntensity()
	 * @model default="0" unique="false" transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='W/m^2' property='Readonly'"
	 * @generated
	 */
	double getSunIntensity();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.EarthWorksite#getSunIntensity <em>Sun Intensity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sun Intensity</em>' attribute.
	 * @see #getSunIntensity()
	 * @generated
	 */
	void setSunIntensity(double value);

} // EarthWorksite

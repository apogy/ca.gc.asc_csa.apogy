/**
 * Canadian Space Agency / Agence spatiale canadienne 2016 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.Variable;
import ca.gc.asc_csa.apogy.core.programs.javascript.ApogyCoreJavaScriptProgramsPackage;
import ca.gc.asc_csa.apogy.core.programs.javascript.ApogyJavaScriptFacade;
import ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Apogy Java Script Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ApogyJavaScriptFacadeImpl extends MinimalEObjectImpl.Container implements ApogyJavaScriptFacade 
{
	public static String JAVASCRIPT_MAIN_FUNCTION_HEADER = 	"/**\n" +
											 				" *\n" + 
											 				" * This is the entry point of the program. The arguments are passed in the same\n" +
											 				" * order as the variables are defined in the Apogy Session.\n" +
											 				" *\n";

	
	public static ApogyJavaScriptFacade instance = null;
	
	public static ApogyJavaScriptFacade getInstance() 
	{
		if (instance == null) {
			instance = new ApogyJavaScriptFacadeImpl();
		}
		return instance;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyJavaScriptFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreJavaScriptProgramsPackage.Literals.APOGY_JAVA_SCRIPT_FACADE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public String createJavaScriptCodeTemplate(InvocatorSession session, JavaScriptProgram javaScriptProgram) 
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(JAVASCRIPT_MAIN_FUNCTION_HEADER);
		sb.append(" *\n");
		
		// Adds a param entry for each of the variables.
		List<String> variablesNames = new ArrayList<String>();
		for(Variable variable : session.getEnvironment().getVariablesList().getVariables())
		{
			variablesNames.add(variable.getName());
			sb.append(" * @param {" + variable.getVariableType().getInterfaceClass().getInstanceTypeName() + "} " + variable.getName() + "\n");
		}
		
		// Adds the variables
		Iterator<String> it2 = variablesNames.iterator();
		while(it2.hasNext())
		{
			String variableName =  it2.next();
			sb.append(" * @variable " + variableName + "\t" + variableName + "\n");			
		}
		
		sb.append(" */");
		sb.append("\n");
		
		// Adds the function main
		sb.append("function main(");
		
		// Adds the parameters.
		Iterator<String> it = variablesNames.iterator();
		while(it.hasNext())
		{
			sb.append(it.next());
			if(it.hasNext())
			{
				sb.append(",");
			}
		}
		sb.append(") { \n\n }");
		
	
		return sb.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCoreJavaScriptProgramsPackage.APOGY_JAVA_SCRIPT_FACADE___CREATE_JAVA_SCRIPT_CODE_TEMPLATE__INVOCATORSESSION_JAVASCRIPTPROGRAM:
				return createJavaScriptCodeTemplate((InvocatorSession)arguments.get(0), (JavaScriptProgram)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ApogyJavaScriptFacadeImpl

/**
 * Canadian Space Agency / Agence spatiale canadienne 2016 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript;

import ca.gc.asc_csa.apogy.core.invocator.AbstractProgramRuntime;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Script Program Runtime</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Specialization of AbstractProgramRuntime used for executing Java Scripts.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ApogyCoreJavaScriptProgramsPackage#getJavaScriptProgramRuntime()
 * @model
 * @generated
 */
public interface JavaScriptProgramRuntime extends AbstractProgramRuntime {
} // JavaScriptProgramRuntime

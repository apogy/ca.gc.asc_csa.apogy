/**
 * Canadian Space Agency / Agence spatiale canadienne 2016 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript;

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyJavaScriptFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apogy Java Script Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ApogyCoreJavaScriptProgramsPackage#getApogyJavaScriptFacade()
 * @model
 * @generated
 */
public interface ApogyJavaScriptFacade extends EObject 
{
	/**
	 * The singleton of 
	 * @generated_NOT
	 */
	public ApogyJavaScriptFacade INSTANCE = ApogyJavaScriptFacadeImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create the default Java Script code template. This template includes the required header and main function.
	 * @param session The session in which the script is being defined.
	 * @param javaScriptProgram The JavaScriptProgram for which the script is being created.
	 * <!-- end-model-doc -->
	 * @model unique="false" sessionUnique="false" javaScriptProgramUnique="false"
	 * @generated
	 */
	String createJavaScriptCodeTemplate(InvocatorSession session, JavaScriptProgram javaScriptProgram);

} // ApogyJavaScriptFacade

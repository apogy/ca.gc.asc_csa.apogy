/**
 * Canadian Space Agency / Agence spatiale canadienne 2016 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ApogyCoreJavaScriptProgramsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyCoreJavaScriptPrograms' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Steve Monnier (steve.monnier@obeo.fr) \n     OBEO - Initial API and implementation\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' modelName='ApogyCoreJavaScriptPrograms' suppressGenModelAnnotations='false' publicConstructors='true' modelDirectory='/ca.gc.asc_csa.apogy.core.programs.javascript/src-generated' editDirectory='/ca.gc.asc_csa.apogy.core.programs.javascript.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.core.programs'"
 * @generated
 */
public interface ApogyCoreJavaScriptProgramsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "javascript";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.core.programs.javascript";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "javascript";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCoreJavaScriptProgramsPackage eINSTANCE = ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyCoreJavaScriptProgramsPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyJavaScriptFacadeImpl <em>Apogy Java Script Facade</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyJavaScriptFacadeImpl
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyCoreJavaScriptProgramsPackageImpl#getApogyJavaScriptFacade()
	 * @generated
	 */
	int APOGY_JAVA_SCRIPT_FACADE = 0;

	/**
	 * The number of structural features of the '<em>Apogy Java Script Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_JAVA_SCRIPT_FACADE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Create Java Script Code Template</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_JAVA_SCRIPT_FACADE___CREATE_JAVA_SCRIPT_CODE_TEMPLATE__INVOCATORSESSION_JAVASCRIPTPROGRAM = 0;

	/**
	 * The number of operations of the '<em>Apogy Java Script Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_JAVA_SCRIPT_FACADE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramImpl <em>Java Script Program</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramImpl
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyCoreJavaScriptProgramsPackageImpl#getJavaScriptProgram()
	 * @generated
	 */
	int JAVA_SCRIPT_PROGRAM = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM__NAME = ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAM__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM__DESCRIPTION = ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Started</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM__STARTED = ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAM__STARTED;

	/**
	 * The feature id for the '<em><b>Programs Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM__PROGRAMS_GROUP = ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAM__PROGRAMS_GROUP;

	/**
	 * The feature id for the '<em><b>Invocator Session</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM__INVOCATOR_SESSION = ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAM__INVOCATOR_SESSION;

	/**
	 * The feature id for the '<em><b>Operation Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM__OPERATION_CALLS = ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Script Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM__SCRIPT_PATH = ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Java Script Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_FEATURE_COUNT = ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAM_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Invocator Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM___GET_INVOCATOR_SESSION = ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAM_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Java Script Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_OPERATION_COUNT = ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAM_OPERATION_COUNT + 1;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramRuntimeImpl <em>Java Script Program Runtime</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramRuntimeImpl
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyCoreJavaScriptProgramsPackageImpl#getJavaScriptProgramRuntime()
	 * @generated
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME__NAME = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME__DESCRIPTION = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME__STATE = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME__STATE;

	/**
	 * The feature id for the '<em><b>Program</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME__PROGRAM = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME__PROGRAM;

	/**
	 * The number of structural features of the '<em>Java Script Program Runtime</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME_FEATURE_COUNT = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME___INIT = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME___INIT;

	/**
	 * The operation id for the '<em>Terminate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME___TERMINATE = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME___TERMINATE;

	/**
	 * The operation id for the '<em>Resume</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME___RESUME = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME___RESUME;

	/**
	 * The operation id for the '<em>Suspend</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME___SUSPEND = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME___SUSPEND;

	/**
	 * The operation id for the '<em>Step Into</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME___STEP_INTO = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME___STEP_INTO;

	/**
	 * The operation id for the '<em>Step Over</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME___STEP_OVER = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME___STEP_OVER;

	/**
	 * The operation id for the '<em>Step Return</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME___STEP_RETURN = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME___STEP_RETURN;

	/**
	 * The number of operations of the '<em>Java Script Program Runtime</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_RUNTIME_OPERATION_COUNT = ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramFactoryImpl <em>Java Script Program Factory</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramFactoryImpl
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyCoreJavaScriptProgramsPackageImpl#getJavaScriptProgramFactory()
	 * @generated
	 */
	int JAVA_SCRIPT_PROGRAM_FACTORY = 3;

	/**
	 * The number of structural features of the '<em>Java Script Program Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_FACTORY_FEATURE_COUNT = ApogyCoreInvocatorPackage.PROGRAM_FACTORY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Create Program</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_FACTORY___CREATE_PROGRAM = ApogyCoreInvocatorPackage.PROGRAM_FACTORY___CREATE_PROGRAM;

	/**
	 * The operation id for the '<em>Apply Settings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_FACTORY___APPLY_SETTINGS__PROGRAM_PROGRAMSETTINGS = ApogyCoreInvocatorPackage.PROGRAM_FACTORY___APPLY_SETTINGS__PROGRAM_PROGRAMSETTINGS;

	/**
	 * The operation id for the '<em>Create Program Runtime</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_FACTORY___CREATE_PROGRAM_RUNTIME__PROGRAM_PROGRAMSETTINGS = ApogyCoreInvocatorPackage.PROGRAM_FACTORY___CREATE_PROGRAM_RUNTIME__PROGRAM_PROGRAMSETTINGS;

	/**
	 * The number of operations of the '<em>Java Script Program Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_FACTORY_OPERATION_COUNT = ApogyCoreInvocatorPackage.PROGRAM_FACTORY_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.programs.javascript.ApogyJavaScriptFacade <em>Apogy Java Script Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apogy Java Script Facade</em>'.
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ApogyJavaScriptFacade
	 * @generated
	 */
	EClass getApogyJavaScriptFacade();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.ApogyJavaScriptFacade#createJavaScriptCodeTemplate(ca.gc.asc_csa.apogy.core.invocator.InvocatorSession, ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram) <em>Create Java Script Code Template</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Java Script Code Template</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ApogyJavaScriptFacade#createJavaScriptCodeTemplate(ca.gc.asc_csa.apogy.core.invocator.InvocatorSession, ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram)
	 * @generated
	 */
	EOperation getApogyJavaScriptFacade__CreateJavaScriptCodeTemplate__InvocatorSession_JavaScriptProgram();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram <em>Java Script Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Script Program</em>'.
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram
	 * @generated
	 */
	EClass getJavaScriptProgram();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram#getScriptPath <em>Script Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Script Path</em>'.
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram#getScriptPath()
	 * @see #getJavaScriptProgram()
	 * @generated
	 */
	EAttribute getJavaScriptProgram_ScriptPath();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgramRuntime <em>Java Script Program Runtime</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Script Program Runtime</em>'.
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgramRuntime
	 * @generated
	 */
	EClass getJavaScriptProgramRuntime();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgramFactory <em>Java Script Program Factory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Script Program Factory</em>'.
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgramFactory
	 * @generated
	 */
	EClass getJavaScriptProgramFactory();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCoreJavaScriptProgramsFactory getApogyCoreJavaScriptProgramsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyJavaScriptFacadeImpl <em>Apogy Java Script Facade</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyJavaScriptFacadeImpl
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyCoreJavaScriptProgramsPackageImpl#getApogyJavaScriptFacade()
		 * @generated
		 */
		EClass APOGY_JAVA_SCRIPT_FACADE = eINSTANCE.getApogyJavaScriptFacade();

		/**
		 * The meta object literal for the '<em><b>Create Java Script Code Template</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_JAVA_SCRIPT_FACADE___CREATE_JAVA_SCRIPT_CODE_TEMPLATE__INVOCATORSESSION_JAVASCRIPTPROGRAM = eINSTANCE.getApogyJavaScriptFacade__CreateJavaScriptCodeTemplate__InvocatorSession_JavaScriptProgram();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramImpl <em>Java Script Program</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramImpl
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyCoreJavaScriptProgramsPackageImpl#getJavaScriptProgram()
		 * @generated
		 */
		EClass JAVA_SCRIPT_PROGRAM = eINSTANCE.getJavaScriptProgram();

		/**
		 * The meta object literal for the '<em><b>Script Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JAVA_SCRIPT_PROGRAM__SCRIPT_PATH = eINSTANCE.getJavaScriptProgram_ScriptPath();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramRuntimeImpl <em>Java Script Program Runtime</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramRuntimeImpl
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyCoreJavaScriptProgramsPackageImpl#getJavaScriptProgramRuntime()
		 * @generated
		 */
		EClass JAVA_SCRIPT_PROGRAM_RUNTIME = eINSTANCE.getJavaScriptProgramRuntime();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramFactoryImpl <em>Java Script Program Factory</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.JavaScriptProgramFactoryImpl
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.impl.ApogyCoreJavaScriptProgramsPackageImpl#getJavaScriptProgramFactory()
		 * @generated
		 */
		EClass JAVA_SCRIPT_PROGRAM_FACTORY = eINSTANCE.getJavaScriptProgramFactory();

	}

} //ApogyCoreJavaScriptProgramsPackage

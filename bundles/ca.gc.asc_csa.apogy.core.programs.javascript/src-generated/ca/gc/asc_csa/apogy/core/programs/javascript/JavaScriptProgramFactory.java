/**
 * Canadian Space Agency / Agence spatiale canadienne 2016 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript;

import ca.gc.asc_csa.apogy.core.invocator.ProgramFactory;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Script Program Factory</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Javascript Factory
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ApogyCoreJavaScriptProgramsPackage#getJavaScriptProgramFactory()
 * @model
 * @generated
 */
public interface JavaScriptProgramFactory extends ProgramFactory {
} // JavaScriptProgramFactory

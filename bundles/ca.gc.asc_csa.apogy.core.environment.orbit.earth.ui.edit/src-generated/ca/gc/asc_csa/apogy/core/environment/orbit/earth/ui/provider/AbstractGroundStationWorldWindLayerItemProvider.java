/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.provider;


import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.provider.EarthSurfaceLocationWorldWindLayerItemProvider;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthFactory;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage;

/**
 * This is the item provider adapter for a {@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AbstractGroundStationWorldWindLayerItemProvider extends EarthSurfaceLocationWorldWindLayerItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractGroundStationWorldWindLayerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addReferenceAltitudePropertyDescriptor(object);
			addShowVisibilityCirclePropertyDescriptor(object);
			addShowVisibilityConePropertyDescriptor(object);
			addShowOutlinePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Reference Altitude feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReferenceAltitudePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractGroundStationWorldWindLayer_referenceAltitude_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractGroundStationWorldWindLayer_referenceAltitude_feature", "_UI_AbstractGroundStationWorldWindLayer_type"),
				 ApogyCoreEnvironmentOrbitEarthUIPackage.Literals.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 getString("_UI_VISUALSPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Show Visibility Circle feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShowVisibilityCirclePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractGroundStationWorldWindLayer_showVisibilityCircle_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractGroundStationWorldWindLayer_showVisibilityCircle_feature", "_UI_AbstractGroundStationWorldWindLayer_type"),
				 ApogyCoreEnvironmentOrbitEarthUIPackage.Literals.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Show Visibility Cone feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShowVisibilityConePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractGroundStationWorldWindLayer_showVisibilityCone_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractGroundStationWorldWindLayer_showVisibilityCone_feature", "_UI_AbstractGroundStationWorldWindLayer_type"),
				 ApogyCoreEnvironmentOrbitEarthUIPackage.Literals.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Show Outline feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShowOutlinePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractGroundStationWorldWindLayer_showOutline_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractGroundStationWorldWindLayer_showOutline_feature", "_UI_AbstractGroundStationWorldWindLayer_type"),
				 ApogyCoreEnvironmentOrbitEarthUIPackage.Literals.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	@Override
	public String getText(Object object) 
	{
		AbstractGroundStationWorldWindLayer layer = (AbstractGroundStationWorldWindLayer) object;
		
		String label = null;
		if(layer.getName() != null && layer.getName().length() > 0)
		{
			label = layer.getName();
		}
		else
		{
			label = getString("_UI_AbstractGroundStationWorldWindLayer_type") ;
		}
		
		String topSuffix = getAbstractWorldWindLayerText(layer);		
		if(topSuffix.length() > 0)
		{
			label += "(" + topSuffix + ", ";
		}
		else
		{
			label += "( ";
		}
		
		if(layer.getEarthSurfaceLocation() != null)
		{
			DecimalFormat format = new DecimalFormat("0.000");
			label += "lat " + format.format(Math.toDegrees(layer.getEarthSurfaceLocation().getLatitude())) + ", lon";
			label += format.format(Math.toDegrees(layer.getEarthSurfaceLocation().getLongitude())) + ")";
		}
		
		return label;				
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AbstractGroundStationWorldWindLayer.class)) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION:
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE:
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE:
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE:
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ApogyEarthEnvironmentUIPackage.Literals.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION,
				 ApogyCoreEnvironmentOrbitEarthFactory.eINSTANCE.createGroundStation()));
	}

}

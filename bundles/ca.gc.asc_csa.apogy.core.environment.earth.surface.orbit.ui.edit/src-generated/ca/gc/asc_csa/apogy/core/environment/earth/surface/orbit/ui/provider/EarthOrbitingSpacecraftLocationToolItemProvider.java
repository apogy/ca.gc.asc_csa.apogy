/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.provider;


import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;

import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import ca.gc.asc_csa.apogy.core.environment.earth.HorizontalCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool;

/**
 * This is the item provider adapter for a {@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EarthOrbitingSpacecraftLocationToolItemProvider extends EarthOrbitModelToolItemProvider 
{
	private DecimalFormat decimalFormat = new DecimalFormat("0.00");
	
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitingSpacecraftLocationToolItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addShowVectorPropertyDescriptor(object);
			addSpacecraftPositionPropertyDescriptor(object);
			addEarthOrbitingSpacecraftLocationToolNodePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Show Vector feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShowVectorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EarthOrbitingSpacecraftLocationTool_showVector_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EarthOrbitingSpacecraftLocationTool_showVector_feature", "_UI_EarthOrbitingSpacecraftLocationTool_type"),
				 ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SHOW_VECTOR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Spacecraft Position feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSpacecraftPositionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EarthOrbitingSpacecraftLocationTool_spacecraftPosition_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EarthOrbitingSpacecraftLocationTool_spacecraftPosition_feature", "_UI_EarthOrbitingSpacecraftLocationTool_type"),
				 ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION,
				 false,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Earth Orbiting Spacecraft Location Tool Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEarthOrbitingSpacecraftLocationToolNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EarthOrbitingSpacecraftLocationTool_earthOrbitingSpacecraftLocationToolNode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EarthOrbitingSpacecraftLocationTool_earthOrbitingSpacecraftLocationToolNode_feature", "_UI_EarthOrbitingSpacecraftLocationTool_type"),
				 ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns EarthOrbitingSpacecraftLocationTool.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/EarthOrbitingSpacecraftLocationTool"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	@Override
	public String getText(Object object) 
	{
		EarthOrbitingSpacecraftLocationTool tool = (EarthOrbitingSpacecraftLocationTool) object;
				
		String label = null;
		if(tool.getName() != null && tool.getName().length() > 0)
		{
			label = tool.getName();
		}
		else
		{
			label = getString("_UI_EarthOrbitingSpacecraftLocationTool_type"); 
		}
		
		String simple3DToolText = getSimple3DToolText(tool);		
		if(tool.isAutoUpdateEnabled())
		{
			if(simple3DToolText != null && simple3DToolText.length() > 0) 
			{
				simple3DToolText += ", auto update";	
			}
			else
			{
				simple3DToolText += "auto update";
			}
		}
		
		label += "(" + simple3DToolText;
		
		HorizontalCoordinates coords = tool.getSpacecraftPosition();
		if(coords!= null)
		{
			if(simple3DToolText != null && simple3DToolText.length() > 0) label += ", ";			
			
			double azimuth = coords.getAzimuth();
			if(azimuth < 0) azimuth += 2*Math.PI;
			
			label += "Az : " + decimalFormat.format(Math.toDegrees(azimuth)) + ", ";
			label += "El : " + decimalFormat.format(Math.toDegrees(coords.getAltitude()));
		}
		label += ")";
		
		return label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(EarthOrbitingSpacecraftLocationTool.class)) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SHOW_VECTOR:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}

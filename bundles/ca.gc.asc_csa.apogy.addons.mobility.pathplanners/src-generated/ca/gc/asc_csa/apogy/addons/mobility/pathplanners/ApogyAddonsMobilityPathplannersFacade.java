package ca.gc.asc_csa.apogy.addons.mobility.pathplanners;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.common.topology.GroupNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Path Planners Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Path Planner Facade.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.mobility.pathplanners.ApogyAddonsMobilityPathplannersPackage#getApogyAddonsMobilityPathplannersFacade()
 * @model
 * @generated
 */
public interface ApogyAddonsMobilityPathplannersFacade extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Circular Exclusion Zone.
	 * @param radius The zone radius.
	 * @param parent The parent node to which to attach the Circular Exclusion Zone.
	 * <!-- end-model-doc -->
	 * @model unique="false" radiusUnique="false"
	 *        radiusAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" parentUnique="false"
	 * @generated
	 */
	CircularExclusionZone createCircularExclusionZone(double radius, GroupNode parent);

} // ApogyAddonsMobilityPathplannersFacade

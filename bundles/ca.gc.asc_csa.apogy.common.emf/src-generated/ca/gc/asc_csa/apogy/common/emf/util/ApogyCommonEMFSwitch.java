/**
 * Copyright (c) 2016, 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf.util;

import ca.gc.asc_csa.apogy.common.emf.*;

import java.util.Comparator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage
 * @generated
 */
public class ApogyCommonEMFSwitch<T1> extends Switch<T1> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ApogyCommonEMFPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonEMFSwitch() {
		if (modelPackage == null) {
			modelPackage = ApogyCommonEMFPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T1 doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ApogyCommonEMFPackage.APOGY_COMMON_EMF_FACADE: {
				ApogyCommonEMFFacade apogyCommonEMFFacade = (ApogyCommonEMFFacade)theEObject;
				T1 result = caseApogyCommonEMFFacade(apogyCommonEMFFacade);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.EOBJECT_REFERENCE: {
				EObjectReference eObjectReference = (EObjectReference)theEObject;
				T1 result = caseEObjectReference(eObjectReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.NAMED: {
				Named named = (Named)theEObject;
				T1 result = caseNamed(named);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.DESCRIBED: {
				Described described = (Described)theEObject;
				T1 result = caseDescribed(described);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.TIMED: {
				Timed timed = (Timed)theEObject;
				T1 result = caseTimed(timed);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.SERVER: {
				Server server = (Server)theEObject;
				T1 result = caseServer(server);
				if (result == null) result = caseStartable(server);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.STARTABLE: {
				Startable startable = (Startable)theEObject;
				T1 result = caseStartable(startable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.DURATION: {
				Duration duration = (Duration)theEObject;
				T1 result = caseDuration(duration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.TIME_SOURCE: {
				TimeSource timeSource = (TimeSource)theEObject;
				T1 result = caseTimeSource(timeSource);
				if (result == null) result = caseNamed(timeSource);
				if (result == null) result = caseDescribed(timeSource);
				if (result == null) result = caseTimed(timeSource);
				if (result == null) result = caseDisposable(timeSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.FIXED_TIME_SOURCE: {
				FixedTimeSource fixedTimeSource = (FixedTimeSource)theEObject;
				T1 result = caseFixedTimeSource(fixedTimeSource);
				if (result == null) result = caseTimeSource(fixedTimeSource);
				if (result == null) result = caseNamed(fixedTimeSource);
				if (result == null) result = caseDescribed(fixedTimeSource);
				if (result == null) result = caseTimed(fixedTimeSource);
				if (result == null) result = caseDisposable(fixedTimeSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.CURRENT_TIME_SOURCE: {
				CurrentTimeSource currentTimeSource = (CurrentTimeSource)theEObject;
				T1 result = caseCurrentTimeSource(currentTimeSource);
				if (result == null) result = caseTimeSource(currentTimeSource);
				if (result == null) result = caseNamed(currentTimeSource);
				if (result == null) result = caseDescribed(currentTimeSource);
				if (result == null) result = caseTimed(currentTimeSource);
				if (result == null) result = caseDisposable(currentTimeSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.BROWSEABLE_TIME_SOURCE: {
				BrowseableTimeSource browseableTimeSource = (BrowseableTimeSource)theEObject;
				T1 result = caseBrowseableTimeSource(browseableTimeSource);
				if (result == null) result = caseTimeSource(browseableTimeSource);
				if (result == null) result = caseNamed(browseableTimeSource);
				if (result == null) result = caseDescribed(browseableTimeSource);
				if (result == null) result = caseTimed(browseableTimeSource);
				if (result == null) result = caseDisposable(browseableTimeSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.COLLECTION_TIMED_TIME_SOURCE: {
				CollectionTimedTimeSource collectionTimedTimeSource = (CollectionTimedTimeSource)theEObject;
				T1 result = caseCollectionTimedTimeSource(collectionTimedTimeSource);
				if (result == null) result = caseBrowseableTimeSource(collectionTimedTimeSource);
				if (result == null) result = caseTimeSource(collectionTimedTimeSource);
				if (result == null) result = caseNamed(collectionTimedTimeSource);
				if (result == null) result = caseDescribed(collectionTimedTimeSource);
				if (result == null) result = caseTimed(collectionTimedTimeSource);
				if (result == null) result = caseDisposable(collectionTimedTimeSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.DISPOSABLE: {
				Disposable disposable = (Disposable)theEObject;
				T1 result = caseDisposable(disposable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.FEATURE_NODE_ADAPTER: {
				FeatureNodeAdapter featureNodeAdapter = (FeatureNodeAdapter)theEObject;
				T1 result = caseFeatureNodeAdapter(featureNodeAdapter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.ABSTRACT_FEATURE_NODE: {
				AbstractFeatureNode abstractFeatureNode = (AbstractFeatureNode)theEObject;
				T1 result = caseAbstractFeatureNode(abstractFeatureNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.ABSTRACT_FEATURE_LIST_NODE: {
				AbstractFeatureListNode abstractFeatureListNode = (AbstractFeatureListNode)theEObject;
				T1 result = caseAbstractFeatureListNode(abstractFeatureListNode);
				if (result == null) result = caseAbstractFeatureNode(abstractFeatureListNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.ABSTRACT_FEATURE_TREE_NODE: {
				AbstractFeatureTreeNode abstractFeatureTreeNode = (AbstractFeatureTreeNode)theEObject;
				T1 result = caseAbstractFeatureTreeNode(abstractFeatureTreeNode);
				if (result == null) result = caseAbstractFeatureNode(abstractFeatureTreeNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.ABSTRACT_ROOT_NODE: {
				AbstractRootNode abstractRootNode = (AbstractRootNode)theEObject;
				T1 result = caseAbstractRootNode(abstractRootNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.TREE_ROOT_NODE: {
				TreeRootNode treeRootNode = (TreeRootNode)theEObject;
				T1 result = caseTreeRootNode(treeRootNode);
				if (result == null) result = caseAbstractFeatureTreeNode(treeRootNode);
				if (result == null) result = caseAbstractRootNode(treeRootNode);
				if (result == null) result = caseAbstractFeatureNode(treeRootNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.TREE_FEATURE_NODE: {
				TreeFeatureNode treeFeatureNode = (TreeFeatureNode)theEObject;
				T1 result = caseTreeFeatureNode(treeFeatureNode);
				if (result == null) result = caseAbstractFeatureTreeNode(treeFeatureNode);
				if (result == null) result = caseAbstractFeatureSpecifier(treeFeatureNode);
				if (result == null) result = caseAbstractFeatureNode(treeFeatureNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.LIST_ROOT_NODE: {
				ListRootNode listRootNode = (ListRootNode)theEObject;
				T1 result = caseListRootNode(listRootNode);
				if (result == null) result = caseAbstractFeatureListNode(listRootNode);
				if (result == null) result = caseAbstractRootNode(listRootNode);
				if (result == null) result = caseAbstractFeatureNode(listRootNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.LIST_FEATURE_NODE: {
				ListFeatureNode listFeatureNode = (ListFeatureNode)theEObject;
				T1 result = caseListFeatureNode(listFeatureNode);
				if (result == null) result = caseAbstractFeatureListNode(listFeatureNode);
				if (result == null) result = caseAbstractFeatureSpecifier(listFeatureNode);
				if (result == null) result = caseAbstractFeatureNode(listFeatureNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.ABSTRACT_FEATURE_SPECIFIER: {
				AbstractFeatureSpecifier abstractFeatureSpecifier = (AbstractFeatureSpecifier)theEObject;
				T1 result = caseAbstractFeatureSpecifier(abstractFeatureSpecifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.FEATURE_PATH_ADAPTER: {
				FeaturePathAdapter featurePathAdapter = (FeaturePathAdapter)theEObject;
				T1 result = caseFeaturePathAdapter(featurePathAdapter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.FEATURE_PATH_ADAPTER_ENTRY: {
				FeaturePathAdapterEntry featurePathAdapterEntry = (FeaturePathAdapterEntry)theEObject;
				T1 result = caseFeaturePathAdapterEntry(featurePathAdapterEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.ICOMPARATOR: {
				Comparator<?> iComparator = (Comparator<?>)theEObject;
				T1 result = caseIComparator(iComparator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.ECOMPARATOR: {
				EComparator<?> eComparator = (EComparator<?>)theEObject;
				T1 result = caseEComparator(eComparator);
				if (result == null) result = caseIComparator(eComparator);
				if (result == null) result = caseNamed(eComparator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.COMPOSITE_COMPARATOR: {
				CompositeComparator<?> compositeComparator = (CompositeComparator<?>)theEObject;
				T1 result = caseCompositeComparator(compositeComparator);
				if (result == null) result = caseEComparator(compositeComparator);
				if (result == null) result = caseIComparator(compositeComparator);
				if (result == null) result = caseNamed(compositeComparator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.EID_COMPARATOR: {
				EIdComparator<?> eIdComparator = (EIdComparator<?>)theEObject;
				T1 result = caseEIdComparator(eIdComparator);
				if (result == null) result = caseEComparator(eIdComparator);
				if (result == null) result = caseIComparator(eIdComparator);
				if (result == null) result = caseNamed(eIdComparator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.TIMED_COMPARATOR: {
				TimedComparator<?> timedComparator = (TimedComparator<?>)theEObject;
				T1 result = caseTimedComparator(timedComparator);
				if (result == null) result = caseEComparator(timedComparator);
				if (result == null) result = caseIComparator(timedComparator);
				if (result == null) result = caseNamed(timedComparator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.NAMED_COMPARATOR: {
				NamedComparator<?> namedComparator = (NamedComparator<?>)theEObject;
				T1 result = caseNamedComparator(namedComparator);
				if (result == null) result = caseEComparator(namedComparator);
				if (result == null) result = caseIComparator(namedComparator);
				if (result == null) result = caseNamed(namedComparator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.IFILTER: {
				IFilter<?> iFilter = (IFilter<?>)theEObject;
				T1 result = caseIFilter(iFilter);
				if (result == null) result = caseNamed(iFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.COMPOSITE_FILTER: {
				CompositeFilter<?> compositeFilter = (CompositeFilter<?>)theEObject;
				T1 result = caseCompositeFilter(compositeFilter);
				if (result == null) result = caseIFilter(compositeFilter);
				if (result == null) result = caseNamed(compositeFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.TIMED_BEFORE_FILTER: {
				TimedBeforeFilter<?> timedBeforeFilter = (TimedBeforeFilter<?>)theEObject;
				T1 result = caseTimedBeforeFilter(timedBeforeFilter);
				if (result == null) result = caseIFilter(timedBeforeFilter);
				if (result == null) result = caseNamed(timedBeforeFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.TIMED_AFTER_FILTER: {
				TimedAfterFilter<?> timedAfterFilter = (TimedAfterFilter<?>)theEObject;
				T1 result = caseTimedAfterFilter(timedAfterFilter);
				if (result == null) result = caseIFilter(timedAfterFilter);
				if (result == null) result = caseNamed(timedAfterFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFPackage.TIMED_COMPOSITE_FILTER: {
				TimedCompositeFilter<?> timedCompositeFilter = (TimedCompositeFilter<?>)theEObject;
				T1 result = caseTimedCompositeFilter(timedCompositeFilter);
				if (result == null) result = caseCompositeFilter(timedCompositeFilter);
				if (result == null) result = caseIFilter(timedCompositeFilter);
				if (result == null) result = caseNamed(timedCompositeFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseApogyCommonEMFFacade(ApogyCommonEMFFacade object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseEObjectReference(EObjectReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNamed(Named object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Described</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Described</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDescribed(Described object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTimed(Timed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseServer(Server object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Startable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Startable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStartable(Startable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Duration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Duration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDuration(Duration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTimeSource(TimeSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fixed Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fixed Time Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFixedTimeSource(FixedTimeSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Current Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Current Time Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCurrentTimeSource(CurrentTimeSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Browseable Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Browseable Time Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseBrowseableTimeSource(BrowseableTimeSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Collection Timed Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Collection Timed Time Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCollectionTimedTimeSource(CollectionTimedTimeSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Disposable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Disposable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDisposable(Disposable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Node Adapter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Node Adapter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFeatureNodeAdapter(FeatureNodeAdapter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Feature Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Feature Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAbstractFeatureNode(AbstractFeatureNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Feature List Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Feature List Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAbstractFeatureListNode(AbstractFeatureListNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Feature Tree Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Feature Tree Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAbstractFeatureTreeNode(AbstractFeatureTreeNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Root Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Root Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAbstractRootNode(AbstractRootNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tree Root Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tree Root Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTreeRootNode(TreeRootNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tree Feature Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tree Feature Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTreeFeatureNode(TreeFeatureNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>List Root Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>List Root Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseListRootNode(ListRootNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>List Feature Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>List Feature Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseListFeatureNode(ListFeatureNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Feature Specifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Feature Specifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAbstractFeatureSpecifier(AbstractFeatureSpecifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Path Adapter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Path Adapter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFeaturePathAdapter(FeaturePathAdapter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Path Adapter Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Path Adapter Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFeaturePathAdapterEntry(FeaturePathAdapterEntry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IComparator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IComparator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T> T1 caseIComparator(Comparator<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EComparator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EComparator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T> T1 caseEComparator(EComparator<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Comparator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Comparator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T> T1 caseCompositeComparator(CompositeComparator<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EId Comparator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EId Comparator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T extends EObject> T1 caseEIdComparator(EIdComparator<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed Comparator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed Comparator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T extends Timed> T1 caseTimedComparator(TimedComparator<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Comparator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Comparator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T extends Named> T1 caseNamedComparator(NamedComparator<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IFilter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IFilter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T> T1 caseIFilter(IFilter<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T> T1 caseCompositeFilter(CompositeFilter<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed Before Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed Before Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T extends Timed> T1 caseTimedBeforeFilter(TimedBeforeFilter<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed After Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed After Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T extends Timed> T1 caseTimedAfterFilter(TimedAfterFilter<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed Composite Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed Composite Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T extends Timed> T1 caseTimedCompositeFilter(TimedCompositeFilter<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T1 defaultCase(EObject object) {
		return null;
	}

} //ApogyCommonEMFSwitch

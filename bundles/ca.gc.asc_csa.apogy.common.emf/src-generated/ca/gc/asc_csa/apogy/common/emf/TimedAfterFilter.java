/**
 * Copyright (c) 2016, 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timed After Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 *  A Filter that matches dates after a specified date.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.TimedAfterFilter#isInclusive <em>Inclusive</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.TimedAfterFilter#getAfterDate <em>After Date</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getTimedAfterFilter()
 * @model
 * @generated
 */
public interface TimedAfterFilter<T extends Timed> extends IFilter<T> {
	/**
	 * Returns the value of the '<em><b>Inclusive</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inclusive</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Whether or not a Timed with perfect match to afterDate should be allowed thru.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Inclusive</em>' attribute.
	 * @see #setInclusive(boolean)
	 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getTimedAfterFilter_Inclusive()
	 * @model default="true" unique="false"
	 * @generated
	 */
	boolean isInclusive();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.TimedAfterFilter#isInclusive <em>Inclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inclusive</em>' attribute.
	 * @see #isInclusive()
	 * @generated
	 */
	void setInclusive(boolean value);

	/**
	 * Returns the value of the '<em><b>After Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>After Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The date defining the earliest date limit.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>After Date</em>' attribute.
	 * @see #setAfterDate(Date)
	 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getTimedAfterFilter_AfterDate()
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true'"
	 * @generated
	 */
	Date getAfterDate();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.TimedAfterFilter#getAfterDate <em>After Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>After Date</em>' attribute.
	 * @see #getAfterDate()
	 * @generated
	 */
	void setAfterDate(Date value);

} // TimedAfterFilter

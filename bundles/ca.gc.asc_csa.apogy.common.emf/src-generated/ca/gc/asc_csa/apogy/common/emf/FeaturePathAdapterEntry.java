/**
 * Copyright (c) 2016, 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf;

import org.eclipse.emf.common.notify.Adapter;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Path Adapter Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Class used to keep track of EObject and feature. Used by FeaturePathAdapter.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapterEntry#getNotifier <em>Notifier</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapterEntry#getFeature <em>Feature</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapterEntry#getAdapter <em>Adapter</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getFeaturePathAdapterEntry()
 * @model
 * @generated
 */
public interface FeaturePathAdapterEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Notifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Notifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The notifier.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Notifier</em>' reference.
	 * @see #setNotifier(EObject)
	 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getFeaturePathAdapterEntry_Notifier()
	 * @model
	 * @generated
	 */
	EObject getNotifier();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapterEntry#getNotifier <em>Notifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Notifier</em>' reference.
	 * @see #getNotifier()
	 * @generated
	 */
	void setNotifier(EObject value);

	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The feature of the notifier.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #setFeature(EStructuralFeature)
	 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getFeaturePathAdapterEntry_Feature()
	 * @model
	 * @generated
	 */
	EStructuralFeature getFeature();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapterEntry#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(EStructuralFeature value);

	/**
	 * Returns the value of the '<em><b>Adapter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adapter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The Adpater used.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Adapter</em>' attribute.
	 * @see #setAdapter(Adapter)
	 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getFeaturePathAdapterEntry_Adapter()
	 * @model unique="false" dataType="ca.gc.asc_csa.apogy.common.emf.Adapter"
	 * @generated
	 */
	Adapter getAdapter();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapterEntry#getAdapter <em>Adapter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Adapter</em>' attribute.
	 * @see #getAdapter()
	 * @generated
	 */
	void setAdapter(Adapter value);

} // FeaturePathAdapterEntry

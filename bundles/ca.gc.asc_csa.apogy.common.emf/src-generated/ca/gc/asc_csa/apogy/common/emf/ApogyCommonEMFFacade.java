package ca.gc.asc_csa.apogy.common.emf;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;

import javax.measure.unit.Unit;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import ca.gc.asc_csa.apogy.common.emf.impl.ApogyCommonEMFFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Facade for Common EMF.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade#getDateFormatString <em>Date Format String</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getApogyCommonEMFFacade()
 * @model
 * @generated
 */
public interface ApogyCommonEMFFacade extends EObject
{
	/**
	 * Returns the value of the '<em><b>Date Format String</b></em>' attribute.
	 * The default value is <code>"yyyy.MM.dd HH:mm:ss.SSS z"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The date format string used for representing date as string.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Date Format String</em>' attribute.
	 * @see #setDateFormatString(String)
	 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getApogyCommonEMFFacade_DateFormatString()
	 * @model default="yyyy.MM.dd HH:mm:ss.SSS z" unique="false"
	 * @generated
	 */
	String getDateFormatString();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade#getDateFormatString <em>Date Format String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date Format String</em>' attribute.
	 * @see #getDateFormatString()
	 * @generated
	 */
	void setDateFormatString(String value);

	public static ApogyCommonEMFFacade INSTANCE = ApogyCommonEMFFacadeImpl.getInstance();
	
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all available EClass currently installed.
	 * <!-- end-model-doc -->
	 * @model kind="operation" dataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EClass>" unique="false" many="false"
	 * @generated
	 */
  List<EClass> getAllAvailableEClasses();

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all the sub classes of the specified super class.  The result list contains
	 * only implementation classes (not abstract).
	 * @param superClass Super type of the classes to be found.
	 * @return List of sub classes.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EClass>" unique="false" many="false" superClassUnique="false"
	 * @generated
	 */
  List<EClass> getAllSubEClasses(EClass superClass);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Applies a filter to a list of EClasses and returns the list of EClass that passes the filter.
	 * @param eClasses The list of EClasses to filter.
	 * @param filter The filter to apply.
	 * @return The list of EClass that passes the filter. Never null, can be empty.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EClass>" unique="false" many="false" eClassesDataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EClass>" eClassesUnique="false" eClassesMany="false" filterDataType="ca.gc.asc_csa.apogy.common.emf.EClassFilter" filterUnique="false"
	 * @generated
	 */
  List<EClass> filterEClasses(List<EClass> eClasses, EClassFilter filter);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Applies a list of filters to a list of EClasses and returns the list of EClass that passes all the filtesr.
	 * @param eClasses The list of EClasses to filter.
	 * @param filters The list filters to apply.
	 * @return The list of EClass that passes all the filters. Never null, can be empty.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EClass>" unique="false" many="false" eClassesDataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EClass>" eClassesUnique="false" eClassesMany="false" filtersDataType="ca.gc.asc_csa.apogy.common.emf.List<ca.gc.asc_csa.apogy.common.emf.EClassFilter>" filtersUnique="false" filtersMany="false"
	 * @generated
	 */
  List<EClass> filterEClasses(List<EClass> eClasses, List<EClassFilter> filters);

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Finds the closest match in a {@link List} of {@link EClass}.
	 * @param eClass reference to the EClass to find the closest match.
	 * @param eClasses list of EClasses to search.
	 * @return {@link EClass} of the closest super class of the specified EClass.
	 * <!-- end-model-doc -->
	 * @model unique="false" eClassUnique="false" eClassesDataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EClass>" eClassesUnique="false" eClassesMany="false"
	 * @generated
	 */
	EClass findClosestMatch(EClass eClass, List<EClass> eClasses);

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the {@link EClass} that fits the specified fully qualified name.
	 * @param str Fully qualified name.
	 * @return Reference to the class or null there is no match.
	 * <!-- end-model-doc -->
	 * @model unique="false" strUnique="false"
	 * @generated
	 */
  EClass getEClass(String str);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Sorts a list of EClass alphabetically based on their Instance Class Name.
	 * @param eClasses The list of EClasses to filter.
	 * @return The eClasses sorted alphabetically.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.SortedSet<org.eclipse.emf.ecore.EClass>" unique="false" eClassesDataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EClass>" eClassesUnique="false" eClassesMany="false"
	 * @generated
	 */
  SortedSet<EClass> sortAlphabetically(List<EClass> eClasses);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the list of EOperation of a given EClass.
	 * @param eClass The EClass.
	 * @return The list of EOperation.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EOperation>" unique="false" many="false" eClassUnique="false"
	 * @generated
	 */
  List<EOperation> getAllAvailableEOperations(EClass eClass);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Sorts a list of EOperation alphabetically based on their name.
	 * @param eOperations The list of EOperation to filter.
	 * @return The eOperations sorted alphabetically.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.SortedSet<org.eclipse.emf.ecore.EOperation>" unique="false" eOperationsDataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EOperation>" eOperationsUnique="false" eOperationsMany="false"
	 * @generated
	 */
  SortedSet<EOperation> sortEOperationsAlphabetically(List<EOperation> eOperations);

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the documentation associated with a EAnnotation.
	 * @param eAnnotation The EAnnotation.
	 * @return The documentation string.
	 * <!-- end-model-doc -->
	 * @model unique="false" eAnnotationUnique="false"
	 * @generated
	 */
	String getDocumentation(EAnnotation eAnnotation);

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the documentation associated with a ETypedElement.
	 * @param eTypedElement The ETypedElement.
	 * @return The documentation string.
	 * <!-- end-model-doc -->
	 * @model unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  String getDocumentation(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the documentation associated with a EParameter, it includes units and ranges (if available).
	 * @param eParameter The EParameter.
	 * @return The documentation string.
	 * <!-- end-model-doc -->
	 * @model unique="false" eParameterUnique="false"
	 * @generated
	 */
	String getDocumentation(EParameter eParameter);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the documentation associated with an EClass.
	 * @param eClass The EClass.
	 * @return The documentation string.
	 * <!-- end-model-doc -->
	 * @model unique="false" eClassUnique="false"
	 * @generated
	 */
	String getDocumentation(EClass eClass);

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the engineering units string associated with a ETypedElement.
	 * @param eTypedElement The ETypedElement.
	 * @return The engineering units string, null if none is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  String getEngineeringUnitsAsString(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the Unit defined for a specified eTypedElement.
	 * @param eTypedElement The specified ETypedElement.
	 * @return the Unit, or null if none found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.Unit" unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  Unit<?> getEngineeringUnits(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the expected update rate for a given eTypedElement.
	 * @param eTypedElement The specified ETypedElement.
	 * @return The update rate, in seconds, -1 if none is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" eTypedElementUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='s'"
	 * @generated
	 */
  Double getValueUpdateRate(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the OCL expression used to determine if an ETypedElement is within Warning range.
	 * @param eTypedElement The specified ETypedElement.
	 * @return The OCL string.
	 * <!-- end-model-doc -->
	 * @model unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  String getWarningOCLExpression(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the OCL expression used to determine if an ETypedElement is within Alarm range.
	 * @param eTypedElement The specified ETypedElement.
	 * @return The OCL string.
	 * <!-- end-model-doc -->
	 * @model unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  String getAlarmOCLExpression(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the OCL expression used to determine if an ETypedElement is within OUt-Of-Range range.
	 * @param eTypedElement The specified ETypedElement.
	 * @return The OCL string.
	 * <!-- end-model-doc -->
	 * @model unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  String getOutOfRangeOCLExpression(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return the Warning minimum value for a given ETypedElement.
	 * @param eTypedElement The specified ETypedElement.
	 * @return The Warning minimum, null if none is found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.Number" unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  Number getWarningMinValue(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return the Warning maximum value for a given ETypedElement.
	 * @param eTypedElement The specified ETypedElement.
	 * @return The Warning maximum, null if none is found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.Number" unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  Number getWarningMaxValue(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return the Alarm minimum value for a given ETypedElement.
	 * @param eTypedElement The specified ETypedElement.
	 * @return The Alarm minimum, null if none is found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.Number" unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  Number getAlarmMinValue(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return the Alarm maximum value for a given ETypedElement.
	 * @param eTypedElement The specified ETypedElement.
	 * @return The Alarm maximum, null if none is found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.Number" unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  Number getAlarmMaxValue(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return the Out-Of-Range minimum value for a given ETypedElement.
	 * @param eTypedElement The specified ETypedElement.
	 * @return The Out-Of-Range minimum, null if none is found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.Number" unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  Number getOutOfRangeMinValue(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return the Out-Of-Range maximum value for a given ETypedElement.
	 * @param eTypedElement The specified ETypedElement.
	 * @return The Out-Of-Range maximum, null if none is found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.Number" unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  Number getOutOfRangeMaxValue(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the Ranges associated with a given value for a specified ETypedElement.
	 * @param eTypedElement The specified ETypedElement.
	 * @param value The value of the specified ETypedElement.
	 * @return The Ranges in which the value falls, UNKNOWN if none is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" eTypedElementUnique="false" valueUnique="false"
	 * @generated
	 */
  Ranges getRange(ETypedElement eTypedElement, Object value);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets a string that describes a a given ETypedElement.
	 * @param eTypedElement The specified ETypedElement.
	 * @return A description string. Includes ranges values.
	 * <!-- end-model-doc -->
	 * @model unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  String getFullDescription(ETypedElement eTypedElement);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method that return a string representing the
	 * ancestors of the specified AbstractFeatureNode.
	 * <!-- end-model-doc -->
	 * @model unique="false" abstractFeatureNodeUnique="false"
	 * @generated
	 */
  String getAncestriesString(AbstractFeatureNode abstractFeatureNode);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method that return the root of the tree for a
	 * specified AbstractFeatureNode.
	 * <!-- end-model-doc -->
	 * @model unique="false" abstractFeatureNodeUnique="false"
	 * @generated
	 */
  AbstractRootNode getFeatureRoot(AbstractFeatureNode abstractFeatureNode);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method that return the list of ancestor of a
	 * specified AbstractFeatureNode, from FeatureRoot to
	 * (and including) the specified AbstractFeatureNode.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.List<ca.gc.asc_csa.apogy.common.emf.AbstractFeatureNode>" unique="false" many="false" abstractFeatureNodeUnique="false"
	 * @generated
	 */
  List<AbstractFeatureNode> getAncestries(AbstractFeatureNode abstractFeatureNode);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method that return the list of descendants of a
	 * specified AbstractFeatureNode, excluding the specified
	 * AbstractFeatureNode.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.List<ca.gc.asc_csa.apogy.common.emf.AbstractFeatureNode>" unique="false" many="false" abstractFeatureNodeUnique="false"
	 * @generated
	 */
  List<AbstractFeatureNode> getDescendants(AbstractFeatureNode abstractFeatureNode);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method that return the list of available child
	 * EStructuralFeature type for a given Node.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EStructuralFeature>" unique="false" many="false" abstractFeatureNodeUnique="false"
	 * @generated
	 */
  List<EStructuralFeature> getChildEStructuralFeatures(AbstractFeatureNode abstractFeatureNode);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method that resolve the value of a FeatureNode
	 * for a specified source EObjet.
	 * <!-- end-model-doc -->
	 * @model unique="false" sourceObjectUnique="false" abstractFeatureNodeUnique="false"
	 * @generated
	 */
  Object resolve(EObject sourceObject, AbstractFeatureNode abstractFeatureNode);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Sets the current of a feature of a given EObject to a specified one.
	 * @param sourceObject The object containing the feature to set.
	 * @param abstractFeatureNode The AbstractFeatureNode specifying the feature to set.
	 * @param value The specified value.
	 * <!-- end-model-doc -->
	 * @model sourceObjectUnique="false" abstractFeatureNodeUnique="false" valueUnique="false"
	 * @generated
	 */
  void setValue(EObject sourceObject, AbstractFeatureNode abstractFeatureNode, Object value);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method that tells whether or not the
	 * FeatureNode is currently accessible for
	 * the specified source EObject.
	 * <!-- end-model-doc -->
	 * @model unique="false" sourceObjectUnique="false" abstractFeatureNodeUnique="false"
	 * @generated
	 */
  boolean isResolved(EObject sourceObject, AbstractFeatureNode abstractFeatureNode);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the leaf (node with nod child) for a given ListRootNode.
	 * @param listRootNode The ListRootNode for which to find the leaf.
	 * @return The leaf node.
	 * <!-- end-model-doc -->
	 * @model unique="false" listRootNodeUnique="false"
	 * @generated
	 */
  AbstractFeatureListNode getLeaf(ListRootNode listRootNode);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the IFile associated with a specified Resource.
	 * @param resource The specified Ressource.
	 * @return The IFile, null if none is found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.IFile" unique="false" resourceDataType="ca.gc.asc_csa.apogy.common.emf.Resource" resourceUnique="false"
	 * @generated
	 */
  IFile getFile(Resource resource);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the content contained in the resource.
	 * @param uri Resource uri.
	 * @return Reference to the content.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.EList<org.eclipse.emf.ecore.EObject>" unique="false" many="false" uriDataType="ca.gc.asc_csa.apogy.common.emf.URI" uriUnique="false"
	 * @generated
	 */
	EList<EObject> getContent(URI uri);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns a sorted set of Timed elements.
	 * @param timedCollection A Collection of Timed elements.
	 * @return The timed elements sorted by date.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.SortedSet<ca.gc.asc_csa.apogy.common.emf.Timed>" unique="false" timedCollectionDataType="ca.gc.asc_csa.apogy.common.emf.Collection<ca.gc.asc_csa.apogy.common.emf.Timed>" timedCollectionUnique="false"
	 * @generated
	 */
	SortedSet<Timed> sortTimed(Collection<Timed> timedCollection);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the time span, in seconds, of a collection of Timed elements.
	 * @param timedCollection A Collection of Timed elements.
	 * @return The time difference between the latest and earliest Timed element in the collection, in seconds.
	 * <!-- end-model-doc -->
	 * @model unique="false" timedCollectionDataType="ca.gc.asc_csa.apogy.common.emf.Collection<ca.gc.asc_csa.apogy.common.emf.Timed>" timedCollectionUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='s'"
	 * @generated
	 */
	double getTimeSpan(Collection<Timed> timedCollection);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return an EObject unique ID.
	 * @param eObject The EObject.
	 * @return the unique ID, null if none is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" eObjectUnique="false"
	 * @generated
	 */
	String getID(EObject eObject);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Searches the ResourceSet for an EObject with the specified id.
	 * @param resourceSet Reference to the resourceSet
	 * @param id Object identifier.
	 * <!-- end-model-doc -->
	 * @model unique="false" resourceSetDataType="ca.gc.asc_csa.apogy.common.emf.ResourceSet" resourceSetUnique="false" idUnique="false"
	 * @generated
	 */
	EObject getEObjectById(ResourceSet resourceSet, String id);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Searches the content of a specified EObject for children that are of type or sub type of a EClass.
	 * @param rootContainer The root EObject to search from.
	 * @param eClass The EClass.
	 * @return The list of EObject.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EObject>" unique="false" many="false" rootContainerUnique="false" eClassUnique="false"
	 * @generated
	 */
	List<EObject> getEObjectsByType(EObject rootContainer, EClass eClass);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Generates and returns a default name for a specific feature container.
	 * @param eContainer The instance of the container.
	 * @param eObject The instance of the object to name.
	 * @param typedElement The typed element containing or referencing the EObject.
	 * @return A unique name.
	 * <!-- end-model-doc -->
	 * @model unique="false" eContainerUnique="false" eObjectUnique="false" typedElementUnique="false"
	 * @generated
	 */
	String getDefaultName(EObject eContainer, EObject eObject, ETypedElement typedElement);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all the child classes of the specified parent class. The result list contains
	 * only implementation classes (not abstract).
	 * @param parentEClass Type of the class to find child classes.
	 * @return List of child classes.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.List<org.eclipse.emf.ecore.EClass>" unique="false" many="false" parentEClassUnique="false"
	 * @generated
	 */
	List<EClass> getChildEClasses(EClass parentEClass);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns a list of all the object's child EReferences that are either empty or a list
	 * @param eObject The parent object
	 * @return List of EReferences that are either empty or a list
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.EList<org.eclipse.emf.ecore.EReference>" unique="false" many="false" eObjectUnique="false"
	 * @generated
	 */
	EList<EReference> getSettableEReferences(EObject eObject);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the string representation of the list of {@link Named}.
	 * @param nameds List of named.
	 * @param separator String used to separate the nameds.
	 * @return Reference to the string representation.
	 * <!-- end-model-doc -->
	 * @model unique="false" namedsDataType="ca.gc.asc_csa.apogy.common.emf.ListNamed" namedsUnique="false" separatorUnique="false"
	 * @generated
	 */
	String toString(List<? extends Named> nameds, String separator);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Formats a specified Date as string. Uses the format defined in attribute dateFormatString.
	 * @param date The specified date.
	 * @return A string representation of the specified date.
	 * <!-- end-model-doc -->
	 * @model unique="false" dateUnique="false"
	 * @generated
	 */
	String format(Date date);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Searches for {@link Startable} in the containment hierarchy of an {@link EObject} and stops them if they are running.
	 * @param root
	 * <!-- end-model-doc -->
	 * @model rootUnique="false"
	 * @generated
	 */
	void stopAllStartables(EObject root);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Serializes an {@link EObject} to obtain a {@link String}.
	 * This methods uses a copy of the {@link EObject}, so it will not be removed for it's ResourceSet.
	 * 
	 * @param eObject reference to the EObject to serialize.
	 * @param id id to use as id for the serialized resource. Cannot be null.
	 * @return The EObject serialized.
	 * <!-- end-model-doc -->
	 * @model unique="false" eObjectUnique="false" uriIDUnique="false"
	 * @generated
	 */
	String serializeEObject(EObject eObject, String uriID);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Deserializes a {@link String} to obtain an {@link EObject}.
	 * 
	 * @param eObject reference to the EObject to serialize.
	 * @param id id to use as id for the serialized resource. Cannot be null.
	 * @return The EObject
	 * <!-- end-model-doc -->
	 * @model unique="false" strUnique="false" uriIDUnique="false"
	 * @generated
	 */
	EObject deserializeString(String str, String uriID);

} // ApogyCommonEMFFacade

/**
 * Copyright (c) 2016, 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf;

import java.util.Collection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IFilter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 *  -------------------------------------------------------------------------
 * Filters
 * -------------------------------------------------------------------------
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getIFilter()
 * @model abstract="true"
 * @generated
 */
public interface IFilter<T> extends Named {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Determines whether or not a specified object should pass through the filter.
	 * @param object The specified object.
	 * @return True if the object satisfies the filter conditions, false otherwise.
	 * <!-- end-model-doc -->
	 * @model unique="false" objectUnique="false"
	 * @generated
	 */
	boolean matches(T object);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Filters a list of objects
	 * @param objects The list of object to filter.
	 * @return The list of objects that passed the filter. Never null, but can be empty.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.Collection<T>" unique="false" objectsDataType="ca.gc.asc_csa.apogy.common.emf.Collection<T>" objectsUnique="false"
	 * @generated
	 */
	Collection<T> filter(Collection<T> objects);

} // IFilter

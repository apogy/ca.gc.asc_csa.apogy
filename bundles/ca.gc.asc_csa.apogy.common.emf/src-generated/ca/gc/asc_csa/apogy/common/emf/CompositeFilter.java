/**
 * Copyright (c) 2016, 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A specialization of Filter that makes use of a list of Filter.
 * If the list of filter is empty, matches(T object) always returns true.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.CompositeFilter#getFilterChainType <em>Filter Chain Type</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.CompositeFilter#getFilters <em>Filters</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getCompositeFilter()
 * @model
 * @generated
 */
public interface CompositeFilter<T> extends IFilter<T> {
	/**
	 * Returns the value of the '<em><b>Filter Chain Type</b></em>' attribute.
	 * The default value is <code>"AND"</code>.
	 * The literals are from the enumeration {@link ca.gc.asc_csa.apogy.common.emf.CompositeFilterType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The type of chain.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Filter Chain Type</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.common.emf.CompositeFilterType
	 * @see #setFilterChainType(CompositeFilterType)
	 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getCompositeFilter_FilterChainType()
	 * @model default="AND" unique="false"
	 * @generated
	 */
	CompositeFilterType getFilterChainType();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.CompositeFilter#getFilterChainType <em>Filter Chain Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter Chain Type</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.common.emf.CompositeFilterType
	 * @see #getFilterChainType()
	 * @generated
	 */
	void setFilterChainType(CompositeFilterType value);

	/**
	 * Returns the value of the '<em><b>Filters</b></em>' containment reference list.
	 * The list contents are of type {@link ca.gc.asc_csa.apogy.common.emf.IFilter}&lt;T>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The list of filter used.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Filters</em>' containment reference list.
	 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getCompositeFilter_Filters()
	 * @model containment="true"
	 * @generated
	 */
	EList<IFilter<T>> getFilters();

} // CompositeFilter

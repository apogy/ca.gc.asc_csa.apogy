/**
 * Copyright (c) 2016, 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf;

import java.util.Comparator;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Comparator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Defines a composite comparator that uses a list of Comparator to compare two objects.
 * This comparator compare(T o1, T o2) method iterates over it list of comparator until
 * one that does not return equality is found.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.CompositeComparator#getComparators <em>Comparators</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getCompositeComparator()
 * @model
 * @generated
 */
public interface CompositeComparator<T> extends EComparator<T> {
	/**
	 * Returns the value of the '<em><b>Comparators</b></em>' containment reference list.
	 * The list contents are of type {@link java.util.Comparator}&lt;T>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The list of comparator used.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Comparators</em>' containment reference list.
	 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#getCompositeComparator_Comparators()
	 * @model type="ca.gc.asc_csa.apogy.common.emf.IComparator<T>" containment="true" required="true"
	 * @generated
	 */
	EList<Comparator<T>> getComparators();

} // CompositeComparator

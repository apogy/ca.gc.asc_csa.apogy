/**
 * Copyright (c) 2016, 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf.impl;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.Timed;
import ca.gc.asc_csa.apogy.common.emf.TimedBeforeFilter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timed Before Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.impl.TimedBeforeFilterImpl#isInclusive <em>Inclusive</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.impl.TimedBeforeFilterImpl#getBeforeDate <em>Before Date</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimedBeforeFilterImpl<T extends Timed> extends IFilterImpl<T> implements TimedBeforeFilter<T> {
	/**
	 * The default value of the '{@link #isInclusive() <em>Inclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInclusive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INCLUSIVE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isInclusive() <em>Inclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInclusive()
	 * @generated
	 * @ordered
	 */
	protected boolean inclusive = INCLUSIVE_EDEFAULT;

	/**
	 * The default value of the '{@link #getBeforeDate() <em>Before Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBeforeDate()
	 * @generated_NOT
	 * @ordered
	 */
	protected static final Date BEFORE_DATE_EDEFAULT = new Date();

	/**
	 * The cached value of the '{@link #getBeforeDate() <em>Before Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBeforeDate()
	 * @generated
	 * @ordered
	 */
	protected Date beforeDate = BEFORE_DATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimedBeforeFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFPackage.Literals.TIMED_BEFORE_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInclusive() {
		return inclusive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInclusive(boolean newInclusive) {
		boolean oldInclusive = inclusive;
		inclusive = newInclusive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFPackage.TIMED_BEFORE_FILTER__INCLUSIVE, oldInclusive, inclusive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getBeforeDate() {
		return beforeDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBeforeDate(Date newBeforeDate) {
		Date oldBeforeDate = beforeDate;
		beforeDate = newBeforeDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFPackage.TIMED_BEFORE_FILTER__BEFORE_DATE, oldBeforeDate, beforeDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public boolean matches(Timed object) 
	{
		if(isInclusive())
		{
			return object.getTime().getTime() <= getBeforeDate().getTime();
		}
		else
		{
			return object.getTime().getTime() < getBeforeDate().getTime();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonEMFPackage.TIMED_BEFORE_FILTER__INCLUSIVE:
				return isInclusive();
			case ApogyCommonEMFPackage.TIMED_BEFORE_FILTER__BEFORE_DATE:
				return getBeforeDate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonEMFPackage.TIMED_BEFORE_FILTER__INCLUSIVE:
				setInclusive((Boolean)newValue);
				return;
			case ApogyCommonEMFPackage.TIMED_BEFORE_FILTER__BEFORE_DATE:
				setBeforeDate((Date)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFPackage.TIMED_BEFORE_FILTER__INCLUSIVE:
				setInclusive(INCLUSIVE_EDEFAULT);
				return;
			case ApogyCommonEMFPackage.TIMED_BEFORE_FILTER__BEFORE_DATE:
				setBeforeDate(BEFORE_DATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFPackage.TIMED_BEFORE_FILTER__INCLUSIVE:
				return inclusive != INCLUSIVE_EDEFAULT;
			case ApogyCommonEMFPackage.TIMED_BEFORE_FILTER__BEFORE_DATE:
				return BEFORE_DATE_EDEFAULT == null ? beforeDate != null : !BEFORE_DATE_EDEFAULT.equals(beforeDate);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (inclusive: ");
		result.append(inclusive);
		result.append(", beforeDate: ");
		result.append(beforeDate);
		result.append(')');
		return result.toString();
	}

} //TimedBeforeFilterImpl

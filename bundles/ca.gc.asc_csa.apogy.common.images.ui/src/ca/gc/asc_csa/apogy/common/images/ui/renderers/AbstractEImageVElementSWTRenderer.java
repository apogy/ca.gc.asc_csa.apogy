/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.images.ui.renderers;

import javax.inject.Inject;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.model.VElement;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.converters.ApogyCommonConvertersFacade;
import ca.gc.asc_csa.apogy.common.converters.IFileExporter;
import ca.gc.asc_csa.apogy.common.converters.ui.wizards.ExportToFileWizard;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.renderers.AbstractCustomElementSWTRenderer;
import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.images.ui.elements.AbstractEImageVElement;

public class AbstractEImageVElementSWTRenderer extends AbstractCustomElementSWTRenderer 
{
	private ImageData imageData;
	private AbstractEImage abstractEImage;

	@Inject
	public AbstractEImageVElementSWTRenderer(VElement element, ViewModelContext viewModelContext, ReportService reportService) 
	{
		super((AbstractEImageVElement) element, viewModelContext, reportService);

		EObject object = viewModelContext.getDomainModel();
		if (object instanceof AbstractEImage) 
		{
			abstractEImage = (AbstractEImage) object;
			imageData = EImagesUtilities.INSTANCE.convertToImageData(abstractEImage.asBufferedImage());
		}
		else if(element instanceof AbstractEImageVElement)
		{
			AbstractEImageVElement abstractEImageVElement = (AbstractEImageVElement) element;
			abstractEImage = abstractEImageVElement.getAbstractEImage();
			imageData = EImagesUtilities.INSTANCE.convertToImageData(abstractEImage.asBufferedImage());
		}
		
	}

	@Override
	protected Control createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.BORDER);
		top.setLayout(new GridLayout(1, false));

		// The Image.
		Label label = new Label(top, SWT.None);		
		label.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TRANSPARENT));
		label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
				
		if (imageData != null) 
		{
			ImageData displayedImageData = (ImageData) imageData.clone();
			Image image = new Image(Display.getCurrent(), displayedImageData);
			label.setImage(image);
		}

		// The Export Button
		IFileExporter iFileExporter = ApogyCommonConvertersFacade.INSTANCE.getIFileExporter(abstractEImage);
		if(iFileExporter != null)
		{
			Button exportButton = new Button(top, SWT.PUSH);
			exportButton.setText("Export Image To File...");
			exportButton.addSelectionListener(new SelectionListener() 
			{		
				@Override
				public void widgetSelected(SelectionEvent arg0) 
				{					
					ExportToFileWizard wizard = new ExportToFileWizard(iFileExporter, abstractEImage);
					WizardDialog dialog = new WizardDialog(top.getShell(), wizard);
					dialog.open();	
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		return top;
	}

	@Override
	protected String getLabelText() {
		return "Image";
	}

}

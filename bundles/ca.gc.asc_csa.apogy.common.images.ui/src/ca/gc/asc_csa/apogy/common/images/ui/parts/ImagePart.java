/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.images.ui.parts;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.converters.ApogyCommonConvertersFacade;
import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.images.ui.composites.SWTImageCanvas;

public class ImagePart 
{
	public static String ID = "ca.gc.asc_csa.apogy.rcp.part.imageview";
	
	@Inject
	protected EPartService ePartService;
	
	@Inject
	protected ESelectionService selectionService;
	
	private ISelectionListener selectionListener;
	
	private String partLabel = "";
	private Composite parentComposite; 	
	private SWTImageCanvas imageCanvas;
	
	protected AbstractEImage displayedAbstractEImage;
		
	@PostConstruct
	public void createContent(Composite parent)
	{
		parentComposite = parent;
		parentComposite.setLayout(new FillLayout());		
		
		imageCanvas = new SWTImageCanvas(parent, SWT.NONE);	
		parentComposite.layout();
		
		// Gets the initial label of the part.
		MPart part = getPart();
		if(part != null) partLabel = part.getLabel();
		
		// Register to the selection service.
		selectionService.addSelectionListener(getISelectionListener());				
	}
	
	@PreDestroy
	public void dispose()
	{
		selectionService.removeSelectionListener(getISelectionListener());
	}
		
	public AbstractEImage getAbstractEImage() {
		return displayedAbstractEImage;
	}

	private MPart getPart()
	{
		return ePartService.findPart(ID);
	}
	
	private void updatePartLabel(String newLabel)
	{
		MPart part = getPart();
		if(part != null)
		{
			part.setLabel(newLabel);
		}
	}
	
	private void updateImage(AbstractEImage abstractEImage)
	{
		this.displayedAbstractEImage = abstractEImage;
		
		if(abstractEImage != null)
		{		
			String height = Integer.toString(abstractEImage.getHeight());
			String width = Integer.toString(abstractEImage.getWidth());
			String suffix =  "(" + width + " w x " + height + " h)";
			updatePartLabel(partLabel + " " + suffix);
							
			ImageData imageData = EImagesUtilities.INSTANCE.convertToImageData(abstractEImage.asBufferedImage());
			if (imageData != null)
			{
				imageCanvas.setImageData(imageData);
			}
		}		
	}
	
	private ISelectionListener getISelectionListener()
	{
		if(selectionListener == null)
		{			
			selectionListener = new ISelectionListener() 
			{			
				@Override
				public void selectionChanged(MPart part, Object selection) 
				{
					if(selection instanceof AbstractEImage)
					{
						AbstractEImage abstractEImage = (AbstractEImage) selection;
						updateImage(abstractEImage);
					}					
					else
					{
						// Call Converters.
						AbstractEImage abstractEImage = (AbstractEImage) ApogyCommonConvertersFacade.INSTANCE.convert(selection, AbstractEImage.class);
						updateImage(abstractEImage);
					}
				}
			};				
		}
		return selectionListener;
	}

}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.images.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.ui.parts.ImagePart;

public class ImagePartExportAbstractEImageHandler 
{	
	@CanExecute
	public boolean canExecute(MPart part){
		ImagePart imagePart = (ImagePart) part.getObject();						
		AbstractEImage image = imagePart.getAbstractEImage();
		return image != null;
	}
	
	@Execute
	public void execute(MPart part)
	{
		if(part.getObject() instanceof ImagePart)
		{
			ImagePart imagePart = (ImagePart) part.getObject();						
			AbstractEImage image = imagePart.getAbstractEImage();
			
			ExportAbstractEImageHandler handler = new ExportAbstractEImageHandler();
			handler.execute(image);					
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.images.ui.elements;

import org.eclipse.emf.ecp.view.spi.model.impl.VContainedElementImpl;

import ca.gc.asc_csa.apogy.common.images.AbstractEImage;

public class AbstractEImageVElement extends VContainedElementImpl 
{
	private AbstractEImage abstractEImage;
	

	public AbstractEImageVElement(AbstractEImage abstractEImage) {
		super();
		
		setName("Image");
		this.abstractEImage = abstractEImage;
	}

	public AbstractEImage getAbstractEImage() {
		return abstractEImage;
	}
			
}

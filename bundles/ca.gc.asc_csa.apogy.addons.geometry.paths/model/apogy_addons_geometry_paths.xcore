// *****************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
//     Pierre Allard - initial API and implementation
//     Regent L'Archeveque
//        
// SPDX-License-Identifier: EPL-1.0
// *****************************************************************************

@GenModel(prefix="ApogyAddonsGeometryPaths",
	      childCreationExtenders="true",
	      extensibleProviderFactory="true",
	      multipleEditorPages="false",
	      copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Pierre Allard - initial API and implementation
     Regent L'Archeveque
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",
	      modelName="ApogyAddonsGeometryPaths",
	      suppressGenModelAnnotations="false",
		  dynamicTemplates="true", 
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.addons.geometry.paths/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.addons.geometry.paths.edit/src-generated")

package ca.gc.asc_csa.apogy.addons.geometry.paths

import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianAxis
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianCoordinatesSet
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPositionCoordinates
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangularMesh
import ca.gc.asc_csa.apogy.common.math.Matrix4x4
import ca.gc.asc_csa.apogy.common.processors.Processor
import ca.gc.asc_csa.apogy.common.topology.GroupNode
import ca.gc.asc_csa.apogy.common.topology.Node

type List<T> wraps java.util.List

/**
 * Mode used for end point generation of splines.
 */
enum SplineEndControlPointGenerationMode
{
	AUTO_CTRL_POINTS_NONE as "AUTO_CTRL_POINTS_NONE" = 0
	AUTO_CTRL_POINTS_DUPLICATE_ENDNODES as "AUTO_CTRL_POINTS_DUPLICATE_ENDNODES" = 1
	AUTO_CTRL_POINTS_REFLECTION as "AUTO_CTRL_POINTS_REFLECTION" = 2
	AUTO_CTRL_POINTS_CLOSE_LOOPS as "AUTO_CTRL_POINTS_CLOSE_LOOPS" = 3
}

/**
 * Defines a waypoint.
 */
class WayPoint extends CartesianPositionCoordinates, GroupNode
{
	
}

/**
 * Base class of paths.
 */
class Path extends Node
{
	
}

/**
 * Class that defines a path as a series CartesianPositionCoordinates.
 */
class WayPointPath extends CartesianCoordinatesSet, Path
{
	/**
	 * The length of the path. This is the sum of the liner distance between its points.
	 */
	@GenModel(notify="true", apogy_units = "m")
	readonly transient derived double length = "0.0"
	
	/**
	 * Returns the start point of the path.
	 * @return The start point of the path, may be null of path is empty.
	 */
	op CartesianPositionCoordinates getStartPoint()
	
	/**
	 * Returns the end point of the path.
	 * @return The end point of the path, may be null of path is empty.
	 */
	op CartesianPositionCoordinates getEndPoint()
}

/**
 * An interpolator for WayPointPath. It generates a new WayPointPath by interpolating points from an original WayPointPath.
 */
abstract class WayPointPathInterpolator extends Processor<WayPointPath, WayPointPath>
{	
}

/**
 * Fits a spline on the specified path using Catmull-Rom and generates an
 * interpolated path using the specified maximum point to point distance.
 * The original waypoints are included in the resulting interpolated path.
 */
class CatmullRomWayPointPathInterpolator extends WayPointPathInterpolator
{
	/**
	 * The tension parameter for CatmullRom.
	 */
	double tension = "0.5"
	
	/**
	 * The maximum distance between the interpolated points.
	 */
	@GenModel(apogy_units="m")
	double maximumWayPointsDistance = "1.0"
	
	/**
	 * The mode to be used for end point control.
	 */
	SplineEndControlPointGenerationMode endControlPointGenerationMode = "AUTO_CTRL_POINTS_DUPLICATE_ENDNODES"
}

/**
 * A WayPointPathInterpolator that generate a new WayPointPath by linearly interpolating along the segment 
 * between points in the original WayPointPath. 
 */
class SegmentWayPointPathInterpolator extends WayPointPathInterpolator
{
	/**
	 * The maximum distance between the interpolated points.
	 */
	@GenModel(apogy_units="m")
	double maximumDistanceInterval = "1.0"
}

/**
 * Base class for filters used to filter WayPointPath. Such filter create a new WayPointPath that is a 
 * filtered version of the original WayPointPath.
 */
abstract class WayPointPathFilter extends Processor<WayPointPath, WayPointPath>
{
}

/**
 * Filter that removes points that are closer together than a specified minimum distance.
 */
class MinimumDistanceFilter extends WayPointPathFilter
{
	/**
	 * The minimum distance between the filtered points.
	 */
	@GenModel(apogy_units="m")
	double minimumDistance
}

/**
 * Filter that filter and interpolate a WayPointPath to attempt to have points at regular distance.
 */
class UniformDistanceWayPointPathInterpolator extends WayPointPathFilter
{
	/**
	 * The target distance between the filtered points.
	 */
	@GenModel(apogy_units="m")
	double distanceInterval
}

/**
 * Paths Facade
 */
class ApogyAddonsGeometryPathsFacade
{
	/**
	 * Creates a WayPointPath from a WayPointPath. The WayPointPath created
	 * contains copies of all the points found in the specified WayPointPath.
	 * @param wayPointPath The original WayPointPath.
	 * @return A WayPointPath that contains copies of the point found in the original.
	 */
	op WayPointPath createWayPointPath(WayPointPath wayPointPath)
	
	/**
	 * Creates a WayPointPath using a list of CartesianPositionCoordinates.
	 * The WayPointPath created contains copies of all the points found in
	 * the specified list.
	 * @param points The list of CartesianPositionCoordinates.
	 * @return A WayPointPath that contains copies of the point found in the list.
	 */
	op WayPointPath createWayPointPath(List<CartesianPositionCoordinates> points)
	
	/**
	 * Creates a WayPointPath by finding the projection of each point of the original WayPointPath onto a specified mesh.
	 * If no projection is found, a copy of the original point is used. Note that point may be added at location where 
	 * the WayPointPath projection crosses polygons edges. 
	 * @param originalPath The original WayPointPath.
	 * @param mesh The mesh on which to find the projection.
	 * @param meshToPathTransform The 4X4 matrix expressing the transformation to bring the trajectory into the mesh reference frame.
	 * @param projectionAxis The axis (w.r.t the mesh reference frame) to use when finding the projection.
	 * @param heightOffset Distance by which the generated WayPointPath's point has to be shifted along the projection axis.
	 */
	op WayPointPath projectOntoMesh(WayPointPath originalPath, CartesianTriangularMesh mesh, Matrix4x4 meshToPathTransform, CartesianAxis projectionAxis, double heightOffset)
}
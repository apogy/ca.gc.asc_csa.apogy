package ca.gc.asc_csa.apogy.core.environment.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.emf.ui.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.TimeSourceCompositeProvider;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;

public class ActiveTimeSourceComposite extends Composite 
{
	private TimeSource timeSource;	
	
	public ActiveTimeSourceComposite(Composite parent, int style) {
		super(parent, style);	
		setLayout(new FillLayout());
		
		updateComposite();
	}
	
	@SuppressWarnings("unchecked")
	private void updateComposite(){
		for(Control control: this.getChildren()){
			control.dispose();
		}
		
		if(this.timeSource != null){			
			TimeSourceCompositeProvider<TimeSource> provider = Activator.getDefault().getTimeSourceCompositeProvider(timeSource);
			
			if(provider != null)
			{
				provider.getComposite(this, SWT.None, timeSource);
			}
		}else{
			new NoContentComposite(this, SWT.None){
				@Override
				protected String getMessage() {
					return "No active time source";
				}
			};
		}		
		
		this.layout();
	}


	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;

		updateComposite();
	}
}

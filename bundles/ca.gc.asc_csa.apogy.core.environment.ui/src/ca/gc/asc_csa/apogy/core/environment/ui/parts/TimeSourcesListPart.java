package ca.gc.asc_csa.apogy.core.environment.ui.parts;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractPart;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.ui.composites.TimeSourcesListComposite;

public class TimeSourcesListPart extends AbstractPart
{
	private TimeSourcesListComposite timeSourcesListComposite;
	private Adapter adapter = null;
		
	public TimeSourcesListPart() 
	{
		// Register adapter to listens for changes in the active apogy environment.
		ApogyCoreEnvironmentFacade.INSTANCE.eAdapters().add(getApogyCoreEnvironmentFacadeAdapter());				
	}
	
	@Override
	protected EObject getInitializeObject() 
	{		
	
		return ApogyCoreEnvironmentFacade.INSTANCE.getActiveApogyEnvironment();
	}

	@Override
	protected void setCompositeContent(EObject eObject) 
	{		
		if(eObject instanceof ApogyEnvironment)
		{
			// Sets the current Apogy Environment.
			timeSourcesListComposite.setTimeSourcesList(((ApogyEnvironment) eObject).getTimeSourcesList());
			}
		else
		{
			timeSourcesListComposite.setTimeSourcesList(null);
		}				
	}

	@Override
	protected void createNoContentComposite(Composite parent, int style) 
	{		
		new NoContentComposite(parent, SWT.None){
			@Override
			protected String getMessage() {
				return "No active Apogy Environment";
			}		
		};	
	}
	
	@Override
	protected void createContentComposite(Composite parent, int style) 
	{						
		timeSourcesListComposite = new TimeSourcesListComposite(parent, style){
			@Override
			protected void newSelection(TreeSelection selection) {
				selectionService
				.setSelection(((TimeSourcesListComposite) getActualComposite()).getSelectedTimeSource());
			}
		};
	}
	
	@Override
	public void dispose()
	{
		// Un-Register adapter to listens for changes in the active apogy environment.
		ApogyCoreEnvironmentFacade.INSTANCE.eAdapters().remove(getApogyCoreEnvironmentFacadeAdapter());
		super.dispose();
	}
		
	private Adapter getApogyCoreEnvironmentFacadeAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof ApogyCoreEnvironmentFacade)
					{
						int featureID = msg.getFeatureID(ApogyCoreEnvironmentFacade.class);
						switch (featureID) 
						{
							case ApogyCoreEnvironmentPackage.APOGY_CORE_ENVIRONMENT_FACADE__ACTIVE_APOGY_ENVIRONMENT:
							{
								if(msg.getNewValue() instanceof ApogyEnvironment)
								{									
									setEObject((ApogyEnvironment) msg.getNewValue());
								}
								else
								{
									setEObject(null);
								}
							}					
							break;

						default:
							break;
						}
					}
				}
			};
		}
		
		return adapter;
	}

	
}

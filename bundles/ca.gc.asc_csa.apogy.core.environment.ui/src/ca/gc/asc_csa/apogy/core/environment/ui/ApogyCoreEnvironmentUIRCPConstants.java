package ca.gc.asc_csa.apogy.core.environment.ui;

public class ApogyCoreEnvironmentUIRCPConstants {

	/**
	 * Active timeSource placeholder
	 */
	public static final String PLACEHOLDER__ACTIVE_TIME_SOURCE__ID = "ca.gc.asc_csa.apogy.core.environment.ui.placeholder.activeTimeSource";	

	/**
	 * Toggle activeTimeSourcePart command
	 */
	public static final String COMMAND__TOGGLE_ACTIVE_TIME_SOURCE_PART__ID = "ca.gc.asc_csa.apogy.core.environment.ui.command.toggleActiveTimeSource";	
	public static final String COMMAND_PARAMETER__TOGGLE_ACTIVE_TIME_SOURCE__SHOW = "ca.gc.asc_csa.apogy.core.environment.ui.commandparameter.toggleActiveTimeSource.show";	

	/**
	 * ToolControl activeTimeSource
	 */
	public static final String TOOL_CONTROL__TIME_SOURCES__ID = "ca.gc.asc_csa.apogy.rcp.toolcontrol.timeSourceToolControl";	

	/**
	 * Part IDs for selectionBasedParts.
	 */
	public static final String PART__TIME_SOURCES_LIST__ID = "ca.gc.asc_csa.apogy.core.environment.ui.part.timeSourcesList";
	
	/**
	 * Main window
	 */
	public static final String MAIN_WINDOW__ID = "ca.gc.asc_csa.apogy.rcp.window.main";

}

package ca.gc.asc_csa.apogy.core.environment.ui.parts;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractPart;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;

abstract public class AbstractApogyEnvironmentBasedPart extends AbstractPart{
	
	private Adapter adapter;

	@Override
	protected void createNoContentComposite(Composite parent, int style) {
		new NoContentComposite(parent, SWT.None){
			@Override
			protected String getMessage() {
				return "No active environment";
			}		
		};	
	}
			
	@Override
	protected EObject getInitializeObject() {
		ApogyCoreEnvironmentFacade.INSTANCE.eAdapters().add(getApogyCoreEnvironmentFacadeAdapter());
		return ApogyCoreEnvironmentFacade.INSTANCE.getActiveApogyEnvironment();
	} 
	
	abstract protected void newEnvironment(ApogyEnvironment environment);

	@Override
	protected void setCompositeContent(EObject eObject) {
		newEnvironment((ApogyEnvironment) eObject); 
	}
	
	private Adapter getApogyCoreEnvironmentFacadeAdapter() {
		if (adapter == null) {
			adapter = new AdapterImpl() {
				@Override
				public void notifyChanged(Notification msg) {
					if (getActualComposite() != null && msg
							.getFeature() == ApogyCoreEnvironmentPackage.Literals.APOGY_CORE_ENVIRONMENT_FACADE__ACTIVE_APOGY_ENVIRONMENT) {
						setEObject(ApogyCoreEnvironmentFacade.INSTANCE.getActiveApogyEnvironment());
					}
				}
			};
		}
		return adapter;
	}

	@Override
	protected void dispose() {
		ApogyCoreEnvironmentFacade.INSTANCE.eAdapters().remove(getApogyCoreEnvironmentFacadeAdapter());
		super.dispose();
	}
}
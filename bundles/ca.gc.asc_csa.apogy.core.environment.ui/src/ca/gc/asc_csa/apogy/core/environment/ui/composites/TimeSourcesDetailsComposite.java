package ca.gc.asc_csa.apogy.core.environment.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.emf.ui.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.TimeSourceCompositeProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;

public class TimeSourcesDetailsComposite extends Composite 
{
	private TimeSource timeSource;
	private Composite timeSourceComposite;
	private Composite emfFormsComposite;
	
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	
	public TimeSourcesDetailsComposite(Composite parent, int style) {
		super(parent, style);	
		setLayout(new GridLayout(1, false));
		
		Section sectionTimeSource = toolkit.createSection(this, Section.TWISTIE | Section.TITLE_BAR);
		sectionTimeSource.setText("Time details");
		toolkit.paintBordersFor(sectionTimeSource);
		sectionTimeSource.setExpanded(true);
		sectionTimeSource.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		timeSourceComposite = new Composite(sectionTimeSource, SWT.None);
		timeSourceComposite.setLayout(new FillLayout());
		toolkit.adapt(timeSourceComposite);
		sectionTimeSource.setClient(timeSourceComposite);
		
		Section sectionEMFForms = toolkit.createSection(this, Section.TWISTIE | Section.TITLE_BAR);
		sectionEMFForms.setText("More details");
		toolkit.paintBordersFor(sectionEMFForms);
		sectionEMFForms.setExpanded(false);
		sectionEMFForms.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		emfFormsComposite = new Composite(sectionEMFForms, SWT.None);
		toolkit.adapt(emfFormsComposite);
		sectionEMFForms.setClient(emfFormsComposite);
		
		updateComposite();
	}
	
	@SuppressWarnings("unchecked")
	private void updateComposite(){
		for(Control control: timeSourceComposite.getChildren()){
			control.dispose();
		}
		
		for(Control control: emfFormsComposite.getChildren()){
			control.dispose();
		}
		
		if(this.timeSource != null){			
			TimeSourceCompositeProvider<TimeSource> provider = Activator.getDefault().getTimeSourceCompositeProvider(timeSource);
			
			if(provider != null)
			{
				provider.getComposite(timeSourceComposite, SWT.BORDER, timeSource);
				timeSourceComposite.layout();
			}
			
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(emfFormsComposite, timeSource);
			this.layout();
			
		}else{
			new NoContentComposite(timeSourceComposite, SWT.None);
			layout();
		}		
	}


	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;

		updateComposite();
	}
}

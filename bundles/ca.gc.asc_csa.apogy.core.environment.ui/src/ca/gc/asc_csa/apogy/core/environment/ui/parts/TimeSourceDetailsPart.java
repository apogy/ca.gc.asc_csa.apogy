package ca.gc.asc_csa.apogy.core.environment.ui.parts;

import java.util.HashMap;

import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.core.environment.ui.ApogyCoreEnvironmentUIRCPConstants;
import ca.gc.asc_csa.apogy.core.environment.ui.composites.TimeSourcesDetailsComposite;

public class TimeSourceDetailsPart extends AbstractEObjectSelectionPart {

	@Override
	protected void setCompositeContents(EObject eObject) {
		TimeSourcesDetailsComposite composite = (TimeSourcesDetailsComposite)getActualComposite();
		if(eObject instanceof TimeSource)
		{
			// Sets the current Apogy Environment.
			composite.setTimeSource((TimeSource)eObject);
		}
		else
		{
			composite.setTimeSource(null);
		}		
	}

	@Override
	protected void createContentComposite(Composite parent, int style) {
		new TimeSourcesDetailsComposite(parent, style);
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() {
		HashMap<String, ISelectionListener> map = new HashMap<>();
		
		map.put(ApogyCoreEnvironmentUIRCPConstants.PART__TIME_SOURCES_LIST__ID, DEFAULT_LISTENER);
		
		return map;
	}

}

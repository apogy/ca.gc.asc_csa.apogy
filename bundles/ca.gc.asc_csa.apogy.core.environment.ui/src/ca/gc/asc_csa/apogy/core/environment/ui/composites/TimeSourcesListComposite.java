package ca.gc.asc_csa.apogy.core.environment.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.emf.Disposable;
import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.EObjectListComposite;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.TimeSourcesList;

public class TimeSourcesListComposite extends Composite 
{
	private EObjectListComposite listComposite;
	private Button newBtn;
	private Button deleteBtn;
	private Button activateBtn;
	
	private TimeSourcesList timeSourcesList;
	
	public TimeSourcesListComposite(Composite parent, int style) {
		super(parent, SWT.BORDER);
		setLayout(new GridLayout(2, false));
		
		listComposite = new EObjectListComposite(this, SWT.None){
			private AdapterFactoryLabelProvider adapterFactoryLabelProvider = new AdapterFactoryLabelProvider( new ComposedAdapterFactory(
					ComposedAdapterFactory.Descriptor.Registry.INSTANCE));
			
			@Override
			protected void newSelection(TreeSelection selection) {
				TimeSourcesListComposite.this.newSelection(selection);
			}
			
			@Override
			protected StyledCellLabelProvider getLabelProvider() {
				return new StyledCellLabelProvider() {
					@Override
					public void update(ViewerCell cell) {
						if(cell.getElement() instanceof TimeSource){
							TimeSource timeSource = (TimeSource)cell.getElement();
							cell.setText(timeSource.getName());
							cell.setImage(adapterFactoryLabelProvider.getColumnImage(cell.getElement(), 0));
						}
					}
				};
			}
		};
		listComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 4));
		
		newBtn = new Button(this, SWT.None);
		newBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		newBtn.setText("New");
		newBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				NamedSetting namedSettings = ApogyCommonEMFUIFactory.eINSTANCE.createNamedSetting();
				namedSettings.setParent(timeSourcesList);
				namedSettings.setContainingFeature(ApogyCoreEnvironmentPackage.Literals.TIME_SOURCES_LIST__TIME_SOURCES);
				ApogyEObjectWizard wizard = new ApogyEObjectWizard(ApogyCoreEnvironmentPackage.Literals.TIME_SOURCES_LIST__TIME_SOURCES,
						timeSourcesList, namedSettings, null) {
					@Override
					public boolean performFinish() {
						boolean value = super.performFinish();
						listComposite.refreshTreeViewer();
						listComposite.setSelectedEObject(getCreatedEObject());
						return value;
					}
				};
				WizardDialog dialog = new WizardDialog(getShell(), wizard);
				dialog.open();
			}
		});
		
		deleteBtn = new Button(this, SWT.None);
		deleteBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		deleteBtn.setText("Delete");
		deleteBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TimeSource selectedTimeSource = getSelectedTimeSource();
				
				if(timeSourcesList != null && timeSourcesList.getEnvironment() != null &&
						timeSourcesList.getEnvironment().getActiveTimeSource() == selectedTimeSource){
					ApogyCommonTransactionFacade.INSTANCE.basicSet(timeSourcesList.getEnvironment(),
							ApogyCoreEnvironmentPackage.Literals.APOGY_ENVIRONMENT__ACTIVE_TIME_SOURCE,
							null);
				}
				
				ApogyCommonTransactionFacade.INSTANCE.basicRemove(timeSourcesList,
						ApogyCoreEnvironmentPackage.Literals.TIME_SOURCES_LIST__TIME_SOURCES, getSelectedTimeSource());				
				
				if(selectedTimeSource instanceof Disposable){
					((Disposable)selectedTimeSource).dispose();
				}
				listComposite.refreshTreeViewer();
			}
		});
		
		new Label(this, SWT.HORIZONTAL |SWT.SEPARATOR);
		
		activateBtn = new Button(this, SWT.None);
		activateBtn.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1));
		activateBtn.setText("Activate");
		activateBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (timeSourcesList != null && timeSourcesList.getEnvironment() != null) {
					ApogyCommonTransactionFacade.INSTANCE.basicSet(timeSourcesList.getEnvironment(),
							ApogyCoreEnvironmentPackage.Literals.APOGY_ENVIRONMENT__ACTIVE_TIME_SOURCE,
							listComposite.getSelectedEObject());
				}
			}
		});
	}

	public void setTimeSourcesList(TimeSourcesList timeSourcesList) 
	{
		this.timeSourcesList = timeSourcesList;	
		listComposite.setEObjectsList(timeSourcesList.getTimeSources(), timeSourcesList.getEnvironment().getActiveTimeSource());
	}		
	
	protected void newSelection(TreeSelection selection){
		
	}
	
	public TimeSource getSelectedTimeSource(){
		return listComposite.getSelectedEObject() == null ? null : (TimeSource)listComposite.getSelectedEObject();
	}
	
}
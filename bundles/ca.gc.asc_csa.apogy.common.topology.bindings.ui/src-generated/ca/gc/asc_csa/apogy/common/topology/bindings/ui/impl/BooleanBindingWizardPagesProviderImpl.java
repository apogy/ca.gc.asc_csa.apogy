/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.topology.AggregateGroupNode;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFactory;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.GroupNode;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.RotationNode;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsFactory;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.BooleanBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.FalseBooleanCase;
import ca.gc.asc_csa.apogy.common.topology.bindings.TrueBooleanCase;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.BooleanBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TopologyUIBindingsConstants;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.BooleanBindingFalseCaseTopologyWizardPage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.BooleanBindingTrueCaseTopologyWizardPage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.TopologyNodeSelectionWizardPage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Boolean Binding Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BooleanBindingWizardPagesProviderImpl extends AbstractTopologyBindingWizardPagesProviderImpl implements BooleanBindingWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BooleanBindingWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyBindingsUIPackage.Literals.BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER;
	}
	
	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		BooleanBinding booleanBinding = ApogyCommonTopologyBindingsFactory.eINSTANCE.createBooleanBinding();
		
		TrueBooleanCase trueCase = ApogyCommonTopologyBindingsFactory.eINSTANCE.createTrueBooleanCase();
		AggregateGroupNode trueCaseRoot = ApogyCommonTopologyFactory.eINSTANCE.createAggregateGroupNode();
		trueCaseRoot.setNodeId("TRUE_CASE_ROOT");
		trueCase.setTopologyRoot(trueCaseRoot);		
		booleanBinding.setTrueCase(trueCase);
		
		FalseBooleanCase falseCase = ApogyCommonTopologyBindingsFactory.eINSTANCE.createFalseBooleanCase();
		AggregateGroupNode falseCaseRoot = ApogyCommonTopologyFactory.eINSTANCE.createAggregateGroupNode();
		falseCaseRoot.setNodeId("FALSE_CASE_ROOT");
		falseCase.setTopologyRoot(falseCaseRoot);			
		booleanBinding.setFalseCase(falseCase);
		
		return booleanBinding;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));
		
		// Add topology node selection page.
		List<Node> nodes = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings  = (MapBasedEClassSettings) settings;
			nodes = (List<Node>) mapBasedEClassSettings.getUserDataMap().get(TopologyUIBindingsConstants.NODES_LIST_USER_ID);
		}
		
		BooleanBinding booleanBinding = (BooleanBinding) eObject;		
		
		TopologyNodeSelectionWizardPage topologyNodeSelectionWizardPage = new TopologyNodeSelectionWizardPage("Select Group Node", 
				"Select the Group Node under which the sub topology will be attached.", 
				booleanBinding, 
				ApogyCommonTopologyPackage.Literals.GROUP_NODE, 
				nodes) 
		{	
			@Override
			protected void nodeSelected(Node nodeSelected) 
			{
				if(booleanBinding != null && nodeSelected instanceof RotationNode)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet((BooleanBinding) abstractTopologyBinding, ApogyCommonTopologyBindingsPackage.Literals.BOOLEAN_BINDING__PARENT_NODE, (GroupNode) nodeSelected, true);
					
					setMessage("Node selected : " + nodeSelected.getNodeId());
					setErrorMessage(null);
				}		
				else
				{
					setErrorMessage("The selected Node is not a Group Node !");
				}
			}
		};
		
		list.add(topologyNodeSelectionWizardPage);
		
		list.add(new BooleanBindingTrueCaseTopologyWizardPage(booleanBinding));
		list.add(new BooleanBindingFalseCaseTopologyWizardPage(booleanBinding));
		
		return list;
	}
} //BooleanBindingWizardPagesProviderImpl

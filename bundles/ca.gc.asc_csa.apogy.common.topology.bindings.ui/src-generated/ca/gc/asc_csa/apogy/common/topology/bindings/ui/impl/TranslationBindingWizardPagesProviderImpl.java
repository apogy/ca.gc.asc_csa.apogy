/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.PositionNode;
import ca.gc.asc_csa.apogy.common.topology.RotationNode;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsFactory;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.Axis;
import ca.gc.asc_csa.apogy.common.topology.bindings.TranslationBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TopologyUIBindingsConstants;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TranslationBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.AxisSelectionWizardPage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.TopologyNodeSelectionWizardPage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Translation Binding Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TranslationBindingWizardPagesProviderImpl extends AbstractTopologyBindingWizardPagesProviderImpl implements TranslationBindingWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TranslationBindingWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyBindingsUIPackage.Literals.TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		TranslationBinding translationBinding = ApogyCommonTopologyBindingsFactory.eINSTANCE.createTranslationBinding();			
		return translationBinding;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));
		
		List<Node> nodes = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings  = (MapBasedEClassSettings) settings;
			nodes = (List<Node>) mapBasedEClassSettings.getUserDataMap().get(TopologyUIBindingsConstants.NODES_LIST_USER_ID);
		}
		
		TranslationBinding translationBinding = (TranslationBinding) eObject;		
		TopologyNodeSelectionWizardPage topologyNodeSelectionWizardPage = new TopologyNodeSelectionWizardPage("Select a Translation Node", 
				"Select the Translation Node that this binding is controlling.", 
				translationBinding, 
				ApogyCommonTopologyPackage.Literals.POSITION_NODE, 
				nodes) 
		{	
			@Override
			protected void nodeSelected(Node nodeSelected) 
			{
				if(translationBinding != null && nodeSelected instanceof PositionNode)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet((TranslationBinding) abstractTopologyBinding, ApogyCommonTopologyBindingsPackage.Literals.TRANSLATION_BINDING__POSITION_NODE, (RotationNode) nodeSelected, true);
					
					setMessage("Node selected : " + nodeSelected.getNodeId());
					setErrorMessage(null);
				}		
				else
				{
					setErrorMessage("The selected Node is not a Position Node !");
				}
			}
		};
		
		list.add(topologyNodeSelectionWizardPage);	
		
		AxisSelectionWizardPage axisSelectionWizardPage = new AxisSelectionWizardPage("Translation Axis", "Select the axis along which the translation is to occur", translationBinding) {
			
			@Override
			protected void axisSelected(Axis axisSelected) 
			{
				if(translationBinding != null)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet((TranslationBinding) abstractTopologyBinding, ApogyCommonTopologyBindingsPackage.Literals.TRANSLATION_BINDING__TRANSLATION_AXIS, axisSelected, true);
					
					setMessage("Axis selected : " + axisSelected.getLiteral());
					setErrorMessage(null);
				}		
				else
				{
					setErrorMessage("No translation axis selected !");
				}
				
			}
		};
		
		list.add(axisSelectionWizardPage);
		
		
		return list;
	}
} //TranslationBindingWizardPagesProviderImpl

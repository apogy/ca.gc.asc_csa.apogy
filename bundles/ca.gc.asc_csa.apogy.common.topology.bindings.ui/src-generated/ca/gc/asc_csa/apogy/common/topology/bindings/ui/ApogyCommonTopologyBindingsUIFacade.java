/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui;

import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationSwitchBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage#getApogyCommonTopologyBindingsUIFacade()
 * @model
 * @generated
 */
public interface ApogyCommonTopologyBindingsUIFacade extends EObject 
{
	public static final ApogyCommonTopologyBindingsUIFacade INSTANCE = ApogyCommonTopologyBindingsUIFacadeImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns wheter or note the specified EEnumLiteral is used in the specified EnumerationSwitchBinding.
	 * @param enumerationSwitchBinding The specified EnumerationSwitchBinding.
	 * @param eEnumLiteral The specified EEnumLiteral.
	 * @return True if the EEnumLiteral is being used, false otherwise.
	 * <!-- end-model-doc -->
	 * @model unique="false" enumerationSwitchBindingUnique="false" eEnumLiteralUnique="false"
	 * @generated
	 */
	boolean isEEnumLiteralInUse(EnumerationSwitchBinding enumerationSwitchBinding, EEnumLiteral eEnumLiteral);



} // ApogyCommonTopologyBindingsUIFacade

/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl;

import ca.gc.asc_csa.apogy.common.topology.bindings.ui.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIFactory;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.BooleanBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.EnumerationSwitchBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.RotationBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TransformMatrixBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TranslationBindingWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCommonTopologyBindingsUIFactoryImpl extends EFactoryImpl implements ApogyCommonTopologyBindingsUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyCommonTopologyBindingsUIFactory init() {
		try {
			ApogyCommonTopologyBindingsUIFactory theApogyCommonTopologyBindingsUIFactory = (ApogyCommonTopologyBindingsUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyCommonTopologyBindingsUIPackage.eNS_URI);
			if (theApogyCommonTopologyBindingsUIFactory != null) {
				return theApogyCommonTopologyBindingsUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyCommonTopologyBindingsUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyBindingsUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyCommonTopologyBindingsUIPackage.APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE: return createApogyCommonTopologyBindingsUIFacade();
			case ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER: return createAbstractTopologyBindingWizardPagesProvider();
			case ApogyCommonTopologyBindingsUIPackage.ROTATION_BINDING_WIZARD_PAGES_PROVIDER: return createRotationBindingWizardPagesProvider();
			case ApogyCommonTopologyBindingsUIPackage.TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER: return createTranslationBindingWizardPagesProvider();
			case ApogyCommonTopologyBindingsUIPackage.TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER: return createTransformMatrixBindingWizardPagesProvider();
			case ApogyCommonTopologyBindingsUIPackage.BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER: return createBooleanBindingWizardPagesProvider();
			case ApogyCommonTopologyBindingsUIPackage.ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER: return createEnumerationSwitchBindingWizardPagesProvider();
			case ApogyCommonTopologyBindingsUIPackage.ENUMERATION_CASE_WIZARD_PAGES_PROVIDER: return createEnumerationCaseWizardPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyBindingsUIFacade createApogyCommonTopologyBindingsUIFacade() {
		ApogyCommonTopologyBindingsUIFacadeImpl apogyCommonTopologyBindingsUIFacade = new ApogyCommonTopologyBindingsUIFacadeImpl();
		return apogyCommonTopologyBindingsUIFacade;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractTopologyBindingWizardPagesProvider createAbstractTopologyBindingWizardPagesProvider() {
		AbstractTopologyBindingWizardPagesProviderImpl abstractTopologyBindingWizardPagesProvider = new AbstractTopologyBindingWizardPagesProviderImpl();
		return abstractTopologyBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RotationBindingWizardPagesProvider createRotationBindingWizardPagesProvider() {
		RotationBindingWizardPagesProviderImpl rotationBindingWizardPagesProvider = new RotationBindingWizardPagesProviderImpl();
		return rotationBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TranslationBindingWizardPagesProvider createTranslationBindingWizardPagesProvider() {
		TranslationBindingWizardPagesProviderImpl translationBindingWizardPagesProvider = new TranslationBindingWizardPagesProviderImpl();
		return translationBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformMatrixBindingWizardPagesProvider createTransformMatrixBindingWizardPagesProvider() {
		TransformMatrixBindingWizardPagesProviderImpl transformMatrixBindingWizardPagesProvider = new TransformMatrixBindingWizardPagesProviderImpl();
		return transformMatrixBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanBindingWizardPagesProvider createBooleanBindingWizardPagesProvider() {
		BooleanBindingWizardPagesProviderImpl booleanBindingWizardPagesProvider = new BooleanBindingWizardPagesProviderImpl();
		return booleanBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationSwitchBindingWizardPagesProvider createEnumerationSwitchBindingWizardPagesProvider() {
		EnumerationSwitchBindingWizardPagesProviderImpl enumerationSwitchBindingWizardPagesProvider = new EnumerationSwitchBindingWizardPagesProviderImpl();
		return enumerationSwitchBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationCaseWizardPagesProvider createEnumerationCaseWizardPagesProvider() {
		EnumerationCaseWizardPagesProviderImpl enumerationCaseWizardPagesProvider = new EnumerationCaseWizardPagesProviderImpl();
		return enumerationCaseWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyBindingsUIPackage getApogyCommonTopologyBindingsUIPackage() {
		return (ApogyCommonTopologyBindingsUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyCommonTopologyBindingsUIPackage getPackage() {
		return ApogyCommonTopologyBindingsUIPackage.eINSTANCE;
	}

} //ApogyCommonTopologyBindingsUIFactoryImpl

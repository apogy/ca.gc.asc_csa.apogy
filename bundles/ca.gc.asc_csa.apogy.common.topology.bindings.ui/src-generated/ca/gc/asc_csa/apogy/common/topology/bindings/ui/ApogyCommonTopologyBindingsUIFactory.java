/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage
 * @generated
 */
public interface ApogyCommonTopologyBindingsUIFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCommonTopologyBindingsUIFactory eINSTANCE = ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Facade</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Facade</em>'.
	 * @generated
	 */
	ApogyCommonTopologyBindingsUIFacade createApogyCommonTopologyBindingsUIFacade();

	/**
	 * Returns a new object of class '<em>Abstract Topology Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Topology Binding Wizard Pages Provider</em>'.
	 * @generated
	 */
	AbstractTopologyBindingWizardPagesProvider createAbstractTopologyBindingWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Rotation Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rotation Binding Wizard Pages Provider</em>'.
	 * @generated
	 */
	RotationBindingWizardPagesProvider createRotationBindingWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Translation Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Translation Binding Wizard Pages Provider</em>'.
	 * @generated
	 */
	TranslationBindingWizardPagesProvider createTranslationBindingWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Transform Matrix Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transform Matrix Binding Wizard Pages Provider</em>'.
	 * @generated
	 */
	TransformMatrixBindingWizardPagesProvider createTransformMatrixBindingWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Boolean Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Binding Wizard Pages Provider</em>'.
	 * @generated
	 */
	BooleanBindingWizardPagesProvider createBooleanBindingWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Enumeration Switch Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumeration Switch Binding Wizard Pages Provider</em>'.
	 * @generated
	 */
	EnumerationSwitchBindingWizardPagesProvider createEnumerationSwitchBindingWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Enumeration Case Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumeration Case Wizard Pages Provider</em>'.
	 * @generated
	 */
	EnumerationCaseWizardPagesProvider createEnumerationCaseWizardPagesProvider();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyCommonTopologyBindingsUIPackage getApogyCommonTopologyBindingsUIPackage();

} //ApogyCommonTopologyBindingsUIFactory

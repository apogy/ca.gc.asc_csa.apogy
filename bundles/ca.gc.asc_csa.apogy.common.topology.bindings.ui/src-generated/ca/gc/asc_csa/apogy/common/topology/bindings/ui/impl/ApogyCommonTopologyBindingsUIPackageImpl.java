/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIFacade;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIFactory;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.BooleanBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.EnumerationCaseWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.EnumerationSwitchBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.RotationBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TransformMatrixBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TranslationBindingWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCommonTopologyBindingsUIPackageImpl extends EPackageImpl implements ApogyCommonTopologyBindingsUIPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass apogyCommonTopologyBindingsUIFacadeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractTopologyBindingWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rotationBindingWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass translationBindingWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transformMatrixBindingWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanBindingWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumerationSwitchBindingWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumerationCaseWizardPagesProviderEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyCommonTopologyBindingsUIPackageImpl() {
		super(eNS_URI, ApogyCommonTopologyBindingsUIFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCommonTopologyBindingsUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyCommonTopologyBindingsUIPackage init() {
		if (isInited) return (ApogyCommonTopologyBindingsUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyBindingsUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCommonTopologyBindingsUIPackageImpl theApogyCommonTopologyBindingsUIPackage = (ApogyCommonTopologyBindingsUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCommonTopologyBindingsUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCommonTopologyBindingsUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCommonTopologyBindingsPackage.eINSTANCE.eClass();
		ApogyCommonEMFUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCommonTopologyBindingsUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCommonTopologyBindingsUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCommonTopologyBindingsUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCommonTopologyBindingsUIPackage.eNS_URI, theApogyCommonTopologyBindingsUIPackage);
		return theApogyCommonTopologyBindingsUIPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApogyCommonTopologyBindingsUIFacade() {
		return apogyCommonTopologyBindingsUIFacadeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTopologyBindingsUIFacade__IsEEnumLiteralInUse__EnumerationSwitchBinding_EEnumLiteral() {
		return apogyCommonTopologyBindingsUIFacadeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractTopologyBindingWizardPagesProvider() {
		return abstractTopologyBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRotationBindingWizardPagesProvider() {
		return rotationBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTranslationBindingWizardPagesProvider() {
		return translationBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransformMatrixBindingWizardPagesProvider() {
		return transformMatrixBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanBindingWizardPagesProvider() {
		return booleanBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnumerationSwitchBindingWizardPagesProvider() {
		return enumerationSwitchBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnumerationCaseWizardPagesProvider() {
		return enumerationCaseWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyBindingsUIFactory getApogyCommonTopologyBindingsUIFactory() {
		return (ApogyCommonTopologyBindingsUIFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		apogyCommonTopologyBindingsUIFacadeEClass = createEClass(APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE);
		createEOperation(apogyCommonTopologyBindingsUIFacadeEClass, APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE___IS_EENUM_LITERAL_IN_USE__ENUMERATIONSWITCHBINDING_EENUMLITERAL);

		abstractTopologyBindingWizardPagesProviderEClass = createEClass(ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER);

		rotationBindingWizardPagesProviderEClass = createEClass(ROTATION_BINDING_WIZARD_PAGES_PROVIDER);

		translationBindingWizardPagesProviderEClass = createEClass(TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER);

		transformMatrixBindingWizardPagesProviderEClass = createEClass(TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER);

		booleanBindingWizardPagesProviderEClass = createEClass(BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER);

		enumerationSwitchBindingWizardPagesProviderEClass = createEClass(ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER);

		enumerationCaseWizardPagesProviderEClass = createEClass(ENUMERATION_CASE_WIZARD_PAGES_PROVIDER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogyCommonTopologyBindingsPackage theApogyCommonTopologyBindingsPackage = (ApogyCommonTopologyBindingsPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyBindingsPackage.eNS_URI);
		ApogyCommonEMFUIPackage theApogyCommonEMFUIPackage = (ApogyCommonEMFUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFUIPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		abstractTopologyBindingWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getWizardPagesProvider());
		rotationBindingWizardPagesProviderEClass.getESuperTypes().add(this.getAbstractTopologyBindingWizardPagesProvider());
		translationBindingWizardPagesProviderEClass.getESuperTypes().add(this.getAbstractTopologyBindingWizardPagesProvider());
		transformMatrixBindingWizardPagesProviderEClass.getESuperTypes().add(this.getAbstractTopologyBindingWizardPagesProvider());
		booleanBindingWizardPagesProviderEClass.getESuperTypes().add(this.getAbstractTopologyBindingWizardPagesProvider());
		enumerationSwitchBindingWizardPagesProviderEClass.getESuperTypes().add(this.getAbstractTopologyBindingWizardPagesProvider());
		enumerationCaseWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(apogyCommonTopologyBindingsUIFacadeEClass, ApogyCommonTopologyBindingsUIFacade.class, "ApogyCommonTopologyBindingsUIFacade", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getApogyCommonTopologyBindingsUIFacade__IsEEnumLiteralInUse__EnumerationSwitchBinding_EEnumLiteral(), theEcorePackage.getEBoolean(), "isEEnumLiteralInUse", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonTopologyBindingsPackage.getEnumerationSwitchBinding(), "enumerationSwitchBinding", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEEnumLiteral(), "eEnumLiteral", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(abstractTopologyBindingWizardPagesProviderEClass, AbstractTopologyBindingWizardPagesProvider.class, "AbstractTopologyBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(rotationBindingWizardPagesProviderEClass, RotationBindingWizardPagesProvider.class, "RotationBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(translationBindingWizardPagesProviderEClass, TranslationBindingWizardPagesProvider.class, "TranslationBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(transformMatrixBindingWizardPagesProviderEClass, TransformMatrixBindingWizardPagesProvider.class, "TransformMatrixBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(booleanBindingWizardPagesProviderEClass, BooleanBindingWizardPagesProvider.class, "BooleanBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(enumerationSwitchBindingWizardPagesProviderEClass, EnumerationSwitchBindingWizardPagesProvider.class, "EnumerationSwitchBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(enumerationCaseWizardPagesProviderEClass, EnumerationCaseWizardPagesProvider.class, "EnumerationCaseWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //ApogyCommonTopologyBindingsUIPackageImpl

/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.common.topology.AggregateGroupNode;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFactory;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsFactory;
import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationCase;
import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationSwitchBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.EnumerationCaseWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TopologyUIBindingsConstants;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.EnumerationCaseWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enumeration Case Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EnumerationCaseWizardPagesProviderImpl extends WizardPagesProviderImpl implements EnumerationCaseWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnumerationCaseWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyBindingsUIPackage.Literals.ENUMERATION_CASE_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		EnumerationCase enumerationCase = ApogyCommonTopologyBindingsFactory.eINSTANCE.createEnumerationCase();
		AggregateGroupNode node = ApogyCommonTopologyFactory.eINSTANCE.createAggregateGroupNode();
		node.setNodeId("GIVE_ME_A_UNIQUE_ID!");
		enumerationCase.setTopologyRoot(node);
		return enumerationCase;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));
				
		EnumerationSwitchBinding enumerationSwitchBinding = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings  = (MapBasedEClassSettings) settings;
			enumerationSwitchBinding = (EnumerationSwitchBinding) mapBasedEClassSettings.getUserDataMap().get(TopologyUIBindingsConstants.ENUMERATION_SWITCH_BINDING_USER_ID);
		}
		
		EnumerationCase enumerationCase = (EnumerationCase) eObject;		
		
		
		list.add(new EnumerationCaseWizardPage(enumerationSwitchBinding, enumerationCase));
		
		return list;
	}
} //EnumerationCaseWizardPagesProviderImpl

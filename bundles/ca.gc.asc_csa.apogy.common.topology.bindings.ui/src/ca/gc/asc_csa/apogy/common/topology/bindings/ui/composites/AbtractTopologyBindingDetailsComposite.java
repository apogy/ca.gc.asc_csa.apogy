package ca.gc.asc_csa.apogy.common.topology.bindings.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding;

public class AbtractTopologyBindingDetailsComposite extends Composite 
{
	private AbstractTopologyBinding abstractTopologyBinding;
	private Composite compositeDetails;
	
	public AbtractTopologyBindingDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		this.setLayout(new GridLayout(1, false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.widthHint = 398;
		gd_compositeDetails.minimumWidth = 398;
		compositeDetails.setLayoutData(gd_compositeDetails);
		
		compositeDetails.setLayout(new GridLayout(1, false));	
	}

	public AbstractTopologyBinding getAbstractTopologyBinding() {
		return abstractTopologyBinding;
	}

	public void setAbstractTopologyBinding(AbstractTopologyBinding newAbstractTopologyBinding) 
	{
		this.abstractTopologyBinding = newAbstractTopologyBinding;
		
		// Update Details EMFForm.
		if(newAbstractTopologyBinding != null)
		{
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, newAbstractTopologyBinding, false);
		}
	}
}

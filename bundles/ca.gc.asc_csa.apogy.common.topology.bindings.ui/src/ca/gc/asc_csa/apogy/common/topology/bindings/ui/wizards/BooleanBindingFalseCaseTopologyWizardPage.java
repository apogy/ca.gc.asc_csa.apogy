/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.bindings.BooleanBinding;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.TopologyTreeEditingComposite;

public class BooleanBindingFalseCaseTopologyWizardPage extends WizardPage 
{	
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.BooleanBindingFalseCaseTopologyWizardPage";		
	
	private BooleanBinding booleanBinding;
	
	private TopologyTreeEditingComposite falseCaseTopologyTreeComposite;

	
	public BooleanBindingFalseCaseTopologyWizardPage(BooleanBinding booleanBinding)
	{
		super(WIZARD_PAGE_ID);
		setTitle("False Case Topology");
		setDescription("Define the topology for the False case of the binding.");
		
		this.booleanBinding = booleanBinding;
	}
	
	@Override
	public void createControl(Composite parent) 
	{	
		Composite container = new Composite(parent, SWT.BORDER);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		container.setLayout(new GridLayout(1, false));	
		setControl(container);

		falseCaseTopologyTreeComposite = new TopologyTreeEditingComposite(container, SWT.NONE, true);
		GridData gd_falseCaseTopologyTreeComposite = new GridData(SWT.FILL, SWT.FILL, true, true,1,1);
		falseCaseTopologyTreeComposite.setLayoutData(gd_falseCaseTopologyTreeComposite);
		
		falseCaseTopologyTreeComposite.setRoot(booleanBinding.getFalseCase().getTopologyRoot());
	}

}

package ca.gc.asc_csa.apogy.common.topology.bindings.ui.parts;

import java.util.Collection;
import java.util.HashMap;

import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.bindings.BindingsSet;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.composites.BindingsSetComposite;

public abstract class BindingsSetPart extends AbstractEObjectSelectionPart
{
	@SuppressWarnings("unused")
	private BindingsSet bindingsSet;	
	private BindingsSetComposite bindingsSetComposite;
	
	@Override
	protected void setCompositeContents(EObject eObject) 
	{
		if(eObject == null || eObject instanceof BindingsSet)
		{
			setBindingsSet((BindingsSet) eObject);			
		}		
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{
		bindingsSetComposite = new BindingsSetComposite(parent, style)
		{
			@Override
			public Collection<Node> getAvailableNodes() 
			{
				return BindingsSetPart.this.getAvailableNodes();
			}
		};
	}
	
	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() 
	{
		HashMap<String, ISelectionListener> selectionProvidersIdsToSelectionListeners = new HashMap<String, ISelectionListener>();
		return selectionProvidersIdsToSelectionListeners;
	}
	
	protected void setBindingsSet(BindingsSet newBindingsSet)
	{
		this.bindingsSet = newBindingsSet;
		
		if(bindingsSetComposite != null && !bindingsSetComposite.isDisposed())
		{
			bindingsSetComposite.setBindingsSet(newBindingsSet);
		}
	}
	
	/**
	 * Returns the list of Node available for binding.
	 * @return The collection of Node, can be empty, never null.
	 */
	public abstract Collection<Node> getAvailableNodes();
}

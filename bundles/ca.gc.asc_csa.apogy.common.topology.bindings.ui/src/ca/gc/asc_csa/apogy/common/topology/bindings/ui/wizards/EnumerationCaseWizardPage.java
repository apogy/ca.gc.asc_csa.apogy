/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureSpecifier;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.BindingsList;
import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationCase;
import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationSwitchBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIFacade;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class EnumerationCaseWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.EnumerationCaseWizardPage";		

	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	
	private EnumerationSwitchBinding enumerationSwitchBinding;
	private EnumerationCase enumerationCase;

	private Tree availableEEnumLiteralTree;
	private TreeViewer availableEEnumLiteralTreeViewer;
	
	private Tree selectedEEnumLiteralTree;
	private TreeViewer selectedEEnumLiteralTreeViewer;
	
	private Button btnAdd;
	private Button btnRemove;	

	
	public EnumerationCaseWizardPage(EnumerationSwitchBinding enumerationSwitchBinding, EnumerationCase enumerationCase)
	{
		super(WIZARD_PAGE_ID);
		setTitle("Enumeration Case");
		setDescription("Defined the Enumeration Case.");	
		
		this.enumerationSwitchBinding = enumerationSwitchBinding;
		this.enumerationCase = enumerationCase;
	}
	
	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		container.setLayout(new GridLayout(3, false));	
		setControl(container);

		// Left Side
		Composite left = new Composite(container, SWT.NONE);
		left.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		left.setLayout(new GridLayout(1, false));	
		
		Label lblAvailableEEnumLiteral = new Label(left, SWT.NONE);
		lblAvailableEEnumLiteral.setAlignment(SWT.CENTER);
		lblAvailableEEnumLiteral.setLayoutData(new GridData(SWT.CENTER,SWT.TOP, true, false));
		lblAvailableEEnumLiteral.setText("Available Literals");
		
		availableEEnumLiteralTreeViewer = new TreeViewer(left, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION );
		availableEEnumLiteralTree = availableEEnumLiteralTreeViewer.getTree();
		GridData gd_availableEEnumLiteralTreeViewer = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		availableEEnumLiteralTree.setLayoutData(gd_availableEEnumLiteralTreeViewer);
		availableEEnumLiteralTree.setLinesVisible(true);
		
		availableEEnumLiteralTreeViewer.setContentProvider(new AvailableEEnumLiteralContentProvider(enumerationSwitchBinding, enumerationCase));
		availableEEnumLiteralTreeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		availableEEnumLiteralTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				if(!event.getSelection().isEmpty())
				{
					btnAdd.setEnabled(true);
				}
				else
				{
					btnAdd.setEnabled(false);
				}
			}
		});
		
		availableEEnumLiteralTreeViewer.setInput(this.enumerationSwitchBinding);						
		
		// Buttons.
		Composite center = new Composite(container, SWT.NONE);
		center.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		center.setLayout(new GridLayout(1, false));	
		
		btnAdd = new Button(center, SWT.NONE);
		btnAdd.setSize(74, 29);
		btnAdd.setText("->");
		btnAdd.setToolTipText("Adds the selected Literal(s).");
		btnAdd.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnAdd.setEnabled(false);
		btnAdd.addSelectionListener(new SelectionAdapter() 
		{					
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				List<EEnumLiteral> toAdd = getSelectedAvailableEEnumLiteral();
				ApogyCommonTransactionFacade.INSTANCE.basicAdd(enumerationCase, ApogyCommonTopologyBindingsPackage.Literals.ENUMERATION_CASE__ENUMERATION_LITERALS, toAdd, true);
				
				if(!selectedEEnumLiteralTreeViewer.isBusy()) 
				{
					selectedEEnumLiteralTreeViewer.refresh();
				}
				
				validate();	
				
				if(!availableEEnumLiteralTreeViewer.isBusy())
				{					
					availableEEnumLiteralTreeViewer.refresh(true);
				}
													
			}
		});
				
		btnRemove = new Button(center, SWT.NONE);
		btnRemove.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnRemove.setSize(74, 29);
		btnRemove.setText("<-");
		btnRemove.setToolTipText("Removes the selected Literal(s).");
		btnRemove.setEnabled(false);
		btnRemove.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				List<EEnumLiteral> toRemove = getSelectedEEnumLiteral();
				ApogyCommonTransactionFacade.INSTANCE.basicRemove(enumerationCase, ApogyCommonTopologyBindingsPackage.Literals.ENUMERATION_CASE__ENUMERATION_LITERALS, toRemove, true);
				
				if(!selectedEEnumLiteralTreeViewer.isBusy()) 
				{
					selectedEEnumLiteralTreeViewer.refresh();
				}
				
				validate();	
				
				if(!availableEEnumLiteralTreeViewer.isBusy())
				{					
					availableEEnumLiteralTreeViewer.refresh(true);
				}
			}
		});
		
		
		// Right Side
		Composite right = new Composite(container, SWT.NONE);
		right.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		right.setLayout(new GridLayout(1, false));	
		
		Label lblCaseEEnumLiteral = new Label(right, SWT.NONE);
		lblCaseEEnumLiteral.setAlignment(SWT.CENTER);
		lblCaseEEnumLiteral.setLayoutData(new GridData(SWT.CENTER,SWT.TOP, true, false));
		lblCaseEEnumLiteral.setText("Selected Literals");
		
		selectedEEnumLiteralTreeViewer = new TreeViewer(right, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION );
		selectedEEnumLiteralTree = selectedEEnumLiteralTreeViewer.getTree();
		GridData gd_selectedEEnumLiteralTreeViewer = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		selectedEEnumLiteralTree.setLayoutData(gd_selectedEEnumLiteralTreeViewer);
		selectedEEnumLiteralTree.setLinesVisible(true);
		
		selectedEEnumLiteralTreeViewer.setContentProvider(new EnumerationCaseLiteralContentProvider());
		selectedEEnumLiteralTreeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		selectedEEnumLiteralTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				if(!event.getSelection().isEmpty())
				{
					btnRemove.setEnabled(true);
				}
				else
				{
					btnRemove.setEnabled(false);
				}
			}
		});
		
		selectedEEnumLiteralTreeViewer.setInput(this.enumerationCase);	
		
		validate();			
	}
	
	@SuppressWarnings("unchecked")
	public List<EEnumLiteral> getSelectedAvailableEEnumLiteral()
	{
		return ((IStructuredSelection) availableEEnumLiteralTreeViewer.getSelection()).toList();
	}
	
	@SuppressWarnings("unchecked")
	public List<EEnumLiteral> getSelectedEEnumLiteral()
	{
		return ((IStructuredSelection) selectedEEnumLiteralTreeViewer.getSelection()).toList();
	}
	
	private void validate()
	{
		setErrorMessage(null);
		
		if(enumerationCase.getEnumerationLiterals().isEmpty())
		{
			setErrorMessage("The Enumeration Case contains no Literals !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}

	private EEnum resolveEENum(EnumerationSwitchBinding enumerationSwitchBinding)
	{		
		EEnum eEnum = null;
		
		if(enumerationSwitchBinding.getFeatureNode() instanceof AbstractFeatureSpecifier)
		{
			AbstractFeatureSpecifier abstractFeatureSpecifier = (AbstractFeatureSpecifier) enumerationSwitchBinding.getFeatureNode();
			if(abstractFeatureSpecifier.getStructuralFeature() != null)
			{
				if(abstractFeatureSpecifier.getStructuralFeature().getEType() instanceof EEnum)
				{
					eEnum = (EEnum) abstractFeatureSpecifier.getStructuralFeature().getEType();
				}				
			}
		}
		return eEnum;
	}
	
	public class EnumerationCaseLiteralContentProvider implements ITreeContentProvider 
	{

		@Override
		public Object[] getChildren(Object parent) 
		{
			if(parent instanceof EnumerationCase)
			{								
				EnumerationCase enumerationCase = (EnumerationCase) parent;					
				return enumerationCase.getEnumerationLiterals().toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getElements(Object inputElement) 
		{
			if(inputElement instanceof EnumerationCase)
			{								
				EnumerationCase enumerationCase = (EnumerationCase) inputElement;					
				return enumerationCase.getEnumerationLiterals().toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean hasChildren(Object parent) 
		{
			if(parent instanceof EnumerationCase)
			{								
				EnumerationCase enumerationCase = (EnumerationCase) parent;					
				return !enumerationCase.getEnumerationLiterals().isEmpty();
			}			
			return false;
		}
		
	}
	
	public class AvailableEEnumLiteralContentProvider implements ITreeContentProvider 
	{
		private EnumerationSwitchBinding enumerationSwitchBinding;
		private EnumerationCase enumerationCase;
		private EEnum eEnum;
		
		public AvailableEEnumLiteralContentProvider(EnumerationSwitchBinding enumerationSwitchBinding, EnumerationCase enumerationCase)
		{
			this.enumerationSwitchBinding = enumerationSwitchBinding;
			this.enumerationCase = enumerationCase;
			
			this.eEnum = resolveEENum(enumerationSwitchBinding);
		}
		
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof EnumerationSwitchBinding)
			{								
				EnumerationSwitchBinding bindingsList = (EnumerationSwitchBinding) inputElement;					
				return filter(bindingsList).toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof EnumerationSwitchBinding)
			{								
				EnumerationSwitchBinding bindingsList = (EnumerationSwitchBinding) parentElement;					
				return filter(bindingsList).toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{					
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof BindingsList)
			{
				EnumerationSwitchBinding bindingsList = (EnumerationSwitchBinding) element;				
				return !bindingsList.getCases().isEmpty();
			}		
			else
			{
				return false;
			}
		}
		
		private List<EEnumLiteral> filter(EnumerationSwitchBinding enumerationSwitchBinding)
		{
			List<EEnumLiteral> filtered = new ArrayList<EEnumLiteral>();
			if(eEnum != null)
			{
				List<EEnumLiteral> allEEnumLiteral = eEnum.getELiterals();
				
				for(EEnumLiteral eEnumLiteral : allEEnumLiteral)
				{
					if(!ApogyCommonTopologyBindingsUIFacade.INSTANCE.isEEnumLiteralInUse(enumerationSwitchBinding, eEnumLiteral))
					{
						filtered.add(eEnumLiteral);
					}
				}
				
				// Removes the eEnumLiteral from the current enumerationCase
				filtered.removeAll(enumerationCase.getEnumerationLiterals());
			}
			
			return filtered;
		}
	}
}

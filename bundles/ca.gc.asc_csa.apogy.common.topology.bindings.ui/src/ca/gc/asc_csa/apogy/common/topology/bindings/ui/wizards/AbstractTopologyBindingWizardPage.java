package ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureNode;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.FeatureRootsList;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.dialogs.AbstractFeatureNodeSelectionDialog;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class AbstractTopologyBindingWizardPage extends WizardPage {

	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.AbstractTopologyBindingWizardPage";		
	
	private AbstractTopologyBinding abstractTopologyBinding;
	private FeatureRootsList featureRootsList;
	@SuppressWarnings("rawtypes")
	private Class featureSuperClass = null;
	
	private Adapter adapter;
	
	private DataBindingContext m_bindingContext;
	private EditingDomain editingDomain;
	
	private Text nameText;
	private StyledText descriptionText;
	private Text featureNodeText;
	private Button selectFeatureButton;
	
	/**
	 * Constructor for the WizardPage.
	 * 
	 * @param pageName
	 */
	public AbstractTopologyBindingWizardPage() {
		super(WIZARD_PAGE_ID);
		setTitle("Name and Description");
		setDescription("Enter a name and a description (optional).");
	}

	public AbstractTopologyBindingWizardPage(FeatureRootsList featureRootsList, AbstractTopologyBinding abstractTopologyBinding) 
	{
		this();
		
		editingDomain = TransactionUtil.getEditingDomain(abstractTopologyBinding);
		
		this.abstractTopologyBinding = abstractTopologyBinding;
		this.featureRootsList = featureRootsList;
		this.featureSuperClass = abstractTopologyBinding.getSupportedFeatureType();
		
		validate();
	}
	
	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) 
	{		
		Composite container = new Composite(parent, SWT.BORDER);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		container.setLayout(new GridLayout(3, false));	
		
		// Name
		Label label = new Label(container, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		label.setText("Name : ");

		nameText = new Text(container, SWT.BORDER);
		nameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		// Description
		Label lblDescription = new Label(container, SWT.NONE);
		lblDescription.setText("Description : ");
		lblDescription.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		descriptionText = new StyledText(container, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		descriptionText.setAlwaysShowScrollBars(false);
		GridData gd_descriptionText = new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
		descriptionText.setLayoutData(gd_descriptionText);
						
		// Feature
		Label lblFeature = new Label(container, SWT.NONE);
		lblFeature.setText("Abstract Feature Node : ");
		lblFeature.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		featureNodeText = new Text(container, SWT.BORDER);
		featureNodeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		featureNodeText.setEditable(false);
		
		selectFeatureButton = new Button(container, SWT.PUSH);
		selectFeatureButton.setText("Select");
		selectFeatureButton.setToolTipText("Select the Feature this Binding if related to.");
		selectFeatureButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				AbstractFeatureNodeSelectionDialog dialog = new AbstractFeatureNodeSelectionDialog(getShell(), featureRootsList, featureSuperClass);
				dialog.open();
				
				AbstractFeatureNode selectedAbstractFeatureNode = dialog.getSelectedAbstractFeatureNode();
				ApogyCommonTransactionFacade.INSTANCE.basicSet(abstractTopologyBinding, ApogyCommonTopologyBindingsPackage.Literals.ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE, selectedAbstractFeatureNode);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) 
			{
			}
		});		
		
		setControl(container);
	
		if(abstractTopologyBinding != null) abstractTopologyBinding.eAdapters().add(getAdapter());
		
		validate();
		
		m_bindingContext = initCustomDataBindings();
	}

	@Override
	public void dispose() 
	{
		super.dispose();
		
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		if(abstractTopologyBinding != null) abstractTopologyBinding.eAdapters().remove(getAdapter());
	}

	/**
	 * This method is invoked to validate the content.
	 */
	protected void validate() 
	{
		String errorStr = null;
		String infoStr = null;

		if (abstractTopologyBinding.getName() == null || abstractTopologyBinding.getName().isEmpty()) 
		{
			errorStr = "A name must be provided.";
		}
		else if(abstractTopologyBinding.getFeatureNode() == null)
		{
			errorStr = "A feature must be selected.";
		}

		setMessage(infoStr);
		setErrorMessage(errorStr);
		setPageComplete(errorStr == null);
	}
			
	@SuppressWarnings("unchecked")
	private DataBindingContext initCustomDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		// Name
		IObservableValue<?> nameObserveWidget = WidgetProperties.text(SWT.Modify).observe(nameText);				
		IObservableValue<?> nameObserveValue = (editingDomain == null
				? EMFProperties.value(ApogyCommonTopologyBindingsPackage.Literals.ABSTRACT_TOPOLOGY_BINDING__NAME).observe(abstractTopologyBinding)
				: EMFEditProperties.value(editingDomain, ApogyCommonTopologyBindingsPackage.Literals.ABSTRACT_TOPOLOGY_BINDING__NAME).observe(abstractTopologyBinding));

		
		bindingContext.bindValue(nameObserveWidget, nameObserveValue, null, null);

		
		// Description
		IObservableValue<?> descriptionObserveWidget = WidgetProperties.text(new int[] { SWT.Modify, SWT.FocusOut, SWT.DefaultSelection }).observeDelayed(500, descriptionText);
		
		IObservableValue<?> descriptionObserveValue = (editingDomain == null
				? EMFProperties.value(ApogyCommonTopologyBindingsPackage.Literals.ABSTRACT_TOPOLOGY_BINDING__DESCRIPTION).observe(abstractTopologyBinding)
				: EMFEditProperties.value(editingDomain, ApogyCommonTopologyBindingsPackage.Literals.ABSTRACT_TOPOLOGY_BINDING__DESCRIPTION).observe(abstractTopologyBinding));

		
		bindingContext.bindValue(descriptionObserveWidget, descriptionObserveValue, null, null);
		
		// Feature
		IObservableValue<?> featureObserveWidget = WidgetProperties.text(SWT.Modify).observe(featureNodeText);	
		IObservableValue<?> featureObserveValue = (editingDomain == null
				? EMFProperties.value(ApogyCommonTopologyBindingsPackage.Literals.ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE).observe(abstractTopologyBinding)
				: EMFEditProperties.value(editingDomain, ApogyCommonTopologyBindingsPackage.Literals.ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE).observe(abstractTopologyBinding));

		bindingContext.bindValue(featureObserveWidget, 
								 featureObserveValue, 
								 new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), 
								 new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE).setConverter(new Converter(AbstractFeatureNode.class, String.class) 
								 {									
									@Override
									public Object convert(Object arg0) 
									{	
										if(arg0 instanceof AbstractFeatureNode)
										{
											AbstractFeatureNode node = (AbstractFeatureNode) arg0;
											return ApogyCommonEMFFacade.INSTANCE.getAncestriesString(node);
										}
										return null;
									}
								}));
		
		return bindingContext;
	}

	private Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					validate();
				}
			};
		}
		return adapter;
	}	
}
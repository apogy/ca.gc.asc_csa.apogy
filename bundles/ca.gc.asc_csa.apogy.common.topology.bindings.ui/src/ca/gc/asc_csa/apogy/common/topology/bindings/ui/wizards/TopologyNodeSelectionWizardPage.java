package ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.NodeListSearchComposite;

public abstract class TopologyNodeSelectionWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.NodeSelectionWizardPage";		

	private EClass nodeType;
	private List<Node> availableNodes;
	private Node selectedNode = null;
	
	private NodeListSearchComposite nodeListSearchComposite;

	protected AbstractTopologyBinding abstractTopologyBinding;
	
	public TopologyNodeSelectionWizardPage(String title, String description, AbstractTopologyBinding abstractTopologyBinding, EClass nodeType, List<Node> availableNodes) 
	{
		super(WIZARD_PAGE_ID);
		setTitle(title);
		setDescription(description);
		
		this.abstractTopologyBinding = abstractTopologyBinding;
		this.nodeType = nodeType;
		this.availableNodes = availableNodes;
		
		
		setPageComplete(false);
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.BORDER);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		container.setLayout(new GridLayout(3, false));					
		
		// Add a selection composite for Node.		
		nodeListSearchComposite = new NodeListSearchComposite(container, SWT.BORDER, availableNodes)
		{
			@Override
			public void nodeSelectedChanged(Node nodeSelected) 
			{
				selectedNode = nodeSelected;
				nodeSelected(nodeSelected);
				validate();
			}
		};
		
		nodeListSearchComposite.initializeTypeFilter(nodeType);
		nodeListSearchComposite.applyFilters();
		
		setControl(container);
		validate();
	}
	
	/**
	 * Returns the Node currently selected.
	 * @return The node currently selected. Can be null.
	 */
	public Node getSelectedNode()
	{
		return this.selectedNode;
	}
	
	abstract protected void nodeSelected(Node nodeSelected);
	
	/**
	 * This method is invoked to validate the content.
	 */
	protected void validate() 
	{			
		setPageComplete(getErrorMessage() == null);
	}
}

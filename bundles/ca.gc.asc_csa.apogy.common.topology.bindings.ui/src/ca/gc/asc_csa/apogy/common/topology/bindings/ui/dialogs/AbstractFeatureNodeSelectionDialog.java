package ca.gc.asc_csa.apogy.common.topology.bindings.ui.dialogs;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureNode;
import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureSpecifier;
import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureTreeNode;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.topology.bindings.FeatureRootsList;

public class AbstractFeatureNodeSelectionDialog extends Dialog
{
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	private FeatureRootsList featureRootsList;
	private AbstractFeatureNode selectedAbstractFeatureNode;
	
	@SuppressWarnings("rawtypes")
	private Class featureSuperClass = null;
	
	private Tree tree;
	private TreeViewer treeViewer;
	private Text txtSelectedAbstractFeatureTreeNode;

	public AbstractFeatureNodeSelectionDialog(Shell parentShell) 
	{
	        super(parentShell);
	}
	
	@SuppressWarnings("rawtypes")
	public AbstractFeatureNodeSelectionDialog(Shell parentShell, FeatureRootsList featureRootsList, Class featureSuperClass) 
	{
	        super(parentShell);
	        this.featureRootsList = featureRootsList;
	        this.featureSuperClass = featureSuperClass;
	}

	public AbstractFeatureNode getSelectedAbstractFeatureNode() 
	{
		return selectedAbstractFeatureNode;
	}

	@Override
    protected void configureShell(Shell newShell) 
	{
        super.configureShell(newShell);
        newShell.setText("Feature Selection dialog");
    }
	
	@Override
	protected void buttonPressed(int buttonId) 
	{
		if(buttonId == Dialog.CANCEL)
		{
			setSelectedAbstractFeatureNode(null);
		}	
		else
		{
			if(selectedAbstractFeatureNode instanceof AbstractFeatureSpecifier)
			{
			}
			else
			{
				setSelectedAbstractFeatureNode(null);
			}			
		}
		
		
		super.buttonPressed(buttonId);
	}
	
	@Override
    protected Point getInitialSize() 
	{
        return new Point(450, 450);
    }
	
	@Override
	protected Control createDialogArea(Composite parent) 
	{
		Composite container = (Composite) super.createDialogArea(parent);		
		
		Composite top = new Composite(container, SWT.NONE);
		top.setLayout(new GridLayout(2,false));
		GridData gd_top = new GridData(SWT.FILL, SWT.FILL, true, true);
		top.setLayoutData(gd_top);
		
		Label lblSelectedAbstractFeatureTreeNode = new Label(top, SWT.NONE);
		lblSelectedAbstractFeatureTreeNode.setText("Selected Feature Node : ");
		
		txtSelectedAbstractFeatureTreeNode = new Text(top, SWT.NONE);
		txtSelectedAbstractFeatureTreeNode.setEditable(false);
		GridData gd_txtSelectedAbstractFeatureTreeNode = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		txtSelectedAbstractFeatureTreeNode.setLayoutData(gd_txtSelectedAbstractFeatureTreeNode);
		
		treeViewer = new TreeViewer(top, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION );
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gd_tree.widthHint = 400;
		gd_tree.minimumWidth = 400;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		treeViewer.setLabelProvider(new CustomAdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				setSelectedAbstractFeatureNode((AbstractFeatureTreeNode)((IStructuredSelection) event.getSelection()).getFirstElement());					
			}
		});
		
		if(featureRootsList != null && !featureRootsList.getFeatureRoots().isEmpty())
		{
			treeViewer.setInput(featureRootsList.getFeatureRoots().get(0));
		}
		
		return top;
	}
	
	private void setSelectedAbstractFeatureNode(final AbstractFeatureNode newSelectedAbstractFeatureNode)
	{				
		if(isCompatibleWithFeatureSuperClass((AbstractFeatureSpecifier) newSelectedAbstractFeatureNode))
		{
			this.selectedAbstractFeatureNode = newSelectedAbstractFeatureNode;			
		}
		else
		{
			this.selectedAbstractFeatureNode = null;
		}
		
		getShell().getDisplay().asyncExec(new Runnable() 
		{		
			@Override
			public void run() 
			{
				if(txtSelectedAbstractFeatureTreeNode != null && !txtSelectedAbstractFeatureTreeNode.isDisposed())
				{
					if(selectedAbstractFeatureNode != null)
					{
						txtSelectedAbstractFeatureTreeNode.setText(ApogyCommonEMFFacade.INSTANCE.getAncestriesString(newSelectedAbstractFeatureNode));
					}
					else
					{
						txtSelectedAbstractFeatureTreeNode.setText("");
					}
				}
			}
		});
	}
	
	private class CustomAdapterFactoryLabelProvider extends AdapterFactoryLabelProvider
	{
		public CustomAdapterFactoryLabelProvider(AdapterFactory adapterFactory) 
		{
			super(adapterFactory);		
		}
		
		@Override
		public Image getImage(Object object) 
		{
			if(featureSuperClass != null)
			{			
				if(object instanceof AbstractFeatureSpecifier)
				{
					AbstractFeatureSpecifier abstractFeatureSpecifier = (AbstractFeatureSpecifier) object;
					
					if(isCompatibleWithFeatureSuperClass(abstractFeatureSpecifier))
					{
						return super.getImage(object);
					}
					else
					{
						return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE);
					}
				}				
			}
			return super.getImage(object);
		}		
	}		
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private boolean isCompatibleWithFeatureSuperClass(AbstractFeatureSpecifier abstractFeatureSpecifier)
	{		
		if(abstractFeatureSpecifier != null && abstractFeatureSpecifier.getStructuralFeature() != null)
		{
			EStructuralFeature eStructuralFeature = abstractFeatureSpecifier.getStructuralFeature();
			if(eStructuralFeature instanceof EAttribute)
			{
				EAttribute eAttribute = (EAttribute) eStructuralFeature;
				Class attributeClass = autoBox(eAttribute.getEAttributeType().getInstanceClass());																												
										
				return featureSuperClass.isAssignableFrom(attributeClass);
			}
			else if(eStructuralFeature instanceof EReference)
			{
				EReference eReference = (EReference) eStructuralFeature;
				Class referrencedClass = eReference.getEType().getInstanceClass();
											
				return featureSuperClass.isAssignableFrom(referrencedClass);
			}						
		}
		return false;
	}
	
	@SuppressWarnings("rawtypes")
	private Class autoBox(Class originalClass)
	{
		Class autoboxedClass = originalClass;
		
		if(originalClass.getName() == "boolean")
		{
			autoboxedClass = Boolean.class;
		}
		else if(originalClass.getName() == "byte")
		{
			autoboxedClass = Byte.class;
		}
		else if(originalClass.getName() == "char")
		{
			autoboxedClass = Character.class;
		}
		else if(originalClass.getName() == "float")
		{
			autoboxedClass = Float.class;
		}
		else if(originalClass.getName() == "int")
		{
			autoboxedClass = Integer.class;
		}
		else if(originalClass.getName() == "long")
		{
			autoboxedClass = Long.class;
		}
		else if(originalClass.getName() == "short")
		{
			autoboxedClass = Short.class;
		}
		else if(originalClass.getName() == "double")
		{
			autoboxedClass = Double.class;
		}
		
		return autoboxedClass;
	}
}

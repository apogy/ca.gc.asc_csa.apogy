package ca.gc.asc_csa.apogy.common.topology.bindings.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureNode;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class AbstractTopologyBindingOverviewComposite extends Composite 
{
	private AbstractTopologyBinding abstractTopologyBinding;
	private DataBindingContext m_bindingContext;
	
	private Text txtNamevalue;
	private Text txtDescriptionvalue;
	private Text txtAbstractFeatureTreeNode;
	
	public AbstractTopologyBindingOverviewComposite(Composite parent, int style) 
	{		
		super(parent, SWT.NO_BACKGROUND);	
		setLayout(new GridLayout(3, false));		
		
		// Name
		Label lblName = new Label(this, SWT.NONE);
		lblName.setAlignment(SWT.RIGHT);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name:");
		
		txtNamevalue = new Text(this, SWT.NONE);
		GridData gd_txtNamevalue = new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1);
		gd_txtNamevalue.minimumWidth = 300;
		gd_txtNamevalue.widthHint = 300;
		txtNamevalue.setLayoutData(gd_txtNamevalue);
		
		// Description
		Label lblDescription = new Label(this, SWT.NONE);
		lblDescription.setAlignment(SWT.RIGHT);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDescription.setText("Description:");
		
		txtDescriptionvalue = new Text(this, SWT.NONE | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtDescriptionvalue = new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1);
		gd_txtDescriptionvalue.minimumWidth = 300;
		gd_txtDescriptionvalue.widthHint = 300;
		gd_txtDescriptionvalue.minimumHeight = 50;
		gd_txtDescriptionvalue.heightHint = 50;
		txtDescriptionvalue.setLayoutData(gd_txtDescriptionvalue);		
		
		// AbstractFeatureTreeNode
		Label AbstractFeatureTreeNode = new Label(this, SWT.NONE);
		AbstractFeatureTreeNode.setAlignment(SWT.RIGHT);
		AbstractFeatureTreeNode.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		AbstractFeatureTreeNode.setText("Feature :");
		
		txtAbstractFeatureTreeNode = new Text(this, SWT.NONE);
		txtAbstractFeatureTreeNode.setEnabled(false);
		GridData gd_txtAbstractFeatureTreeNode = new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1);
		gd_txtAbstractFeatureTreeNode.minimumWidth = 300;
		gd_txtAbstractFeatureTreeNode.widthHint = 300;
		txtAbstractFeatureTreeNode.setLayoutData(gd_txtAbstractFeatureTreeNode);
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();				
			}
		});
	}

	public AbstractTopologyBinding getAbstractTopologyBinding() {
		return abstractTopologyBinding;
	}

	public void setAbstractTopologyBinding(AbstractTopologyBinding newAbstractTopologyBinding) 
	{	
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.abstractTopologyBinding = newAbstractTopologyBinding;
		
		if(newAbstractTopologyBinding != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}				
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		/* Name Value. */
		IObservableValue<Double> observeName = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonTopologyBindingsPackage.Literals.ABSTRACT_TOPOLOGY_BINDING__NAME)).observe(getAbstractTopologyBinding());
		IObservableValue<String> observeNameValueText = WidgetProperties.text(SWT.Modify).observe(txtNamevalue);

		bindingContext.bindValue(observeNameValueText,
								observeName, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		/* Description Value. */
		IObservableValue<Double> observeDescription = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonTopologyBindingsPackage.Literals.ABSTRACT_TOPOLOGY_BINDING__DESCRIPTION)).observe(getAbstractTopologyBinding());
		IObservableValue<String> observeDescriptionText = WidgetProperties.text(SWT.Modify).observe(txtDescriptionvalue);

		bindingContext.bindValue(observeDescriptionText,
								 observeDescription, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		// AbstractFeatureTreeNode
		IObservableValue observeAbstractFeatureValue = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  									   FeaturePath.fromList(ApogyCommonTopologyBindingsPackage.Literals.ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE)).observe(getAbstractTopologyBinding());

		IObservableValue<String> observeAbstractFeatureTreeNodeText = WidgetProperties.text(SWT.Modify).observe(txtAbstractFeatureTreeNode);

		bindingContext.bindValue(observeAbstractFeatureTreeNodeText,
								observeAbstractFeatureValue, 
				 				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), 
				 				new UpdateValueStrategy().setConverter(new Converter(AbstractFeatureNode.class, String.class)
				 				{																		 											
									@Override
									public Object convert(Object fromObject) 
									{											
										if(fromObject instanceof AbstractFeatureNode)
										{
											AbstractFeatureNode abstractFeatureNode = (AbstractFeatureNode) fromObject;
											return ApogyCommonEMFFacade.INSTANCE.getAncestriesString(abstractFeatureNode);
										}
										else
										{
											return "";
										}
									}
				 				}));
		
		return bindingContext;
	}
}

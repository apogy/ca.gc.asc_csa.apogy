package ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationCase;
import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationSwitchBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.composites.EnumerationSwitchBindingCasesComposite;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.TopologyTreeEditingComposite;

public class EnumerationSwitchBindingWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.EnumerationSwitchBindingWizardPage";		

	private EnumerationSwitchBinding enumerationSwitchBinding;
	
	private EnumerationSwitchBindingCasesComposite  enumerationSwitchBindingCasesComposite;
	private TopologyTreeEditingComposite topologyTreeComposite;
	
	public EnumerationSwitchBindingWizardPage(EnumerationSwitchBinding enumerationSwitchBinding)
	{
		super(WIZARD_PAGE_ID);
		setTitle("Enum Cases Topologies.");
		setDescription("Define the enum cases topologies.");
		
		this.enumerationSwitchBinding = enumerationSwitchBinding;
	}
	
	@Override
	public void createControl(Composite parent) 
	{		
		Composite container = new Composite(parent, SWT.BORDER);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		container.setLayout(new GridLayout(2, true));	
		setControl(container);		
		
		enumerationSwitchBindingCasesComposite = new EnumerationSwitchBindingCasesComposite(container, SWT.BORDER)
		{
			@Override
			protected void enumerationCaseSelected(EnumerationCase selectedEnumerationCase) 
			{
				topologyTreeComposite.setRoot(selectedEnumerationCase.getTopologyRoot());
			}
		};
		enumerationSwitchBindingCasesComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		enumerationSwitchBindingCasesComposite.setEnumerationSwitchBinding(enumerationSwitchBinding);
		
		topologyTreeComposite = new TopologyTreeEditingComposite(container, SWT.BORDER, true);
		topologyTreeComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));		
	}
}

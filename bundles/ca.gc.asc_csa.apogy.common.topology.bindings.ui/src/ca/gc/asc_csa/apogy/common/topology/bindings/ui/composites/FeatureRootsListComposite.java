package ca.gc.asc_csa.apogy.common.topology.bindings.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureSpecifier;
import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureTreeNode;
import ca.gc.asc_csa.apogy.common.emf.AbstractRootNode;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.topology.bindings.FeatureRootsList;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class FeatureRootsListComposite extends Composite 
{
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	private FeatureRootsList featureRootsList;
	private AbstractFeatureTreeNode selectedAbstractFeatureTreeNode = null;
	private boolean editable = true;
	
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;	
	
	private DataBindingContext m_bindingContext;
	
	public FeatureRootsListComposite(Composite parent, int style) 
	{
		this(parent, style, true);
	}
		
	public FeatureRootsListComposite(Composite parent, int style, boolean editable) 
	{
		super(parent, style);
		this.editable = editable;
		
		if(editable)
		{
			setLayout(new GridLayout(2, false));
		}
		else
		{
			setLayout(new GridLayout(1, false));
		}
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION );
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				AbstractFeatureTreeNode selectedNode = (AbstractFeatureTreeNode)((IStructuredSelection) event.getSelection()).getFirstElement();				
				setSelectedAbstractFeatureTreeNode(selectedNode);
				newAbstractFeatureTreeNodeSelected(selectedNode);					
			}
		});
		
		if(editable)
		{
			// Buttons.
			Composite composite = new Composite(this, SWT.NONE);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
			composite.setLayout(new GridLayout(1, false));	
			
			btnNew = new Button(composite, SWT.NONE);
			btnNew.setSize(74, 29);
			btnNew.setText("New");
			btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnNew.setEnabled(true);
			btnNew.addListener(SWT.Selection, new Listener() 
			{		
				@Override
				public void handleEvent(Event event) 
				{				
					if (event.type == SWT.Selection) 
					{	
						AbstractFeatureTreeNode parent = null;
						if(getSelectedAbstractFeatureTreeNode() == null)
						{
							parent = getFeatureRootsList().getFeatureRoots().get(0);
						}
						else
						{
							parent = getSelectedAbstractFeatureTreeNode();
						}
						
						MapBasedEClassSettings settings = ApogyCommonEMFUIFactory.eINSTANCE.createMapBasedEClassSettings();
						settings.getUserDataMap().put("parent", parent);		
						
						ApogyEObjectWizard wizard = new ApogyEObjectWizard(ApogyCommonEMFPackage.Literals.ABSTRACT_FEATURE_TREE_NODE__CHILDREN, parent, settings, ApogyCommonEMFPackage.Literals.TREE_FEATURE_NODE); 
						WizardDialog dialog = new WizardDialog(getShell(), wizard);
						dialog.open();
						
						// Forces the viewer to refresh its input.
						if(!treeViewer.isBusy())
						{					
							treeViewer.setInput(getFeatureRootsList().getFeatureRoots().get(0));
							
							if(wizard.getCreatedEObject() != null)
							{
								treeViewer.setSelection(new StructuredSelection(wizard.getCreatedEObject()));
							}
						}					
					}
				}
			});
					
			btnDelete = new Button(composite, SWT.NONE);
			btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnDelete.setSize(74, 29);
			btnDelete.setText("Delete");
			btnDelete.setEnabled(false);
			btnDelete.addSelectionListener(new SelectionAdapter() 
			{
				@Override
				public void widgetSelected(SelectionEvent event) 
				{
					String deleteMessage = "";			
					
					Iterator<AbstractFeatureTreeNode> treeNodeIterator = getSelectedAbstractFeatureTreeNodes().iterator();
					while (treeNodeIterator.hasNext()) 
					{
						// TODO
						AbstractFeatureTreeNode treeNode = treeNodeIterator.next();
						
						if(treeNode instanceof AbstractFeatureSpecifier)
						{
							AbstractFeatureSpecifier abstractFeatureSpecifier = (AbstractFeatureSpecifier) treeNode;
							
							if( abstractFeatureSpecifier.getStructuralFeature() != null)
							{
								deleteMessage = abstractFeatureSpecifier.getStructuralFeature().getName();
							}
						}					
	
						if (treeNodeIterator.hasNext()) 
						{
							deleteMessage = deleteMessage + ", ";
						}
					}
	
					MessageDialog dialog = new MessageDialog(null, "Delete the selected Features", null,
							"Are you sure to delete these Features: " + deleteMessage, MessageDialog.QUESTION,
							new String[] { "Yes", "No" }, 1);
					int result = dialog.open();
					if (result == 0) 
					{
						List<AbstractFeatureTreeNode> listOfNodesToRemove = getSelectedAbstractFeatureTreeNodes();
						
						for(AbstractFeatureTreeNode nodeToRemove : listOfNodesToRemove)
						{
							if(!(nodeToRemove instanceof AbstractRootNode))
							{
								ApogyCommonTransactionFacade.INSTANCE.basicRemove(nodeToRemove.getParent(), ApogyCommonEMFPackage.Literals.ABSTRACT_FEATURE_TREE_NODE__CHILDREN, nodeToRemove);
							}
						}										
					}
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(getFeatureRootsList().getFeatureRoots().get(0));
					}					
				}
			});	
		}
		// Cleanup on dispose
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (m_bindingContext != null) m_bindingContext.dispose();			
			}
		});
	}

	public FeatureRootsList getFeatureRootsList() {
		return featureRootsList;
	}

	public void setFeatureRootsList(FeatureRootsList newFeatureRootsList) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.featureRootsList = newFeatureRootsList;	
		
		if(newFeatureRootsList != null)
		{
			m_bindingContext = initDataBindingsCustom();
			
			if(newFeatureRootsList.getFeatureRoots() != null && newFeatureRootsList.getFeatureRoots().size() > 0)
			{
				treeViewer.setInput(newFeatureRootsList.getFeatureRoots().get(0));
			}			
			
			AbstractFeatureTreeNode abtractFeatureTreeNode = null;
			
			if(!newFeatureRootsList.getFeatureRoots().isEmpty())
			{
				abtractFeatureTreeNode = newFeatureRootsList.getFeatureRoots().get(0);
			}
			
			if(abtractFeatureTreeNode != null)
			{
				treeViewer.setSelection(new StructuredSelection(abtractFeatureTreeNode), true);
			}
		}					
	}
	
	public AbstractFeatureTreeNode getSelectedAbstractFeatureTreeNode() {
		return selectedAbstractFeatureTreeNode;
	}

	public void setSelectedAbstractFeatureTreeNode(AbstractFeatureTreeNode selectedAbstractFeatureTreeNode) {
		this.selectedAbstractFeatureTreeNode = selectedAbstractFeatureTreeNode;
	}

	@SuppressWarnings("unchecked")
	public List<AbstractFeatureTreeNode> getSelectedAbstractFeatureTreeNodes()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	protected void newAbstractFeatureTreeNodeSelected(AbstractFeatureTreeNode selectedAbstractFeatureTreeNode)
	{		
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
		
		if(editable)
		{
			/* Delete Button Enabled Binding. */
			IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
			bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
					new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
							.setConverter(new Converter(Object.class, Boolean.class) {
								@Override
								public Object convert(Object fromObject) {
									return fromObject != null;
								}
							}));
		}
		return bindingContext;
	}
}

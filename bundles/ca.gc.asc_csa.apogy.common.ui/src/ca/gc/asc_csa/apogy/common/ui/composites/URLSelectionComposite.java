package ca.gc.asc_csa.apogy.common.ui.composites;

import java.io.File;
import java.net.URL;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.ui.Activator;
import ca.gc.asc_csa.apogy.common.ui.dialogs.PluginResourcesURLDialog;

@SuppressWarnings("unused")
/**
 * Composite providing selection of URLs from the workspace, installed plug-ins or file system.
 * @author pallard
 *
 */
public class URLSelectionComposite extends Composite 
{			
	private boolean workspaceURLEnabled = true;
	private boolean instalationURLEnabled = true;
	private boolean fileSystemURLEnabled = true;
	private String[] fileExtensions = null;
	
	private String urlString = null;
	private Text txtUrltext;
			
	public URLSelectionComposite(Composite parent, int style)
	{
		this(parent, style, null, true, true, true);
	}
	
	/**
	 * 
	 * @param parent The parent composite.
	 * @param style The style.
	 * @param fileExtensions  An array containing the file extensions to use as filters. For example new String[]{"*.txt","*.gif"}
	 * @param workspaceURLEnabled Whether or not to activate workspace scope selection.
	 * @param instalationURLEnabled Whether or not to activate installed plug-ins scope selection.
	 * @param fileSystemURLEnabled Whether or not to activate file system selection.
	 */
	public URLSelectionComposite(Composite parent, int style, String[] fileExtensions, boolean workspaceURLEnabled, boolean instalationURLEnabled, boolean fileSystemURLEnabled) 
	{
		super(parent, style);	
		setLayout(new GridLayout(4, false));
		
		this.workspaceURLEnabled = workspaceURLEnabled;
		this.instalationURLEnabled = instalationURLEnabled;
		this.fileSystemURLEnabled = fileSystemURLEnabled;		
		
		setFileExtension(fileExtensions);
		
		Label lblNewLabel = new Label(this, SWT.NONE);
		lblNewLabel.setAlignment(SWT.RIGHT);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText("URL:");
		
		txtUrltext = new Text(this, SWT.BORDER | SWT.WRAP);
		GridData gd_txtUrltext = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 3);
		gd_txtUrltext.heightHint = 50;
		gd_txtUrltext.minimumHeight = 50;
		txtUrltext.setLayoutData(gd_txtUrltext);
		
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));
		GridData gd_composite = new GridData(SWT.LEFT, SWT.TOP, false, true, 1, 3);
		gd_composite.widthHint = 110;
		gd_composite.minimumWidth = 110;
		composite.setLayoutData(gd_composite);	
		
		if(workspaceURLEnabled)
		{
			Button btnWorkspace = new Button(composite, SWT.NONE);
			btnWorkspace.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			btnWorkspace.setSize(110, 33);
			btnWorkspace.setText("Workspace...");
			btnWorkspace.setToolTipText("Selects a URL refering to a ressource in the current project.");			
			btnWorkspace.addSelectionListener(new SelectionListener() 
			{			
				@Override
				public void widgetSelected(SelectionEvent e) 
				{
					
					String currentDir =  ResourcesPlugin.getWorkspace().getRoot().getLocation().toString();
					
					FileDialog fileChooser = new FileDialog(URLSelectionComposite.this.getShell(), SWT.OPEN);
					fileChooser.setText("Select a file:");
					fileChooser.setFilterPath(currentDir);
					
					String[] extensions = URLSelectionComposite.this.fileExtensions;
					if(extensions == null || extensions.length == 0)
					{
						extensions = new String[]{"*.*"};
					}
					fileChooser.setFilterExtensions(fileExtensions);		
					String filename = fileChooser.open();
					
					// Adds the file prefix
					if(filename != null)
					{
						try
						{
							filename = filename.replace(File.separatorChar, '/');
							String workspaceDir = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString();
							int index = filename.lastIndexOf(workspaceDir);
							if(index != -1)
							{
								index += workspaceDir.length();
								filename = filename.substring(index);
							}
							
							String pathAsURL = filename.replace(File.separatorChar, '/');
							
							String newURLString = "platform:/resource";
							if(pathAsURL.startsWith("/"))
							{
								newURLString += pathAsURL;
							}
							else
							{
								newURLString += "/" + pathAsURL;
							}
			
							setUrlString(newURLString);
							urlStringSelected(newURLString);
						}
						catch (Exception ex) 
						{
							ex.printStackTrace();							
						}
					}
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) 
				{		
				}
				
			});
		}
		
		if(instalationURLEnabled)
		{
			Button btnInstallation = new Button(composite, SWT.NONE);
			btnInstallation.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			btnInstallation.setSize(110, 33);
			btnInstallation.setText("Installation...");		
			btnInstallation.setToolTipText("Selects a URL refering to a ressource located in one installed plugin.");
			btnInstallation.addSelectionListener(new SelectionListener() 
			{			
				@Override
				public void widgetSelected(SelectionEvent e) 
				{						
					PluginResourcesURLDialog dialog = new PluginResourcesURLDialog(getShell(), fileExtensions);
	
					if(dialog.open() == 0)
					{						
						try
						{
							String pathAsURL = dialog.getSelectedPath().replace(File.separatorChar, '/');
							String newURLString = "platform:/plugin/" + dialog.getSelectedPluginSymbolicName() + pathAsURL;
							setUrlString(newURLString);
							urlStringSelected(newURLString);
						}
						catch (Exception ex) 
						{
							Logger.INSTANCE.log(Activator.ID, "Could not create URL for <" + dialog.getSelectedPath() + ">.", EventSeverity.ERROR);
						}
					}
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) 
				{		
				}
			});
		}
	
		if(fileSystemURLEnabled)
		{
			Button btnFileSystem = new Button(composite, SWT.NONE);
			btnFileSystem.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			btnFileSystem.setSize(110, 33);
			btnFileSystem.setText("File System...");
			btnFileSystem.setToolTipText("Selects a URL refering to a ressource located on the file system.");						
			btnFileSystem.addSelectionListener(new SelectionListener() 
			{			
				@Override
				public void widgetSelected(SelectionEvent e) 
				{			
					String currentDir = System.getProperty("user.dir");
					
					FileDialog fileChooser = new FileDialog(URLSelectionComposite.this.getShell(), SWT.OPEN);
					fileChooser.setText("Select a file:");
					fileChooser.setFilterPath(currentDir);
					
					String[] extensions = URLSelectionComposite.this.fileExtensions;
					if(extensions == null || extensions.length == 0)
					{
						extensions = new String[]{"*.*"};
					}
					fileChooser.setFilterExtensions(fileExtensions);		
					String filename = fileChooser.open();
					
					// Adds the file prefix
					if(filename != null)
					{
						try
						{
							String newURLString = convertFilenameToURL(filename);
							setUrlString(newURLString);
							urlStringSelected(newURLString);
						}
						catch (Exception ex) 
						{
							ex.printStackTrace();
						}
					}
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) 
				{		
				}
			});
		}
	}

	public String getUrlString() 
	{
		return txtUrltext.getText();
	}
	
	public void setUrlString(String newURLString) 
	{
		this.urlString = newURLString;
		txtUrltext.setText(newURLString);
	}
	
	public void setFileExtension(String[] fileExtensions)
	{
		this.fileExtensions = fileExtensions;
	}	
	
	/**
	 * Method called upon selection of a valie URL. This method can be overloaded by user.
	 * @param newURLString The URL string selected.
	 */
	protected void urlStringSelected(String newURLString)
	{		
	}
	
	private String convertFilenameToURL(String filename) throws Exception
	{		
		File tmpFile = new File(filename);
		URL url = tmpFile.toURI().toURL();
		return url.toString();
	}
}

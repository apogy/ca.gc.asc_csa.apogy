package ca.gc.asc_csa.apogy.common.ui.composites;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class ClosableNoContentComposite extends NoContentComposite {
	
	public ClosableNoContentComposite(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	protected void createContent(Composite parent) {
		setLayout(new GridLayout(1, false));
		
		Composite closeComposite = new Composite(this, SWT.None);
		closeComposite.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		closeComposite.setLayout(new FillLayout());

		Button btnClose = new Button(closeComposite, SWT.None);
		ImageDescriptor image = AbstractUIPlugin.imageDescriptorFromPlugin(
				"org.eclipse.ui",
				"icons/full/elcl16/close_view.png");
		btnClose.setImage(image.createImage());
		btnClose.addSelectionListener(getSelectionListener());
		
		Composite noContenLabelComposite = new Composite(this, SWT.None);
		noContenLabelComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		GridLayout gridLayout_noContenLabel = new GridLayout(1, false);
		gridLayout_noContenLabel.marginWidth = 0;
		gridLayout_noContenLabel.marginHeight = 0;
		noContenLabelComposite.setLayout(gridLayout_noContenLabel);

		Label lblNoActiveSession = new Label(noContenLabelComposite, SWT.None);
		lblNoActiveSession.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblNoActiveSession.setText(getMessage());
	}
	
	protected SelectionListener getSelectionListener(){
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ClosableNoContentComposite.this.dispose();
			}
		}; 
	}
}
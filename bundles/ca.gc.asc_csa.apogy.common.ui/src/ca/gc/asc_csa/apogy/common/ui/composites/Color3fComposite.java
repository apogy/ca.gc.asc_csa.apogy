package ca.gc.asc_csa.apogy.common.ui.composites;

import javax.vecmath.Color3f;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class Color3fComposite extends Composite 
{
	private EObject owner = null;
	private EStructuralFeature feature = null;
	private Adapter adapter;

	private boolean enableEditing = true;
	
	private ColorFieldEditor colorFieldEditor;
			
	public Color3fComposite(Composite parent, int style) {
		this(parent, style, "", true, null, null);		
	}

	public Color3fComposite(Composite parent, int style, String label, boolean enableEditing, EObject owner, EStructuralFeature feature)
	{
		super(parent, style);		
		this.enableEditing = enableEditing;		
		this.owner = owner;
		this.feature = feature;
		
		colorFieldEditor = new ColorFieldEditor("Color", label, this);
		colorFieldEditor.setEnabled(this.enableEditing, this);
		colorFieldEditor.getColorSelector().addListener(new IPropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent event) 
			{				
				Color3f color = convert(colorFieldEditor.getColorSelector().getColorValue());
				ApogyCommonTransactionFacade.INSTANCE.basicSet(getOwner(), getFeature(), color);
			}
		});
		
		// Sets initial color
		if(getOwner() != null)
		{
			if(getFeature() != null)
			{
				// Update the color.
				Object value = getOwner().eGet(getFeature(), true);
				if(value instanceof Color3f)
				{
					updateDisplayedColor((Color3f) value);
				}
				else
				{
					updateDisplayedColor(null);
				}
			}
		}
		
		addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if(getOwner() != null)
				{
					getOwner().eAdapters().remove(getAdapter());
				}				
			}
		});
	}
	
	public Color3f getSelectedColor()
	{
		if(colorFieldEditor != null)
		{
			return convert(colorFieldEditor.getColorSelector().getColorValue());
		}
		else
		{
			return null;
		}
	}
	
	public EObject getOwner() 
	{		
		return owner;
	}

	public void setOwner(EObject newOwner) 
	{
		// Un-Register listener from past owner.
		if(this.owner != null)
		{
			this.owner.eAdapters().remove(getAdapter());
		}
		
		this.owner = newOwner;
		
		if(newOwner != null)
		{
			if(getFeature() != null)
			{
				// Update the color.
				Object value = newOwner.eGet(getFeature(), true);
				if(value instanceof Color3f)
				{
					updateDisplayedColor((Color3f) value);
				}
				else
				{
					updateDisplayedColor(null);
				}
			}
			// Register listener to the new owner.
			newOwner.eAdapters().add(getAdapter());
		}
	}

	public EStructuralFeature getFeature() {
		return feature;
	}

	public void setFeature(EStructuralFeature feature) 
	{
		this.feature = feature;
		
		// Update displayed color.
		if(getOwner() != null && feature != null)
		{
			Object value = getOwner().eGet(getFeature(), true);
			if(value instanceof Color3f)
			{
				updateDisplayedColor((Color3f) value);
			}
			else
			{
				updateDisplayedColor(null);
			}
		}
		else
		{
			updateDisplayedColor(null);
		}
	}

	protected void newColorSelected(Color3f color)
	{		
	}
	
	protected Color3f convert(org.eclipse.swt.graphics.RGB rgb)
	{
		Color3f color = new Color3f( ((float)rgb.red / 255.0f), ((float)rgb.green / 255.0f), ((float)rgb.blue / 255.0f));
		return color;
	}
	
	protected org.eclipse.swt.graphics.RGB convert(Color3f color)
	{
		if(color != null)
		{
			int red = Math.round(255.0f * (color.getX()));
			int green = Math.round(255.0f * (color.getY()));
			int blue = Math.round(255.0f * (color.getZ()));
			
			return new RGB(red, green, blue);
		}
		else
		{
			return new RGB(0, 0, 0);
		}
	}
	
	private void updateDisplayedColor(final Color3f newColor)
	{				
		getDisplay().asyncExec(new Runnable() {
			
			@Override
			public void run() 
			{	
				if(colorFieldEditor != null)
				{
					colorFieldEditor.getColorSelector().setColorValue(convert(newColor));
				}
			}
		});	
		
		newColorSelected(newColor);
	}
	
	private Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getFeature() == getFeature())
					{
						if(msg.getNewValue() instanceof Color3f)
						{
							Color3f color = (Color3f) msg.getNewValue();
							updateDisplayedColor(color);
						}
						else
						{
							updateDisplayedColor(null);
						}
					}
				}
			};
		}
		
		return adapter;
	}
}

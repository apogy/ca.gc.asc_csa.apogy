package ca.gc.asc_csa.apogy.common.ui.dialogs;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import ca.gc.asc_csa.apogy.common.ui.composites.BundleResourcesSelectionComposite;

/**
 * Dialog used to browse resources in installed plug-ins.
 * @author pallard
 *
 */
public class PluginResourcesURLDialog extends MessageDialog
{
	protected String[] fileExtensions;
	
	protected String selectedPluginSymbolicName;
	protected String selectedPath;

	protected BundleResourcesSelectionComposite bundleResourcesSelectionComposite;
	
	/**
	 * @param parentShell The parent shell.
	 * @param fileExtensions An array containing the file extensions to use as filters. For example new String[]{"*.txt","*.gif"}
	 */
	public PluginResourcesURLDialog(Shell parentShell, String[] fileExtensions)
	{
		super(null, "Please select the ressource.", null,"Please select the ressource.", MessageDialog.NONE, new String[] { "Select", "Cancel" }, 1);
		this.fileExtensions = fileExtensions;
	}
	
	/**
	 * Gets the file extension filters.
	 * @return The array containing the file extensions to use as filters. For example new String[]{"*.txt","*.gif"}
	 */
	public String[] getFileExtensions() {
		return fileExtensions;
	}

	/**
	 * Set the file extension filters.
	 * @param fileExtensions An array containing the file extensions to use as filters. For example new String[]{"*.txt","*.gif"}
	 */
	public void setFileExtensions(String[] fileExtensions) 
	{
		this.fileExtensions = fileExtensions;
		
		if(bundleResourcesSelectionComposite != null)
		{
			bundleResourcesSelectionComposite.setFileExtensions(fileExtensions);
		}
	}
	
	@Override
	protected Control createCustomArea(Composite parent) 
	{
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout());
		
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.minimumHeight = 300;
		gridData.heightHint = 300;
		composite.setLayoutData(gridData);
				
		bundleResourcesSelectionComposite = new BundleResourcesSelectionComposite(composite, SWT.NONE, this.fileExtensions)
		{
			@Override
			protected void newSelection() 
			{
				selectedPluginSymbolicName = bundleResourcesSelectionComposite.getSelectedPluginSymbolicName();
				selectedPath = bundleResourcesSelectionComposite.getSelectedPath();
			}
		};
		bundleResourcesSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		 
		return composite;
	}
	
	public String getSelectedPluginSymbolicName() {
		return selectedPluginSymbolicName;
	}

	public String getSelectedPath() {
		return selectedPath;
	}
}

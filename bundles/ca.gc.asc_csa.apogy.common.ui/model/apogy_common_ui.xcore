// *****************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
//     Pierre Allard - initial API and implementation
//     Regent L'Archeveque
//     Sebastien Gemme
//        
// SPDX-License-Identifier: EPL-1.0
// *****************************************************************************
@GenModel(prefix="ApogyCommonUi",
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Pierre Allard - initial API and implementation
     Regent L'Archeveque 
     Sebastien Gemme
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",	
		  modelName="ApogyCommonUi")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.common.ui/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.common.ui/src-generated")

package ca.gc.asc_csa.apogy.common.ui

import org.eclipse.jface.resource.ImageDescriptor
import org.eclipse.jface.viewers.ISelection
import org.eclipse.jface.viewers.TreeViewer
import org.eclipse.jface.wizard.WizardPage
import org.eclipse.swt.widgets.Display


/* Types definitions. */
type ImageDescriptor wraps ImageDescriptor
type ISelection wraps ISelection
type WizardPage wraps WizardPage
type TreeViewer wraps TreeViewer
type Display wraps Display

/* -------------------------------------------------------------------------
 * 
 * Apogy Workspace Ui Facade.
 * 
 * ------------------------------------------------------------------------- */ 
class ApogyCommonUiFacade{
	/*
	 * Returns the {@link ImageDescriptor} specified by the uri.
	 * @param uri URI that refers to the image.
	 *            e.g. "platform:/plugin/ca.gc.asc_csa.apogy.common/ui/icons/pin.gif"
	 * @return Reference to the {@link ImageDescriptor} or null if not available.
	 */
	op ImageDescriptor getImageDescriptor(String uri)
	
	/*
	 * Adjusts the size according to the ratio specified and the location of the wizard
	 * at the center of the screen where the cursor currently is.
	 * @param wizardPage the {@link WizardPage} to adjust.
	 * @param ratio the wizard size/screen size wanted. (value from 0 to 1)
	 */
	op void adjustWizardPage(WizardPage wizardPage, Double ratio)
	
	/*
	 * Adds a double click listener that expands/collapses the tree
	 * when an element is double clicked. 
	 * Also adds a listener on the tree that adjusts the columns when an element is expanded. 
	 * @param treeViewer The viewer to add the listeners
	 */
	op void addExpandOnDoubleClick(TreeViewer treeViewer)
}
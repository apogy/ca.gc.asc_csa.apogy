package ca.gc.asc_csa.apogy.core.programs.controllers.ui.parts;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.HashMap;

import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.core.programs.controllers.BindedEDataTypeArgument;
import ca.gc.asc_csa.apogy.core.programs.controllers.OperationCallControllerBinding;
import ca.gc.asc_csa.apogy.core.programs.controllers.ui.ApogyCoreProgramsControllersUIRCPConstants;
import ca.gc.asc_csa.apogy.core.programs.controllers.ui.composite.BindedArgumentComposite;
import ca.gc.asc_csa.apogy.core.programs.controllers.ui.composite.ControllerBindingDetailsComposite;

public class ControllerBindingDetailsPart extends AbstractEObjectSelectionPart {

	Composite parent;

	@Override
	protected void createContentComposite(Composite parent, int style) {
		this.parent = parent;
		new ControllerBindingDetailsComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
	}

	@Override
	protected void setCompositeContents(EObject eObject) {
		/** If the eObject is an OperationCallControllerBinding */
		if (eObject instanceof OperationCallControllerBinding) {
			/** Display a ControllerBindingDetailsComposite */
			if (!(getActualComposite() instanceof ControllerBindingDetailsComposite)) {
				if (getActualComposite() != null) {
					getActualComposite().dispose();
				}
				new ControllerBindingDetailsComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
				parent.layout();
			}
			((ControllerBindingDetailsComposite) getActualComposite())
					.setOperationCallControllerBinding((OperationCallControllerBinding) eObject);
		}
		/** Otherwise If the eObject is a BindedEDataTypeArgument */
		else if (eObject instanceof BindedEDataTypeArgument) {
			/** Display a BindedArgumentComposite */
			if (!(getActualComposite() instanceof BindedArgumentComposite)) {
				if (getActualComposite() != null) {
					getActualComposite().dispose();
				}
				new BindedArgumentComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
				parent.layout();
			}
			((BindedArgumentComposite) getActualComposite())
					.setBindedEDataTypeArgument((BindedEDataTypeArgument) eObject);
		}
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() {
		HashMap<String, ISelectionListener> selectionProvidersIdsToSelectionListeners = new HashMap<String, ISelectionListener>();

		selectionProvidersIdsToSelectionListeners
				.put(ApogyCoreProgramsControllersUIRCPConstants.PART__CONTROLLER_BINDINGS__ID, DEFAULT_LISTENER);

		return selectionProvidersIdsToSelectionListeners;
	}
}
package ca.gc.asc_csa.apogy.core.programs.controllers.ui.composite;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.llarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.wb.swt.SWTResourceManager;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.impl.FeaturePathAdapterImpl;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.OperationCallControllerBinding;

public class ControllerBindingDetailsComposite extends ScrolledComposite {

	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());

	private Composite composite;
	private Section sectionTrigger;
	private TriggerComposite triggerComposite;
	private Label label;
	private Label startedLabel;

	private DataBindingContext m_bindingContext;

	private OperationCallControllerBinding operationCallControllerBinding;
	
	private List<FeaturePathAdapter> featurePathAdapters;

	/**
	 * Creates the parentComposite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ControllerBindingDetailsComposite(Composite parent, int style) {
		super(parent, style);

		setExpandHorizontal(true);
		setExpandVertical(true);
		addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if(operationCallControllerBinding != null){
					for(FeaturePathAdapter adapter: getFeatureAdapter()){
						adapter.dispose();
					}
				}
				toolkit.dispose();
				
				if (m_bindingContext != null) m_bindingContext.dispose();
			}
		});

		composite = new Composite(this, SWT.None);
		composite.setLayout(new GridLayout(1, true));
		
		label = new Label(composite, SWT.WRAP);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1));
		label.setFont(SWTResourceManager.getFont("Ubuntu", 17, SWT.NORMAL));
		label.setText("Operation call informations");

		sectionTrigger = toolkit.createSection(composite, Section.EXPANDED | Section.TITLE_BAR);
		sectionTrigger.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		sectionTrigger.setLayout(new FillLayout());
		sectionTrigger.setText("Trigger");
		triggerComposite = new TriggerComposite(sectionTrigger, SWT.None) {
			@Override
			protected void newSelection(ISelection selection) {
				ControllerBindingDetailsComposite.this.newSelection(selection);
			}
		};
		triggerComposite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		triggerComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		sectionTrigger.setClient(triggerComposite);

		setContent(composite);
		setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		m_bindingContext = initDataBindings();
	}

	/**
	 * This method is called when a new selection is made in the
	 * parentComposite.
	 * 
	 * @param selection
	 *            Reference to the selection.
	 */
	protected void newSelection(ISelection selection) {
	}

	public OperationCallControllerBinding getOperationCallControllerBinding() {
		return this.operationCallControllerBinding;
	}

	public void setOperationCallControllerBinding(OperationCallControllerBinding operationCallControllerBinding) {
		if(this.operationCallControllerBinding != null){
			for(FeaturePathAdapter adapter: getFeatureAdapter()){
				adapter.dispose();
			}
		}
		
		this.operationCallControllerBinding = operationCallControllerBinding;

		triggerComposite.setOperationCallControllerBinding(this.operationCallControllerBinding);
		label.setText(ApogyCoreInvocatorFacade.INSTANCE.getOperationCallString(this.operationCallControllerBinding));
		composite.layout();
		
		setEnabled(!this.operationCallControllerBinding.isStarted());
		if(this.operationCallControllerBinding != null){
			for(FeaturePathAdapter adapter: getFeatureAdapter()){
				adapter.init(this.operationCallControllerBinding);
			}
		}
	}

	protected DataBindingContext initDataBindings() {
		m_bindingContext = new DataBindingContext();

		return m_bindingContext;
	}
	
	private List<FeaturePathAdapter> getFeatureAdapter(){
		if (this.featurePathAdapters == null) {
			this.featurePathAdapters = new ArrayList<>();
			featurePathAdapters.add(new FeaturePathAdapterImpl() {

				@Override
				public void notifyChanged(Notification msg) {
					label.setText(
							ApogyCoreInvocatorFacade.INSTANCE.getOperationCallString(operationCallControllerBinding));
					composite.layout();
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> featurePath = new ArrayList<EStructuralFeature>();
					featurePath.add(ApogyCoreInvocatorPackage.Literals.VARIABLE_FEATURE_REFERENCE__VARIABLE);
					featurePath.add(ApogyCommonEMFPackage.Literals.NAMED__NAME);
					return featurePath;
				}
			});

			featurePathAdapters.add(new FeaturePathAdapterImpl() {

				@Override
				public void notifyChanged(Notification msg) {
					enableUI(!msg.getNewBooleanValue());
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> featurePath = new ArrayList<EStructuralFeature>();
					featurePath.add(ApogyCommonEMFPackage.Literals.STARTABLE__STARTED);
					return featurePath;
				}
			});
		}
		return this.featurePathAdapters;
	}
	
	/**
	 * Enables and disables the UI. This is used to be sure that the details
	 * don't change while executing.
	 */
	private void enableUI(boolean value){
		if(value){
			if(startedLabel != null && !startedLabel.isDisposed()){
				startedLabel.dispose();
			}	
		}else{
			if(startedLabel == null || startedLabel.isDisposed()){
				startedLabel = new Label(composite, SWT.WRAP);
				startedLabel.setText("Controller active, deactivate to edit");
				startedLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
				startedLabel.moveAbove(sectionTrigger);
			}			
		}
		
		if(triggerComposite.isEnabled() != value){
			triggerComposite.setEnabled(value);
		}
		
		composite.layout();
	}
}
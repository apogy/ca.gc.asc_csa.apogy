package ca.gc.asc_csa.apogy.core.programs.controllers.ui.parts;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersFacade;
import ca.gc.asc_csa.apogy.core.programs.controllers.ui.composite.ControllerConfigsComposite;

public class ControllerConfigsPart extends AbstractSessionBasedPart {

	@Override
	protected void createContentComposite(Composite parent, int style) {
		new ControllerConfigsComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL) {
			@Override
			protected void newSelection(ISelection selection) {
				selectionService.setSelection(((ControllerConfigsComposite)getActualComposite()).getSelectedControllerConfiguration());
			}
		};
	}

	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) {
		if(ApogyCoreInvocatorFacade.INSTANCE != null){
			((ControllerConfigsComposite) getActualComposite())
			.setControllersGroup(ApogyCoreProgramsControllersFacade.INSTANCE.getControllersGroup());
		}
	}
}
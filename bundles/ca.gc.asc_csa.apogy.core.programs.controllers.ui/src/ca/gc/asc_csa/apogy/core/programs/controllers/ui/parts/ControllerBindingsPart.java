package ca.gc.asc_csa.apogy.core.programs.controllers.ui.parts;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.HashMap;

import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllersConfiguration;
import ca.gc.asc_csa.apogy.core.programs.controllers.ui.ApogyCoreProgramsControllersUIRCPConstants;
import ca.gc.asc_csa.apogy.core.programs.controllers.ui.composite.ControllerBindingsComposite;

public class ControllerBindingsPart extends AbstractEObjectSelectionPart {
	@Override
	protected void createContentComposite(Composite parent, int style) {
		new ControllerBindingsComposite(parent, SWT.None) {
			@Override
			protected void newSelection(ISelection selection) {
				selectionService
						.setSelection(((ControllerBindingsComposite) getActualComposite()).getSelectedEObject());
			}
		};
	}

	@Override
	protected void setCompositeContents(EObject eObject) {
		((ControllerBindingsComposite) getActualComposite())
				.setControllersConfiguration((ControllersConfiguration) eObject);
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() {
		HashMap<String, ISelectionListener> selectionProvidersIdsToSelectionListeners = new HashMap<String, ISelectionListener>();

		selectionProvidersIdsToSelectionListeners.put(ApogyCoreProgramsControllersUIRCPConstants.PART__CONTROLLER_CONFIGS__ID,
				DEFAULT_LISTENER);

		return selectionProvidersIdsToSelectionListeners;
	}
}
package ca.gc.asc_csa.apogy.core.programs.controllers.ui.composite;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.llarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.wb.swt.SWTResourceManager;

import ca.gc.asc_csa.apogy.common.emf.ui.composites.EOperationsComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.VariableFeatureReferenceComposite;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersFacade;
import ca.gc.asc_csa.apogy.core.programs.controllers.OperationCallControllerBinding;

public class ControllerBindingDetailsEditComposite extends ScrolledComposite {

	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());

	private CreateResultsComposite createResultsComposite;
	private VariableFeatureReferenceComposite variableFeatureReferenceComposite;
	private EOperationsComposite eOperationsComposite;
	private TriggerComposite triggerComposite;

	private DataBindingContext m_bindingContext;

	private OperationCallControllerBinding operationCallControllerBinding;

	/**
	 * Creates the parentComposite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ControllerBindingDetailsEditComposite(Composite parent, int style) {
		super(parent, style);

		setExpandHorizontal(true);
		setExpandVertical(true);
		addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				toolkit.dispose();
			}
		});

		Composite composite = new Composite(this, SWT.None);
		composite.setLayout(new GridLayout(2, true));

		createResultsComposite = new CreateResultsComposite(composite, SWT.None);
		createResultsComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));

		/**
		 * VariableFeatureReference
		 */
		variableFeatureReferenceComposite = new VariableFeatureReferenceComposite(composite, SWT.NONE) {
			@Override
			protected void newSelection(ISelection selection) {
				if (operationCallControllerBinding.getVariable() != null) {
					eOperationsComposite.setEClass(
							ApogyCoreInvocatorFacade.INSTANCE.getInstanceClass(operationCallControllerBinding),
							operationCallControllerBinding.getEOperation());
				}
				ControllerBindingDetailsEditComposite.this.newSelection(selection);
			}
		};
		variableFeatureReferenceComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));

		Section sectionOperaion = toolkit.createSection(composite, Section.EXPANDED | Section.TITLE_BAR);
		sectionOperaion.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		sectionOperaion.setLayout(new FillLayout());
		sectionOperaion.setText("Operation");
		eOperationsComposite = new EOperationsComposite(sectionOperaion, SWT.NONE) {
			@Override
			protected void newSelection(TreeSelection selection) {
				EOperation eOperation = eOperationsComposite.getSelectedEOperation();
				if (ApogyCommonTransactionFacade.INSTANCE
						.getTransactionalEditingDomain(operationCallControllerBinding) != null) {
					ApogyCommonTransactionFacade.INSTANCE.basicSet(operationCallControllerBinding,
							ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__EOPERATION, eOperation);
				} else {
					operationCallControllerBinding.setEOperation(eOperation);
				}
				if (operationCallControllerBinding.getEOperation() != null) {
					if (!operationCallControllerBinding.getEOperation().getEParameters().isEmpty()) {
						ApogyCoreProgramsControllersFacade.INSTANCE
								.initOperationCallControllerBindingArguments(getOperationCallControllerBinding());
					} else {
						if (ApogyCommonTransactionFacade.INSTANCE
								.getTransactionalEditingDomain(operationCallControllerBinding) != null) {
							ApogyCommonTransactionFacade.INSTANCE.basicSet(getOperationCallControllerBinding(),
									ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__ARGUMENTS_LIST, null);
						} else {
							operationCallControllerBinding.setArgumentsList(null);
						}
					}
				}

				ControllerBindingDetailsEditComposite.this.newSelection(selection);
			}
		};
		sectionOperaion.setClient(eOperationsComposite);

		Section sectionTrigger = toolkit.createSection(composite, Section.EXPANDED | Section.TITLE_BAR);
		sectionTrigger.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		sectionTrigger.setLayout(new FillLayout());
		sectionTrigger.setText("Trigger");
		triggerComposite = new TriggerComposite(sectionTrigger, SWT.None) {
			@Override
			protected void newSelection(ISelection selection) {
				ControllerBindingDetailsEditComposite.this.newSelection(selection);
			}
		};
		triggerComposite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		triggerComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		sectionTrigger.setClient(triggerComposite);

		setContent(composite);
		setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		m_bindingContext = initDataBindings();
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	/**
	 * This method is called when a new selection is made in the
	 * parentComposite.
	 * 
	 * @param selection
	 *            Reference to the selection.
	 */
	protected void newSelection(ISelection selection) {
	}

	public OperationCallControllerBinding getOperationCallControllerBinding() {
		return this.operationCallControllerBinding;
	}

	public void setOperationCallControllerBinding(OperationCallControllerBinding operationCallControllerBinding) {
		this.operationCallControllerBinding = operationCallControllerBinding;

		createResultsComposite.setOperationCallControllerBinding(this.operationCallControllerBinding);
		variableFeatureReferenceComposite.set(
				ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment().getVariablesList(),
				this.operationCallControllerBinding);
		if (this.operationCallControllerBinding.getVariable() != null) {
			eOperationsComposite.setEClass(
					ApogyCoreInvocatorFacade.INSTANCE.getInstanceClass(this.operationCallControllerBinding),
					this.operationCallControllerBinding.getEOperation());
		} else {
			eOperationsComposite.setEClass(null);
		}
		triggerComposite.setOperationCallControllerBinding(this.operationCallControllerBinding);
	}

	protected DataBindingContext initDataBindings() {
		m_bindingContext = new DataBindingContext();

		return m_bindingContext;
	}
}
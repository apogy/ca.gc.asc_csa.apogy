package ca.gc.asc_csa.apogy.core.programs.controllers.ui.renderers;

import java.text.DecimalFormat;

import javax.inject.Inject;
import javax.measure.unit.Unit;

import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.Activator;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.core.invocator.ui.renderers.EDataTypeArgumentControlCompositeRenderer;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerValueSource;

public class InputConditioningControlCompositeRenderer extends EDataTypeArgumentControlCompositeRenderer {

	@Inject
	public InputConditioningControlCompositeRenderer(VControl vElement, ViewModelContext viewContext,
			ReportService reportService, EMFFormsDatabinding emfFormsDatabinding,
			EMFFormsLabelProvider emfFormsLabelProvider, VTViewTemplateProvider vtViewTemplateProvider) {
		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);
	}

	@Override
	protected UpdateValueStrategy getUpdateModelValueStrategy() {
		return new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
				.setConverter(new Converter(String.class, Float.class) {
					@Override
					public Object convert(Object fromObject) {
						if (fromObject != null && !"".equals(fromObject)) {
							try {
								Number number = ApogyCommonEMFUIFacade.INSTANCE.convertToNativeUnits(
										Double.parseDouble((String) fromObject), getNativeUnits(), getDisplayUnits(),
										getTypedElement().getEType());

								/** Format to update the text if there is rounding errors */
								Unit<?> displayUnits = getDisplayUnits();
								Unit<?> nativeUnits = getNativeUnits();
								DecimalFormat format = getDecimalFormat();

								if (displayUnits != null && !displayUnits.equals(nativeUnits)) {
									textValue.setText(format.format(nativeUnits.getConverterTo(displayUnits).convert(number.doubleValue())));
								}else{
									textValue.setText(format.format(number.doubleValue()));
								}

								return number.floatValue();
							} catch (Exception e) {
								/** Error message */
								MessageDialog.openError(textValue.getDisplay().getActiveShell(), "Invalid Number",
										"The number entered is invalid. The value will be unset.");
							}
						}
						/** Set to the current value */
						textValue.setText(getFormatedValue());
						return getValue();
					}
				});
	}
	
	@Override
	protected UpdateValueStrategy getUpdateTextValueStrategy() {
		return new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
				.setConverter(new Converter(Float.class, String.class) {

					@Override
					public Object convert(Object fromObject) {
						if (fromObject != null) {
							String value = getFormatedValue();
							return value == null ? "" : value;
						}
						return "";
					}
				});
	}

	@Override
	protected Number getValue() {
		try {
			if (!getModelValue().isDisposed() && getModelValue().getValue() != null) {
				return (Number)getModelValue().getValue();
			} else {
				return 0;
			}
		} catch (DatabindingFailedException e) {
			Logger.INSTANCE.log(Activator.ID, "Error getting the value. ", EventSeverity.ERROR);
		}
		return null;
	}

	@Override
	protected EParameter getEParameter() {
		EObject element = getViewModelContext().getDomainModel().eContainer();

		if (element instanceof ControllerValueSource) {
			return ((ControllerValueSource) element).getBindedEDataTypeArgument().getEParameter();
		}

		return null;
	}
}
package ca.gc.asc_csa.apogy.core.programs.controllers.ui.composite;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.measure.unit.Unit;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.list.DecoratingObservableList;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.wb.swt.SWTResourceManager;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.impl.FeaturePathAdapterImpl;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.preferences.PreferencesConstants;
import ca.gc.asc_csa.apogy.common.io.jinput.ApogyCommonIOJInputPackage;
import ca.gc.asc_csa.apogy.common.io.jinput.EComponentQualifier;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.ArgumentsList;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.BindedEDataTypeArgument;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerValueSource;
import ca.gc.asc_csa.apogy.core.programs.controllers.FixedValueSource;
import ca.gc.asc_csa.apogy.core.programs.controllers.OperationCallControllerBinding;
import ca.gc.asc_csa.apogy.core.programs.controllers.ToggleValueSource;
import ca.gc.asc_csa.apogy.core.programs.controllers.ValueSource;

/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

public class BindedArgumentsListComposite extends ScrolledComposite {

	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());

	private TableViewer tableViewerArguments;
	private Composite composite;

	private Composite compositeValueSource;

	private Section sectionConditioning;
	private Composite compositeConditioning;

	private OperationCallControllerBinding operationCallControllerBinding;

	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
			ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	// private Adapter adapter;
	private List<FeaturePathAdapter> adapters;
	private IPropertyChangeListener propertyChangeListener;

	private DataBindingContext m_bindingContext;

	/**
	 * Create the parentComposite.
	 * 
	 * @param parent
	 *            Reference to the parent parentComposite.
	 * @param style
	 *            Composite style.
	 */
	public BindedArgumentsListComposite(Composite parent, int style) {
		super(parent, style);

		/** Preference listener */
		ca.gc.asc_csa.apogy.common.emf.ui.Activator.getDefault().getPreferenceStore()
				.addPropertyChangeListener(getPropertyChangeListener());

		this.computeSize(1, 1);
		setExpandHorizontal(true);
		setExpandVertical(true);
		addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				for (FeaturePathAdapter adapter : getAdapters()) {
					adapter.dispose();
				}
				toolkit.dispose();

				if (propertyChangeListener != null) {
					ca.gc.asc_csa.apogy.common.emf.ui.Activator.getDefault().getPreferenceStore()
							.removePropertyChangeListener(getPropertyChangeListener());
				}
			}
		});

		composite = new Composite(this, SWT.None);
		composite.setLayout(new GridLayout(2, true));
		/**
		 * Arguments list
		 */
		Section sectionArgumentsList = toolkit.createSection(composite, Section.EXPANDED | Section.TITLE_BAR);
		sectionArgumentsList.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));
		sectionArgumentsList.setLayout(new FillLayout());
		sectionArgumentsList.setText("Arguments");

		Composite compositeArguments = new Composite(sectionArgumentsList, SWT.None);
		GridLayout gridLayout_arguments = new GridLayout(1, true);
		gridLayout_arguments.marginWidth = 0;
		gridLayout_arguments.marginHeight = 0;
		gridLayout_arguments.marginBottom = 5;
		compositeArguments.setLayout(gridLayout_arguments);

		tableViewerArguments = new TableViewer(compositeArguments,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.SINGLE);
		Table tableArguments = tableViewerArguments.getTable();
		tableArguments.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tableArguments.setLinesVisible(true);
		tableArguments.setHeaderVisible(true);
		tableViewerArguments.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				updateDetailsComposites();
				newSelection(event.getSelection());
			}
		});

		TableViewerColumn tableViewerActionColumn = new TableViewerColumn(tableViewerArguments, SWT.NONE);
		TableColumn tableclmnAction = tableViewerActionColumn.getColumn();
		tableclmnAction.setWidth(100);
		tableclmnAction.setText("Parameter");

		TableViewerColumn tableViewerValueSourceColumn = new TableViewerColumn(tableViewerArguments, SWT.NONE);
		TableColumn tableclmnValueSource = tableViewerValueSourceColumn.getColumn();
		tableclmnValueSource.setWidth(100);
		tableclmnValueSource.setText("Value source");

		TableViewerColumn tableViewerValueColumn = new TableViewerColumn(tableViewerArguments, SWT.NONE);
		TableColumn tableclmnValue = tableViewerValueColumn.getColumn();
		tableclmnValue.setWidth(100);
		tableclmnValue.setText("Value");

		TableViewerColumn tableViewerConditioningColumn = new TableViewerColumn(tableViewerArguments, SWT.NONE);
		TableColumn tableclmnConditioning = tableViewerConditioningColumn.getColumn();
		tableclmnConditioning.setWidth(100);
		tableclmnConditioning.setText("Conditioning");
		sectionArgumentsList.setClient(compositeArguments);

		/**
		 * Value source
		 */
		compositeValueSource = new Composite(composite, SWT.None);
		compositeValueSource.setLayout(new GridLayout(1, false));
		compositeValueSource.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		compositeValueSource.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		compositeValueSource.setBackgroundMode(SWT.INHERIT_FORCE);

		/**
		 * Conditioning
		 */
		sectionConditioning = toolkit.createSection(composite, Section.EXPANDED | Section.TITLE_BAR);
		sectionConditioning.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		sectionConditioning.setLayout(new GridLayout(1, false));
		sectionConditioning.setText("Conditioning");
		compositeConditioning = getNoContentComposite(sectionConditioning);

		updateDetailsComposites();

		setContent(composite);
		setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		m_bindingContext = initDataBindingsCustom();

		// Dispose
		addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null)
					m_bindingContext.dispose();

			}
		});
	}

	/**
	 * Updates the value source composite depending on the selected argument
	 */
	private void updateCompositeValueSource() {
		if (compositeValueSource != null) {
			for (Control control : compositeValueSource.getChildren()) {
				control.dispose();
			}
		}

		if (getSelectedArgument() != null) {
			ValueSourceComposite contentComposite = new ValueSourceComposite(compositeValueSource, SWT.None) {
				@Override
				protected void newSelection(ISelection selection) {
					updateCompositeConditioning();
				}
			};
			contentComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			contentComposite.setBindedEDataTypeArgument(getSelectedArgument());
		} else {
			getNoContentComposite(compositeValueSource);
		}
		compositeValueSource.layout();
		this.layout();
	}

	/**
	 * Updates the conditioning composite depending on the selected argument.
	 */
	private void updateCompositeConditioning() {
		if (compositeConditioning != null) {
			compositeConditioning.dispose();
		}

		if (getSelectedArgument() != null && getSelectedArgument().getValueSource() instanceof ControllerValueSource) {
			compositeConditioning = new ConditioningComposite(sectionConditioning, SWT.NO_SCROLL) {
				@Override
				protected void newSelection(ISelection selection) {
					BindedArgumentsListComposite.this.newSelection(selection);
				}
			};
			((ConditioningComposite) compositeConditioning).setAbstractInputConditioning(
					((ControllerValueSource) getSelectedArgument().getValueSource()).getConditioning());

			if (((ControllerValueSource) getSelectedArgument().getValueSource()).getEComponentQualifier() != null) {
				((ConditioningComposite) compositeConditioning).setEComponentQualifier(
						((ControllerValueSource) getSelectedArgument().getValueSource()).getEComponentQualifier());
			}
			compositeConditioning.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			compositeConditioning.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		} else {
			compositeConditioning = getNoContentComposite(sectionConditioning);
		}

		sectionConditioning.setClient(compositeConditioning);
		sectionConditioning.layout();
		layout();
	}

	/**
	 * Updates the details composites depending on the selected argument.
	 */
	private void updateDetailsComposites() {
		updateCompositeValueSource();
		updateCompositeConditioning();
	}

	/**
	 * Returns a {@link NoContentComposite} if a detail section is not
	 * applicable.
	 * 
	 * @param section
	 *            The parent {@link Section}.
	 * @return Reference to the {@link NoContentComposite}.
	 */
	private Composite getNoContentComposite(Composite parent) {
		NoContentComposite composite = new NoContentComposite(parent, SWT.None) {
			@Override
			protected String getMessage() {
				return "No compatible selection";
			}
		};
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		composite.setBackgroundMode(SWT.INHERIT_FORCE);
		return composite;
	}

	/** Preference listener */
	private IPropertyChangeListener getPropertyChangeListener() {
		if (propertyChangeListener == null) {
			propertyChangeListener = new IPropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent event) {
					/**
					 * Unit of format preference event, update the value text
					 */
					if (event.getProperty().equals(PreferencesConstants.TYPED_ELEMENTS_UNITS_ID)
							|| PreferencesConstants.isFormatPreference(event.getProperty())) {
						refreshViewer();
					}
				}
			};
		}

		return propertyChangeListener;
	}

	/**
	 * Refreshed the viewer and adjusts the columns.
	 */
	protected void refreshViewer() {
		if (!tableViewerArguments.getTable().isDisposed() && !tableViewerArguments.isBusy()) {
			tableViewerArguments.refresh();
			for (TableColumn column : tableViewerArguments.getTable().getColumns()) {
				column.pack();
			}
		}
	}

	/**
	 * Gets the {@link BindedEDataTypeArgument} selected in the viewer
	 * 
	 * @return
	 */
	private BindedEDataTypeArgument getSelectedArgument() {
		if (!tableViewerArguments.getStructuredSelection().isEmpty()) {
			return (BindedEDataTypeArgument) tableViewerArguments.getStructuredSelection().getFirstElement();
		}
		return null;
	}

	/**
	 * This method is called when a new selection is made .
	 * 
	 * @param selection
	 *            Reference to the selection.
	 */
	protected void newSelection(ISelection selection) {
	}

	/**
	 * Binds the {@link OperationCallControllerBinding} with the UI components.
	 * 
	 * @param operationCall
	 *            Reference to the {@link OperationCallControllerBinding}.
	 */
	@SuppressWarnings("unchecked")
	public void setOperationCallControllerBinding(OperationCallControllerBinding operationCallControllerBinding) {
		if (this.operationCallControllerBinding != null) {
			for (FeaturePathAdapter adapter : getAdapters()) {
				adapter.dispose();
			}
		}
		this.operationCallControllerBinding = operationCallControllerBinding;

		TransactionalEditingDomain domain = (TransactionalEditingDomain) AdapterFactoryEditingDomain
				.getEditingDomainFor(operationCallControllerBinding);

		if (operationCallControllerBinding != null && operationCallControllerBinding.getArgumentsList() != null) {
			tableViewerArguments.setInput(
					EMFEditProperties.list(domain, ApogyCoreInvocatorPackage.Literals.ARGUMENTS_LIST__ARGUMENTS)
							.observe(operationCallControllerBinding.getArgumentsList()));
		} else {
			tableViewerArguments.setInput(null);
		}

		refreshViewer();

		if (this.operationCallControllerBinding != null) {
			for (FeaturePathAdapter adapter : getAdapters()) {
				adapter.init(this.operationCallControllerBinding);
			}
		}
	}

	protected DataBindingContext initDataBindingsCustom() {
		m_bindingContext = new DataBindingContext();

		tableViewerArguments.setContentProvider(new ArgumentsContentProvider(adapterFactory));
		tableViewerArguments.setLabelProvider(new ArgumentsLabelProvider(adapterFactory));

		return m_bindingContext;
	}

	/**
	 * Content provider for the arguments.
	 */
	private class ArgumentsContentProvider extends AdapterFactoryContentProvider {

		public ArgumentsContentProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		@Override
		public Object[] getElements(Object object) {
			List<Object> objects = new ArrayList<>();
			for (Object object1 : ((DecoratingObservableList<?>) object)) {
				if (object1 instanceof BindedEDataTypeArgument) {
					objects.add(object1);
				}
			}
			return objects.toArray();
		}

		@Override
		public boolean hasChildren(Object object) {
			return false;
		}

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			refreshViewer();
		}

	}

	/**
	 * Label provider for the arguments.
	 */
	private class ArgumentsLabelProvider extends AdapterFactoryLabelProvider {

		private static final int PARAMETER_COLUMN_ID = 0;
		private static final int VALUE_SOURCE_ID = 1;
		private static final int VALUE_ID = 2;
		private static final int CONDITIONING_ID = 3;

		public ArgumentsLabelProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		@Override
		public String getColumnText(Object object, int columnIndex) {
			String str = "<undefined>";

			// This is used if refresh is called on an Argument that is not in
			// the ArgumentsList anymore.
			if (operationCallControllerBinding.getArgumentsList() == null
					|| !operationCallControllerBinding.getArgumentsList().getArguments().contains(object)) {
				return str;
			}

			ValueSource valueSource = ((BindedEDataTypeArgument) object).getValueSource();
			switch (columnIndex) {
			case PARAMETER_COLUMN_ID:
				str = super.getColumnText(object, 0);
				break;
			case VALUE_SOURCE_ID:
				if (valueSource != null) {
					str = valueSource.eClass().getName();
				}
				break;
			case VALUE_ID:
				/** Fixed value source */
				if (valueSource instanceof FixedValueSource) {
					/** Get the value */
					Double valueDouble = 0.0;
					str = ((FixedValueSource) valueSource).getValue();
					try {
						valueDouble = Double.parseDouble(str);
					} catch (Exception e) {
					}
					EParameter param = ((FixedValueSource) valueSource).getEParameter();

					Unit<?> unit = ApogyCommonEMFFacade.INSTANCE.getEngineeringUnits(param);
					if (unit != null) {
						/** Initialize if needed */
						if (((FixedValueSource) valueSource).getValue() == null) {
							ApogyCommonTransactionFacade.INSTANCE.basicSet(valueSource,
									ApogyCoreInvocatorPackage.Literals.EDATA_TYPE_ARGUMENT__VALUE, "0.0");
						}
						/** Get the display format */
						EOperationEParametersFormatProviderParameters formatParams = ApogyCommonEMFUIFactory.eINSTANCE
								.createEOperationEParametersFormatProviderParameters();
						formatParams.setParam(param);
						/** Get the display units */
						EOperationEParametersUnitsProviderParameters unitsParams = ApogyCommonEMFUIFactory.eINSTANCE
								.createEOperationEParametersUnitsProviderParameters();
						unitsParams.setParam(param);
						DecimalFormat format = ApogyCommonEMFUIFacade.INSTANCE.getDisplayFormat(param.getEOperation(),
								formatParams);
						Unit<?> displayUnits = ApogyCommonEMFUIFacade.INSTANCE.getDisplayUnits(param.getEOperation(),
								unitsParams);

						/** Convert to display units */
						if (displayUnits != null && !displayUnits.equals(unit)) {
							valueDouble = unit.getConverterTo(displayUnits).convert(valueDouble);
						}

						/** Format */
						str = format.format(valueDouble) + " " + displayUnits.toString();
					}
				}
				/**
				 * Toggle valueSource, display the initial and current values
				 */
				else if (valueSource instanceof ToggleValueSource) {
					str = "Initial : " + String.valueOf(((ToggleValueSource) valueSource).isInitialValue())
							+ ", Current : " + String.valueOf(((ToggleValueSource) valueSource).isCurrentValue());
				}
				/**
				 * Controller valueSource, display the controller and component
				 */
				else if (valueSource instanceof ControllerValueSource) {
					EComponentQualifier qualifier = ((ControllerValueSource) valueSource).getEComponentQualifier();
					str = qualifier.getEControllerName() + "." + qualifier.getEComponentName();
				}
				break;
			case CONDITIONING_ID:
				if (valueSource instanceof ControllerValueSource) {
					str = super.getColumnText(((ControllerValueSource) valueSource).getConditioning(), columnIndex);
				} else {
					str = "N/A";
				}
				break;
			default:
				break;
			}
			return str;
		}

		@Override
		public Image getColumnImage(Object object, int columnIndex) {
			ValueSource valueSource = ((BindedEDataTypeArgument) object).getValueSource();
			switch (columnIndex) {
			case CONDITIONING_ID:
				if (valueSource instanceof ControllerValueSource) {
					return super.getColumnImage(((ControllerValueSource) valueSource).getConditioning(), columnIndex);
				}
			default:
				return null;
			}
		}

	}

	private List<FeaturePathAdapter> getAdapters() {
		if (adapters == null) {
			adapters = new ArrayList<>();

			adapters.add(new FeaturePathAdapterImpl() {

				@Override
				public void notifyChanged(Notification msg) {
					featurePathAdapterNotify(msg);
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> features = new ArrayList<>();

					features.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__ARGUMENTS_LIST);
					features.add(ApogyCoreInvocatorPackage.Literals.ARGUMENTS_LIST__ARGUMENTS);
					features.add(ApogyCoreProgramsControllersPackage.Literals.BINDED_EDATA_TYPE_ARGUMENT__VALUE_SOURCE);
					features.add(ApogyCoreInvocatorPackage.Literals.EDATA_TYPE_ARGUMENT__VALUE);

					return features;
				}
			});

			adapters.add(new FeaturePathAdapterImpl() {

				@Override
				public void notifyChanged(Notification msg) {
					featurePathAdapterNotify(msg);
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> features = new ArrayList<>();

					features.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__ARGUMENTS_LIST);
					features.add(ApogyCoreInvocatorPackage.Literals.ARGUMENTS_LIST__ARGUMENTS);
					features.add(ApogyCoreProgramsControllersPackage.Literals.BINDED_EDATA_TYPE_ARGUMENT__VALUE_SOURCE);
					features.add(ApogyCoreProgramsControllersPackage.Literals.CONTROLLER_VALUE_SOURCE__COMPONENT);

					return features;
				}
			});

			adapters.add(new FeaturePathAdapterImpl() {

				@Override
				public void notifyChanged(Notification msg) {
					featurePathAdapterNotify(msg);
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> features = new ArrayList<>();

					features.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__ARGUMENTS_LIST);
					features.add(ApogyCoreInvocatorPackage.Literals.ARGUMENTS_LIST__ARGUMENTS);
					features.add(ApogyCoreProgramsControllersPackage.Literals.BINDED_EDATA_TYPE_ARGUMENT__VALUE_SOURCE);
					features.add(ApogyCoreProgramsControllersPackage.Literals.CONTROLLER_VALUE_SOURCE__CONDITIONING);

					return features;
				}
			});
			adapters.add(new FeaturePathAdapterImpl() {

				@Override
				public void notifyChanged(Notification msg) {
					featurePathAdapterNotify(msg);
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> features = new ArrayList<>();

					features.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__ARGUMENTS_LIST);
					features.add(ApogyCoreInvocatorPackage.Literals.ARGUMENTS_LIST__ARGUMENTS);
					features.add(ApogyCoreProgramsControllersPackage.Literals.BINDED_EDATA_TYPE_ARGUMENT__VALUE_SOURCE);
					features.add(
							ApogyCoreProgramsControllersPackage.Literals.CONTROLLER_VALUE_SOURCE__ECOMPONENT_QUALIFIER);
					features.add(ApogyCommonIOJInputPackage.Literals.ECOMPONENT_QUALIFIER__ECOMPONENT_NAME);

					return features;
				}
			});

			adapters.add(new FeaturePathAdapterImpl() {

				@Override
				public void notifyChanged(Notification msg) {
					featurePathAdapterNotify(msg);
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> features = new ArrayList<>();

					features.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__ARGUMENTS_LIST);
					features.add(ApogyCoreInvocatorPackage.Literals.ARGUMENTS_LIST__ARGUMENTS);
					features.add(ApogyCoreProgramsControllersPackage.Literals.BINDED_EDATA_TYPE_ARGUMENT__VALUE_SOURCE);
					features.add(
							ApogyCoreProgramsControllersPackage.Literals.CONTROLLER_VALUE_SOURCE__ECOMPONENT_QUALIFIER);
					features.add(ApogyCommonIOJInputPackage.Literals.ECOMPONENT_QUALIFIER__ECONTROLLER_NAME);

					return features;
				}
			});
		}
		return adapters;
	}

	private void featurePathAdapterNotify(Notification msg) {
		if (msg.getFeature() == ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__ARGUMENTS_LIST
				&& msg.getNewValue() != null) {
			setOperationCallControllerBinding(
					(OperationCallControllerBinding) ((ArgumentsList) msg.getNewValue()).getOperationCall());
			composite.layout();
			compositeValueSource.layout();
			sectionConditioning.layout();
		}

		refreshViewer();
		newSelection(null);
	}
}
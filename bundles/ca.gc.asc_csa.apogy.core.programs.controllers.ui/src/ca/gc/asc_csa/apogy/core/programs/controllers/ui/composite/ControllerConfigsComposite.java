package ca.gc.asc_csa.apogy.core.programs.controllers.ui.composite;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.llarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.list.DecoratingObservableList;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.impl.FeaturePathAdapterImpl;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersFacade;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllersConfiguration;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllersGroup;

public class ControllerConfigsComposite extends ScrolledComposite {

	private DataBindingContext dataBindingContext;

	private TableViewer tableViewer;

	private ControllersGroup controllersGroup;
	private List<FeaturePathAdapter> adapters;

	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
			ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	private Button btnDelete;
	private Button btnActivate;
	private Button btnDeactivate;

	/**
	 * Creates the parentComposite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ControllerConfigsComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, true));
		setExpandHorizontal(true);
		setExpandVertical(true);
		addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (controllersGroup != null) {
					for (FeaturePathAdapter adapter : getAdapters()) {
						adapter.dispose();
					}
				}
			}
		});

		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));

		tableViewer = new TableViewer(composite, SWT.BORDER | SWT.FULL_SELECTION);
		tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				ControllerConfigsComposite.this.newSelection(null);
			}
		});
		Table table = tableViewer.getTable();
		table.setLinesVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 5));
		TableColumn tblclmnName = new TableColumn(table, SWT.NONE);
		tblclmnName.setWidth(100);

		Button btnNew = new Button(composite, SWT.NONE);
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnNew.setText("New");
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				NamedSetting setting = ApogyCommonEMFUIFactory.eINSTANCE.createNamedSetting();
				setting.setParent(controllersGroup);
				setting.setContainingFeature(ApogyCoreInvocatorPackage.Literals.PROGRAMS_GROUP__PROGRAMS);
				ApogyEObjectWizard wizard = new ApogyEObjectWizard(
						ApogyCoreInvocatorPackage.Literals.PROGRAMS_GROUP__PROGRAMS, controllersGroup, setting,
						ApogyCoreProgramsControllersPackage.Literals.CONTROLLERS_CONFIGURATION);
				WizardDialog dialog = new WizardDialog(getShell(), wizard);
				dialog.open();
				tableViewer.setSelection(new StructuredSelection(wizard.getCreatedEObject()));
			}
		});

		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnDelete.setText("Delete");
		btnDelete.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (getSelectedControllerConfiguration() != null) {
					if (getSelectedControllerConfiguration().isStarted()) {
						ApogyCoreProgramsControllersFacade.INSTANCE
								.setActiveControllersConfiguration(getSelectedControllerConfiguration(), false);
					}
					ApogyCommonTransactionFacade.INSTANCE.basicRemove(controllersGroup,
							ApogyCoreInvocatorPackage.Literals.PROGRAMS_GROUP__PROGRAMS,
							getSelectedControllerConfiguration());
				}
			}
		});

		Label label = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		btnActivate = new Button(composite, SWT.NONE);
		btnActivate.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnActivate.setText("Activate");
		btnActivate.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {

				ControllersConfiguration config = (ControllersConfiguration) tableViewer.getStructuredSelection()
						.getFirstElement();

				ApogyCoreProgramsControllersFacade.INSTANCE.setActiveControllersConfiguration(config, true);
			}
		});

		btnDeactivate = new Button(composite, SWT.NONE);
		btnDeactivate.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		btnDeactivate.setText("Deactivate");
		btnDeactivate.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				ControllersConfiguration config = (ControllersConfiguration) tableViewer.getStructuredSelection()
						.getFirstElement();

				ApogyCoreProgramsControllersFacade.INSTANCE.setActiveControllersConfiguration(config, false);
			}
		});

		setContent(composite);
		setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		dataBindingContext = initDataBindingsCustom();
	}

	protected void newSelection(ISelection selection) {
	}

	public ControllersConfiguration getSelectedControllerConfiguration() {
		return (ControllersConfiguration) tableViewer.getStructuredSelection().getFirstElement();
	}

	/**
	 * Sets the root object for the parentComposite
	 * 
	 * @param eObject
	 *            The root eObject
	 */
	@SuppressWarnings("unchecked")
	public void setControllersGroup(ControllersGroup controllersGroup) {
		if (this.controllersGroup != null) {
			for (FeaturePathAdapter adapter : getAdapters()) {
				adapter.dispose();
			}
		}

		this.controllersGroup = controllersGroup;

		IObservableList<?> programGroupProgramsObserveList = EMFProperties
				.list(ApogyCoreInvocatorPackage.Literals.PROGRAMS_GROUP__PROGRAMS).observe(controllersGroup);

		tableViewer.setInput(programGroupProgramsObserveList);

		for (FeaturePathAdapter adapter : getAdapters()) {
			adapter.init(this.controllersGroup);
		}
	}

	@SuppressWarnings("unchecked")
	protected DataBindingContext initDataBindingsCustom() {

		dataBindingContext = new DataBindingContext();

		tableViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory) {
			@Override
			public Object[] getElements(Object object) {
				List<Object> objects = new ArrayList<>();
				for (Object object1 : ((DecoratingObservableList<?>) object)) {
					if (object1 instanceof ControllersConfiguration) {
						objects.add(object1);
					}
				}
				return objects.toArray();
			}

			@Override
			public boolean hasChildren(Object object) {
				return false;
			}
		});
		tableViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory) {
			@Override
			public Image getColumnImage(Object object, int columnIndex) {
				return null;
			}

			@Override
			public String getText(Object object) {
				return ((ControllersConfiguration) object).getName();
			}
		});

		/**
		 * Buttons bindings
		 */
		IObservableValue<?> tableViewerSingleSelectionObserveValue = ViewerProperties.singleSelection()
				.observe(tableViewer);
		IObservableValue<?> variablesInstantiated = EMFProperties
				.value(FeaturePath.fromList(ApogyCoreInvocatorPackage.Literals.INVOCATOR_SESSION__ENVIRONMENT,
						ApogyCoreInvocatorPackage.Literals.ENVIRONMENT__ACTIVE_CONTEXT,
						ApogyCoreInvocatorPackage.Literals.CONTEXT__VARIABLES_INSTANTIATED))
				.observe(ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession());

		IObservableValue<?> enableDeleteButtonObserveValue = WidgetProperties.enabled().observe(btnDelete);
		dataBindingContext.bindValue(enableDeleteButtonObserveValue, tableViewerSingleSelectionObserveValue,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(ControllersConfiguration.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return updateDelete();
							}
						}));
		dataBindingContext.bindValue(enableDeleteButtonObserveValue, variablesInstantiated,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Boolean.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return updateDelete();
							}
						}));

		IObservableValue<?> enableActivateButtonObserveValue = WidgetProperties.enabled().observe(btnActivate);
		dataBindingContext.bindValue(enableActivateButtonObserveValue, tableViewerSingleSelectionObserveValue,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(ControllersConfiguration.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return updateActivate();
							}
						}));
		dataBindingContext.bindValue(enableActivateButtonObserveValue, variablesInstantiated,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Boolean.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return updateActivate();
							}
						}));

		IObservableValue<?> enableDeactivateButtonObserveValue = WidgetProperties.enabled().observe(btnDeactivate);
		dataBindingContext.bindValue(enableDeactivateButtonObserveValue, tableViewerSingleSelectionObserveValue,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(ControllersConfiguration.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return updateDeactivate();
							}
						}));
		dataBindingContext.bindValue(enableDeactivateButtonObserveValue, variablesInstantiated,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Boolean.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return updateDeactivate();
							}
						}));

		return dataBindingContext;
	}

	/**
	 * This method is used to update the delete button. This is needed because
	 * more than one binding will update this value.
	 */
	private boolean updateDelete() {
		if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() != null
				&& ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment() != null
				&& ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment()
						.getActiveContext() != null) {
			if (tableViewer.getStructuredSelection().getFirstElement() != null) {
				return !((ControllersConfiguration) tableViewer.getStructuredSelection().getFirstElement()).isStarted();
			}
		}
		return false;
	}

	/**
	 * This method is used to update the activate button. This is needed because
	 * more than one binding will update this value.
	 */
	private boolean updateActivate() {
		if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() != null
				&& ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment() != null
				&& ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment()
						.getActiveContext() != null) {
			if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment().getActiveContext()
					.isVariablesInstantiated()) {
				if (tableViewer.getStructuredSelection().getFirstElement() != null) {
					return !((ControllersConfiguration) tableViewer.getStructuredSelection().getFirstElement())
							.isStarted();
				}
			}
		}
		return false;
	}

	/**
	 * This method is used to update the deactivate button. This is needed
	 * because more than one binding will update this value.
	 */
	private boolean updateDeactivate() {
		if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() != null
				&& ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment() != null
				&& ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment()
						.getActiveContext() != null) {
			if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment().getActiveContext()
					.isVariablesInstantiated()) {
				if (tableViewer.getStructuredSelection().getFirstElement() != null) {
					return ((ControllersConfiguration) tableViewer.getStructuredSelection().getFirstElement())
							.isStarted();
				}
			}
		}
		return false;
	}

	private List<FeaturePathAdapter> getAdapters() {
		if (this.adapters == null) {
			adapters = new ArrayList<FeaturePathAdapter>();

			adapters.add(new FeaturePathAdapterImpl() {

				@Override
				public void notifyChanged(Notification msg) {
					if (!tableViewer.isBusy() && !ControllerConfigsComposite.this.isDisposed()) 
					{										
						System.out.println("Allo : ");
						
						Display.getDefault().asyncExec(new Runnable() 
						{							
							@Override
							public void run() 
							{
								if(!tableViewer.isBusy())
								{
									tableViewer.refresh();
								}
							}
						});						
					}

				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> features = new ArrayList<>();

					features.add(ApogyCoreInvocatorPackage.Literals.PROGRAMS_GROUP__PROGRAMS);
					features.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL_CONTAINER__OPERATION_CALLS);
					features.add(ApogyCommonEMFPackage.Literals.NAMED__NAME);

					return features;
				}
			});
			adapters.add(new FeaturePathAdapterImpl() {

				@Override
				public void notifyChanged(Notification msg) {
					getDisplay().asyncExec(new Runnable() {
						@Override
						public void run() {
							if (msg.getNotifier() == tableViewer.getStructuredSelection().getFirstElement()) {
								btnActivate.setEnabled(updateActivate());
								btnDeactivate.setEnabled(updateDeactivate());
							}
						}
					});
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> features = new ArrayList<>();

					features.add(ApogyCoreInvocatorPackage.Literals.PROGRAMS_GROUP__PROGRAMS);
					features.add(ApogyCommonEMFPackage.Literals.STARTABLE__STARTED);

					return features;
				}
			});

		}
		return this.adapters;
	}
}
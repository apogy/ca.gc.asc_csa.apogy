package ca.gc.asc_csa.apogy.core.programs.controllers.ui.composite;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.EObjectListComposite;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.io.jinput.ui.composites.AutomaticControllerSelectionComposite;
import ca.gc.asc_csa.apogy.common.io.jinput.ui.composites.ManualControllerSelectionComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.OperationCall;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerEdgeTrigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerStateTrigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.EdgeType;
import ca.gc.asc_csa.apogy.core.programs.controllers.OperationCallControllerBinding;
import ca.gc.asc_csa.apogy.core.programs.controllers.TimeTrigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.Trigger;

public class TriggerComposite extends ScrolledComposite {

	private Composite composite;
	Composite controllerSelectionComposite;
	private EObjectListComposite triggersListComposite;
	private Composite eComponentComposite;

	private OperationCallControllerBinding operationCallControllerBinding;

	private Adapter adapter;

	/**
	 * Create the parentComposite.
	 * 
	 * @param parent
	 *            Reference to the parent parentComposite.
	 * @param style
	 *            Composite style.
	 */
	public TriggerComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout());
		setExpandHorizontal(true);
		setExpandVertical(true);
		addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (operationCallControllerBinding != null) {
					operationCallControllerBinding.eAdapters().remove(getAdapter());
				}
			}
		});

		composite = new Composite(this, SWT.None);
		GridLayout gridLayout = new GridLayout(1, true);
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		composite.setLayout(gridLayout);

		triggersListComposite = new EObjectListComposite(composite, SWT.None) 
		{
			@Override
			protected void newSelection(TreeSelection selection) 
			{
				Trigger currentTrigger = operationCallControllerBinding.getTrigger();
				if (currentTrigger == null || getSelectedEObject() != operationCallControllerBinding.getTrigger().eClass()) {

					// Dispose the componentComposite.
					for (Control control : eComponentComposite.getChildren()) {
						control.dispose();
					}

					// Create the new trigger.
					Trigger trigger = (Trigger) EcoreUtil.create((EClass) getSelectedEObject());

					// Stop the current trigger if needed.
					if (currentTrigger != null && currentTrigger.isStarted()) {
						ApogyCommonTransactionFacade.INSTANCE.basicSet(currentTrigger,
								ApogyCommonEMFPackage.Literals.STARTABLE__STARTED, false);
					}

					// Set the new trigger.
					ApogyCommonTransactionFacade.INSTANCE.basicSet(operationCallControllerBinding,
							ApogyCoreProgramsControllersPackage.Literals.OPERATION_CALL_CONTROLLER_BINDING__TRIGGER,
							trigger);

					// Start the new trigger if needed.
					if (operationCallControllerBinding.isStarted()) {
						ApogyCommonTransactionFacade.INSTANCE.basicSet(trigger,
								ApogyCommonEMFPackage.Literals.STARTABLE__STARTED,
								operationCallControllerBinding.isStarted());
					}
					TriggerComposite.this.newSelection(selection);
				}
			}

			@Override
			protected StyledCellLabelProvider getLabelProvider() {
				return new StyledCellLabelProvider() {
					@Override
					public void update(ViewerCell cell) {
						if (cell.getElement() instanceof EClass) {
							cell.setText(((EClass) cell.getElement()).getName());
						}
					}
				};
			}
		};
		triggersListComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		eComponentComposite = new Composite(composite, SWT.NONE);
		eComponentComposite.setLayout(new GridLayout(1, false));
		eComponentComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		setContent(composite);
		setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	public Trigger getTrigger() {
		return this.operationCallControllerBinding == null ? null : this.operationCallControllerBinding.getTrigger();
	}

	private void setEComponentComposite() {

		if (!eComponentComposite.isDisposed()) 
		{
			Trigger trigger = this.operationCallControllerBinding.getTrigger();

			for (Control control : eComponentComposite.getChildren()) {
				control.dispose();
			}						

			/** Time trigger */
			if (trigger instanceof TimeTrigger) 
			{
				Composite contentComposite = new Composite(eComponentComposite, SWT.None);
				contentComposite.setLayoutData(new GridData(SWT.FILL, SWT.UP, true, false, 1, 1));
				contentComposite.setLayout(new GridLayout(1, false));
				ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(contentComposite, trigger);
			}
			/** Controller edge trigger */
			else if (trigger instanceof ControllerEdgeTrigger) 
			{			
				Composite contentComposite = new Composite(eComponentComposite, SWT.None);
				contentComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
				GridLayout gridLayout = new GridLayout(1, true);
				gridLayout.marginWidth = 0;
				gridLayout.marginHeight = 0;
				contentComposite.setLayout(gridLayout);
																				
				/** List of edge types */
				EObjectListComposite edgeTypesComposite = new EObjectListComposite(contentComposite, SWT.None) {
					@Override
					protected void newSelection(TreeSelection selection) {
						TriggerComposite.this.newSelection(selection);
						if (selection.getFirstElement() != null) {
							EdgeType edgeType = EdgeType.get(((EEnumLiteral) selection.getFirstElement()).getLiteral());
							if (edgeType != ((ControllerEdgeTrigger) trigger).getEdgeType()) {
								ApogyCommonTransactionFacade.INSTANCE.basicSet(trigger,
										ApogyCoreProgramsControllersPackage.Literals.CONTROLLER_EDGE_TRIGGER__EDGE_TYPE,
										edgeType);
							}
						}
					}

					@Override
					protected StyledCellLabelProvider getLabelProvider() {
						return new StyledCellLabelProvider() {
							@Override
							public void update(ViewerCell cell) {
								if (cell.getElement() instanceof EEnumLiteral) {
									cell.setText(((EEnumLiteral) cell.getElement()).getLiteral());
								}
							}
						};
					}
				};
				edgeTypesComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
				if (((ControllerEdgeTrigger) trigger).getEdgeType() != null) {
					edgeTypesComposite.setEObjectsList(
							ApogyCoreProgramsControllersPackage.Literals.EDGE_TYPE.getELiterals(),
							((ControllerEdgeTrigger) trigger).getEdgeType());
				} else {
					edgeTypesComposite
							.setEObjectsList(ApogyCoreProgramsControllersPackage.Literals.EDGE_TYPE.getELiterals());
				}

				/** Controller selection */
				Button autoDetectEnabled = new Button(contentComposite, SWT.CHECK);
				autoDetectEnabled.setText("Auto Detect");
				autoDetectEnabled.setToolTipText("Enables auto detection of the selected controller component.");
				autoDetectEnabled.addSelectionListener(new SelectionListener() {
					
					@Override
					public void widgetSelected(SelectionEvent arg0) 
					{
						if(controllerSelectionComposite != null && !controllerSelectionComposite.isDisposed())
						{
							// Clear previous controls
							for(Control control : controllerSelectionComposite.getChildren())
							{
								control.dispose();
							}
							
							if(autoDetectEnabled.getSelection())
							{
								AutomaticControllerSelectionComposite automaticControllerSelectionComposite = new AutomaticControllerSelectionComposite(controllerSelectionComposite, SWT.NONE) 
								{
									@Override
									protected void newSelection(ISelection selection) 
									{
										TriggerComposite.this.newSelection(selection);
									}
								};
								automaticControllerSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
								automaticControllerSelectionComposite.setEComponentQualifier(((ControllerEdgeTrigger) trigger).getComponentQualifier());
							}
							else
							{
								ManualControllerSelectionComposite manualControllerSelectionComposite = new ManualControllerSelectionComposite(controllerSelectionComposite, SWT.NONE)
								{
									@Override
									protected void newSelection(ISelection selection) 
									{
										TriggerComposite.this.newSelection(selection);
									}
								};
								
								manualControllerSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
								manualControllerSelectionComposite.setEComponentQualifier(((ControllerEdgeTrigger) trigger).getComponentQualifier());
							}
							
							controllerSelectionComposite.layout();
							controllerSelectionComposite.redraw();
						}
					}
					
					@Override
					public void widgetDefaultSelected(SelectionEvent arg0) 
					{				
					}
				});
				
				controllerSelectionComposite = new Composite(contentComposite, SWT.BORDER);				
				controllerSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
				GridLayout gd_controllerSelectionComposite = new GridLayout(1, true);
				gd_controllerSelectionComposite.marginWidth = 0;
				gd_controllerSelectionComposite.marginHeight = 0;
				controllerSelectionComposite.setLayout(gd_controllerSelectionComposite);	
				
				ManualControllerSelectionComposite manualControllerSelectionComposite = new ManualControllerSelectionComposite(controllerSelectionComposite, SWT.NONE)
				{
					@Override
					protected void newSelection(ISelection selection) 
					{
						TriggerComposite.this.newSelection(selection);
					}
				};
				manualControllerSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
				manualControllerSelectionComposite.setEComponentQualifier(((ControllerEdgeTrigger) trigger).getComponentQualifier());
				controllerSelectionComposite.layout();
				controllerSelectionComposite.redraw();
				
				autoDetectEnabled.setSelection(false);
	
			} 
			else if (trigger instanceof ControllerStateTrigger) 
			{
				
				Composite contentComposite = new Composite(eComponentComposite, SWT.None);
				contentComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
				GridLayout gridLayout = new GridLayout(1, true);
				gridLayout.marginWidth = 0;
				gridLayout.marginHeight = 0;
				contentComposite.setLayout(gridLayout);

				/** Controller selection */
				Button autoDetectEnabled = new Button(contentComposite, SWT.CHECK);
				autoDetectEnabled.setText("Auto Detect");
				autoDetectEnabled.setToolTipText("Enables auto detection of the selected controller component.");
				autoDetectEnabled.addSelectionListener(new SelectionListener() {
					
					@Override
					public void widgetSelected(SelectionEvent arg0) 
					{
						if(controllerSelectionComposite != null && !controllerSelectionComposite.isDisposed())
						{
							// Clear previous controls
							for(Control control : controllerSelectionComposite.getChildren())
							{
								control.dispose();
							}
							
							if(autoDetectEnabled.getSelection())
							{
								AutomaticControllerSelectionComposite automaticControllerSelectionComposite = new AutomaticControllerSelectionComposite(controllerSelectionComposite, SWT.NONE) 
								{
									@Override
									protected void newSelection(ISelection selection) 
									{
										TriggerComposite.this.newSelection(selection);
									}
								};
								automaticControllerSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
								automaticControllerSelectionComposite.setEComponentQualifier(((ControllerStateTrigger) trigger).getComponentQualifier());
							}
							else
							{
								ManualControllerSelectionComposite manualControllerSelectionComposite = new ManualControllerSelectionComposite(controllerSelectionComposite, SWT.NONE)
								{
									@Override
									protected void newSelection(ISelection selection) 
									{
										TriggerComposite.this.newSelection(selection);
									}
								};
								manualControllerSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
								manualControllerSelectionComposite.setEComponentQualifier(((ControllerStateTrigger) trigger).getComponentQualifier());
							}
							
							controllerSelectionComposite.layout();
							controllerSelectionComposite.redraw();
						}
					}
					
					@Override
					public void widgetDefaultSelected(SelectionEvent arg0) 
					{				
					}
				});
				
				controllerSelectionComposite = new Composite(contentComposite, SWT.BORDER);				
				controllerSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
				GridLayout gd_controllerSelectionComposite = new GridLayout(1, true);
				gd_controllerSelectionComposite.marginWidth = 0;
				gd_controllerSelectionComposite.marginHeight = 0;
				controllerSelectionComposite.setLayout(gd_controllerSelectionComposite);	
				
				ManualControllerSelectionComposite manualControllerSelectionComposite = new ManualControllerSelectionComposite(controllerSelectionComposite, SWT.NONE)
				{
					@Override
					protected void newSelection(ISelection selection) 
					{
						TriggerComposite.this.newSelection(selection);
					}
				};
				manualControllerSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
				manualControllerSelectionComposite.setEComponentQualifier(((ControllerStateTrigger) trigger).getComponentQualifier());
				controllerSelectionComposite.layout();
				controllerSelectionComposite.redraw();								
								
				Composite controllerStateTriggerContentComposite = new Composite(eComponentComposite, SWT.None);
				controllerStateTriggerContentComposite.setLayoutData(new GridData(SWT.FILL, SWT.UP, true, false, 1, 1));
				controllerStateTriggerContentComposite.setLayout(new GridLayout(1, false));
				ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(controllerStateTriggerContentComposite, trigger);
				
				autoDetectEnabled.setSelection(false);
				
				controllerSelectionComposite.layout();
				controllerSelectionComposite.redraw();
			}

			eComponentComposite.layout();
			eComponentComposite.getParent().layout();
		}
	}

	/**
	 * Binds the {@link OperationCall} with the UI components.
	 * 
	 * @param operationCall
	 *            Reference to the {@link OperationCall}.
	 */
	public void setOperationCallControllerBinding(OperationCallControllerBinding operationCallControllerBinding) {
		if (this.operationCallControllerBinding != null) {
			this.operationCallControllerBinding.eAdapters().remove(getAdapter());
		}
		this.operationCallControllerBinding = operationCallControllerBinding;

		/**
		 * Set the triggers to select
		 */
		EList<EObject> eObjectsEClassList = new BasicEList<EObject>();
		eObjectsEClassList.addAll(ApogyCommonEMFFacade.INSTANCE.getAllSubEClasses(
				ApogyCoreProgramsControllersPackage.Literals.OPERATION_CALL_CONTROLLER_BINDING__TRIGGER
						.getEReferenceType()));
		if (operationCallControllerBinding.getTrigger() != null) {
			triggersListComposite.setEObjectsList(eObjectsEClassList,
					operationCallControllerBinding.getTrigger().eClass());
		} else {
			triggersListComposite.setEObjectsList(eObjectsEClassList);
		}
		setEComponentComposite();

		this.operationCallControllerBinding.eAdapters().add(getAdapter());
	}

	protected void newSelection(ISelection selection) {

	}

	public EObject getSelectedTrigger() {
		return triggersListComposite.getSelectedEObject();
	}

	/**
	 * @return
	 */
	public Adapter getAdapter() {
		if (adapter == null) {
			adapter = new AdapterImpl() {
				@Override
				public void notifyChanged(Notification msg) {
					if (msg.getFeature() != null) {
						if (msg.getFeature() != ApogyCoreProgramsControllersPackage.Literals.OPERATION_CALL_CONTROLLER_BINDING__TRIGGER) {
							triggersListComposite.refreshTreeViewer();
						} else {
							setEComponentComposite();
						}

					}
				}
			};
		}
		return adapter;
	}
}
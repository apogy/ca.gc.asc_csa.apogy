package ca.gc.asc_csa.apogy.core.programs.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ca.gc.asc_csa.apogy.core.programs.ApogyCoreProgramsFacade;
import ca.gc.asc_csa.apogy.core.programs.ApogyCoreProgramsFactory;
import ca.gc.asc_csa.apogy.core.programs.ApogyCoreProgramsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc --> * @generated
 */
public class ApogyCoreProgramsFactoryImpl extends EFactoryImpl implements ApogyCoreProgramsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public static ApogyCoreProgramsFactory init() {
		try {
			ApogyCoreProgramsFactory theApogyCoreProgramsFactory = (ApogyCoreProgramsFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyCoreProgramsPackage.eNS_URI);
			if (theApogyCoreProgramsFactory != null) {
				return theApogyCoreProgramsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyCoreProgramsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCoreProgramsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyCoreProgramsPackage.APOGY_CORE_PROGRAMS_FACADE: return createApogyCoreProgramsFacade();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCoreProgramsFacade createApogyCoreProgramsFacade() {
		ApogyCoreProgramsFacadeImpl apogyCoreProgramsFacade = new ApogyCoreProgramsFacadeImpl();
		return apogyCoreProgramsFacade;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCoreProgramsPackage getApogyCoreProgramsPackage() {
		return (ApogyCoreProgramsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyCoreProgramsPackage getPackage() {
		return ApogyCoreProgramsPackage.eINSTANCE;
	}

} //ApogyCoreProgramsFactoryImpl

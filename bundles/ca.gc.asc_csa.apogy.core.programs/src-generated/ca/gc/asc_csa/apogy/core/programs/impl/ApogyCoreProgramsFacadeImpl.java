package ca.gc.asc_csa.apogy.core.programs.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.core.programs.ApogyCoreProgramsFacade;
import ca.gc.asc_csa.apogy.core.programs.ApogyCoreProgramsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Program Facade</b></em>'.
 * <!-- end-user-doc --> *
 * @generated
 */
public class ApogyCoreProgramsFacadeImpl extends MinimalEObjectImpl.Container implements ApogyCoreProgramsFacade {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected ApogyCoreProgramsFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreProgramsPackage.Literals.APOGY_CORE_PROGRAMS_FACADE;
	}

} //ApogyCoreProgramsFacadeImpl

package ca.gc.asc_csa.apogy.addons.ui.jme3.scene_objects;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.Trajectory3DTool;
import ca.gc.asc_csa.apogy.addons.Trajectory3DToolNode;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects.DefaultJME3SceneObject;

public class Trajectory3DToolNodeJME3Object extends DefaultJME3SceneObject<Trajectory3DToolNode>
{				
	private Adapter adapter;	

	public Trajectory3DToolNodeJME3Object(Trajectory3DToolNode node, JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{
		super(node, jme3RenderEngineDelegate);		
			
		// Sets initial visibility
		setVisible(getTopologyNode().getTrajectory3DTool().isVisible());
		
		// Register an adapter to the Trajectory3DTool.
		getTopologyNode().getTrajectory3DTool().eAdapters().add(getAdapter());						
	}

	@Override
	public void updateGeometry(float tpf) 
	{
		// Nothing to do.
	}
	
//	@Override
//	public void setVisible(boolean visible) 
//	{
//		System.out.println("======================> " + visible);
//		
//		try
//		{
//			String x = null;
//			x.length();
//		}
//		catch(Throwable t)
//		{
//			t.printStackTrace();
//		}
//		
//		super.setVisible(visible);
//	}
	
	@Override
	public void dispose()
	{		
		if(getTopologyNode() != null && getTopologyNode().getTrajectory3DTool() != null)
		{
			getTopologyNode().getTrajectory3DTool().eAdapters().remove(getAdapter());
		}
		super.dispose();					
	}
	

	private Adapter getAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{					
					if(msg.getNotifier() instanceof Trajectory3DTool)
					{												
						int featureId = msg.getFeatureID(Trajectory3DTool.class);
						
						switch(featureId)
						{
							case ApogyAddonsPackage.SIMPLE3_DTOOL__VISIBLE:								
								setVisible(msg.getNewBooleanValue());
							break;
						}						
					}				
				}
			};
		}
		return adapter;
	}
}

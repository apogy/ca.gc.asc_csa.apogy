/**
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.SpotLightBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.impl.AbstractTopologyBindingImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Spot Light Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.SpotLightBindingImpl#getSpotLight <em>Spot Light</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SpotLightBindingImpl extends AbstractTopologyBindingImpl implements SpotLightBinding {
	/**
	 * The cached value of the '{@link #getSpotLight() <em>Spot Light</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpotLight()
	 * @generated
	 * @ordered
	 */
	protected SpotLight spotLight;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpotLightBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyAddonsPrimitivesBindingsPackage.Literals.SPOT_LIGHT_BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpotLight getSpotLight() {
		if (spotLight != null && spotLight.eIsProxy()) {
			InternalEObject oldSpotLight = (InternalEObject)spotLight;
			spotLight = (SpotLight)eResolveProxy(oldSpotLight);
			if (spotLight != oldSpotLight) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCommonTopologyAddonsPrimitivesBindingsPackage.SPOT_LIGHT_BINDING__SPOT_LIGHT, oldSpotLight, spotLight));
			}
		}
		return spotLight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpotLight basicGetSpotLight() {
		return spotLight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpotLight(SpotLight newSpotLight) {
		SpotLight oldSpotLight = spotLight;
		spotLight = newSpotLight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyAddonsPrimitivesBindingsPackage.SPOT_LIGHT_BINDING__SPOT_LIGHT, oldSpotLight, spotLight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.SPOT_LIGHT_BINDING__SPOT_LIGHT:
				if (resolve) return getSpotLight();
				return basicGetSpotLight();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.SPOT_LIGHT_BINDING__SPOT_LIGHT:
				setSpotLight((SpotLight)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.SPOT_LIGHT_BINDING__SPOT_LIGHT:
				setSpotLight((SpotLight)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.SPOT_LIGHT_BINDING__SPOT_LIGHT:
				return spotLight != null;
		}
		return super.eIsSet(featureID);
	}

	@Override
	public Class<?> getSupportedFeatureType() 
	{
		return SpotLight.class;
	}
	
	@Override
	protected void valueChanged(Object newValue) {
		// TODO Auto-generated method stub
		
	}

} //SpotLightBindingImpl

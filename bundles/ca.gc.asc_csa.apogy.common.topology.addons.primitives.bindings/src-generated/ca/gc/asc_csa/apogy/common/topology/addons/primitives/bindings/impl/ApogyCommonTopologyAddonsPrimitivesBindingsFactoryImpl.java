/**
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsFactory;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.LightEnablementBinding;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.PointLightBinding;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.SpotLightBinding;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCommonTopologyAddonsPrimitivesBindingsFactoryImpl extends EFactoryImpl implements ApogyCommonTopologyAddonsPrimitivesBindingsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyCommonTopologyAddonsPrimitivesBindingsFactory init() {
		try {
			ApogyCommonTopologyAddonsPrimitivesBindingsFactory theApogyCommonTopologyAddonsPrimitivesBindingsFactory = (ApogyCommonTopologyAddonsPrimitivesBindingsFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyCommonTopologyAddonsPrimitivesBindingsPackage.eNS_URI);
			if (theApogyCommonTopologyAddonsPrimitivesBindingsFactory != null) {
				return theApogyCommonTopologyAddonsPrimitivesBindingsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyCommonTopologyAddonsPrimitivesBindingsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyAddonsPrimitivesBindingsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING: return createLightEnablementBinding();
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.POINT_LIGHT_BINDING: return createPointLightBinding();
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.SPOT_LIGHT_BINDING: return createSpotLightBinding();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LightEnablementBinding createLightEnablementBinding() {
		LightEnablementBindingImpl lightEnablementBinding = new LightEnablementBindingImpl();
		return lightEnablementBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PointLightBinding createPointLightBinding() {
		PointLightBindingImpl pointLightBinding = new PointLightBindingImpl();
		return pointLightBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpotLightBinding createSpotLightBinding() {
		SpotLightBindingImpl spotLightBinding = new SpotLightBindingImpl();
		return spotLightBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyAddonsPrimitivesBindingsPackage getApogyCommonTopologyAddonsPrimitivesBindingsPackage() {
		return (ApogyCommonTopologyAddonsPrimitivesBindingsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyCommonTopologyAddonsPrimitivesBindingsPackage getPackage() {
		return ApogyCommonTopologyAddonsPrimitivesBindingsPackage.eINSTANCE;
	}

} //ApogyCommonTopologyAddonsPrimitivesBindingsFactoryImpl

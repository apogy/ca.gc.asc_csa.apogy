/**
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.Light;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.LightEnablementBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.impl.AbstractTopologyBindingImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Light Enablement Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.LightEnablementBindingImpl#isCurrentValue <em>Current Value</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.LightEnablementBindingImpl#getLight <em>Light</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LightEnablementBindingImpl extends AbstractTopologyBindingImpl implements LightEnablementBinding {
	/**
	 * The default value of the '{@link #isCurrentValue() <em>Current Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCurrentValue()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CURRENT_VALUE_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isCurrentValue() <em>Current Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCurrentValue()
	 * @generated
	 * @ordered
	 */
	protected boolean currentValue = CURRENT_VALUE_EDEFAULT;
	/**
	 * The cached value of the '{@link #getLight() <em>Light</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLight()
	 * @generated
	 * @ordered
	 */
	protected Light light;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LightEnablementBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyAddonsPrimitivesBindingsPackage.Literals.LIGHT_ENABLEMENT_BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCurrentValue() {
		return currentValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentValue(boolean newCurrentValue) {
		boolean oldCurrentValue = currentValue;
		currentValue = newCurrentValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING__CURRENT_VALUE, oldCurrentValue, currentValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Light getLight() {
		if (light != null && light.eIsProxy()) {
			InternalEObject oldLight = (InternalEObject)light;
			light = (Light)eResolveProxy(oldLight);
			if (light != oldLight) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING__LIGHT, oldLight, light));
			}
		}
		return light;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Light basicGetLight() {
		return light;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLight(Light newLight) {
		Light oldLight = light;
		light = newLight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING__LIGHT, oldLight, light));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING__CURRENT_VALUE:
				return isCurrentValue();
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING__LIGHT:
				if (resolve) return getLight();
				return basicGetLight();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING__CURRENT_VALUE:
				setCurrentValue((Boolean)newValue);
				return;
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING__LIGHT:
				setLight((Light)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING__CURRENT_VALUE:
				setCurrentValue(CURRENT_VALUE_EDEFAULT);
				return;
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING__LIGHT:
				setLight((Light)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING__CURRENT_VALUE:
				return currentValue != CURRENT_VALUE_EDEFAULT;
			case ApogyCommonTopologyAddonsPrimitivesBindingsPackage.LIGHT_ENABLEMENT_BINDING__LIGHT:
				return light != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (currentValue: ");
		result.append(currentValue);
		result.append(')');
		return result.toString();
	}

	@Override
	public AbstractTopologyBinding clone(Map<Node, Node> originalToCopyNodeMap) 
	{
		LightEnablementBinding lightEnablementBindingCopy = EcoreUtil.copy(this);
		lightEnablementBindingCopy.setLight((Light) originalToCopyNodeMap.get(this.getLight()));				
		return lightEnablementBindingCopy;
	}	
		
	@Override
	public Class<?> getSupportedFeatureType() 
	{
		return Light.class;
	}
	
	@Override
	protected void valueChanged(Object newValue) 
	{		
		boolean value = false;		
		if(newValue instanceof Boolean)
		{
			value = (Boolean) newValue;
			setCurrentValue(value);
			
			if(getLight() != null) getLight().setEnabled(value);
		}			
	}

} //LightEnablementBindingImpl

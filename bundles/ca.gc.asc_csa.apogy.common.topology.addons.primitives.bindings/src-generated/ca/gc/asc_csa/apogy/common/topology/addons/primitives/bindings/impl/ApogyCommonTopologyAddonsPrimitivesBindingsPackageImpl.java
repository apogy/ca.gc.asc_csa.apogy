/**
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsFactory;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.LightEnablementBinding;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.PointLightBinding;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.SpotLightBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl extends EPackageImpl implements ApogyCommonTopologyAddonsPrimitivesBindingsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lightEnablementBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pointLightBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass spotLightBindingEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl() {
		super(eNS_URI, ApogyCommonTopologyAddonsPrimitivesBindingsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCommonTopologyAddonsPrimitivesBindingsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyCommonTopologyAddonsPrimitivesBindingsPackage init() {
		if (isInited) return (ApogyCommonTopologyAddonsPrimitivesBindingsPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyAddonsPrimitivesBindingsPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl theApogyCommonTopologyAddonsPrimitivesBindingsPackage = (ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCommonTopologyBindingsPackage.eINSTANCE.eClass();
		ApogyCommonTopologyAddonsPrimitivesPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCommonTopologyAddonsPrimitivesBindingsPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCommonTopologyAddonsPrimitivesBindingsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCommonTopologyAddonsPrimitivesBindingsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCommonTopologyAddonsPrimitivesBindingsPackage.eNS_URI, theApogyCommonTopologyAddonsPrimitivesBindingsPackage);
		return theApogyCommonTopologyAddonsPrimitivesBindingsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLightEnablementBinding() {
		return lightEnablementBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLightEnablementBinding_CurrentValue() {
		return (EAttribute)lightEnablementBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLightEnablementBinding_Light() {
		return (EReference)lightEnablementBindingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPointLightBinding() {
		return pointLightBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPointLightBinding_PointLight() {
		return (EReference)pointLightBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSpotLightBinding() {
		return spotLightBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSpotLightBinding_SpotLight() {
		return (EReference)spotLightBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyAddonsPrimitivesBindingsFactory getApogyCommonTopologyAddonsPrimitivesBindingsFactory() {
		return (ApogyCommonTopologyAddonsPrimitivesBindingsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		lightEnablementBindingEClass = createEClass(LIGHT_ENABLEMENT_BINDING);
		createEAttribute(lightEnablementBindingEClass, LIGHT_ENABLEMENT_BINDING__CURRENT_VALUE);
		createEReference(lightEnablementBindingEClass, LIGHT_ENABLEMENT_BINDING__LIGHT);

		pointLightBindingEClass = createEClass(POINT_LIGHT_BINDING);
		createEReference(pointLightBindingEClass, POINT_LIGHT_BINDING__POINT_LIGHT);

		spotLightBindingEClass = createEClass(SPOT_LIGHT_BINDING);
		createEReference(spotLightBindingEClass, SPOT_LIGHT_BINDING__SPOT_LIGHT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCommonTopologyBindingsPackage theApogyCommonTopologyBindingsPackage = (ApogyCommonTopologyBindingsPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyBindingsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogyCommonTopologyAddonsPrimitivesPackage theApogyCommonTopologyAddonsPrimitivesPackage = (ApogyCommonTopologyAddonsPrimitivesPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyAddonsPrimitivesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		lightEnablementBindingEClass.getESuperTypes().add(theApogyCommonTopologyBindingsPackage.getAbstractTopologyBinding());
		pointLightBindingEClass.getESuperTypes().add(theApogyCommonTopologyBindingsPackage.getAbstractTopologyBinding());
		spotLightBindingEClass.getESuperTypes().add(theApogyCommonTopologyBindingsPackage.getAbstractTopologyBinding());

		// Initialize classes, features, and operations; add parameters
		initEClass(lightEnablementBindingEClass, LightEnablementBinding.class, "LightEnablementBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLightEnablementBinding_CurrentValue(), theEcorePackage.getEBoolean(), "currentValue", null, 0, 1, LightEnablementBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLightEnablementBinding_Light(), theApogyCommonTopologyAddonsPrimitivesPackage.getLight(), null, "light", null, 0, 1, LightEnablementBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pointLightBindingEClass, PointLightBinding.class, "PointLightBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPointLightBinding_PointLight(), theApogyCommonTopologyAddonsPrimitivesPackage.getPointLight(), null, "pointLight", null, 0, 1, PointLightBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(spotLightBindingEClass, SpotLightBinding.class, "SpotLightBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSpotLightBinding_SpotLight(), theApogyCommonTopologyAddonsPrimitivesPackage.getSpotLight(), null, "spotLight", null, 0, 1, SpotLightBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "prefix", "ApogyCommonTopologyAddonsPrimitivesBindings",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque \n     Sebastien Gemme\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "modelName", "ApogyCommonTopologyAddonsPrimitivesBindings",
			 "complianceLevel", "6.0",
			 "dynamicTemplates", "true",
			 "suppressGenModelAnnotations", "false",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.common.topology.addons.primitives"
		   });	
		addAnnotation
		  (lightEnablementBindingEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBinding that binds a Light enablement to a boolean value in a model."
		   });	
		addAnnotation
		  (getLightEnablementBinding_CurrentValue(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe current value of the light enablement."
		   });	
		addAnnotation
		  (getLightEnablementBinding_Light(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe Light being controlled by the binding."
		   });	
		addAnnotation
		  (pointLightBindingEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBinding that binds all parameters of a PointLight to another PointLight."
		   });	
		addAnnotation
		  (getPointLightBinding_PointLight(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe Point Light being controlled by the binding."
		   });	
		addAnnotation
		  (spotLightBindingEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBinding that binds all parameters of a SpotLight to another SpotLight."
		   });	
		addAnnotation
		  (getSpotLightBinding_SpotLight(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe Spot Light being controlled by the binding."
		   });
	}

} //ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl

/**
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyCommonTopologyAddonsPrimitivesBindings' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque \n     Sebastien Gemme\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' modelName='ApogyCommonTopologyAddonsPrimitivesBindings' complianceLevel='6.0' dynamicTemplates='true' suppressGenModelAnnotations='false' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' modelDirectory='/ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings/src-generated' editDirectory='/ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.common.topology.addons.primitives'"
 * @generated
 */
public interface ApogyCommonTopologyAddonsPrimitivesBindingsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "bindings";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "bindings";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCommonTopologyAddonsPrimitivesBindingsPackage eINSTANCE = ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.LightEnablementBindingImpl <em>Light Enablement Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.LightEnablementBindingImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl#getLightEnablementBinding()
	 * @generated
	 */
	int LIGHT_ENABLEMENT_BINDING = 0;

	/**
	 * The feature id for the '<em><b>Binded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING__BINDED = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__BINDED;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING__DESCRIPTION = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING__NAME = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__NAME;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING__SOURCE = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__SOURCE;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING__FEATURE_NODE = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Supported Feature Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING__SUPPORTED_FEATURE_TYPE = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__SUPPORTED_FEATURE_TYPE;

	/**
	 * The feature id for the '<em><b>Feature Node Adapter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING__FEATURE_NODE_ADAPTER = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE_ADAPTER;

	/**
	 * The feature id for the '<em><b>Current Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING__CURRENT_VALUE = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Light</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING__LIGHT = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Light Enablement Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING_FEATURE_COUNT = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Bind</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING___BIND = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING___BIND;

	/**
	 * The operation id for the '<em>Unbind</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING___UNBIND = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING___UNBIND;

	/**
	 * The operation id for the '<em>Clone</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING___CLONE__MAP = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING___CLONE__MAP;

	/**
	 * The number of operations of the '<em>Light Enablement Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING_OPERATION_COUNT = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.PointLightBindingImpl <em>Point Light Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.PointLightBindingImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl#getPointLightBinding()
	 * @generated
	 */
	int POINT_LIGHT_BINDING = 1;

	/**
	 * The feature id for the '<em><b>Binded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING__BINDED = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__BINDED;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING__DESCRIPTION = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING__NAME = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__NAME;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING__SOURCE = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__SOURCE;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING__FEATURE_NODE = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Supported Feature Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING__SUPPORTED_FEATURE_TYPE = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__SUPPORTED_FEATURE_TYPE;

	/**
	 * The feature id for the '<em><b>Feature Node Adapter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING__FEATURE_NODE_ADAPTER = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE_ADAPTER;

	/**
	 * The feature id for the '<em><b>Point Light</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING__POINT_LIGHT = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Point Light Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING_FEATURE_COUNT = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Bind</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING___BIND = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING___BIND;

	/**
	 * The operation id for the '<em>Unbind</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING___UNBIND = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING___UNBIND;

	/**
	 * The operation id for the '<em>Clone</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING___CLONE__MAP = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING___CLONE__MAP;

	/**
	 * The number of operations of the '<em>Point Light Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING_OPERATION_COUNT = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.SpotLightBindingImpl <em>Spot Light Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.SpotLightBindingImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl#getSpotLightBinding()
	 * @generated
	 */
	int SPOT_LIGHT_BINDING = 2;

	/**
	 * The feature id for the '<em><b>Binded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING__BINDED = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__BINDED;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING__DESCRIPTION = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING__NAME = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__NAME;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING__SOURCE = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__SOURCE;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING__FEATURE_NODE = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Supported Feature Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING__SUPPORTED_FEATURE_TYPE = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__SUPPORTED_FEATURE_TYPE;

	/**
	 * The feature id for the '<em><b>Feature Node Adapter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING__FEATURE_NODE_ADAPTER = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE_ADAPTER;

	/**
	 * The feature id for the '<em><b>Spot Light</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING__SPOT_LIGHT = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Spot Light Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING_FEATURE_COUNT = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Bind</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING___BIND = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING___BIND;

	/**
	 * The operation id for the '<em>Unbind</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING___UNBIND = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING___UNBIND;

	/**
	 * The operation id for the '<em>Clone</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING___CLONE__MAP = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING___CLONE__MAP;

	/**
	 * The number of operations of the '<em>Spot Light Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING_OPERATION_COUNT = ApogyCommonTopologyBindingsPackage.ABSTRACT_TOPOLOGY_BINDING_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.LightEnablementBinding <em>Light Enablement Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Light Enablement Binding</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.LightEnablementBinding
	 * @generated
	 */
	EClass getLightEnablementBinding();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.LightEnablementBinding#isCurrentValue <em>Current Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Current Value</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.LightEnablementBinding#isCurrentValue()
	 * @see #getLightEnablementBinding()
	 * @generated
	 */
	EAttribute getLightEnablementBinding_CurrentValue();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.LightEnablementBinding#getLight <em>Light</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Light</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.LightEnablementBinding#getLight()
	 * @see #getLightEnablementBinding()
	 * @generated
	 */
	EReference getLightEnablementBinding_Light();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.PointLightBinding <em>Point Light Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point Light Binding</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.PointLightBinding
	 * @generated
	 */
	EClass getPointLightBinding();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.PointLightBinding#getPointLight <em>Point Light</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Point Light</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.PointLightBinding#getPointLight()
	 * @see #getPointLightBinding()
	 * @generated
	 */
	EReference getPointLightBinding_PointLight();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.SpotLightBinding <em>Spot Light Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Spot Light Binding</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.SpotLightBinding
	 * @generated
	 */
	EClass getSpotLightBinding();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.SpotLightBinding#getSpotLight <em>Spot Light</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Spot Light</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.SpotLightBinding#getSpotLight()
	 * @see #getSpotLightBinding()
	 * @generated
	 */
	EReference getSpotLightBinding_SpotLight();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCommonTopologyAddonsPrimitivesBindingsFactory getApogyCommonTopologyAddonsPrimitivesBindingsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.LightEnablementBindingImpl <em>Light Enablement Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.LightEnablementBindingImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl#getLightEnablementBinding()
		 * @generated
		 */
		EClass LIGHT_ENABLEMENT_BINDING = eINSTANCE.getLightEnablementBinding();

		/**
		 * The meta object literal for the '<em><b>Current Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIGHT_ENABLEMENT_BINDING__CURRENT_VALUE = eINSTANCE.getLightEnablementBinding_CurrentValue();

		/**
		 * The meta object literal for the '<em><b>Light</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIGHT_ENABLEMENT_BINDING__LIGHT = eINSTANCE.getLightEnablementBinding_Light();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.PointLightBindingImpl <em>Point Light Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.PointLightBindingImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl#getPointLightBinding()
		 * @generated
		 */
		EClass POINT_LIGHT_BINDING = eINSTANCE.getPointLightBinding();

		/**
		 * The meta object literal for the '<em><b>Point Light</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POINT_LIGHT_BINDING__POINT_LIGHT = eINSTANCE.getPointLightBinding_PointLight();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.SpotLightBindingImpl <em>Spot Light Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.SpotLightBindingImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.ApogyCommonTopologyAddonsPrimitivesBindingsPackageImpl#getSpotLightBinding()
		 * @generated
		 */
		EClass SPOT_LIGHT_BINDING = eINSTANCE.getSpotLightBinding();

		/**
		 * The meta object literal for the '<em><b>Spot Light</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPOT_LIGHT_BINDING__SPOT_LIGHT = eINSTANCE.getSpotLightBinding_SpotLight();

	}

} //ApogyCommonTopologyAddonsPrimitivesBindingsPackage

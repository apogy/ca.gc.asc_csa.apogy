package ca.gc.asc_csa.apogy.common.math;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;

import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.common.math.impl.ApogyCommonMathFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Facade for Math.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.math.ApogyCommonMathPackage#getApogyCommonMathFacade()
 * @model
 * @generated
 */
public interface ApogyCommonMathFacade extends EObject
{
	public static ApogyCommonMathFacade INSTANCE = ApogyCommonMathFacadeImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Matrix4x4 from a javax.vecmath.Matrix4d.
	 * @param matrix The javax.vecmath.Matrix4d.
	 * @return The Matrix4x4 initialized with the matrix elements.
	 * <!-- end-model-doc -->
	 * @model unique="false" matrixDataType="ca.gc.asc_csa.apogy.common.math.Matrix4d" matrixUnique="false"
	 * @generated
	 */
	Matrix4x4 createMatrix4x4(Matrix4d matrix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Matrix3x3 from a javax.vecmath.Matrix3d.
	 * @param matrix The javax.vecmath.Matrix3d.
	 * @return The Matrix3x3 initialized with the matrix elements.
	 * <!-- end-model-doc -->
	 * @model unique="false" matrixDataType="ca.gc.asc_csa.apogy.common.math.Matrix3d" matrixUnique="false"
	 * @generated
	 */
	Matrix3x3 createMatrix3x3(Matrix3d matrix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Tuple3d from a javax.vecmath.Tuple3d.
	 * @param matrix The javax.vecmath.Tuple3d.
	 * @return The Tuple3d initialized with the tuple coordinates.
	 * <!-- end-model-doc -->
	 * @model unique="false" tupleDataType="ca.gc.asc_csa.apogy.common.math.VecmathTuple3d" tupleUnique="false"
	 * @generated
	 */
	Tuple3d createTuple3d(javax.vecmath.Tuple3d tuple);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Tuple3d using 3 coordinates.
	 * @param x The x coordinate value.
	 * @param y The y coordinate value.
	 * @param z The z coordinate value.
	 * @return The Tuple3d initialized with the specified coordinates.
	 * <!-- end-model-doc -->
	 * @model unique="false" xUnique="false" yUnique="false" zUnique="false"
	 * @generated
	 */
	Tuple3d createTuple3d(double x, double y, double z);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Polynomial from a list of its coefficients.
	 * @param coeffs The coefficients of the polynomial, sorted in increasing order.
	 * @return The polynomial.
	 * <!-- end-model-doc -->
	 * @model unique="false" coeffsDataType="ca.gc.asc_csa.apogy.common.math.EDoubleArray" coeffsUnique="false"
	 * @generated
	 */
	Polynomial createPolynomial(double[] coeffs);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create a Matrix4x4 that is the identity matrix.
	 * @return The Matrix4x4.
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 * @generated
	 */
	Matrix4x4 createIdentityMatrix4x4();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create a Matrix3x3 that is the identity matrix.
	 * @return The Matrix3x3.
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 * @generated
	 */
	Matrix3x3 createIdentityMatrix3x3();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Extract the translation as a Tuple3d from a Matrix4x4 that represents an affine transformation (rotation and translation) in 3D.
	 * @param matrix The affine transformation matrix.
	 * @return The translation as a Tuple3d.
	 * <!-- end-model-doc -->
	 * @model unique="false" matrixUnique="false"
	 * @generated
	 */
	Tuple3d extractPosition(Matrix4x4 matrix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Extract the Euler angles as a Tuple3d from a Matrix4x4 that represents an affine transformation (rotation and translation) in 3D.
	 * @param matrix The affine transformation matrix.
	 * @return The Euler angles (Rx, Ry, Rz) as a Tuple3d. Angles are in radians.
	 * <!-- end-model-doc -->
	 * @model unique="false" matrixUnique="false"
	 * @generated
	 */
	Tuple3d extractOrientation(Matrix4x4 matrix);

} // ApogyCommonMathFacade

package ca.gc.asc_csa.apogy.common.math;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import javax.vecmath.Matrix4d;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Math Input and Output utilities methods.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.math.ApogyCommonMathPackage#getMathIO()
 * @model
 * @generated
 */
public interface MathIO extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Reads a Matrix4x4 from a file.
	 * @param fileName The absolute path to the file.
	 * @return The Matrix4x4 read from the file in Comma Separated Value format (CSV).
	 * @throws An exception in the read fails.
	 * <!-- end-model-doc -->
	 * @model unique="false" exceptions="ca.gc.asc_csa.apogy.common.math.Exception" fileNameUnique="false"
	 * @generated
	 */
	Matrix4x4 readTrMatrix(String fileName) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Writes a Matrix4x4 from a file in Comma Separated Value format (CSV).
	 * @param trMatrix The matrix to write to file.
	 * @param fileName The absolute path to the file.
	 * @throws An exception in the write fails.
	 * <!-- end-model-doc -->
	 * @model exceptions="ca.gc.asc_csa.apogy.common.math.Exception" trMatrixUnique="false" fileNameUnique="false"
	 * @generated
	 */
	void writeTrMatrix(Matrix4x4 trMatrix, String fileName) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Writes a javax.vecmath.Matrix4d from a file in Comma Separated Value format (CSV).
	 * @param trMatrix The matrix to write to file.
	 * @param fileName The absolute path to the file.
	 * @throws An exception in the write fails.
	 * <!-- end-model-doc -->
	 * @model exceptions="ca.gc.asc_csa.apogy.common.math.Exception" trMatrixDataType="ca.gc.asc_csa.apogy.common.math.Matrix4d" trMatrixUnique="false" fileNameUnique="false"
	 * @generated
	 */
	void writeTrMatrix(Matrix4d trMatrix, String fileName) throws Exception;

} // MathIO

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.converters;

import java.util.List;

public interface IFileExporter extends IConverter
{	
	/**
	 * File extension for file storing metadata.
	 */
	public static final String METADATA_FILE_EXTENSION = "metadata";
	
	/**
	 * Converts the input to an object of the output type.
	 * @param input The input object.
	 * @param filePath The path to the destination file or folder.
	 * @param extensions The extensions (file types) to export. 
	 * @exception If the export failed.
	 */
	public void exportToFile(final Object input, String filePath, List<String> extensions) throws Exception;
	
	/**
	 * Return the supported export file extensions.
	 * @return The list of supported file extensions, never null.
	 */
	public List<String> getSupportedFileExtensions();
	
	/**
	 * Return the description of the file content for a specified extension.
	 * @param fileExtension The file extension.
	 * @return THe description, can be null.
	 */
	public String getDescription(String fileExtension);
}

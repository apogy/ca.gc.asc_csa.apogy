/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.converters;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class FileExporterUtilities 
{
	/**
	 * Given a file path, saves a given metadata string to a file. 
	 * @param fullPathString The destination file path.
	 * @param metadata The metadata string.
	 * @throws Exception If the save fails.
	 */
	public static void saveMetaDataToFile(String fullPathString, String metadata) throws Exception 
	{
		FileWriter fileWriter = new FileWriter(fullPathString);							
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		bufferedWriter.write(metadata);
		bufferedWriter.close();
		fileWriter.close();
	}
}

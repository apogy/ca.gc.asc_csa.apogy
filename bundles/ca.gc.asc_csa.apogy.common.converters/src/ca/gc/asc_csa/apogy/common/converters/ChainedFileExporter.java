/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.converters;

import java.util.ArrayList;
import java.util.List;

public class ChainedFileExporter extends ChainedConverter implements IFileExporter {

	public ChainedFileExporter(List<IConverter> converters) 
	{
		super(converters);
		
		if(getIFileExporter() == null)
		{
			throw new RuntimeException("No IFileExporter at the end of the conversion chain !");
		}
	}

	@Override
	public void exportToFile(Object input, String filePath, List<String> extensions) throws Exception 
	{
		if(getIFileExporter() != null)
		{
			Object toExport = null;
			
			// First create a converter chain up to the IFileExporter.
			ChainedConverter upStreamConverter = getUpstreamConverter();
			if(upStreamConverter != null)
			{
				toExport = upStreamConverter.convert(input);
			}
			else
			{
				toExport = input;
			}
			
			// Export the converted input.
			getIFileExporter().exportToFile(toExport, filePath, extensions);
		}
		else 
		{
			throw new RuntimeException("No IFileExporter at the end of the conversion chain !");
		}
	}

	@Override
	public List<String> getSupportedFileExtensions() 
	{
		IFileExporter iFileExporter = getIFileExporter();
		
		if(iFileExporter != null)
		{
			return iFileExporter.getSupportedFileExtensions();
		}
		else
		{
			return new ArrayList<String>();
		}		
	}

	@Override
	public String getDescription(String fileExtension) 
	{
		IFileExporter iFileExporter = getIFileExporter();
		
		if(iFileExporter != null)
		{
			return iFileExporter.getDescription(fileExtension);
		}
		else
		{
			return null;
		}	
	}
	
	private ChainedConverter getUpstreamConverter()
	{
		if(converters.size() > 1)
		{
			List<IConverter> upstreamConverters = new ArrayList<IConverter>();
			for(int i = 0; i < converters.size() - 1; i++)
			{
				upstreamConverters.add(converters.get(i));
			}
			
			return new ChainedConverter(upstreamConverters);
		}
		return null;
	}
	
	private IFileExporter getIFileExporter()
	{
		IFileExporter iFileExporter = null;
		
		if(converters.size() > 0)
		{
			IConverter lastConverter = converters.get(converters.size() - 1);
			if(lastConverter instanceof IFileExporter)
			{
				iFileExporter = (IFileExporter) lastConverter;
			}
		}
		
		return iFileExporter;
	}
}

// *****************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
//     Pierre Allard - initial API and implementation
//     Regent L'Archeveque
//        
// SPDX-License-Identifier: EPL-1.0
// *****************************************************************************

@GenModel(prefix="ApogyAddonsSensorsImagingUI",
	      childCreationExtenders="true",
	      extensibleProviderFactory="true",
	      multipleEditorPages="false",
copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Pierre Allard - initial API and implementation
     Regent L'Archeveque
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",
	      modelName="ApogyAddonsSensorsImagingUI",
		  dynamicTemplates="true", 
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.addons.sensors.imaging.ui/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.addons.sensors.imaging.ui.edit/src-generated")

package ca.gc.asc_csa.apogy.addons.sensors.imaging.ui

import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation
import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode

/**
 * NodePresentation of ImageSnapshot.
 */
class ImageSnapshotPresentation extends NodePresentation
{
	/**
	 * The presentation mode of the geometry representing the FOV.
	 */
	MeshPresentationMode presentationMode
	
	/**
	 * The transparency of the FOV geometry. 
	 */
	float transparency
	
	/**
	 * Whether or not to show the FOV geometry.
	 */
	@GenModel(notify="true", property="Editable")
	boolean fovVisible = "false"	
	
	/**
	 * Whether or not to show the projection of the image in the 3D environment.
	 */
	boolean imageProjectionVisible = "false"
	
	/**
	 * Whether or not to show the projection of the image onto the FOV geometry.
	 */
	boolean imageProjectionOnFOVVisible = "false"
	
	/**
	 * Whether or not to show the the reference frame axis of the FOV.
	 */
	@GenModel(notify="true", property="Editable", propertyCategory="VISUAL_INFORMATION")
	boolean axisVisible = "false"

	/**
	 * The length of the reference frame axis of the FOV, in meters.
	 */
	@GenModel(apogy_units = "m", notify="true", property="Editable", propertyCategory="VISUAL_INFORMATION")
	double axisLength = "1.0"
}


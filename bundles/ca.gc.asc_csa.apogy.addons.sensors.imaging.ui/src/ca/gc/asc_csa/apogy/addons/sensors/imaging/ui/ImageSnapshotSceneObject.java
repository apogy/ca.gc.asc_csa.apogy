package ca.gc.asc_csa.apogy.addons.sensors.imaging.ui;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;
import ca.gc.asc_csa.apogy.common.topology.ui.SceneObject;

public interface ImageSnapshotSceneObject extends SceneObject 
{		
	public MeshPresentationMode getPresentationMode();
	public void setPresentationMode(MeshPresentationMode presentationMode);
		
	public void setFOVVisible(boolean visible);	
	public boolean isFOVVisible();
	
	public void setAxisVisible(boolean visible);	
	public boolean isAxisVisible();
	
	public void setAxisLength(double length);
	public double getAxisLength();
	
	public boolean isImageProjectionEnabled();
	public void setImageProjectionEnabled(boolean enabled);
		
	public boolean isImageProjectionOnFOVEnabled(); 
	public void setImageProjectionOnFOVEnabled(boolean enabled); 
}

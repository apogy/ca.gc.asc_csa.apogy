/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.ui.services;

import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.model.VContainedElement;
import org.eclipse.emf.ecp.view.spi.model.VElement;
import org.eclipse.emfforms.spi.swt.core.AbstractSWTRenderer;
import org.eclipse.emfforms.spi.swt.core.di.EMFFormsDIRendererService;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.ui.elements.ImageSnapshotVElement;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ui.renderers.ImageSnapshotVElementSWTRenderer;

public class ImageSnapshotRendererService implements EMFFormsDIRendererService<VContainedElement> {
	@Override
	public double isApplicable(VElement vElement, ViewModelContext viewModelContext) {
		if (vElement instanceof ImageSnapshotVElement) {
			return 10;
		}
		return NOT_APPLICABLE;
	}

	@Override
	public Class<? extends AbstractSWTRenderer<VContainedElement>> getRendererClass() {
		return ImageSnapshotVElementSWTRenderer.class;
	}
}
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.ui.renderers;

import javax.inject.Inject;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.model.VElement;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.ImageSnapshot;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ui.elements.ImageSnapshotVElement;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.renderers.AbstractCustomElementSWTRenderer;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;

public class ImageSnapshotVElementSWTRenderer extends AbstractCustomElementSWTRenderer 
{
	private ImageData imageData;

	@Inject
	public ImageSnapshotVElementSWTRenderer(VElement element, ViewModelContext viewModelContext,
			ReportService reportService) 
	{
		super((ImageSnapshotVElement) element, viewModelContext, reportService);

		EObject object = viewModelContext.getDomainModel();
		if (object instanceof ImageSnapshot) 
		{
			ImageSnapshot imageSnapshot = (ImageSnapshot) object;
			if(imageSnapshot.getImage() != null)
			{
				imageData = EImagesUtilities.INSTANCE.convertToImageData(imageSnapshot.getImage().asBufferedImage());
			}
		}
	}

	@Override
	protected Control createControl(Composite parent) 
	{
		Label label = new Label(parent, SWT.None);
		label.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TRANSPARENT));
		if (imageData != null) 
		{
			ImageData displayedImageData = (ImageData) imageData.clone();
			Image image = new Image(Display.getCurrent(), displayedImageData);
			label.setImage(image);
		}

		return label;
	}

	@Override
	protected String getLabelText() 
	{
		return "Image";
	}
}

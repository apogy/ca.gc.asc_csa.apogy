package ca.gc.asc_csa.apogy.addons.sensors.imaging.ui.preferences;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.swt.graphics.RGB;
import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;

public class MRTImagingPreferencesConstants 
{
	// Visibility Constants.
	public static final String DEFAULT_IMAGE_SNAPSHOT_VISIBILITY_ID = "DEFAULT_IMAGE_SNAPSHOT_VISIBILITY_ID";	
	public static final Boolean DEFAULT_IMAGE_SNAPSHOT_VISIBILITY = new Boolean(true);
	
	// Image Projection Constants.
	public static final String DEFAULT_IMAGE_SNAPSHOT_SHOW_PROJECTION_ID = "DEFAULT_IMAGE_SNAPSHOT_SHOW_PROJECTION_ID";	
	public static final Boolean DEFAULT_IMAGE_SNAPSHOT_SHOW_PROJECTION = new Boolean(false);

	// FOV Visibility Constants
	public static final String DEFAULT_IMAGE_SNAPSHOT_FOV_VISIBLE_ID = "DEFAULT_IMAGE_SNAPSHOT_FOV_VISIBLE_ID";	
	public static final Boolean DEFAULT_IMAGE_SNAPSHOT_FOV_VISIBLE = new Boolean(false);

	// Axis Visibility Constants
	public static final String DEFAULT_IMAGE_SNAPSHOT_AXIS_VISIBLE_ID = "DEFAULT_IMAGE_SNAPSHOT_AXIS_VISIBLE_ID";	
	public static final Boolean DEFAULT_IMAGE_SNAPSHOT_AXIS_VISIBLE = new Boolean(false);

	// Axis Length Constants
	public static final String DEFAULT_IMAGE_SNAPSHOT_AXIS_LENGTH_ID = "DEFAULT_IMAGE_SNAPSHOT_AXIS_LENGTH_ID";	
	public static final Double DEFAULT_IMAGE_SNAPSHOT_AXIS_LENGTH = new Double(1.0);

	
	// Image Projection on FOV Constants.
	public static final String DEFAULT_IMAGE_SNAPSHOT_SHOW_PROJECTION_ON_FOV_ID = "DEFAULT_IMAGE_SNAPSHOT_SHOW_PROJECTION_ON_FOV_ID";	
	public static final Boolean DEFAULT_IMAGE_SNAPSHOT_SHOW_PROJECTION_ON_FOV = new Boolean(false);
	
	// Presentation Mode Constants.
	public static final String DEFAULT_IMAGE_SNAPSHOT_FOV_PRESENTATION_MODE_ID = "DEFAULT_IMAGE_SNAPSHOT_FOV_PRESENTATION_MODE_ID";	
	public static final MeshPresentationMode DEFAULT_IMAGE_SNAPSHOT_FOV_PRESENTATION_MODE = MeshPresentationMode.WIREFRAME;
	
	// Color Constants.
	public static final String DEFAULT_IMAGE_SNAPSHOT_FOV_COLOR_ID = "DEFAULT_IMAGE_SNAPSHOT_FOV_COLOR_ID";		
	public static final RGB DEFAULT_IMAGE_SNAPSHOT_FOV_COLOR = new RGB(255,255,0);
	
	
	
}

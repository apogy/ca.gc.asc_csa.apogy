/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.imaging.ui.models;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.spi.model.VView;
import org.eclipse.emf.ecp.view.spi.model.VViewFactory;
import org.eclipse.emf.ecp.view.spi.model.VViewModelProperties;
import org.eclipse.emf.ecp.view.spi.provider.IViewProvider;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.ApogyAddonsSensorsImagingPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ImageSnapshot;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.PropertyType;
import ca.gc.asc_csa.apogy.common.images.ApogyCommonImagesPackage;
import ca.gc.asc_csa.apogy.common.images.ui.elements.AbstractEImageVElement;

public class ImageSnapshotViewModel implements IViewProvider 
{
	@Override
	public double canProvideViewModel(EObject eObject, VViewModelProperties properties) 
	{
		if (eObject instanceof ImageSnapshot) 
		{
			return 10;
		}
		return NOT_APPLICABLE;
	}

	@Override
	public VView provideViewModel(EObject eObject, VViewModelProperties properties) 
	{		
		EClass eClass = eObject.eClass();
		
		VView vView = VViewFactory.eINSTANCE.createView();
		vView.setRootEClass(eClass);
		vView.setVisible(true);
	
		List<VControl> vControls = new ArrayList<>();
		
		for(EAttribute attribute : eClass.getEAllAttributes())
		{
			if(attribute != ApogyCommonImagesPackage.Literals.EIMAGE__IMAGE_CONTENT)
			{
				PropertyType type = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.getPropertyType(attribute);				
				if(type != PropertyType.NONE)
				{
					VControl vControl = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createVControl(attribute);
					if(vControl != null) vControls.add(vControl);
				}
			}
		}
		
		// Adds all References		
		for(EReference eReference : eClass.getEAllReferences())
		{				
			// Skip if the eReference is the Image.
			if(eReference != ApogyAddonsSensorsImagingPackage.Literals.IMAGE_SNAPSHOT__IMAGE)
			{							
				PropertyType type = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.getPropertyType(eReference);				
				if(type != PropertyType.NONE)
				{
					VControl vControl = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createVControl(eReference);
					if(vControl != null) vControls.add(vControl);
				}
			}
		}

		/** Add the Image element.*/		
		ImageSnapshot imageSnapshot = (ImageSnapshot) eObject;
		AbstractEImageVElement imageElement = new AbstractEImageVElement(imageSnapshot.getImage());
		vView.getChildren().add(imageElement);
		
		SortedSet<VControl> sortedVControls = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.sortVControlAlphabetically(vControls);		
		vView.getChildren().addAll(sortedVControls);
		
		return vView;
	}
}

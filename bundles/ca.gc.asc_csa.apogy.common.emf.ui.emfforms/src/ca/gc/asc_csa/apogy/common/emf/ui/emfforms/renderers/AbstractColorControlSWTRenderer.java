/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.emfforms.renderers;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.core.swt.SimpleControlSWTControlSWTRenderer;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.jface.preference.ColorSelector;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.Activator;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;

public abstract class AbstractColorControlSWTRenderer extends SimpleControlSWTControlSWTRenderer {

	private Composite content;
	private ColorSelector selector;
	private Button transparent;

	private IChangeListener changeListener;
	private IPropertyChangeListener propertyChangeListener;

	@Inject
	public AbstractColorControlSWTRenderer(VControl vElement, ViewModelContext viewContext, ReportService reportService,
			EMFFormsDatabinding emfFormsDatabinding, EMFFormsLabelProvider emfFormsLabelProvider,
			VTViewTemplateProvider vtViewTemplateProvider) {
		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);
	}

	@Override
	protected Binding[] createBindings(Control control) throws DatabindingFailedException {
		getModelValue().addChangeListener(getChangeListener());

		return new Binding[0];
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Control createSWTControl(Composite parent) throws DatabindingFailedException {
		content = new Composite(parent, SWT.None);
		content.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				if(changeListener != null){
					try {
						getModelValue().removeChangeListener(getChangeListener());
					} catch (DatabindingFailedException e1) {}
				}
			}
		});
		content.setBackground(parent.getBackground());
		content.addPaintListener(new PaintListener() {
			
			@Override
			public void paintControl(PaintEvent e) {
				content.setBackground(parent.getBackground());
				
			}
		});
		content.setBackgroundMode(SWT.INHERIT_FORCE);
		content.setLayout(new GridLayout(2, false));
		content.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));

		initSelector();
		
		/** Button to set the color as transparent */
		transparent = new Button(content, SWT.None);
		transparent.setText("Clear");
		transparent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		transparent.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					getModelValue().setValue(getNullObj());
				} catch (DatabindingFailedException e1) {}
			}
		});

		return content;
	}
	
	private void initSelector(){
		/** Color selection button */
		selector = new ColorSelector(content);
		selector.getButton().setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, true, 1, 1));
		if(getValue() != null){
			selector.setColorValue(getRGB());
		}
		if(transparent != null){
			selector.getButton().moveAbove(transparent);
		}
		selector.addListener(getPropertyChangeListener());
	}
	
	/**
	 * Property change listener for the color selector.
	 * @return
	 */
	private IPropertyChangeListener getPropertyChangeListener(){
		if(propertyChangeListener == null){
			propertyChangeListener = new IPropertyChangeListener() {

				@SuppressWarnings("unchecked")
				@Override
				public void propertyChange(PropertyChangeEvent event) {
					if (event.getNewValue() instanceof RGB) {
						try {
							getModelValue().setValue(convert((RGB) event.getNewValue()));
						} catch (DatabindingFailedException e) {
							Logger.INSTANCE.log(Activator.ID, " error setting the color value. ", EventSeverity.ERROR);
						}
					}
				}
			};
		}
		return propertyChangeListener;
	}
	
	/**
	 * ChangeListener for the value
	 */
	public IChangeListener getChangeListener() {
		if(changeListener == null){
			changeListener = new IChangeListener() {

				@Override
				public void handleChange(ChangeEvent event) {
					if (getValue() != null) {
						selector.setColorValue(getRGB());
					} else {
						if (selector != null) {
							selector.getButton().dispose();
							initSelector();
							content.layout();
						}
					}
				}
			};
		}
		return changeListener;
	}

	@Override
	protected String getUnsetText() {
		return "Unset";
	}

	/**
	 * Gets the value and deals with the exceptions.
	 * @return
	 */
	protected Object getValue() {
		try {
			Object obj = getModelValue().getValue();
			return obj;
		} catch (DatabindingFailedException e) {
			Logger.INSTANCE.log(Activator.ID, " error setting the RGB value. ", EventSeverity.ERROR);
		}
		return null;
	}
	
	
	/**
	 * @return  the {@link RGB} value.
	 */
	abstract protected RGB getRGB();
	
	/**
	 * Converts a {@link RGB} output to a value that can be set in the model.
	 * @param rgb
	 * @return the converted {@link Object}
	 */
	abstract protected Object convert(RGB rgb);

	/**
	 * Get a null object to set the transparent color. This is used beacuse
	 * depending on how the color is stored, a transparent color is not
	 * implemented the same way.
	 */
	abstract protected Object getNullObj();
}

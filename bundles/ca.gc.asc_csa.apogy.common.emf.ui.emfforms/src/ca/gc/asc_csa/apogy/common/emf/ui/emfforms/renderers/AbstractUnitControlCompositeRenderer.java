/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.emfforms.renderers;

import java.text.DecimalFormat;

import javax.inject.Inject;
import javax.measure.converter.ConversionException;
import javax.measure.unit.Unit;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.core.swt.SimpleControlSWTControlSWTRenderer;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.spi.model.VElement;
import org.eclipse.emf.ecp.view.spi.model.VFeaturePathDomainModelReference;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.google.common.collect.Range;

import ca.gc.asc_csa.apogy.common.emf.Ranges;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.dialogs.SelectUnitDialog;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.PropertyType;
import ca.gc.asc_csa.apogy.common.emf.ui.preferences.PreferencesConstants;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;

public abstract class AbstractUnitControlCompositeRenderer extends SimpleControlSWTControlSWTRenderer {

	protected Text textValue;
	private Button buttonUnits;

	private Binding colorBinding;
	private Binding valueBinding;

	private IPropertyChangeListener propertyChangeListener;
	
	private VControl vElement;

	@Inject
	public AbstractUnitControlCompositeRenderer(VControl vElement, ViewModelContext viewContext,
			ReportService reportService, EMFFormsDatabinding emfFormsDatabinding,
			EMFFormsLabelProvider emfFormsLabelProvider, VTViewTemplateProvider vtViewTemplateProvider) {

		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);

		
		/** Preference listener */
		ca.gc.asc_csa.apogy.common.emf.ui.Activator.getDefault().getPreferenceStore()
				.addPropertyChangeListener(getPropertyChangeListener());
	}

	@Override
	protected Control createSWTControl(Composite parent) 
	{
		PropertyType propertyType = getPropertyType(getVElement());
						
		/**
		 * This composite is created because it is impossible to change the
		 * background color of a control directly under parent.
		 */
		final Composite main = new Composite(parent, SWT.None);
		main.setLayout(GridLayoutFactory.fillDefaults().margins(0, 0).numColumns(2).create());
		main.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		main.setBackground(parent.getBackground());
		main.setBackgroundMode(SWT.INHERIT_FORCE);
		
		
		/** Value */
		textValue = new Text(main, SWT.BORDER | SWT.RIGHT);
		textValue.setEditable(propertyType == PropertyType.EDITABLE);
		textValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		/** Units */
		buttonUnits = new Button(main, SWT.PUSH);
		GridData gd_buttonUnits = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_buttonUnits.widthHint = 100;
		buttonUnits.setLayoutData(gd_buttonUnits);
		updateButton();
		buttonUnits.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				SelectUnitDialog dialog = new SelectUnitDialog(buttonUnits.getShell(), getValue(),
						getDecimalFormat(),
						getNativeUnits(),
						getDisplayUnits()) {
					@Override
					protected void buttonPressed(int buttonId) {
						if (buttonId == OK) {
							try {

								/** If the format registry needs to be updated */
								if (getDecimalFormat() != getResultFormat()) {
									addToDecimalFormatRegistry(getResultFormat());
								}
								/** If the units registry needs to be updated */
								if (getDisplayUnits() != getResultUnit()) {
									addToDisplayUnitsRegistry(getResultUnit());
								}
							} catch (ConversionException e) {
								Logger.INSTANCE.log(Activator.ID, " value can not be converted ", EventSeverity.ERROR);
							}
						}
						super.buttonPressed(buttonId);
					}
				};
				dialog.open();
				if(dialog.getReturnCode() == Dialog.OK){
					updateButton();
					textValue.setText(getFormatedValue());
				}
			}
		});

		return main;
	}

	@Override
	protected Binding[] createBindings(Control control) throws DatabindingFailedException {
		Binding[] bindings = new Binding[3];

		IObservableValue<?> textValueText = WidgetProperties.text(SWT.FocusOut).observe(textValue);				
		valueBinding = getDataBindingContext().bindValue(textValueText, getModelValue(), getUpdateModelValueStrategy(),	getUpdateTextValueStrategy());
		bindings[0] = valueBinding;

		/** Range colors binding */
		IObservableValue<?> textValueBackground = WidgetProperties.background().observe(textValue);
		colorBinding = getDataBindingContext().bindValue(textValueBackground, getModelValue(),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(String.class, Color.class) {

							@Override
							public Object convert(Object fromObject) {
								Color color = SWTResourceManager.getColor(SWT.COLOR_TRANSPARENT);
								color = ApogyCommonEMFUIFacade.INSTANCE.getColorForRange(getRange());

								return color;
							}
						}));
		bindings[1] = colorBinding;

		/** Units binding */
		IObservableValue<?> buttonText = WidgetProperties.text().observe(buttonUnits);
		bindings[2] = getDataBindingContext().bindValue(buttonText, getModelValue(),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(EObject.class, String.class) {
							@Override
							public Object convert(Object fromObject) {
								Unit<?> units = getDisplayUnits();
								return units == null ? "Select units" : units.toString();
							}
						}));

		return bindings;
	}


	/**
	 * Update strategy for the model.
	 */
	protected abstract UpdateValueStrategy getUpdateModelValueStrategy();
	/**
	 * Update strategy fot the value text.
	 */
	protected abstract UpdateValueStrategy getUpdateTextValueStrategy();
	/**
	 * Adds the unit to the registry using the right {@link UnitsProvider} type.
	 * @param resultUnits
	 */
	protected abstract void addToDisplayUnitsRegistry(Unit<?> resultUnits);
	/**
	 * Adds the format to the registry using the right {@link FormatProvider} type.
	 * @param resultFormat
	 */
	protected abstract void addToDecimalFormatRegistry(DecimalFormat resultFormat);
	/**
	 * @return the native {@link Unit} 
	 */
	protected abstract Unit<?> getNativeUnits();
	/**
	 * Gets the display {@link Unit} using the right {@link UnitsProviderParameters}. 
	 * @return the native {@link Unit} 
	 */
	protected abstract Unit<?> getDisplayUnits();
	/**
	 * Gets the display {@link DecimalFormat} using the right {@link FormatProviderParameters}. 
	 * @return the {@link DecimalFormat}.
	 */
	protected abstract DecimalFormat getDecimalFormat();

	/**
	 * @return the formatted {@link String} of the value but formatted using the
	 *         {@link DecimalFormat} and the display {@link Unit}s.
	 */
	protected abstract String getFormatedValue();

	/**
	 * @return the current {@link Range} of the value.
	 */
	protected abstract Ranges getRange();
	
	/**
	 * Gets the {@link ETypedElement} and catches the {@link DatabindingFailedException} if it occurs.
	 * @return {@link ETypedElement}.
	 */
	protected ETypedElement getTypedElement() {
		try {
			return (ETypedElement) getModelValue().getValueType();
		} catch (DatabindingFailedException e) {
			Logger.INSTANCE.log(Activator.ID, "Error getting the typedElement. ", EventSeverity.ERROR);
			return null;
		}
	}

	/**
	 * Gets the value and catches the {@link DatabindingFailedException} if it occurs.
	 * @return {@link Number} value.
	 */
	protected Number getValue() {
		try {
			return (Number) getModelValue().getValue();
		} catch (DatabindingFailedException e) {
			Logger.INSTANCE.log(Activator.ID, "Error getting the value. ", EventSeverity.ERROR);
			return null;
		}
	}

	/** Preference listener */
	private IPropertyChangeListener getPropertyChangeListener() {
		if (propertyChangeListener == null) {
			propertyChangeListener = new IPropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent event) {
					/** Unit of format preference event, update the value text */
					if (event.getProperty().equals(PreferencesConstants.TYPED_ELEMENTS_UNITS_ID)
							|| PreferencesConstants.isFormatPreference(event.getProperty())) {
						textValue.setText(getFormatedValue());
						updateButton();

					} 
					/** Range preference event, update the background color */
					else if (event.getProperty().equals(Ranges.UNKNOWN.getName())
							|| event.getProperty().equals(Ranges.NOMINAL.getName())
							|| event.getProperty().equals(Ranges.WARNING.getName())
							|| event.getProperty().equals(Ranges.ALARM.getName())
							|| event.getProperty().equals(Ranges.OUT_OF_RANGE.getName())) {
						colorBinding.updateModelToTarget();
					}
				}
			};
		}

		return propertyChangeListener;
	}

	/** Updates the buttons */
	private void updateButton() {
		Unit<?> units = getDisplayUnits();
		if (units != null) {
			buttonUnits.setText(units.toString());
		} else {
			buttonUnits.setText("Select units");
		}
		buttonUnits.requestLayout();
	}

	@Override
	protected String getUnsetText() {
		return "unset";
	}
	
	protected PropertyType getPropertyType(VElement vElement)
	{
		PropertyType propertyType = PropertyType.EDITABLE;
		if(vElement instanceof VControl)
		{
			VControl vControl = (VControl) vElement;
			if(vControl.getDomainModelReference() instanceof VFeaturePathDomainModelReference)
			{
				VFeaturePathDomainModelReference vFeaturePathDomainModelReference  = (VFeaturePathDomainModelReference) vControl.getDomainModelReference();
				EStructuralFeature feature = vFeaturePathDomainModelReference.getDomainModelEFeature();
				
				if(feature.isDerived() || !feature.isChangeable())
				{
					propertyType = PropertyType.READONLY;
				}				
				else
				{
					if(feature instanceof EAttribute)
					{									
						propertyType = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.getPropertyType((EAttribute) feature);					
					}
					else if(feature instanceof EReference)
					{
						propertyType = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.getPropertyType((EReference) feature);
					}
				}
			}
		}
		
		return propertyType;
	}
	
	@Override
	protected void dispose() {
		if(propertyChangeListener != null){
			ca.gc.asc_csa.apogy.common.emf.ui.Activator.getDefault().getPreferenceStore()
			.removePropertyChangeListener(getPropertyChangeListener());
		}
		super.dispose();
	}
}
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.emfforms.renderers;

import java.text.DecimalFormat;

import javax.inject.Inject;
import javax.measure.unit.Unit;

import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.Ranges;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.PropertyType;

public class UnitControlCompositeRenderer extends AbstractUnitControlCompositeRenderer {

	@Inject
	public UnitControlCompositeRenderer(VControl vElement, ViewModelContext viewContext, ReportService reportService,
			EMFFormsDatabinding emfFormsDatabinding, EMFFormsLabelProvider emfFormsLabelProvider,
			VTViewTemplateProvider vtViewTemplateProvider) {
		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);		
	}

	@Override
	protected UpdateValueStrategy getUpdateModelValueStrategy() 	
	{
		PropertyType propertyType = getPropertyType(getVElement());
		
		if(propertyType == PropertyType.EDITABLE)
		{
			return new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
				.setConverter(new Converter(String.class, getTypedElement().getEType().getInstanceClass()) {
					@Override
					public Object convert(Object fromObject) {
						if (fromObject != null && !"".equals(fromObject)) {
							try {
								Number number = ApogyCommonEMFUIFacade.INSTANCE.convertToNativeUnits(
										Double.parseDouble((String) fromObject), getNativeUnits(), getDisplayUnits(),
										getTypedElement().getEType());

								/** Format to update the text if there is rounding errors */
								Unit<?> displayUnits = getDisplayUnits();
								Unit<?> nativeUnits = getNativeUnits();
								DecimalFormat format = getDecimalFormat();

								if (displayUnits != null && !displayUnits.equals(nativeUnits)) {
									textValue.setText(format.format(nativeUnits.getConverterTo(displayUnits).convert(number.doubleValue())));
								}else{
									textValue.setText(format.format(number.doubleValue()));
								}

								/** Set the new value in the right type */
								EClassifier classifier = getTypedElement().getEType();
								if (classifier == EcorePackage.Literals.EFLOAT) {
									number = number.floatValue();
								} else if (classifier == EcorePackage.Literals.EBYTE) {
									number = number.byteValue();
								} else if (classifier == EcorePackage.Literals.ESHORT) {
									number = number.shortValue();
								} else if (classifier == EcorePackage.Literals.EINT) {
									number = number.intValue();
								} else if (classifier == EcorePackage.Literals.ELONG) {
									number = number.longValue();
								}

								return number;
							} catch (Exception e) {
								/** Error message */
								MessageDialog.openError(textValue.getDisplay().getActiveShell(), "Invalid Number",
										"The number entered is invalid. The value will be unset.");
							}
						}
						/** Set to the current value */
						textValue.setText(getFormatedValue());
						return getValue();
					}
				});
		}
		else
		{
			return new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER);
		}
	}

	@Override
	protected UpdateValueStrategy getUpdateTextValueStrategy() {
		return new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
				.setConverter(new Converter(getTypedElement().getEType().getInstanceClass(), String.class) {

					@Override
					public Object convert(Object fromObject) {
						if (fromObject != null) {
							String value = getFormatedValue();
							return value == null ? "" : value;
						}
						return "";

					}
				});
	}

	@Override
	protected void addToDisplayUnitsRegistry(Unit<?> resultUnits) {
		/** Create a units provider */
		SimpleUnitsProvider unitsProvider = ApogyCommonEMFUIFactory.eINSTANCE.createSimpleUnitsProvider();
		unitsProvider.setUnit(resultUnits);

		/** Update the units registry */
		ApogyCommonEMFUIFacade.INSTANCE.addUnitsProviderToRegistry(getTypedElement(), unitsProvider);
	}

	@Override
	protected void addToDecimalFormatRegistry(DecimalFormat resultFormat) {
		/** Create a format provider */
		SimpleFormatProvider formatProvider = ApogyCommonEMFUIFactory.eINSTANCE.createSimpleFormatProvider();
		formatProvider.setFormatPattern(resultFormat.toPattern());

		/** Update the format registry */
		ApogyCommonEMFUIFacade.INSTANCE.addFormatProviderToRegistry(getTypedElement(), formatProvider);
	}

	@Override
	protected Unit<?> getNativeUnits() {
		return ApogyCommonEMFFacade.INSTANCE.getEngineeringUnits(getTypedElement());
	}

	@Override
	protected Unit<?> getDisplayUnits() {
		return ApogyCommonEMFUIFacade.INSTANCE.getDisplayUnits(getTypedElement());
	}

	@Override
	protected DecimalFormat getDecimalFormat() {
		return ApogyCommonEMFUIFacade.INSTANCE.getDisplayFormat(getTypedElement());
	}

	@Override
	protected String getFormatedValue() {
		Double valueDouble = getValue().doubleValue();

		DecimalFormat format = ApogyCommonEMFUIFacade.INSTANCE.getDisplayFormat(getTypedElement());
		Unit<?> displayUnits = ApogyCommonEMFUIFacade.INSTANCE.getDisplayUnits(getTypedElement());
		Unit<?> nativeUnits = ApogyCommonEMFFacade.INSTANCE.getEngineeringUnits(getTypedElement());

		if (displayUnits != null && !displayUnits.equals(nativeUnits)) {
			valueDouble = nativeUnits.getConverterTo(displayUnits).convert(valueDouble);
		}

		return format.format(valueDouble);
	}
	
	@Override
	protected Ranges getRange() {
		return ApogyCommonEMFFacade.INSTANCE.getRange(getTypedElement(), getValue());
	}

}
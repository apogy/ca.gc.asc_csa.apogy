/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.*;

import java.util.List;
import java.util.SortedSet;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecp.ui.view.swt.ECPSWTView;
import org.eclipse.emf.ecp.view.spi.model.VContainedElement;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.spi.model.VView;
import org.eclipse.swt.widgets.Composite;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCommonEMFUiEMFFormsFactoryImpl extends EFactoryImpl implements ApogyCommonEMFUiEMFFormsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyCommonEMFUiEMFFormsFactory init() {
		try {
			ApogyCommonEMFUiEMFFormsFactory theApogyCommonEMFUiEMFFormsFactory = (ApogyCommonEMFUiEMFFormsFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyCommonEMFUiEMFFormsPackage.eNS_URI);
			if (theApogyCommonEMFUiEMFFormsFactory != null) {
				return theApogyCommonEMFUiEMFFormsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyCommonEMFUiEMFFormsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonEMFUiEMFFormsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyCommonEMFUiEMFFormsPackage.APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE: return createApogyCommonEMFUiEMFFormsFacade();
			case ApogyCommonEMFUiEMFFormsPackage.EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER: return createEObjectEMFFormsWizardPageProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ApogyCommonEMFUiEMFFormsPackage.PROPERTY_TYPE:
				return createPropertyTypeFromString(eDataType, initialValue);
			case ApogyCommonEMFUiEMFFormsPackage.COMPOSITE:
				return createCompositeFromString(eDataType, initialValue);
			case ApogyCommonEMFUiEMFFormsPackage.VVIEW:
				return createVViewFromString(eDataType, initialValue);
			case ApogyCommonEMFUiEMFFormsPackage.VCONTROL:
				return createVControlFromString(eDataType, initialValue);
			case ApogyCommonEMFUiEMFFormsPackage.VCONTAINED_ELEMENT:
				return createVContainedElementFromString(eDataType, initialValue);
			case ApogyCommonEMFUiEMFFormsPackage.ECPSWT_VIEW:
				return createECPSWTViewFromString(eDataType, initialValue);
			case ApogyCommonEMFUiEMFFormsPackage.LIST:
				return createListFromString(eDataType, initialValue);
			case ApogyCommonEMFUiEMFFormsPackage.SORTED_SET:
				return createSortedSetFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ApogyCommonEMFUiEMFFormsPackage.PROPERTY_TYPE:
				return convertPropertyTypeToString(eDataType, instanceValue);
			case ApogyCommonEMFUiEMFFormsPackage.COMPOSITE:
				return convertCompositeToString(eDataType, instanceValue);
			case ApogyCommonEMFUiEMFFormsPackage.VVIEW:
				return convertVViewToString(eDataType, instanceValue);
			case ApogyCommonEMFUiEMFFormsPackage.VCONTROL:
				return convertVControlToString(eDataType, instanceValue);
			case ApogyCommonEMFUiEMFFormsPackage.VCONTAINED_ELEMENT:
				return convertVContainedElementToString(eDataType, instanceValue);
			case ApogyCommonEMFUiEMFFormsPackage.ECPSWT_VIEW:
				return convertECPSWTViewToString(eDataType, instanceValue);
			case ApogyCommonEMFUiEMFFormsPackage.LIST:
				return convertListToString(eDataType, instanceValue);
			case ApogyCommonEMFUiEMFFormsPackage.SORTED_SET:
				return convertSortedSetToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonEMFUiEMFFormsFacade createApogyCommonEMFUiEMFFormsFacade() {
		ApogyCommonEMFUiEMFFormsFacadeImpl apogyCommonEMFUiEMFFormsFacade = new ApogyCommonEMFUiEMFFormsFacadeImpl();
		return apogyCommonEMFUiEMFFormsFacade;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObjectEMFFormsWizardPageProvider createEObjectEMFFormsWizardPageProvider() {
		EObjectEMFFormsWizardPageProviderImpl eObjectEMFFormsWizardPageProvider = new EObjectEMFFormsWizardPageProviderImpl();
		return eObjectEMFFormsWizardPageProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyType createPropertyTypeFromString(EDataType eDataType, String initialValue) {
		PropertyType result = PropertyType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPropertyTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Composite createCompositeFromString(EDataType eDataType, String initialValue) {
		return (Composite)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCompositeToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VView createVViewFromString(EDataType eDataType, String initialValue) {
		return (VView)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVViewToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VControl createVControlFromString(EDataType eDataType, String initialValue) {
		return (VControl)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVControlToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VContainedElement createVContainedElementFromString(EDataType eDataType, String initialValue) {
		return (VContainedElement)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVContainedElementToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ECPSWTView createECPSWTViewFromString(EDataType eDataType, String initialValue) {
		return (ECPSWTView)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertECPSWTViewToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<?> createListFromString(EDataType eDataType, String initialValue) {
		return (List<?>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SortedSet<?> createSortedSetFromString(EDataType eDataType, String initialValue) {
		return (SortedSet<?>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSortedSetToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonEMFUiEMFFormsPackage getApogyCommonEMFUiEMFFormsPackage() {
		return (ApogyCommonEMFUiEMFFormsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyCommonEMFUiEMFFormsPackage getPackage() {
		return ApogyCommonEMFUiEMFFormsPackage.eINSTANCE;
	}

} //ApogyCommonEMFUiEMFFormsFactoryImpl

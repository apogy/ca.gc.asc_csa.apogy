/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.emfforms;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyCommonEMFUiEMFForms' childCreationExtenders='true' extensibleProviderFactory='true' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque, \n     Olivier L. Larouche\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' modelName='ApogyCommonEMFUiEMFForms' modelDirectory='/ca.gc.asc_csa.apogy.common.emf.ui.emfforms/src-generated' editDirectory='/ca.gc.asc_csa.apogy.common.emf.ui.emfforms/src-generated' basePackage='ca.gc.asc_csa.apogy.common.emf.ui'"
 * @generated
 */
public interface ApogyCommonEMFUiEMFFormsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "emfforms";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.common.emf.ui.emfforms";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "emfforms";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCommonEMFUiEMFFormsPackage eINSTANCE = ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsFacadeImpl <em>Facade</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsFacadeImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getApogyCommonEMFUiEMFFormsFacade()
	 * @generated
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE = 0;

	/**
	 * The number of structural features of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Create EMF Forms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT = 0;

	/**
	 * The operation id for the '<em>Create EMF Forms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_STRING = 1;

	/**
	 * The operation id for the '<em>Create EMF Forms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_BOOLEAN = 2;

	/**
	 * The operation id for the '<em>Create EMF Forms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_VVIEW = 3;

	/**
	 * The operation id for the '<em>Create EMF Forms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_VVIEW_STRING = 4;

	/**
	 * The operation id for the '<em>Create Default View Model</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_DEFAULT_VIEW_MODEL__EOBJECT = 5;

	/**
	 * The operation id for the '<em>Sort VControl Alphabetically</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___SORT_VCONTROL_ALPHABETICALLY__LIST = 6;

	/**
	 * The operation id for the '<em>Sort VElement Alphabetically</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___SORT_VELEMENT_ALPHABETICALLY__LIST = 7;

	/**
	 * The operation id for the '<em>Create VControl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_VCONTROL__EATTRIBUTE = 8;

	/**
	 * The operation id for the '<em>Create VControl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_VCONTROL__EREFERENCE = 9;

	/**
	 * The operation id for the '<em>Get Property Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___GET_PROPERTY_TYPE__EATTRIBUTE = 10;

	/**
	 * The operation id for the '<em>Get Property Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___GET_PROPERTY_TYPE__EREFERENCE = 11;

	/**
	 * The number of operations of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE_OPERATION_COUNT = 12;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.EObjectEMFFormsWizardPageProviderImpl <em>EObject EMF Forms Wizard Page Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.EObjectEMFFormsWizardPageProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getEObjectEMFFormsWizardPageProvider()
	 * @generated
	 */
	int EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER = 1;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER__PAGES = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER__EOBJECT = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>EObject EMF Forms Wizard Page Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER_FEATURE_COUNT = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>EObject EMF Forms Wizard Page Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER_OPERATION_COUNT = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.PropertyType <em>Property Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.PropertyType
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getPropertyType()
	 * @generated
	 */
	int PROPERTY_TYPE = 2;

	/**
	 * The meta object id for the '<em>Composite</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.swt.widgets.Composite
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getComposite()
	 * @generated
	 */
	int COMPOSITE = 3;


	/**
	 * The meta object id for the '<em>VView</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecp.view.spi.model.VView
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getVView()
	 * @generated
	 */
	int VVIEW = 4;


	/**
	 * The meta object id for the '<em>VControl</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecp.view.spi.model.VControl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getVControl()
	 * @generated
	 */
	int VCONTROL = 5;

	/**
	 * The meta object id for the '<em>VContained Element</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecp.view.spi.model.VContainedElement
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getVContainedElement()
	 * @generated
	 */
	int VCONTAINED_ELEMENT = 6;

	/**
	 * The meta object id for the '<em>ECPSWT View</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecp.ui.view.swt.ECPSWTView
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getECPSWTView()
	 * @generated
	 */
	int ECPSWT_VIEW = 7;


	/**
	 * The meta object id for the '<em>List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getList()
	 * @generated
	 */
	int LIST = 8;

	/**
	 * The meta object id for the '<em>Sorted Set</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.SortedSet
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getSortedSet()
	 * @generated
	 */
	int SORTED_SET = 9;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade <em>Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Facade</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade
	 * @generated
	 */
	EClass getApogyCommonEMFUiEMFFormsFacade();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createEMFForms(org.eclipse.swt.widgets.Composite, org.eclipse.emf.ecore.EObject) <em>Create EMF Forms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create EMF Forms</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createEMFForms(org.eclipse.swt.widgets.Composite, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createEMFForms(org.eclipse.swt.widgets.Composite, org.eclipse.emf.ecore.EObject, java.lang.String) <em>Create EMF Forms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create EMF Forms</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createEMFForms(org.eclipse.swt.widgets.Composite, org.eclipse.emf.ecore.EObject, java.lang.String)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_String();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createEMFForms(org.eclipse.swt.widgets.Composite, org.eclipse.emf.ecore.EObject, boolean) <em>Create EMF Forms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create EMF Forms</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createEMFForms(org.eclipse.swt.widgets.Composite, org.eclipse.emf.ecore.EObject, boolean)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_boolean();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createEMFForms(org.eclipse.swt.widgets.Composite, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecp.view.spi.model.VView) <em>Create EMF Forms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create EMF Forms</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createEMFForms(org.eclipse.swt.widgets.Composite, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecp.view.spi.model.VView)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_VView();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createEMFForms(org.eclipse.swt.widgets.Composite, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecp.view.spi.model.VView, java.lang.String) <em>Create EMF Forms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create EMF Forms</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createEMFForms(org.eclipse.swt.widgets.Composite, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecp.view.spi.model.VView, java.lang.String)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_VView_String();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createDefaultViewModel(org.eclipse.emf.ecore.EObject) <em>Create Default View Model</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Default View Model</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createDefaultViewModel(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__CreateDefaultViewModel__EObject();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#sortVControlAlphabetically(java.util.List) <em>Sort VControl Alphabetically</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Sort VControl Alphabetically</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#sortVControlAlphabetically(java.util.List)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__SortVControlAlphabetically__List();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#sortVElementAlphabetically(java.util.List) <em>Sort VElement Alphabetically</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Sort VElement Alphabetically</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#sortVElementAlphabetically(java.util.List)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__SortVElementAlphabetically__List();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createVControl(org.eclipse.emf.ecore.EAttribute) <em>Create VControl</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create VControl</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createVControl(org.eclipse.emf.ecore.EAttribute)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__CreateVControl__EAttribute();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createVControl(org.eclipse.emf.ecore.EReference) <em>Create VControl</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create VControl</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#createVControl(org.eclipse.emf.ecore.EReference)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__CreateVControl__EReference();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#getPropertyType(org.eclipse.emf.ecore.EAttribute) <em>Get Property Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Property Type</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#getPropertyType(org.eclipse.emf.ecore.EAttribute)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__GetPropertyType__EAttribute();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#getPropertyType(org.eclipse.emf.ecore.EReference) <em>Get Property Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Property Type</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade#getPropertyType(org.eclipse.emf.ecore.EReference)
	 * @generated
	 */
	EOperation getApogyCommonEMFUiEMFFormsFacade__GetPropertyType__EReference();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.EObjectEMFFormsWizardPageProvider <em>EObject EMF Forms Wizard Page Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EObject EMF Forms Wizard Page Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.EObjectEMFFormsWizardPageProvider
	 * @generated
	 */
	EClass getEObjectEMFFormsWizardPageProvider();

	/**
	 * Returns the meta object for enum '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.PropertyType <em>Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Property Type</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.PropertyType
	 * @generated
	 */
	EEnum getPropertyType();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.swt.widgets.Composite <em>Composite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Composite</em>'.
	 * @see org.eclipse.swt.widgets.Composite
	 * @model instanceClass="org.eclipse.swt.widgets.Composite"
	 * @generated
	 */
	EDataType getComposite();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecp.view.spi.model.VView <em>VView</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>VView</em>'.
	 * @see org.eclipse.emf.ecp.view.spi.model.VView
	 * @model instanceClass="org.eclipse.emf.ecp.view.spi.model.VView"
	 * @generated
	 */
	EDataType getVView();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecp.view.spi.model.VControl <em>VControl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>VControl</em>'.
	 * @see org.eclipse.emf.ecp.view.spi.model.VControl
	 * @model instanceClass="org.eclipse.emf.ecp.view.spi.model.VControl"
	 * @generated
	 */
	EDataType getVControl();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecp.view.spi.model.VContainedElement <em>VContained Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>VContained Element</em>'.
	 * @see org.eclipse.emf.ecp.view.spi.model.VContainedElement
	 * @model instanceClass="org.eclipse.emf.ecp.view.spi.model.VContainedElement"
	 * @generated
	 */
	EDataType getVContainedElement();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecp.ui.view.swt.ECPSWTView <em>ECPSWT View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ECPSWT View</em>'.
	 * @see org.eclipse.emf.ecp.ui.view.swt.ECPSWTView
	 * @model instanceClass="org.eclipse.emf.ecp.ui.view.swt.ECPSWTView"
	 * @generated
	 */
	EDataType getECPSWTView();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>List</em>'.
	 * @see java.util.List
	 * @model instanceClass="java.util.List" typeParameters="T"
	 * @generated
	 */
	EDataType getList();

	/**
	 * Returns the meta object for data type '{@link java.util.SortedSet <em>Sorted Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Sorted Set</em>'.
	 * @see java.util.SortedSet
	 * @model instanceClass="java.util.SortedSet" typeParameters="T"
	 * @generated
	 */
	EDataType getSortedSet();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCommonEMFUiEMFFormsFactory getApogyCommonEMFUiEMFFormsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsFacadeImpl <em>Facade</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsFacadeImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getApogyCommonEMFUiEMFFormsFacade()
		 * @generated
		 */
		EClass APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade();
		/**
		 * The meta object literal for the '<em><b>Create EMF Forms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject();
		/**
		 * The meta object literal for the '<em><b>Create EMF Forms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_STRING = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_String();
		/**
		 * The meta object literal for the '<em><b>Create EMF Forms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_BOOLEAN = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_boolean();
		/**
		 * The meta object literal for the '<em><b>Create EMF Forms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_VVIEW = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_VView();
		/**
		 * The meta object literal for the '<em><b>Create EMF Forms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_VVIEW_STRING = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_VView_String();
		/**
		 * The meta object literal for the '<em><b>Create Default View Model</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_DEFAULT_VIEW_MODEL__EOBJECT = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__CreateDefaultViewModel__EObject();
		/**
		 * The meta object literal for the '<em><b>Sort VControl Alphabetically</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___SORT_VCONTROL_ALPHABETICALLY__LIST = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__SortVControlAlphabetically__List();
		/**
		 * The meta object literal for the '<em><b>Sort VElement Alphabetically</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___SORT_VELEMENT_ALPHABETICALLY__LIST = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__SortVElementAlphabetically__List();
		/**
		 * The meta object literal for the '<em><b>Create VControl</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_VCONTROL__EATTRIBUTE = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__CreateVControl__EAttribute();
		/**
		 * The meta object literal for the '<em><b>Create VControl</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_VCONTROL__EREFERENCE = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__CreateVControl__EReference();
		/**
		 * The meta object literal for the '<em><b>Get Property Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___GET_PROPERTY_TYPE__EATTRIBUTE = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__GetPropertyType__EAttribute();
		/**
		 * The meta object literal for the '<em><b>Get Property Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___GET_PROPERTY_TYPE__EREFERENCE = eINSTANCE.getApogyCommonEMFUiEMFFormsFacade__GetPropertyType__EReference();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.EObjectEMFFormsWizardPageProviderImpl <em>EObject EMF Forms Wizard Page Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.EObjectEMFFormsWizardPageProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getEObjectEMFFormsWizardPageProvider()
		 * @generated
		 */
		EClass EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER = eINSTANCE.getEObjectEMFFormsWizardPageProvider();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.emfforms.PropertyType <em>Property Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.PropertyType
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getPropertyType()
		 * @generated
		 */
		EEnum PROPERTY_TYPE = eINSTANCE.getPropertyType();
		/**
		 * The meta object literal for the '<em>Composite</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.swt.widgets.Composite
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getComposite()
		 * @generated
		 */
		EDataType COMPOSITE = eINSTANCE.getComposite();
		/**
		 * The meta object literal for the '<em>VView</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecp.view.spi.model.VView
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getVView()
		 * @generated
		 */
		EDataType VVIEW = eINSTANCE.getVView();
		/**
		 * The meta object literal for the '<em>VControl</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecp.view.spi.model.VControl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getVControl()
		 * @generated
		 */
		EDataType VCONTROL = eINSTANCE.getVControl();
		/**
		 * The meta object literal for the '<em>VContained Element</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecp.view.spi.model.VContainedElement
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getVContainedElement()
		 * @generated
		 */
		EDataType VCONTAINED_ELEMENT = eINSTANCE.getVContainedElement();
		/**
		 * The meta object literal for the '<em>ECPSWT View</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecp.ui.view.swt.ECPSWTView
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getECPSWTView()
		 * @generated
		 */
		EDataType ECPSWT_VIEW = eINSTANCE.getECPSWTView();
		/**
		 * The meta object literal for the '<em>List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getList()
		 * @generated
		 */
		EDataType LIST = eINSTANCE.getList();
		/**
		 * The meta object literal for the '<em>Sorted Set</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.SortedSet
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl.ApogyCommonEMFUiEMFFormsPackageImpl#getSortedSet()
		 * @generated
		 */
		EDataType SORTED_SET = eINSTANCE.getSortedSet();

	}

} //ApogyCommonEMFUiEMFFormsPackage

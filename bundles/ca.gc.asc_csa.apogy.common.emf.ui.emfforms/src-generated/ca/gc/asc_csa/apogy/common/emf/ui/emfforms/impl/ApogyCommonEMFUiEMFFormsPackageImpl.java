/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsPackage;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.EObjectEMFFormsWizardPageProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.PropertyType;
import java.util.List;
import java.util.SortedSet;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecp.ui.view.swt.ECPSWTView;
import org.eclipse.emf.ecp.view.spi.model.VContainedElement;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.spi.model.VView;
import org.eclipse.swt.widgets.Composite;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCommonEMFUiEMFFormsPackageImpl extends EPackageImpl implements ApogyCommonEMFUiEMFFormsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass apogyCommonEMFUiEMFFormsFacadeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eObjectEMFFormsWizardPageProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum propertyTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType compositeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType vViewEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType vControlEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType vContainedElementEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ecpswtViewEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType listEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType sortedSetEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyCommonEMFUiEMFFormsPackageImpl() {
		super(eNS_URI, ApogyCommonEMFUiEMFFormsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCommonEMFUiEMFFormsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyCommonEMFUiEMFFormsPackage init() {
		if (isInited) return (ApogyCommonEMFUiEMFFormsPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFUiEMFFormsPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCommonEMFUiEMFFormsPackageImpl theApogyCommonEMFUiEMFFormsPackage = (ApogyCommonEMFUiEMFFormsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCommonEMFUiEMFFormsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCommonEMFUiEMFFormsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCommonEMFUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCommonEMFUiEMFFormsPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCommonEMFUiEMFFormsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCommonEMFUiEMFFormsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCommonEMFUiEMFFormsPackage.eNS_URI, theApogyCommonEMFUiEMFFormsPackage);
		return theApogyCommonEMFUiEMFFormsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApogyCommonEMFUiEMFFormsFacade() {
		return apogyCommonEMFUiEMFFormsFacadeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_String() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_boolean() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_VView() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_VView_String() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__CreateDefaultViewModel__EObject() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__SortVControlAlphabetically__List() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__SortVElementAlphabetically__List() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__CreateVControl__EAttribute() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__CreateVControl__EReference() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__GetPropertyType__EAttribute() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFUiEMFFormsFacade__GetPropertyType__EReference() {
		return apogyCommonEMFUiEMFFormsFacadeEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEObjectEMFFormsWizardPageProvider() {
		return eObjectEMFFormsWizardPageProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPropertyType() {
		return propertyTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getComposite() {
		return compositeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getVView() {
		return vViewEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getVControl() {
		return vControlEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getVContainedElement() {
		return vContainedElementEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getECPSWTView() {
		return ecpswtViewEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getList() {
		return listEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSortedSet() {
		return sortedSetEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonEMFUiEMFFormsFactory getApogyCommonEMFUiEMFFormsFactory() {
		return (ApogyCommonEMFUiEMFFormsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		apogyCommonEMFUiEMFFormsFacadeEClass = createEClass(APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_STRING);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_BOOLEAN);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_VVIEW);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_VVIEW_STRING);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_DEFAULT_VIEW_MODEL__EOBJECT);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___SORT_VCONTROL_ALPHABETICALLY__LIST);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___SORT_VELEMENT_ALPHABETICALLY__LIST);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_VCONTROL__EATTRIBUTE);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_VCONTROL__EREFERENCE);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___GET_PROPERTY_TYPE__EATTRIBUTE);
		createEOperation(apogyCommonEMFUiEMFFormsFacadeEClass, APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___GET_PROPERTY_TYPE__EREFERENCE);

		eObjectEMFFormsWizardPageProviderEClass = createEClass(EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER);

		// Create enums
		propertyTypeEEnum = createEEnum(PROPERTY_TYPE);

		// Create data types
		compositeEDataType = createEDataType(COMPOSITE);
		vViewEDataType = createEDataType(VVIEW);
		vControlEDataType = createEDataType(VCONTROL);
		vContainedElementEDataType = createEDataType(VCONTAINED_ELEMENT);
		ecpswtViewEDataType = createEDataType(ECPSWT_VIEW);
		listEDataType = createEDataType(LIST);
		sortedSetEDataType = createEDataType(SORTED_SET);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogyCommonEMFUIPackage theApogyCommonEMFUIPackage = (ApogyCommonEMFUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFUIPackage.eNS_URI);

		// Create type parameters
		addETypeParameter(listEDataType, "T");
		addETypeParameter(sortedSetEDataType, "T");

		// Set bounds for type parameters

		// Add supertypes to classes
		eObjectEMFFormsWizardPageProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(apogyCommonEMFUiEMFFormsFacadeEClass, ApogyCommonEMFUiEMFFormsFacade.class, "ApogyCommonEMFUiEMFFormsFacade", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject(), null, "createEMFForms", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getComposite(), "parent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_String(), null, "createEMFForms", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getComposite(), "parent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "message", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_boolean(), null, "createEMFForms", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getComposite(), "parent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "readOnly", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_VView(), null, "createEMFForms", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getComposite(), "parent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVView(), "viewModel", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__CreateEMFForms__Composite_EObject_VView_String(), null, "createEMFForms", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getComposite(), "parent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVView(), "viewModel", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "message", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__CreateDefaultViewModel__EObject(), this.getVView(), "createDefaultViewModel", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__SortVControlAlphabetically__List(), null, "sortVControlAlphabetically", 0, 1, !IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(this.getList());
		EGenericType g2 = createEGenericType(this.getVControl());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "vcontrols", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getSortedSet());
		g2 = createEGenericType(this.getVControl());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__SortVElementAlphabetically__List(), null, "sortVElementAlphabetically", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(this.getVContainedElement());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "vContainedElements", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getSortedSet());
		g2 = createEGenericType(this.getVContainedElement());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__CreateVControl__EAttribute(), this.getVControl(), "createVControl", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEAttribute(), "attribute", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__CreateVControl__EReference(), this.getVControl(), "createVControl", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEReference(), "eReference", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__GetPropertyType__EAttribute(), this.getPropertyType(), "getPropertyType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEAttribute(), "attribute", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUiEMFFormsFacade__GetPropertyType__EReference(), this.getPropertyType(), "getPropertyType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEReference(), "eReference", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(eObjectEMFFormsWizardPageProviderEClass, EObjectEMFFormsWizardPageProvider.class, "EObjectEMFFormsWizardPageProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(propertyTypeEEnum, PropertyType.class, "PropertyType");
		addEEnumLiteral(propertyTypeEEnum, PropertyType.NONE);
		addEEnumLiteral(propertyTypeEEnum, PropertyType.READONLY);
		addEEnumLiteral(propertyTypeEEnum, PropertyType.EDITABLE);

		// Initialize data types
		initEDataType(compositeEDataType, Composite.class, "Composite", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(vViewEDataType, VView.class, "VView", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(vControlEDataType, VControl.class, "VControl", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(vContainedElementEDataType, VContainedElement.class, "VContainedElement", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(ecpswtViewEDataType, ECPSWTView.class, "ECPSWTView", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(listEDataType, List.class, "List", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(sortedSetEDataType, SortedSet.class, "SortedSet", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //ApogyCommonEMFUiEMFFormsPackageImpl

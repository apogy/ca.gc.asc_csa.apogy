/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.EObjectEMFFormsWizardPageProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.wizards.EMFFormsWizardPage;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderImpl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EObject EMF Forms Wizard Page Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EObjectEMFFormsWizardPageProviderImpl extends WizardPagesProviderImpl implements EObjectEMFFormsWizardPageProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EObjectEMFFormsWizardPageProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUiEMFFormsPackage.Literals.EOBJECT_EMF_FORMS_WIZARD_PAGE_PROVIDER;
	}

	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) {
		EList<WizardPage> list = new BasicEList<>();

		list.add(new EMFFormsWizardPage(eObject));
		
		return list;
	}
	
} //EObjectEMFFormsWizardPageProviderImpl

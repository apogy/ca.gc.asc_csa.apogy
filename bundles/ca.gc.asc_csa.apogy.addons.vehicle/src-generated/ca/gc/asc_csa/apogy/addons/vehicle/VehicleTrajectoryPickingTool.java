/**
 * Copyright (c) 2015-2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.vehicle;

import ca.gc.asc_csa.apogy.addons.TrajectoryPickingTool;

import ca.gc.asc_csa.apogy.addons.geometry.paths.WayPointPath;

import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vehicle Trajectory Picking Tool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A specialization of the TrajectoryPickingTool that is used to specify a Path for a vehicule to follow.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.vehicle.VehicleTrajectoryPickingTool#getVehiculeVariableFeatureReference <em>Vehicule Variable Feature Reference</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage#getVehicleTrajectoryPickingTool()
 * @model
 * @generated
 */
public interface VehicleTrajectoryPickingTool extends TrajectoryPickingTool {
	/**
	 * Returns the value of the '<em><b>Vehicule Variable Feature Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The Variable Feature Reference representing the vehicule that will follow the path.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Vehicule Variable Feature Reference</em>' containment reference.
	 * @see #setVehiculeVariableFeatureReference(VariableFeatureReference)
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage#getVehicleTrajectoryPickingTool_VehiculeVariableFeatureReference()
	 * @model containment="true"
	 * @generated
	 */
	VariableFeatureReference getVehiculeVariableFeatureReference();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.vehicle.VehicleTrajectoryPickingTool#getVehiculeVariableFeatureReference <em>Vehicule Variable Feature Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vehicule Variable Feature Reference</em>' containment reference.
	 * @see #getVehiculeVariableFeatureReference()
	 * @generated
	 */
	void setVehiculeVariableFeatureReference(VariableFeatureReference value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method returns the current active path in the Vehicule reference frame.
	 * @return The path to be followed, expressed in the vehicule frame.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	WayPointPath getLocalPath();

} // VehicleTrajectoryPickingTool

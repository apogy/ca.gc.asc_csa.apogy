package ca.gc.asc_csa.apogy.addons.sensors.fov;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.topology.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Of View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Base class for Field Of View
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVPackage#getFieldOfView()
 * @model abstract="true"
 * @generated
 */
public interface FieldOfView extends Node {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Whether or not the selected point visible from the FieldOfView (i.e. the selected point is not necessary inside the
	 * the volume of the FieldOfView, but its angular coordinates are.)
	 * @param point The point, expressed in the FieldOfView reference frame.
	 * @return True is the point is visible, false otherwise.
	 * <!-- end-model-doc -->
	 * @model unique="false" pointUnique="false"
	 * @generated
	 */
	boolean isPointVisible(Tuple3d point);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Whether or not the selected point in inside the volume (or area) of the FieldOfView.
	 * @param point The point, expressed in the FieldOfView reference frame.
	 * @return True is the point is inside the FieldOfView volume, false otherwise.
	 * <!-- end-model-doc -->
	 * @model unique="false" pointUnique="false"
	 * @generated
	 */
	boolean isPointInside(Tuple3d point);
} // FieldOfView

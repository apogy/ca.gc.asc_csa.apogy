package ca.gc.asc_csa.apogy.addons.sensors.fov;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.addons.sensors.fov.impl.ApogyAddonsSensorsFOVFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FOV Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Field Of View Facade.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVPackage#getApogyAddonsSensorsFOVFacade()
 * @model
 * @generated
 */
public interface ApogyAddonsSensorsFOVFacade extends EObject {
	
	public static final ApogyAddonsSensorsFOVFacade INSTANCE = ApogyAddonsSensorsFOVFacadeImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a DistanceRange from minimum and maximum distances.
	 * @param minimumDistance The minimum distance, in meters.
	 * @param maximumDistance The maximum distance, in meters.
	 * @return The DistanceRange.
	 * <!-- end-model-doc -->
	 * @model unique="false" minimumDistanceUnique="false"
	 *        minimumDistanceAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" maximumDistanceUnique="false"
	 *        maximumDistanceAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'"
	 * @generated
	 */
	DistanceRange createDistanceRange(double minimumDistance, double maximumDistance);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a copy of a DistanceRange from a DistanceRange.
	 * @param distanceRange The original DistanceRange.
	 * @return The DistanceRange.
	 * <!-- end-model-doc -->
	 * @model unique="false" distanceRangeUnique="false"
	 * @generated
	 */
	DistanceRange createDistanceRange(DistanceRange distanceRange);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates an AngularSpan from a minimum and a maximum angle.
	 * @param minimumAngle The minimum angle, in radians.
	 * @param maximumAngle The maximum angle, in radians.
	 * @return The AngularSpan.
	 * <!-- end-model-doc -->
	 * @model unique="false" minimumAngleUnique="false"
	 *        minimumAngleAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" maximumAngleUnique="false"
	 *        maximumAngleAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'"
	 * @generated
	 */
	AngularSpan createAngularSpan(double minimumAngle, double maximumAngle);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a copy of a AngularSpan from a AngularSpan.
	 * @param angularSpan The original AngularSpan.
	 * @return The AngularSpan.
	 * <!-- end-model-doc -->
	 * @model unique="false" angularSpanUnique="false"
	 * @generated
	 */
	AngularSpan createAngularSpan(AngularSpan angularSpan);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a RectangularFrustrumFieldOfView from a minimum and maximum distancesm horizontal and vertical field of view angles.
	 * @param minimumDistance The minimum distance, in meters.
	 * @param maximumDistance The maximum distance, in meters.
	 * @param horizontalFieldOfViewAngle The horizontal field of view angle, in radians.
	 * @param verticalFieldOfViewAngle The vertical field of view angle, in radians.
	 * @return The RectangularFrustrumFieldOfView.
	 * <!-- end-model-doc -->
	 * @model unique="false" minimumDistanceUnique="false"
	 *        minimumDistanceAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" maximumDistanceUnique="false"
	 *        maximumDistanceAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" horizontalFieldOfViewAngleUnique="false"
	 *        horizontalFieldOfViewAngleAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" verticalFieldOfViewAngleUnique="false"
	 *        verticalFieldOfViewAngleAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'"
	 * @generated
	 */
	RectangularFrustrumFieldOfView createRectangularFrustrumFieldOfView(double minimumDistance, double maximumDistance, double horizontalFieldOfViewAngle, double verticalFieldOfViewAngle);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a copy of a RectangularFrustrumFieldOfView from a RectangularFrustrumFieldOfView.
	 * @param rectangularFrustrumFieldOfView The original RectangularFrustrumFieldOfView.
	 * @return The RectangularFrustrumFieldOfView.
	 * <!-- end-model-doc -->
	 * @model unique="false" rectangularFrustrumFieldOfViewUnique="false"
	 * @generated
	 */
	RectangularFrustrumFieldOfView createRectangularFrustrumFieldOfView(RectangularFrustrumFieldOfView rectangularFrustrumFieldOfView);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a ConicalFieldOfView from minimum and maximum distances and an field of view angle.
	 * @param minimumDistance The minimum distance, in meters.
	 * @param maximumDistance The maximum distance, in meters.
	 * @param fieldOfViewAngle The field of view angle, in radians.
	 * @return The ConicalFieldOfView.
	 * <!-- end-model-doc -->
	 * @model unique="false" minimumDistanceUnique="false"
	 *        minimumDistanceAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" maximumDistanceUnique="false"
	 *        maximumDistanceAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" fieldOfViewAngleUnique="false"
	 *        fieldOfViewAngleAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'"
	 * @generated
	 */
	ConicalFieldOfView createConicalFieldOfView(double minimumDistance, double maximumDistance, double fieldOfViewAngle);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a copy of a ConicalFieldOfView from a ConicalFieldOfView.
	 * @param conicalFieldOfView The original ConicalFieldOfView.
	 * @return The ConicalFieldOfView.
	 * <!-- end-model-doc -->
	 * @model unique="false" conicalFieldOfViewUnique="false"
	 * @generated
	 */
	ConicalFieldOfView createConicalFieldOfView(ConicalFieldOfView conicalFieldOfView);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CircularSectorFieldOfView from minimum and maximum angles and distances.
	 * @param minimumAngle The minimum angle, in radians.
	 * @param maximumAngle The maximum angle, in radians.
	 * @param minimumDistance The minimum distance, in meters.
	 * @param maximumDistance The maximum distance, in meters.
	 * @return The CircularSectorFieldOfView.
	 * <!-- end-model-doc -->
	 * @model unique="false" minimumAngleUnique="false"
	 *        minimumAngleAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" maximumAngleUnique="false"
	 *        maximumAngleAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" minimumDistanceUnique="false"
	 *        minimumDistanceAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" maximumDistanceUnique="false"
	 *        maximumDistanceAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'"
	 * @generated
	 */
	CircularSectorFieldOfView createCircularSectorFieldOfView(double minimumAngle, double maximumAngle, double minimumDistance, double maximumDistance);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a copy of a CircularSectorFieldOfView from a CircularSectorFieldOfView.
	 * @param circularSectorFieldOfView The original CircularSectorFieldOfView.
	 * @return The CircularSectorFieldOfView.
	 * <!-- end-model-doc -->
	 * @model unique="false" circularSectorFieldOfViewUnique="false"
	 * @generated
	 */
	CircularSectorFieldOfView createCircularSectorFieldOfView(CircularSectorFieldOfView circularSectorFieldOfView);

} // ApogyAddonsSensorsFOVFacade

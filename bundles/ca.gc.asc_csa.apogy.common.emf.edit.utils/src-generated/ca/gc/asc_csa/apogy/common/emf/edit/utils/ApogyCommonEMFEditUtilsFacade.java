package ca.gc.asc_csa.apogy.common.emf.edit.utils;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.common.emf.edit.utils.impl.ApogyCommonEMFEditUtilsFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Facade for Edit Utils
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.emf.edit.utils.ApogyCommonEMFEditUtilsPackage#getApogyCommonEMFEditUtilsFacade()
 * @model
 * @generated
 */
public interface ApogyCommonEMFEditUtilsFacade extends EObject
{
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the string, as returned by the appropriate item provider, for a given object.
	 * @param object The object.
	 * @return The string, null if none is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" objectUnique="false"
	 * @generated
	 */
  String getText(Object object);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Removes the Child descriptor associated with a given feature from a list.
	 * @param newChildDescriptors The child descriptor.
	 * @param feature The feature.
	 * <!-- end-model-doc -->
	 * @model newChildDescriptorsDataType="ca.gc.asc_csa.apogy.common.emf.edit.utils.CollectionObject" newChildDescriptorsUnique="false" featureUnique="false"
	 * @generated
	 */
  void removeChildDescriptor(Collection<Object> newChildDescriptors, Object feature);

  public static ApogyCommonEMFEditUtilsFacade INSTANCE = ApogyCommonEMFEditUtilsFacadeImpl.getInstance();
} // ApogyCommonEMFEditUtilsFacade

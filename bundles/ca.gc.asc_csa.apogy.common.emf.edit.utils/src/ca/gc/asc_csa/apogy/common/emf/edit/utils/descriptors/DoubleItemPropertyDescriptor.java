package ca.gc.asc_csa.apogy.common.emf.edit.utils.descriptors;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.text.NumberFormat;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

public class DoubleItemPropertyDescriptor extends ItemPropertyDescriptor
{

	public DoubleItemPropertyDescriptor(AdapterFactory adapterFactory, ResourceLocator resourceLocator, String displayName, String description, EStructuralFeature feature, boolean isSettable, boolean multiLine, boolean sortChoices, Object staticImage, String category, String[] filterFlags)
	{
		super(adapterFactory, resourceLocator, displayName, description, feature, isSettable, multiLine, sortChoices, staticImage, category, filterFlags);
	}

	@Override
	protected Object getValue(EObject object, EStructuralFeature feature)
	{

		if (object != null)
		{
			NumberFormat formatter = NumberFormat.getInstance();
			Double value = (Double) super.getValue(object, feature);
			
			if (value != null)
			{
				return formatter.format(value);
			} else
			{
				return value;
			}
		} else
		{
			return super.getValue(object, feature);
		}
	}

}
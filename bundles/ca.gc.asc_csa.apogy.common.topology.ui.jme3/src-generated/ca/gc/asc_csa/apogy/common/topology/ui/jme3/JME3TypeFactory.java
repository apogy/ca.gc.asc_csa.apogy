package ca.gc.asc_csa.apogy.common.topology.ui.jme3;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import javax.vecmath.Color3f;

import org.eclipse.emf.ecore.EObject;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Type Factory</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Factory for JME3.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.topology.ui.jme3.ApogyCommonTopologyUIJME3Package#getJME3TypeFactory()
 * @model
 * @generated
 */
public interface JME3TypeFactory extends EObject {

	public JME3TypeFactory INSTANCE = ApogyCommonTopologyUIJME3Factory.eINSTANCE
			.createJME3TypeFactory();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a ColorRGBA from a Color3f.
	 * @param color The Color3f.
	 * @return The ColorRGBA. The alpha is set to fully opaque.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.topology.ui.jme3.ColorRGBA" unique="false" colorDataType="ca.gc.asc_csa.apogy.common.topology.ui.jme3.Color3f" colorUnique="false"
	 * @generated
	 */
	ColorRGBA createColorRGBA(Color3f color);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a JMEVector3f from a Vector3f.
	 * @param vector The Vector3f.
	 * @return The JMEVector3f.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.topology.ui.jme3.JMEVector3f" unique="false" vectorDataType="ca.gc.asc_csa.apogy.common.topology.ui.jme3.Vector3f" vectorUnique="false"
	 * @generated
	 */
	Vector3f createVector3f(javax.vecmath.Vector3f vector);

} // JME3TypeFactory

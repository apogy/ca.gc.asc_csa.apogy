/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.ui.jme3.internal;

import com.jme3.input.InputManager;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.renderer.Camera;

import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Application;

public abstract class MouseClickListener implements ActionListener 
{
	public static final String LEFT_MOUSE_CLICK_EVENT = "LeftMouseButtonClickEvent";
	public static final String MIDDLE_MOUSE_CLICK_EVENT = "MiddleMouseButtonClickEvent";
	public static final String RIGHT_MOUSE_CLICK_EVENT = "RightMouseButtonClickEvent";
	
	private Camera camera = null;	
	private JME3Application jme3Application;
	private InputManager inputManager;

	private boolean mouseInputRegistered = false;

	private boolean enabled = false;
	
	public MouseClickListener(Camera camera, JME3Application jme3Application, InputManager inputManager)
	{
		this.camera = camera;
		this.jme3Application = jme3Application;
		this.inputManager = inputManager;	
	}
	
	public void setEnabled(boolean enable)
	{				
		if(enable)
		{
			if(!mouseInputRegistered)
			{
				registerMouseInput();
			}
		}
		else
		{
			if(mouseInputRegistered)
			{
				unregisterMouseInput();
			}
		}
		this.enabled = enable;
	}
	
	public boolean isEnabled()
	{
		return this.enabled;
	}
	
	@Override
	public void onAction(String name, boolean keyPressed, float tpf) 
	{	
		if(name.equals(LEFT_MOUSE_CLICK_EVENT) && keyPressed)
		{			
			mouseClicked(MouseInput.BUTTON_LEFT);
		}
		
		if(name.equals(MIDDLE_MOUSE_CLICK_EVENT) && keyPressed)
		{			
			mouseClicked(MouseInput.BUTTON_MIDDLE);
		}
				
		if(name.equals(RIGHT_MOUSE_CLICK_EVENT) && keyPressed)
		{			
			mouseClicked(MouseInput.BUTTON_RIGHT);
		}
	}

	protected abstract void mouseClicked(int mouseButton);
	
	private void registerMouseInput()
	{		
		if(!inputManager.hasMapping(LEFT_MOUSE_CLICK_EVENT))
		{
			inputManager.addMapping(LEFT_MOUSE_CLICK_EVENT, new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
			inputManager.addListener(this, new String[]{LEFT_MOUSE_CLICK_EVENT});
		}
		
		if(!inputManager.hasMapping(MIDDLE_MOUSE_CLICK_EVENT))
		{
			inputManager.addMapping(MIDDLE_MOUSE_CLICK_EVENT, new MouseButtonTrigger(MouseInput.BUTTON_MIDDLE));
			inputManager.addListener(this, new String[]{MIDDLE_MOUSE_CLICK_EVENT});
		}		
		
		if(!inputManager.hasMapping(RIGHT_MOUSE_CLICK_EVENT))
		{
			inputManager.addMapping(RIGHT_MOUSE_CLICK_EVENT, new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
			inputManager.addListener(this, new String[]{RIGHT_MOUSE_CLICK_EVENT});
		}
		
		mouseInputRegistered = true;
	}
	
	private void unregisterMouseInput()
	{		
		if(inputManager.hasMapping(LEFT_MOUSE_CLICK_EVENT))
		{
			inputManager.deleteMapping(LEFT_MOUSE_CLICK_EVENT);
		}
				
		if(inputManager.hasMapping(MIDDLE_MOUSE_CLICK_EVENT))
		{
			inputManager.deleteMapping(MIDDLE_MOUSE_CLICK_EVENT);
		}
		
		if(inputManager.hasMapping(RIGHT_MOUSE_CLICK_EVENT))
		{
			inputManager.deleteMapping(RIGHT_MOUSE_CLICK_EVENT);
		}
		
		mouseInputRegistered = false;
	}
}

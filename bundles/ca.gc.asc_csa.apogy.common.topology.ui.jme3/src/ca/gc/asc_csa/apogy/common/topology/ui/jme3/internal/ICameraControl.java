package ca.gc.asc_csa.apogy.common.topology.ui.jme3.internal;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.control.Control;

import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;

public interface ICameraControl extends Control 
{
	/**
     * Enables or disables the ICameraControl
     * @param enabled true to enable, false to disable.
     */
	public void setEnabled(boolean enabled); 
    
    /**
     * Return the enabled/disabled state of the ICameraControl.
     * @return true if the ICameraControl is enabled, false otherwise.
     */
	public boolean isEnabled();
	
	public Vector3f getCurrentLocation();
	
	public void setCurrentLocation(Vector3f newLocation);
	
	public Quaternion getCurrentOrientation();
	
	public void setCurrentOrientation(Quaternion newRotation);
	
	public void levelPose();
	
	public void setHighSpeedMotionEnabled(boolean highSpeedMotionEnabled);
	
	public void dispose();
	
	public void setTranslationEnabled(boolean translationEnabled);
	
	public void setRotationEnabled(boolean rotationEnabled);
	
	public void attachViewPoint(AbstractViewPoint viewPoint);
}

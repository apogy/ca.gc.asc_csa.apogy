package ca.gc.asc_csa.apogy.common.topology.ui.preferences;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;

public class TopologyViewerPreferences extends AbstractPreferenceInitializer {

	public TopologyViewerPreferences() {
		Activator.getDefault().getPreferenceStore().setDefault(
				PreferenceConstants.P_ANTI_ALIASING, false);
		
		Activator.getDefault().getPreferenceStore().setDefault(
				PreferenceConstants.P_SHOW_3D_AXIS, false);
	}

	@Override
	public void initializeDefaultPreferences() {

	}

}

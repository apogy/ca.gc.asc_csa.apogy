/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.impl;

import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;
import ca.gc.asc_csa.apogy.common.topology.AbstractViewPointReference;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract View Point Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.impl.AbstractViewPointReferenceImpl#getAbstractViewPoint <em>Abstract View Point</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AbstractViewPointReferenceImpl extends MinimalEObjectImpl.Container implements AbstractViewPointReference {
	/**
	 * The cached value of the '{@link #getAbstractViewPoint() <em>Abstract View Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstractViewPoint()
	 * @generated
	 * @ordered
	 */
	protected AbstractViewPoint abstractViewPoint;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractViewPointReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyPackage.Literals.ABSTRACT_VIEW_POINT_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractViewPoint getAbstractViewPoint() {
		if (abstractViewPoint != null && abstractViewPoint.eIsProxy()) {
			InternalEObject oldAbstractViewPoint = (InternalEObject)abstractViewPoint;
			abstractViewPoint = (AbstractViewPoint)eResolveProxy(oldAbstractViewPoint);
			if (abstractViewPoint != oldAbstractViewPoint) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCommonTopologyPackage.ABSTRACT_VIEW_POINT_REFERENCE__ABSTRACT_VIEW_POINT, oldAbstractViewPoint, abstractViewPoint));
			}
		}
		return abstractViewPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractViewPoint basicGetAbstractViewPoint() {
		return abstractViewPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractViewPoint(AbstractViewPoint newAbstractViewPoint) {
		AbstractViewPoint oldAbstractViewPoint = abstractViewPoint;
		abstractViewPoint = newAbstractViewPoint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyPackage.ABSTRACT_VIEW_POINT_REFERENCE__ABSTRACT_VIEW_POINT, oldAbstractViewPoint, abstractViewPoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.ABSTRACT_VIEW_POINT_REFERENCE__ABSTRACT_VIEW_POINT:
				if (resolve) return getAbstractViewPoint();
				return basicGetAbstractViewPoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.ABSTRACT_VIEW_POINT_REFERENCE__ABSTRACT_VIEW_POINT:
				setAbstractViewPoint((AbstractViewPoint)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.ABSTRACT_VIEW_POINT_REFERENCE__ABSTRACT_VIEW_POINT:
				setAbstractViewPoint((AbstractViewPoint)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.ABSTRACT_VIEW_POINT_REFERENCE__ABSTRACT_VIEW_POINT:
				return abstractViewPoint != null;
		}
		return super.eIsSet(featureID);
	}

} //AbstractViewPointReferenceImpl

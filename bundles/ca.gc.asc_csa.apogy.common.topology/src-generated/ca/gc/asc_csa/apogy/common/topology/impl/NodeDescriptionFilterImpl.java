/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.impl;

import org.eclipse.emf.ecore.EClass;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.NodeDescriptionFilter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node Description Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NodeDescriptionFilterImpl extends RegexNodeFilterImpl implements NodeDescriptionFilter 
{	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NodeDescriptionFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyPackage.Literals.NODE_DESCRIPTION_FILTER;
	}

	@Override
	public boolean matches(Node node) 
	{
		return matchesPattern(node.getDescription());
	}	
} //NodeDescriptionFilterImpl

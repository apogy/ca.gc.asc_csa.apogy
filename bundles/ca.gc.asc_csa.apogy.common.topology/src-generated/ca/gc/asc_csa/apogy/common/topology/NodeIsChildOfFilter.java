/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node Is Child Of Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A Node filter that filter a Node on whether it is under the parent specified parent node.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.NodeIsChildOfFilter#getParentNode <em>Parent Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.NodeIsChildOfFilter#isParentNodeIncluded <em>Parent Node Included</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage#getNodeIsChildOfFilter()
 * @model
 * @generated
 */
public interface NodeIsChildOfFilter extends NodeFilter {
	/**
	 * Returns the value of the '<em><b>Parent Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The parent node.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parent Node</em>' reference.
	 * @see #setParentNode(Node)
	 * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage#getNodeIsChildOfFilter_ParentNode()
	 * @model
	 * @generated
	 */
	Node getParentNode();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.topology.NodeIsChildOfFilter#getParentNode <em>Parent Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Node</em>' reference.
	 * @see #getParentNode()
	 * @generated
	 */
	void setParentNode(Node value);

	/**
	 * Returns the value of the '<em><b>Parent Node Included</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Whether or not to include the parent Node itself passes the filter.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parent Node Included</em>' attribute.
	 * @see #setParentNodeIncluded(boolean)
	 * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage#getNodeIsChildOfFilter_ParentNodeIncluded()
	 * @model default="true" unique="false"
	 * @generated
	 */
	boolean isParentNodeIncluded();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.topology.NodeIsChildOfFilter#isParentNodeIncluded <em>Parent Node Included</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Node Included</em>' attribute.
	 * @see #isParentNodeIncluded()
	 * @generated
	 */
	void setParentNodeIncluded(boolean value);

} // NodeIsChildOfFilter

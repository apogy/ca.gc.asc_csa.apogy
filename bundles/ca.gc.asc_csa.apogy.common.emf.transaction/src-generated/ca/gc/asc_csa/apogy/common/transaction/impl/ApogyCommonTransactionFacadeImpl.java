/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *  
 *  Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *      Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *      Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *      Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.transaction.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.AbstractOverrideableCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.Activator;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Facade</b></em>'. <!-- end-user-doc -->
 *
 * @generated
 */
public class ApogyCommonTransactionFacadeImpl extends MinimalEObjectImpl.Container
		implements ApogyCommonTransactionFacade {

	/**
	 * Facade Singleton.
	 */
	private static ApogyCommonTransactionFacade instance = null;

	/**
	 * Returns the facade singleton.
	 * 
	 * @return Reference to the facade.
	 */
	public static ApogyCommonTransactionFacade getInstance() {
		if (instance == null) {
			instance = new ApogyCommonTransactionFacadeImpl();
		}

		return instance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ApogyCommonTransactionFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTransactionPackage.Literals.APOGY_COMMON_TRANSACTION_FACADE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public TransactionalEditingDomain getDefaultEditingDomain() {
		return TransactionalEditingDomain.Registry.INSTANCE
				.getEditingDomain("ca.gc.asc_csa.apogy.common.transaction.defaultApogyEditingDomain");

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void addInTempTransactionalEditingDomain(EObject eObject) {
		if (TransactionUtil.getEditingDomain(eObject) == null) {
			final EObject rootObject = EcoreUtil.getRootContainer(eObject);
			final ResourceSet resourceSet = new ResourceSetImpl();
			Resource resource = resourceSet.createResource(URI.createURI("TEMP_RESOURCE"));
			if (resource != null) {
				resource.getContents().add(rootObject);
			}
			final TransactionalEditingDomain domain = TransactionalEditingDomain.Factory.INSTANCE
					.createEditingDomain(resourceSet);
			resourceSet.eAdapters().add(new AdapterFactoryEditingDomain.EditingDomainProvider(domain));
		} else {
			String message = this.getClass().getSimpleName() + eObject
					+ " is already in an EditingDomain.\nRemove it from it's EditingDomain using ApogyCommonTransactionFacade.removeFromEditingDomain(EObject eObject) ";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void removeFromEditingDomain(EObject eObject) {
		if (eObject != null && eObject.eResource() != null && eObject.eResource().getResourceSet() != null
				&& TransactionUtil.getEditingDomain(eObject) != null) {
			eObject.eResource().getResourceSet().getResources().remove(eObject.eResource());
			TransactionUtil.disconnectFromEditingDomain(eObject.eResource());
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public int areEditingDomainsValid(EObject owner, EStructuralFeature feature, Object value, boolean fix) {
		TransactionalEditingDomain ownerDomain = TransactionUtil.getEditingDomain(owner);
		TransactionalEditingDomain childDomain = TransactionUtil.getEditingDomain(value);
		boolean containment = true;

		if (feature instanceof EReference) {
			containment = ((EReference) feature).isContainment();
		} else if (feature instanceof EAttribute) {
			if (ownerDomain != null) {
				return EXECUTE_COMMAND_ON_OWNER_DOMAIN;
			} else {
				return EXECUTE_EMF_METHOD;
			}
		}

		if (containment) {
			if (value == null) {
				if (ownerDomain != null) {
					return EXECUTE_COMMAND_ON_OWNER_DOMAIN;
				} else {
					return EXECUTE_EMF_METHOD;
				}
			}

			if (ownerDomain == null) {
				if (childDomain == null) {
					if (fix) {
						return EXECUTE_EMF_METHOD;
					} else {
						String message = " ApogyCommonEmfTransactionFacade.areEditingDomainsValid: Owner and value domains are null.";
						Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
						return DONT_EXECUTE;
					}
				} else {
					if (fix) {
						removeFromEditingDomain((EObject) value);
						return EXECUTE_EMF_METHOD;
					} else {
						String message = " ApogyCommonEmfTransactionFacade.areEditingDomainsValid: Owner domain is null and value domain is not.";
						Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
						return DONT_EXECUTE;
					}
				}
			} else {
				if (childDomain == null || ownerDomain == childDomain) {
					return EXECUTE_COMMAND_ON_OWNER_DOMAIN;
				} else {
					if (fix) {
						removeFromEditingDomain((EObject) value);
						return EXECUTE_COMMAND_ON_OWNER_DOMAIN;
					} else {
						String message = " ApogyCommonEmfTransactionFacade.areEditingDomainsValid: Owner domain and value domain are different.";
						Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
						return DONT_EXECUTE;
					}
				}
			}
		} else {
			if (ownerDomain != null) {
				return EXECUTE_COMMAND_ON_OWNER_DOMAIN;
			} else {
				return EXECUTE_EMF_METHOD;
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void basicSet(EObject owner, EStructuralFeature feature, Object value, boolean fix) {
		switch (areEditingDomainsValid(owner, feature, value, fix)) {
		case EXECUTE_COMMAND_ON_OWNER_DOMAIN:
			executeCommand(new SetCommand(getTransactionalEditingDomain(owner), owner, feature, value));
			break;
		case EXECUTE_EMF_METHOD:
			owner.eSet(feature, value);
			break;
		case DONT_EXECUTE:
			String message = " ApogyCommonEmfTransactionFacade.basicSet: Command was not executed.";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
			break;
		default:
			break;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void basicSet(EObject owner, EStructuralFeature feature, Object value) {
		basicSet(owner, feature, value, false);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	@SuppressWarnings({ "unchecked", "rawtypes"})
	public void basicAdd(EObject owner, EStructuralFeature feature, Object value, boolean fix) {
		switch (areEditingDomainsValid(owner, feature, value, fix)) {
		case EXECUTE_COMMAND_ON_OWNER_DOMAIN:
			executeCommand(new AddCommand(getTransactionalEditingDomain(owner), owner, feature, value));
			break;
		case EXECUTE_EMF_METHOD:
			EList list = (EList) owner.eGet(feature);
			list.add(value);
			break;
		case DONT_EXECUTE:
			String message = " ApogyCommonEmfTransactionFacade.basicAdd: Command was not executed.";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
			break;
		default:
			break;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void basicAdd(EObject owner, EStructuralFeature feature, Object value) {
		basicAdd(owner, feature, value, false);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void basicAdd(EObject owner, EStructuralFeature feature, Collection<?> collection, boolean fix) {
		int execute = EXECUTE_COMMAND_ON_OWNER_DOMAIN;

		for (Object object : collection) {
			execute = areEditingDomainsValid(owner, feature, object, fix);
			if (execute != EXECUTE_COMMAND_ON_OWNER_DOMAIN) {
				break;
			}
		}

		String message;
		switch (execute) {
		case EXECUTE_COMMAND_ON_OWNER_DOMAIN:
			executeCommand(new AddCommand(getTransactionalEditingDomain(owner), owner, feature, collection));
			break;
		case EXECUTE_EMF_METHOD:
			message = " ApogyCommonEmfTransactionFacade.basicAdd: Command was not executed, error in editing domains.";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
			break;
		case DONT_EXECUTE:
			message = " ApogyCommonEmfTransactionFacade.basicAdd: Command was not executed.";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
			break;
		default:
			break;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void basicAdd(EObject owner, EStructuralFeature feature, Collection<?> collection) {
		basicAdd(owner, feature, collection, false);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void basicRemove(EObject owner, EStructuralFeature feature, Object value, boolean fix) {
		switch (areEditingDomainsValid(owner, feature, value, fix)) {
		case EXECUTE_COMMAND_ON_OWNER_DOMAIN:
			executeCommand(new RemoveCommand(getTransactionalEditingDomain(owner), owner, feature, value));
			break;
		case EXECUTE_EMF_METHOD:
			EList<?> list = (EList<?>) owner.eGet(feature);
			list.remove(value);
			break;
		case DONT_EXECUTE:
			String message = " ApogyCommonEmfTransactionFacade.basicRemove: Command was not executed.";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
			break;
		default:
			break;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void basicRemove(EObject owner, EStructuralFeature feature, Object value) {
		EditingDomain domain = AdapterFactoryEditingDomain.getEditingDomainFor(owner);
		if (domain instanceof TransactionalEditingDomain) {
			RemoveCommand command = new RemoveCommand(domain, owner, feature, value);
			domain.getCommandStack().execute(command);
		} else {
			String message = this.getClass().getSimpleName() + " .basicAdd(): " + " Editing domain of " + owner
					+ " is not Transactional ";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void basicRemove(EObject owner, EStructuralFeature feature, Collection<?> collection, boolean fix) {
		int execute = EXECUTE_COMMAND_ON_OWNER_DOMAIN;

		for (Object object : collection) {
			execute = areEditingDomainsValid(owner, feature, object, fix);
			if (execute != EXECUTE_COMMAND_ON_OWNER_DOMAIN) {
				break;
			}
		}

		String message;
		switch (execute) {
		case EXECUTE_COMMAND_ON_OWNER_DOMAIN:
			executeCommand(new RemoveCommand(getTransactionalEditingDomain(owner), owner, feature, collection));
			break;
		case EXECUTE_EMF_METHOD:
			message = " ApogyCommonEmfTransactionFacade.basicRemove: Command was not executed, error in editing domains.";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
			break;
		case DONT_EXECUTE:
			message = " ApogyCommonEmfTransactionFacade.basicRemove: Command was not executed.";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
			break;
		default:
			break;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void basicRemove(EObject owner, EStructuralFeature feature, Collection<?> collection) {
		basicRemove(owner, feature, collection, false);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void basicDelete(EObject owner, EStructuralFeature feature, Object value, boolean fix) {
		if (feature.isMany()) {
			basicRemove(owner, feature, value, fix);
		} else {
			basicSet(owner, feature, null, fix);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void basicDelete(EObject owner, EStructuralFeature feature, Object value) {
		basicDelete(owner, feature, value, false);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void executeCommand(AbstractOverrideableCommand command) {
		if (command.getDomain() instanceof TransactionalEditingDomain) {
			command.getDomain().getCommandStack().execute(command);
		} else {
			String message = this.getClass().getSimpleName()
					+ " Command can not be executed. Is the owner in a TransactionalEditingDomain? ";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public TransactionalEditingDomain getTransactionalEditingDomain(EObject eObject) {
		EditingDomain domain = AdapterFactoryEditingDomain.getEditingDomainFor(eObject);
		if (domain instanceof TransactionalEditingDomain) {
			return (TransactionalEditingDomain) domain;
		} else {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___GET_DEFAULT_EDITING_DOMAIN:
				return getDefaultEditingDomain();
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___ADD_IN_TEMP_TRANSACTIONAL_EDITING_DOMAIN__EOBJECT:
				addInTempTransactionalEditingDomain((EObject)arguments.get(0));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___REMOVE_FROM_EDITING_DOMAIN__EOBJECT:
				removeFromEditingDomain((EObject)arguments.get(0));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___ARE_EDITING_DOMAINS_VALID__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN:
				return areEditingDomainsValid((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), arguments.get(2), (Boolean)arguments.get(3));
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_SET__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN:
				basicSet((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), arguments.get(2), (Boolean)arguments.get(3));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_SET__EOBJECT_ESTRUCTURALFEATURE_OBJECT:
				basicSet((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), arguments.get(2));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN:
				basicAdd((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), arguments.get(2), (Boolean)arguments.get(3));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_OBJECT:
				basicAdd((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), arguments.get(2));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_COLLECTION_BOOLEAN:
				basicAdd((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), (Collection<?>)arguments.get(2), (Boolean)arguments.get(3));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_COLLECTION:
				basicAdd((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), (Collection<?>)arguments.get(2));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN:
				basicRemove((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), arguments.get(2), (Boolean)arguments.get(3));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_OBJECT:
				basicRemove((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), arguments.get(2));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_COLLECTION_BOOLEAN:
				basicRemove((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), (Collection<?>)arguments.get(2), (Boolean)arguments.get(3));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_COLLECTION:
				basicRemove((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), (Collection<?>)arguments.get(2));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_DELETE__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN:
				basicDelete((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), arguments.get(2), (Boolean)arguments.get(3));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___BASIC_DELETE__EOBJECT_ESTRUCTURALFEATURE_OBJECT:
				basicDelete((EObject)arguments.get(0), (EStructuralFeature)arguments.get(1), arguments.get(2));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___EXECUTE_COMMAND__ABSTRACTOVERRIDEABLECOMMAND:
				executeCommand((AbstractOverrideableCommand)arguments.get(0));
				return null;
			case ApogyCommonTransactionPackage.APOGY_COMMON_TRANSACTION_FACADE___GET_TRANSACTIONAL_EDITING_DOMAIN__EOBJECT:
				return getTransactionalEditingDomain((EObject)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} // ApogyCommonTransactionFacadeImpl

/**
 * Copyright (c) 2015-2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.mobility.pathplanners.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.addons.mobility.pathplanners.ApogyAddonsMobilityPathplannersFactory;
import ca.gc.asc_csa.apogy.addons.mobility.pathplanners.CircularExclusionZone;
import ca.gc.asc_csa.apogy.addons.mobility.pathplanners.ui.ApogyAddonsMobilityPathplannersUIPackage;
import ca.gc.asc_csa.apogy.addons.mobility.pathplanners.ui.CircularExclusionZoneWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.mobility.pathplanners.ui.wizards.CircularExclusionZoneWizardPage;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.topology.ui.impl.NodeWizardPagesProviderImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Circular Exclusion Zone Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CircularExclusionZoneWizardPagesProviderImpl extends NodeWizardPagesProviderImpl implements CircularExclusionZoneWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CircularExclusionZoneWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsMobilityPathplannersUIPackage.Literals.CIRCULAR_EXCLUSION_ZONE_WIZARD_PAGES_PROVIDER;
	}
	
	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		CircularExclusionZone zone = ApogyAddonsMobilityPathplannersFactory.eINSTANCE.createCircularExclusionZone();
		zone.setRadius(1.0f);		
		return zone;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	

		list.add(new CircularExclusionZoneWizardPage((CircularExclusionZone) eObject));
		
		return list;
	}

} //CircularExclusionZoneWizardPagesProviderImpl

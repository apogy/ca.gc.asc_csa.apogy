/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.mobility.pathplanners.ui.wizards;

import java.text.DecimalFormat;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.mobility.pathplanners.ApogyAddonsMobilityPathplannersPackage;
import ca.gc.asc_csa.apogy.addons.mobility.pathplanners.CircularExclusionZone;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;

public class CircularExclusionZoneWizardPage extends WizardPage 
{
	public static String NO_VALUE_AVAILABLE_STRING = "N/A";
	private int LABEL_WIDTH = 200; 
	private int VALUE_WIDTH = 100; 
	private int BUTTON_WIDTH = 50; 	

	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.mobility.pathplanners.ui.wizards.CircularExclusionZoneWizardPage";
	
	private CircularExclusionZone circularExclusionZone;
	
	private TypedElementSimpleUnitsComposite radiusComposite;
	
	private Adapter adapter;
	
	public CircularExclusionZoneWizardPage(CircularExclusionZone circularExclusionZone) 
	{
		super(WIZARD_PAGE_ID);
		this.circularExclusionZone = circularExclusionZone;
				
		setTitle("Circular Exclusion Zone");
		setDescription("Select the Circular Exclusion Zone radius.");	
	}
	
	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(2, false));
		setControl(top);

		radiusComposite = new TypedElementSimpleUnitsComposite(top, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.0");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Range :";
			}
		};
		radiusComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		radiusComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsMobilityPathplannersPackage.Literals.CIRCULAR_EXCLUSION_ZONE__RADIUS), circularExclusionZone);

		if(circularExclusionZone != null)  circularExclusionZone.eAdapters().add(getAdapter());		
		top.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent arg0) {				
				if(circularExclusionZone != null) circularExclusionZone.eAdapters().remove(getAdapter());
			}
		});
		
		validate();
	}

	protected void validate()
	{
		setErrorMessage(null);
		
		if(circularExclusionZone.getRadius() <= 0)
		{
			setErrorMessage("The specified radius must be greater than zero !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	protected Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof CircularExclusionZone)
					{
						validate();
					}
				}
			};
		}
		return adapter;
	}
}

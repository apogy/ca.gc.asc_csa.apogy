/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *  
 *  Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *      Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *      Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *      Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.invocator.impl;

import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.TriggeredBasedProgram;
import ca.gc.asc_csa.apogy.core.invocator.TriggeredBasedProgramsGroup;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Triggered Based Programs Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TriggeredBasedProgramsGroupImpl extends ProgramsGroupImpl<TriggeredBasedProgram> implements TriggeredBasedProgramsGroup {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TriggeredBasedProgramsGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreInvocatorPackage.Literals.TRIGGERED_BASED_PROGRAMS_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * This is specialized for the more specific element type known in this context.
	 * @generated
	 */
	@Override
	public EList<TriggeredBasedProgram> getPrograms() {
		if (programs == null) {
			programs = new EObjectContainmentWithInverseEList<TriggeredBasedProgram>(TriggeredBasedProgram.class, this, ApogyCoreInvocatorPackage.TRIGGERED_BASED_PROGRAMS_GROUP__PROGRAMS, ApogyCoreInvocatorPackage.PROGRAM__PROGRAMS_GROUP) { private static final long serialVersionUID = 1L; @Override public Class<?> getInverseFeatureClass() { return Program.class; } };
		}
		return programs;
	}

} //TriggeredBasedProgramsGroupImpl

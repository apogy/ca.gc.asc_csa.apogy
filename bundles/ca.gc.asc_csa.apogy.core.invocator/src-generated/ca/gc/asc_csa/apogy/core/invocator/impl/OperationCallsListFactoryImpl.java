/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.invocator.impl;

import org.eclipse.emf.ecore.EClass;

import ca.gc.asc_csa.apogy.core.invocator.AbstractProgramRuntime;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFactory;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallsListFactory;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallsListProgramRuntime;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ProgramSettings;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation Calls List Factory</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OperationCallsListFactoryImpl extends ProgramFactoryImpl implements OperationCallsListFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public OperationCallsListFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreInvocatorPackage.Literals.OPERATION_CALLS_LIST_FACTORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public Program createProgram() {
		return ApogyCoreInvocatorFactory.eINSTANCE.createOperationCallsList();
	}

	@Override
	public AbstractProgramRuntime createProgramRuntime(Program program, ProgramSettings settings) 
	{
		OperationCallsListProgramRuntime runtime =  ApogyCoreInvocatorFactory.eINSTANCE.createOperationCallsListProgramRuntime();
		runtime.setProgram(program);
		return runtime;
	}
	
	@Override
	public void applySettings(Program program, ProgramSettings settings) 
	{
		// Nothing to do for now.
	}
} //OperationCallsListFactoryImpl

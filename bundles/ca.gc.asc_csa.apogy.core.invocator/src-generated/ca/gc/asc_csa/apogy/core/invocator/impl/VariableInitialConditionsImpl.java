/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *  
 *  Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *      Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *      Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *      Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.invocator.impl;

import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.InitialConditions;
import ca.gc.asc_csa.apogy.core.invocator.Variable;
import ca.gc.asc_csa.apogy.core.invocator.VariableInitialConditions;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable Initial Conditions</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.impl.VariableInitialConditionsImpl#getVariable <em>Variable</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.impl.VariableInitialConditionsImpl#getInitialConditions <em>Initial Conditions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VariableInitialConditionsImpl extends AbstractInitialConditionsImpl implements VariableInitialConditions {
	/**
	 * The cached value of the '{@link #getVariable() <em>Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariable()
	 * @generated
	 * @ordered
	 */
	protected Variable variable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableInitialConditionsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreInvocatorPackage.Literals.VARIABLE_INITIAL_CONDITIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable getVariable() {
		if (variable != null && variable.eIsProxy()) {
			InternalEObject oldVariable = (InternalEObject)variable;
			variable = (Variable)eResolveProxy(oldVariable);
			if (variable != oldVariable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__VARIABLE, oldVariable, variable));
			}
		}
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable basicGetVariable() {
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariable(Variable newVariable) {
		Variable oldVariable = variable;
		variable = newVariable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__VARIABLE, oldVariable, variable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InitialConditions getInitialConditions() {
		if (eContainerFeatureID() != ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS) return null;
		return (InitialConditions)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InitialConditions basicGetInitialConditions() {
		if (eContainerFeatureID() != ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS) return null;
		return (InitialConditions)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitialConditions(InitialConditions newInitialConditions, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newInitialConditions, ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialConditions(InitialConditions newInitialConditions) {
		if (newInitialConditions != eInternalContainer() || (eContainerFeatureID() != ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS && newInitialConditions != null)) {
			if (EcoreUtil.isAncestor(this, newInitialConditions))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newInitialConditions != null)
				msgs = ((InternalEObject)newInitialConditions).eInverseAdd(this, ApogyCoreInvocatorPackage.INITIAL_CONDITIONS__VARIABLE_INITIAL_CONDITIONS, InitialConditions.class, msgs);
			msgs = basicSetInitialConditions(newInitialConditions, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS, newInitialConditions, newInitialConditions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetInitialConditions((InitialConditions)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS:
				return basicSetInitialConditions(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS:
				return eInternalContainer().eInverseRemove(this, ApogyCoreInvocatorPackage.INITIAL_CONDITIONS__VARIABLE_INITIAL_CONDITIONS, InitialConditions.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__VARIABLE:
				if (resolve) return getVariable();
				return basicGetVariable();
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS:
				if (resolve) return getInitialConditions();
				return basicGetInitialConditions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__VARIABLE:
				setVariable((Variable)newValue);
				return;
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS:
				setInitialConditions((InitialConditions)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__VARIABLE:
				setVariable((Variable)null);
				return;
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS:
				setInitialConditions((InitialConditions)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__VARIABLE:
				return variable != null;
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS__INITIAL_CONDITIONS:
				return basicGetInitialConditions() != null;
		}
		return super.eIsSet(featureID);
	}

} //VariableInitialConditionsImpl

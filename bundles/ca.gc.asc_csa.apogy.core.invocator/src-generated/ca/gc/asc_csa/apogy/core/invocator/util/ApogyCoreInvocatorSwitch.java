package ca.gc.asc_csa.apogy.core.invocator.util;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/


import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import ca.gc.asc_csa.apogy.common.emf.BrowseableTimeSource;
import ca.gc.asc_csa.apogy.common.emf.CollectionTimedTimeSource;
import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Disposable;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.Startable;
import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.emf.Timed;
import ca.gc.asc_csa.apogy.core.invocator.*;
import ca.gc.asc_csa.apogy.core.invocator.AbstractInitializationData;
import ca.gc.asc_csa.apogy.core.invocator.AbstractProgramRuntime;
import ca.gc.asc_csa.apogy.core.invocator.AbstractResult;
import ca.gc.asc_csa.apogy.core.invocator.AbstractResultValue;
import ca.gc.asc_csa.apogy.core.invocator.AbstractToolsListContainer;
import ca.gc.asc_csa.apogy.core.invocator.AbstractType;
import ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.Argument;
import ca.gc.asc_csa.apogy.core.invocator.ArgumentsList;
import ca.gc.asc_csa.apogy.core.invocator.AttributeResultValue;
import ca.gc.asc_csa.apogy.core.invocator.AttributeValue;
import ca.gc.asc_csa.apogy.core.invocator.BooleanEDataTypeArgument;
import ca.gc.asc_csa.apogy.core.invocator.Context;
import ca.gc.asc_csa.apogy.core.invocator.ContextsList;
import ca.gc.asc_csa.apogy.core.invocator.DataProductsList;
import ca.gc.asc_csa.apogy.core.invocator.DataProductsListsContainer;
import ca.gc.asc_csa.apogy.core.invocator.EClassArgument;
import ca.gc.asc_csa.apogy.core.invocator.EDataTypeArgument;
import ca.gc.asc_csa.apogy.core.invocator.EEnumArgument;
import ca.gc.asc_csa.apogy.core.invocator.Environment;
import ca.gc.asc_csa.apogy.core.invocator.ExceptionContainer;
import ca.gc.asc_csa.apogy.core.invocator.InitializationData;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.LocalTypesList;
import ca.gc.asc_csa.apogy.core.invocator.NumericEDataTypeArgument;
import ca.gc.asc_csa.apogy.core.invocator.OperationCall;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallContainer;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallResult;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallResultsList;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallResultsListTimeSource;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallsList;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallsListFactory;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallsListProgramRuntime;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ProgramFactoriesRegistry;
import ca.gc.asc_csa.apogy.core.invocator.ProgramFactory;
import ca.gc.asc_csa.apogy.core.invocator.ProgramRuntimesList;
import ca.gc.asc_csa.apogy.core.invocator.ProgramSettings;
import ca.gc.asc_csa.apogy.core.invocator.ProgramsGroup;
import ca.gc.asc_csa.apogy.core.invocator.ProgramsList;
import ca.gc.asc_csa.apogy.core.invocator.ReferenceResultValue;
import ca.gc.asc_csa.apogy.core.invocator.ResultsList;
import ca.gc.asc_csa.apogy.core.invocator.ScriptBasedProgram;
import ca.gc.asc_csa.apogy.core.invocator.SpecificProgramSettings;
import ca.gc.asc_csa.apogy.core.invocator.StringEDataTypeArgument;
import ca.gc.asc_csa.apogy.core.invocator.ToolsList;
import ca.gc.asc_csa.apogy.core.invocator.TriggeredBasedProgram;
import ca.gc.asc_csa.apogy.core.invocator.Type;
import ca.gc.asc_csa.apogy.core.invocator.TypeApiAdapter;
import ca.gc.asc_csa.apogy.core.invocator.TypeMember;
import ca.gc.asc_csa.apogy.core.invocator.TypeMemberImplementation;
import ca.gc.asc_csa.apogy.core.invocator.TypeMemberReference;
import ca.gc.asc_csa.apogy.core.invocator.TypeMemberReferenceListElement;
import ca.gc.asc_csa.apogy.core.invocator.TypeMemberReferenceTreeElement;
import ca.gc.asc_csa.apogy.core.invocator.TypesRegistry;
import ca.gc.asc_csa.apogy.core.invocator.Value;
import ca.gc.asc_csa.apogy.core.invocator.ValuesList;
import ca.gc.asc_csa.apogy.core.invocator.Variable;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;
import ca.gc.asc_csa.apogy.core.invocator.VariableImplementation;
import ca.gc.asc_csa.apogy.core.invocator.VariableImplementationsList;
import ca.gc.asc_csa.apogy.core.invocator.VariablesList;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage
 * @generated
 */
public class ApogyCoreInvocatorSwitch<T1> extends Switch<T1>
{
  /**
	 * The cached model package
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  protected static ApogyCoreInvocatorPackage modelPackage;

  /**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public ApogyCoreInvocatorSwitch()
  {
		if (modelPackage == null) {
			modelPackage = ApogyCoreInvocatorPackage.eINSTANCE;
		}
	}

  /**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
		return ePackage == modelPackage;
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  @Override
  protected T1 doSwitch(int classifierID, EObject theEObject)
  {
		switch (classifierID) {
			case ApogyCoreInvocatorPackage.APOGY_CORE_INVOCATOR_FACADE: {
				ApogyCoreInvocatorFacade apogyCoreInvocatorFacade = (ApogyCoreInvocatorFacade)theEObject;
				T1 result = caseApogyCoreInvocatorFacade(apogyCoreInvocatorFacade);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.INVOCATOR_SESSION: {
				InvocatorSession invocatorSession = (InvocatorSession)theEObject;
				T1 result = caseInvocatorSession(invocatorSession);
				if (result == null) result = caseNamed(invocatorSession);
				if (result == null) result = caseDescribed(invocatorSession);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ENVIRONMENT: {
				Environment environment = (Environment)theEObject;
				T1 result = caseEnvironment(environment);
				if (result == null) result = caseNamed(environment);
				if (result == null) result = caseDescribed(environment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.DATA_PRODUCTS_LISTS_CONTAINER: {
				DataProductsListsContainer dataProductsListsContainer = (DataProductsListsContainer)theEObject;
				T1 result = caseDataProductsListsContainer(dataProductsListsContainer);
				if (result == null) result = caseNamed(dataProductsListsContainer);
				if (result == null) result = caseDescribed(dataProductsListsContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.DATA_PRODUCTS_LIST: {
				DataProductsList dataProductsList = (DataProductsList)theEObject;
				T1 result = caseDataProductsList(dataProductsList);
				if (result == null) result = caseNamed(dataProductsList);
				if (result == null) result = caseDescribed(dataProductsList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TOOLS_LIST: {
				ToolsList toolsList = (ToolsList)theEObject;
				T1 result = caseToolsList(toolsList);
				if (result == null) result = caseNamed(toolsList);
				if (result == null) result = caseDescribed(toolsList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ABSTRACT_TOOLS_LIST_CONTAINER: {
				AbstractToolsListContainer abstractToolsListContainer = (AbstractToolsListContainer)theEObject;
				T1 result = caseAbstractToolsListContainer(abstractToolsListContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.LOCAL_TYPES_LIST: {
				LocalTypesList localTypesList = (LocalTypesList)theEObject;
				T1 result = caseLocalTypesList(localTypesList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TYPES_REGISTRY: {
				TypesRegistry typesRegistry = (TypesRegistry)theEObject;
				T1 result = caseTypesRegistry(typesRegistry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ABSTRACT_TYPE: {
				AbstractType abstractType = (AbstractType)theEObject;
				T1 result = caseAbstractType(abstractType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TYPE: {
				Type type = (Type)theEObject;
				T1 result = caseType(type);
				if (result == null) result = caseNamed(type);
				if (result == null) result = caseAbstractType(type);
				if (result == null) result = caseDescribed(type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TYPE_API_ADAPTER: {
				TypeApiAdapter typeApiAdapter = (TypeApiAdapter)theEObject;
				T1 result = caseTypeApiAdapter(typeApiAdapter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TYPE_MEMBER: {
				TypeMember typeMember = (TypeMember)theEObject;
				T1 result = caseTypeMember(typeMember);
				if (result == null) result = caseNamed(typeMember);
				if (result == null) result = caseDescribed(typeMember);
				if (result == null) result = caseAbstractType(typeMember);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TYPE_MEMBER_REFERENCE: {
				TypeMemberReference typeMemberReference = (TypeMemberReference)theEObject;
				T1 result = caseTypeMemberReference(typeMemberReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TYPE_MEMBER_REFERENCE_LIST_ELEMENT: {
				TypeMemberReferenceListElement typeMemberReferenceListElement = (TypeMemberReferenceListElement)theEObject;
				T1 result = caseTypeMemberReferenceListElement(typeMemberReferenceListElement);
				if (result == null) result = caseTypeMemberReference(typeMemberReferenceListElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TYPE_MEMBER_REFERENCE_TREE_ELEMENT: {
				TypeMemberReferenceTreeElement typeMemberReferenceTreeElement = (TypeMemberReferenceTreeElement)theEObject;
				T1 result = caseTypeMemberReferenceTreeElement(typeMemberReferenceTreeElement);
				if (result == null) result = caseTypeMemberReference(typeMemberReferenceTreeElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.VARIABLES_LIST: {
				VariablesList variablesList = (VariablesList)theEObject;
				T1 result = caseVariablesList(variablesList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.VARIABLE: {
				Variable variable = (Variable)theEObject;
				T1 result = caseVariable(variable);
				if (result == null) result = caseNamed(variable);
				if (result == null) result = caseDescribed(variable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.CONTEXTS_LIST: {
				ContextsList contextsList = (ContextsList)theEObject;
				T1 result = caseContextsList(contextsList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.CONTEXT: {
				Context context = (Context)theEObject;
				T1 result = caseContext(context);
				if (result == null) result = caseNamed(context);
				if (result == null) result = caseDescribed(context);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.VARIABLE_IMPLEMENTATIONS_LIST: {
				VariableImplementationsList variableImplementationsList = (VariableImplementationsList)theEObject;
				T1 result = caseVariableImplementationsList(variableImplementationsList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ABSTRACT_TYPE_IMPLEMENTATION: {
				AbstractTypeImplementation abstractTypeImplementation = (AbstractTypeImplementation)theEObject;
				T1 result = caseAbstractTypeImplementation(abstractTypeImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.VARIABLE_IMPLEMENTATION: {
				VariableImplementation variableImplementation = (VariableImplementation)theEObject;
				T1 result = caseVariableImplementation(variableImplementation);
				if (result == null) result = caseAbstractTypeImplementation(variableImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TYPE_MEMBER_IMPLEMENTATION: {
				TypeMemberImplementation typeMemberImplementation = (TypeMemberImplementation)theEObject;
				T1 result = caseTypeMemberImplementation(typeMemberImplementation);
				if (result == null) result = caseAbstractTypeImplementation(typeMemberImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIALIZATION_DATA: {
				AbstractInitializationData abstractInitializationData = (AbstractInitializationData)theEObject;
				T1 result = caseAbstractInitializationData(abstractInitializationData);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.INITIALIZATION_DATA: {
				InitializationData initializationData = (InitializationData)theEObject;
				T1 result = caseInitializationData(initializationData);
				if (result == null) result = caseAbstractInitializationData(initializationData);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS: {
				AbstractInitialConditions abstractInitialConditions = (AbstractInitialConditions)theEObject;
				T1 result = caseAbstractInitialConditions(abstractInitialConditions);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TYPE_MEMBER_INITIAL_CONDITIONS: {
				TypeMemberInitialConditions typeMemberInitialConditions = (TypeMemberInitialConditions)theEObject;
				T1 result = caseTypeMemberInitialConditions(typeMemberInitialConditions);
				if (result == null) result = caseAbstractInitialConditions(typeMemberInitialConditions);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.VARIABLE_INITIAL_CONDITIONS: {
				VariableInitialConditions variableInitialConditions = (VariableInitialConditions)theEObject;
				T1 result = caseVariableInitialConditions(variableInitialConditions);
				if (result == null) result = caseAbstractInitialConditions(variableInitialConditions);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.INITIAL_CONDITIONS: {
				InitialConditions initialConditions = (InitialConditions)theEObject;
				T1 result = caseInitialConditions(initialConditions);
				if (result == null) result = caseNamed(initialConditions);
				if (result == null) result = caseDescribed(initialConditions);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.INITIAL_CONDITIONS_LIST: {
				InitialConditionsList initialConditionsList = (InitialConditionsList)theEObject;
				T1 result = caseInitialConditionsList(initialConditionsList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.VALUES_LIST: {
				ValuesList valuesList = (ValuesList)theEObject;
				T1 result = caseValuesList(valuesList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.VALUE: {
				Value value = (Value)theEObject;
				T1 result = caseValue(value);
				if (result == null) result = caseNamed(value);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.PROGRAMS_LIST: {
				ProgramsList programsList = (ProgramsList)theEObject;
				T1 result = caseProgramsList(programsList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.PROGRAMS_GROUP: {
				ProgramsGroup<?> programsGroup = (ProgramsGroup<?>)theEObject;
				T1 result = caseProgramsGroup(programsGroup);
				if (result == null) result = caseNamed(programsGroup);
				if (result == null) result = caseDescribed(programsGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAMS_GROUP: {
				ScriptBasedProgramsGroup scriptBasedProgramsGroup = (ScriptBasedProgramsGroup)theEObject;
				T1 result = caseScriptBasedProgramsGroup(scriptBasedProgramsGroup);
				if (result == null) result = caseProgramsGroup(scriptBasedProgramsGroup);
				if (result == null) result = caseNamed(scriptBasedProgramsGroup);
				if (result == null) result = caseDescribed(scriptBasedProgramsGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TRIGGERED_BASED_PROGRAMS_GROUP: {
				TriggeredBasedProgramsGroup triggeredBasedProgramsGroup = (TriggeredBasedProgramsGroup)theEObject;
				T1 result = caseTriggeredBasedProgramsGroup(triggeredBasedProgramsGroup);
				if (result == null) result = caseProgramsGroup(triggeredBasedProgramsGroup);
				if (result == null) result = caseNamed(triggeredBasedProgramsGroup);
				if (result == null) result = caseDescribed(triggeredBasedProgramsGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.PROGRAM: {
				Program program = (Program)theEObject;
				T1 result = caseProgram(program);
				if (result == null) result = caseNamed(program);
				if (result == null) result = caseDescribed(program);
				if (result == null) result = caseStartable(program);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.SCRIPT_BASED_PROGRAM: {
				ScriptBasedProgram scriptBasedProgram = (ScriptBasedProgram)theEObject;
				T1 result = caseScriptBasedProgram(scriptBasedProgram);
				if (result == null) result = caseProgram(scriptBasedProgram);
				if (result == null) result = caseNamed(scriptBasedProgram);
				if (result == null) result = caseDescribed(scriptBasedProgram);
				if (result == null) result = caseStartable(scriptBasedProgram);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.TRIGGERED_BASED_PROGRAM: {
				TriggeredBasedProgram triggeredBasedProgram = (TriggeredBasedProgram)theEObject;
				T1 result = caseTriggeredBasedProgram(triggeredBasedProgram);
				if (result == null) result = caseProgram(triggeredBasedProgram);
				if (result == null) result = caseNamed(triggeredBasedProgram);
				if (result == null) result = caseDescribed(triggeredBasedProgram);
				if (result == null) result = caseStartable(triggeredBasedProgram);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.OPERATION_CALL_CONTAINER: {
				OperationCallContainer operationCallContainer = (OperationCallContainer)theEObject;
				T1 result = caseOperationCallContainer(operationCallContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.PROGRAM_SETTINGS: {
				ProgramSettings programSettings = (ProgramSettings)theEObject;
				T1 result = caseProgramSettings(programSettings);
				if (result == null) result = caseNamed(programSettings);
				if (result == null) result = caseDescribed(programSettings);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.SPECIFIC_PROGRAM_SETTINGS: {
				SpecificProgramSettings specificProgramSettings = (SpecificProgramSettings)theEObject;
				T1 result = caseSpecificProgramSettings(specificProgramSettings);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.PROGRAM_FACTORY: {
				ProgramFactory programFactory = (ProgramFactory)theEObject;
				T1 result = caseProgramFactory(programFactory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.PROGRAM_FACTORIES_REGISTRY: {
				ProgramFactoriesRegistry programFactoriesRegistry = (ProgramFactoriesRegistry)theEObject;
				T1 result = caseProgramFactoriesRegistry(programFactoriesRegistry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST: {
				OperationCallsList operationCallsList = (OperationCallsList)theEObject;
				T1 result = caseOperationCallsList(operationCallsList);
				if (result == null) result = caseScriptBasedProgram(operationCallsList);
				if (result == null) result = caseOperationCallContainer(operationCallsList);
				if (result == null) result = caseProgram(operationCallsList);
				if (result == null) result = caseNamed(operationCallsList);
				if (result == null) result = caseDescribed(operationCallsList);
				if (result == null) result = caseStartable(operationCallsList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_FACTORY: {
				OperationCallsListFactory operationCallsListFactory = (OperationCallsListFactory)theEObject;
				T1 result = caseOperationCallsListFactory(operationCallsListFactory);
				if (result == null) result = caseProgramFactory(operationCallsListFactory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.VARIABLE_FEATURE_REFERENCE: {
				VariableFeatureReference variableFeatureReference = (VariableFeatureReference)theEObject;
				T1 result = caseVariableFeatureReference(variableFeatureReference);
				if (result == null) result = caseNamed(variableFeatureReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.OPERATION_CALL: {
				OperationCall operationCall = (OperationCall)theEObject;
				T1 result = caseOperationCall(operationCall);
				if (result == null) result = caseVariableFeatureReference(operationCall);
				if (result == null) result = caseDescribed(operationCall);
				if (result == null) result = caseNamed(operationCall);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ARGUMENTS_LIST: {
				ArgumentsList argumentsList = (ArgumentsList)theEObject;
				T1 result = caseArgumentsList(argumentsList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ARGUMENT: {
				Argument argument = (Argument)theEObject;
				T1 result = caseArgument(argument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.EDATA_TYPE_ARGUMENT: {
				EDataTypeArgument eDataTypeArgument = (EDataTypeArgument)theEObject;
				T1 result = caseEDataTypeArgument(eDataTypeArgument);
				if (result == null) result = caseArgument(eDataTypeArgument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.BOOLEAN_EDATA_TYPE_ARGUMENT: {
				BooleanEDataTypeArgument booleanEDataTypeArgument = (BooleanEDataTypeArgument)theEObject;
				T1 result = caseBooleanEDataTypeArgument(booleanEDataTypeArgument);
				if (result == null) result = caseEDataTypeArgument(booleanEDataTypeArgument);
				if (result == null) result = caseArgument(booleanEDataTypeArgument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.NUMERIC_EDATA_TYPE_ARGUMENT: {
				NumericEDataTypeArgument numericEDataTypeArgument = (NumericEDataTypeArgument)theEObject;
				T1 result = caseNumericEDataTypeArgument(numericEDataTypeArgument);
				if (result == null) result = caseEDataTypeArgument(numericEDataTypeArgument);
				if (result == null) result = caseArgument(numericEDataTypeArgument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.STRING_EDATA_TYPE_ARGUMENT: {
				StringEDataTypeArgument stringEDataTypeArgument = (StringEDataTypeArgument)theEObject;
				T1 result = caseStringEDataTypeArgument(stringEDataTypeArgument);
				if (result == null) result = caseEDataTypeArgument(stringEDataTypeArgument);
				if (result == null) result = caseArgument(stringEDataTypeArgument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.EENUM_ARGUMENT: {
				EEnumArgument eEnumArgument = (EEnumArgument)theEObject;
				T1 result = caseEEnumArgument(eEnumArgument);
				if (result == null) result = caseArgument(eEnumArgument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ECLASS_ARGUMENT: {
				EClassArgument eClassArgument = (EClassArgument)theEObject;
				T1 result = caseEClassArgument(eClassArgument);
				if (result == null) result = caseArgument(eClassArgument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.PROGRAM_RUNTIMES_LIST: {
				ProgramRuntimesList programRuntimesList = (ProgramRuntimesList)theEObject;
				T1 result = caseProgramRuntimesList(programRuntimesList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ABSTRACT_PROGRAM_RUNTIME: {
				AbstractProgramRuntime abstractProgramRuntime = (AbstractProgramRuntime)theEObject;
				T1 result = caseAbstractProgramRuntime(abstractProgramRuntime);
				if (result == null) result = caseNamed(abstractProgramRuntime);
				if (result == null) result = caseDescribed(abstractProgramRuntime);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_PROGRAM_RUNTIME: {
				OperationCallsListProgramRuntime operationCallsListProgramRuntime = (OperationCallsListProgramRuntime)theEObject;
				T1 result = caseOperationCallsListProgramRuntime(operationCallsListProgramRuntime);
				if (result == null) result = caseAbstractProgramRuntime(operationCallsListProgramRuntime);
				if (result == null) result = caseNamed(operationCallsListProgramRuntime);
				if (result == null) result = caseDescribed(operationCallsListProgramRuntime);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.RESULTS_LIST: {
				ResultsList resultsList = (ResultsList)theEObject;
				T1 result = caseResultsList(resultsList);
				if (result == null) result = caseNamed(resultsList);
				if (result == null) result = caseDescribed(resultsList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.OPERATION_CALL_RESULTS_LIST: {
				OperationCallResultsList operationCallResultsList = (OperationCallResultsList)theEObject;
				T1 result = caseOperationCallResultsList(operationCallResultsList);
				if (result == null) result = caseResultsList(operationCallResultsList);
				if (result == null) result = caseNamed(operationCallResultsList);
				if (result == null) result = caseDescribed(operationCallResultsList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ABSTRACT_RESULT: {
				AbstractResult abstractResult = (AbstractResult)theEObject;
				T1 result = caseAbstractResult(abstractResult);
				if (result == null) result = caseTimed(abstractResult);
				if (result == null) result = caseDescribed(abstractResult);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ABSTRACT_RESULT_VALUE: {
				AbstractResultValue abstractResultValue = (AbstractResultValue)theEObject;
				T1 result = caseAbstractResultValue(abstractResultValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.OPERATION_CALL_RESULT: {
				OperationCallResult operationCallResult = (OperationCallResult)theEObject;
				T1 result = caseOperationCallResult(operationCallResult);
				if (result == null) result = caseAbstractResult(operationCallResult);
				if (result == null) result = caseOperationCallContainer(operationCallResult);
				if (result == null) result = caseTimed(operationCallResult);
				if (result == null) result = caseDescribed(operationCallResult);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.EXCEPTION_CONTAINER: {
				ExceptionContainer exceptionContainer = (ExceptionContainer)theEObject;
				T1 result = caseExceptionContainer(exceptionContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ATTRIBUTE_RESULT_VALUE: {
				AttributeResultValue attributeResultValue = (AttributeResultValue)theEObject;
				T1 result = caseAttributeResultValue(attributeResultValue);
				if (result == null) result = caseAbstractResultValue(attributeResultValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.ATTRIBUTE_VALUE: {
				AttributeValue attributeValue = (AttributeValue)theEObject;
				T1 result = caseAttributeValue(attributeValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.REFERENCE_RESULT_VALUE: {
				ReferenceResultValue referenceResultValue = (ReferenceResultValue)theEObject;
				T1 result = caseReferenceResultValue(referenceResultValue);
				if (result == null) result = caseAbstractResultValue(referenceResultValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreInvocatorPackage.OPERATION_CALL_RESULTS_LIST_TIME_SOURCE: {
				OperationCallResultsListTimeSource operationCallResultsListTimeSource = (OperationCallResultsListTimeSource)theEObject;
				T1 result = caseOperationCallResultsListTimeSource(operationCallResultsListTimeSource);
				if (result == null) result = caseCollectionTimedTimeSource(operationCallResultsListTimeSource);
				if (result == null) result = caseBrowseableTimeSource(operationCallResultsListTimeSource);
				if (result == null) result = caseTimeSource(operationCallResultsListTimeSource);
				if (result == null) result = caseNamed(operationCallResultsListTimeSource);
				if (result == null) result = caseDescribed(operationCallResultsListTimeSource);
				if (result == null) result = caseTimed(operationCallResultsListTimeSource);
				if (result == null) result = caseDisposable(operationCallResultsListTimeSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseApogyCoreInvocatorFacade(ApogyCoreInvocatorFacade object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Invocator Session</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Invocator Session</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseInvocatorSession(InvocatorSession object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Environment</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Environment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseEnvironment(Environment object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Data Products Lists Container</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Products Lists Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseDataProductsListsContainer(DataProductsListsContainer object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Data Products List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Products List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseDataProductsList(DataProductsList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Tools List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tools List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseToolsList(ToolsList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Tools List Container</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Tools List Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseAbstractToolsListContainer(AbstractToolsListContainer object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Local Types List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Local Types List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseLocalTypesList(LocalTypesList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Types Registry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Types Registry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTypesRegistry(TypesRegistry object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Type</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseAbstractType(AbstractType object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseType(Type object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Type Api Adapter</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Api Adapter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseTypeApiAdapter(TypeApiAdapter object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Type Member</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Member</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseTypeMember(TypeMember object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Type Member Reference</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Member Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseTypeMemberReference(TypeMemberReference object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Type Member Reference List Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Member Reference List Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseTypeMemberReferenceListElement(TypeMemberReferenceListElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Type Member Reference Tree Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Member Reference Tree Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseTypeMemberReferenceTreeElement(TypeMemberReferenceTreeElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Variables List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variables List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseVariablesList(VariablesList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseVariable(Variable object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Contexts List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contexts List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseContextsList(ContextsList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Context</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Context</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseContext(Context object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Implementations List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Implementations List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseVariableImplementationsList(VariableImplementationsList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Initialization Data</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Initialization Data</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseAbstractInitializationData(AbstractInitializationData object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Initialization Data</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Initialization Data</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseInitializationData(InitializationData object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Initial Conditions</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Initial Conditions</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAbstractInitialConditions(AbstractInitialConditions object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Member Initial Conditions</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Member Initial Conditions</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTypeMemberInitialConditions(TypeMemberInitialConditions object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Initial Conditions</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Initial Conditions</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseVariableInitialConditions(VariableInitialConditions object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Initial Conditions</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Initial Conditions</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseInitialConditions(InitialConditions object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Initial Conditions List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Initial Conditions List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseInitialConditionsList(InitialConditionsList object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Type Implementation</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Type Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseAbstractTypeImplementation(AbstractTypeImplementation object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Implementation</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseVariableImplementation(VariableImplementation object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Type Member Implementation</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Member Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseTypeMemberImplementation(TypeMemberImplementation object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Values List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Values List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseValuesList(ValuesList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Value</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseValue(Value object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Programs List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Programs List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseProgramsList(ProgramsList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Programs Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Programs Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T extends Program> T1 caseProgramsGroup(ProgramsGroup<T> object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Script Based Programs Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Script Based Programs Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseScriptBasedProgramsGroup(ScriptBasedProgramsGroup object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Triggered Based Programs Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Triggered Based Programs Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTriggeredBasedProgramsGroup(TriggeredBasedProgramsGroup object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Program</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Program</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseProgram(Program object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Script Based Program</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Script Based Program</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseScriptBasedProgram(ScriptBasedProgram object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Triggered Based Program</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Triggered Based Program</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTriggeredBasedProgram(TriggeredBasedProgram object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Call Container</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Call Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseOperationCallContainer(OperationCallContainer object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Program Settings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Program Settings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseProgramSettings(ProgramSettings object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Specific Program Settings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Specific Program Settings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseSpecificProgramSettings(SpecificProgramSettings object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Program Factory</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Program Factory</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseProgramFactory(ProgramFactory object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Program Factories Registry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Program Factories Registry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseProgramFactoriesRegistry(ProgramFactoriesRegistry object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Calls List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Calls List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseOperationCallsList(OperationCallsList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Calls List Factory</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Calls List Factory</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseOperationCallsListFactory(OperationCallsListFactory object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Feature Reference</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Feature Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseVariableFeatureReference(VariableFeatureReference object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Call</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseOperationCall(OperationCall object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Arguments List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Arguments List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseArgumentsList(ArgumentsList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Argument</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseArgument(Argument object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>EData Type Argument</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EData Type Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseEDataTypeArgument(EDataTypeArgument object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean EData Type Argument</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean EData Type Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseBooleanEDataTypeArgument(BooleanEDataTypeArgument object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Numeric EData Type Argument</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Numeric EData Type Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNumericEDataTypeArgument(NumericEDataTypeArgument object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>String EData Type Argument</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String EData Type Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStringEDataTypeArgument(StringEDataTypeArgument object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EEnum Argument</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EEnum Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseEEnumArgument(EEnumArgument object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>EClass Argument</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EClass Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseEClassArgument(EClassArgument object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Program Runtimes List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Program Runtimes List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseProgramRuntimesList(ProgramRuntimesList object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Program Runtime</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Program Runtime</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAbstractProgramRuntime(AbstractProgramRuntime object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Calls List Program Runtime</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Calls List Program Runtime</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseOperationCallsListProgramRuntime(OperationCallsListProgramRuntime object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Results List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Results List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseResultsList(ResultsList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Call Results List</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Call Results List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseOperationCallResultsList(OperationCallResultsList object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Result</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Result</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseAbstractResult(AbstractResult object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Result Value</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Result Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseAbstractResultValue(AbstractResultValue object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Call Result</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Call Result</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseOperationCallResult(OperationCallResult object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Exception Container</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exception Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseExceptionContainer(ExceptionContainer object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Result Value</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Result Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseAttributeResultValue(AttributeResultValue object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Value</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseAttributeValue(AttributeValue object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Result Value</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Result Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T1 caseReferenceResultValue(ReferenceResultValue object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Call Results List Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Call Results List Time Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseOperationCallResultsListTimeSource(OperationCallResultsListTimeSource object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNamed(Named object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Described</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Described</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDescribed(Described object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Startable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Startable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStartable(Startable object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTimed(Timed object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Disposable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Disposable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDisposable(Disposable object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTimeSource(TimeSource object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Browseable Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Browseable Time Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseBrowseableTimeSource(BrowseableTimeSource object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Collection Timed Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Collection Timed Time Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCollectionTimedTimeSource(CollectionTimedTimeSource object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
  @Override
  public T1 defaultCase(EObject object)
  {
		return null;
	}

} //ApogyCoreInvocatorSwitch

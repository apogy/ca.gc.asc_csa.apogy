/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.invocator;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program Factory</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Defines a Factory to create programs and their Runtime.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage#getProgramFactory()
 * @model abstract="true"
 * @generated
 */
public interface ProgramFactory extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Creates the program
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 * @generated
	 */
	Program createProgram();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Applies the settings to the program
	 * <!-- end-model-doc -->
	 * @model programUnique="false" settingsUnique="false"
	 * @generated
	 */
	void applySettings(Program program, ProgramSettings settings);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Creates the AbstractProgramRuntime required to execute a specified Program with the specified settings.
	 * @param program The specified Program.
	 * @param settings The program settings to use.
	 * <!-- end-model-doc -->
	 * @model unique="false" programUnique="false" settingsUnique="false"
	 * @generated
	 */
	AbstractProgramRuntime createProgramRuntime(Program program, ProgramSettings settings);
} // ProgramFactory

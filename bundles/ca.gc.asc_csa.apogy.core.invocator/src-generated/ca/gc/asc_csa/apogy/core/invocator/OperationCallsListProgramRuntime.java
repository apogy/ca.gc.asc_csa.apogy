/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.invocator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Calls List Program Runtime</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class provides the {@link AbstractProgramRuntime} for the {@link OperationCallResultsList}.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.OperationCallsListProgramRuntime#getIndexLastExecuted <em>Index Last Executed</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.OperationCallsListProgramRuntime#getIndexCurrentlyExecuted <em>Index Currently Executed</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage#getOperationCallsListProgramRuntime()
 * @model
 * @generated
 */
public interface OperationCallsListProgramRuntime extends AbstractProgramRuntime {

	/**
	 * Returns the value of the '<em><b>Index Last Executed</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Last Executed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Index of the last executed operation call in the OperationCallsList.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Index Last Executed</em>' attribute.
	 * @see #setIndexLastExecuted(int)
	 * @see ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage#getOperationCallsListProgramRuntime_IndexLastExecuted()
	 * @model default="-1" unique="false"
	 * @generated
	 */
	int getIndexLastExecuted();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.invocator.OperationCallsListProgramRuntime#getIndexLastExecuted <em>Index Last Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index Last Executed</em>' attribute.
	 * @see #getIndexLastExecuted()
	 * @generated
	 */
	void setIndexLastExecuted(int value);

	/**
	 * Returns the value of the '<em><b>Index Currently Executed</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Currently Executed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Index of the currently executed operation call in the OperationCallsList.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Index Currently Executed</em>' attribute.
	 * @see #setIndexCurrentlyExecuted(int)
	 * @see ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage#getOperationCallsListProgramRuntime_IndexCurrentlyExecuted()
	 * @model default="-1" unique="false"
	 * @generated
	 */
	int getIndexCurrentlyExecuted();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.invocator.OperationCallsListProgramRuntime#getIndexCurrentlyExecuted <em>Index Currently Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index Currently Executed</em>' attribute.
	 * @see #getIndexCurrentlyExecuted()
	 * @generated
	 */
	void setIndexCurrentlyExecuted(int value);
} // OperationCallsListProgramRuntime

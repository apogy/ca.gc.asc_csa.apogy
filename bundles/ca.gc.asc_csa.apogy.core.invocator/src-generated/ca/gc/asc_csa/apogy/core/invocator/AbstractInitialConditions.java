/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *  
 *  Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *      Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *      Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *      Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.invocator;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Initial Conditions</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 *  -------------------------------------------------------------------------
 * Initial Conditions.
 * -------------------------------------------------------------------------
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.AbstractInitialConditions#getAbstractInitializationData <em>Abstract Initialization Data</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.AbstractInitialConditions#getTypeMembersInitialConditions <em>Type Members Initial Conditions</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage#getAbstractInitialConditions()
 * @model abstract="true"
 * @generated
 */
public interface AbstractInitialConditions extends EObject {
	/**
	 * Returns the value of the '<em><b>Abstract Initialization Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The initialization data associated with these initial conditions.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Abstract Initialization Data</em>' containment reference.
	 * @see #setAbstractInitializationData(AbstractInitializationData)
	 * @see ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage#getAbstractInitialConditions_AbstractInitializationData()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AbstractInitializationData getAbstractInitializationData();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.invocator.AbstractInitialConditions#getAbstractInitializationData <em>Abstract Initialization Data</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Initialization Data</em>' containment reference.
	 * @see #getAbstractInitializationData()
	 * @generated
	 */
	void setAbstractInitializationData(AbstractInitializationData value);

	/**
	 * Returns the value of the '<em><b>Type Members Initial Conditions</b></em>' containment reference list.
	 * The list contents are of type {@link ca.gc.asc_csa.apogy.core.invocator.TypeMemberInitialConditions}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Initial conditions associated with type members.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Type Members Initial Conditions</em>' containment reference list.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage#getAbstractInitialConditions_TypeMembersInitialConditions()
	 * @model containment="true"
	 * @generated
	 */
	EList<TypeMemberInitialConditions> getTypeMembersInitialConditions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the TypeMemberInitialConditions associated with a specified TypeMember.
	 * @param typeMember The specified TypeMember.
	 * @return The TypeMemberInitialConditions associated with the specified TypeMember, null if none is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" typeMemberUnique="false"
	 * @generated
	 */
	TypeMemberInitialConditions getTypeMemberInitialConditionsFor(TypeMember typeMember);

} // AbstractInitialConditions

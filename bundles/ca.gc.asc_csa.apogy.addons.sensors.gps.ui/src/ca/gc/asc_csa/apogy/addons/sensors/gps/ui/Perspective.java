/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque,
 * 	 	 Segastien Gemme
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.gps.ui;

import ca.gc.asc_csa.apogy.addons.sensors.gps.ui.views.GPSView;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class Perspective implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {

		layout.setEditorAreaVisible(false);
		layout.addStandaloneView(GPSView.ID, false, IPageLayout.LEFT, 1.0f,
				layout.getEditorArea());
	}
}

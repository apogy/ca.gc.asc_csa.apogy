package ca.gc.asc_csa.apogy.common.topology.addons.dynamics;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Dynamics Engine</b></em>'.
 * <!-- end-user-doc --> *
 * <!-- begin-model-doc -->
 * Defines a generic Dynamics Engine.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.topology.addons.dynamics.ApogyCommonTopologyAddonsDynamicsPackage#getAbstractDynamicsEngine()
 * @model abstract="true"
 * @generated
 */
public interface AbstractDynamicsEngine extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * Method that starts the simulation.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void startSimulation();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * Method that stops the simulation.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void stopSimulation();

} // AbstractDynamicsEngine

package ca.gc.asc_csa.apogy.addons.sensors.range;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.addons.sensors.range.impl.ApogyAddonsSensorsRangeFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sensors Range Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Facade for Range sensors.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.range.ApogyAddonsSensorsRangePackage#getApogyAddonsSensorsRangeFacade()
 * @model
 * @generated
 */
public interface ApogyAddonsSensorsRangeFacade extends EObject {
	
	/**
	 * Singleton of ApogyAddonsSensorsRangeFacade.
	 */
	public static final ApogyAddonsSensorsRangeFacade INSTANCE = ApogyAddonsSensorsRangeFacadeImpl.getInstance(); 
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a RasterScanSettings using minimum and mxaimum distances, horizontal and vertical view angles and resolutions.
	 * @param minimumDistance The minimum range of the scan, in meters.
	 * @param maximumDistance The maximum range of the scan, in meters.
	 * @param horizontalFieldOfViewAngle The horizontal field of view angle, in radians.
	 * @param verticalFieldOfViewAngle The vertical field of view angle, in radians.
	 * @param horizontalResolution The number of scan line in the horizontal direction.
	 * @param verticalResolution The number of scan line in the vertical direction.
	 * @return The RasterScanSettings.
	 * <!-- end-model-doc -->
	 * @model unique="false" minimumDistanceUnique="false"
	 *        minimumDistanceAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" maximumDistanceUnique="false"
	 *        maximumDistanceAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" horizontalFieldOfViewAngleUnique="false"
	 *        horizontalFieldOfViewAngleAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" verticalFieldOfViewAngleUnique="false"
	 *        verticalFieldOfViewAngleAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" horizontalResolutionUnique="false" verticalResolutionUnique="false"
	 * @generated
	 */
	RasterScanSettings createRasterScanSettings(double minimumDistance, double maximumDistance, double horizontalFieldOfViewAngle, double verticalFieldOfViewAngle, int horizontalResolution, int verticalResolution);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create a copy of RasterScanSettings from an original.
	 * @param  rasterScanSettings The original RasterScanSettings.
	 * @return The copy.
	 * <!-- end-model-doc -->
	 * @model unique="false" rasterScanSettingsUnique="false"
	 * @generated
	 */
	RasterScanSettings createRasterScanSettings(RasterScanSettings rasterScanSettings);

} // ApogyAddonsSensorsRangeFacade

package ca.gc.asc_csa.apogy.addons.sensors.gps;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque, 
 *     Sebastien Gemme 
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.addons.sensors.gps.impl.ApogyAddonsSensorsGPSFacadeImpl;
import ca.gc.asc_csa.apogy.common.geometry.data3d.PositionMarker;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>GPS
 * Facade</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Facade for GPS.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.gps.ApogyAddonsSensorsGPSPackage#getApogyAddonsSensorsGPSFacade()
 * @model
 * @generated
 */
public interface ApogyAddonsSensorsGPSFacade extends EObject {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> *
	 * Creates a GPSSensor that returns its position relative to the provided
	 * location.
	 * 
	 * @param originLatitude
	 *            The latitude of the reference frame, in degrees
	 * @param originLongitude
	 *            The longitude of the reference frame, in degree.
	 * @param neAngle
	 *            The North-Esat angle, in degrees.
	 * @return The GPSPoseSensor. <!-- end-model-doc -->
	 * @model unique="false" originLatitudeUnique="false"
	 *        originLatitudeAnnotation="http://www.eclipse.org/emf/2002/GenModel
	 *        apogy_units='deg'" originLongitudeUnique="false"
	 *        originLongitudeAnnotation="http://www.eclipse.org/emf/2002/GenModel
	 *        apogy_units='deg'" neAngleUnique="false"
	 *        neAngleAnnotation="http://www.eclipse.org/emf/2002/GenModel
	 *        apogy_units='deg'"
	 * @generated
	 */
	GPSPoseSensor createGPSPoseSensor(double originLatitude, double originLongitude, double neAngle);

	public static final ApogyAddonsSensorsGPSFacade INSTANCE = ApogyAddonsSensorsGPSFacadeImpl.getInstance();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> *
	 * Creates a MarkedGPS from a PositionMarker.
	 * 
	 * @param marker
	 *            The PositionMarker.
	 * @return The MarkedGPS. <!-- end-model-doc -->
	 * @model unique="false" markerUnique="false"
	 * @generated
	 */
	MarkedGPS createMarkedGPS(PositionMarker marker);

} // ApogyAddonsSensorsGPSFacade

package ca.gc.asc_csa.apogy.addons.sensors.gps;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque, 
 *     Sebastien Gemme 
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>GPS
 * Repository</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Defines a repository of GPS.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.gps.GPSRepository#getGpsDevices <em>Gps Devices</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.gps.ApogyAddonsSensorsGPSPackage#getGPSRepository()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface GPSRepository extends EObject {
	/**
	 * Returns the value of the '<em><b>Gps Devices</b></em>' reference list.
	 * The list contents are of type
	 * {@link ca.gc.asc_csa.apogy.addons.sensors.gps.GPS}. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Gps Devices</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc --> <!-- begin-model-doc --> * The list of GPS. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Gps Devices</em>' reference list.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.gps.ApogyAddonsSensorsGPSPackage#getGPSRepository_GpsDevices()
	 * @model
	 * @generated
	 */
	EList<GPS> getGpsDevices();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> *
	 * Automatically detect GPS to populate the GPSRepository. <!--
	 * end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void scanForDevices();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> *
	 * Get a GPS by its ID.
	 * 
	 * @param gpsId
	 *            The ID.
	 * @return The GPS, null if none is found. <!-- end-model-doc -->
	 * @model unique="false" gpsIdUnique="false"
	 * @generated
	 */
	GPS getGPSById(String gpsId);

} // GPSRepository

package ca.gc.asc_csa.apogy.rcp.dialogs;

import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.workbench.IResourceUtilities;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import ca.gc.asc_csa.apogy.common.images.ui.composites.ImageDisplayComposite;
import ca.gc.asc_csa.apogy.common.ui.composites.URLSelectionComposite;

public class IconURLDialog extends WizardPage {
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.IconURLDialog";

	private MPerspective perspective;
	private URLSelectionComposite urlSelectionComposite;
	private ImageDisplayComposite imageDisplayComposite;

	private Label lblImageWidthValue;
	private Label lblImageHeightValue;

	private String urlString = null;

	private IResourceUtilities<?> resourceUtilities;

	public IconURLDialog(MPerspective perspective, IResourceUtilities<?> resourceUtilities) {
		super(WIZARD_PAGE_ID);
		this.perspective = perspective;
		this.resourceUtilities = resourceUtilities;

		if (this.perspective != null) {
			this.urlString = perspective.getIconURI();
		}

		setTitle("Perspective Icon : URL selection");
		setDescription("Sets the Perspective Icon URL.");

		validate();
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(1, false));

		// URL Selection
		urlSelectionComposite = new URLSelectionComposite(container, SWT.None,
				new String[] { "*.gif", "*.png", "*.jpg", "*.jpeg" }, true, true, true) {
			@Override
			protected void urlStringSelected(String newURLString) {
				IconURLDialog.this.urlString = newURLString;
				validate();
			}
		};
		urlSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		if (perspective != null && !perspective.getIconURI().equals("")) {
			urlSelectionComposite.setUrlString(perspective.getIconURI());
		}

		// Image Preview.
		Group grImagePreview = new Group(container, SWT.BORDER);
		grImagePreview.setText("Image Preview");
		grImagePreview.setLayout(new GridLayout(3, false));
		grImagePreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Label lblImageWidth = new Label(grImagePreview, SWT.NONE);
		lblImageWidth.setText("Image Width (pixels):");
		lblImageWidth.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));

		lblImageWidthValue = new Label(grImagePreview, SWT.BORDER);
		GridData gd_lblImageWidthValue = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_lblImageWidthValue.widthHint = 100;
		gd_lblImageWidthValue.minimumWidth = 100;
		lblImageWidthValue.setLayoutData(gd_lblImageWidthValue);

		imageDisplayComposite = new ImageDisplayComposite(grImagePreview, SWT.BORDER);
		imageDisplayComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		imageDisplayComposite.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event event) {
				imageDisplayComposite.fitImage();
			}
		});

		Label lblImageHeight = new Label(grImagePreview, SWT.NONE);
		lblImageHeight.setText("Image Height (pixels):");
		lblImageHeight.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));

		lblImageHeightValue = new Label(grImagePreview, SWT.BORDER);
		GridData gd_lblImageHeightValue = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_lblImageHeightValue.widthHint = 100;
		gd_lblImageHeightValue.minimumWidth = 100;
		lblImageHeightValue.setLayoutData(gd_lblImageHeightValue);

		setControl(container);
		urlSelectionComposite.setFocus();
	}

	protected void validate() {
		String errorMessage = null;

		if (this.urlString != null) {
			URI uri = URI.createURI(this.urlString);
			ImageDescriptor descriptor = (ImageDescriptor) resourceUtilities.imageDescriptorFromURI(uri);

			if (descriptor == null || descriptor.createImage() == null) {
				setErrorMessage("Unable to load image");
			}
		} else {
			setErrorMessage("Unable to load image");
		}

		setErrorMessage(errorMessage);
		setPageComplete(errorMessage == null);
	}

	public String getUrlString() {
		return urlString;
	}
}

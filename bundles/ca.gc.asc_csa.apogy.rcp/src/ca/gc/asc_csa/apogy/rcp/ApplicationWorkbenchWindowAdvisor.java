package ca.gc.asc_csa.apogy.rcp;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IContributionManager;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

	@Inject
	EModelService modelService;

	@Inject
	MApplication application;
	
	public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		super(configurer);
	}

	@Override
	public void postWindowCreate() {
		// remove unwanted menu entries
		List<String> unwantedItems = Arrays.asList("org.eclipse.ui.openLocalFile", "converstLineDelimitersTo",
				"org.eclipse.ui.cheatsheets.actions.CheatSheetHelpMenuAction",
				"org.eclipse.debug.ui.actions.BreakpointTypesContribution", "ExternalToolsGroup",
				"org.eclipse.ui.externaltools.ExternalToolMenuDelegateMenu", "navigate", "org.eclipse.search.menu",
				"org.eclipse.ui.run", "org.eclipse.debug.ui", "org.eclipse.debug.ui.launchActionSet",
				"org.eclipse.search.searchActionSet", "org.eclipse.ui.edit.text.actionSet.annotationNavigation",
				"org.eclipse.ui.edit.text.actionSet.navigation");

		removeUnwantedItems(unwantedItems, getWindowConfigurer().getActionBarConfigurer().getMenuManager());
	
		// remove unwanted toolbar entries
		unwantedItems = Arrays.asList("org.eclipse.ui.StatusLine", "org.eclipse.search.searchActionSet",
				"org.eclipse.ui.edit.text.actionSet.annotationNavigation",
				"org.eclipse.ui.edit.text.actionSet.navigation", "org.eclipse.debug.ui.launchActionSet");
		removeUnwantedItems(unwantedItems, getWindowConfigurer().getActionBarConfigurer().getCoolBarManager());			
	}

	private void removeUnwantedItems(final List<String> unwantedItems, final IContributionManager contributionManager) {
		IContributionItem[] items = contributionManager.getItems();

		for (IContributionItem item : items) {
			if (item instanceof IContributionManager) {
				removeUnwantedItems(unwantedItems, (IContributionManager) item);
			}
			if (unwantedItems.contains(item.getId())) {
				contributionManager.remove(item);
			}
		}
	}
}
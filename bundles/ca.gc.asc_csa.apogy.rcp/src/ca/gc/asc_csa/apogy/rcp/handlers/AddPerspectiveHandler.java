package ca.gc.asc_csa.apogy.rcp.handlers;

import javax.inject.Named;

import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspectiveStack;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainer;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.registry.PerspectiveRegistry;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.rcp.Activator;
import ca.gc.asc_csa.apogy.rcp.ApogyRCPConstants;
import ca.gc.asc_csa.apogy.rcp.dialogs.AddPerspectiveDialog;

@SuppressWarnings("restriction")
public class AddPerspectiveHandler {
	
	@Execute
	public void execute(IEclipseContext context, EModelService modelService, MApplication application,
			EPartService partService, EHandlerService handlerService, ECommandService commandService,
			@Optional @Named(ApogyRCPConstants.COMMANDS_PARAMETER__ADD_PERSPECTIVE__ID) String perspectiveID) {		
		MWindow window = (MWindow)modelService.find(ApogyRCPConstants.MAIN_WINDOW__ID, application);
		if (perspectiveID == null || perspectiveID.equals("")) {
			try {
				AddPerspectiveDialog dialog = ContextInjectionFactory.make(AddPerspectiveDialog.class, context);
				dialog.open();
			} catch (Exception e) {
				Logger.INSTANCE.log(Activator.ID, " Unable to open the dialog ", EventSeverity.ERROR, e);
			}
		}

		else {
			try {
				MPerspectiveStack stack = (MPerspectiveStack) modelService.find(ApogyRCPConstants.PERSPECTIVE_STACK__ID,
						window);
				
				Object object = modelService.cloneSnippet(application, perspectiveID, window);
							
				if(object instanceof MPerspective){
					MPerspective perspective = (MPerspective)object;
					perspective.getPersistedState().remove(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE);

					makeIDsUnique(perspective, stack);
					// Change the MParts IDs.
					for (MPart part : modelService.findElements(perspective, null, MPart.class, null)) {
						if (part.getContributionURI() != null && !part.getContributionURI().equals(
								"bundleclass://org.eclipse.ui.workbench/org.eclipse.ui.internal.e4.compatibility.CompatibilityView")) {
							part.setElementId(part.getElementId().concat("_" + perspective.getElementId()));
						}
					}

					// Change the MPartStacks IDs.
					for (MPartStack partStack : modelService.findElements(perspective, null, MPartStack.class, null)) {
						partStack.setElementId(partStack.getElementId().concat("_" + perspective.getElementId()));
					}

					// Change the MPartSashContaines IDs.
					for (MPartSashContainer partSashContainer : modelService.findElements(perspective, null,
							MPartSashContainer.class, null)) {
						partSashContainer.setElementId(
								partSashContainer.getElementId().concat("_" + perspective.getElementId()));
					}

					// Add the perspective to the registry.
					if(PlatformUI.getWorkbench().getPerspectiveRegistry().findPerspectiveWithId(perspective.getElementId()) == null){
						((PerspectiveRegistry)PlatformUI.getWorkbench().getPerspectiveRegistry()).addPerspective(perspective);
						application.getSnippets().add(modelService.cloneElement(perspective, application));						
					}
					
					// Add is to the stack and select it.
					stack.getChildren().add(perspective);
					stack.setSelectedElement(perspective);	
				}

				
			} catch (Exception e) {
				Logger.INSTANCE.log(Activator.ID, " Unable to open < " + perspectiveID + ">", EventSeverity.ERROR, e);
			}
		}
	}
	
	/**
	 * Makes the id of the perspective unique in the stack.
	 */
	private void makeIDsUnique(MPerspective perspective, MPerspectiveStack stack){
		int j = 1;
		// Find an ID that is unique
		for (int i = 0; i < stack.getChildren().size(); i++) {
			if (stack.getChildren().get(i) instanceof MPerspective) {
				MPerspective stackedPerspective = (MPerspective) stack.getChildren().get(i);
				if (stackedPerspective.getElementId() != null && stackedPerspective.getElementId()
						.startsWith(perspective.getElementId() + "_" + Integer.toString(j))) {
					j++;
					i = 0;
				}
			}
		}
		perspective.setElementId(perspective.getElementId() + "_" + Integer.toString(j));
	}
}
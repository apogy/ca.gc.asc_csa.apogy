package ca.gc.asc_csa.apogy.rcp;

public class ApogyRCPConstants {
	
	/**
	 * Perspective stack
	 */
	public static final String PERSPECTIVE_STACK__ID = "org.eclipse.ui.ide.perspectivestack";

	/**
	 * Main window
	 */
	public static final String MAIN_WINDOW__ID = "ca.gc.asc_csa.apogy.rcp.window.main";

	/**
	 * Command addPerspective
	 */
	public static final String COMMANDS__ADD_PERSPECTIVE__ID = "ca.gc.asc_csa.apogy.rcp.command.addPerspective";
	public static final String COMMANDS_PARAMETER__ADD_PERSPECTIVE__ID = "ca.gc.asc_csa.apogy.rcp.commandparameter.addPerspective.perspectiveID";

	/**
	 * Perspective icon and label persisted states.
	 */
	public static final String PERSISTED_STATE__MPERSPECTIVE__DISPLAY_ICON = "Icon";
	public static final String PERSISTED_STATE__MPERSPECTIVE__DISPLAY_LABEL = "Label";
	
	/**
	 * Standalone element.
	 */
	public static final String PERSISTED_STATE__MUIELEMENT__STANDALONE = "Standalone";
 }

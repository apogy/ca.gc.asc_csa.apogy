package ca.gc.asc_csa.apogy.rcp.dialogs;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.IResourceUtilities;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.rcp.Activator;
import ca.gc.asc_csa.apogy.rcp.ApogyRCPConstants;

@SuppressWarnings("restriction")
public class AddPerspectiveDialog extends Dialog {

	TableViewer viewer;

	@Inject
	private ECommandService commandService;

	@Inject
	private EHandlerService handlerService;

	private MApplication application;
	private IResourceUtilities<?> resourceUtilities;

	@Inject
	public AddPerspectiveDialog(@Named(IServiceConstants.ACTIVE_SHELL) Shell parentShell, MApplication application) {
		super(parentShell);
		setShellStyle(getShellStyle() | SWT.SHEET);
		this.application = application;
		this.resourceUtilities = (IResourceUtilities<?>) PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getService(IResourceUtilities.class);
	}

	@Override
	protected void configureShell(Shell shell) {
		shell.setText("Add Perspective");
		super.configureShell(shell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = new Composite(parent, SWT.None);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		composite.setLayout(new FillLayout());

		viewer = new TableViewer(composite, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		viewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				okPressed();
			}
		});

		viewer.setLabelProvider(new perspectivesLabelProvider());
		viewer.setContentProvider(new perspectivesContentProvider());

		viewer.setInput(application);

		return composite;
	}

	@Override
	protected void okPressed() {
		if (viewer.getStructuredSelection() != null
				&& viewer.getStructuredSelection().getFirstElement() instanceof MPerspective) {

			MPerspective perspective = ((MPerspective) viewer.getStructuredSelection().getFirstElement());

			/**
			 * Create the command
			 */
			HashMap<String, Object> parameters = new HashMap<>(1);
			parameters.put(ApogyRCPConstants.COMMANDS_PARAMETER__ADD_PERSPECTIVE__ID, perspective.getElementId());

			ParameterizedCommand command = commandService.createCommand(ApogyRCPConstants.COMMANDS__ADD_PERSPECTIVE__ID,
					parameters);
			// Execute the command
			handlerService.executeHandler(command);
		}
		super.okPressed();
	}

	/**
	 * Label provider for the treeViewer.
	 */
	private class perspectivesLabelProvider extends ColumnLabelProvider {

		@Override
		public String getText(Object element) {
			return ((MPerspective) element).getLabel();
		}

		@Override
		public Image getImage(Object element) {
			ImageDescriptor descriptor = null;
			try {
				String str_uri = ((MPerspective) element).getIconURI();
				if (str_uri != null) 
				{
					URI uri = URI.createURI(str_uri);
					descriptor = (ImageDescriptor) resourceUtilities.imageDescriptorFromURI(uri);
					
					if(descriptor != null)
					{
						return descriptor.createImage();
					}
					else
					{
						return null;
					}
				}

			} catch (RuntimeException e) {
				Logger.INSTANCE.log(Activator.ID, this.getClass().getName() + "Unable to display the icon",
						EventSeverity.ERROR, e);
			}
			return null;
		}
	}

	/**
	 * Content provider for the treeViewer.
	 */
	private class perspectivesContentProvider implements IStructuredContentProvider {
		@Override
		public Object[] getElements(Object inputElement) {
			List<MPerspective> perspectives = new ArrayList<MPerspective>();

			for (MUIElement element : application.getSnippets()) {
				if (element instanceof MPerspective 
						&& element.getPersistedState().containsKey(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE) 
						&& Boolean.parseBoolean(element.getPersistedState().get(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE))) {
					perspectives.add((MPerspective) element);
				}
			}
			
			List<MPerspective> sorted = sortMPerspectiveAlphabetically(perspectives);
			
			return sorted.toArray();
		}
		
		private List<MPerspective> sortMPerspectiveAlphabetically(List<MPerspective> mUIElements)
		{
			SortedSet<MPerspective> sorted = new TreeSet<MPerspective>(new Comparator<MPerspective>() 
			{
				@Override
				public int compare(MPerspective arg0, MPerspective arg1) 
				{									
					if(arg0.getLabel() != null && arg1.getLabel() != null)
					{
						if(arg0.getLabel().compareTo(arg1.getLabel()) > 0)
						{
							return 1;
						}
						else if(arg0.getLabel().compareTo(arg1.getLabel()) < 0)
						{
							return -1;
						}
						else
						{
							if(arg0.hashCode() > arg1.hashCode())
							{
								return 1;
							}
							else
							{
								return -1;
							}
						}
					}
					
					if(arg0.hashCode() > arg1.hashCode())
					{
						return 1;
					}
					else
					{
						return -1;
					}																									
				}
			});		
			sorted.addAll(mUIElements);														
			
			List<MPerspective> result = new ArrayList<MPerspective>();
			result.addAll(sorted);
							
			return result;
		}
		
	}
}

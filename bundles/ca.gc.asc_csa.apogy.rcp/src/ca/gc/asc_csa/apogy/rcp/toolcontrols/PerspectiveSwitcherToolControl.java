package ca.gc.asc_csa.apogy.rcp.toolcontrols;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspectiveStack;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainer;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.IResourceUtilities;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.perspectiveswitcher.tools.E4Util;
import org.eclipse.e4.ui.workbench.perspectiveswitcher.tools.EPerspectiveSwitcher;
import org.eclipse.e4.ui.workbench.perspectiveswitcher.tools.IPerspectiveSwitcherControl;
import org.eclipse.e4.ui.workbench.perspectiveswitcher.tools.PerspectiveSwitcherSwtTrim;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.rcp.Activator;
import ca.gc.asc_csa.apogy.rcp.ApogyRCPConstants;
import ca.gc.asc_csa.apogy.rcp.dialogs.ModifyPerspectiveShortcutDialog;

@SuppressWarnings("restriction")
public class PerspectiveSwitcherToolControl implements IPerspectiveSwitcherControl {

	private final String DEFAULT_ICON_URI = "icons/full/eview16/new_persp.gif";

	private ToolBar toolBar;
	private Composite composite;

	@Inject
	private MApplication application;

	private MWindow window;

	@Inject
	private IResourceUtilities<?> resourceUtilities;

	@Inject
	private EPerspectiveSwitcher perspectiveSwitcher;

	@Inject
	private ECommandService commandService;

	@Inject
	private EModelService modelService;

	@Inject
	private EHandlerService handlerService;

	@Inject
	private Shell shell;

	@PostConstruct
	public void createControls(Composite parent) {
		this.window = (MWindow) modelService.find(ApogyRCPConstants.MAIN_WINDOW__ID, application);

		perspectiveSwitcher.setControlProvider(this);
		composite = new Composite(parent, SWT.None);
		RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
		rowLayout.marginLeft = 0;
		rowLayout.marginRight = window.getWidth();
		rowLayout.marginTop = 0;
		rowLayout.marginBottom = 0;
		rowLayout.justify = false;
		rowLayout.center = false;
		composite.setLayout(rowLayout);

		toolBar = new ToolBar(composite, SWT.FLAT | SWT.WRAP | SWT.RIGHT);
		toolBar.addMenuDetectListener(new MenuDetectListener() {

			@Override
			public void menuDetected(MenuDetectEvent event) {
				/**
				 * Menu when a perspective shortcut is clicked
				 */
				ToolBar tb = (ToolBar) event.widget;
				Point p = new Point(event.x, event.y);

				p = toolBar.getDisplay().map(null, toolBar, p);
				ToolItem item = tb.getItem(p);
				if (item != null && item.getData() != null) {
					MPerspective perspective = (MPerspective) item.getData();
					final Menu menu = new Menu(toolBar);
					menu.setData(perspective);

					/*
					 * Reset
					 */
					MPerspectiveStack stack = (MPerspectiveStack) modelService
							.find(ApogyRCPConstants.PERSPECTIVE_STACK__ID, window);

					if (stack.getSelectedElement() == perspective) {
						MenuItem resetMenuItem = new MenuItem(menu, SWT.Activate);
						resetMenuItem.setText("Reset");
						resetMenuItem.addSelectionListener(new SelectionAdapter() {
							@Override
							public void widgetSelected(SelectionEvent event) {
								IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
										.getActivePage();
								page.resetPerspective();
							}
						});
					}

					/*
					 * Change shortcut
					 */
					MenuItem changeShortcutMenuItem = new MenuItem(menu, SWT.Activate);
					changeShortcutMenuItem.setText("Modify shortcut");
					changeShortcutMenuItem.addSelectionListener(new SelectionAdapter() {

						@Override
						public void widgetSelected(SelectionEvent event) {
							ModifyPerspectiveShortcutDialog dialog = new ModifyPerspectiveShortcutDialog(shell,
									(MPerspective) item.getData(), resourceUtilities);
							dialog.open();
							toolBar.layout();
							composite.layout();
						}
					});

					new MenuItem(menu, SWT.SEPARATOR);

					/*
					 * Save as
					 */
					MenuItem saveAsMenuItem = new MenuItem(menu, SWT.Activate);
					saveAsMenuItem.setText("Save as");
					saveAsMenuItem.addSelectionListener(new SelectionAdapter() {

						@Override
						public void widgetSelected(SelectionEvent event) {

							FileDialog dialog = new FileDialog(shell, SWT.SAVE);
							dialog.setFileName("perspective");
							dialog.setFilterExtensions(new String[] { "*.e4xmi" });
							String path = dialog.open();

							if (path != null) {
								try {
									// Find the directory to save the
									// perspective
									File tmpFile = new File(path);
									URI uri = URI.createURI(tmpFile.toURI().toString());

									// Create the resource
									ResourceSet resourceSet = new ResourceSetImpl();
									Resource resource = resourceSet.createResource(uri);
									MPerspective clonedPerspective = (MPerspective) modelService
											.cloneElement(perspective, null);
									preparePerspective(clonedPerspective);
									resource.getContents().add((EObject) clonedPerspective);
									// Save the resource
									resource.save(Collections.EMPTY_MAP);

								} catch (IOException e) {
									Logger.INSTANCE.log(Activator.ID, this, " Error in url file creation",
											EventSeverity.ERROR);
								}
							}
						}
					});

					/*
					 * Close
					 */
					MenuItem closeMenuItem = new MenuItem(menu, SWT.Activate);
					closeMenuItem.setText("Close");
					closeMenuItem.addSelectionListener(new SelectionAdapter() {

						@Override
						public void widgetSelected(SelectionEvent event) {
							closePerspective(perspective, item);
						}

					});

					Rectangle bounds = item.getBounds();
					Point point = toolBar.toDisplay(bounds.x, bounds.y + bounds.height);
					menu.setLocation(point);
					menu.setVisible(true);
					menu.addMenuListener(new MenuListener() {

						@Override
						public void menuShown(MenuEvent e) {
						}

						@Override
						public void menuHidden(MenuEvent e) {
							toolBar.getDisplay().asyncExec(new Runnable() {

								@Override
								public void run() {
									menu.dispose();
								}
							});
						}
					});
				}
			}
		});

		/**
		 * AddPerspective button
		 */
		ToolItem addPerspectiveItem = new ToolItem(toolBar, SWT.PUSH);
		// Image
		Bundle bundle = FrameworkUtil.getBundle(PerspectiveSwitcherSwtTrim.class);
		URL url = FileLocator.find(bundle, new Path(DEFAULT_ICON_URI), null);
		ImageDescriptor imageDescr = ImageDescriptor.createFromURL(url);
		addPerspectiveItem.setImage(imageDescr.createImage());

		addPerspectiveItem.setToolTipText("Open New Perspective");
		addPerspectiveItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				ParameterizedCommand command = commandService
						.createCommand(ApogyRCPConstants.COMMANDS__ADD_PERSPECTIVE__ID, Collections.emptyMap());
				handlerService.executeHandler(command);
			}
		});

		/**
		 * Separator
		 */
		new ToolItem(toolBar, SWT.SEPARATOR);

		MPerspectiveStack stack = (MPerspectiveStack) modelService.find(ApogyRCPConstants.PERSPECTIVE_STACK__ID,
				window);
		/** Add shortcuts for opened perspectives */
		for (MPerspective perspective : stack.getChildren()) 
		{
			if (perspective.isToBeRendered()) 
			{
				addPerspectiveShortcut(perspective);
			}
		}
	}

	private void closePerspective(MPerspective perspective, ToolItem item) {
		MPerspectiveStack stack = (MPerspectiveStack) (MElementContainer<?>) perspective.getParent();
		int index = stack.getChildren().indexOf(perspective);

		perspective.setToBeRendered(false);
		stack.getChildren().remove(perspective);
		// Dispose the shortcut.
		item.dispose();

		// Change the stack's selection if needed.
		if (stack.getSelectedElement() == null) {
			if (stack.getChildren().isEmpty()) {
				Object widget = perspective.getWidget();
				if (widget instanceof Composite) {
					((Composite) widget).dispose();
				}
			} else {
				if (index < stack.getChildren().size()) {
					stack.setSelectedElement(stack.getChildren().get(index));
				} else {
					stack.setSelectedElement(stack.getChildren().get(stack.getChildren().size() - 1));
				}
			}
		}

		MPerspective snippetPerspective = (MPerspective) modelService.cloneSnippet(application,
				perspective.getElementId(), window);
		if (!snippetPerspective.getPersistedState()
				.containsKey(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE)
				|| !Boolean.parseBoolean(snippetPerspective.getPersistedState()
						.get(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE))) {
			PlatformUI.getWorkbench().getPerspectiveRegistry().deletePerspective(PlatformUI.getWorkbench()
					.getPerspectiveRegistry().findPerspectiveWithId(snippetPerspective.getElementId()));
			application.getSnippets().remove(modelService.findSnippet(application, snippetPerspective.getElementId()));
		}
	}

	/**
	 * @param perspective
	 *            the perspective to find
	 * @return the corresponding {@link ToolItem} or null if not found.
	 */
	private ToolItem getToolItemFor(MPerspective perspective) {
		if (toolBar != null && !toolBar.isDisposed()) {
			for (ToolItem item : toolBar.getItems()) {
				if (item.getData() == perspective)
					return item;
			}
		}
		return null;
	}

	@Override
	public void addPerspectiveShortcut(MPerspective perspective) {
		// Create the item.
		ToolItem perspectiveItem = new ToolItem(toolBar, SWT.RADIO);
		perspectiveItem.setData(perspective);

		if (!perspective.getPersistedState().containsKey(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_LABEL)
				&& !perspective.getPersistedState()
						.containsKey(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_ICON)) {
			perspective.getPersistedState().put(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_ICON, "true");
			perspective.getPersistedState().put(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_LABEL, "true");
		}

		// Set the icon as shortcut.
		ImageDescriptor descriptor = getIconFor(perspective.getIconURI());
		if (descriptor != null
				&& perspective.getPersistedState()
						.containsKey(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_ICON)
				&& perspective.getPersistedState().get(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_ICON)
						.equals("true")) {
			Image icon = descriptor.createImage();
			perspectiveItem.setImage(icon);
			if (perspectiveItem.getText() == null) {
				perspectiveItem.setText("");
			}

			perspectiveItem.setToolTipText(perspective.getLocalizedLabel());
		}
		// Otherwise, if there is no icon, set the text as shorcut.
		if (perspective.getPersistedState().containsKey(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_LABEL)
				&& perspective.getPersistedState().get(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_LABEL)
						.equals("true")) {
			String label = perspective.getLocalizedLabel();
			if (label != null) {
				perspectiveItem.setText(label);
			} else {
				perspectiveItem.setText("");
			}
			perspectiveItem.setToolTipText(perspective.getLocalizedTooltip());
		}

		perspectiveItem.setSelection(E4Util.isSelectedElement(perspective));
		// Opens the perspective
		perspectiveItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				MPerspective perspective = (MPerspective) event.widget.getData();
				E4Util.setWindowSelectedElement(perspective);
			}
		});

		toolBar.layout();
		composite.layout();
	}

	@Override
	public void removePerspectiveShortcut(MPerspective perspective) {
		ToolItem item = getToolItemFor(perspective);
		if (item != null && !item.isDisposed()) {

			Image icon = item.getImage();
			if (icon != null) {
				item.setImage(null);
				icon.dispose();
				icon = null;
			}
			item.dispose();
		}
	}

	@Override
	public void setSelectedElement(MPerspective perspective) {
		if (getToolItemFor(perspective) == null) {
			addPerspectiveShortcut(perspective);
		}
		for (ToolItem item : toolBar.getItems()) {
			item.setSelection(item.getData() == perspective);
		}
	}

	@Override
	public void updateAttributeFor(MPerspective perspective, String attributeName, Object newValue) {
		if (getToolItemFor(perspective) == null) {
			addPerspectiveShortcut(perspective);
		}

		if (newValue instanceof Entry<?, ?>) {
			newValue = ((Entry<?, ?>) newValue).getValue();
		}

		ToolItem item = getToolItemFor(perspective);
		// If the name is changed.
		if (UIEvents.UILabel.LABEL.equals(attributeName)
				|| UIEvents.ApplicationElement.PERSISTEDSTATE.equals(attributeName)) {
			if (perspective.getPersistedState()
					.containsKey(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_LABEL)
					&& perspective.getPersistedState()
							.get(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_LABEL).equals("true")) {
				if (perspective.getLabel() != null) {
					item.setText(perspective.getLabel());
				} else {
					item.setText("");
				}
			} else {
				item.setText("");
			}
		}
		// Otherwise, if the toolTip is changed.
		if (UIEvents.UILabel.TOOLTIP.equals(attributeName)) {
			if (perspective.getTooltip() != null) {
				item.setToolTipText(perspective.getTooltip());
			} else {
				item.setToolTipText("");
			}
		}
		// Otherwise, if the icon is changed.
		if (UIEvents.UILabel.ICONURI.equals(attributeName)
				|| UIEvents.ApplicationElement.PERSISTEDSTATE.equals(attributeName)) {
			if (perspective.getPersistedState()
					.containsKey(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_ICON)
					&& perspective.getPersistedState()
							.get(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_ICON).equals("true")) {
				ImageDescriptor descriptor = getIconFor(perspective.getIconURI());
				Image oldIcon = item.getImage();

				Image newIcon = null;
				if (descriptor != null) {
					newIcon = descriptor.createImage();
				}

				item.setImage(newIcon);

				if (oldIcon != null) {
					oldIcon.dispose();
					oldIcon = null;
				}
			} else {
				item.setImage(null);
			}

		}
		toolBar.layout();
		composite.layout();
	}

	/**
	 * Gets the icon from the resourceUtilities using a {@link URI}.
	 * 
	 * @param iconURI
	 *            {@link String} of the {@link URI}.
	 * @return {@link ImageDescriptor}
	 */
	private ImageDescriptor getIconFor(String iconURI) {
		ImageDescriptor descriptor = null;
		try {
			URI uri = URI.createURI(iconURI);
			descriptor = (ImageDescriptor) resourceUtilities.imageDescriptorFromURI(uri);
		} catch (RuntimeException ex) {
			String message = this.getClass().getSimpleName() + " Error while getting the icon for the perspective";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
		}
		return descriptor;
	}

	@PreDestroy
	void cleanUp() {
		if (perspectiveSwitcher != null) {
			perspectiveSwitcher.setControlProvider(null);
		}

		perspectiveSwitcher = null;

		if (toolBar != null && !toolBar.isDisposed()) {
			for (ToolItem item : toolBar.getItems()) {
				Image icon = item.getImage();
				if (icon != null) {
					item.setImage(null);
					icon.dispose();
					icon = null;
				}
			}
		}
	}

	/**
	 * Removes the perspective in the {@link MPart}s,
	 * {@link MPartSashContainer}s and {@link MPartStack}s in the
	 * {@link MPerspective} and changes the persistedState. This is needed to
	 * have unique IDs for selectionBasedParts when importing the perspective.
	 */
	private void preparePerspective(MPerspective perspective) {
		/** Change the MParts IDs. */
		for (MPart part : modelService.findElements(perspective, null, MPart.class, null)) {
			if (part.getElementId().endsWith("_" + perspective.getElementId())) {
				part.setElementId(part.getElementId().substring(0,
						part.getElementId().indexOf("_" + perspective.getElementId())));
			}
		}

		/** Change the MPartStacks IDs. */
		for (MPartStack partStack : modelService.findElements(perspective, null, MPartStack.class, null)) {
			if (partStack.getElementId().endsWith("_" + perspective.getElementId())) {
				partStack.setElementId(partStack.getElementId().substring(0,
						partStack.getElementId().indexOf("_" + perspective.getElementId())));
			}
		}

		/** Change the MPartSashContaines IDs. */
		for (MPartSashContainer partSashContainer : modelService.findElements(perspective, null,
				MPartSashContainer.class, null)) 
		{
			if (partSashContainer.getElementId() != null && partSashContainer.getElementId().endsWith("_" + perspective.getElementId())) 
			{
				partSashContainer.setElementId(partSashContainer.getElementId().substring(0,
						partSashContainer.getElementId().indexOf("_" + perspective.getElementId())));
			}
		}

		/** Change the persisted state */
		perspective.getPersistedState().put(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE, "false");
	}
}
package ca.gc.asc_csa.apogy.common.topology.ui.viewer.preferences;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.swt.graphics.RGB;

import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;

public class TopologyViewerLightingPreferenceConstants {

	public static final String P_ANTI_ALIASING = "ca.gc.asc_csa.apogy.common.topology.ui.antialising";

	public static final String P_SHOW_3D_AXIS = "ca.gc.asc_csa.apogy.common.topology.ui.show3DAxis";

	// Colors
	public static final String AMBIENT_LIGHT_COLOR_ID = "AMBIENT_LIGHT_COLOR_ID";
	public static final RGB AMBIENT_LIGHT_COLOR_DEFAULT = new RGB(255, 255, 255);
	
	// Light Direction
	public static final String AMBIENT_LIGHT_DIRECTION_ID = "AMBIENT_LIGHT_DIRECTION_ID";
	public static final Tuple3d AMBIENT_LIGHT_DIRECTION_DEFAULT = ApogyCommonMathFacade.INSTANCE.createTuple3d(-1.0, -1.0, -0.5);
}

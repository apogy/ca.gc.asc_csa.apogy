package ca.gc.asc_csa.apogy.common.topology.ui.viewer.actions;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.TopologyViewerProvider;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.internal.IConstants;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.internal.PluginImages;


public class ZoomToFitAction extends Action 
{
	/**
	 * The {@link TopologyView} on which we want to perform a zoom to fit.
	 */
	private TopologyViewerProvider provider;
	
	public ZoomToFitAction(TopologyViewerProvider provider) 
	{
		super("Zoom-to-Fit");
		if (provider == null) 
		{
			throw new IllegalArgumentException("Provider is null");
		}

		// The image to assign to this action.
		ImageDescriptor zoomToFit = PluginImages.getImageDescriptor(IConstants.IMG_ELCL_ZOOM_TO_FIT);

		setImageDescriptor(zoomToFit);

		this.provider = provider;
	}

	@Override
	public void run() 
	{
		if (provider.getTopologyViewer() != null) 
		{
			provider.getTopologyViewer().zoomToFit();
		}
	}
}

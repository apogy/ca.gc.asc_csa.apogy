package ca.gc.asc_csa.apogy.common.topology.ui.viewer.preferences;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;

import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.Activator;

public class TopologyViewerLightingPreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() 
	{
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();				
		PreferenceConverter.setDefault(store, TopologyViewerLightingPreferenceConstants.AMBIENT_LIGHT_COLOR_ID, TopologyViewerLightingPreferenceConstants.AMBIENT_LIGHT_COLOR_DEFAULT);
		
		String vectorString = getTuple3dPreferenceStoreString(TopologyViewerLightingPreferenceConstants.AMBIENT_LIGHT_DIRECTION_DEFAULT); 
		store.setDefault(TopologyViewerLightingPreferenceConstants.AMBIENT_LIGHT_DIRECTION_ID, vectorString);		
	}
	
	public static String getTuple3dPreferenceStoreString(Tuple3d tuple3d)
	{
		String vectorString = tuple3d.getX() + ", " + tuple3d.getY() + ", " + tuple3d.getZ();
		
		return vectorString;
	}
	
	public static Tuple3d getTuple3dFromPreferenceStoreString(String vectorString)
	{
		Tuple3d direction = null;
		if(vectorString != null)
		{
			String[] items = vectorString.split(",");
			if(items.length >= 3)
			{
				double x = Double.parseDouble(items[0].trim());
				double y = Double.parseDouble(items[1].trim());
				double z = Double.parseDouble(items[2].trim());
				
				direction = ApogyCommonMathFacade.INSTANCE.createTuple3d(x, y, z);
			}
		}
		return direction;
	}
	
	public static Tuple3d getTuple3dFromPreferenceStore(IPreferenceStore store)
	{
		String string = store.getString(TopologyViewerLightingPreferenceConstants.AMBIENT_LIGHT_DIRECTION_ID);
		return getTuple3dFromPreferenceStoreString(string);
	}

}

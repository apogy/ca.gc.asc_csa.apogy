package ca.gc.asc_csa.apogy.common.topology.ui.viewer.commands;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.TopologyViewerProvider;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

public class ZoomToFit extends AbstractHandler {		

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IWorkbenchWindow activeWorkbenchWindow = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);

		if (activeWorkbenchWindow.getActivePage().getActivePart() instanceof TopologyViewerProvider) 
		{
			TopologyViewerProvider topoView = (TopologyViewerProvider) activeWorkbenchWindow.getActivePage().getActivePart();
			topoView.getTopologyViewer().zoomToFit();		
		}
		return null;
	}

}

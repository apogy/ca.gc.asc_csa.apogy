package ca.gc.asc_csa.apogy.common.topology.ui.viewer;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/


public interface TopologyViewerProvider 
{

	/**
	 * Return the ITopologyViewer referred to the interface implementer.
	 * @return The ITopologyViewer, can be null.
	 */
	public ITopologyViewer getTopologyViewer();

}

package ca.gc.asc_csa.apogy.common.topology.ui.viewer;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/


public interface ITopologyViewerListener 
{
	public void mouseClicked(MouseButton button); 
	
	public void busyChanged(boolean oldBusy, boolean newBusy);
	
	public void antiAliasingChanged(boolean oldAntiAliasing, boolean newAntiAliasing);		
}

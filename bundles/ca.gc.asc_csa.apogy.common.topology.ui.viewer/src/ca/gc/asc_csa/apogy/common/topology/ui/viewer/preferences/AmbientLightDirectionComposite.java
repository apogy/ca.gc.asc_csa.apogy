/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.ui.viewer.preferences;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;

public class AmbientLightDirectionComposite extends Composite 
{
	private Tuple3d direction;
	private DecimalFormat decimalFormat = new DecimalFormat("0.000");
	
	private Text xText;
	private Text yText;
	private Text zText;
	
	public AmbientLightDirectionComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(3, true));
		
		Label xLabel = new Label(this, SWT.NONE);
		xLabel.setText("X (m)");
		
		Label yLabel = new Label(this, SWT.NONE);
		yLabel.setText("Y (m)");
	
		Label zLabel = new Label(this, SWT.NONE);
		zLabel.setText("Z (m)");
		
		xText = new Text(this, SWT.BORDER);
		xText.setText("?");
		xText.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent arg0) 
			{				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) 
			{
				try
				{
					Double.parseDouble(xText.getText());
					xText.setBackground(null);										
				}
				catch (Exception e) 
				{
					xText.setBackground(new Color(getDisplay(), new RGB(255, 0, 0)));
				}
			}
		});
				
		yText = new Text(this, SWT.BORDER);
		yText.setText("?");
		yText.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent arg0) 
			{				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) 
			{
				try
				{
					Double.parseDouble(yText.getText());
					yText.setBackground(null);										
				}
				catch (Exception e) 
				{
					yText.setBackground(new Color(getDisplay(), new RGB(255, 0, 0)));
				}
			}
		});
				
		zText = new Text(this, SWT.BORDER);
		zText.setText("?");	
		zText.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent arg0) 
			{				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) 
			{
				try
				{
					Double.parseDouble(zText.getText());
					zText.setBackground(null);										
				}
				catch (Exception e) 
				{
					zText.setBackground(new Color(getDisplay(), new RGB(255, 0, 0)));
				}
			}
		});
	}

	public Tuple3d getDirection() 
	{
		double x = Double.parseDouble(xText.getText());
		double y = Double.parseDouble(yText.getText());
		double z = Double.parseDouble(zText.getText());
		
		this.direction = ApogyCommonMathFacade.INSTANCE.createTuple3d(x, y, z);		
		return direction;
	}

	public void setDirection(Tuple3d direction) 
	{
		xText.setText(decimalFormat.format(direction.getX()));		
		yText.setText(decimalFormat.format(direction.getY()));		
		zText.setText(decimalFormat.format(direction.getZ()));	
		
		this.direction = direction;
	}
}

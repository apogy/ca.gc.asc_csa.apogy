package ca.gc.asc_csa.apogy.addons.mobility.controllers;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.addons.geometry.paths.WayPointPath;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Waypoint
 * Path Recorder</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A specialization of PathRecorder that produces a WayPointPath.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.mobility.controllers.ApogyAddonsMobilityControllersPackage#getWaypointPathRecorder()
 * @model
 * @generated
 */
public interface WaypointPathRecorder extends PathRecorder<WayPointPath> {
} // WaypointPathRecorder

// *****************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
//     Pierre Allard - initial API and implementation
//     Regent L'Archeveque
//        
// SPDX-License-Identifier: EPL-1.0
// *****************************************************************************
@GenModel(prefix="ApogyAddonsMobilityControllers",
 copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Pierre Allard - initial API and implementation
     Regent L'Archeveque
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",	
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  modelName="ApogyAddonsMobilityControllers",
		  dynamicTemplates="true", 
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.addons.mobility.controllers/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.addons.mobility.controllers.edit/src-generated")

package ca.gc.asc_csa.apogy.addons.mobility.controllers

import ca.gc.asc_csa.apogy.addons.mobility.MobilePlatform
import ca.gc.asc_csa.apogy.addons.geometry.paths.Path
import ca.gc.asc_csa.apogy.addons.sensors.pose.PoseSensor
import ca.gc.asc_csa.apogy.addons.geometry.paths.WayPointPath
import ca.gc.asc_csa.apogy.addons.mobility.SkidSteeredMobilePlatform
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPositionCoordinates
import ca.gc.asc_csa.apogy.common.geometry.data3d.Pose

/**
 * A PathFollower is an entity that makes a MobilePlatform follow a specified Path.
 * The default implementation of PathFollower listens for changes in the SensorStatus 
 * of its PoseSensor and the MobilePlateformStatus of its MobilePlateform and pauses 
 * or resumes the path following accordingly.
 */
class PathFollower <PlatformType extends MobilePlatform, PathType extends Path>
{
	/**
	 * Start following the path. 
	 */
	op boolean start()
	
	/**
	 * Pause the following of the path. 
	 */
	op boolean pause()
			
	/**
	 * Resumes the following of the path. 
	 */
	op boolean resume()
	
	/**
	 * Stop following the path. 
	 */
	op boolean stop()
	
	/**
	 * Returns whether or not a transition from the current state to a target state is valid.
	 * @param state The target state.
	 * @return True if the transition is valid, false otherwise.
	 */
	op boolean isTransitionValid(PathFollowerState state)
	
	/**
	 * The path to follow.
	 */
	refers PathType path
	
	/**
	 * The mobile platform to control.
	 */
	refers PlatformType platform
	
	/**
	 * The pose sensor to use to get position feedback.
	 */
	refers PoseSensor poseSensor
	
	/**
	 * Whether or not the destination (end of the path) has been reached.
	 */
	boolean destinationReached
	
	/**
	 * The current state of the follower.
	 */
	PathFollowerState pathFollowerState = "IDLE"
}

/**
 * The path follower states.
 */
enum PathFollowerState
{
	IDLE = 0,
	RUNNING = 1,
	PAUSED = 2,
	FAILED = -1
}

/**
 * A specialization of the PathFollower used with a SkidSteeredMobilePlatform.
 */
class SkidSteeredPlatformPathFollower <PlateformType extends SkidSteeredMobilePlatform, PathType extends Path> extends PathFollower <PlateformType, PathType>
{
	/**
	 * The maximum angular velocity supported.
	 */
	@GenModel(apogy_units = "rad/s")	
	double maximumAngularVelocity
	
	/**
	 * The maximum linear velocity supported.
	 */
	@GenModel(apogy_units = "m/s")
	double maximumLinearVelocity
}

/**
 * A recorder of the path provided by successive pose provided by a PoseSensor.
 */
class PathRecorder <PathType extends Path>
{
	/**
	 * The PoseSensor.
	 */
	refers PoseSensor positionSensor
	
	/**
	 * The minimum distance between sensor reading that should cause a pose to be recorded when in ON_DISTANCE_DELTA or ON_TIME_OR_DISTANCE_DELTA mode.
	 */
	@GenModel(apogy_units = "m")
	double minimumDistanceDelta = "0.1"
	
	/**
	 * The minimum period between sensor reading that should cause a pose to be recorded when in ON_TIME_DELTA or ON_TIME_OR_DISTANCE_DELTA mode.
	 */
	@GenModel(apogy_units = "s")
	double minimumTimeDelta = "1.0"
	
	/**
	 * The recording mode.
	 */
	PathRecorderSamplingMode samplingMode = "ON_DISTANCE_DELTA"
	
	/**
	 * The path being recorded.
	 */
	refers PathType recordedPath
}

/**
 * Recording Mode used to record a path.
 */
enum PathRecorderSamplingMode
{
	ON_DISTANCE_DELTA = 0,
	ON_TIME_DELTA = 1,
	ON_TIME_OR_DISTANCE_DELTA = 2
}

/**
 * A specialization of PathRecorder that produces a WayPointPath.
 */
class WaypointPathRecorder extends PathRecorder<WayPointPath>
{	
}

/**
 * A specialization of the SkidSteeredPlatformPathFollower used with a SkidSteeredMobilePlatform and a WayPointPath
 */
class SkidSteeredWayPointPathFollower extends SkidSteeredPlatformPathFollower <SkidSteeredMobilePlatform, WayPointPath>
{	
}

/**
 * A SkidSteeredWayPointPathFollower implemented using the Astolfi algorithm.
 */
class AstolfiGuidanceController extends SkidSteeredPlatformPathFollower <SkidSteeredMobilePlatform, WayPointPath>
{
	/**
	 * The way point currently tracked.
	 */
	refers CartesianPositionCoordinates currentWayPoint
	
	/**
	 * The way point previously tracked.
	 */
	refers CartesianPositionCoordinates previousWayPoint
	
	/**
	 * The current pose of the SkidSteeredMobilePlatform in the guidance frame of reference.
	 */
	refers Pose currentPoseInGuidanceReferenceFrame
	
	
	readonly transient double rho
	
	readonly transient volatile derived double phi
	
	readonly transient volatile derived double alpha
	
	readonly transient volatile derived double yaw
	
	readonly transient volatile derived double  nu
	
	readonly transient volatile derived double  omega
	
	double krho
	
	double kphi
	
	double kalpha
	
	double destinationDistanceThreshold
	
	double wayPointDistanceThreshold
	
	double kHill
	
	double hillThreshold
	
	double phiThresholdForReducedVelocity
	
	double alphaThresholdForReducedVelocity
	
	boolean smoothPathEnabled = "true"
}
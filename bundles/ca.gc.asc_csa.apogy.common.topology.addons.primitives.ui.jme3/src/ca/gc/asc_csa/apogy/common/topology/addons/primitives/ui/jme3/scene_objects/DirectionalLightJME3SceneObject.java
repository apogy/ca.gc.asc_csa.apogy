package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.jme3.scene_objects;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.concurrent.Callable;

import javax.vecmath.Color3f;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;

import com.jme3.asset.AssetManager;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;

import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects.DefaultJME3SceneObject;

public class DirectionalLightJME3SceneObject extends DefaultJME3SceneObject<ca.gc.asc_csa.apogy.common.topology.addons.primitives.DirectionalLight> 
{
	private Adapter adapter;	
	public static float DEFAULT_POINT_LIGHT_RADIUS = 10.0f;
		
	@SuppressWarnings("unused")
	private AssetManager assetManager;
	
	private DirectionalLight directionalLight;
	
	public DirectionalLightJME3SceneObject(ca.gc.asc_csa.apogy.common.topology.addons.primitives.DirectionalLight topologyNode, JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{
		super(topologyNode, jme3RenderEngineDelegate);
														
		this.assetManager = jme3Application.getAssetManager();
			
		requestUpdate();
				
		topologyNode.eAdapters().add(getAdapter());		
	}		
	
	@Override
	public void updateGeometry(float tpf) 
	{
		if(directionalLight == null) getDirectionalLight();
		
		if(getTopologyNode().isEnabled())
		{
			jme3Application.getRootNode().addLight(directionalLight);
		}
		else
		{
			jme3Application.getRootNode().removeLight(directionalLight);	
		}
	}
	
	@Override
	public void setVisible(final boolean visible) 
	{	
		super.setVisible(visible);
		
		if(directionalLight != null)
		{
			jme3Application.enqueue(new Callable<Object>() 
			{
				@Override
				public Object call() throws Exception 
				{
					if(visible)
					{
						if(!jme3Application.getRootNode().getChildren().contains(directionalLight))
						{
							jme3Application.getRootNode().addLight(directionalLight);
						}
					}
					else
					{
						jme3Application.getRootNode().removeLight(directionalLight);	
					}
					
					return null;
				}
			});
		}		
	}
	
	@Override
	public void dispose() 
	{
		if(getTopologyNode() != null)
		{
			getTopologyNode().eAdapters().remove(getAdapter());
		}
				
		if(directionalLight != null)
		{
			jme3Application.enqueue(new Callable<Object>() 
			{
				@Override
				public Object call() throws Exception 
				{
					jme3Application.getRootNode().removeLight(directionalLight);
					return null;
				}
			});
		}
		super.dispose();
	}
	
	public void setLightDirection(final Tuple3d direction)
	{
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				// TODO						
				return null;
			}
		});
	}
	
	public void setLightColor(Color3f color)
	{
		final ColorRGBA lightColor = JME3Utilities.convertToColorRGBA(color);
		
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				getDirectionalLight().setColor(lightColor);								
				return null;
			}
		});
	}
	
	private DirectionalLight getDirectionalLight()
	{
		if(directionalLight == null)
		{
			directionalLight = new DirectionalLight();
			directionalLight.setColor(ColorRGBA.White);
			directionalLight.setDirection(new Vector3f(0, 0, -1));		
			
			if(getTopologyNode().getNodeId() != null) directionalLight.setName(getTopologyNode().getNodeId());
		}
		
		return directionalLight;
	}				

	private Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
			@Override
			public void notifyChanged(Notification msg) 
			{			
				if(msg.getNotifier() instanceof ca.gc.asc_csa.apogy.common.topology.addons.primitives.PointLight)
				{
					int featureID =  msg.getFeatureID(ca.gc.asc_csa.apogy.common.topology.addons.primitives.PointLight.class);
					switch (featureID) 
					{
						case ApogyCommonTopologyAddonsPrimitivesPackage.LIGHT__ENABLED:						
							setVisible(msg.getNewBooleanValue());
						break;
						
						case ApogyCommonTopologyAddonsPrimitivesPackage.DIRECTIONAL_LIGHT__DIRECTION:
							if(msg.getNewFloatValue() >= 0)
							{
								if(msg.getNewValue() instanceof Tuple3d)
								{
									Tuple3d direction = (Tuple3d) msg.getNewValue();
									setLightDirection(direction);
								}
							}
						break;
						
						case ApogyCommonTopologyAddonsPrimitivesPackage.LIGHT__COLOR:
							if(msg.getNewValue() instanceof Color3f)
							{
								setLightColor((Color3f) msg.getNewValue());
							}
						break;
		

					default:
						break;
					}
				}
			}	
			};
		}
		return adapter;
	}
}

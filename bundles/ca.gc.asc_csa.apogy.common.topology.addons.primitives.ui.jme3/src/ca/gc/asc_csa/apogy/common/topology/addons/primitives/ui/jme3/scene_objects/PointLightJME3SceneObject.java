package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.jme3.scene_objects;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.concurrent.Callable;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;

import com.jme3.app.state.AbstractAppState;
import com.jme3.asset.AssetManager;
import com.jme3.light.PointLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects.DefaultJME3SceneObject;

public class PointLightJME3SceneObject extends DefaultJME3SceneObject<ca.gc.asc_csa.apogy.common.topology.addons.primitives.PointLight> 
{
	private Adapter adapter;
	private UpdateAppState updateAppState = null;		
	public static float DEFAULT_POINT_LIGHT_RADIUS = 10.0f;
		
	@SuppressWarnings("unused")
	private AssetManager assetManager;
	
	private PointLight pointLight;
	
	public PointLightJME3SceneObject(ca.gc.asc_csa.apogy.common.topology.addons.primitives.PointLight topologyNode, JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{
		super(topologyNode, jme3RenderEngineDelegate);
														
		this.assetManager = jme3Application.getAssetManager();
		
		jme3Application.getStateManager().attach(getUpdateAppState());
		
		requestUpdate();
				
		topologyNode.eAdapters().add(getAdapter());		
	}		
	
	@Override
	public void updateGeometry(float tpf) 
	{
		if(pointLight == null) getPointLight();
		
		if(getTopologyNode().isEnabled())
		{
			jme3Application.getRootNode().addLight(pointLight);
		}
		else
		{
			jme3Application.getRootNode().removeLight(pointLight);	
		}
	}
	
	@Override
	public void setVisible(final boolean visible) 
	{	
		super.setVisible(visible);
		
		System.out.println("PointLightJME3SceneObject.setVisible() " + getTopologyNode().getNodeId());
		
		if(pointLight != null)
		{
			jme3Application.enqueue(new Callable<Object>() 
			{
				@Override
				public Object call() throws Exception 
				{
					if(visible)
					{
						if(!jme3Application.getRootNode().getChildren().contains(pointLight))
						{
							jme3Application.getRootNode().addLight(pointLight);
						}
					}
					else
					{
						jme3Application.getRootNode().removeLight(pointLight);	
					}
					
					return null;
				}
			});
		}		
	}
	
	@Override
	public void dispose() 
	{
		if(getTopologyNode() != null)
		{
			getTopologyNode().eAdapters().remove(getAdapter());
		}
		
		jme3Application.getStateManager().detach(getUpdateAppState());
		
		if(pointLight != null)
		{
			jme3Application.enqueue(new Callable<Object>() 
			{
				@Override
				public Object call() throws Exception 
				{
					jme3Application.getRootNode().removeLight(pointLight);
					return null;
				}
			});
		}
		super.dispose();
	}
	
	public void setLightRadius(final float radius)
	{
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				getPointLight().setRadius(radius);							
				return null;
			}
		});
	}
	
	public void setLightColor(Color3f color)
	{
		final ColorRGBA lightColor = JME3Utilities.convertToColorRGBA(color);
		
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				getPointLight().setColor(lightColor);								
				return null;
			}
		});
	}
		
	private PointLight getPointLight()
	{
		if(pointLight == null)
		{
			pointLight = new PointLight();
			pointLight.setColor(ColorRGBA.White);
			pointLight.setRadius(getTopologyNode().getRadius());
			
			Vector3f lightPosition = new Vector3f();
			if(getTopologyNode() != null)
			{
				Matrix4d m = ApogyCommonTopologyFacade.INSTANCE.expressNodeInRootFrame(getTopologyNode());
				Vector3d position = new Vector3d();
				m.get(position);
								
				lightPosition = new Vector3f((float) position.x, (float) position.y, (float) position.z);
			}
			pointLight.setPosition(lightPosition);	
			
			if(getTopologyNode().getNodeId() != null) pointLight.setName(getTopologyNode().getNodeId());
		}
		
		return pointLight;
	}				

	private Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
			@Override
			public void notifyChanged(Notification msg) 
			{			
				if(msg.getNotifier() instanceof ca.gc.asc_csa.apogy.common.topology.addons.primitives.PointLight)
				{
					int featureID =  msg.getFeatureID(ca.gc.asc_csa.apogy.common.topology.addons.primitives.PointLight.class);
					switch (featureID) 
					{
						case ApogyCommonTopologyAddonsPrimitivesPackage.LIGHT__ENABLED:						
							setVisible(msg.getNewBooleanValue());
						break;
						
						case ApogyCommonTopologyAddonsPrimitivesPackage.POINT_LIGHT__RADIUS:
							if(msg.getNewFloatValue() >= 0)
							{
								setLightRadius(msg.getNewFloatValue());
							}
						break;
						
						case ApogyCommonTopologyAddonsPrimitivesPackage.LIGHT__COLOR:
							if(msg.getNewValue() instanceof Color3f)
							{
								setLightColor((Color3f) msg.getNewValue());
							}
						break;
		

					default:
						break;
					}
				}
			}	
			};
		}
		return adapter;
	}
	
	protected UpdateAppState getUpdateAppState() 
	{
		if(updateAppState == null)
		{
			updateAppState = new UpdateAppState();
		}
		return updateAppState;
	}
	
	
	private class UpdateAppState extends AbstractAppState
	{
		private Node root = ApogyCommonTopologyFacade.INSTANCE.findRoot(getTopologyNode());	
		
		public UpdateAppState() {			
		}
		
		@Override
		public void update(float tpf) 
		{						
			Matrix4d newPose = ApogyCommonTopologyFacade.INSTANCE.expressInFrame(getTopologyNode(), root);									
			if(newPose != null)
			{
				if(newPose != null)
				{	
					Vector3d pos = new Vector3d();
					newPose.get(pos);									
					getPointLight().setPosition(new Vector3f((float) pos.x, (float) pos.y, (float) pos.z));									
				}																										
			}	
			
			super.update(tpf);
		}
	}
}

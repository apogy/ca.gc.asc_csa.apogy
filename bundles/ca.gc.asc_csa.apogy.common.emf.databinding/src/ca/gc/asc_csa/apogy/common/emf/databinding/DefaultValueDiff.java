package ca.gc.asc_csa.apogy.common.emf.databinding;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.core.databinding.observable.value.ValueDiff;

public class DefaultValueDiff extends ValueDiff 
{
	private Object oldValue;
	private Object newValue;
	
	public DefaultValueDiff(Object oldValue, Object newValue)
	{
		this.oldValue = oldValue;
		this.newValue = newValue;
	}
	
	@Override
	public Object getOldValue() 
	{	
		return oldValue;
	}
	@Override
	public Object getNewValue() 
	{	
		return newValue;
	}
}

package ca.gc.asc_csa.apogy.addons.sensors.pose;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Facade for Pose Sensor.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.pose.ApogyAddonsSensorsPosePackage#getApogyAddonsSensorsPoseFacade()
 * @model
 * @generated
 */
public interface ApogyAddonsSensorsPoseFacade extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CSVDataLogger with a specified destination file path.
	 * @param fileName The file path of the file to use for logging.
	 * @return The CSVDataLogger.
	 * <!-- end-model-doc -->
	 * @model unique="false" fileNameUnique="false"
	 * @generated
	 */
	CSVDataLogger createCSVDataLogger(String fileName);

} // ApogyAddonsSensorsPoseFacade

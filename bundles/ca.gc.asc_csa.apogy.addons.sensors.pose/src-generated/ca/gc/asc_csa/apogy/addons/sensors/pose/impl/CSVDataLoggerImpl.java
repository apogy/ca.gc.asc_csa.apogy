package ca.gc.asc_csa.apogy.addons.sensors.pose.impl;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.addons.sensors.pose.Activator;
import ca.gc.asc_csa.apogy.addons.sensors.pose.ApogyAddonsSensorsPosePackage;
import ca.gc.asc_csa.apogy.addons.sensors.pose.CSVDataLogger;
import ca.gc.asc_csa.apogy.common.geometry.data3d.Pose;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>CSV Data Logger</b></em>'. <!-- end-user-doc -->
 *
 * @generated
 */
public class CSVDataLoggerImpl extends PoseDataLoggerImpl implements CSVDataLogger 
{
	private BufferedWriter csvWriter = null;
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CSVDataLoggerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsSensorsPosePackage.Literals.CSV_DATA_LOGGER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void setOutputFile(String newOutputFile) 
	{
		String oldOutputFile = outputFile;
		outputFile = newOutputFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ApogyAddonsSensorsPosePackage.CSV_DATA_LOGGER__OUTPUT_FILE, oldOutputFile,
					outputFile));

		if (csvWriter != null)
		{
			try
			{
				csvWriter.flush();
				csvWriter.close();
				csvWriter = null;
			}
			catch (Exception e) 
			{
				Logger.INSTANCE.log(Activator.ID, "Failed to open file <" + " for output!", EventSeverity.ERROR);
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void logPose(Pose pose) throws IOException 
	{
		double tx = pose.getX();
		double ty = pose.getY();
		double tz = pose.getZ();

		double rx = pose.getXRotation();
		double ry = pose.getYRotation();
		double rz = pose.getZRotation();

		long timeStamp = System.currentTimeMillis();

		String line = Long.toString(timeStamp) + "," + Double.toString(tx) + "," + Double.toString(ty) + "," +   Double.toString(tz) + ",";
		line += Double.toString(rx) + "," + Double.toString(ry) + "," + Double.toString(rz) + "\n";

		getCsvWriter().write(line);
	}

	private BufferedWriter getCsvWriter() throws IOException 
	{
		if (csvWriter == null) 
		{			
			csvWriter = new BufferedWriter(new FileWriter(getOutputFile()));			
			String header =  "Time stam,tx,ty,tz,rx,ry,rz \n"; 			
			csvWriter.write(header);
			
			Logger.INSTANCE.log(Activator.ID, "Sucessfully openned file <" + " for output!", EventSeverity.INFO);
		}

		return csvWriter;

	}

	@Override
	protected void finalize() throws Throwable 
	{
		if (csvWriter != null) 
		{
			csvWriter.close();
		}
	}

} // CSVDataLoggerImpl

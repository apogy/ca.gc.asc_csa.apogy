package ca.gc.asc_csa.apogy.core.environment.converters;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.converters.IConverter;
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.TimeSourcesList;

public class ApogyEnvironmentToTimeSourcesListConverter implements IConverter {

	public ApogyEnvironmentToTimeSourcesListConverter() {
	}

	@Override
	public Class<?> getOutputType() {
		return TimeSourcesList.class;
	}

	@Override
	public Class<?> getInputType() {
		return ApogyEnvironment.class;
	}

	@Override
	public boolean canConvert(Object input) {
		return input instanceof ApogyEnvironment;
	}

	@Override
	public Object convert(Object input) throws Exception {
		return ((ApogyEnvironment)input).getTimeSourcesList() ;
	}
}
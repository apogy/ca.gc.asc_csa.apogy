/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.core.environment.impl.WorksitesRegistryImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Worksites Registry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * List of {@link AbstractWorksite}s that are registered in bundles.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.WorksitesRegistry#getTYPE_CONTRIBUTOR_EXTENSION_POINT_ID <em>TYPE CONTRIBUTOR EXTENSION POINT ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.WorksitesRegistry#getTYPE_CONTRIBUTOR_URI_ID <em>TYPE CONTRIBUTOR URI ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.WorksitesRegistry#getWorksites <em>Worksites</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage#getWorksitesRegistry()
 * @model
 * @generated
 */
public interface WorksitesRegistry extends EObject 
{
	/** 
	 * Singleton.
	 * @generated_NOT
	 */
	public WorksitesRegistry INSTANCE = WorksitesRegistryImpl.getInstance();
	
	
	/**
	 * Returns the value of the '<em><b>TYPE CONTRIBUTOR EXTENSION POINT ID</b></em>' attribute.
	 * The default value is <code>"ca.gc.asc_csa.apogy.core.environment.worksiteContributor"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  Extension Point Id.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>TYPE CONTRIBUTOR EXTENSION POINT ID</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage#getWorksitesRegistry_TYPE_CONTRIBUTOR_EXTENSION_POINT_ID()
	 * @model default="ca.gc.asc_csa.apogy.core.environment.worksiteContributor" unique="false" transient="true" changeable="false"
	 * @generated
	 */
	String getTYPE_CONTRIBUTOR_EXTENSION_POINT_ID();

	/**
	 * Returns the value of the '<em><b>TYPE CONTRIBUTOR URI ID</b></em>' attribute.
	 * The default value is <code>"URI"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  URI that refers the type.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>TYPE CONTRIBUTOR URI ID</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage#getWorksitesRegistry_TYPE_CONTRIBUTOR_URI_ID()
	 * @model default="URI" unique="false" transient="true" changeable="false"
	 * @generated
	 */
	String getTYPE_CONTRIBUTOR_URI_ID();

	/**
	 * Returns the value of the '<em><b>Worksites</b></em>' reference list.
	 * The list contents are of type {@link ca.gc.asc_csa.apogy.core.environment.AbstractWorksite}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Contains the bundled {@link AbstractWorksite}s.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Worksites</em>' reference list.
	 * @see ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage#getWorksitesRegistry_Worksites()
	 * @model transient="true" changeable="false" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel children='true' createChild='false' property='None'"
	 * @generated
	 */
	EList<AbstractWorksite> getWorksites();

} // WorksitesRegistry

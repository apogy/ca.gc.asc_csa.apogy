/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.AbstractWorksite;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.WorksitesRegistry;
import ca.gc.asc_csa.apogy.core.invocator.Activator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Worksites Registry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.impl.WorksitesRegistryImpl#getTYPE_CONTRIBUTOR_EXTENSION_POINT_ID <em>TYPE CONTRIBUTOR EXTENSION POINT ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.impl.WorksitesRegistryImpl#getTYPE_CONTRIBUTOR_URI_ID <em>TYPE CONTRIBUTOR URI ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.impl.WorksitesRegistryImpl#getWorksites <em>Worksites</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WorksitesRegistryImpl extends MinimalEObjectImpl.Container implements WorksitesRegistry 
{
	/**
	 * @generated_NOT
	 */
	private static WorksitesRegistry instance = null;

	/**
	 * @generated_NOT
	 */
	public static WorksitesRegistry getInstance() {
		if (instance == null) 
		{
			instance = new WorksitesRegistryImpl();
		}
		return instance;
	}
	
	
	/**
	 * The default value of the '{@link #getTYPE_CONTRIBUTOR_EXTENSION_POINT_ID() <em>TYPE CONTRIBUTOR EXTENSION POINT ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE_CONTRIBUTOR_EXTENSION_POINT_ID()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_CONTRIBUTOR_EXTENSION_POINT_ID_EDEFAULT = "ca.gc.asc_csa.apogy.core.environment.worksiteContributor";

	/**
	 * The cached value of the '{@link #getTYPE_CONTRIBUTOR_EXTENSION_POINT_ID() <em>TYPE CONTRIBUTOR EXTENSION POINT ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE_CONTRIBUTOR_EXTENSION_POINT_ID()
	 * @generated
	 * @ordered
	 */
	protected String typE_CONTRIBUTOR_EXTENSION_POINT_ID = TYPE_CONTRIBUTOR_EXTENSION_POINT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getTYPE_CONTRIBUTOR_URI_ID() <em>TYPE CONTRIBUTOR URI ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE_CONTRIBUTOR_URI_ID()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_CONTRIBUTOR_URI_ID_EDEFAULT = "URI";

	/**
	 * The cached value of the '{@link #getTYPE_CONTRIBUTOR_URI_ID() <em>TYPE CONTRIBUTOR URI ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE_CONTRIBUTOR_URI_ID()
	 * @generated
	 * @ordered
	 */
	protected String typE_CONTRIBUTOR_URI_ID = TYPE_CONTRIBUTOR_URI_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getWorksites() <em>Worksites</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorksites()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractWorksite> worksites;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WorksitesRegistryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreEnvironmentPackage.Literals.WORKSITES_REGISTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTYPE_CONTRIBUTOR_EXTENSION_POINT_ID() {
		return typE_CONTRIBUTOR_EXTENSION_POINT_ID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTYPE_CONTRIBUTOR_URI_ID() {
		return typE_CONTRIBUTOR_URI_ID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractWorksite> getWorksitesGen() {
		if (worksites == null) {
			worksites = new EObjectResolvingEList<AbstractWorksite>(AbstractWorksite.class, this, ApogyCoreEnvironmentPackage.WORKSITES_REGISTRY__WORKSITES);
		}
		return worksites;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EList<AbstractWorksite> getWorksites() 
	{
		if (worksites == null) 
		{
			worksites = getWorksitesGen();
			List<AbstractWorksite> list = new ArrayList<AbstractWorksite>();

			IExtensionPoint extensionPoint = Platform
					.getExtensionRegistry()
					.getExtensionPoint(
					getTYPE_CONTRIBUTOR_EXTENSION_POINT_ID());
			IConfigurationElement[] contributors = extensionPoint.getConfigurationElements();

			for (int i = 0; i < contributors.length; i++) 
			{
				IConfigurationElement contributor = contributors[i];
				String uri_str = contributor
						.getAttribute(getTYPE_CONTRIBUTOR_URI_ID());

				URI typeURI = URI.createURI(
						"platform:/plugin/"
								+ contributor.getNamespaceIdentifier() + "/"
								+ uri_str, true);

				try 
				{
					TransactionalEditingDomain domain = ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain();
					Resource resource = domain.getResourceSet().getResource(typeURI,true);
					resource.load(Collections.EMPTY_MAP);
					EObject content = resource.getContents().get(0);

					if (content instanceof AbstractWorksite) 
					{
						AbstractWorksite worksite = (AbstractWorksite) content;
						list.add(worksite);
					} 
					else 
					{
						Logger.INSTANCE.log(Activator.ID, typeURI.toString() + " does not contain a valid Apogy Worksite model.", EventSeverity.ERROR);
					}

				} 
				catch (Throwable t) 
				{
					t.printStackTrace();
					Logger.INSTANCE.log(Activator.ID, "Unable to load " + typeURI.toString(), EventSeverity.ERROR);
				}
			}
			
			worksites.addAll(list);
		}
		return worksites;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreEnvironmentPackage.WORKSITES_REGISTRY__TYPE_CONTRIBUTOR_EXTENSION_POINT_ID:
				return getTYPE_CONTRIBUTOR_EXTENSION_POINT_ID();
			case ApogyCoreEnvironmentPackage.WORKSITES_REGISTRY__TYPE_CONTRIBUTOR_URI_ID:
				return getTYPE_CONTRIBUTOR_URI_ID();
			case ApogyCoreEnvironmentPackage.WORKSITES_REGISTRY__WORKSITES:
				return getWorksites();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentPackage.WORKSITES_REGISTRY__TYPE_CONTRIBUTOR_EXTENSION_POINT_ID:
				return TYPE_CONTRIBUTOR_EXTENSION_POINT_ID_EDEFAULT == null ? typE_CONTRIBUTOR_EXTENSION_POINT_ID != null : !TYPE_CONTRIBUTOR_EXTENSION_POINT_ID_EDEFAULT.equals(typE_CONTRIBUTOR_EXTENSION_POINT_ID);
			case ApogyCoreEnvironmentPackage.WORKSITES_REGISTRY__TYPE_CONTRIBUTOR_URI_ID:
				return TYPE_CONTRIBUTOR_URI_ID_EDEFAULT == null ? typE_CONTRIBUTOR_URI_ID != null : !TYPE_CONTRIBUTOR_URI_ID_EDEFAULT.equals(typE_CONTRIBUTOR_URI_ID);
			case ApogyCoreEnvironmentPackage.WORKSITES_REGISTRY__WORKSITES:
				return worksites != null && !worksites.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (TYPE_CONTRIBUTOR_EXTENSION_POINT_ID: ");
		result.append(typE_CONTRIBUTOR_EXTENSION_POINT_ID);
		result.append(", TYPE_CONTRIBUTOR_URI_ID: ");
		result.append(typE_CONTRIBUTOR_URI_ID);
		result.append(')');
		return result.toString();
	}

} //WorksitesRegistryImpl

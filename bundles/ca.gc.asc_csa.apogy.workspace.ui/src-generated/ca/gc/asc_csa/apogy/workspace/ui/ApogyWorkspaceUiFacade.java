/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.workspace.ui;

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.workspace.ui.impl.ApogyWorkspaceUiFacadeImpl;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>Facade</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Apogy Workspace Ui Facade.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.workspace.ui.ApogyWorkspaceUiPackage#getApogyWorkspaceUiFacade()
 * @model
 * @generated
 */
public interface ApogyWorkspaceUiFacade extends EObject {

	/**
	 * @generated_NOT
	 */
	public static ApogyWorkspaceUiFacade INSTANCE = ApogyWorkspaceUiFacadeImpl.getInstance();
} // ApogyWorkspaceUiFacade

package ca.gc.asc_csa.apogy.workspace.ui;

public class ApogyWorkspaceRCPConstants {

	/**
	 * Bottom trimBar 
	 */
	public static final String TRIMBAR__BOTTOM__ID = "ca.gc.asc_csa.apogy.rcp.trimbar.bottom";
	
}

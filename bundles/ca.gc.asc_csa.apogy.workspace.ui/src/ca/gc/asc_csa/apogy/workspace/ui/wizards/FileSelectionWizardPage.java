package ca.gc.asc_csa.apogy.workspace.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class FileSelectionWizardPage extends WizardPage 
{
	private String[] fileExtensions = null;	
	private String currentDir = System.getProperty("user.dir");
	private String selectedFileName = null;
	
	public FileSelectionWizardPage(String pageName) 
	{
		super(pageName);	
	}
	
	public FileSelectionWizardPage(String pageName, String[] fileExtensions) 
	{
		super(pageName);	
		this.fileExtensions = fileExtensions;
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));
		System.out.println(parent.getLayout());
				
		Label label = new Label(composite, SWT.NONE);
		label.setText("File Path :");		
		
		Text text = new Text(composite, SWT.BORDER | SWT.READ_ONLY);		
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		Button button = new Button(composite, SWT.PUSH);
		button.setText("Select File ...");
		
		button.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{				
				FileDialog fileChooser = new FileDialog(getShell(), SWT.OPEN);
				fileChooser.setText("Select a file:");
				fileChooser.setFilterPath(currentDir);
				
				String[] extensions = fileExtensions;
				if(extensions == null || extensions.length == 0)
				{
					extensions = new String[]{"*.*"};
				}
				fileChooser.setFilterExtensions(fileExtensions);		
				String filename = fileChooser.open();
				
				// Adds the file prefix
				if(filename != null)
				{
					selectedFileName = filename;
					text.setText(selectedFileName);
					text.setToolTipText(selectedFileName);
					
					fileSelected(selectedFileName);
					
					setPageComplete(true);
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {	
			}
		});
		
		setControl(composite);
	}
	
	public String getSelectedFileName() 
	{
		return selectedFileName;
	}
	
	/**
	 * Method called when a valid path is selected. Can be overloaded by users.
	 * @param selectedFilePath The newly selected file path.
	 */
	protected void fileSelected(String selectedFilePath)
	{
	}
	
	@Override
	public boolean isPageComplete() 
	{	
		return (selectedFileName != null);
	}
}

package ca.gc.asc_csa.apogy.core.environment.orbit.earth;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nadir Pointing Attitude Provider</b></em>'.
 * <!-- end-user-doc --> *
 * <!-- begin-model-doc -->
 * This class represents the attitude provider where the satellite z axis is pointing to the vertical of the ground point under satellite.
 * It is backed by a org.orekit.attitudes.NadirPointing AttitudeProvider.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthPackage#getNadirPointingAttitudeProvider()
 * @model
 * @generated
 */
public interface NadirPointingAttitudeProvider extends OreKitBackedAttitudeProvider {
} // NadirPointingAttitudeProvider

package ca.gc.asc_csa.apogy.core.environment.orbit.earth.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import ca.gc.asc_csa.apogy.core.environment.Earth;
import ca.gc.asc_csa.apogy.core.environment.Moon;
import ca.gc.asc_csa.apogy.core.environment.impl.SkyImpl;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.EarthOrbitSky;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.EarthOrbitWorksite;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Orbit Sky</b></em>'.
 * <!-- end-user-doc --> * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.impl.EarthOrbitSkyImpl#getEarthOrbitWorksite <em>Earth Orbit Worksite</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.impl.EarthOrbitSkyImpl#getMoon <em>Moon</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.impl.EarthOrbitSkyImpl#getEarth <em>Earth</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EarthOrbitSkyImpl extends SkyImpl implements EarthOrbitSky {
	/**
	 * The cached value of the '{@link #getEarthOrbitWorksite() <em>Earth Orbit Worksite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getEarthOrbitWorksite()
	 * @generated
	 * @ordered
	 */
	protected EarthOrbitWorksite earthOrbitWorksite;

	/**
	 * The cached value of the '{@link #getMoon() <em>Moon</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getMoon()
	 * @generated
	 * @ordered
	 */
	protected Moon moon;

	/**
	 * The cached value of the '{@link #getEarth() <em>Earth</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getEarth()
	 * @generated
	 * @ordered
	 */
	protected Earth earth;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected EarthOrbitSkyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreEnvironmentOrbitEarthPackage.Literals.EARTH_ORBIT_SKY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EarthOrbitWorksite getEarthOrbitWorksite() {
		if (earthOrbitWorksite != null && earthOrbitWorksite.eIsProxy()) {
			InternalEObject oldEarthOrbitWorksite = (InternalEObject)earthOrbitWorksite;
			earthOrbitWorksite = (EarthOrbitWorksite)eResolveProxy(oldEarthOrbitWorksite);
			if (earthOrbitWorksite != oldEarthOrbitWorksite) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__EARTH_ORBIT_WORKSITE, oldEarthOrbitWorksite, earthOrbitWorksite));
			}
		}
		return earthOrbitWorksite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EarthOrbitWorksite basicGetEarthOrbitWorksite() {
		return earthOrbitWorksite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setEarthOrbitWorksite(EarthOrbitWorksite newEarthOrbitWorksite) {
		EarthOrbitWorksite oldEarthOrbitWorksite = earthOrbitWorksite;
		earthOrbitWorksite = newEarthOrbitWorksite;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__EARTH_ORBIT_WORKSITE, oldEarthOrbitWorksite, earthOrbitWorksite));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Moon getMoon() {
		if (moon != null && moon.eIsProxy()) {
			InternalEObject oldMoon = (InternalEObject)moon;
			moon = (Moon)eResolveProxy(oldMoon);
			if (moon != oldMoon) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__MOON, oldMoon, moon));
			}
		}
		return moon;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Moon basicGetMoon() {
		return moon;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Earth getEarth() {
		if (earth != null && earth.eIsProxy()) {
			InternalEObject oldEarth = (InternalEObject)earth;
			earth = (Earth)eResolveProxy(oldEarth);
			if (earth != oldEarth) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__EARTH, oldEarth, earth));
			}
		}
		return earth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Earth basicGetEarth() {
		return earth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public double getMoonAngularDiameter() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public double getEarthAngularDiameter() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__EARTH_ORBIT_WORKSITE:
				if (resolve) return getEarthOrbitWorksite();
				return basicGetEarthOrbitWorksite();
			case ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__MOON:
				if (resolve) return getMoon();
				return basicGetMoon();
			case ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__EARTH:
				if (resolve) return getEarth();
				return basicGetEarth();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__EARTH_ORBIT_WORKSITE:
				setEarthOrbitWorksite((EarthOrbitWorksite)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__EARTH_ORBIT_WORKSITE:
				setEarthOrbitWorksite((EarthOrbitWorksite)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__EARTH_ORBIT_WORKSITE:
				return earthOrbitWorksite != null;
			case ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__MOON:
				return moon != null;
			case ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY__EARTH:
				return earth != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY___GET_MOON_ANGULAR_DIAMETER:
				return getMoonAngularDiameter();
			case ApogyCoreEnvironmentOrbitEarthPackage.EARTH_ORBIT_SKY___GET_EARTH_ANGULAR_DIAMETER:
				return getEarthAngularDiameter();
		}
		return super.eInvoke(operationID, arguments);
	}

} //EarthOrbitSkyImpl

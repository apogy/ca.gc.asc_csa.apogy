/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *      Regent L'Archeveque
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.provider;


import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;

import ca.gc.asc_csa.apogy.addons.provider.AbstractTwoPoints3DToolItemProvider;

import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FieldOfViewEntry3DToolItemProvider extends AbstractTwoPoints3DToolItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfViewEntry3DToolItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTargetVisibilityPropertyDescriptor(object);
			addFovPropertyDescriptor(object);
			addDefaultVectorColorPropertyDescriptor(object);
			addVectorColorSelectionVisiblePropertyDescriptor(object);
			addVectorColorSelectionEnteredPropertyDescriptor(object);
			addMaximumVectorLengthPropertyDescriptor(object);
			addVectorDiameterPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Target Visibility feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetVisibilityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FieldOfViewEntry3DTool_targetVisibility_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FieldOfViewEntry3DTool_targetVisibility_feature", "_UI_FieldOfViewEntry3DTool_type"),
				 ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Fov feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFovPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FieldOfViewEntry3DTool_fov_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FieldOfViewEntry3DTool_fov_feature", "_UI_FieldOfViewEntry3DTool_type"),
				 ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__FOV,
				 false,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Default Vector Color feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDefaultVectorColorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FieldOfViewEntry3DTool_defaultVectorColor_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FieldOfViewEntry3DTool_defaultVectorColor_feature", "_UI_FieldOfViewEntry3DTool_type"),
				 ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__DEFAULT_VECTOR_COLOR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Vector Color Selection Visible feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVectorColorSelectionVisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FieldOfViewEntry3DTool_vectorColorSelectionVisible_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FieldOfViewEntry3DTool_vectorColorSelectionVisible_feature", "_UI_FieldOfViewEntry3DTool_type"),
				 ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_VISIBLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Vector Color Selection Entered feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVectorColorSelectionEnteredPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FieldOfViewEntry3DTool_vectorColorSelectionEntered_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FieldOfViewEntry3DTool_vectorColorSelectionEntered_feature", "_UI_FieldOfViewEntry3DTool_type"),
				 ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_ENTERED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Maximum Vector Length feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMaximumVectorLengthPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FieldOfViewEntry3DTool_maximumVectorLength_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FieldOfViewEntry3DTool_maximumVectorLength_feature", "_UI_FieldOfViewEntry3DTool_type"),
				 ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Vector Diameter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVectorDiameterPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FieldOfViewEntry3DTool_vectorDiameter_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FieldOfViewEntry3DTool_vectorDiameter_feature", "_UI_FieldOfViewEntry3DTool_type"),
				 ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_DIAMETER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns FieldOfViewEntry3DTool.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/FieldOfViewEntry3DTool"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	@Override
	public String getText(Object object) 
	{
		FieldOfViewEntry3DTool fieldOfViewEntry3DTool = (FieldOfViewEntry3DTool) object;
		
		String label = null;		
		if(fieldOfViewEntry3DTool.getName() != null && fieldOfViewEntry3DTool.getName().length() > 0)
		{
			label = fieldOfViewEntry3DTool.getName();
		}
		else
		{
			label = getString("_UI_FieldOfViewEntry3DTool_type");
		}
		
		label += " (";
		String simpleToolText = getSimple3DToolText(fieldOfViewEntry3DTool);
		if(simpleToolText.length() > 0)
		{
			label += simpleToolText;
		}		
				
		// Adds the visible, inside status.
		label += "," + fieldOfViewEntry3DTool.getTargetVisibility().getLiteral();

		// Adds lock Status.
		String lockText = getAbstractTwoPoints3DToolText(fieldOfViewEntry3DTool); 
		if(lockText.length() > 0)
		{
			label += ", " + lockText;
		}
			
		label += ")";
		
		return label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FieldOfViewEntry3DTool.class)) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY:
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FOV:
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE:
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR:
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__DEFAULT_VECTOR_COLOR:
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_VISIBLE:
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_ENTERED:
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH:
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_DIAMETER:
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_LENGTH:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION ||
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION ||
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION ||
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION ||
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH ||
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}

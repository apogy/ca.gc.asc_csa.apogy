/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui;

import ca.gc.asc_csa.apogy.core.environment.earth.HorizontalCoordinates;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earth Orbiting Spacecraft Location Tool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Tool used to display a spacecraft location as viewed from a surface worksite.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#isShowVector <em>Show Vector</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#getSpacecraftPosition <em>Spacecraft Position</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#getEarthOrbitingSpacecraftLocationToolNode <em>Earth Orbiting Spacecraft Location Tool Node</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitingSpacecraftLocationTool()
 * @model
 * @generated
 */
public interface EarthOrbitingSpacecraftLocationTool extends EarthOrbitModelTool {
	/**
	 * Returns the value of the '<em><b>Show Vector</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Whether or not to show the vector from the worksite origin to the spacecraft.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Show Vector</em>' attribute.
	 * @see #setShowVector(boolean)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitingSpacecraftLocationTool_ShowVector()
	 * @model default="true" unique="false"
	 * @generated
	 */
	boolean isShowVector();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#isShowVector <em>Show Vector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Vector</em>' attribute.
	 * @see #isShowVector()
	 * @generated
	 */
	void setShowVector(boolean value);

	/**
	 * Returns the value of the '<em><b>Spacecraft Position</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The spacecraft location relative to the worksite.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Spacecraft Position</em>' reference.
	 * @see #setSpacecraftPosition(HorizontalCoordinates)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitingSpacecraftLocationTool_SpacecraftPosition()
	 * @model transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Readonly' children='true' notify='true'"
	 * @generated
	 */
	HorizontalCoordinates getSpacecraftPosition();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#getSpacecraftPosition <em>Spacecraft Position</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Spacecraft Position</em>' reference.
	 * @see #getSpacecraftPosition()
	 * @generated
	 */
	void setSpacecraftPosition(HorizontalCoordinates value);

	/**
	 * Returns the value of the '<em><b>Earth Orbiting Spacecraft Location Tool Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode#getEarthOrbitingSpacecraftLocationTool <em>Earth Orbiting Spacecraft Location Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The node representing this tool in the topology.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Earth Orbiting Spacecraft Location Tool Node</em>' reference.
	 * @see #setEarthOrbitingSpacecraftLocationToolNode(EarthOrbitingSpacecraftLocationToolNode)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitingSpacecraftLocationTool_EarthOrbitingSpacecraftLocationToolNode()
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode#getEarthOrbitingSpacecraftLocationTool
	 * @model opposite="earthOrbitingSpacecraftLocationTool" transient="true"
	 * @generated
	 */
	EarthOrbitingSpacecraftLocationToolNode getEarthOrbitingSpacecraftLocationToolNode();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#getEarthOrbitingSpacecraftLocationToolNode <em>Earth Orbiting Spacecraft Location Tool Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Earth Orbiting Spacecraft Location Tool Node</em>' reference.
	 * @see #getEarthOrbitingSpacecraftLocationToolNode()
	 * @generated
	 */
	void setEarthOrbitingSpacecraftLocationToolNode(EarthOrbitingSpacecraftLocationToolNode value);

} // EarthOrbitingSpacecraftLocationTool

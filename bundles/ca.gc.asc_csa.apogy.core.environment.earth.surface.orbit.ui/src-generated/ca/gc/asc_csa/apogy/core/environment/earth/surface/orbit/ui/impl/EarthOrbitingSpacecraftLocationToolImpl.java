/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.topology.GroupNode;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.EarthSurfaceLocation;
import ca.gc.asc_csa.apogy.core.environment.earth.HorizontalCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthFacade;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.EarthOrbitModel;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.TLE;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Orbiting Spacecraft Location Tool</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolImpl#isShowVector <em>Show Vector</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolImpl#getSpacecraftPosition <em>Spacecraft Position</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolImpl#getEarthOrbitingSpacecraftLocationToolNode <em>Earth Orbiting Spacecraft Location Tool Node</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EarthOrbitingSpacecraftLocationToolImpl extends EarthOrbitModelToolImpl implements EarthOrbitingSpacecraftLocationTool 
{
	/**
	 * The default value of the '{@link #isShowVector() <em>Show Vector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowVector()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SHOW_VECTOR_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isShowVector() <em>Show Vector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowVector()
	 * @generated
	 * @ordered
	 */
	protected boolean showVector = SHOW_VECTOR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSpacecraftPosition() <em>Spacecraft Position</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpacecraftPosition()
	 * @generated
	 * @ordered
	 */
	protected HorizontalCoordinates spacecraftPosition;

	private EarthSurfaceLocation earthSurfaceLocation;
	
	/**
	 * The cached value of the '{@link #getEarthOrbitingSpacecraftLocationToolNode() <em>Earth Orbiting Spacecraft Location Tool Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEarthOrbitingSpacecraftLocationToolNode()
	 * @generated
	 * @ordered
	 */
	protected EarthOrbitingSpacecraftLocationToolNode earthOrbitingSpacecraftLocationToolNode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EarthOrbitingSpacecraftLocationToolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isShowVector() {
		return showVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowVector(boolean newShowVector) {
		boolean oldShowVector = showVector;
		showVector = newShowVector;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SHOW_VECTOR, oldShowVector, showVector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HorizontalCoordinates getSpacecraftPosition() {
		if (spacecraftPosition != null && spacecraftPosition.eIsProxy()) {
			InternalEObject oldSpacecraftPosition = (InternalEObject)spacecraftPosition;
			spacecraftPosition = (HorizontalCoordinates)eResolveProxy(oldSpacecraftPosition);
			if (spacecraftPosition != oldSpacecraftPosition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION, oldSpacecraftPosition, spacecraftPosition));
			}
		}
		return spacecraftPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HorizontalCoordinates basicGetSpacecraftPosition() {
		return spacecraftPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpacecraftPosition(HorizontalCoordinates newSpacecraftPosition) {
		HorizontalCoordinates oldSpacecraftPosition = spacecraftPosition;
		spacecraftPosition = newSpacecraftPosition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION, oldSpacecraftPosition, spacecraftPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitingSpacecraftLocationToolNode getEarthOrbitingSpacecraftLocationToolNode() {
		if (earthOrbitingSpacecraftLocationToolNode != null && earthOrbitingSpacecraftLocationToolNode.eIsProxy()) {
			InternalEObject oldEarthOrbitingSpacecraftLocationToolNode = (InternalEObject)earthOrbitingSpacecraftLocationToolNode;
			earthOrbitingSpacecraftLocationToolNode = (EarthOrbitingSpacecraftLocationToolNode)eResolveProxy(oldEarthOrbitingSpacecraftLocationToolNode);
			if (earthOrbitingSpacecraftLocationToolNode != oldEarthOrbitingSpacecraftLocationToolNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE, oldEarthOrbitingSpacecraftLocationToolNode, earthOrbitingSpacecraftLocationToolNode));
			}
		}
		return earthOrbitingSpacecraftLocationToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitingSpacecraftLocationToolNode basicGetEarthOrbitingSpacecraftLocationToolNode() {
		return earthOrbitingSpacecraftLocationToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEarthOrbitingSpacecraftLocationToolNode(EarthOrbitingSpacecraftLocationToolNode newEarthOrbitingSpacecraftLocationToolNode, NotificationChain msgs) {
		EarthOrbitingSpacecraftLocationToolNode oldEarthOrbitingSpacecraftLocationToolNode = earthOrbitingSpacecraftLocationToolNode;
		earthOrbitingSpacecraftLocationToolNode = newEarthOrbitingSpacecraftLocationToolNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE, oldEarthOrbitingSpacecraftLocationToolNode, newEarthOrbitingSpacecraftLocationToolNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEarthOrbitingSpacecraftLocationToolNode(EarthOrbitingSpacecraftLocationToolNode newEarthOrbitingSpacecraftLocationToolNode) {
		if (newEarthOrbitingSpacecraftLocationToolNode != earthOrbitingSpacecraftLocationToolNode) {
			NotificationChain msgs = null;
			if (earthOrbitingSpacecraftLocationToolNode != null)
				msgs = ((InternalEObject)earthOrbitingSpacecraftLocationToolNode).eInverseRemove(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL, EarthOrbitingSpacecraftLocationToolNode.class, msgs);
			if (newEarthOrbitingSpacecraftLocationToolNode != null)
				msgs = ((InternalEObject)newEarthOrbitingSpacecraftLocationToolNode).eInverseAdd(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL, EarthOrbitingSpacecraftLocationToolNode.class, msgs);
			msgs = basicSetEarthOrbitingSpacecraftLocationToolNode(newEarthOrbitingSpacecraftLocationToolNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE, newEarthOrbitingSpacecraftLocationToolNode, newEarthOrbitingSpacecraftLocationToolNode));
	}

	@Override
	public void setEarthOrbitModel(EarthOrbitModel newEarthOrbitModel) 
	{	
		super.setEarthOrbitModel(newEarthOrbitModel);
		
		try
		{
			update();
		}
		catch (Exception e) 
		{
			// TODO
		}				
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE:
				if (earthOrbitingSpacecraftLocationToolNode != null)
					msgs = ((InternalEObject)earthOrbitingSpacecraftLocationToolNode).eInverseRemove(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL, EarthOrbitingSpacecraftLocationToolNode.class, msgs);
				return basicSetEarthOrbitingSpacecraftLocationToolNode((EarthOrbitingSpacecraftLocationToolNode)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE:
				return basicSetEarthOrbitingSpacecraftLocationToolNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SHOW_VECTOR:
				return isShowVector();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION:
				if (resolve) return getSpacecraftPosition();
				return basicGetSpacecraftPosition();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE:
				if (resolve) return getEarthOrbitingSpacecraftLocationToolNode();
				return basicGetEarthOrbitingSpacecraftLocationToolNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SHOW_VECTOR:
				setShowVector((Boolean)newValue);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION:
				setSpacecraftPosition((HorizontalCoordinates)newValue);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE:
				setEarthOrbitingSpacecraftLocationToolNode((EarthOrbitingSpacecraftLocationToolNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SHOW_VECTOR:
				setShowVector(SHOW_VECTOR_EDEFAULT);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION:
				setSpacecraftPosition((HorizontalCoordinates)null);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE:
				setEarthOrbitingSpacecraftLocationToolNode((EarthOrbitingSpacecraftLocationToolNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SHOW_VECTOR:
				return showVector != SHOW_VECTOR_EDEFAULT;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION:
				return spacecraftPosition != null;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE:
				return earthOrbitingSpacecraftLocationToolNode != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (showVector: ");
		result.append(showVector);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EarthOrbitModel getEarthOrbitModel() 
	{
		EarthOrbitModel tmp = super.getEarthOrbitModel();
		
		// DEBUG
		if(tmp == null)
		{
			TLE tle;
			try 
			{
				tle = ApogyCoreEnvironmentOrbitEarthFacade.INSTANCE.loadTLE("platform:/plugin/com.neptec.ogr.simulator/data/ISS.tle");
				tmp = ApogyCoreEnvironmentOrbitEarthFacade.INSTANCE.createTLEEarthOrbitModel(tle);
				tmp.setName("ISS");
				tmp.setDescription("The ISS.");
				
				// ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL, tmp);
								
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		// DEBUG
		
		return tmp;
	}
	
	@Override
	public void setActiveEarthSurfaceWorksite(EarthSurfaceWorksite newActiveEarthSurfaceWorksite) 
	{	
		// Detach node from old sky.
		detachToolNode();
		
		super.setActiveEarthSurfaceWorksite(newActiveEarthSurfaceWorksite);
				
		if(newActiveEarthSurfaceWorksite != null && newActiveEarthSurfaceWorksite.getGeographicalCoordinates() != null)
		{
			earthSurfaceLocation = ApogyEarthEnvironmentFactory.eINSTANCE.createEarthSurfaceLocation();
			earthSurfaceLocation.setLatitude(getActiveEarthSurfaceWorksite().getGeographicalCoordinates().getLatitude());
			earthSurfaceLocation.setLongitude(getActiveEarthSurfaceWorksite().getGeographicalCoordinates().getLongitude());
			earthSurfaceLocation.setElevation(getActiveEarthSurfaceWorksite().getGeographicalCoordinates().getElevation());
		}
		else
		{
			earthSurfaceLocation = null;
		}
		
		// Attaches node to new sky
		attachToolNode();
	}
	
	@Override
	public void setActive(boolean newActive) 
	{	
		super.setActive(newActive);
		
		if(newActive)
		{
			// Forces the tool to update.
			try 
			{
				update();
			} 
			catch (Exception e) 
			{			
				e.printStackTrace();
			}	
		}
	}
	
	@Override
	public void initialise() 
	{
		
		// Initialize the tool node.
		EarthOrbitingSpacecraftLocationToolNode toolNode = ApogyEarthSurfaceOrbitEnvironmentUIFactory.eINSTANCE.createEarthOrbitingSpacecraftLocationToolNode();
		if(getName() != null)
		{
			toolNode.setDescription("Node associated with the EarthOrbitingSpacecraftLocationToo named <" + getName() + ">");
			toolNode.setNodeId("Earth_Orbiting_Spacecraft_Location_Tool_" +  getName().replaceAll(" ", "_"));
		}
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE, toolNode, true);	
	
		
		if(getActiveEarthSurfaceWorksite() != null && getActiveEarthSurfaceWorksite().getGeographicalCoordinates() != null)
		{
			earthSurfaceLocation = ApogyEarthEnvironmentFactory.eINSTANCE.createEarthSurfaceLocation();
			earthSurfaceLocation.setLatitude(getActiveEarthSurfaceWorksite().getGeographicalCoordinates().getLatitude());
			earthSurfaceLocation.setLongitude(getActiveEarthSurfaceWorksite().getGeographicalCoordinates().getLongitude());
			earthSurfaceLocation.setElevation(getActiveEarthSurfaceWorksite().getGeographicalCoordinates().getElevation());						
		}
		else
		{
			earthSurfaceLocation = null;
		}	
		
		super.initialise();
					
	}
	
	@Override
	public void dispose() 
	{
		// Detaches the node representing this tool in the topology.
		detachToolNode();
		
		super.dispose();
	}
	
	@Override
	public void update() throws Exception 
	{
		if(isAutoUpdateEnabled()  && !isUpdating())
		{			
			if(getActiveTimeSource() != null)
			{
				updateTime(getActiveTimeSource().getTime());
			}
		}
	}

	@Override
	public void updateTime(Date newTime) 
	{
		if(getEarthOrbitModel() != null)
		{
			if(isAutoUpdateEnabled())
			{				
				try 
				{
					if(earthSurfaceLocation != null)
					{
						// Propagate the spacecraft position.
						ca.gc.asc_csa.apogy.core.environment.orbit.SpacecraftState spacecraftState = getEarthOrbitModel().propagate(newTime);
					
						// Converts the position in azimuth, elevation and range.					
						double elevationAngle = ApogyCoreEnvironmentOrbitEarthFacade.INSTANCE.getSpacecraftElevationAngle(spacecraftState, earthSurfaceLocation);
						double azimuthAngle = ApogyCoreEnvironmentOrbitEarthFacade.INSTANCE.getSpacecraftAzimuthAngle(spacecraftState, earthSurfaceLocation);
						double range = ApogyCoreEnvironmentOrbitEarthFacade.INSTANCE.getRange(spacecraftState, earthSurfaceLocation);
						
						HorizontalCoordinates coord = ApogyEarthEnvironmentFactory.eINSTANCE.createHorizontalCoordinates();
						coord.setAltitude(elevationAngle);
						coord.setAzimuth(azimuthAngle);
						coord.setRadius(range);
						
						// Updates the spacecraft position.
						ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION, coord);
					}
				} 
				catch (Exception e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void setRootNode(Node newRootNode) 
	{	
		super.setRootNode(newRootNode);
		
		// Attaches the ToolNode to the new to Node.
		attachToolNode();	
		
		// Forces the tool to update.
		try 
		{
			update();
		} 
		catch (Exception e) 
		{			
			e.printStackTrace();
		}	
	}
	
	@Override
	public void variablesInstantiated() 
	{		
		super.variablesInstantiated();
		
		// Attaches the ToolNode to the new to Node.
		attachToolNode();	
		
		// Forces the tool to update.
		try 
		{
			update();
		} 
		catch (Exception e) 
		{			
			e.printStackTrace();
		}			
	}
	
	protected void attachToolNode()
	{
		if(getEarthOrbitingSpacecraftLocationToolNode() != null)
		{
			detachToolNode();
			
			if(getActiveEarthSurfaceWorksite() != null)
			{
				EarthSky earthSky = getActiveEarthSurfaceWorksite().getEarthSky();
				if(earthSky != null && earthSky.getSkyNode() != null)
				{
					earthSky.getSkyNode().getChildren().add(getEarthOrbitingSpacecraftLocationToolNode());
				}
			}
		}
	}
	
	protected void detachToolNode()
	{
		if(getEarthOrbitingSpacecraftLocationToolNode() != null && getEarthOrbitingSpacecraftLocationToolNode().getParent() instanceof GroupNode)
		{
			GroupNode parent = (GroupNode) getEarthOrbitingSpacecraftLocationToolNode().getParent();
			parent.getChildren().remove(getEarthOrbitingSpacecraftLocationToolNode());			
		}
	}
} //EarthOrbitingSpacecraftLocationToolImpl

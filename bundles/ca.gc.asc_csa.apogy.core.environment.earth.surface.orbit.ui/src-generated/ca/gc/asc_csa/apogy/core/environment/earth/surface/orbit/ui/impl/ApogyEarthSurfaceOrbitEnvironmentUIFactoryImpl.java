/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl;

import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyEarthSurfaceOrbitEnvironmentUIFactoryImpl extends EFactoryImpl implements ApogyEarthSurfaceOrbitEnvironmentUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyEarthSurfaceOrbitEnvironmentUIFactory init() {
		try {
			ApogyEarthSurfaceOrbitEnvironmentUIFactory theApogyEarthSurfaceOrbitEnvironmentUIFactory = (ApogyEarthSurfaceOrbitEnvironmentUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyEarthSurfaceOrbitEnvironmentUIPackage.eNS_URI);
			if (theApogyEarthSurfaceOrbitEnvironmentUIFactory != null) {
				return theApogyEarthSurfaceOrbitEnvironmentUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyEarthSurfaceOrbitEnvironmentUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthSurfaceOrbitEnvironmentUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL: return createEarthOrbitingSpacecraftLocationTool();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE: return createEarthOrbitingSpacecraftLocationToolNode();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL: return createEarthOrbitModelPassTool();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE: return createEarthOrbitModelPassToolNode();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitingSpacecraftLocationTool createEarthOrbitingSpacecraftLocationTool() {
		EarthOrbitingSpacecraftLocationToolImpl earthOrbitingSpacecraftLocationTool = new EarthOrbitingSpacecraftLocationToolImpl();
		return earthOrbitingSpacecraftLocationTool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitingSpacecraftLocationToolNode createEarthOrbitingSpacecraftLocationToolNode() {
		EarthOrbitingSpacecraftLocationToolNodeImpl earthOrbitingSpacecraftLocationToolNode = new EarthOrbitingSpacecraftLocationToolNodeImpl();
		return earthOrbitingSpacecraftLocationToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitModelPassTool createEarthOrbitModelPassTool() {
		EarthOrbitModelPassToolImpl earthOrbitModelPassTool = new EarthOrbitModelPassToolImpl();
		return earthOrbitModelPassTool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitModelPassToolNode createEarthOrbitModelPassToolNode() {
		EarthOrbitModelPassToolNodeImpl earthOrbitModelPassToolNode = new EarthOrbitModelPassToolNodeImpl();
		return earthOrbitModelPassToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthSurfaceOrbitEnvironmentUIPackage getApogyEarthSurfaceOrbitEnvironmentUIPackage() {
		return (ApogyEarthSurfaceOrbitEnvironmentUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyEarthSurfaceOrbitEnvironmentUIPackage getPackage() {
		return ApogyEarthSurfaceOrbitEnvironmentUIPackage.eINSTANCE;
	}

} //ApogyEarthSurfaceOrbitEnvironmentUIFactoryImpl

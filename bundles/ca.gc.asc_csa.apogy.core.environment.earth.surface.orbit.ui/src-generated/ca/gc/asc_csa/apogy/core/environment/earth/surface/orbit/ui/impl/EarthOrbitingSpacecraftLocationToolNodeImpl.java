/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl;

import ca.gc.asc_csa.apogy.common.topology.impl.NodeImpl;

import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Orbiting Spacecraft Location Tool Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolNodeImpl#getEarthOrbitingSpacecraftLocationTool <em>Earth Orbiting Spacecraft Location Tool</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EarthOrbitingSpacecraftLocationToolNodeImpl extends NodeImpl implements EarthOrbitingSpacecraftLocationToolNode {
	/**
	 * The cached value of the '{@link #getEarthOrbitingSpacecraftLocationTool() <em>Earth Orbiting Spacecraft Location Tool</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEarthOrbitingSpacecraftLocationTool()
	 * @generated
	 * @ordered
	 */
	protected EarthOrbitingSpacecraftLocationTool earthOrbitingSpacecraftLocationTool;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EarthOrbitingSpacecraftLocationToolNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitingSpacecraftLocationTool getEarthOrbitingSpacecraftLocationTool() {
		if (earthOrbitingSpacecraftLocationTool != null && earthOrbitingSpacecraftLocationTool.eIsProxy()) {
			InternalEObject oldEarthOrbitingSpacecraftLocationTool = (InternalEObject)earthOrbitingSpacecraftLocationTool;
			earthOrbitingSpacecraftLocationTool = (EarthOrbitingSpacecraftLocationTool)eResolveProxy(oldEarthOrbitingSpacecraftLocationTool);
			if (earthOrbitingSpacecraftLocationTool != oldEarthOrbitingSpacecraftLocationTool) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL, oldEarthOrbitingSpacecraftLocationTool, earthOrbitingSpacecraftLocationTool));
			}
		}
		return earthOrbitingSpacecraftLocationTool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitingSpacecraftLocationTool basicGetEarthOrbitingSpacecraftLocationTool() {
		return earthOrbitingSpacecraftLocationTool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEarthOrbitingSpacecraftLocationTool(EarthOrbitingSpacecraftLocationTool newEarthOrbitingSpacecraftLocationTool, NotificationChain msgs) {
		EarthOrbitingSpacecraftLocationTool oldEarthOrbitingSpacecraftLocationTool = earthOrbitingSpacecraftLocationTool;
		earthOrbitingSpacecraftLocationTool = newEarthOrbitingSpacecraftLocationTool;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL, oldEarthOrbitingSpacecraftLocationTool, newEarthOrbitingSpacecraftLocationTool);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEarthOrbitingSpacecraftLocationTool(EarthOrbitingSpacecraftLocationTool newEarthOrbitingSpacecraftLocationTool) {
		if (newEarthOrbitingSpacecraftLocationTool != earthOrbitingSpacecraftLocationTool) {
			NotificationChain msgs = null;
			if (earthOrbitingSpacecraftLocationTool != null)
				msgs = ((InternalEObject)earthOrbitingSpacecraftLocationTool).eInverseRemove(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE, EarthOrbitingSpacecraftLocationTool.class, msgs);
			if (newEarthOrbitingSpacecraftLocationTool != null)
				msgs = ((InternalEObject)newEarthOrbitingSpacecraftLocationTool).eInverseAdd(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE, EarthOrbitingSpacecraftLocationTool.class, msgs);
			msgs = basicSetEarthOrbitingSpacecraftLocationTool(newEarthOrbitingSpacecraftLocationTool, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL, newEarthOrbitingSpacecraftLocationTool, newEarthOrbitingSpacecraftLocationTool));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL:
				if (earthOrbitingSpacecraftLocationTool != null)
					msgs = ((InternalEObject)earthOrbitingSpacecraftLocationTool).eInverseRemove(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE, EarthOrbitingSpacecraftLocationTool.class, msgs);
				return basicSetEarthOrbitingSpacecraftLocationTool((EarthOrbitingSpacecraftLocationTool)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL:
				return basicSetEarthOrbitingSpacecraftLocationTool(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL:
				if (resolve) return getEarthOrbitingSpacecraftLocationTool();
				return basicGetEarthOrbitingSpacecraftLocationTool();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL:
				setEarthOrbitingSpacecraftLocationTool((EarthOrbitingSpacecraftLocationTool)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL:
				setEarthOrbitingSpacecraftLocationTool((EarthOrbitingSpacecraftLocationTool)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL:
				return earthOrbitingSpacecraftLocationTool != null;
		}
		return super.eIsSet(featureID);
	}

} //EarthOrbitingSpacecraftLocationToolNodeImpl

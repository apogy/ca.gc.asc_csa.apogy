/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage
 * @generated
 */
public interface ApogyEarthSurfaceOrbitEnvironmentUIFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyEarthSurfaceOrbitEnvironmentUIFactory eINSTANCE = ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Earth Orbiting Spacecraft Location Tool</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Earth Orbiting Spacecraft Location Tool</em>'.
	 * @generated
	 */
	EarthOrbitingSpacecraftLocationTool createEarthOrbitingSpacecraftLocationTool();

	/**
	 * Returns a new object of class '<em>Earth Orbiting Spacecraft Location Tool Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Earth Orbiting Spacecraft Location Tool Node</em>'.
	 * @generated
	 */
	EarthOrbitingSpacecraftLocationToolNode createEarthOrbitingSpacecraftLocationToolNode();

	/**
	 * Returns a new object of class '<em>Earth Orbit Model Pass Tool</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Earth Orbit Model Pass Tool</em>'.
	 * @generated
	 */
	EarthOrbitModelPassTool createEarthOrbitModelPassTool();

	/**
	 * Returns a new object of class '<em>Earth Orbit Model Pass Tool Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Earth Orbit Model Pass Tool Node</em>'.
	 * @generated
	 */
	EarthOrbitModelPassToolNode createEarthOrbitModelPassToolNode();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyEarthSurfaceOrbitEnvironmentUIPackage getApogyEarthSurfaceOrbitEnvironmentUIPackage();

} //ApogyEarthSurfaceOrbitEnvironmentUIFactory

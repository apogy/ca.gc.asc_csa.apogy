/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.SortedSet;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.GroupNode;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.Activator;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthFacade;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthFactory;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthPackage;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ConstantElevationMask;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.EarthOrbitModel;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.GroundStation;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.SpacecraftsVisibilitySet;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.TLE;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.VisibilityPass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Orbit Model Pass Tool</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolImpl#getLookAheadPeriod <em>Look Ahead Period</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolImpl#getLastPassesUpdateTime <em>Last Passes Update Time</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolImpl#getSpacecraftsVisibilitySet <em>Spacecrafts Visibility Set</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolImpl#getDisplayedPass <em>Displayed Pass</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolImpl#getEarthOrbitModelPassToolNode <em>Earth Orbit Model Pass Tool Node</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EarthOrbitModelPassToolImpl extends EarthOrbitModelToolImpl implements EarthOrbitModelPassTool 
{	
	private Adapter spacecraftsVisibilitySetAdapter;

	
	/**
	 * The default value of the '{@link #getLookAheadPeriod() <em>Look Ahead Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLookAheadPeriod()
	 * @generated
	 * @ordered
	 */
	protected static final long LOOK_AHEAD_PERIOD_EDEFAULT = 43200L;
	/**
	 * The cached value of the '{@link #getLookAheadPeriod() <em>Look Ahead Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLookAheadPeriod()
	 * @generated
	 * @ordered
	 */
	protected long lookAheadPeriod = LOOK_AHEAD_PERIOD_EDEFAULT;
	/**
	 * The default value of the '{@link #getLastPassesUpdateTime() <em>Last Passes Update Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPassesUpdateTime()
	 * @generated
	 * @ordered
	 */
	protected static final Date LAST_PASSES_UPDATE_TIME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getLastPassesUpdateTime() <em>Last Passes Update Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPassesUpdateTime()
	 * @generated
	 * @ordered
	 */
	protected Date lastPassesUpdateTime = LAST_PASSES_UPDATE_TIME_EDEFAULT;
	/**
	 * The cached value of the '{@link #getSpacecraftsVisibilitySet() <em>Spacecrafts Visibility Set</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpacecraftsVisibilitySet()
	 * @generated
	 * @ordered
	 */
	protected SpacecraftsVisibilitySet spacecraftsVisibilitySet;

	/**
	 * The cached value of the '{@link #getDisplayedPass() <em>Displayed Pass</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedPass()
	 * @generated
	 * @ordered
	 */
	protected VisibilityPass displayedPass;
	/**
	 * The cached value of the '{@link #getEarthOrbitModelPassToolNode() <em>Earth Orbit Model Pass Tool Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEarthOrbitModelPassToolNode()
	 * @generated
	 * @ordered
	 */
	protected EarthOrbitModelPassToolNode earthOrbitModelPassToolNode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EarthOrbitModelPassToolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getLookAheadPeriod() {
		return lookAheadPeriod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setLookAheadPeriod(long newLookAheadPeriod) 
	{
		setLookAheadPeriodGen(newLookAheadPeriod);
		runUpdateInJob();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLookAheadPeriodGen(long newLookAheadPeriod) {
		long oldLookAheadPeriod = lookAheadPeriod;
		lookAheadPeriod = newLookAheadPeriod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LOOK_AHEAD_PERIOD, oldLookAheadPeriod, lookAheadPeriod));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getLastPassesUpdateTime() {
		return lastPassesUpdateTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastPassesUpdateTime(Date newLastPassesUpdateTime) {
		Date oldLastPassesUpdateTime = lastPassesUpdateTime;
		lastPassesUpdateTime = newLastPassesUpdateTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LAST_PASSES_UPDATE_TIME, oldLastPassesUpdateTime, lastPassesUpdateTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EarthOrbitModel getEarthOrbitModel() 
	{
		EarthOrbitModel tmp = super.getEarthOrbitModel();
		
		// DEBUG
		if(tmp == null)
		{
			TLE tle;
			try 
			{
				tle = ApogyCoreEnvironmentOrbitEarthFacade.INSTANCE.loadTLE("platform:/plugin/com.neptec.ogr.simulator/data/ISS.tle");
				tmp = ApogyCoreEnvironmentOrbitEarthFacade.INSTANCE.createTLEEarthOrbitModel(tle);
				tmp.setName("ISS");
				tmp.setDescription("The ISS.");
				
				// ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL, tmp);
								
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		// DEBUG
		
		return tmp;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setEarthOrbitModel(EarthOrbitModel newEarthOrbitModel) 
	{
		super.setEarthOrbitModel(newEarthOrbitModel);
		
		// Trigger an update.
		runUpdateInJob();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public SpacecraftsVisibilitySet getSpacecraftsVisibilitySet() 
	{
		SpacecraftsVisibilitySet tmp = getSpacecraftsVisibilitySetGen();
		if(tmp == null)
		{
			tmp = ApogyCoreEnvironmentOrbitEarthFactory.eINSTANCE.createSpacecraftsVisibilitySet();
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET, tmp);
			
			tmp.eAdapters().add(new AdapterImpl()
			{
				public void notifyChanged(Notification msg) 
				{	
					int featureId = msg.getFeatureID(SpacecraftsVisibilitySet.class);
					if(featureId == ApogyCoreEnvironmentOrbitEarthPackage.SPACECRAFTS_VISIBILITY_SET__UPDATING)
					{					
						ApogyCommonTransactionFacade.INSTANCE.basicSet(EarthOrbitModelPassToolImpl.this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, msg.getNewBooleanValue(), true);
					}
				}
			});
		}
		
		return tmp;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpacecraftsVisibilitySet getSpacecraftsVisibilitySetGen() {
		if (spacecraftsVisibilitySet != null && spacecraftsVisibilitySet.eIsProxy()) {
			InternalEObject oldSpacecraftsVisibilitySet = (InternalEObject)spacecraftsVisibilitySet;
			spacecraftsVisibilitySet = (SpacecraftsVisibilitySet)eResolveProxy(oldSpacecraftsVisibilitySet);
			if (spacecraftsVisibilitySet != oldSpacecraftsVisibilitySet) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET, oldSpacecraftsVisibilitySet, spacecraftsVisibilitySet));
			}
		}
		return spacecraftsVisibilitySet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpacecraftsVisibilitySet basicGetSpacecraftsVisibilitySet() {
		return spacecraftsVisibilitySet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpacecraftsVisibilitySet(SpacecraftsVisibilitySet newSpacecraftsVisibilitySet) {
		SpacecraftsVisibilitySet oldSpacecraftsVisibilitySet = spacecraftsVisibilitySet;
		spacecraftsVisibilitySet = newSpacecraftsVisibilitySet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET, oldSpacecraftsVisibilitySet, spacecraftsVisibilitySet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityPass getDisplayedPass() {
		if (displayedPass != null && displayedPass.eIsProxy()) {
			InternalEObject oldDisplayedPass = (InternalEObject)displayedPass;
			displayedPass = (VisibilityPass)eResolveProxy(oldDisplayedPass);
			if (displayedPass != oldDisplayedPass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS, oldDisplayedPass, displayedPass));
			}
		}
		return displayedPass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityPass basicGetDisplayedPass() {
		return displayedPass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisplayedPass(VisibilityPass newDisplayedPass) {
		VisibilityPass oldDisplayedPass = displayedPass;
		displayedPass = newDisplayedPass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS, oldDisplayedPass, displayedPass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitModelPassToolNode getEarthOrbitModelPassToolNode() {
		if (earthOrbitModelPassToolNode != null && earthOrbitModelPassToolNode.eIsProxy()) {
			InternalEObject oldEarthOrbitModelPassToolNode = (InternalEObject)earthOrbitModelPassToolNode;
			earthOrbitModelPassToolNode = (EarthOrbitModelPassToolNode)eResolveProxy(oldEarthOrbitModelPassToolNode);
			if (earthOrbitModelPassToolNode != oldEarthOrbitModelPassToolNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE, oldEarthOrbitModelPassToolNode, earthOrbitModelPassToolNode));
			}
		}
		return earthOrbitModelPassToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitModelPassToolNode basicGetEarthOrbitModelPassToolNode() {
		return earthOrbitModelPassToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEarthOrbitModelPassToolNode(EarthOrbitModelPassToolNode newEarthOrbitModelPassToolNode, NotificationChain msgs) {
		EarthOrbitModelPassToolNode oldEarthOrbitModelPassToolNode = earthOrbitModelPassToolNode;
		earthOrbitModelPassToolNode = newEarthOrbitModelPassToolNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE, oldEarthOrbitModelPassToolNode, newEarthOrbitModelPassToolNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEarthOrbitModelPassToolNode(EarthOrbitModelPassToolNode newEarthOrbitModelPassToolNode) {
		if (newEarthOrbitModelPassToolNode != earthOrbitModelPassToolNode) {
			NotificationChain msgs = null;
			if (earthOrbitModelPassToolNode != null)
				msgs = ((InternalEObject)earthOrbitModelPassToolNode).eInverseRemove(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL, EarthOrbitModelPassToolNode.class, msgs);
			if (newEarthOrbitModelPassToolNode != null)
				msgs = ((InternalEObject)newEarthOrbitModelPassToolNode).eInverseAdd(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL, EarthOrbitModelPassToolNode.class, msgs);
			msgs = basicSetEarthOrbitModelPassToolNode(newEarthOrbitModelPassToolNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE, newEarthOrbitModelPassToolNode, newEarthOrbitModelPassToolNode));
	}

	@Override
	public void setActive(boolean newActive) 
	{	
		super.setActive(newActive);
		
		if(newActive) runUpdateInJob();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setActiveTimeSource(TimeSource newActiveTimeSource) 
	{
		super.setActiveTimeSource(newActiveTimeSource);
		
		// Trigger an update.		
		runUpdateInJob();
	}

	@Override
	public void setActiveEarthSurfaceWorksite(EarthSurfaceWorksite newActiveEarthSurfaceWorksite) 
	{
		if(getActiveEarthSurfaceWorksite() != newActiveEarthSurfaceWorksite)
		{
			// Detach node from old sky.
			detachToolNode();
			
			// Updates active worksite.
			super.setActiveEarthSurfaceWorksite(ApogyEarthSurfaceEnvironmentFacade.INSTANCE.getActiveEarthSurfaceWorksite());
			
			// Attaches node to new sky
			attachToolNode();
			
			runUpdateInJob();
		}				
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE:
				if (earthOrbitModelPassToolNode != null)
					msgs = ((InternalEObject)earthOrbitModelPassToolNode).eInverseRemove(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL, EarthOrbitModelPassToolNode.class, msgs);
				return basicSetEarthOrbitModelPassToolNode((EarthOrbitModelPassToolNode)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE:
				return basicSetEarthOrbitModelPassToolNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LOOK_AHEAD_PERIOD:
				return getLookAheadPeriod();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LAST_PASSES_UPDATE_TIME:
				return getLastPassesUpdateTime();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET:
				if (resolve) return getSpacecraftsVisibilitySet();
				return basicGetSpacecraftsVisibilitySet();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS:
				if (resolve) return getDisplayedPass();
				return basicGetDisplayedPass();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE:
				if (resolve) return getEarthOrbitModelPassToolNode();
				return basicGetEarthOrbitModelPassToolNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LOOK_AHEAD_PERIOD:
				setLookAheadPeriod((Long)newValue);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LAST_PASSES_UPDATE_TIME:
				setLastPassesUpdateTime((Date)newValue);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET:
				setSpacecraftsVisibilitySet((SpacecraftsVisibilitySet)newValue);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS:
				setDisplayedPass((VisibilityPass)newValue);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE:
				setEarthOrbitModelPassToolNode((EarthOrbitModelPassToolNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LOOK_AHEAD_PERIOD:
				setLookAheadPeriod(LOOK_AHEAD_PERIOD_EDEFAULT);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LAST_PASSES_UPDATE_TIME:
				setLastPassesUpdateTime(LAST_PASSES_UPDATE_TIME_EDEFAULT);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET:
				setSpacecraftsVisibilitySet((SpacecraftsVisibilitySet)null);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS:
				setDisplayedPass((VisibilityPass)null);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE:
				setEarthOrbitModelPassToolNode((EarthOrbitModelPassToolNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LOOK_AHEAD_PERIOD:
				return lookAheadPeriod != LOOK_AHEAD_PERIOD_EDEFAULT;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LAST_PASSES_UPDATE_TIME:
				return LAST_PASSES_UPDATE_TIME_EDEFAULT == null ? lastPassesUpdateTime != null : !LAST_PASSES_UPDATE_TIME_EDEFAULT.equals(lastPassesUpdateTime);
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET:
				return spacecraftsVisibilitySet != null;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS:
				return displayedPass != null;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE:
				return earthOrbitModelPassToolNode != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (lookAheadPeriod: ");
		result.append(lookAheadPeriod);
		result.append(", lastPassesUpdateTime: ");
		result.append(lastPassesUpdateTime);
		result.append(')');
		return result.toString();
	}

	@Override
	public void initialise() 
	{	
		// Initialize the tool node.
		EarthOrbitModelPassToolNode toolNode = ApogyEarthSurfaceOrbitEnvironmentUIFactory.eINSTANCE.createEarthOrbitModelPassToolNode();
		if(getName() != null)
		{
			toolNode.setDescription("Node associated with the EarthOrbitModelPassTool named <" + getName() + ">");
			toolNode.setNodeId("Earth_Orbit_Model_Pass_Tool_" +  getName().replaceAll(" ", "_"));
		}
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE, toolNode, true);	
									
		// Register to the Spacecraft Visibility Set.	
		getSpacecraftsVisibilitySet().eAdapters().add(getSpacecraftsVisibilitySetAdapter());
		
		super.initialise();

		runUpdateInJob();
	}
	
	@Override
	public void setRootNode(Node newRootNode) 
	{	
		super.setRootNode(newRootNode);
		
		// Attaches the ToolNode to the new to Node.
		attachToolNode();	
		
		// Forces the tool to update.
		runUpdateInJob();
	}
	
	@Override
	public void variablesInstantiated() 
	{		
		super.variablesInstantiated();
		
		// Attaches the ToolNode to the new to Node.
		attachToolNode();	
		
		// Forces the tool to update.
		runUpdateInJob();		
	}
	
	
	@Override
	public void dispose() 
	{
		// Unregister from the Spacecraft Visibility Set.		
		getSpacecraftsVisibilitySet().eAdapters().remove(getSpacecraftsVisibilitySetAdapter());
		
		// Detaches the node representing this tool in the topology.
		detachToolNode();
		
		super.dispose();
	}

	@Override
	public void update() throws Exception
	{
		if(isAutoUpdateEnabled()  && !isUpdating())
		{			
			// Update the passes.			
			updatePasses();
		}
	}
			
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public boolean getDefaultAutoUpdateEnabled() 
	{
		return true;
	}

	/**
	 * Update the current time. This can trigger an update().
	 * @param newTime The new time. Should not be null.
	 */
	public void updateTime(Date newTime)
	{
		// Updates the displayed pass.
		VisibilityPass nextPass = getApplicableVisibilityPass(newTime);				
		updateDisplayedPass(nextPass);
				
		// If no pass has been found and auto update is enabled, tries to upades passes.
		if(isAutoUpdateEnabled() && getDisplayedPass() == null)
		{
			boolean updadeNeeded = false;
			if(getLastPassesUpdateTime() != null)
			{
				long updatePeriodEndTime = getLastPassesUpdateTime().getTime() + getLookAheadPeriod() * 1000;
				if(newTime.getTime() > updatePeriodEndTime)
				{
					updadeNeeded = true;
				}
			}
			else
			{
				updadeNeeded = true;
			}
			
			if(updadeNeeded)
			{
				runUpdateInJob();
			}
		}
	}
	
	protected void runUpdateInJob()
	{
		Job job = new Job(getJobName())
		{
			@Override
			protected IStatus run(IProgressMonitor arg0) 
			{
				try 
				{
					if(isAutoUpdateEnabled())
					{
						update();
					}
				} 
				catch (Exception e) 
				{						
					e.printStackTrace();
				}
				return Status.OK_STATUS;
			}
		};
		job.schedule();
	}
	
	protected String getJobName()
	{
		String jobName = "EarthOrbitModelPassTool";
		if(getName() != null) jobName += " <" + getName() + ">";
		jobName += "- Update";
		return jobName;
	}
	
	protected void updatePasses()
	{
		if(getEarthOrbitModel() != null)
		{
			GroundStation groundStation = resolveGroundStation();
			TimeSource timeSource = getActiveTimeSource();
			if(groundStation != null && timeSource != null)
			{
				// ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, true, true);
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS, null, true);
				
				try
				{
					Date startDate = new Date(timeSource.getTime().getTime());
					
					// End date is now + 2 * lookAheadPeriod.
					Date endDate = new Date(startDate.getTime() + getLookAheadPeriod() * 1000 * 2);									
					ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__LAST_PASSES_UPDATE_TIME, startDate, false);
					
					SpacecraftsVisibilitySet spacecraftsVisibilitySet = getSpacecraftsVisibilitySet();
					spacecraftsVisibilitySet.getGroundStations().clear();
					spacecraftsVisibilitySet.getGroundStations().add(groundStation);
					
					spacecraftsVisibilitySet.getOrbitModels().clear();
					spacecraftsVisibilitySet.getOrbitModels().add(getEarthOrbitModel());
					
					spacecraftsVisibilitySet.setStartTime(startDate);
					spacecraftsVisibilitySet.setEndTime(endDate);
					spacecraftsVisibilitySet.setAutoUpdateEnabled(true);
								
					spacecraftsVisibilitySet.update();					
				}
				catch (Exception e) 
				{
					e.printStackTrace();					
				}					
				// ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, false, true);
			}
		}
	}
	
	/**
	 * Creates a GroundStation representing the EarthSurfaceWorksite.
	 * @return The GroundStation. Can be null if no EarthSurfaceWorksite is active.
	 */
	protected GroundStation resolveGroundStation()
	{
		EarthSurfaceWorksite earthSurfaceWorksite = getActiveEarthSurfaceWorksite();
		if(earthSurfaceWorksite != null)
		{
			GeographicCoordinates geographicCoordinates = earthSurfaceWorksite.getGeographicalCoordinates();
			if(geographicCoordinates != null)
			{
				GroundStation groundStation = ApogyCoreEnvironmentOrbitEarthFactory.eINSTANCE.createGroundStation();
				groundStation.setName(earthSurfaceWorksite.getName());
				groundStation.setLatitude(geographicCoordinates.getLatitude());
				groundStation.setLongitude(geographicCoordinates.getLongitude());
				groundStation.setElevation(geographicCoordinates.getElevation());
				
				ConstantElevationMask constantElevationMask = ApogyCoreEnvironmentOrbitEarthFactory.eINSTANCE.createConstantElevationMask();
				constantElevationMask.setConstantElevation(Math.toRadians(0));
				groundStation.setElevationMask(constantElevationMask);
				
				return groundStation;
			}
		}
		return null;
	}

	protected VisibilityPass getApplicableVisibilityPass(Date currentTime)
	{
		VisibilityPass visibilityPass = null;
		
		if(!getSpacecraftsVisibilitySet().getPasses().isEmpty())
		{
			SortedSet<VisibilityPass> passes = ApogyCoreEnvironmentOrbitEarthFacade.INSTANCE.getVisibilityPassSortedByStartDate(getSpacecraftsVisibilitySet().getPasses());
					
			long now = currentTime.getTime();
			
			Iterator<VisibilityPass> it = passes.iterator();
			while(it.hasNext() && (visibilityPass == null))
			{
				VisibilityPass pass = it.next();
				
				if(pass.getStartTime().getTime() <= now && 
				   pass.getEndTime().getTime()   >= now)
				{
					visibilityPass = pass;
				}
				else if(pass.getStartTime().getTime() >= now)
				{
					visibilityPass = pass;
				}
			}
		}
		
		return visibilityPass;				
	}
	
	private void updateDisplayedPass(VisibilityPass newDisplayedPass)
	{
		if(newDisplayedPass != getDisplayedPass())
		{
			Logger.INSTANCE.log(Activator.ID, this, "Updating next pass to <" + newDisplayedPass + ">.", EventSeverity.INFO);
			
			// Update displayed pass.
			ApogyCommonTransactionFacade.INSTANCE.basicSet(EarthOrbitModelPassToolImpl.this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS, newDisplayedPass, true);
		}
	}
	
	protected void attachToolNode()
	{
		if(getEarthOrbitModelPassToolNode() != null)
		{
			detachToolNode();
			
			if(getActiveEarthSurfaceWorksite() != null)
			{
				EarthSky earthSky = getActiveEarthSurfaceWorksite().getEarthSky();
				if(earthSky != null && earthSky.getSkyNode() != null)
				{
					earthSky.getSkyNode().getChildren().add(getEarthOrbitModelPassToolNode());
				}
			}
		}
	}
	
	protected void detachToolNode()
	{
		if(getEarthOrbitModelPassToolNode() != null && getEarthOrbitModelPassToolNode().getParent() instanceof GroupNode)
		{
			GroupNode parent = (GroupNode) getEarthOrbitModelPassToolNode().getParent();
			parent.getChildren().remove(getEarthOrbitModelPassToolNode());			
		}
	}

	private Adapter getSpacecraftsVisibilitySetAdapter() 
	{
		if(spacecraftsVisibilitySetAdapter == null)
		{
			spacecraftsVisibilitySetAdapter = new AdapterImpl()
			{
				public void notifyChanged(Notification msg) 
				{					
					if(msg.getNotifier() instanceof SpacecraftsVisibilitySet)
					{
						int featureId = msg.getFeatureID(SpacecraftsVisibilitySet.class);
						switch (featureId) 
						{
							case ApogyCoreEnvironmentOrbitEarthPackage.SPACECRAFTS_VISIBILITY_SET__PASSES:
							{
								VisibilityPass pass = null;
								if(getActiveTimeSource() != null)
								{
									pass = getApplicableVisibilityPass(getActiveTimeSource().getTime());																
								}								
								updateDisplayedPass(pass);
							}							
							break;

							default:
							break;
						}
					}
				}
			};
		}
		return spacecraftsVisibilitySetAdapter;
	}
	
} //EarthOrbitModelPassToolImpl

/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui;

import ca.gc.asc_csa.apogy.addons.Simple3DTool;

import ca.gc.asc_csa.apogy.common.emf.TimeSource;

import ca.gc.asc_csa.apogy.core.Updatable;

import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;

import ca.gc.asc_csa.apogy.core.environment.orbit.earth.EarthOrbitModel;
import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earth Orbit Model Tool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Abstract base class for tools displaying properties of EarthOrbitModel.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getEarthOrbitModel <em>Earth Orbit Model</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getActiveTimeSource <em>Active Time Source</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getActiveEarthSurfaceWorksite <em>Active Earth Surface Worksite</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelTool()
 * @model abstract="true"
 * @generated
 */
public interface EarthOrbitModelTool extends Simple3DTool, Updatable {
	/**
	 * Returns the value of the '<em><b>Earth Orbit Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The orbit model defining the orbit of the spacecraft.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Earth Orbit Model</em>' reference.
	 * @see #setEarthOrbitModel(EarthOrbitModel)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelTool_EarthOrbitModel()
	 * @model transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Editable' children='true'"
	 * @generated
	 */
	EarthOrbitModel getEarthOrbitModel();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getEarthOrbitModel <em>Earth Orbit Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Earth Orbit Model</em>' reference.
	 * @see #getEarthOrbitModel()
	 * @generated
	 */
	void setEarthOrbitModel(EarthOrbitModel value);

	/**
	 * Returns the value of the '<em><b>Active Time Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The time source currently used to compute the spacecraft location.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Active Time Source</em>' reference.
	 * @see #setActiveTimeSource(TimeSource)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelTool_ActiveTimeSource()
	 * @model transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Editable'"
	 * @generated
	 */
	TimeSource getActiveTimeSource();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getActiveTimeSource <em>Active Time Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active Time Source</em>' reference.
	 * @see #getActiveTimeSource()
	 * @generated
	 */
	void setActiveTimeSource(TimeSource value);

	/**
	 * Returns the value of the '<em><b>Active Earth Surface Worksite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The worksite for which to compute the the spacecraft location.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Active Earth Surface Worksite</em>' reference.
	 * @see #setActiveEarthSurfaceWorksite(EarthSurfaceWorksite)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelTool_ActiveEarthSurfaceWorksite()
	 * @model transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Editable'"
	 * @generated
	 */
	EarthSurfaceWorksite getActiveEarthSurfaceWorksite();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getActiveEarthSurfaceWorksite <em>Active Earth Surface Worksite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active Earth Surface Worksite</em>' reference.
	 * @see #getActiveEarthSurfaceWorksite()
	 * @generated
	 */
	void setActiveEarthSurfaceWorksite(EarthSurfaceWorksite value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method called when the active time source time changes.
	 * <!-- end-model-doc -->
	 * @model newTimeUnique="false"
	 * @generated
	 */
	void updateTime(Date newTime);

} // EarthOrbitModelTool

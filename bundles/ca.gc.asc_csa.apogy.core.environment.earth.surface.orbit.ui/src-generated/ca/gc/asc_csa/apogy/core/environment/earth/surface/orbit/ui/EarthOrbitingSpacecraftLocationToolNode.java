/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui;

import ca.gc.asc_csa.apogy.common.topology.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earth Orbiting Spacecraft Location Tool Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Specialized Node that represents the EarthOrbitModelPassTool in the topology.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode#getEarthOrbitingSpacecraftLocationTool <em>Earth Orbiting Spacecraft Location Tool</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitingSpacecraftLocationToolNode()
 * @model
 * @generated
 */
public interface EarthOrbitingSpacecraftLocationToolNode extends Node {
	/**
	 * Returns the value of the '<em><b>Earth Orbiting Spacecraft Location Tool</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#getEarthOrbitingSpacecraftLocationToolNode <em>Earth Orbiting Spacecraft Location Tool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Earth Orbiting Spacecraft Location Tool</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Earth Orbiting Spacecraft Location Tool</em>' reference.
	 * @see #setEarthOrbitingSpacecraftLocationTool(EarthOrbitingSpacecraftLocationTool)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitingSpacecraftLocationToolNode_EarthOrbitingSpacecraftLocationTool()
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#getEarthOrbitingSpacecraftLocationToolNode
	 * @model opposite="earthOrbitingSpacecraftLocationToolNode" transient="true"
	 * @generated
	 */
	EarthOrbitingSpacecraftLocationTool getEarthOrbitingSpacecraftLocationTool();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode#getEarthOrbitingSpacecraftLocationTool <em>Earth Orbiting Spacecraft Location Tool</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Earth Orbiting Spacecraft Location Tool</em>' reference.
	 * @see #getEarthOrbitingSpacecraftLocationTool()
	 * @generated
	 */
	void setEarthOrbitingSpacecraftLocationTool(EarthOrbitingSpacecraftLocationTool value);

} // EarthOrbitingSpacecraftLocationToolNode

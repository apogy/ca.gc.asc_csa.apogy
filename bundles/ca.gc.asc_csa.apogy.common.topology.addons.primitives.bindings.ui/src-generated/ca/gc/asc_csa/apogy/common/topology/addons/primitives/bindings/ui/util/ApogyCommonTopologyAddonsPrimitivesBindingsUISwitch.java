/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.LightEnablementBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.PointLightBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.SpotLightBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage
 * @generated
 */
public class ApogyCommonTopologyAddonsPrimitivesBindingsUISwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyAddonsPrimitivesBindingsUISwitch() {
		if (modelPackage == null) {
			modelPackage = ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER: {
				LightEnablementBindingWizardPagesProvider lightEnablementBindingWizardPagesProvider = (LightEnablementBindingWizardPagesProvider)theEObject;
				T result = caseLightEnablementBindingWizardPagesProvider(lightEnablementBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(lightEnablementBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(lightEnablementBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER: {
				PointLightBindingWizardPagesProvider pointLightBindingWizardPagesProvider = (PointLightBindingWizardPagesProvider)theEObject;
				T result = casePointLightBindingWizardPagesProvider(pointLightBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(pointLightBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(pointLightBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER: {
				SpotLightBindingWizardPagesProvider spotLightBindingWizardPagesProvider = (SpotLightBindingWizardPagesProvider)theEObject;
				T result = caseSpotLightBindingWizardPagesProvider(spotLightBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(spotLightBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(spotLightBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Light Enablement Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Light Enablement Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLightEnablementBindingWizardPagesProvider(LightEnablementBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Point Light Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Point Light Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePointLightBindingWizardPagesProvider(PointLightBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Spot Light Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Spot Light Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSpotLightBindingWizardPagesProvider(SpotLightBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWizardPagesProvider(WizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Topology Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Topology Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractTopologyBindingWizardPagesProvider(AbstractTopologyBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ApogyCommonTopologyAddonsPrimitivesBindingsUISwitch

/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.PointLight;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsFactory;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.PointLightBinding;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.PointLightBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TopologyUIBindingsConstants;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.AbstractTopologyBindingWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.TopologyNodeSelectionWizardPage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Point Light Binding Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PointLightBindingWizardPagesProviderImpl extends AbstractTopologyBindingWizardPagesProviderImpl implements PointLightBindingWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PointLightBindingWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.Literals.POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		PointLightBinding pointLightBinding = ApogyCommonTopologyAddonsPrimitivesBindingsFactory.eINSTANCE.createPointLightBinding();		
		return pointLightBinding;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));
		
		// Add topology node selection page.
		List<Node> nodes = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings  = (MapBasedEClassSettings) settings;
			nodes = (List<Node>) mapBasedEClassSettings.getUserDataMap().get(TopologyUIBindingsConstants.NODES_LIST_USER_ID);
		}
		
		PointLightBinding binding = (PointLightBinding) eObject;		
		
		TopologyNodeSelectionWizardPage topologyNodeSelectionWizardPage = new TopologyNodeSelectionWizardPage("Select Point Light", 
				"Select the Point Light that this binding is controlling.", 
				binding, 
				ApogyCommonTopologyAddonsPrimitivesPackage.Literals.POINT_LIGHT, 
				nodes) 
		{	
			@Override
			protected void nodeSelected(Node nodeSelected) 
			{
				if(abstractTopologyBinding != null && nodeSelected instanceof PointLight)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet((PointLightBinding) abstractTopologyBinding, ApogyCommonTopologyAddonsPrimitivesBindingsPackage.Literals.POINT_LIGHT_BINDING__POINT_LIGHT, (PointLight) nodeSelected, true);
					
					setMessage("Node selected : " + nodeSelected.getNodeId());
					setErrorMessage(null);
				}		
				else
				{
					setErrorMessage("The selected Node is not a Point Light !");
				}
			}
		};
		
		list.add(topologyNodeSelectionWizardPage);
		
		return list;
	}	
} //PointLightBindingWizardPagesProviderImpl

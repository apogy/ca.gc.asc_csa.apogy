/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.ApogyCommonTopologyAddonsPrimitivesBindingsUIFactory;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.LightEnablementBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.PointLightBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.SpotLightBindingWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCommonTopologyAddonsPrimitivesBindingsUIFactoryImpl extends EFactoryImpl implements ApogyCommonTopologyAddonsPrimitivesBindingsUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyCommonTopologyAddonsPrimitivesBindingsUIFactory init() {
		try {
			ApogyCommonTopologyAddonsPrimitivesBindingsUIFactory theApogyCommonTopologyAddonsPrimitivesBindingsUIFactory = (ApogyCommonTopologyAddonsPrimitivesBindingsUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.eNS_URI);
			if (theApogyCommonTopologyAddonsPrimitivesBindingsUIFactory != null) {
				return theApogyCommonTopologyAddonsPrimitivesBindingsUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyCommonTopologyAddonsPrimitivesBindingsUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyAddonsPrimitivesBindingsUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER: return createLightEnablementBindingWizardPagesProvider();
			case ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER: return createPointLightBindingWizardPagesProvider();
			case ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER: return createSpotLightBindingWizardPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LightEnablementBindingWizardPagesProvider createLightEnablementBindingWizardPagesProvider() {
		LightEnablementBindingWizardPagesProviderImpl lightEnablementBindingWizardPagesProvider = new LightEnablementBindingWizardPagesProviderImpl();
		return lightEnablementBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PointLightBindingWizardPagesProvider createPointLightBindingWizardPagesProvider() {
		PointLightBindingWizardPagesProviderImpl pointLightBindingWizardPagesProvider = new PointLightBindingWizardPagesProviderImpl();
		return pointLightBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpotLightBindingWizardPagesProvider createSpotLightBindingWizardPagesProvider() {
		SpotLightBindingWizardPagesProviderImpl spotLightBindingWizardPagesProvider = new SpotLightBindingWizardPagesProviderImpl();
		return spotLightBindingWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage getApogyCommonTopologyAddonsPrimitivesBindingsUIPackage() {
		return (ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage getPackage() {
		return ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.eINSTANCE;
	}

} //ApogyCommonTopologyAddonsPrimitivesBindingsUIFactoryImpl

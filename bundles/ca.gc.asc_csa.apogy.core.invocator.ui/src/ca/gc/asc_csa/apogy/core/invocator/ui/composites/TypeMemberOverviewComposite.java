/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureNode;
import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureTreeNode;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.TreeRootNode;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.Type;
import ca.gc.asc_csa.apogy.core.invocator.TypeMember;

public class TypeMemberOverviewComposite extends Composite {

	private TypeMember typeMember;
	
	private DataBindingContext m_bindingContext;
	
	private Text txtNamevalue;
	private Text txtDescriptionvalue;
	private Text txtInterfaceClass;
	private Text txtAbstractFeatureTreeNode;
	
	public TypeMemberOverviewComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, false));		
		
		Label lblName = new Label(this, SWT.NONE);
		lblName.setAlignment(SWT.RIGHT);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name:");
		
		txtNamevalue = new Text(this, SWT.NONE);
		GridData gd_txtNamevalue = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_txtNamevalue.minimumWidth = 400;
		gd_txtNamevalue.widthHint = 400;
		txtNamevalue.setLayoutData(gd_txtNamevalue);
		
		Label lblDescription = new Label(this, SWT.NONE);
		lblDescription.setAlignment(SWT.RIGHT);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDescription.setText("Description:");
		
		txtDescriptionvalue = new Text(this, SWT.NONE | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtDescriptionvalue = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_txtDescriptionvalue.minimumWidth = 400;
		gd_txtDescriptionvalue.widthHint = 400;
		gd_txtDescriptionvalue.minimumHeight = 50;
		gd_txtDescriptionvalue.heightHint = 50;
		txtDescriptionvalue.setLayoutData(gd_txtDescriptionvalue);		
		
		Label lblInterfaceClass = new Label(this, SWT.NONE);
		lblInterfaceClass.setAlignment(SWT.RIGHT);
		lblInterfaceClass.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblInterfaceClass.setText("Interface Class:");
		
		txtInterfaceClass = new Text(this, SWT.NONE);
		GridData gd_txtInterfaceClass = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_txtInterfaceClass.minimumWidth = 400;
		gd_txtInterfaceClass.widthHint = 400;		
		txtInterfaceClass.setLayoutData(gd_txtInterfaceClass);
		
		// AbstractFeatureTreeNode
		Label AbstractFeatureTreeNode = new Label(this, SWT.NONE);
		AbstractFeatureTreeNode.setAlignment(SWT.RIGHT);
		AbstractFeatureTreeNode.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		AbstractFeatureTreeNode.setText("Feature :");
		
		txtAbstractFeatureTreeNode = new Text(this, SWT.NONE);
		txtAbstractFeatureTreeNode.setEnabled(false);
		txtAbstractFeatureTreeNode.setToolTipText("Feature of this system interface class to which this member is mapped.");
		GridData gd_txtAbstractFeatureTreeNode = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_txtAbstractFeatureTreeNode.minimumWidth = 300;
		gd_txtAbstractFeatureTreeNode.widthHint = 300;
		txtAbstractFeatureTreeNode.setLayoutData(gd_txtAbstractFeatureTreeNode);
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();				
			}
		});
	}

	public TypeMember getTypeMember() {
		return typeMember;
	}

	public void setTypeMember(TypeMember newTypeMember) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.typeMember = newTypeMember;
		
		if(newTypeMember != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		/* Name Value. */
		IObservableValue<String> observeName = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.NAMED__NAME)).observe(getTypeMember());
		IObservableValue<String> observeNameValueText = WidgetProperties.text(SWT.Modify).observe(txtNamevalue);

		bindingContext.bindValue(observeNameValueText,
								observeName, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		/* Description Value. */
		IObservableValue<String> observeDescription = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.DESCRIBED__DESCRIPTION)).observe(getTypeMember());
		IObservableValue<String> observeDescriptionText = WidgetProperties.text(SWT.Modify).observe(txtDescriptionvalue);

		bindingContext.bindValue(observeDescriptionText,
								 observeDescription, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		/* Interface Class.*/
		IObservableValue<EClass> observeInterface = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  									FeaturePath.fromList(ApogyCoreInvocatorPackage.Literals.TYPE_MEMBER__MEMBER_TYPE)).observe(getTypeMember());
		
		IObservableValue<String> observeInterfaceText = WidgetProperties.text(SWT.Modify).observe(txtInterfaceClass);
		
		bindingContext.bindValue(observeInterfaceText,
				observeInterface, 
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				 new UpdateValueStrategy().setConverter(new Converter(EClass.class, String.class)
				 {																		
					@Override
					public Object convert(Object fromObject) 
					{										
						if(fromObject instanceof Type)
						{
							Type type = (Type) fromObject;
							EClass eClass = type.getInterfaceClass();
							
							if(eClass != null)
							{
								return eClass.getInstanceClassName();
							}
							else
							{
								return "None";
							}																										
						}
						else
						{
							return "?";
						}
					}

					}));
		
		// Instance Class Tooltip.
		IObservableValue<String> observeInterfaceTextToolTip = WidgetProperties.tooltipText().observe(txtInterfaceClass);
		bindingContext.bindValue(observeInterfaceTextToolTip,
				observeInterface, 
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				 new UpdateValueStrategy().setConverter(new Converter(EClass.class, String.class)
				 {																		
					@Override
					public Object convert(Object fromObject) 
					{										
						if(fromObject instanceof Type)
						{
							Type type = (Type) fromObject;
							EClass eClass = type.getInterfaceClass();
							
							if(eClass != null)
							{
								return eClass.getInstanceClassName();
							}
							else
							{
								return "None";
							}																										
						}
						else
						{
							return "?";
						}
					}

					}));
		
		
		// AbstractFeatureTreeNode
		IObservableValue<AbstractFeatureTreeNode> observeAbstractFeatureValue = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  									   FeaturePath.fromList(ApogyCoreInvocatorPackage.Literals.TYPE_MEMBER__TYPE_FEATURE_ROOT_NODE)).observe(getTypeMember());

		IObservableValue<String> observeAbstractFeatureTreeNodeText = WidgetProperties.text(SWT.Modify).observe(txtAbstractFeatureTreeNode);

		bindingContext.bindValue(observeAbstractFeatureTreeNodeText,
								observeAbstractFeatureValue, 
				 				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), 
				 				new UpdateValueStrategy().setConverter(new Converter(AbstractFeatureNode.class, String.class)
				 				{																		 											
									@Override
									public Object convert(Object fromObject) 
									{											
										if(fromObject instanceof TreeRootNode)
										{
											TreeRootNode rootNode = (TreeRootNode) fromObject;
											if(rootNode.getChildren().size() > 0)
											{
												return ApogyCommonEMFFacade.INSTANCE.getAncestriesString(rootNode.getChildren().get(0));												
											}
											else
											{
												return "";
											}
										}
										else
										{
											return "";
										}
									}
				 				}));
		
		return bindingContext;
	}
	

}

package ca.gc.asc_csa.apogy.core.invocator.ui.handlers;

import java.util.List;

import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.impl.PartImpl;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.AbstractProgramRuntime;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ProgramRuntimeState;
import ca.gc.asc_csa.apogy.core.invocator.ProgramSettings;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.ProgramPart;

@SuppressWarnings("restriction")
public class StepOverProgramRuntimeHandler extends AbstractProgramRuntimeHandler {

	@Execute
	public void execute(MPart part, ECommandService commandService, EHandlerService handlerService) 
	{
		ProgramPart programPart = (ProgramPart) ((PartImpl) part).getObject();

		Program program = programPart.getSelectedProgram();		
		
		List<AbstractProgramRuntime> runtimes = programPart.getRuntimes();
		if (runtimes.size() > 0) 
		{
			for (int i = runtimes.size() - 1; i >= 0; i--) 
			{
				AbstractProgramRuntime runtime = runtimes.get(i);
				if (runtime.getProgram() == program) 
				{
					runtime.stepOver();
					break;
				}
			}
			/**
			 * Otherwise if the list is empty, the button starts a new runtime
			 * in a step by step mode
			 */
		} 
		else 
		{
			ProgramSettings programSettings = null;
			AbstractProgramRuntime programRuntime = createAbstractProgramRuntime(program, programSettings);
			if(programRuntime != null)
			{
				programPart.newRuntime(programRuntime);
				programRuntime.init();		
				ApogyCommonTransactionFacade.INSTANCE.basicSet(programRuntime, ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE, ProgramRuntimeState.SUSPENDED);
				programRuntime.stepOver();
			}								
		}
	}

	@CanExecute
	public boolean canExecute(MPart part) {
		if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() == null
				|| ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment() == null
				|| ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment()
						.getActiveContext() == null
				|| !ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment().getActiveContext()
						.isVariablesInstantiated()) {
			return false;
		}

		if (part instanceof PartImpl && ((PartImpl) part).getObject() instanceof ProgramPart) 
		{
			ProgramPart programPart = (ProgramPart) ((PartImpl) part).getObject();		
			Program program = programPart.getSelectedProgram();
			return (program != null);
			
			/** If there is a runtime */
//			if (programPart.getRuntimes().size() > 0) 
//			{				
//				for (AbstractProgramRuntime runtime : programPart.getRuntimes()) 
//				{
//					if (programPart.getRuntimes().indexOf(runtime) == programPart.getRuntimes().size() - 1
//							&& runtime.getProgram() == program
//							&& runtime.getState() == ProgramRuntimeState.SUSPENDED) {
//						/** Can be executed if the state is suspended */
//						return true;
//					}
//				}				
//			} 		
		}
		return false;
	}
}
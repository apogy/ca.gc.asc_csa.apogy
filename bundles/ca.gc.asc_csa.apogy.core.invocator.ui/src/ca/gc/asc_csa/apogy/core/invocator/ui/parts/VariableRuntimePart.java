package ca.gc.asc_csa.apogy.core.invocator.ui.parts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.impl.FeaturePathAdapterImpl;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.EObjectComposite;
import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIRCPConstants;

public class VariableRuntimePart extends AbstractEObjectSelectionPart {
	
	private FeaturePathAdapter adapter;
	private VariableFeatureReference variableFeatureReference;

	@Override
	protected void createContentComposite(Composite parent, int style) {
		new EObjectComposite(parent, SWT.None) {
			@Override
			protected void newSelection(ISelection selection) {
				selectionService.setSelection(((EObjectComposite) getActualComposite()).getSelectedEObject());
			}
		};
	}

	@Override
	protected void setCompositeContents(EObject eObject) {
		if(eObject instanceof VariableFeatureReference){
			if(variableFeatureReference != null){
				getAdapter().dispose();
			}
			
			this.variableFeatureReference = (VariableFeatureReference)eObject;
			getAdapter().init(eObject);
			
		
		}
		
		if (this.variableFeatureReference != null && this.variableFeatureReference.getVariable() != null
				&& this.variableFeatureReference.getVariable().getEnvironment() != null
				&& this.variableFeatureReference.getVariable().getEnvironment().getActiveContext() != null
				&& this.variableFeatureReference.getVariable().getEnvironment().getActiveContext()
						.isVariablesInstantiated()) {
			((EObjectComposite) getActualComposite())
			.setEObject(ApogyCoreInvocatorFacade.INSTANCE.getInstance(variableFeatureReference));
		}else{
			setNoContentComposite();
		}
	}

	@Override
	protected void createNoContentComposite(Composite parent, int style) {
		if (this.variableFeatureReference != null) {
			new NoContentComposite(parent, style) {
				@Override
				protected String getMessage() {
					return "Variables not instantiated";
				}
			};
		} else {
			super.createNoContentComposite(parent, style);
		}
	}

	private FeaturePathAdapter getAdapter() {
		if(adapter == null){
			adapter = new FeaturePathAdapterImpl() {
				
				@Override
				public void notifyChanged(Notification msg) {
					setEObject(variableFeatureReference);
				}
				
				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> features = new ArrayList<EStructuralFeature>();
					
					features.add(ApogyCoreInvocatorPackage.Literals.VARIABLE_FEATURE_REFERENCE__VARIABLE);
					features.add(ApogyCoreInvocatorPackage.Literals.VARIABLE__ENVIRONMENT);
					features.add(ApogyCoreInvocatorPackage.Literals.ENVIRONMENT__ACTIVE_CONTEXT);
					features.add(ApogyCoreInvocatorPackage.Literals.CONTEXT__VARIABLES_INSTANTIATED);
					
					return features;
				}
			};
		}
		return adapter;
	}

	@Override
	public void dispose() {
		if(variableFeatureReference != null){
			getAdapter().dispose();
		}

		super.dispose();
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() {
		HashMap<String, ISelectionListener> selectionProvidersIdsToSelectionListeners = new HashMap<String, ISelectionListener>();

		selectionProvidersIdsToSelectionListeners.put(ApogyCoreInvocatorUIRCPConstants.PART__VARIABLES_LIST__ID,
				DEFAULT_LISTENER);

		return selectionProvidersIdsToSelectionListeners;
	}
}

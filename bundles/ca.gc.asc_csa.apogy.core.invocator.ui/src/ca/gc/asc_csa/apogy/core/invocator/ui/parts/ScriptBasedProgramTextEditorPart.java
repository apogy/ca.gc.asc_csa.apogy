package ca.gc.asc_csa.apogy.core.invocator.ui.parts;

/*
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import java.util.HashMap;

import javax.inject.Inject;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.core.invocator.ScriptBasedProgram;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIRCPConstants;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.TextScriptEditorComposite;

public abstract class ScriptBasedProgramTextEditorPart extends AbstractEObjectSelectionPart 
{
	@Inject
	private IEventBroker broker;
	
	private ScriptBasedProgram scriptBasedProgram = null;
		
	private Composite top;

	private TextScriptEditorComposite textScriptEditorComposite;
	
	@Override
	protected void createContentComposite(Composite parent, int style) 
	{
		top = new Composite(parent, SWT.BORDER);
		top.setLayout(new GridLayout(1,false));	
		
		textScriptEditorComposite = new TextScriptEditorComposite(top, SWT.BORDER)
		{
			@Override
			protected void fileDirtyChanged(boolean isDirty) 
			{
				if(isDirty)
				{
					// Adds the * to from part label.
					mPart.setDirty(true);
				}
				else
				{
					// Removes * from part label.
					mPart.setDirty(false);
				}
			}
		};
		GridData gd_textScriptEditorComposite = new GridData(SWT.FILL, SWT.FILL, false, false);
		textScriptEditorComposite.setLayoutData(gd_textScriptEditorComposite);				
	}

	protected abstract String getRelativeScriptPath(ScriptBasedProgram scriptBasedProgram);
	
	@Override
	protected void setCompositeContents(EObject eObject) 
	{			
		if(eObject instanceof ScriptBasedProgram)
		{
			scriptBasedProgram = (ScriptBasedProgram) eObject;
			textScriptEditorComposite.setScriptPath(getRelativeScriptPath(scriptBasedProgram));
		}
		else
		{
			textScriptEditorComposite.setScriptPath(null);
		}
	}

	@Override
	public void userPostConstruct(MPart mPart) {
		
		super.userPostConstruct(mPart);
	}

	public ScriptBasedProgram getSelectedProgram()
	{
		return scriptBasedProgram;
	}
	
	@Override
	public void userPreDestroy(MPart mPart) {
		
		super.userPreDestroy(mPart);
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() 
	{
		HashMap<String, ISelectionListener> selectionProvidersIdsToSelectionListeners = new HashMap<String, ISelectionListener>();

		selectionProvidersIdsToSelectionListeners.put(ApogyCoreInvocatorUIRCPConstants.PART__SCRIPT_BASED_PROGRAMS_LIST__ID, DEFAULT_LISTENER);

		return selectionProvidersIdsToSelectionListeners;
	}
}

package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import ca.gc.asc_csa.apogy.core.invocator.InitialConditions;
import ca.gc.asc_csa.apogy.core.invocator.InitialConditionsList;
import ca.gc.asc_csa.apogy.core.invocator.TypeMemberInitialConditions;
import ca.gc.asc_csa.apogy.core.invocator.VariableInitialConditions;

public class InitialConditionsComposite extends ScrolledComposite 
{
	private InitialConditionsList initialConditionsList;
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	private InitialConditionsListComposite initialConditionsListComposite;
	private InitialConditionsOverviewComposite initialConditionsOverviewComposite;
	private InitialConditionsTreeComposite initialConditionsTreeComposite;
	private AbstractInitialConditionsDetailsComposite abstractInitialConditionsDetailsComposite;
	
	public InitialConditionsComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new FillLayout());
		setExpandHorizontal(true);
		setExpandVertical(true);
		
		Composite composite = new Composite(this, SWT.None);
		composite.setLayout(new GridLayout(3, false));		
		
		// Available Initial Conditions.
		Section sctnAvailableInitialConditions = formToolkit.createSection(composite, Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		sctnAvailableInitialConditions.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 2));
		formToolkit.paintBordersFor(sctnAvailableInitialConditions);
		sctnAvailableInitialConditions.setText("Available Initial Conditions");
		
		initialConditionsListComposite = new InitialConditionsListComposite(sctnAvailableInitialConditions, SWT.NONE)
		{
			@Override
			protected void newInitialConditionsSelected(InitialConditions initialConditions) 
			{
				initialConditionsTreeComposite.setInitialConditions(initialConditions);					
				initialConditionsOverviewComposite.setInitialConditions(initialConditions);
			}
		};
		formToolkit.adapt(initialConditionsListComposite);
		formToolkit.paintBordersFor(initialConditionsListComposite);
		sctnAvailableInitialConditions.setClient(initialConditionsListComposite);
				
		// Initial Condition Overview
		Section sctnInitialConditionsOverview = formToolkit.createSection(composite, Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		sctnInitialConditionsOverview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		formToolkit.paintBordersFor(sctnInitialConditionsOverview);
		sctnInitialConditionsOverview.setText("Initial Conditions Overview");
				
		initialConditionsOverviewComposite = new InitialConditionsOverviewComposite(sctnInitialConditionsOverview, SWT.NONE);
		formToolkit.adapt(initialConditionsOverviewComposite);
		formToolkit.paintBordersFor(initialConditionsOverviewComposite);
		sctnInitialConditionsOverview.setClient(initialConditionsOverviewComposite);
		
		
		// Initialization Details
		Section sctnInitializationDetails = formToolkit.createSection(composite, Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);				
		sctnInitializationDetails.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 2));
		formToolkit.paintBordersFor(sctnInitializationDetails);
		sctnInitializationDetails.setText("Initialization Details");
		
		abstractInitialConditionsDetailsComposite = new AbstractInitialConditionsDetailsComposite(sctnInitializationDetails, SWT.BORDER);
		formToolkit.adapt(abstractInitialConditionsDetailsComposite);
		formToolkit.paintBordersFor(abstractInitialConditionsDetailsComposite);
		sctnInitializationDetails.setClient(abstractInitialConditionsDetailsComposite);
		
		
		// Initial Condition Details					
		Section sctnInitialConditionsDetails = formToolkit.createSection(composite, Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);				
		sctnInitialConditionsDetails.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		formToolkit.paintBordersFor(sctnInitialConditionsDetails);
		sctnInitialConditionsDetails.setText("Initial Conditions Details");
		
		initialConditionsTreeComposite = new InitialConditionsTreeComposite(sctnInitialConditionsDetails, style)
		{
			@Override
			protected void newTypeMemberInitialConditionsSelected(TypeMemberInitialConditions typeMemberInitialConditions) 
			{
				abstractInitialConditionsDetailsComposite.setAbstractInitialConditions(typeMemberInitialConditions);
			}	
			
			@Override
			protected void newVariableInitialConditionsSelected(VariableInitialConditions variableInitialConditions) 
			{
				abstractInitialConditionsDetailsComposite.setAbstractInitialConditions(variableInitialConditions);
			}
		};
		formToolkit.adapt(initialConditionsTreeComposite);
		formToolkit.paintBordersFor(initialConditionsTreeComposite);
		sctnInitialConditionsDetails.setClient(initialConditionsTreeComposite);
			
		/**
		 * Perform a layout otherwise the VariableImplementation Content is not
		 * displayed without resize.
		 */
		composite.layout(true, true);

		setContent(composite);
		setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}
	
	public InitialConditionsList getInitialConditionsList() 
	{
		return initialConditionsList;
	}

	public void setInitialConditionsList(InitialConditionsList initialConditionsList) 
	{
		this.initialConditionsList = initialConditionsList;
			
		if(!initialConditionsListComposite.isDisposed())
		{
			initialConditionsListComposite.setInitialConditionsList(initialConditionsList);
			
			InitialConditions initialConditions = null;
			if(initialConditionsList != null && !initialConditionsList.getInitialConditions().isEmpty())
			{
				initialConditions = initialConditionsList.getInitialConditions().get(0);
			}
			
			if(!initialConditionsTreeComposite.isDisposed())
			{
				initialConditionsTreeComposite.setInitialConditions(initialConditions);				
			}
			
			if(!initialConditionsOverviewComposite.isDisposed())
			{
				initialConditionsOverviewComposite.setInitialConditions(initialConditions);
			}
			
			if(!abstractInitialConditionsDetailsComposite.isDisposed())
			{
				abstractInitialConditionsDetailsComposite.setAbstractInitialConditions(null);
			}
		}
	}
}

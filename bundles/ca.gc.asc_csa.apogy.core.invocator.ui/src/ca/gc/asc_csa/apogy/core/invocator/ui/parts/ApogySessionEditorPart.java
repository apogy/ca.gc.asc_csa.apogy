package ca.gc.asc_csa.apogy.core.invocator.ui.parts;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.composites.EObjectEditorComposite;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;

public class ApogySessionEditorPart extends AbstractSessionBasedPart {
	@Override
	protected void createContentComposite(Composite parent, int style) {
		new EObjectEditorComposite(parent, SWT.None) {
			@Override
			protected void newSelection(ISelection selection) {
				selectionService.setSelection(((EObjectEditorComposite) getActualComposite()).getSelectedEObject());
			}
		};
	}

	@Override
	protected void createNoContentComposite(Composite parent, int style) {
		if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() == null) {
			super.createNoContentComposite(parent, style);
		} else {
			new NoContentComposite(parent, style) {
				@Override
				protected String getMessage() {
					return "No compatible selection";
				}
			};
		}
	}

	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) {
		((EObjectEditorComposite) getActualComposite()).setEObject(invocatorSession);
	}
}
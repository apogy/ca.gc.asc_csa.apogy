package ca.gc.asc_csa.apogy.core.invocator.ui.commands;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import ca.gc.asc_csa.apogy.core.invocator.AbstractProgramRuntime;

public class ProgramRuntimeStepReturnCommand extends AbstractHandler implements
		IHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		new Job("ApogyCoreInvocator: Program Runtime Step Return") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				final Iterator<?> selections = ((IStructuredSelection) HandlerUtil
						.getActiveMenuSelection(event)).iterator();

				final AbstractProgramRuntime programRuntime = (AbstractProgramRuntime) selections.next();
				programRuntime.stepReturn();
				return Status.OK_STATUS;
			}
		}.schedule();
		return null;
	}
}
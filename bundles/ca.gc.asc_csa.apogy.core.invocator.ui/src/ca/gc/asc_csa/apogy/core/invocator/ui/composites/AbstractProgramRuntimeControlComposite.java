package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.invocator.AbstractProgramRuntime;

public class AbstractProgramRuntimeControlComposite extends Composite 
{
	protected AbstractProgramRuntime abstractProgramRuntime;
	
	private Button initButton;
	private Button terminateButton;
	private Button resumeButton;
	private Button suspendButton;
	private Button stepIntoButton;
	private Button stepOverButton;
	private Button stepReturnButton;
	
	public AbstractProgramRuntimeControlComposite(Composite parent, int style) 
	{
		super(parent, style);		
		
		setLayout(new GridLayout(7, false))	;
		
		initButton = new Button(this, SWT.PUSH);
		initButton.setText("Init");
		initButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{				
				if(abstractProgramRuntime != null)
				{
					try
					{
						abstractProgramRuntime.init();
					}
					catch(Throwable t)
					{
						t.printStackTrace();
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {								
			}
		});
		
		terminateButton = new Button(this, SWT.PUSH);
		terminateButton.setText("Terminate");
		terminateButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{				
				if(abstractProgramRuntime != null)
				{
					try
					{
						abstractProgramRuntime.terminate();
					}
					catch(Throwable t)
					{
						t.printStackTrace();
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {								
			}
		});
				
		resumeButton = new Button(this, SWT.PUSH);
		resumeButton.setText("Resume");
		resumeButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{				
				if(abstractProgramRuntime != null)
				{
					try
					{
						abstractProgramRuntime.resume();
					}
					catch(Throwable t)
					{
						t.printStackTrace();
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {								
			}
		});
		
		suspendButton = new Button(this, SWT.PUSH);
		suspendButton.setText("Suspend");
		suspendButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{				
				if(abstractProgramRuntime != null)
				{
					try
					{
						abstractProgramRuntime.suspend();
					}
					catch(Throwable t)
					{
						t.printStackTrace();
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {								
			}
		});
		
		stepIntoButton = new Button(this, SWT.PUSH);
		stepIntoButton.setText("Step Into");
		stepIntoButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{				
				if(abstractProgramRuntime != null)
				{
					try
					{
						abstractProgramRuntime.stepInto();
					}
					catch(Throwable t)
					{
						t.printStackTrace();
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {								
			}
		});
		
		stepOverButton = new Button(this, SWT.PUSH);
		stepOverButton.setText("Step Into");
		stepOverButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{				
				if(abstractProgramRuntime != null)
				{
					try
					{
						abstractProgramRuntime.stepOver();
					}
					catch(Throwable t)
					{
						t.printStackTrace();
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {								
			}
		});
		
		stepReturnButton = new Button(this, SWT.PUSH);
		stepReturnButton.setText("Step Into");
		stepReturnButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{				
				if(abstractProgramRuntime != null)
				{
					try
					{
						abstractProgramRuntime.stepReturn();
					}
					catch(Throwable t)
					{
						t.printStackTrace();
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {								
			}
		});
		
	}

	public AbstractProgramRuntime getAbstractProgramRuntime() 
	{
		return abstractProgramRuntime;
	}

	public void setAbstractProgramRuntime(AbstractProgramRuntime abstractProgramRuntime) 
	{
		this.abstractProgramRuntime = abstractProgramRuntime;
	}	
}

package ca.gc.asc_csa.apogy.core.invocator.ui.parts;

import java.util.HashMap;

import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.parts.AbstractFormPropertiesPart;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIRCPConstants;

public class SessionElementPropertiesPart extends AbstractFormPropertiesPart {	
	@Override
	protected boolean isReadOnly() {
		return false;
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() {
		HashMap<String, ISelectionListener> selectionProvidersIdsToSelectionListeners = new HashMap<String, ISelectionListener>();

		selectionProvidersIdsToSelectionListeners.put(ApogyCoreInvocatorUIRCPConstants.PART__SESSION_EDITOR__ID,
				DEFAULT_LISTENER);

		return selectionProvidersIdsToSelectionListeners;
	}
}
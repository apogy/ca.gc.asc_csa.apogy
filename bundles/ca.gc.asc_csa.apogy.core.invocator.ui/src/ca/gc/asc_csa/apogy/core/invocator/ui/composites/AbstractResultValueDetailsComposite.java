/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 		 Regent L'Archevesque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.common.converters.ApogyCommonConvertersFacade;
import ca.gc.asc_csa.apogy.common.converters.IFileExporter;
import ca.gc.asc_csa.apogy.common.converters.ui.wizards.ExportToFileWizard;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.invocator.AbstractResultValue;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallResult;

public class AbstractResultValueDetailsComposite extends Composite 
{
	private AbstractResultValue resultValue;
	private Object value = null;
	
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());		
	private ScrolledForm scrldResults;
	private Section sctnProductDetails;
	
	private Button exportButton;	
	private Composite compositeDetails;
	
	public AbstractResultValueDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new GridLayout(1, false));

		scrldResults = formToolkit.createScrolledForm(this);
		scrldResults.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrldResults.setBounds(0, 0, 186, 165);
		formToolkit.paintBordersFor(scrldResults);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			tableWrapLayout.numColumns = 1;			
			scrldResults.getBody().setLayout(tableWrapLayout);
		}
		
		// Abstract Result Details		
		sctnProductDetails = formToolkit.createSection(scrldResults.getBody(), Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnNewSection = new TableWrapData(TableWrapData.FILL, TableWrapData.FILL, 1, 1);		
		twd_sctnNewSection.grabHorizontal = true;
		twd_sctnNewSection.grabVertical = true;		
		twd_sctnNewSection.valign = TableWrapData.FILL;		
		sctnProductDetails.setLayoutData(twd_sctnNewSection);
		// sctnProductDetails.setBounds(0, 0, 112, 27);
		formToolkit.paintBordersFor(sctnProductDetails);
		sctnProductDetails.setText("Return Value Details");
		
		
		// Details
		Composite detailsTop = new Composite(sctnProductDetails, SWT.NONE);
		detailsTop.setLayout(new GridLayout(1,false));
		
		// Export.
		exportButton = new Button(detailsTop, SWT.PUSH);
		exportButton.setText("Export Data Product to File...");
		exportButton.setToolTipText("Export this Data Product to file(s).");
		exportButton.setEnabled(false);
		exportButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				IFileExporter iFileExporter = ApogyCommonConvertersFacade.INSTANCE.getIFileExporter(value);
				ExportToFileWizard wizard = new ExportToFileWizard(iFileExporter, value);
				WizardDialog dialog = new WizardDialog(getShell(), wizard);
				dialog.open();	
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		// Details
		compositeDetails = new Composite(detailsTop, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_compositeDetails.widthHint = 398;
		gd_compositeDetails.minimumWidth = 398;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));	
		
		formToolkit.adapt(detailsTop);
		formToolkit.paintBordersFor(detailsTop);
		sctnProductDetails.setClient(detailsTop);	
		sctnProductDetails.setExpanded(true);			
	}

	public AbstractResultValue getAbstractResultValue() {
		return resultValue;
	}

	public void setAbstractResultValue(AbstractResultValue resultValue) 
	{
		this.resultValue = resultValue;
					
		if(!isDisposed())
		{		
						
			// Dispose of the compositeDetails children.
			for (Control control : compositeDetails.getChildren()) 
			{
				control.dispose();
			}
			
			if(resultValue != null)
			{
				value = ApogyCoreInvocatorFacade.INSTANCE.getResultValue(resultValue.getResult());
				
				if(value instanceof EObject)
				{					
					EObject eObject = (EObject) value;
																	
					// VView viewModel = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createDefaultViewModel(eObject);
					// ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, eObject, viewModel);
					ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, eObject);										
					if(!exportButton.isDisposed()) exportButton.setEnabled(canBeExportedToFile(eObject));											

				}
				else if(value != null)
				{
					if(!exportButton.isDisposed()) exportButton.setEnabled(false);
					if(!compositeDetails.isDisposed()) createValueLabel(compositeDetails, resultValue);								
				}
				else
				{
					if(!exportButton.isDisposed()) exportButton.setEnabled(false);
					ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, null, "No data to display");	
				}
				
				if(!compositeDetails.isDisposed()) compositeDetails.layout();
			}
		}
	}
	
	protected void createValueLabel(Composite parent, AbstractResultValue resultValue)	
	{
		parent.setLayout(new GridLayout(1,false));
		
		Object value = ApogyCoreInvocatorFacade.INSTANCE.getResultValue(resultValue.getResult());
		
		Composite baseComposite = new Composite(parent, SWT.NONE);		
		baseComposite.setLayout(new GridLayout(3, false));
		baseComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		
		Label resultLabel = new Label(baseComposite, SWT.NONE);
		resultLabel.setText("Result : ");
				
		Text valueLabel = new Text(baseComposite, SWT.NONE);
		valueLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));		
		valueLabel.setText(value.toString());	
		valueLabel.setEditable(false);
		
		Label unitsLabel = new Label(baseComposite, SWT.NONE);
		
		if(resultValue.getResult() instanceof OperationCallResult)
		{
			OperationCallResult operationCallResult = (OperationCallResult) resultValue.getResult();
			if(operationCallResult.getOperationCall() != null)
			{
				String units = ApogyCommonEMFFacade.INSTANCE.getEngineeringUnitsAsString(operationCallResult.getOperationCall().getEOperation());
				if(units != null) unitsLabel.setText(units);
			}
			else
			{
				unitsLabel.setText("No units applicable.");
			}
		}		
	}
	
	protected boolean canBeExportedToFile(EObject eObject)
	{					
		return (ApogyCommonConvertersFacade.INSTANCE.getIFileExporter(eObject) != null);
	}		
}

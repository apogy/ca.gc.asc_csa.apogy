package ca.gc.asc_csa.apogy.core.invocator.ui.handlers;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.impl.PartImpl;

import ca.gc.asc_csa.apogy.core.invocator.AbstractProgramRuntime;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ProgramFactoriesRegistry;
import ca.gc.asc_csa.apogy.core.invocator.ProgramFactory;
import ca.gc.asc_csa.apogy.core.invocator.ProgramSettings;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.ProgramPart;

@SuppressWarnings("restriction")
public abstract class AbstractProgramRuntimeHandler 
{
	public boolean canExecute(MPart part)
	{
		if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() == null
				|| ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment() == null
				|| ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment().getActiveContext() == null
				|| !ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment().getActiveContext().isVariablesInstantiated()) {
			return false;
		}

		if (part instanceof PartImpl && ((PartImpl) part).getObject() instanceof ProgramPart) 
		{
			ProgramPart programPart = (ProgramPart) ((PartImpl) part).getObject();
			Program program = programPart.getSelectedProgram();					
			return (program != null);		
		}
		return false;
	}
	
	public AbstractProgramRuntime createAbstractProgramRuntime(Program program, ProgramSettings programSettings)
	{
		AbstractProgramRuntime runtime = null;
		
		if(program != null)
		{
			ProgramFactory programFactory = ProgramFactoriesRegistry.INSTANCE.getFactory(program.eClass());
			
			if(programFactory != null)
			{
				runtime = programFactory.createProgramRuntime(program, programSettings);				
			}
		}
		
		return runtime;
	}
}

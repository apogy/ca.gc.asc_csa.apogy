package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

public class TextScriptEditorComposite extends Composite 
{
	private String relativeScriptPath;
	private StyledText styledText;
	private boolean isDirty = false;
	
	public TextScriptEditorComposite(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(1, false));
		
		styledText = new StyledText(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);	
		styledText.setEditable(true);
		styledText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		
		styledText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent arg0) 
			{
				setDirty(true);
			}
		});
	}

	public String getScriptRelativePath() 
	{
		return relativeScriptPath;
	}

	public void setScriptPath(String relativeScriptPath) 
	{
		this.relativeScriptPath = relativeScriptPath;
		
		if(relativeScriptPath != null)
		{
			try 
			{				
				styledText.setText(loadScript(relativeScriptPath));
			} 
			catch (Exception e) 
			{
				styledText.setText(null);
			}
		}
		else
		{
			styledText.setText(null);
		}
		
		setDirty(false);
	}
	
	/**
	 * Return whether or not the displayed text has been modified since opened.
	 * @return True if the displayed text has been modified, false if the changes have been changed. 
	 */
	public boolean isDirty() {
		return isDirty;
	}
	
	private void setDirty(boolean isDirty)
	{
		this.isDirty = isDirty;
		fileDirtyChanged(isDirty);
	}
	
	/**
	 * Method called when the status of the editor has changed.
	 * @param isDirty True if the displayed text has been modified, false if the changes have been changed.
	 */
	protected void fileDirtyChanged(boolean isDirty)
	{
	}
	
	/**
	 * Saves the displayed text to the file being edited.
	 * @throws Exception An exception if the specified file is not found or cannot be written to.
	 */
	public void saveScript() throws Exception
	{
		// Save content to file	
		IPath absolutePath = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(relativeScriptPath);	
		BufferedWriter out = new BufferedWriter(new FileWriter(absolutePath.toOSString()));
		out.write(styledText.getText());
		out.flush();
		out.close();
		
		// Update Dirty flag.
		setDirty(false);
	}
	
	/**
	 * Load a script from a relative path.
	 * @param relativeScriptPath The path of the script file, relative the workspace.
	 * @return The script as a string, null if none if found.
	 * @throws An exception if the specified file is not found or cannot be opened.
	 */
	private String loadScript(String relativeScriptPath) throws Exception
	{
		IPath absolutePath = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(relativeScriptPath);						
		BufferedReader in = new BufferedReader(new FileReader(absolutePath.toOSString()));
		String line = null;
		StringBuffer sb = new StringBuffer();
		while ((line = in.readLine()) != null) 
		{
			sb.append(line + "\n");
		}
		in.close();
		
		return sb.toString();
	}
}

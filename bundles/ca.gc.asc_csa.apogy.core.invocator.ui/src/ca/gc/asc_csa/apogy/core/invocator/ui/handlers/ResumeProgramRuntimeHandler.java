package ca.gc.asc_csa.apogy.core.invocator.ui.handlers;

import java.util.List;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.impl.PartImpl;

import ca.gc.asc_csa.apogy.core.invocator.AbstractProgramRuntime;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ProgramRuntimeState;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.ProgramPart;

@SuppressWarnings("restriction")
public class ResumeProgramRuntimeHandler extends AbstractProgramRuntimeHandler {

	@Execute
	public void execute(MPart part) {

		ProgramPart programPart = (ProgramPart) ((PartImpl) part).getObject();

		Program program = programPart.getSelectedProgram();

		List<AbstractProgramRuntime> runtimes = programPart.getRuntimes();
		if (runtimes.size() > 0) {
			for (int i = runtimes.size() - 1; i >= 0; i--) {
				AbstractProgramRuntime runtime = runtimes.get(i);
				if (runtime.getProgram() == program) {
					runtime.resume();
					break;
				}
			}
		}
	}

	@CanExecute
	public boolean canExecute(MPart part) {
		if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() == null) {
			return false;
		}

		if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getProgramRuntimesList() == null) 
		{
			return false;
		}
		
		// if (part instanceof PartImpl && ((PartImpl) part).getObject() instanceof ProgramPart) {
		if (part instanceof PartImpl && ((PartImpl) part).getObject() instanceof ProgramPart) {
			ProgramPart programPart = (ProgramPart) ((PartImpl) part).getObject();
			Program program = programPart.getSelectedProgram();		
			
			if(program == null) return false;
			
			for (AbstractProgramRuntime runtime :programPart.getRuntimes()) {
				if (programPart.getRuntimes().indexOf(runtime) == programPart.getRuntimes().size() - 1
						&& runtime.getProgram() == program
						&& runtime.getState() == ProgramRuntimeState.SUSPENDED
						|| runtime.getState() == ProgramRuntimeState.INITIALIZED) {
					/**
					 * Can be executed if the runtime is suspended or
					 * initialized
					 */
					return true;
				}
			}
						
		}
		return false;
	}
}
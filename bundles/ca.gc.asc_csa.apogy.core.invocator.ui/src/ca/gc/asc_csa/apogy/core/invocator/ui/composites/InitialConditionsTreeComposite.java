package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.invocator.AbstractInitialConditions;
import ca.gc.asc_csa.apogy.core.invocator.InitialConditions;
import ca.gc.asc_csa.apogy.core.invocator.TypeMemberInitialConditions;
import ca.gc.asc_csa.apogy.core.invocator.VariableInitialConditions;

public class InitialConditionsTreeComposite extends Composite {

	private InitialConditions initialConditions;
	
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		
	private TreeViewer treeViewer;
	
	private DataBindingContext m_currentDataBindings;
	
	public InitialConditionsTreeComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new GridLayout(2, false));
		
		treeViewer = new TreeViewer(this);
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener(){

			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				Object selection = ((IStructuredSelection) event.getSelection()).getFirstElement();
				
				if(selection instanceof VariableInitialConditions)
				{
					newVariableInitialConditionsSelected((VariableInitialConditions) selection);
				}
				else if(selection instanceof TypeMemberInitialConditions)
				{
					newTypeMemberInitialConditionsSelected((TypeMemberInitialConditions) selection);
				}
			}
			
		});
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.setContentProvider(getContentProvider());
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2);
		gridData.widthHint = 200;
		gridData.minimumWidth = 200;
		treeViewer.getTree().setLayoutData(gridData);
		
		// Dispose
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{				
				if(m_currentDataBindings != null) m_currentDataBindings.dispose();
			}
		});
	}

	public InitialConditions getInitialConditions() {
		return initialConditions;
	}

	public void setInitialConditions(InitialConditions initialConditions) 
	{
		if(m_currentDataBindings != null)
		{
			m_currentDataBindings.dispose();
		}
		
		this.initialConditions = initialConditions;
		
		if(initialConditions != null)
		{
			m_currentDataBindings = initDataBindingsCustom();
			
			treeViewer.setInput(initialConditions);
		}
		else
		{
			treeViewer.setInput(null);
		}
		
		refreshViewer();		
	}
	
	protected void newVariableInitialConditionsSelected(VariableInitialConditions variableInitialConditions)
	{
		
	}
	
	protected void newTypeMemberInitialConditionsSelected(TypeMemberInitialConditions typeMemberInitialConditions)
	{
		
	}
	
	protected void refreshViewer()
	{
		if (!treeViewer.isBusy()) 
		{
			getDisplay().asyncExec(new Runnable() 
			{
				@Override
				public void run() 
				{
					treeViewer.refresh();					
				}
			});
		}
	}
	
	protected DataBindingContext initDataBindingsCustom() {

		DataBindingContext bindingContext = new DataBindingContext();
	
		return bindingContext;
	}
	
	protected AdapterFactoryContentProvider getContentProvider() 
	{
		return new AdapterFactoryContentProvider(adapterFactory) 
		{
			@Override
			public Object[] getElements(Object object) 
			{
				if(object instanceof InitialConditions)
				{
					return ((InitialConditions) object).getVariableInitialConditions().toArray();
				}
				return null;
			}

			@Override
			public Object[] getChildren(Object object) 
			{
				if(object instanceof InitialConditions)
				{
					return ((InitialConditions) object).getVariableInitialConditions().toArray();
				}
				else if(object instanceof AbstractInitialConditions)
				{
					return ((AbstractInitialConditions) object).getTypeMembersInitialConditions().toArray();
				}
				else
				{
					return null;
				}
			}
			
			@Override
			public boolean hasChildren(Object object) 
			{
				if(object instanceof InitialConditions)
				{
					return !((InitialConditions) object).getVariableInitialConditions().isEmpty();
				}
				else if(object instanceof AbstractInitialConditions)
				{
					return !((AbstractInitialConditions) object).getTypeMembersInitialConditions().isEmpty();
				}
				else
				{
					return false;
				}
			}
		};
	}
}

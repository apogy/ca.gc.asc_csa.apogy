package ca.gc.asc_csa.apogy.core.invocator.ui.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFactory;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.InitialConditionsList;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.InitialConditionsComposite;

public class InitialConditionPart extends AbstractSessionBasedPart
{	
	private InitialConditionsComposite initialConditionsComposite;
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{
		InitialConditionsList initialConditionsList = null;
		
		if(invocatorSession != null)
		{
			initialConditionsList = invocatorSession.getInitialConditionsList();
			
			// If there is no Initial Condition List defined, creates one.
			if(initialConditionsList == null)
			{
				initialConditionsList = ApogyCoreInvocatorFactory.eINSTANCE.createInitialConditionsList();
				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(invocatorSession, ApogyCoreInvocatorPackage.Literals.INVOCATOR_SESSION__INITIAL_CONDITIONS_LIST, initialConditionsList);
			}
		}
		
		initialConditionsComposite.setInitialConditionsList(initialConditionsList);
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{		
		initialConditionsComposite = new InitialConditionsComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
	}
}

package ca.gc.asc_csa.apogy.core.invocator.ui.renderers;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.inject.Inject;
import javax.measure.unit.Unit;

import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.Ranges;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.renderers.AbstractUnitControlCompositeRenderer;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.core.invocator.EDataTypeArgument;

public class EDataTypeArgumentControlCompositeRenderer extends AbstractUnitControlCompositeRenderer {

	private EOperationEParametersFormatProviderParameters formatParams;
	private EOperationEParametersUnitsProviderParameters unitsParams;

	@Inject
	public EDataTypeArgumentControlCompositeRenderer(VControl vElement, ViewModelContext viewContext,
			ReportService reportService, EMFFormsDatabinding emfFormsDatabinding,
			EMFFormsLabelProvider emfFormsLabelProvider, VTViewTemplateProvider vtViewTemplateProvider) {
		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);

		formatParams = ApogyCommonEMFUIFactory.eINSTANCE.createEOperationEParametersFormatProviderParameters();
		formatParams.setParam(getEParameter());
		unitsParams = ApogyCommonEMFUIFactory.eINSTANCE.createEOperationEParametersUnitsProviderParameters();
		unitsParams.setParam(getEParameter());
	}

	@Override
	protected UpdateValueStrategy getUpdateModelValueStrategy() {
		return new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
				.setConverter(new Converter(String.class, String.class) {
					@Override
					public Object convert(Object fromObject) {
						if (fromObject != null && !"".equals(fromObject)) {
							try {
								Number number = ApogyCommonEMFUIFacade.INSTANCE.convertToNativeUnits(
										Double.parseDouble((String) fromObject), getNativeUnits(), getDisplayUnits(),
										getTypedElement().getEType());

								/** Format to update the text if there is rounding errors */
								Unit<?> displayUnits = getDisplayUnits();
								Unit<?> nativeUnits = getNativeUnits();
								DecimalFormat format = getDecimalFormat();
								
								if (displayUnits != null && !displayUnits.equals(nativeUnits)) {
									textValue.setText(format.format(nativeUnits.getConverterTo(displayUnits).convert(number.doubleValue())));
								}else{
									textValue.setText(format.format(number.doubleValue()));
								}

								/** Set the new value in the right type */
								EClassifier classifier = getTypedElement().getEType();
								if (classifier == EcorePackage.Literals.EFLOAT) {
									number = number.floatValue();
								} else if (classifier == EcorePackage.Literals.EBYTE) {
									number = number.byteValue();
								} else if (classifier == EcorePackage.Literals.ESHORT) {
									number = number.shortValue();
								} else if (classifier == EcorePackage.Literals.EINT) {
									number = number.intValue();
								} else if (classifier == EcorePackage.Literals.ELONG) {
									number = number.longValue();
								}

								return number.toString();
							} catch (Exception e) {
								/** Error message */
								MessageDialog.openError(textValue.getDisplay().getActiveShell(), "Invalid Number",
										"The number entered is invalid. The value will be unset.");
							}
						}
						/** Set to the current value */
						textValue.setText(getFormatedValue());
						return getValue();
					}
				});
	}

	@Override
	protected Number getValue() {
		try {
			if (!getModelValue().isDisposed() && getModelValue().getValue() != null) {
				return NumberFormat.getInstance().parse((String) getModelValue().getValue());
			} else {
				return 0;
			}
		} catch (DatabindingFailedException | ParseException e) {
			Logger.INSTANCE.log(Activator.ID, "Error getting the value. ", EventSeverity.ERROR);
		}
		return null;
	}

	@Override
	protected UpdateValueStrategy getUpdateTextValueStrategy() {
		return new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
				.setConverter(new Converter(String.class, String.class) {

					@Override
					public Object convert(Object fromObject) {
						if (fromObject != null) {
							String value = getFormatedValue();
							return value == null ? "" : value;
						}
						return "";
					}
				});
	}

	protected EParameter getEParameter() {
		EObject element = getViewModelContext().getDomainModel();

		if (element instanceof EDataTypeArgument) {
			return ((EDataTypeArgument) element).getEParameter();
		}

		return null;
	}

	@Override
	protected void addToDisplayUnitsRegistry(Unit<?> resultUnits) {
		SimpleUnitsProvider simpleUnitsProvider = ApogyCommonEMFUIFactory.eINSTANCE.createSimpleUnitsProvider();
		simpleUnitsProvider.setUnit(resultUnits);
		UnitsProvider provider = DisplayUnitsRegistry.INSTANCE.getEntriesMap().getEntries()
				.get(getEParameter().getEOperation());

		if (provider instanceof EOperationEParametersUnitsProvider) {
			((EOperationEParametersUnitsProvider) provider).getMap().getEntries().put(getEParameter(),
					simpleUnitsProvider);
			DisplayUnitsRegistry.INSTANCE.save();
		} else {
			EOperationEParametersUnitsProvider eOpeParamProvider = ApogyCommonEMFUIFactory.eINSTANCE
					.createEOperationEParametersUnitsProvider();

			eOpeParamProvider.getMap().getEntries().put(getEParameter(), simpleUnitsProvider);

			ApogyCommonEMFUIFacade.INSTANCE.addUnitsProviderToRegistry(getEParameter().getEOperation(),
					eOpeParamProvider);
		}
	}

	@Override
	protected void addToDecimalFormatRegistry(DecimalFormat resultFormat) {
		SimpleFormatProvider simpleFormatProvider = ApogyCommonEMFUIFactory.eINSTANCE.createSimpleFormatProvider();
		simpleFormatProvider.setFormatPattern(resultFormat.toPattern());
		FormatProvider provider = DecimalFormatRegistry.INSTANCE.getEntriesMap().getEntries()
				.get(getEParameter().getEOperation());

		if (provider instanceof EOperationEParametersFormatProvider) {
			((EOperationEParametersFormatProvider) provider).getMap().getEntries().put(getEParameter(),
					simpleFormatProvider);
			DecimalFormatRegistry.INSTANCE.save();
		} else {
			EOperationEParametersFormatProvider eOpeParamProvider = ApogyCommonEMFUIFactory.eINSTANCE
					.createEOperationEParametersFormatProvider();

			eOpeParamProvider.getMap().getEntries().put(getEParameter(), simpleFormatProvider);

			ApogyCommonEMFUIFacade.INSTANCE.addFormatProviderToRegistry(getEParameter().getEOperation(),
					eOpeParamProvider);
		}
	}

	@Override
	protected Unit<?> getNativeUnits() {
		return ApogyCommonEMFFacade.INSTANCE.getEngineeringUnits(getEParameter());
	}

	@Override
	protected Unit<?> getDisplayUnits() {
		Unit<?> units = ApogyCommonEMFUIFacade.INSTANCE.getDisplayUnits(getEParameter().getEOperation(), unitsParams);
		if (units == null) {
			units = ApogyCommonEMFUIFacade.INSTANCE.getDisplayUnits(getEParameter());
		}
		return units;
	}

	@Override
	protected DecimalFormat getDecimalFormat() {
		DecimalFormat format = ApogyCommonEMFUIFacade.INSTANCE.getDisplayFormat(getEParameter().getEOperation(),
				formatParams);
		if (format == null) {
			format = ApogyCommonEMFUIFacade.INSTANCE.getDisplayFormat(getEParameter());
		}
		return format;
	}

	@Override
	protected String getFormatedValue() {
		if(getValue() != null){
			Double valueDouble = getValue().doubleValue();
			
			DecimalFormat format = ApogyCommonEMFUIFacade.INSTANCE.getDisplayFormat(getEParameter().getEOperation(), formatParams);
			Unit<?> displayUnits = ApogyCommonEMFUIFacade.INSTANCE.getDisplayUnits(getEParameter().getEOperation(), unitsParams);
			Unit<?> nativeUnits = ApogyCommonEMFFacade.INSTANCE.getEngineeringUnits(getEParameter());
			
			if(displayUnits != null && !displayUnits.equals(nativeUnits)){
				valueDouble = nativeUnits.getConverterTo(displayUnits).convert(valueDouble);
			}
					
			return format.format(valueDouble);
		}else{
			return "0";
		}
	}
	
	@Override
	protected Ranges getRange() {
		return ApogyCommonEMFFacade.INSTANCE.getRange(getEParameter(), getValue());
	}
}
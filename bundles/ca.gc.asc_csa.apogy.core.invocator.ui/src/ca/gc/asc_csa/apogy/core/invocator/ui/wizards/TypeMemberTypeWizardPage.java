/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.invocator.ui.wizards;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.TypeMember;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.TypesRegistryComposite;

public class TypeMemberTypeWizardPage extends WizardPage
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.invocator.ui.wizards.TypeMemberTypeWizardPage";
	
	private TypeMember typeMember;	
	
	public TypeMemberTypeWizardPage(TypeMember typeMember)
	{
		super(WIZARD_PAGE_ID);			
		setTitle("Type of Member");
		setDescription("Please select the Type of the member.");
		
		this.typeMember = typeMember;
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(1, false));
				
		setControl(container);
		
		TypesRegistryComposite typesRegistryComposite = new TypesRegistryComposite(container, SWT.NONE)
		{
			@Override
			protected void newSelection(ISelection selection) 
			{
				ApogyCommonTransactionFacade.INSTANCE.basicSet(typeMember, ApogyCoreInvocatorPackage.Literals.TYPE_MEMBER__MEMBER_TYPE, getSelectedType(), true);
				validate();
			}
		};
		typesRegistryComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		validate();
	}
	
	/** 
	 * This method is invoked to validate the content. 
	 */
	protected void validate() {	
		String errorMessage = "";		
				
		if(typeMember.getMemberType() == null)
		{
			errorMessage = "You must select the memeber type.";
		}
		setPageComplete(errorMessage.isEmpty());
	}
}

package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.EIdComparator;
import ca.gc.asc_csa.apogy.core.invocator.Variable;

public class VariableComboComposite extends Composite 
{
	private List<Variable> variableList = new ArrayList<Variable>();
	private Variable selectedVariable = null;
	private ComboViewer comboViewer;
	
	public VariableComboComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new FillLayout());
		comboViewer = createCombo(this, SWT.READ_ONLY);		
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{		
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{	
				if(event.getSelection().isEmpty())
				{
					newSelection(null);
				}
				else if(event.getSelection() instanceof IStructuredSelection)
				{
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					if(iStructuredSelection.getFirstElement() instanceof Variable)
					{
						newSelection((Variable)iStructuredSelection.getFirstElement());
					}
					else
					{
						newSelection(null);
					}
				}
			}
		});		
	}

	private ComboViewer createCombo(Composite parent, int style)
	{
		ComboViewer comboViewer = new ComboViewer(parent, SWT.DROP_DOWN | SWT.READ_ONLY);		
		comboViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboViewer.setLabelProvider(new LabelProvider()
		{
			@Override
			public String getText(Object element) 
			{
				if(element instanceof Variable)
				{
					Variable variable = (Variable) element;
					return variable.getName();
				}
				else
				{
					return "";
				}
			}
		});
				
		// Display the combo element sorted by displayed name.
		comboViewer.setComparator(new ViewerComparator()
		{
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) 
			{
				Variable variable1 = (Variable) e1;
				Variable variable2 = (Variable) e2;
				
				int result = variable1.getName().compareTo(variable2.getName());
				if(result == 0)
				{
					EIdComparator<Variable> comparator = ApogyCommonEMFFactory.eINSTANCE.createEIdComparator();
					return comparator.compare(variable1, variable2);
				}
				else
				{
					return result;
				}
			}
		});
						
		comboViewer.setInput(variableList);
				
		return comboViewer;
	}		
	
	
	
	public Variable getSelectedVariable() {
		return selectedVariable;
	}

	public void setSelectedVariable(Variable selectedVariable) 
	{
		this.selectedVariable = selectedVariable;
		
		if(selectedVariable != null)
		{
			comboViewer.setSelection(new StructuredSelection(selectedVariable));
		}
	}

	public List<Variable> getVariableList() 
	{
		return variableList;
	}

	public void setVariableList(List<Variable> newVariableList) 
	{
		this.variableList.clear();
		if(newVariableList != null)
		{
			this.variableList.addAll(newVariableList);
		}
		comboViewer.setInput(variableList);
		
		// Attempts to reselect the variable.
		if(this.selectedVariable != null)
		{
			comboViewer.setSelection(new StructuredSelection(this.selectedVariable));
		}
	}

	protected void newSelection(Variable variable)
	{		
	}
}

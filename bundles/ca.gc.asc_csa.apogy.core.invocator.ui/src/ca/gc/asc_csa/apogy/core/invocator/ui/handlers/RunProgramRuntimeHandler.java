package ca.gc.asc_csa.apogy.core.invocator.ui.handlers;

import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.impl.PartImpl;

import ca.gc.asc_csa.apogy.core.invocator.AbstractProgramRuntime;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ProgramSettings;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIRCPConstants;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.ProgramPart;

@SuppressWarnings("restriction")
public class RunProgramRuntimeHandler extends AbstractProgramRuntimeHandler{

	@Execute
	public void execute(
			@Optional @Named(ApogyCoreInvocatorUIRCPConstants.COMMAND_PARAMETER__RUN_PROGRAM_RUNTIME_STEP_BY_STEP__ID) String stepByStep, MPart part) 
	{
		ProgramPart programPart = (ProgramPart) ((PartImpl) part).getObject();

		Program program = programPart.getSelectedProgram();
		ProgramSettings programSettings = null;
		
		AbstractProgramRuntime programRuntime = createAbstractProgramRuntime(program, programSettings);
		if(programRuntime != null)
		{
			programPart.newRuntime(programRuntime);
			programRuntime.init();			
			programRuntime.resume();			
		}				
	}
	
}
package ca.gc.asc_csa.apogy.core.invocator.ui.handlers;

import java.util.List;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.impl.PartImpl;

import ca.gc.asc_csa.apogy.core.invocator.AbstractProgramRuntime;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallsList;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.OperationCallsListComposite;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.ProgramPart;

@SuppressWarnings("restriction")
public class StepIntoProgramRuntimeHandler extends AbstractProgramRuntimeHandler {

	@Execute
	public void execute(MPart part) {

		ProgramPart programPart = (ProgramPart) ((PartImpl) part).getObject();

		OperationCallsList operationCallsList = ((OperationCallsListComposite) programPart.getActualComposite())
				.getOperationCallsList();

		List<AbstractProgramRuntime> runtimes = programPart.getRuntimes();
		if (runtimes.size() > 0) {
			for (int i = runtimes.size() - 1; i >= 0; i--) {
				AbstractProgramRuntime runtime = runtimes.get(i);
				if (runtime.getProgram() == operationCallsList) {
					runtime.stepInto();
					break;
				}
			}
		}

	}

	@CanExecute
	public boolean canExecute(MPart part) {
		if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() == null) {
			return false;
		}

		if (ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getProgramRuntimesList() == null) {
			return false;
		}

		if (part instanceof PartImpl && ((PartImpl) part).getObject() instanceof ProgramPart) 
		{
			ProgramPart programPart = (ProgramPart) ((PartImpl) part).getObject();
			Program program = programPart.getSelectedProgram();					
			return (program != null);	
		}
		return false;
	}
}
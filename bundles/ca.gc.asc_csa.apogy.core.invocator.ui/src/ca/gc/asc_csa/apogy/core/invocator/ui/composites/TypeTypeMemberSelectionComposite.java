package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.invocator.Type;
import ca.gc.asc_csa.apogy.core.invocator.TypeMember;

public class TypeTypeMemberSelectionComposite extends Composite 
{
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	private Type type;	
	private TypeMember selectedTypeMember;
	private ComboViewer comboViewer;
	
	public TypeTypeMemberSelectionComposite(Composite parent, int style) 
	{
		this(parent, style, null);		
	}

	/**
	 * 
	 * @param parent The composite parent.
	 * @param style The composite style.
	 * @param superType Super type for which all sub classes are to be show. Null shows all available EClasses.
	 */
	public TypeTypeMemberSelectionComposite(Composite parent, int style, Type type) 
	{
		super(parent, style);
		setLayout(new FillLayout());
		
		this.type = type;
		
		comboViewer = createCombo(this, SWT.READ_ONLY);		
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{		
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{	
				if(event.getSelection().isEmpty())
				{
					selectedTypeMember = null;
					newEClassSelected(selectedTypeMember);
					comboViewer.getCombo().setToolTipText("");
				}
				else if(event.getSelection() instanceof IStructuredSelection)
				{
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					if(iStructuredSelection.getFirstElement() instanceof EClass)
					{
						selectedTypeMember = (TypeMember) iStructuredSelection.getFirstElement();
						comboViewer.getCombo().setToolTipText(selectedTypeMember.getName());
						newEClassSelected(selectedTypeMember);
					}
					else
					{
						selectedTypeMember = null;
						comboViewer.getCombo().setToolTipText("");
						newEClassSelected(selectedTypeMember);
					}
				}
			}
		});	
	}
	
	public Type getType() 
	{
		return type;
	}

	public void setType(Type newType) 
	{
		this.type = newType;
		
		if(newType != null)
		{
			comboViewer.setInput(type.getMembers());
		}
	}

	public ComboViewer getComboViewer() {
		return comboViewer;
	}

	public TypeMember getSelectedTypeMember() 
	{
		return selectedTypeMember;
	}

	public void select(TypeMember typeMember)
	{
		if(comboViewer != null && !comboViewer.getCombo().isDisposed())
		{
			if(typeMember != null)
			{
				comboViewer.setSelection(new StructuredSelection(typeMember), true);
			}
			else
			{
				comboViewer.setSelection(new StructuredSelection(), true);
			}
		}
	}
	
	private ComboViewer createCombo(Composite parent, int style)
	{
		ComboViewer comboViewer = new ComboViewer(parent, SWT.DROP_DOWN);		
		comboViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
				
		// Display the combo element sorted by displayed name.
		comboViewer.setComparator(new ViewerComparator()
		{
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) 
			{
				TypeMember typeMember1 = (TypeMember) e1;
				TypeMember typeMember2 = (TypeMember) e2;
				
				String name1 = typeMember1.getName();
				String name2 = typeMember2.getName();
								
				return name1.compareTo(name2);				
			}
		});
		
		if(type != null && type.getMembers() != null)
		{
			comboViewer.setInput(type.getMembers());
		}
		
		return comboViewer;
	}	
	
	protected void newEClassSelected(TypeMember newTypeMember)
	{		
	}
}

package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.invocator.AbstractResult;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallResult;

public class AbstractResultDetailsComposite extends Composite 
{
	private AbstractResult result;
	
	private ScrolledForm scrldResults;
	
	private Section sctnProductDetails;
	private Composite compositeAbstractResultDetails;
	
	private Section sctnOpsCallDetails;
	private Composite compositeOpsCallDetails;
	
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	public AbstractResultDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(1, false));

		scrldResults = formToolkit.createScrolledForm(this);
		scrldResults.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrldResults.setBounds(0, 0, 186, 165);
		formToolkit.paintBordersFor(scrldResults);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			tableWrapLayout.numColumns = 1;			
			scrldResults.getBody().setLayout(tableWrapLayout);
		}
		
		// Abstract Result Details		
		sctnProductDetails = formToolkit.createSection(scrldResults.getBody(), Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnNewSection = new TableWrapData(TableWrapData.FILL, TableWrapData.FILL, 1, 1);		
		twd_sctnNewSection.grabHorizontal = true;
		twd_sctnNewSection.grabVertical = true;		
		twd_sctnNewSection.valign = TableWrapData.FILL;		
		sctnProductDetails.setLayoutData(twd_sctnNewSection);
		// sctnProductDetails.setBounds(0, 0, 112, 27);
		formToolkit.paintBordersFor(sctnProductDetails);
		sctnProductDetails.setText("Product Details");
						
		compositeAbstractResultDetails = new Composite(sctnProductDetails, SWT.NONE);						
		compositeAbstractResultDetails.setLayout(new GridLayout(1, false));
		
		formToolkit.adapt(compositeAbstractResultDetails);
		formToolkit.paintBordersFor(compositeAbstractResultDetails);
		sctnProductDetails.setClient(compositeAbstractResultDetails);	
		sctnProductDetails.setExpanded(true);
		
		// OpsCall Result Details.		
		sctnOpsCallDetails = formToolkit.createSection(scrldResults.getBody(), Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnOpsCallDetails = new TableWrapData(TableWrapData.FILL, TableWrapData.FILL, 1, 1);
		twd_sctnOpsCallDetails.grabHorizontal = true;
		twd_sctnOpsCallDetails.grabVertical = true;		
		twd_sctnOpsCallDetails.valign = TableWrapData.FILL;
		sctnOpsCallDetails.setLayoutData(twd_sctnOpsCallDetails);		
		formToolkit.paintBordersFor(sctnOpsCallDetails);
		sctnOpsCallDetails.setText("Operation Call Details");
		
		compositeOpsCallDetails = new Composite(sctnOpsCallDetails, SWT.NONE);			
		compositeOpsCallDetails.setLayout(new GridLayout(1, false));
		
		formToolkit.adapt(compositeOpsCallDetails);
		formToolkit.paintBordersFor(compositeOpsCallDetails);
		sctnOpsCallDetails.setClient(compositeOpsCallDetails);	
		sctnOpsCallDetails.setExpanded(true);				
		
		scrldResults.layout();
	}

	public AbstractResult getResult() 
	{
		return result;
	}

	public void setResult(AbstractResult result) 
	{
		this.result = result;
					
		if(!isDisposed())
		{		
			// Dispose of the compositeAbstractResultDetails children.
			for (Control control : compositeAbstractResultDetails.getChildren()) 
			{
				control.dispose();
			}
			
			// Dispose of the compositeOpsCallDetails children.
			for (Control control : compositeOpsCallDetails.getChildren()) 
			{
				control.dispose();
			}
			
			if(result != null)
			{							
				ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeAbstractResultDetails, result);
				
				if(result instanceof OperationCallResult)
				{
					OperationCallResult operationCallResult = (OperationCallResult) result;
					if(operationCallResult.getOperationCall() != null)
					{											
						ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeOpsCallDetails, operationCallResult.getOperationCall());										
					}
					else
					{
						ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeOpsCallDetails, null, "No Operation Call to display");
					}
				}
			}
			else
			{				
				ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeAbstractResultDetails, null, "No data to display");				
				ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeOpsCallDetails, null, "No Operation Call to display");
			}				
			
			if(!compositeAbstractResultDetails.isDisposed()) compositeAbstractResultDetails.layout();
			if(!compositeOpsCallDetails.isDisposed()) compositeOpsCallDetails.layout();
			
			if(!scrldResults.isDisposed()) scrldResults.layout();
		}
	}
}

package ca.gc.asc_csa.apogy.core.invocator.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.EObjectReference;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.impl.FeaturePathAdapterImpl;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.OperationCall;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallsList;
import ca.gc.asc_csa.apogy.core.invocator.ui.wizards.OperationCallWizard;

public class OperationCallsListComposite extends ScrolledComposite 
{
	private DataBindingContext m_currentDataBindings;
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
			ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	private OperationCallsList operationCallsList;
	private List<FeaturePathAdapter> featurePathAdapters;

	private TableViewer tableViewer;
	private Button btnDelete;
	private Button btnNew;
	private Button btnInvoke;

	public OperationCallsListComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, true));
		setExpandHorizontal(true);
		setExpandVertical(true);
		addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				if (m_currentDataBindings != null) {
					m_currentDataBindings.dispose();
				}

				if (operationCallsList != null) {
					for (FeaturePathAdapter adapter : featurePathAdapters) {
						adapter.dispose();
					}
				}
			}
		});

		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));

		tableViewer = new TableViewer(composite, SWT.BORDER | SWT.FULL_SELECTION);
		Table table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 4));
		tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				newSelection(event.getSelection());
			}
		});

		TableColumn tblclmnName = new TableColumn(table, SWT.NONE);
		tblclmnName.setText("Name");

		TableColumn tblclmnFeature = new TableColumn(table, SWT.NONE);
		tblclmnFeature.setText("Feature");

		TableColumn tblclmnCommand = new TableColumn(table, SWT.NONE);
		tblclmnCommand.setText("Command");

		btnInvoke = new Button(composite, SWT.None);
		btnInvoke.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnInvoke.setText("Invoke");
		btnInvoke.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final OperationCall opsCall = (OperationCall) tableViewer.getStructuredSelection().getFirstElement();
				Job job = new Job(ApogyCoreInvocatorFacade.INSTANCE.getOperationCallString(opsCall)) {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						ApogyCoreInvocatorFacade.INSTANCE.exec(opsCall);
						return Status.OK_STATUS;
					}
				};
				job.schedule();

			}
		});

		Label label = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		btnNew = new Button(composite, SWT.NONE);
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnNew.setText("New");
		btnNew.setEnabled(false);
		btnNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				OperationCallWizard newOperationCallWizard = new OperationCallWizard(operationCallsList) {
					@Override
					public boolean performFinish() {
						getOperationCall().eResource().getResourceSet().getResources()
								.remove(getOperationCall().eResource());
						TransactionUtil.disconnectFromEditingDomain(getOperationCall().eResource());

						/**
						 * Needs to be executed in a asyncExec because of the
						 * databinding and the ObservableListContentProvider.
						 */
						getDisplay().asyncExec(new Runnable() {
							@Override
							public void run() {
								ApogyCommonTransactionFacade.INSTANCE.basicAdd(operationCallsList,
										ApogyCoreInvocatorPackage.Literals.OPERATION_CALL_CONTAINER__OPERATION_CALLS,
										getOperationCall());
							}
						});

						OperationCallsListComposite.this.tableViewer
								.setSelection(new StructuredSelection(getOperationCall()));
						packColumns();
						return true;
					}
				};
				WizardDialog dialog = new WizardDialog(getShell(), newOperationCallWizard);
				dialog.open();
			}
		});

		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		btnDelete.setText("Delete");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ApogyCommonTransactionFacade.INSTANCE.basicRemove(operationCallsList,
						ApogyCoreInvocatorPackage.Literals.OPERATION_CALL_CONTAINER__OPERATION_CALLS,
						getSelectedOperationCall());
			}
		});

		setContent(composite);
		setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		m_currentDataBindings = initDataBindingsCustom();
	}

	/**
	 * Returns the selected {@link OperationCall}.
	 * 
	 * @return Reference to the selected {@link OperationCall}.
	 */
	public OperationCall getSelectedOperationCall() {
		return (OperationCall) tableViewer.getStructuredSelection().getFirstElement();
	}

	/**
	 * This method is called when a new selection is made in the
	 * parentComposite.
	 * 
	 * @param selection
	 *            Reference to the selection.
	 */
	protected void newSelection(ISelection selection) {
	}

	/**
	 * This methods packs the columns to adjust their widths
	 */
	private void packColumns() {
		if (!tableViewer.isBusy()) {
			getDisplay().asyncExec(new Runnable() {
				@Override
				public void run() {
					for (TableColumn column : tableViewer.getTable().getColumns()) {
						column.pack();
					}
				}
			});
		}
	}

	protected void refreshViewer() {
		if (!tableViewer.isBusy()) {
			getDisplay().asyncExec(new Runnable() {
				@Override
				public void run() {
					tableViewer.refresh();
					packColumns();
				}
			});
		}
	}

	/**
	 * Sets the {@link OperationCallsList} in the parentComposite
	 * 
	 * @param operationCallsList
	 */
	public void setOperationCallsList(OperationCallsList operationCallsList) {
		if (this.operationCallsList != null) {
			for (FeaturePathAdapter adapter : getFeaturePathAdapters()) {
				adapter.dispose();
			}
		}

		this.operationCallsList = operationCallsList;

		if (this.operationCallsList.getOperationCalls() != null) {
			EObjectReference eObjectReference = ApogyCommonEMFFactory.eINSTANCE.createEObjectReference();
			eObjectReference.setEObject(operationCallsList);

			tableViewer.setInput(eObjectReference);
			btnNew.setEnabled(true);
			packColumns();
		} else {
			tableViewer.setInput(null);
		}

		for (FeaturePathAdapter adapter : getFeaturePathAdapters()) {
			adapter.init(operationCallsList);
		}
	}

	public OperationCallsList getOperationCallsList() {
		return operationCallsList;
	}

	/**
	 * Sets the background colors of the {@link OperationCall}s. If no color is
	 * specified for an {@link OperationCall}, the color will be set to white.
	 * 
	 * @param map
	 */
	public void setBackgroudColor(Map<OperationCall, Integer> map) {
		for (TableItem item : tableViewer.getTable().getItems()) {
			Color color = null;
			if (map.containsKey(item.getData())) {
				color = getDisplay().getSystemColor(map.get(item.getData()));
			}

			if (color == null) {
				color = getDisplay().getSystemColor(SWT.COLOR_WHITE);
			}

			item.setBackground(color);
		}
	}

	private List<FeaturePathAdapter> getFeaturePathAdapters() {
		if (this.featurePathAdapters == null) {
			featurePathAdapters = new ArrayList<>();
			featurePathAdapters.add(new FeaturePathAdapterImpl() {
				@Override
				public void notifyChanged(Notification msg) {
					refreshViewer();
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> featurePath = new ArrayList<EStructuralFeature>();
					featurePath.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL_CONTAINER__OPERATION_CALLS);
					featurePath.add(ApogyCommonEMFPackage.Literals.NAMED__NAME);
					return featurePath;
				}
			});
			featurePathAdapters.add(new FeaturePathAdapterImpl() {
				@Override
				public void notifyChanged(Notification msg) {
					refreshViewer();
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> featurePath = new ArrayList<EStructuralFeature>();
					featurePath.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL_CONTAINER__OPERATION_CALLS);
					featurePath.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__EOPERATION);
					return featurePath;
				}
			});
			featurePathAdapters.add(new FeaturePathAdapterImpl() {
				@Override
				public void notifyChanged(Notification msg) {
					refreshViewer();
					OperationCallsListComposite.this.newSelection(null);
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> featurePath = new ArrayList<EStructuralFeature>();
					featurePath.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL_CONTAINER__OPERATION_CALLS);
					featurePath.add(ApogyCoreInvocatorPackage.Literals.VARIABLE_FEATURE_REFERENCE__VARIABLE);
					featurePath.add(ApogyCommonEMFPackage.Literals.NAMED__NAME);
					return featurePath;
				}
			});
			featurePathAdapters.add(new FeaturePathAdapterImpl() {
				@Override
				public void notifyChanged(Notification msg) {
					refreshViewer();
					OperationCallsListComposite.this.newSelection(null);
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> featurePath = new ArrayList<EStructuralFeature>();
					featurePath.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL_CONTAINER__OPERATION_CALLS);
					featurePath.add(ApogyCoreInvocatorPackage.Literals.VARIABLE_FEATURE_REFERENCE__FEATURE_ROOT);
					return featurePath;
				}
			});
			featurePathAdapters.add(new FeaturePathAdapterImpl() {
				@Override
				public void notifyChanged(Notification msg) {
					refreshViewer();
					OperationCallsListComposite.this.newSelection(null);
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> featurePath = new ArrayList<EStructuralFeature>();
					featurePath.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL_CONTAINER__OPERATION_CALLS);
					featurePath.add(
							ApogyCoreInvocatorPackage.Literals.VARIABLE_FEATURE_REFERENCE__TYPE_MEMBER_REFERENCE_LIST_ELEMENT);
					return featurePath;
				}
			});
			featurePathAdapters.add(new FeaturePathAdapterImpl() {
				@Override
				public void notifyChanged(Notification msg) {
					refreshViewer();
					OperationCallsListComposite.this.newSelection(null);
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> featurePath = new ArrayList<EStructuralFeature>();
					featurePath.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL_CONTAINER__OPERATION_CALLS);
					featurePath.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__ARGUMENTS_LIST);
					featurePath.add(ApogyCoreInvocatorPackage.Literals.ARGUMENTS_LIST__ARGUMENTS);
					return featurePath;
				}
			});
		}
		return featurePathAdapters;
	}

	protected DataBindingContext initDataBindingsCustom() {

		DataBindingContext bindingContext = new DataBindingContext();

		tableViewer.setContentProvider(new ControllerBindingsContentProvider(adapterFactory));
		tableViewer.setLabelProvider(new ControllerBindingsLabelProvider(adapterFactory));

		/**
		 * Invoke button enabling bindings.
		 */
		IObservableValue<?> observeSingleSelectionTableViewer = ViewerProperties.singleSelection().observe(tableViewer);
		IObservableValue<?> observeEnabledBtnInvokeObserveWidget = WidgetProperties.enabled().observe(btnInvoke);
		bindingContext.bindValue(observeEnabledBtnInvokeObserveWidget, observeSingleSelectionTableViewer,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(OperationCall.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								if (fromObject instanceof OperationCall) {
									return Diagnostician.INSTANCE.validate((EObject) fromObject)
											.getSeverity() == Diagnostic.OK;
								} else {
									return false;
								}
							}
						}));

		/**
		 * Delete button enabling bindings.
		 */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionTableViewer,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;

							}
						}));

		return bindingContext;
	}

	/**
	 * Label provider for the TreeViewer
	 */
	private class ControllerBindingsLabelProvider extends AdapterFactoryLabelProvider implements ITableLabelProvider {

		public ControllerBindingsLabelProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		private final static int NAME_COLUMN_ID = 0;
		private final static int FEATURE_COLUMN_ID = 1;
		private final static int COMMAND_COLUMN_ID = 2;

		@Override
		public String getColumnText(Object object, int columnIndex) {
			String str = "<undefined>";
			if (object instanceof OperationCall) {
				OperationCall operationCall = (OperationCall) object;

				switch (columnIndex) {
				case NAME_COLUMN_ID:
					str = operationCall.getName() == null ? "<unnamed>" : operationCall.getName();
					break;
				case FEATURE_COLUMN_ID:
					str = ApogyCoreInvocatorFacade.INSTANCE.getOperationCallString(operationCall);
					if (str.contains("#")) {
						str = str.substring(0, str.indexOf("#"));
					}
					break;
				case COMMAND_COLUMN_ID:
					str = ApogyCoreInvocatorFacade.INSTANCE.getEOperationString(operationCall.getArgumentsList(),
							operationCall.getEOperation());
					str = str.substring(1, str.length());
					break;
				}
			}
			return str;
		}

		@Override
		public Image getColumnImage(Object object, int columnIndex) {
			return null;
		}
	}

	/**
	 * Content provider for the TreeViewer
	 */
	private class ControllerBindingsContentProvider extends AdapterFactoryContentProvider {

		public ControllerBindingsContentProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		@Override
		public Object[] getElements(Object object) {
			if (object instanceof EObjectReference) {
				return ((OperationCallsList) ((EObjectReference) object).getEObject()).getOperationCalls().toArray();
			}
			return null;
		}

		@Override
		public Object[] getChildren(Object object) {
			return null;
		}
	}
}
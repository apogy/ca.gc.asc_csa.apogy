package ca.gc.asc_csa.apogy.core.invocator.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.measure.unit.Unit;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecp.ui.view.ECPRendererException;
import org.eclipse.emf.ecp.ui.view.swt.ECPSWTViewRenderer;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.EObjectReference;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.impl.FeaturePathAdapterImpl;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.preferences.PreferencesConstants;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ChooseEClassWizard;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.NewChildWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.ApogyCommonUiFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.Argument;
import ca.gc.asc_csa.apogy.core.invocator.ArgumentsList;
import ca.gc.asc_csa.apogy.core.invocator.BooleanEDataTypeArgument;
import ca.gc.asc_csa.apogy.core.invocator.EClassArgument;
import ca.gc.asc_csa.apogy.core.invocator.EDataTypeArgument;
import ca.gc.asc_csa.apogy.core.invocator.EEnumArgument;
import ca.gc.asc_csa.apogy.core.invocator.NumericEDataTypeArgument;
import ca.gc.asc_csa.apogy.core.invocator.OperationCall;
import ca.gc.asc_csa.apogy.core.invocator.StringEDataTypeArgument;
import ca.gc.asc_csa.apogy.core.invocator.ui.Activator;

public class ArgumentsComposite extends Composite {

	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;
	private Composite compositeEMFForms;

	boolean emfForms;

	private OperationCall operationCall;

	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
			ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private FeaturePathAdapter adapter;
	private IPropertyChangeListener propertyChangeListener;

	private DataBindingContext m_bindingContext;

	/**
	 * Create the parentComposite.
	 * 
	 * @param parent
	 *            Reference to the parent parentComposite.
	 * @param style
	 *            Composite style.
	 */
	public ArgumentsComposite(Composite parent, int style) {
		super(parent, style);
		
		/** Preference listener */
		ca.gc.asc_csa.apogy.common.emf.ui.Activator.getDefault().getPreferenceStore()
				.addPropertyChangeListener(getPropertyChangeListener());
		
		setLayout(new FillLayout());
		addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (operationCall != null) {
					getAdapter().dispose();
				}
				adapterFactory.dispose();
				if (m_bindingContext != null)
					m_bindingContext.dispose();
				
				if(propertyChangeListener != null){
					ca.gc.asc_csa.apogy.common.emf.ui.Activator.getDefault().getPreferenceStore()
					.removePropertyChangeListener(getPropertyChangeListener());
				}
			}
		});

		this.emfForms = false;

		Composite compositeArguments = new Composite(this, SWT.None);
		compositeArguments.setLayout(new GridLayout(2, false));

		treeViewer = new TreeViewer(compositeArguments,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.SINGLE);
		Tree treeInstance = treeViewer.getTree();
		treeInstance.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		treeInstance.setLinesVisible(true);
		treeInstance.setHeaderVisible(true);
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				if (treeViewer.getStructuredSelection() != null) {

					Object obj = treeViewer.getStructuredSelection().getFirstElement();

					// When a selection is made, if EMFForms is activated if the selection is not
					// one of the arguments or it's an argument with a unit.
					if (emfForms && ((StructuredSelection) event.getSelection()).getFirstElement() != null
							&& (!getArguments().contains(((StructuredSelection) event.getSelection()).getFirstElement())
									|| (obj instanceof Argument && ApogyCommonEMFFacade.INSTANCE
											.getEngineeringUnits(((Argument) obj).getEParameter()) != null))) {
						// Remove the placeholder composite if the composite is
						// a NoContentComposite
						if (compositeEMFForms instanceof NoContentComposite) {
							compositeEMFForms.dispose();
							compositeEMFForms = new Composite(ArgumentsComposite.this, SWT.None);
							compositeEMFForms.setLayout(GridLayoutFactory.fillDefaults().margins(10, 10).create());
							ArgumentsComposite.this.layout();
						} else {
							for (Control control : compositeEMFForms.getChildren()) {
								control.dispose();
							}
						}
						try {
							// Render the EMFForms
							ECPSWTViewRenderer.INSTANCE.render(compositeEMFForms,
									(EObject) ((StructuredSelection) event.getSelection()).getFirstElement());
						} catch (ECPRendererException e) {
							String message = this.getClass().getSimpleName() + ".selectionChanged(): "
									+ "Error while opening EMF Forms";
							Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.WARNING);
						}
						compositeEMFForms.layout();
					}
					// Otherwise, if the composite is a EMFForms and needs to be
					// replaced by a placeholder
					else if (emfForms && !(compositeEMFForms instanceof NoContentComposite)) {
						compositeEMFForms.dispose();
						compositeEMFForms = getNoSelectionComposite(ArgumentsComposite.this, SWT.None);
						ArgumentsComposite.this.layout();
					}
				}
				newSelection(event.getSelection());
			}
		});

		TreeViewerColumn treeViewerActionColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn treeclmnAction = treeViewerActionColumn.getColumn();
		treeclmnAction.setWidth(100);
		treeclmnAction.setText("Parameter");

		TreeViewerColumn treeViewerParameterColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn treeclmnParameter = treeViewerParameterColumn.getColumn();
		treeclmnParameter.setWidth(100);
		treeclmnParameter.setText("Value");
		ApogyCommonUiFacade.INSTANCE.addExpandOnDoubleClick(treeViewer);
		treeViewerParameterColumn.setEditingSupport(new ArgumentsEditor(treeViewer));

		btnNew = new Button(compositeArguments, SWT.None);
		btnNew.setText("New");
		btnNew.setEnabled(false);
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		btnNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				/**
				 * Creates and opens the wizard to create a new child
				 */
				Wizard wizard;
				// If the parent is an EClassArgument
				if (getSelectedEObject() instanceof EClassArgument) {
					wizard = new ChooseEClassWizard(ApogyCoreInvocatorPackage.Literals.ECLASS_ARGUMENT__VALUE,
							getSelectedEObject(),
							(EClass) ((Argument) getSelectedEObject()).getEParameter().getEType()) {
						@Override
						public boolean performFinish() {
							EObject eObject = EcoreUtil
									.create(getChooseEClassImplementationWizardPage().getSelectedEClass());
							ApogyCommonTransactionFacade.INSTANCE.basicSet(getParent(),
									ApogyCoreInvocatorPackage.Literals.ECLASS_ARGUMENT__VALUE, eObject);
							treeViewer.setSelection(new StructuredSelection(eObject));

							return true;
						}
					};
				} else {
					wizard = new NewChildWizard(
							ApogyCommonEMFFacade.INSTANCE.getSettableEReferences(getSelectedEObject()),
							getSelectedEObject()) {
						@Override
						public boolean performFinish() {
							EObject eObject = EcoreUtil.create(getSelectedEClass());

							if (getSelectedEReference().isMany()) {
								ApogyCommonTransactionFacade.INSTANCE.basicAdd(getParent(), getSelectedEReference(),
										eObject);
							} else {
								ApogyCommonTransactionFacade.INSTANCE.basicSet(getParent(), getSelectedEReference(),
										eObject);
							}

							treeViewer.refresh();
							treeViewer.setSelection(new StructuredSelection(eObject));
							return true;
						}
					};

				}
				WizardDialog dialog = new WizardDialog(getShell(), wizard);
				dialog.open();
			}
		});

		btnDelete = new Button(compositeArguments, SWT.None);
		btnDelete.setText("Delete");
		btnDelete.setEnabled(false);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false));
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ApogyCommonTransactionFacade.INSTANCE.basicDelete(getSelectedEObject().eContainer(),
						getSelectedEObject().eContainmentFeature(), getSelectedEObject());
				treeViewer.refresh();
			}
		});

		m_bindingContext = initDataBindingsCustom();
	}

	/**
	 * Constructor to create the editable composite
	 * 
	 * @param parent
	 * @param style
	 * @param emfForms
	 */
	public ArgumentsComposite(Composite parent, int style, boolean emfForms) {
		this(parent, style);

		this.emfForms = emfForms;
		if (emfForms) {
			compositeEMFForms = getNoSelectionComposite(ArgumentsComposite.this, SWT.None);
		}
	}

	/**
	 * Method to get a composite with a message that tells the user that there
	 * is no compatible selection
	 * 
	 * @param parent
	 * @param style
	 * @return A {@link Composite} with a message
	 */
	private NoContentComposite getNoSelectionComposite(Composite parent, int style) {
		return new NoContentComposite(ArgumentsComposite.this, SWT.None) {
			@Override
			protected String getMessage() {
				return "No compatible selection";
			}
		};
	}

	/**
	 * EditingSupport for the Arguments table. Depending on the type of
	 * argument, the editingSupport is different.
	 */
	private class ArgumentsEditor extends EditingSupport {

		private String[] booleanLabels;
		private String[] literalsLabels;

		public ArgumentsEditor(ColumnViewer viewer) {
			super(viewer);

			booleanLabels = new String[2];
			booleanLabels[0] = "true";
			booleanLabels[1] = "false";
		}

		@Override
		protected void setValue(Object element, Object value) {
			if (element instanceof BooleanEDataTypeArgument && (int) value != -1) {
				ApogyCommonTransactionFacade.INSTANCE.basicSet((EObject) element,
						ApogyCoreInvocatorPackage.Literals.EDATA_TYPE_ARGUMENT__VALUE,
						Arrays.asList(booleanLabels).get((int) value));
			}
			if (element instanceof StringEDataTypeArgument) {
				ApogyCommonTransactionFacade.INSTANCE.basicSet((EObject) element,
						ApogyCoreInvocatorPackage.Literals.EDATA_TYPE_ARGUMENT__VALUE, value);
			}
			if (element instanceof NumericEDataTypeArgument) {
				// To set a numeric value, the editor verifies if it can parse
				// the entered value.
				double doublevalue = Double.parseDouble(((EDataTypeArgument) element).getValue());
				try {
					doublevalue = Double.parseDouble((String) value);
				} catch (NumberFormatException e) {
					String message = this.getClass().getSimpleName() + ".setValue(Object element, Object value): "
							+ "Value entered is not a number";
					Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.WARNING);
				}
				ApogyCommonTransactionFacade.INSTANCE.basicSet((EObject) element,
						ApogyCoreInvocatorPackage.Literals.EDATA_TYPE_ARGUMENT__VALUE, Double.toString(doublevalue));
			}
			if (element instanceof EEnumArgument) {
				EEnum eEnum = ((EEnumArgument) element).getEEnum();
				if ((int) value != -1) {
					ApogyCommonTransactionFacade.INSTANCE.basicSet((EObject) element,
							ApogyCoreInvocatorPackage.Literals.EENUM_ARGUMENT__EENUM_LITERAL,
							eEnum.getELiterals().get((int) value));
					treeViewer.refresh();
				}
			}

		}

		@Override
		protected Object getValue(Object element) {
			if (element instanceof BooleanEDataTypeArgument) {
				return Arrays.asList(booleanLabels).indexOf(((EDataTypeArgument) element).getValue());
			}
			if (element instanceof EDataTypeArgument) {
				return ((EDataTypeArgument) element).getValue();
			}
			if (element instanceof EEnumArgument) {
				return Arrays.asList(literalsLabels).indexOf(((EEnumArgument) element).getEEnumLiteral().getLiteral());
			}
			return null;
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			if (element instanceof EDataTypeArgument) {
				// ComboBox for a boolean argument.
				if (element instanceof BooleanEDataTypeArgument) {
					return new ComboBoxCellEditor(treeViewer.getTree(), booleanLabels);
				}
				// Text editor for a numeric argument.
				if (element instanceof NumericEDataTypeArgument) {
					return new TextCellEditor(treeViewer.getTree());
				}
				// Text editor for a string argument.
				if (element instanceof StringEDataTypeArgument) {
					return new TextCellEditor(treeViewer.getTree());
				}
			}
			// ComboBox for an enum argument.
			if (element instanceof EEnumArgument) {
				List<EEnumLiteral> literals = ((EEnumArgument) element).getEEnum().getELiterals();
				literalsLabels = new String[literals.size()];
				for (int i = 0; i < literals.size(); i++) {
					literalsLabels[i] = literals.get(i).getLiteral();
				}
				return new ComboBoxCellEditor(treeViewer.getTree(), literalsLabels);
			}
			return new TextCellEditor(treeViewer.getTree());
		}

		@Override
		protected boolean canEdit(Object element) {
			if (getArguments().contains(element)
					&& (element instanceof EDataTypeArgument || element instanceof EEnumArgument)
					&& ApogyCommonEMFFacade.INSTANCE
							.getEngineeringUnits(((Argument) element).getEParameter()) == null) {
				return true;
			}
			return false;
		}
	}

	/**
	 * This method is called when a new selection is made in the
	 * parentComposite.
	 * 
	 * @param selection
	 *            Reference to the selection.
	 */
	protected void newSelection(ISelection selection) {
	}
	
	/** Preference listener */
	private IPropertyChangeListener getPropertyChangeListener() {
		if (propertyChangeListener == null) {
			propertyChangeListener = new IPropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent event) {
					/**
					 * Unit of format preference event, update the value text
					 */
					if (event.getProperty().equals(PreferencesConstants.TYPED_ELEMENTS_UNITS_ID)
							|| PreferencesConstants.isFormatPreference(event.getProperty())) {
						if (!treeViewer.isBusy()) {
							setOperationCall(operationCall);
						}
					}
				}
			};
		}

		return propertyChangeListener;
	}

	public EObject getSelectedEObject() {
		return (EObject) treeViewer.getStructuredSelection().getFirstElement();
	}

	/**
	 * Gets the {@link EList} of {@link Argument} in the {@link ArgumentsList}.
	 * 
	 * @return List of {@link Argument}.
	 */
	public EList<Argument> getArguments() {
		return operationCall.getArgumentsList().getArguments();
	}

	/**
	 * Binds the {@link OperationCall} with the UI components.
	 * 
	 * @param operationCall
	 *            Reference to the {@link OperationCall}.
	 */
	public void setOperationCall(OperationCall operationCall) {
		if (this.operationCall != null) {
			getAdapter().dispose();
		}
		this.operationCall = operationCall;

		if (this.operationCall.getArgumentsList() != null) {
			EObjectReference eObjectReference = ApogyCommonEMFFactory.eINSTANCE.createEObjectReference();
			eObjectReference.setEObject(operationCall);

			treeViewer.setInput(eObjectReference);
			treeViewer.expandAll();
			// Adjust columns
			for (TreeColumn column : treeViewer.getTree().getColumns()) {
				column.pack();
			}
		} else {
			treeViewer.setInput(null);
		}

		if (this.operationCall != null) {
			getAdapter().init(this.operationCall);
		}
	}

	protected DataBindingContext initDataBindingsCustom() {

		if (m_bindingContext != null) {
			m_bindingContext.dispose();
		}

		m_bindingContext = new DataBindingContext();

		treeViewer.setContentProvider(new ArgumentsContentProvider(adapterFactory));
		treeViewer.setLabelProvider(new ArgumentsLabelProvider(adapterFactory));

		IObservableValue<?> treeViewerSingleSelectionObservableValue = ViewerProperties.singleSelection()
				.observe(treeViewer);

		/**
		 * Data binding to enable/disable the new button.
		 */
		IObservableValue<?> btnNewEnabledObserveValue = WidgetProperties.enabled().observe(btnNew);
		m_bindingContext.bindValue(btnNewEnabledObserveValue, treeViewerSingleSelectionObservableValue,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								if (fromObject instanceof EClassArgument
										&& ((EClassArgument) fromObject).getValue() == null) {
									return true;
								} else if (fromObject instanceof EObject && !ApogyCommonEMFFacade.INSTANCE
										.getSettableEReferences((EObject) fromObject).isEmpty()) {
									return true;
								}
								return false;
							}
						}));
		/**
		 * Data binding to enable/disable the delete button.
		 */
		IObservableValue<?> btnDeleteEnabledObserveValue = WidgetProperties.enabled().observe(btnDelete);
		m_bindingContext.bindValue(btnDeleteEnabledObserveValue, treeViewerSingleSelectionObservableValue,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(EObject.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								if (fromObject == operationCall) {
									return false;
								} else if (operationCall.getArgumentsList() != null
										&& operationCall.getArgumentsList().getArguments().indexOf(fromObject) != -1) {
									return false;
								}
								return true;
							}
						}));

		return m_bindingContext;
	}

	/**
	 * Content provider for the arguments.
	 * 
	 */
	private class ArgumentsContentProvider extends AdapterFactoryContentProvider {

		public ArgumentsContentProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		@Override
		public Object[] getElements(Object object) {
			if (object instanceof EObjectReference) {
				if (((EObjectReference) object).getEObject() instanceof OperationCall) {
					// Returns the arguments list of the treeViewer
					// operationCall input.
					return ((OperationCall) ((EObjectReference) object).getEObject()).getArgumentsList().getArguments()
							.toArray();
				}
			}
			return super.getElements(object);
		}

		@Override
		public Object[] getChildren(Object object) {
			if (object == operationCall) {
				// Only returns the arguments as children of the operationCall
				// input.
				if (((OperationCall) object).getArgumentsList() != null) {
					return ((OperationCall) object).getArgumentsList().getArguments().toArray();
				}
				Object[] nullObjects = {};
				return nullObjects;
			}
			return super.getChildren(object);
		}

		@Override
		public boolean hasChildren(Object object) {
			if (object == operationCall) {
				// Only returns true if the operationCall input has arguments.
				if (((OperationCall) object).getArgumentsList() != null) {
					return true;
				}
				return false;
			}
			return super.hasChildren(object);
		}
	}

	/**
	 * Label provider for the arguments.
	 * 
	 */
	private class ArgumentsLabelProvider extends AdapterFactoryLabelProvider {

		private static final int PARAMETER_COLUMN_ID = 0;
		private static final int VALUE_ID = 1;

		public ArgumentsLabelProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		@Override
		public String getColumnText(Object object, int columnIndex) {
			String str = "<undefined>";

			switch (columnIndex) {
			case PARAMETER_COLUMN_ID:
				if (getArguments().contains(object)) {
					str = super.getColumnText(object, 0);
					// Cut the values from the return of the itemProvider labels
					if (str.contains("(")) {
						str = str.substring(0, str.indexOf("("));
					}
					break;
				}
				str = object.getClass().getName();
				break;
			case VALUE_ID:
				if (getArguments().contains(object)) {
					if (object instanceof EDataTypeArgument) {
						EDataTypeArgument argument = (EDataTypeArgument)object;
						str = argument.getValue();
						
						Double valueDouble = 0.0;
						try {
							valueDouble = Double.parseDouble(str);
						} catch (Exception e) {
						}
						
						/** Units and format*/
						EParameter param = argument.getEParameter();
						Unit<?> unit = ApogyCommonEMFFacade.INSTANCE.getEngineeringUnits(param);
						if (unit != null) {
							if (argument.getValue() == null) {
								ApogyCommonTransactionFacade.INSTANCE.basicSet(argument,
										ApogyCoreInvocatorPackage.Literals.EDATA_TYPE_ARGUMENT__VALUE, "0.0");
							}
							EOperationEParametersFormatProviderParameters formatParams = ApogyCommonEMFUIFactory.eINSTANCE
									.createEOperationEParametersFormatProviderParameters();
							formatParams.setParam(param);
							EOperationEParametersUnitsProviderParameters unitsParams = ApogyCommonEMFUIFactory.eINSTANCE
									.createEOperationEParametersUnitsProviderParameters();
							unitsParams.setParam(param);
							DecimalFormat format = ApogyCommonEMFUIFacade.INSTANCE.getDisplayFormat(param.getEOperation(),
									formatParams);
							Unit<?> displayUnits = ApogyCommonEMFUIFacade.INSTANCE.getDisplayUnits(param.getEOperation(),
									unitsParams);
							Unit<?> nativeUnits = ApogyCommonEMFFacade.INSTANCE.getEngineeringUnits(param);

							if (displayUnits != null && !displayUnits.equals(nativeUnits)) {
								valueDouble = nativeUnits.getConverterTo(displayUnits).convert(valueDouble);
							}
							str = format.format(valueDouble) + " " + displayUnits.toString();
						}						
					} else if (object instanceof EClassArgument) {
						EClassArgument eClassArgumentObject = (EClassArgument) object;
						if (eClassArgumentObject.getValue() != null) {
							str = ((EClassArgument) object).getValue().getClass().getName();
						}
					} else if (object instanceof EEnumArgument) {
						str = ((EEnumArgument) object).getEEnumLiteral().getLiteral();
					}
				} else {
					str = super.getText(object);
				}
				break;
			default:
				break;
			}
			return str;
		}
	}

	/**
	 * Adapter that updates the treeViewer input if the {@link ArgumentsList} or
	 * the value of an argument is changed.
	 */
	public FeaturePathAdapter getAdapter() {
		if (adapter == null) {
			adapter = new FeaturePathAdapterImpl() {

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> features = new ArrayList<EStructuralFeature>();

					features.add(ApogyCoreInvocatorPackage.Literals.OPERATION_CALL__ARGUMENTS_LIST);
					features.add(ApogyCoreInvocatorPackage.Literals.ARGUMENTS_LIST__ARGUMENTS);
					features.add(ApogyCoreInvocatorPackage.Literals.ECLASS_ARGUMENT__VALUE);

					return features;
				}

				@Override
				public void notifyChanged(Notification msg) {
					if (!treeViewer.isBusy()) {
						setOperationCall(operationCall);
					}
				}
			};
		}
		return adapter;
	}
}
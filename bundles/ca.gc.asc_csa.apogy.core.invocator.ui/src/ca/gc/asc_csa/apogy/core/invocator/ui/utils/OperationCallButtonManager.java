package ca.gc.asc_csa.apogy.core.invocator.ui.utils;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.progress.UIJob;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.OperationCall;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallResult;
import ca.gc.asc_csa.apogy.core.invocator.ui.Activator;

public class OperationCallButtonManager 
{
	public OperationCallResult operationCallResult = null;
		
	protected Button managedButton = null;
	protected boolean notifyOnCompletion = true;
	protected String operationDescription = null;	
	
	public OperationCallButtonManager(Button managedButton, String operationDescription, boolean notifyOnCompletion)
	{
		this.managedButton = managedButton;
		this.operationDescription = operationDescription;
		this.notifyOnCompletion = notifyOnCompletion;
	}
	
	public OperationCallResult execute(final OperationCall operationCall)
	{
		Job job = new Job(operationDescription)
		{						
			@Override
			protected IStatus run(IProgressMonitor monitor) 
			{								
				try
				{
					Logger.INSTANCE.log(Activator.ID, "Launching execution of " + operationDescription, EventSeverity.INFO);
					OperationCallButtonManager.this.operationCallResult = ApogyCoreInvocatorFacade.INSTANCE.exec(operationCall);
																	
					if(operationCallResult.getExceptionContainer() != null)
					{						
						Display.getDefault().asyncExec(new Runnable() 
						{													
							@Override
							public void run() 
							{
								try
								{
									String message = "The following error occured during " + operationDescription + " : " + operationCallResult.getExceptionContainer().getException().toString();									
									Logger.INSTANCE.log(Activator.ID, message, EventSeverity.ERROR, operationCallResult.getExceptionContainer().getException());
									MessageDialog.openError(managedButton.getDisplay().getActiveShell(), "Take Scan Error", message);
								}
								catch (Exception e) 
								{
									e.printStackTrace();
								}
							}
						});						
					}
					else if(notifyOnCompletion)
					{						
						Display.getDefault().asyncExec(new Runnable() 
						{													
							@Override
							public void run() 
							{
								try
								{
									String message = operationDescription + " : Completed sucessfully.";									
									Logger.INSTANCE.log(Activator.ID, message, EventSeverity.INFO);
									MessageDialog.openInformation(managedButton.getDisplay().getActiveShell(), operationDescription + " Success", message);
								}
								catch (Exception e) 
								{
									e.printStackTrace();
								}
							}
						});							
					}
				}
				catch (final Exception e) 
				{											
					Display.getDefault().asyncExec(new Runnable() {													
						@Override
						public void run() 
						{
							String message = "The following error occured during " + operationDescription + " : " + e.toString();
																				
							Logger.INSTANCE.log(Activator.ID, message, EventSeverity.ERROR);
							MessageDialog.openError(managedButton.getDisplay().getActiveShell(), "Error", message);		
						}
					});
				}
											
				// Starts UI job that wait for the execution to complete.
				UIJob uiJob = new UIJob("Waiting for " + operationDescription + " to complete.")
				{
					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) 
					{
						if(!managedButton.isDisposed())
						{
							managedButton.setEnabled(true);
						}
						return Status.OK_STATUS;
					}
				};
				uiJob.schedule();
				
				return Status.OK_STATUS;
			}			
		};
		job.setPriority(Job.LONG);			
		job.schedule();	
		
		return OperationCallButtonManager.this.operationCallResult;
	}
	
}

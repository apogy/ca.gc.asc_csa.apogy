package ca.gc.asc_csa.apogy.core.invocator.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.impl.FeaturePathAdapterImpl;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.ApogyCommonUiFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ProgramsGroup;
import ca.gc.asc_csa.apogy.core.invocator.ProgramsList;
import ca.gc.asc_csa.apogy.core.invocator.ScriptBasedProgram;
import ca.gc.asc_csa.apogy.core.invocator.ScriptBasedProgramsGroup;
import ca.gc.asc_csa.apogy.core.invocator.ui.wizards.NewScriptBasedProgramsGroupWizard;

public class ScriptBasedProgramsListComposite extends ScrolledComposite {

	private ProgramsList programsList;
	private FeaturePathAdapter nameAdapter;
	
	private TreeViewer treeViewer;
	private Button btnNewProgram;

	private DataBindingContext bindingContext;
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
			ComposedAdapterFactory.Descriptor.Registry.INSTANCE);


	public ScriptBasedProgramsListComposite(Composite parent, int style) {
		super(parent, style);
		
		setLayout(new GridLayout(1, true));
		setExpandHorizontal(true);
		setExpandVertical(true);
		addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (programsList != null) {
					getNameAdapter().dispose();
				}
			}
		});


		Composite compositeProgramsList = new Composite(this, SWT.NONE);
		compositeProgramsList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		compositeProgramsList.setLayout(new GridLayout(2, false));

		treeViewer = new TreeViewer(compositeProgramsList,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.SINGLE);

		Tree treeInstance = treeViewer.getTree();
		treeInstance.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 5));
		treeInstance.setLinesVisible(true);
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				newSelection(event.getSelection());
			}
		});
		ApogyCommonUiFacade.INSTANCE.addExpandOnDoubleClick(treeViewer);

		Button btnNewGroup = new Button(compositeProgramsList, SWT.NONE);
		btnNewGroup.setText("New Group");
		btnNewGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnNewGroup.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				/**
				 * Creates and opens the wizard to create a valid ProgramsGroup
				 */
				NewScriptBasedProgramsGroupWizard newProgramsGroupWizard = new NewScriptBasedProgramsGroupWizard() {
					@Override
					public boolean performFinish() {
						getScriptBasedProgramsGroup().eResource().getResourceSet().getResources()
								.remove(getScriptBasedProgramsGroup().eResource());
						TransactionUtil.disconnectFromEditingDomain(getScriptBasedProgramsGroup().eResource());

						ApogyCommonTransactionFacade.INSTANCE.basicAdd(getProgramsList(),
								ApogyCoreInvocatorPackage.Literals.PROGRAMS_LIST__PROGRAMS_GROUPS,
								getScriptBasedProgramsGroup());

						ScriptBasedProgramsListComposite.this.treeViewer
								.setSelection(new StructuredSelection(getScriptBasedProgramsGroup()));
						return true;
					}
				};
				WizardDialog dialog = new WizardDialog(getShell(), newProgramsGroupWizard);
				dialog.open();

			}
		});

		btnNewProgram = new Button(compositeProgramsList, SWT.NONE);
		btnNewProgram.setText("New Program");
		btnNewProgram.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		
		
		btnNewProgram.addListener(SWT.Selection, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{
				if (event.type == SWT.Selection) 
				{	
					EClassSettings settings = ApogyCommonEMFUIFactory.eINSTANCE.createMapBasedEClassSettings();
					
					ApogyEObjectWizard wizard = new ApogyEObjectWizard(ApogyCoreInvocatorPackage.Literals.PROGRAMS_GROUP__PROGRAMS,  getSelectedScriptBasedProgramsGroup(), settings, ApogyCoreInvocatorPackage.Literals.SCRIPT_BASED_PROGRAM);
					WizardDialog dialog = new WizardDialog(getShell(), wizard);
					dialog.open();
					
					ScriptBasedProgramsListComposite.this.treeViewer.setSelection(new StructuredSelection(wizard.getCreatedEObject()));
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.refresh();
					}
				}
			}
		});
		
//		btnNewProgram.addSelectionListener(new SelectionAdapter() 
//		{
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//
//				/**
//				 * Creates and opens the wizard to create a valid Program
//				 */
//				NewScriptBasedProgramWizard newScriptBasedProgramWizard = new NewScriptBasedProgramWizard(getSelectedScriptBasedProgramsGroup()) 
//				{
//					@Override
//					public boolean performFinish() {
//						ProgramFactory factory = ProgramFactoriesRegistry.INSTANCE.getFactory(getProgramType());
//
//						Program program = factory.createProgram();
//						program.setName(getProgramSettings().getName());
//						program.setDescription(getProgramSettings().getDescription());
//
//						ApogyCommonTransactionFacade.INSTANCE.basicAdd(getCreationProgramsGroup(),
//								ApogyCoreInvocatorPackage.Literals.PROGRAMS_GROUP__PROGRAMS, program);
//
//						ScriptBasedProgramsListComposite.this.treeViewer.setSelection(new StructuredSelection(program));
//						return true;
//					}
//				};
//				WizardDialog dialog = new WizardDialog(getShell(), newScriptBasedProgramWizard);
//				dialog.open();
//			}
//		});
		

		Button btnDelete = new Button(compositeProgramsList, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		btnDelete.setText("Delete");
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (isProgramSelected()) {
					ApogyCommonTransactionFacade.INSTANCE.basicRemove(getSelectedProgram().getProgramsGroup(),
							ApogyCoreInvocatorPackage.Literals.PROGRAMS_GROUP__PROGRAMS, getSelectedProgram());
				} else if (isProgramsGroupSelected()) {
					ApogyCommonTransactionFacade.INSTANCE.basicRemove(
							getSelectedScriptBasedProgramsGroup().getProgramsList(),
							ApogyCoreInvocatorPackage.Literals.PROGRAMS_LIST__PROGRAMS_GROUPS,
							getSelectedScriptBasedProgramsGroup());
				}
			}
		});

		this.setContent(compositeProgramsList);
		this.setMinSize(compositeProgramsList.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		initDataBindingsCustom();
	}

	/**
	 * This is used to indicate if a program should belong to the list. By
	 * default all types of {@link Program} are applicable. However the
	 * developers may override this method to exclude some programs from the
	 * list.
	 * 
	 * @param program
	 *            Reference to the program.
	 * @return Return true means the {@link Program} is applicable.
	 */
	protected boolean isApplicable(Program program) {
		return true;
	}

	/**
	 * This method is called when a new selection is made in the
	 * parentComposite.
	 * 
	 * @param selection
	 *            Reference to the selection.
	 */
	protected void newSelection(ISelection selection) {
	}

	/**
	 * Returns the selected {@link Program}.
	 * 
	 * @return Reference to the selected program.
	 */
	public Program getSelectedProgram() {
		if (isProgramSelected()) {
			return (Program) treeViewer.getStructuredSelection().getFirstElement();
		}
		return null;
	}

	/**
	 * Returns the selected {@link ScriptBasedProgramsGroup}.
	 * 
	 * @return Reference to the selected programsGroup.
	 */
	public ScriptBasedProgramsGroup getSelectedScriptBasedProgramsGroup() {
		Object selection = treeViewer.getStructuredSelection().getFirstElement();

		if (isProgramSelected()) {
			return (ScriptBasedProgramsGroup) (ProgramsGroup<? extends Program>) ((ScriptBasedProgram) selection)
					.getProgramsGroup();
		} else if (isProgramsGroupSelected()) {
			return (ScriptBasedProgramsGroup) selection;
		}
		return null;
	}

	/**
	 * Returns a boolean to know if a {@link ProgramsGroup} or another object is
	 * selected
	 * 
	 * @return true if a ProgramsGroup is selected, false otherwise
	 */
	private boolean isProgramsGroupSelected() {
		return treeViewer.getStructuredSelection().getFirstElement() instanceof ScriptBasedProgramsGroup;
	}

	/**
	 * Returns a boolean to know if a {@link Program} or another object is
	 * selected
	 * 
	 * @return true if a program is selected, false otherwise
	 */
	private boolean isProgramSelected() {
		return treeViewer.getStructuredSelection().getFirstElement() instanceof Program;
	}

	/**
	 * Sets the programsList in the parentComposite
	 * 
	 * @param programsList
	 */
	public void setProgramsList(ProgramsList programsList) {
		if (this.programsList != null) {
			getNameAdapter().dispose();
		}

		this.programsList = programsList;

		treeViewer.setInput(programsList);

		if (programsList != null) {
			getNameAdapter().init(programsList);
		}
	}

	private FeaturePathAdapter getNameAdapter() {
		if (nameAdapter == null) {
			nameAdapter = new FeaturePathAdapterImpl() {

				@Override
				public void notifyChanged(Notification msg) 
				{
					treeViewer.getTree().getDisplay().asyncExec(new Runnable() 
					{						
						@Override
						public void run() 
						{	
							// If the tree is not disposed, refresh it.S
							if(!treeViewer.getTree().isDisposed())
							{
								treeViewer.refresh();
							}
						}
					});					
				}

				@Override
				public List<? extends EStructuralFeature> getFeatureList() {
					List<EStructuralFeature> features = new ArrayList<>();

					features.add(ApogyCoreInvocatorPackage.Literals.PROGRAMS_LIST__PROGRAMS_GROUPS);
					features.add(ApogyCoreInvocatorPackage.Literals.PROGRAMS_GROUP__PROGRAMS);
					features.add(ApogyCommonEMFPackage.Literals.NAMED__NAME);

					return features;
				}
			};
		}
		return nameAdapter;
	}
	
	/**
	 * Creates and returns the data bindings.
	 * 
	 * @return Reference to the data bindings.
	 */
	private void initDataBindingsCustom() {

		if (bindingContext != null) {
			bindingContext.dispose();
		}
		bindingContext = new DataBindingContext();

		treeViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory) {
			@Override
			public Object[] getElements(Object object) {
				if (object instanceof ProgramsList) {
					List<Object> objects = new ArrayList<>();
					for (ProgramsGroup<? extends Program> group : ((ProgramsList) object).getProgramsGroups()) {
						if (group instanceof ScriptBasedProgramsGroup) {
							objects.add(group);
						}
					}
					return objects.toArray();
				}
				return null;
			}

			@Override
			public Object[] getChildren(Object object) {
				if (object instanceof ScriptBasedProgramsGroup) {
					return ((ScriptBasedProgramsGroup) object).getPrograms().toArray();
				}
				return null;
			}

			@Override
			public boolean hasChildren(Object object) {
				return object instanceof ScriptBasedProgramsGroup
						? (((ScriptBasedProgramsGroup) object).getPrograms().size() > 0) : false;
			}
		});
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory) {
			@Override
			public Image getColumnImage(Object object, int columnIndex) {
				return null;
			}

			@Override
			public Image getImage(Object object) {
				return null;
			}

			@Override
			public String getText(Object object) {
				return object instanceof Named ? ((Named) object).getName() : null;
			}
		});

		IObservableValue<?> observeTreeViewerSelection = ViewerProperties.singleSelection().observe(treeViewer);

		IObservableValue<?> observeBtnNewProgramEnabled = WidgetProperties.enabled().observe(btnNewProgram);
		bindingContext.bindValue(observeBtnNewProgramEnabled, observeTreeViewerSelection,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, boolean.class) {

							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));

	}

}
package ca.gc.asc_csa.apogy.core.invocator.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage
 * @generated
 */
public interface ApogyCoreInvocatorUIFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	ApogyCoreInvocatorUIFactory eINSTANCE = ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>New Program Settings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>New Program Settings</em>'.
	 * @generated
	 */
	NewProgramSettings createNewProgramSettings();

	/**
	 * Returns a new object of class '<em>Program UI Factories Registry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Program UI Factories Registry</em>'.
	 * @generated
	 */
	ProgramUIFactoriesRegistry createProgramUIFactoriesRegistry();

	/**
	 * Returns a new object of class '<em>Operation Calls List Program UI Factory</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Operation Calls List Program UI Factory</em>'.
	 * @generated
	 */
	OperationCallsListProgramUIFactory createOperationCallsListProgramUIFactory();

	/**
	 * Returns a new object of class '<em>Type Member Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Member Wizard Pages Provider</em>'.
	 * @generated
	 */
	TypeMemberWizardPagesProvider createTypeMemberWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Facade</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Facade</em>'.
	 * @generated
	 */
	ApogyCoreInvocatorUIFacade createApogyCoreInvocatorUIFacade();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyCoreInvocatorUIPackage getApogyCoreInvocatorUIPackage();

} //ApogyCoreInvocatorUIFactory

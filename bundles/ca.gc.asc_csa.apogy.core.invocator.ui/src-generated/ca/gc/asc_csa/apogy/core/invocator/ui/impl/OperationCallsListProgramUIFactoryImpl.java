/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *  
 *  Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *      Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *      Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *      Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.invocator.ui.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISelectionListener;

import ca.gc.asc_csa.apogy.core.invocator.OperationCallsList;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage;
import ca.gc.asc_csa.apogy.core.invocator.ui.OperationCallsListProgramUIFactory;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.OperationCallsListComposite;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation Calls List Program UI Factory</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OperationCallsListProgramUIFactoryImpl extends ProgramUIFactoryImpl implements OperationCallsListProgramUIFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public OperationCallsListProgramUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreInvocatorUIPackage.Literals.OPERATION_CALLS_LIST_PROGRAM_UI_FACTORY;
	}

	@Override
	public Composite createProgramComposite(Composite parent, Program program, final ISelectionListener selectionListener) 
	{	
		OperationCallsListComposite composite = new OperationCallsListComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL)
		{
			@Override
			protected void newSelection(final ISelection selection) 
			{
				selectionListener.selectionChanged(null, selection);
			}
		};
		composite.setOperationCallsList((OperationCallsList) program);
		return composite;
	}
} //OperationCallsListProgramUIFactoryImpl

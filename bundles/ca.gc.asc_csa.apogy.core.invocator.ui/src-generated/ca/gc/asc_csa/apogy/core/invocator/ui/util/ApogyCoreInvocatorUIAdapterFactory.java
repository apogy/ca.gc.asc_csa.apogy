package ca.gc.asc_csa.apogy.core.invocator.ui.util;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.core.invocator.ui.*;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage;
import ca.gc.asc_csa.apogy.core.invocator.ui.NewProgramSettings;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage
 * @generated
 */
public class ApogyCoreInvocatorUIAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected static ApogyCoreInvocatorUIPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCoreInvocatorUIAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ApogyCoreInvocatorUIPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected ApogyCoreInvocatorUISwitch<Adapter> modelSwitch =
		new ApogyCoreInvocatorUISwitch<Adapter>() {
			@Override
			public Adapter caseApogyCoreInvocatorUIFacade(ApogyCoreInvocatorUIFacade object) {
				return createApogyCoreInvocatorUIFacadeAdapter();
			}
			@Override
			public Adapter caseNewProgramSettings(NewProgramSettings object) {
				return createNewProgramSettingsAdapter();
			}
			@Override
			public Adapter caseProgramUIFactoriesRegistry(ProgramUIFactoriesRegistry object) {
				return createProgramUIFactoriesRegistryAdapter();
			}
			@Override
			public Adapter caseProgramUIFactory(ProgramUIFactory object) {
				return createProgramUIFactoryAdapter();
			}
			@Override
			public Adapter caseOperationCallsListProgramUIFactory(OperationCallsListProgramUIFactory object) {
				return createOperationCallsListProgramUIFactoryAdapter();
			}
			@Override
			public Adapter caseTypeMemberWizardPagesProvider(TypeMemberWizardPagesProvider object) {
				return createTypeMemberWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseWizardPagesProvider(WizardPagesProvider object) {
				return createWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseNamedDescribedWizardPagesProvider(NamedDescribedWizardPagesProvider object) {
				return createNamedDescribedWizardPagesProviderAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.NewProgramSettings <em>New Program Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.NewProgramSettings
	 * @generated
	 */
	public Adapter createNewProgramSettingsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry <em>Program UI Factories Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry
	 * @generated
	 */
	public Adapter createProgramUIFactoriesRegistryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactory <em>Program UI Factory</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactory
	 * @generated
	 */
	public Adapter createProgramUIFactoryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.OperationCallsListProgramUIFactory <em>Operation Calls List Program UI Factory</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.OperationCallsListProgramUIFactory
	 * @generated
	 */
	public Adapter createOperationCallsListProgramUIFactoryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.TypeMemberWizardPagesProvider <em>Type Member Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.TypeMemberWizardPagesProvider
	 * @generated
	 */
	public Adapter createTypeMemberWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider <em>Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider
	 * @generated
	 */
	public Adapter createWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider <em>Named Described Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider
	 * @generated
	 */
	public Adapter createNamedDescribedWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade <em>Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade
	 * @generated
	 */
	public Adapter createApogyCoreInvocatorUIFacadeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ApogyCoreInvocatorUIAdapterFactory

package ca.gc.asc_csa.apogy.core.invocator.ui.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.dialogs.MessageDialog;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.Context;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.Type;
import ca.gc.asc_csa.apogy.core.invocator.TypeMemberImplementation;
import ca.gc.asc_csa.apogy.core.invocator.Variable;
import ca.gc.asc_csa.apogy.core.invocator.VariableImplementation;
import ca.gc.asc_csa.apogy.core.invocator.VariablesList;
import ca.gc.asc_csa.apogy.core.invocator.ui.Activator;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Facade</b></em>'. <!-- end-user-doc --> *
 * @generated
 */
public class ApogyCoreInvocatorUIFacadeImpl extends MinimalEObjectImpl.Container implements ApogyCoreInvocatorUIFacade {

	private static ApogyCoreInvocatorUIFacade instance = null;

	public static ApogyCoreInvocatorUIFacade getInstance() {
		if (instance == null) {
			instance = new ApogyCoreInvocatorUIFacadeImpl();
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	protected ApogyCoreInvocatorUIFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreInvocatorUIPackage.Literals.APOGY_CORE_INVOCATOR_UI_FACADE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void copyInitializationData(Context source, Context destination) throws Exception {

		if (source.getVariableImplementationsList().getVariableImplementations().size() != destination
				.getVariableImplementationsList().getVariableImplementations().size()) {
			throw new Exception(
					"Contexts <" + source.getName() + "> and <" + destination.getName() + "> are not consistent");
		}

		Iterator<VariableImplementation> sourceVariableImplementations = source.getVariableImplementationsList()
				.getVariableImplementations().iterator();
		Iterator<VariableImplementation> destinationVariableImplementations = destination
				.getVariableImplementationsList().getVariableImplementations().iterator();

		while (sourceVariableImplementations.hasNext()) {
			try {
				CompoundCommand command = new CompoundCommand();
				copyInitializationData(sourceVariableImplementations.next(), destinationVariableImplementations.next(),
						command);
				EditingDomain editingDomain = AdapterFactoryEditingDomain.getEditingDomainFor(source);
				editingDomain.getCommandStack().execute(command);

			} catch (Exception e) {
				throw new Exception(
						"Contexts <" + source.getName() + "> and <" + destination.getName() + "> are not consistent",
						e);
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void copyInitializationData(AbstractTypeImplementation source, AbstractTypeImplementation destination,
			CompoundCommand command) throws Exception {
		if (source.getHandlingType() != destination.getHandlingType()) {
			throw new Exception();
		}
		if (source.getTypeMemberImplementations().size() != destination.getTypeMemberImplementations().size()) {
			throw new Exception();
		}

		command.append(new SetCommand(AdapterFactoryEditingDomain.getEditingDomainFor(source), destination,
				ApogyCoreInvocatorPackage.Literals.ABSTRACT_TYPE_IMPLEMENTATION__ABSTRACT_INITIALIZATION_DATA,
				EcoreUtil.copy(source.getAbstractInitializationData())));

		Iterator<TypeMemberImplementation> sourceImplementations = source.getTypeMemberImplementations().iterator();
		Iterator<TypeMemberImplementation> destinationImplementations = destination.getTypeMemberImplementations()
				.iterator();
		while (sourceImplementations.hasNext()) {
			copyInitializationData(sourceImplementations.next(), destinationImplementations.next(), command);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void initSession() {
		ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain().getCommandStack()
				.execute(new RecordingCommand(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain()) {
					@Override
					protected void doExecute() {
						ApogyCoreInvocatorFacade.INSTANCE.initVariableInstances();
					}
				});

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void disposeSession() {
		ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain().getCommandStack()
				.execute(new RecordingCommand(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain()) {
					@Override
					protected void doExecute() {
						ApogyCoreInvocatorFacade.INSTANCE.disposeVariableInstances();
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void addVariable(VariablesList variablesList, Variable variable) {
		TransactionalEditingDomain domain = (TransactionalEditingDomain) AdapterFactoryEditingDomain
				.getEditingDomainFor(variablesList);
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {
				ApogyCoreInvocatorFacade.INSTANCE.addVariable(variablesList, variable);
			}
		});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void deleteVariables(VariablesList variablesList, List<Variable> variables) {
		String variablesToDeleteMessage = ApogyCommonEMFFacade.INSTANCE.toString(variables, ", ");

		MessageDialog dialog = new MessageDialog(null, "Delete the selected variables", null,
				"Are you sure to delete these variables: " + variablesToDeleteMessage, MessageDialog.QUESTION,
				new String[] { "Yes", "No" }, 1);
		int result = dialog.open();
		if (result == 0) {

			TransactionalEditingDomain domain = (TransactionalEditingDomain) AdapterFactoryEditingDomain
					.getEditingDomainFor(variablesList);
			domain.getCommandStack().execute(new RecordingCommand(domain) {

				@Override
				protected void doExecute() {
					for (Variable variable : variables) {
						try {
							ApogyCoreInvocatorFacade.INSTANCE.deleteVariable(variablesList, variable);
						} catch (Exception e) {
							Logger.INSTANCE.log(Activator.ID,
									"Unable to delete the variable <" + variable.getName() + ">", EventSeverity.ERROR,
									e);
						}
					}
				}
			});
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void saveToPersistedState(MPart mPart, String persistedStateKey, EObject eObject) 
	{
		InvocatorSession activeInvocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();
		if(activeInvocatorSession != null)
		{
			Resource parentResource = activeInvocatorSession.eResource();
			ResourceSet resourceSet = parentResource.getResourceSet();									
			ApogyCommonEMFUIFacade.INSTANCE.saveToPersistedState(mPart, persistedStateKey, eObject, resourceSet);
		}
		else
		{
			Logger.INSTANCE.log(Activator.ID, this, "Failed to save EObject to Part <"+ mPart.getElementId() + "> Persisted State at key <" + persistedStateKey + "> : The active session is null !", EventSeverity.ERROR);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EObject readFromPersistedState(MPart mPart, String persistedStateKey) 
	{
		InvocatorSession activeInvocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();
		if(activeInvocatorSession != null)
		{
			Resource parentResource = activeInvocatorSession.eResource();
			ResourceSet resourceSet = parentResource.getResourceSet();
			
			return ApogyCommonEMFUIFacade.INSTANCE.readFromPersistedState(mPart, persistedStateKey, resourceSet);
		}
		else
		{
			Logger.INSTANCE.log(Activator.ID, this, "Failed to load EObject from Part <" + mPart.getElementId() + "> Persisted State at key <" + persistedStateKey + "> : The active session is null !", EventSeverity.ERROR);	
			return null;
		}	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public SortedSet<Type> sortTypeByName(List<Type> types) 
	{
		SortedSet<Type> sorted = new TreeSet<Type>(new Comparator<Type>() 
		{
			@Override
			public int compare(Type o1, Type o2) 
			{
				if(o1.getName() != null && o2.getName() != null)
				{
					int compare = o1.getName().compareToIgnoreCase(o2.getName());
					if(compare != 0)
					{
						return compare;
					}
					else
					{
						return o1.getTypeApiAdapterClass().getName().compareTo(o2.getTypeApiAdapterClass().getName());
					}
				}
				else
				{
					return o1.getTypeApiAdapterClass().getName().compareTo(o2.getTypeApiAdapterClass().getName());
				}				
			}
		});
		sorted.addAll(types);
		return sorted;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCoreInvocatorUIPackage.APOGY_CORE_INVOCATOR_UI_FACADE___COPY_INITIALIZATION_DATA__CONTEXT_CONTEXT:
				try {
					copyInitializationData((Context)arguments.get(0), (Context)arguments.get(1));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case ApogyCoreInvocatorUIPackage.APOGY_CORE_INVOCATOR_UI_FACADE___COPY_INITIALIZATION_DATA__ABSTRACTTYPEIMPLEMENTATION_ABSTRACTTYPEIMPLEMENTATION_COMPOUNDCOMMAND:
				try {
					copyInitializationData((AbstractTypeImplementation)arguments.get(0), (AbstractTypeImplementation)arguments.get(1), (CompoundCommand)arguments.get(2));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case ApogyCoreInvocatorUIPackage.APOGY_CORE_INVOCATOR_UI_FACADE___INIT_SESSION:
				initSession();
				return null;
			case ApogyCoreInvocatorUIPackage.APOGY_CORE_INVOCATOR_UI_FACADE___DISPOSE_SESSION:
				disposeSession();
				return null;
			case ApogyCoreInvocatorUIPackage.APOGY_CORE_INVOCATOR_UI_FACADE___ADD_VARIABLE__VARIABLESLIST_VARIABLE:
				addVariable((VariablesList)arguments.get(0), (Variable)arguments.get(1));
				return null;
			case ApogyCoreInvocatorUIPackage.APOGY_CORE_INVOCATOR_UI_FACADE___DELETE_VARIABLES__VARIABLESLIST_LIST:
				deleteVariables((VariablesList)arguments.get(0), (List<Variable>)arguments.get(1));
				return null;
			case ApogyCoreInvocatorUIPackage.APOGY_CORE_INVOCATOR_UI_FACADE___SAVE_TO_PERSISTED_STATE__MPART_STRING_EOBJECT:
				saveToPersistedState((MPart)arguments.get(0), (String)arguments.get(1), (EObject)arguments.get(2));
				return null;
			case ApogyCoreInvocatorUIPackage.APOGY_CORE_INVOCATOR_UI_FACADE___READ_FROM_PERSISTED_STATE__MPART_STRING:
				return readFromPersistedState((MPart)arguments.get(0), (String)arguments.get(1));
			case ApogyCoreInvocatorUIPackage.APOGY_CORE_INVOCATOR_UI_FACADE___SORT_TYPE_BY_NAME__LIST:
				return sortTypeByName((List<Type>)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} // ApogyCoreInvocatorUIFacadeImpl

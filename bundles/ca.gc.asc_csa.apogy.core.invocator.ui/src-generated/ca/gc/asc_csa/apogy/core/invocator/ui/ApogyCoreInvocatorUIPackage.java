package ca.gc.asc_csa.apogy.core.invocator.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyCoreInvocatorUI' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque \n     Olivier L. Larouche\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' childCreationExtenders='true' modelName='ApogyCoreInvocatorUI' modelDirectory='/ca.gc.asc_csa.apogy.core.invocator.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.core.invocator.ui/src-generated' basePackage='ca.gc.asc_csa.apogy.core.invocator'"
 * @generated
 */
public interface ApogyCoreInvocatorUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.core.invocator.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	ApogyCoreInvocatorUIPackage eINSTANCE = ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.NewProgramSettingsImpl <em>New Program Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.NewProgramSettingsImpl
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getNewProgramSettings()
	 * @generated
	 */
	int NEW_PROGRAM_SETTINGS = 1;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIFacadeImpl <em>Facade</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIFacadeImpl
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getApogyCoreInvocatorUIFacade()
	 * @generated
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE = 0;

	/**
	 * The number of structural features of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Copy Initialization Data</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE___COPY_INITIALIZATION_DATA__CONTEXT_CONTEXT = 0;

	/**
	 * The operation id for the '<em>Copy Initialization Data</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE___COPY_INITIALIZATION_DATA__ABSTRACTTYPEIMPLEMENTATION_ABSTRACTTYPEIMPLEMENTATION_COMPOUNDCOMMAND = 1;

	/**
	 * The operation id for the '<em>Init Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE___INIT_SESSION = 2;

	/**
	 * The operation id for the '<em>Dispose Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE___DISPOSE_SESSION = 3;

	/**
	 * The operation id for the '<em>Add Variable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE___ADD_VARIABLE__VARIABLESLIST_VARIABLE = 4;

	/**
	 * The operation id for the '<em>Delete Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE___DELETE_VARIABLES__VARIABLESLIST_LIST = 5;

	/**
	 * The operation id for the '<em>Save To Persisted State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE___SAVE_TO_PERSISTED_STATE__MPART_STRING_EOBJECT = 6;

	/**
	 * The operation id for the '<em>Read From Persisted State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE___READ_FROM_PERSISTED_STATE__MPART_STRING = 7;

	/**
	 * The operation id for the '<em>Sort Type By Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE___SORT_TYPE_BY_NAME__LIST = 8;

	/**
	 * The number of operations of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_INVOCATOR_UI_FACADE_OPERATION_COUNT = 9;

	/**
	 * The feature id for the '<em><b>EClass</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NEW_PROGRAM_SETTINGS__ECLASS = 0;

	/**
	 * The feature id for the '<em><b>Program Settings</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NEW_PROGRAM_SETTINGS__PROGRAM_SETTINGS = 1;

	/**
	 * The number of structural features of the '<em>New Program Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NEW_PROGRAM_SETTINGS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>New Program Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NEW_PROGRAM_SETTINGS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoriesRegistryImpl <em>Program UI Factories Registry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoriesRegistryImpl
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getProgramUIFactoriesRegistry()
	 * @generated
	 */
	int PROGRAM_UI_FACTORIES_REGISTRY = 2;

	/**
	 * The feature id for the '<em><b>PROGRAM FACTORY PROVIDER CONTRIBUTORS POINT ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID = 0;

	/**
	 * The feature id for the '<em><b>PROGRAM FACTORY PROVIDER CONTRIBUTORS ECLASS ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID = 1;

	/**
	 * The feature id for the '<em><b>PROGRAM FACTORY PROVIDER CONTRIBUTORS FACTORY ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID = 2;

	/**
	 * The feature id for the '<em><b>Program UI Factories Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_UI_FACTORIES_MAP = 3;

	/**
	 * The number of structural features of the '<em>Program UI Factories Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int PROGRAM_UI_FACTORIES_REGISTRY_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Get Factory</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int PROGRAM_UI_FACTORIES_REGISTRY___GET_FACTORY__ECLASS = 0;

	/**
	 * The number of operations of the '<em>Program UI Factories Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int PROGRAM_UI_FACTORIES_REGISTRY_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoryImpl <em>Program UI Factory</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoryImpl
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getProgramUIFactory()
	 * @generated
	 */
	int PROGRAM_UI_FACTORY = 3;

	/**
	 * The number of structural features of the '<em>Program UI Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int PROGRAM_UI_FACTORY_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Create Program Composite</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM_UI_FACTORY___CREATE_PROGRAM_COMPOSITE__COMPOSITE_PROGRAM_ISELECTIONLISTENER = 0;

	/**
	 * The number of operations of the '<em>Program UI Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int PROGRAM_UI_FACTORY_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.OperationCallsListProgramUIFactoryImpl <em>Operation Calls List Program UI Factory</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.OperationCallsListProgramUIFactoryImpl
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getOperationCallsListProgramUIFactory()
	 * @generated
	 */
	int OPERATION_CALLS_LIST_PROGRAM_UI_FACTORY = 4;

	/**
	 * The number of structural features of the '<em>Operation Calls List Program UI Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int OPERATION_CALLS_LIST_PROGRAM_UI_FACTORY_FEATURE_COUNT = PROGRAM_UI_FACTORY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Create Program Composite</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_CALLS_LIST_PROGRAM_UI_FACTORY___CREATE_PROGRAM_COMPOSITE__COMPOSITE_PROGRAM_ISELECTIONLISTENER = PROGRAM_UI_FACTORY___CREATE_PROGRAM_COMPOSITE__COMPOSITE_PROGRAM_ISELECTIONLISTENER;

	/**
	 * The number of operations of the '<em>Operation Calls List Program UI Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int OPERATION_CALLS_LIST_PROGRAM_UI_FACTORY_OPERATION_COUNT = PROGRAM_UI_FACTORY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.TypeMemberWizardPagesProviderImpl <em>Type Member Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.TypeMemberWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getTypeMemberWizardPagesProvider()
	 * @generated
	 */
	int TYPE_MEMBER_WIZARD_PAGES_PROVIDER = 5;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MEMBER_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MEMBER_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Type Member Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MEMBER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MEMBER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MEMBER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MEMBER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MEMBER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MEMBER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Type Member Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MEMBER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '<em>MPart</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.e4.ui.model.application.ui.basic.MPart
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getMPart()
	 * @generated
	 */
	int MPART = 6;

	/**
	 * The meta object id for the '<em>Compound Command</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.emf.common.command.CompoundCommand
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getCompoundCommand()
	 * @generated
	 */
	int COMPOUND_COMMAND = 7;


	/**
	 * The meta object id for the '<em>List Variables</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see java.util.List
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getListVariables()
	 * @generated
	 */
	int LIST_VARIABLES = 8;

	/**
	 * The meta object id for the '<em>List Types</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see java.util.List
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getListTypes()
	 * @generated
	 */
	int LIST_TYPES = 9;

	/**
	 * The meta object id for the '<em>Sorted Set</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see java.util.SortedSet
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getSortedSet()
	 * @generated
	 */
	int SORTED_SET = 10;

	/**
	 * The meta object id for the '<em>Hash Map</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see java.util.HashMap
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getHashMap()
	 * @generated
	 */
	int HASH_MAP = 11;

	/**
	 * The meta object id for the '<em>Composite</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.swt.widgets.Composite
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getComposite()
	 * @generated
	 */
	int COMPOSITE = 12;

	/**
	 * The meta object id for the '<em>ISelection Listener</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ui.ISelectionListener
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getISelectionListener()
	 * @generated
	 */
	int ISELECTION_LISTENER = 13;

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.NewProgramSettings <em>New Program Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>New Program Settings</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.NewProgramSettings
	 * @generated
	 */
	EClass getNewProgramSettings();

	/**
	 * Returns the meta object for the containment reference '{@link ca.gc.asc_csa.apogy.core.invocator.ui.NewProgramSettings#getEClass <em>EClass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the containment reference '<em>EClass</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.NewProgramSettings#getEClass()
	 * @see #getNewProgramSettings()
	 * @generated
	 */
	EReference getNewProgramSettings_EClass();

	/**
	 * Returns the meta object for the containment reference '{@link ca.gc.asc_csa.apogy.core.invocator.ui.NewProgramSettings#getProgramSettings <em>Program Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the containment reference '<em>Program Settings</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.NewProgramSettings#getProgramSettings()
	 * @see #getNewProgramSettings()
	 * @generated
	 */
	EReference getNewProgramSettings_ProgramSettings();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry <em>Program UI Factories Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Program UI Factories Registry</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry
	 * @generated
	 */
	EClass getProgramUIFactoriesRegistry();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS POINT ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>PROGRAM FACTORY PROVIDER CONTRIBUTORS POINT ID</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID()
	 * @see #getProgramUIFactoriesRegistry()
	 * @generated
	 */
	EAttribute getProgramUIFactoriesRegistry_PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS ECLASS ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>PROGRAM FACTORY PROVIDER CONTRIBUTORS ECLASS ID</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID()
	 * @see #getProgramUIFactoriesRegistry()
	 * @generated
	 */
	EAttribute getProgramUIFactoriesRegistry_PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS FACTORY ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>PROGRAM FACTORY PROVIDER CONTRIBUTORS FACTORY ID</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID()
	 * @see #getProgramUIFactoriesRegistry()
	 * @generated
	 */
	EAttribute getProgramUIFactoriesRegistry_PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getProgramUIFactoriesMap <em>Program UI Factories Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>Program UI Factories Map</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getProgramUIFactoriesMap()
	 * @see #getProgramUIFactoriesRegistry()
	 * @generated
	 */
	EAttribute getProgramUIFactoriesRegistry_ProgramUIFactoriesMap();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getFactory(org.eclipse.emf.ecore.EClass) <em>Get Factory</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Factory</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getFactory(org.eclipse.emf.ecore.EClass)
	 * @generated
	 */
	EOperation getProgramUIFactoriesRegistry__GetFactory__EClass();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactory <em>Program UI Factory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Program UI Factory</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactory
	 * @generated
	 */
	EClass getProgramUIFactory();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactory#createProgramComposite(org.eclipse.swt.widgets.Composite, ca.gc.asc_csa.apogy.core.invocator.Program, org.eclipse.ui.ISelectionListener) <em>Create Program Composite</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Program Composite</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactory#createProgramComposite(org.eclipse.swt.widgets.Composite, ca.gc.asc_csa.apogy.core.invocator.Program, org.eclipse.ui.ISelectionListener)
	 * @generated
	 */
	EOperation getProgramUIFactory__CreateProgramComposite__Composite_Program_ISelectionListener();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.OperationCallsListProgramUIFactory <em>Operation Calls List Program UI Factory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Operation Calls List Program UI Factory</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.OperationCallsListProgramUIFactory
	 * @generated
	 */
	EClass getOperationCallsListProgramUIFactory();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.TypeMemberWizardPagesProvider <em>Type Member Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Member Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.TypeMemberWizardPagesProvider
	 * @generated
	 */
	EClass getTypeMemberWizardPagesProvider();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.e4.ui.model.application.ui.basic.MPart <em>MPart</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>MPart</em>'.
	 * @see org.eclipse.e4.ui.model.application.ui.basic.MPart
	 * @model instanceClass="org.eclipse.e4.ui.model.application.ui.basic.MPart"
	 * @generated
	 */
	EDataType getMPart();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade <em>Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Facade</em>'.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade
	 * @generated
	 */
	EClass getApogyCoreInvocatorUIFacade();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#copyInitializationData(ca.gc.asc_csa.apogy.core.invocator.Context, ca.gc.asc_csa.apogy.core.invocator.Context) <em>Copy Initialization Data</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Copy Initialization Data</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#copyInitializationData(ca.gc.asc_csa.apogy.core.invocator.Context, ca.gc.asc_csa.apogy.core.invocator.Context)
	 * @generated
	 */
	EOperation getApogyCoreInvocatorUIFacade__CopyInitializationData__Context_Context();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#copyInitializationData(ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation, ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation, org.eclipse.emf.common.command.CompoundCommand) <em>Copy Initialization Data</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Copy Initialization Data</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#copyInitializationData(ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation, ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation, org.eclipse.emf.common.command.CompoundCommand)
	 * @generated
	 */
	EOperation getApogyCoreInvocatorUIFacade__CopyInitializationData__AbstractTypeImplementation_AbstractTypeImplementation_CompoundCommand();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#initSession() <em>Init Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Init Session</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#initSession()
	 * @generated
	 */
	EOperation getApogyCoreInvocatorUIFacade__InitSession();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#disposeSession() <em>Dispose Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Dispose Session</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#disposeSession()
	 * @generated
	 */
	EOperation getApogyCoreInvocatorUIFacade__DisposeSession();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#addVariable(ca.gc.asc_csa.apogy.core.invocator.VariablesList, ca.gc.asc_csa.apogy.core.invocator.Variable) <em>Add Variable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Add Variable</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#addVariable(ca.gc.asc_csa.apogy.core.invocator.VariablesList, ca.gc.asc_csa.apogy.core.invocator.Variable)
	 * @generated
	 */
	EOperation getApogyCoreInvocatorUIFacade__AddVariable__VariablesList_Variable();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#deleteVariables(ca.gc.asc_csa.apogy.core.invocator.VariablesList, java.util.List) <em>Delete Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Delete Variables</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#deleteVariables(ca.gc.asc_csa.apogy.core.invocator.VariablesList, java.util.List)
	 * @generated
	 */
	EOperation getApogyCoreInvocatorUIFacade__DeleteVariables__VariablesList_List();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#saveToPersistedState(org.eclipse.e4.ui.model.application.ui.basic.MPart, java.lang.String, org.eclipse.emf.ecore.EObject) <em>Save To Persisted State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Save To Persisted State</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#saveToPersistedState(org.eclipse.e4.ui.model.application.ui.basic.MPart, java.lang.String, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getApogyCoreInvocatorUIFacade__SaveToPersistedState__MPart_String_EObject();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#readFromPersistedState(org.eclipse.e4.ui.model.application.ui.basic.MPart, java.lang.String) <em>Read From Persisted State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Read From Persisted State</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#readFromPersistedState(org.eclipse.e4.ui.model.application.ui.basic.MPart, java.lang.String)
	 * @generated
	 */
	EOperation getApogyCoreInvocatorUIFacade__ReadFromPersistedState__MPart_String();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#sortTypeByName(java.util.List) <em>Sort Type By Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Sort Type By Name</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade#sortTypeByName(java.util.List)
	 * @generated
	 */
	EOperation getApogyCoreInvocatorUIFacade__SortTypeByName__List();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.common.command.CompoundCommand <em>Compound Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Compound Command</em>'.
	 * @see org.eclipse.emf.common.command.CompoundCommand
	 * @model instanceClass="org.eclipse.emf.common.command.CompoundCommand"
	 * @generated
	 */
	EDataType getCompoundCommand();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>List Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>List Variables</em>'.
	 * @see java.util.List
	 * @model instanceClass="java.util.List<ca.gc.asc_csa.apogy.core.invocator.Variable>"
	 * @generated
	 */
	EDataType getListVariables();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>List Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>List Types</em>'.
	 * @see java.util.List
	 * @model instanceClass="java.util.List<ca.gc.asc_csa.apogy.core.invocator.Type>"
	 * @generated
	 */
	EDataType getListTypes();

	/**
	 * Returns the meta object for data type '{@link java.util.SortedSet <em>Sorted Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Sorted Set</em>'.
	 * @see java.util.SortedSet
	 * @model instanceClass="java.util.SortedSet" typeParameters="T"
	 * @generated
	 */
	EDataType getSortedSet();

	/**
	 * Returns the meta object for data type '{@link java.util.HashMap <em>Hash Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Hash Map</em>'.
	 * @see java.util.HashMap
	 * @model instanceClass="java.util.HashMap" typeParameters="key value"
	 * @generated
	 */
	EDataType getHashMap();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.swt.widgets.Composite <em>Composite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Composite</em>'.
	 * @see org.eclipse.swt.widgets.Composite
	 * @model instanceClass="org.eclipse.swt.widgets.Composite"
	 * @generated
	 */
	EDataType getComposite();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.ui.ISelectionListener <em>ISelection Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ISelection Listener</em>'.
	 * @see org.eclipse.ui.ISelectionListener
	 * @model instanceClass="org.eclipse.ui.ISelectionListener"
	 * @generated
	 */
	EDataType getISelectionListener();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCoreInvocatorUIFactory getApogyCoreInvocatorUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.NewProgramSettingsImpl <em>New Program Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.NewProgramSettingsImpl
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getNewProgramSettings()
		 * @generated
		 */
		EClass NEW_PROGRAM_SETTINGS = eINSTANCE.getNewProgramSettings();
		/**
		 * The meta object literal for the '<em><b>EClass</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference NEW_PROGRAM_SETTINGS__ECLASS = eINSTANCE.getNewProgramSettings_EClass();
		/**
		 * The meta object literal for the '<em><b>Program Settings</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference NEW_PROGRAM_SETTINGS__PROGRAM_SETTINGS = eINSTANCE.getNewProgramSettings_ProgramSettings();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoriesRegistryImpl <em>Program UI Factories Registry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoriesRegistryImpl
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getProgramUIFactoriesRegistry()
		 * @generated
		 */
		EClass PROGRAM_UI_FACTORIES_REGISTRY = eINSTANCE.getProgramUIFactoriesRegistry();
		/**
		 * The meta object literal for the '<em><b>PROGRAM FACTORY PROVIDER CONTRIBUTORS POINT ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID = eINSTANCE.getProgramUIFactoriesRegistry_PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID();
		/**
		 * The meta object literal for the '<em><b>PROGRAM FACTORY PROVIDER CONTRIBUTORS ECLASS ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID = eINSTANCE.getProgramUIFactoriesRegistry_PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID();
		/**
		 * The meta object literal for the '<em><b>PROGRAM FACTORY PROVIDER CONTRIBUTORS FACTORY ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID = eINSTANCE.getProgramUIFactoriesRegistry_PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID();
		/**
		 * The meta object literal for the '<em><b>Program UI Factories Map</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_UI_FACTORIES_MAP = eINSTANCE.getProgramUIFactoriesRegistry_ProgramUIFactoriesMap();
		/**
		 * The meta object literal for the '<em><b>Get Factory</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation PROGRAM_UI_FACTORIES_REGISTRY___GET_FACTORY__ECLASS = eINSTANCE.getProgramUIFactoriesRegistry__GetFactory__EClass();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoryImpl <em>Program UI Factory</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoryImpl
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getProgramUIFactory()
		 * @generated
		 */
		EClass PROGRAM_UI_FACTORY = eINSTANCE.getProgramUIFactory();
		/**
		 * The meta object literal for the '<em><b>Create Program Composite</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PROGRAM_UI_FACTORY___CREATE_PROGRAM_COMPOSITE__COMPOSITE_PROGRAM_ISELECTIONLISTENER = eINSTANCE.getProgramUIFactory__CreateProgramComposite__Composite_Program_ISelectionListener();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.OperationCallsListProgramUIFactoryImpl <em>Operation Calls List Program UI Factory</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.OperationCallsListProgramUIFactoryImpl
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getOperationCallsListProgramUIFactory()
		 * @generated
		 */
		EClass OPERATION_CALLS_LIST_PROGRAM_UI_FACTORY = eINSTANCE.getOperationCallsListProgramUIFactory();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.TypeMemberWizardPagesProviderImpl <em>Type Member Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.TypeMemberWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getTypeMemberWizardPagesProvider()
		 * @generated
		 */
		EClass TYPE_MEMBER_WIZARD_PAGES_PROVIDER = eINSTANCE.getTypeMemberWizardPagesProvider();
		/**
		 * The meta object literal for the '<em>MPart</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see org.eclipse.e4.ui.model.application.ui.basic.MPart
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getMPart()
		 * @generated
		 */
		EDataType MPART = eINSTANCE.getMPart();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIFacadeImpl <em>Facade</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIFacadeImpl
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getApogyCoreInvocatorUIFacade()
		 * @generated
		 */
		EClass APOGY_CORE_INVOCATOR_UI_FACADE = eINSTANCE.getApogyCoreInvocatorUIFacade();
		/**
		 * The meta object literal for the '<em><b>Copy Initialization Data</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_CORE_INVOCATOR_UI_FACADE___COPY_INITIALIZATION_DATA__CONTEXT_CONTEXT = eINSTANCE.getApogyCoreInvocatorUIFacade__CopyInitializationData__Context_Context();
		/**
		 * The meta object literal for the '<em><b>Copy Initialization Data</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_CORE_INVOCATOR_UI_FACADE___COPY_INITIALIZATION_DATA__ABSTRACTTYPEIMPLEMENTATION_ABSTRACTTYPEIMPLEMENTATION_COMPOUNDCOMMAND = eINSTANCE.getApogyCoreInvocatorUIFacade__CopyInitializationData__AbstractTypeImplementation_AbstractTypeImplementation_CompoundCommand();
		/**
		 * The meta object literal for the '<em><b>Init Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_CORE_INVOCATOR_UI_FACADE___INIT_SESSION = eINSTANCE.getApogyCoreInvocatorUIFacade__InitSession();
		/**
		 * The meta object literal for the '<em><b>Dispose Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_CORE_INVOCATOR_UI_FACADE___DISPOSE_SESSION = eINSTANCE.getApogyCoreInvocatorUIFacade__DisposeSession();
		/**
		 * The meta object literal for the '<em><b>Add Variable</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_CORE_INVOCATOR_UI_FACADE___ADD_VARIABLE__VARIABLESLIST_VARIABLE = eINSTANCE.getApogyCoreInvocatorUIFacade__AddVariable__VariablesList_Variable();
		/**
		 * The meta object literal for the '<em><b>Delete Variables</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_CORE_INVOCATOR_UI_FACADE___DELETE_VARIABLES__VARIABLESLIST_LIST = eINSTANCE.getApogyCoreInvocatorUIFacade__DeleteVariables__VariablesList_List();
		/**
		 * The meta object literal for the '<em><b>Save To Persisted State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_CORE_INVOCATOR_UI_FACADE___SAVE_TO_PERSISTED_STATE__MPART_STRING_EOBJECT = eINSTANCE.getApogyCoreInvocatorUIFacade__SaveToPersistedState__MPart_String_EObject();
		/**
		 * The meta object literal for the '<em><b>Read From Persisted State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_CORE_INVOCATOR_UI_FACADE___READ_FROM_PERSISTED_STATE__MPART_STRING = eINSTANCE.getApogyCoreInvocatorUIFacade__ReadFromPersistedState__MPart_String();
		/**
		 * The meta object literal for the '<em><b>Sort Type By Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_CORE_INVOCATOR_UI_FACADE___SORT_TYPE_BY_NAME__LIST = eINSTANCE.getApogyCoreInvocatorUIFacade__SortTypeByName__List();
		/**
		 * The meta object literal for the '<em>Compound Command</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see org.eclipse.emf.common.command.CompoundCommand
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getCompoundCommand()
		 * @generated
		 */
		EDataType COMPOUND_COMMAND = eINSTANCE.getCompoundCommand();
		/**
		 * The meta object literal for the '<em>List Variables</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see java.util.List
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getListVariables()
		 * @generated
		 */
		EDataType LIST_VARIABLES = eINSTANCE.getListVariables();
		/**
		 * The meta object literal for the '<em>List Types</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see java.util.List
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getListTypes()
		 * @generated
		 */
		EDataType LIST_TYPES = eINSTANCE.getListTypes();
		/**
		 * The meta object literal for the '<em>Sorted Set</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see java.util.SortedSet
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getSortedSet()
		 * @generated
		 */
		EDataType SORTED_SET = eINSTANCE.getSortedSet();
		/**
		 * The meta object literal for the '<em>Hash Map</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see java.util.HashMap
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getHashMap()
		 * @generated
		 */
		EDataType HASH_MAP = eINSTANCE.getHashMap();
		/**
		 * The meta object literal for the '<em>Composite</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see org.eclipse.swt.widgets.Composite
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getComposite()
		 * @generated
		 */
		EDataType COMPOSITE = eINSTANCE.getComposite();
		/**
		 * The meta object literal for the '<em>ISelection Listener</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ui.ISelectionListener
		 * @see ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIPackageImpl#getISelectionListener()
		 * @generated
		 */
		EDataType ISELECTION_LISTENER = eINSTANCE.getISelectionListener();

	}

} //ApogyCoreInvocatorUIPackage

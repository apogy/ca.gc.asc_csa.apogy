/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *  
 *  Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *      Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *      Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *      Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.invocator.ui;

import ca.gc.asc_csa.apogy.core.invocator.Program;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISelectionListener;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program UI Factory</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 *  -------------------------------------------------------------------------
 * 
 * Factory used to create Program specific UI elements.
 * 
 * -------------------------------------------------------------------------
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage#getProgramUIFactory()
 * @model abstract="true"
 * @generated
 */
public interface ProgramUIFactory extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Program specific composite for the specified program.
	 * @param parent The parent composite.
	 * @param program The Program to display in the composite.
	 * @param selectionListener The selectionListener to call when a selection if made within the returned composite.
	 * @return The composite used to display the Program.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.core.invocator.ui.Composite" unique="false" parentDataType="ca.gc.asc_csa.apogy.core.invocator.ui.Composite" parentUnique="false" programUnique="false" selectionListenerDataType="ca.gc.asc_csa.apogy.core.invocator.ui.ISelectionListener" selectionListenerUnique="false"
	 * @generated
	 */
	Composite createProgramComposite(Composite parent, Program program, ISelectionListener selectionListener);

} // ProgramUIFactory

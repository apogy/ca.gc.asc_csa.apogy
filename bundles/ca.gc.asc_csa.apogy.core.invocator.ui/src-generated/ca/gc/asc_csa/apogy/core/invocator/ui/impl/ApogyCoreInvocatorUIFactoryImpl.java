package ca.gc.asc_csa.apogy.core.invocator.ui.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.core.invocator.Type;
import java.util.List;

import java.util.SortedSet;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISelectionListener;
import ca.gc.asc_csa.apogy.core.invocator.Variable;
import ca.gc.asc_csa.apogy.core.invocator.ui.*;
import java.util.HashMap;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFactory;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage;
import ca.gc.asc_csa.apogy.core.invocator.ui.NewProgramSettings;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc --> * @generated
 */
public class ApogyCoreInvocatorUIFactoryImpl extends EFactoryImpl implements ApogyCoreInvocatorUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public static ApogyCoreInvocatorUIFactory init() {
		try {
			ApogyCoreInvocatorUIFactory theApogyCoreInvocatorUIFactory = (ApogyCoreInvocatorUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyCoreInvocatorUIPackage.eNS_URI);
			if (theApogyCoreInvocatorUIFactory != null) {
				return theApogyCoreInvocatorUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyCoreInvocatorUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCoreInvocatorUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyCoreInvocatorUIPackage.APOGY_CORE_INVOCATOR_UI_FACADE: return createApogyCoreInvocatorUIFacade();
			case ApogyCoreInvocatorUIPackage.NEW_PROGRAM_SETTINGS: return createNewProgramSettings();
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY: return createProgramUIFactoriesRegistry();
			case ApogyCoreInvocatorUIPackage.OPERATION_CALLS_LIST_PROGRAM_UI_FACTORY: return createOperationCallsListProgramUIFactory();
			case ApogyCoreInvocatorUIPackage.TYPE_MEMBER_WIZARD_PAGES_PROVIDER: return createTypeMemberWizardPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ApogyCoreInvocatorUIPackage.MPART:
				return createMPartFromString(eDataType, initialValue);
			case ApogyCoreInvocatorUIPackage.COMPOUND_COMMAND:
				return createCompoundCommandFromString(eDataType, initialValue);
			case ApogyCoreInvocatorUIPackage.LIST_VARIABLES:
				return createListVariablesFromString(eDataType, initialValue);
			case ApogyCoreInvocatorUIPackage.LIST_TYPES:
				return createListTypesFromString(eDataType, initialValue);
			case ApogyCoreInvocatorUIPackage.SORTED_SET:
				return createSortedSetFromString(eDataType, initialValue);
			case ApogyCoreInvocatorUIPackage.HASH_MAP:
				return createHashMapFromString(eDataType, initialValue);
			case ApogyCoreInvocatorUIPackage.COMPOSITE:
				return createCompositeFromString(eDataType, initialValue);
			case ApogyCoreInvocatorUIPackage.ISELECTION_LISTENER:
				return createISelectionListenerFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ApogyCoreInvocatorUIPackage.MPART:
				return convertMPartToString(eDataType, instanceValue);
			case ApogyCoreInvocatorUIPackage.COMPOUND_COMMAND:
				return convertCompoundCommandToString(eDataType, instanceValue);
			case ApogyCoreInvocatorUIPackage.LIST_VARIABLES:
				return convertListVariablesToString(eDataType, instanceValue);
			case ApogyCoreInvocatorUIPackage.LIST_TYPES:
				return convertListTypesToString(eDataType, instanceValue);
			case ApogyCoreInvocatorUIPackage.SORTED_SET:
				return convertSortedSetToString(eDataType, instanceValue);
			case ApogyCoreInvocatorUIPackage.HASH_MAP:
				return convertHashMapToString(eDataType, instanceValue);
			case ApogyCoreInvocatorUIPackage.COMPOSITE:
				return convertCompositeToString(eDataType, instanceValue);
			case ApogyCoreInvocatorUIPackage.ISELECTION_LISTENER:
				return convertISelectionListenerToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public NewProgramSettings createNewProgramSettings() {
		NewProgramSettingsImpl newProgramSettings = new NewProgramSettingsImpl();
		return newProgramSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ProgramUIFactoriesRegistry createProgramUIFactoriesRegistry() {
		ProgramUIFactoriesRegistryImpl programUIFactoriesRegistry = new ProgramUIFactoriesRegistryImpl();
		return programUIFactoriesRegistry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public OperationCallsListProgramUIFactory createOperationCallsListProgramUIFactory() {
		OperationCallsListProgramUIFactoryImpl operationCallsListProgramUIFactory = new OperationCallsListProgramUIFactoryImpl();
		return operationCallsListProgramUIFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeMemberWizardPagesProvider createTypeMemberWizardPagesProvider() {
		TypeMemberWizardPagesProviderImpl typeMemberWizardPagesProvider = new TypeMemberWizardPagesProviderImpl();
		return typeMemberWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public MPart createMPartFromString(EDataType eDataType, String initialValue) {
		return (MPart)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertMPartToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCoreInvocatorUIFacade createApogyCoreInvocatorUIFacade() {
		ApogyCoreInvocatorUIFacadeImpl apogyCoreInvocatorUIFacade = new ApogyCoreInvocatorUIFacadeImpl();
		return apogyCoreInvocatorUIFacade;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public CompoundCommand createCompoundCommandFromString(EDataType eDataType, String initialValue) {
		return (CompoundCommand)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertCompoundCommandToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<Variable> createListVariablesFromString(EDataType eDataType, String initialValue) {
		return (List<Variable>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertListVariablesToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<Type> createListTypesFromString(EDataType eDataType, String initialValue) {
		return (List<Type>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertListTypesToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public SortedSet<?> createSortedSetFromString(EDataType eDataType, String initialValue) {
		return (SortedSet<?>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertSortedSetToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public HashMap<?, ?> createHashMapFromString(EDataType eDataType, String initialValue) {
		return (HashMap<?, ?>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertHashMapToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Composite createCompositeFromString(EDataType eDataType, String initialValue) {
		return (Composite)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertCompositeToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISelectionListener createISelectionListenerFromString(EDataType eDataType, String initialValue) {
		return (ISelectionListener)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertISelectionListenerToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCoreInvocatorUIPackage getApogyCoreInvocatorUIPackage() {
		return (ApogyCoreInvocatorUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyCoreInvocatorUIPackage getPackage() {
		return ApogyCoreInvocatorUIPackage.eINSTANCE;
	}

} //ApogyCoreInvocatorUIFactoryImpl

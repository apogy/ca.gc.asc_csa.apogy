/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *  
 *  Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *      Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *      Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *      Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.invocator.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFactory;
import ca.gc.asc_csa.apogy.core.invocator.Type;
import ca.gc.asc_csa.apogy.core.invocator.TypeMember;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIRCPConstants;
import ca.gc.asc_csa.apogy.core.invocator.ui.TypeMemberWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.invocator.ui.wizards.TypeMemberFeatureNodeWizardPage;
import ca.gc.asc_csa.apogy.core.invocator.ui.wizards.TypeMemberTypeWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Member Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TypeMemberWizardPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements TypeMemberWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeMemberWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreInvocatorUIPackage.Literals.TYPE_MEMBER_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		TypeMember typeMember = ApogyCoreInvocatorFactory.eINSTANCE.createTypeMember();
		
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings map = (MapBasedEClassSettings) settings;
			String name = (String) map.getUserDataMap().get(ApogyCoreInvocatorUIRCPConstants.NAME_ID);
			if(name != null)
			{
				typeMember.setName(name);
			}
		}
		
		return typeMember;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		TypeMember typeMember = (TypeMember) eObject;
				
		Type parentType = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings map = (MapBasedEClassSettings) settings;
			parentType =  (Type) map.getUserDataMap().get(ApogyCoreInvocatorUIRCPConstants.TYPE_ID);			
		}
		
		// Add other pages
		TypeMemberTypeWizardPage typeMemberTypeWizardPage = new TypeMemberTypeWizardPage(typeMember);
		list.add(typeMemberTypeWizardPage);
		
		TypeMemberFeatureNodeWizardPage typeMemberFeatureNodeWizardPage  = new TypeMemberFeatureNodeWizardPage(parentType, typeMember);
		list.add(typeMemberFeatureNodeWizardPage);
		
		return list;
	}
	
} //TypeMemberWizardPagesProviderImpl

/**
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.ros.ui;

import ca.gc.asc_csa.apogy.addons.Simple3DTool;
import ca.gc.asc_csa.apogy.addons.ros.ROSInterface;
import ca.gc.asc_csa.apogy.addons.ros.ROSNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TF Display3 DTool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Tool used to display a ROS Transform into the 3D environment.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#getToolRosNode <em>Tool Ros Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#isConnected <em>Connected</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#getTopicName <em>Topic Name</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.addons.ros.ui.ApogyAddonsROSUIPackage#getTFDisplay3DTool()
 * @model
 * @generated
 */
public interface TFDisplay3DTool extends Simple3DTool, ROSInterface {

	/**
	 * Returns the value of the '<em><b>Tool Ros Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tool Ros Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The ROS node used to monitor the TF.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Tool Ros Node</em>' containment reference.
	 * @see #setToolRosNode(ROSNode)
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.ApogyAddonsROSUIPackage#getTFDisplay3DTool_ToolRosNode()
	 * @model containment="true"
	 * @generated
	 */
	ROSNode getToolRosNode();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#getToolRosNode <em>Tool Ros Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tool Ros Node</em>' containment reference.
	 * @see #getToolRosNode()
	 * @generated
	 */
	void setToolRosNode(ROSNode value);

	/**
	 * Returns the value of the '<em><b>Connected</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Displays whether or not the tool if connected to its topic.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Connected</em>' attribute.
	 * @see #setConnected(boolean)
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.ApogyAddonsROSUIPackage#getTFDisplay3DTool_Connected()
	 * @model default="false" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' children='false' property='Readonly'"
	 * @generated
	 */
	boolean isConnected();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#isConnected <em>Connected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connected</em>' attribute.
	 * @see #isConnected()
	 * @generated
	 */
	void setConnected(boolean value);

	/**
	 * Returns the value of the '<em><b>Topic Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The name of the topic the provides the transform.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Topic Name</em>' attribute.
	 * @see #setTopicName(String)
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.ApogyAddonsROSUIPackage#getTFDisplay3DTool_TopicName()
	 * @model default="" unique="false"
	 * @generated
	 */
	String getTopicName();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#getTopicName <em>Topic Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Topic Name</em>' attribute.
	 * @see #getTopicName()
	 * @generated
	 */
	void setTopicName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Starts tool and connected to the topic.
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 * @generated
	 */
	boolean start();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Stops the tool and disconnects from the topic.
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 * @generated
	 */
	boolean stop();
} // TFDisplay3DTool

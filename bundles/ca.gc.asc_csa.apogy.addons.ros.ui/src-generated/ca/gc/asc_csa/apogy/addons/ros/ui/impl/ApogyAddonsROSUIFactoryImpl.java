/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ros.ui.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ca.gc.asc_csa.apogy.addons.ros.ui.ApogyAddonsROSUIFactory;
import ca.gc.asc_csa.apogy.addons.ros.ui.ApogyAddonsROSUIPackage;
import ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyAddonsROSUIFactoryImpl extends EFactoryImpl implements ApogyAddonsROSUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyAddonsROSUIFactory init() {
		try {
			ApogyAddonsROSUIFactory theApogyAddonsROSUIFactory = (ApogyAddonsROSUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyAddonsROSUIPackage.eNS_URI);
			if (theApogyAddonsROSUIFactory != null) {
				return theApogyAddonsROSUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyAddonsROSUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsROSUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL: return createTFDisplay3DTool();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TFDisplay3DTool createTFDisplay3DTool() {
		TFDisplay3DToolImpl tfDisplay3DTool = new TFDisplay3DToolImpl();
		return tfDisplay3DTool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsROSUIPackage getApogyAddonsROSUIPackage() {
		return (ApogyAddonsROSUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyAddonsROSUIPackage getPackage() {
		return ApogyAddonsROSUIPackage.eINSTANCE;
	}

} //ApogyAddonsROSUIFactoryImpl

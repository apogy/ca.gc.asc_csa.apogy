/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ros.ui.composites;

import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.addons.ros.ROSInterface;

public class ROSInterfaceListComposite extends Composite 
{
	private EList<ROSInterface> rosInterfaceList;
	private ROSInterface selectedROSInterface = null;
		
	private Tree tree;	
	private TreeViewer treeViewer;
				
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;
	
	public ROSInterfaceListComposite(Composite parent, int style) 
	{		
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tree.widthHint = 500;
		gd_tree.minimumWidth = 500;
		gd_tree.heightHint = 500;
		gd_tree.minimumHeight = 500;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory)
		{
			@Override
			public Object[] getElements(Object inputElement) 
			{								
				if(inputElement instanceof List)
				{								
					List<?> interfaceList = (List<?>) inputElement;														
					return interfaceList.toArray();
				}			
						
				return null;
			}

			@Override
			public Object[] getChildren(Object parentElement) 
			{				
				return null;
			}
			
			@Override
			public boolean hasChildren(Object object) {
				return false;
			}

		});
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				selectedROSInterface = (ROSInterface)((IStructuredSelection) event.getSelection()).getFirstElement();
				newROSInterfaceSelected(selectedROSInterface);					
			}
		});		
		
				
		this.addDisposeListener(new DisposeListener()
		{		
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}

	public EList<ROSInterface> getRosInterfaceList() {
		return rosInterfaceList;
	}

	public void setRosInterfaceList(EList<ROSInterface> rosInterfaceList) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.rosInterfaceList = rosInterfaceList;
		
		if(rosInterfaceList != null)
		{						
			m_bindingContext = customInitDataBindings();
			treeViewer.setInput(rosInterfaceList);
				
			// Selects the first EarthSurfaceWorksite found.
			selectedROSInterface = getFirstROSInterface(rosInterfaceList);		
			if(selectedROSInterface != null) treeViewer.setSelection(new StructuredSelection(selectedROSInterface), true);
			newROSInterfaceSelected(selectedROSInterface);
		}	
		else
		{
			treeViewer.setSelection(null, true);
			newROSInterfaceSelected(null);
		}
	}	
	
	protected void newROSInterfaceSelected(ROSInterface rosInterface)
	{		
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		// IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
		
		return bindingContext;
	}
	
	protected ROSInterface getFirstROSInterface(EList<ROSInterface> rosInterfaceList)
	{		
		if(!rosInterfaceList.isEmpty())
		{
			return rosInterfaceList.get(0);
		}
		else
		{
			return null;
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ros.ui.parts;

import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.ros.ApogyROSRegistry;
import ca.gc.asc_csa.apogy.addons.ros.ui.composites.ROSInterfacesComposite;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;

public class ROSRegistryPart extends AbstractSessionBasedPart
{
	@Inject
	protected EPartService ePartService;
	
	private ROSInterfacesComposite rosInterfacesComposite;
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{
		if(invocatorSession != null)
		{
			rosInterfacesComposite.setApogyROSRegistry(ApogyROSRegistry.INSTANCE);			
		}		
		else
		{
			rosInterfacesComposite.setApogyROSRegistry(null);		
		}
		
	}	
	
	@Override
	protected void createContentComposite(Composite parent, int style) 
	{
		parent.setLayout(new FillLayout());
		rosInterfacesComposite = new ROSInterfacesComposite(parent, SWT.NONE);				
	}
	
	/**
	 * Method called before the Part is destroyed.
	 * 
	 * @param mPart
	 *            The reference to the Part model.
	 */
	public void userPreDestroy(MPart mPart) 
	{
		if(rosInterfacesComposite != null && !rosInterfacesComposite.isDisposed())
		{
			rosInterfacesComposite.setApogyROSRegistry(null);
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.converters.ui.part;

import javax.annotation.PostConstruct;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import ca.gc.asc_csa.apogy.common.converters.ApogyCommonConvertersFacade;
import ca.gc.asc_csa.apogy.common.converters.graphs.ApogyCommonConvertersGraphsFacade;
import ca.gc.asc_csa.apogy.common.converters.graphs.ConverterEdge;
import ca.gc.asc_csa.apogy.common.converters.ui.composites.AvailableConversionComposite;
import ca.gc.asc_csa.apogy.common.converters.ui.composites.ConverterGraphComposite;
import ca.gc.asc_csa.apogy.common.converters.ui.composites.ConvertersUIConstants.ClassNameDisplayMode;

public class ConvertersPart
{
	private Composite parent = null;
	private ConverterGraphComposite converterGraphComposite = null;
	private AvailableConversionComposite availableConversionComposite = null;

	@PostConstruct
	public void createPartControl(Composite parent) 
	{
		this.parent = parent;
		this.parent.setLayout(new FillLayout());				
		SimpleDirectedWeightedGraph<Class<?>, ConverterEdge> graph = ApogyCommonConvertersFacade.INSTANCE.getGraph();
		converterGraphComposite = new ConverterGraphComposite(parent, SWT.NONE, graph);
		availableConversionComposite = new AvailableConversionComposite(parent, SWT.NONE, graph);				
		ApogyCommonConvertersGraphsFacade.INSTANCE.getAvailableDestinationTypeMap(graph);
	}		
	
	public void setClassNameDisplayMode(ClassNameDisplayMode classNameDisplayMode) 
	{
		if(converterGraphComposite != null)
		{
			converterGraphComposite.setClassNameDisplayMode(classNameDisplayMode);
		}
		
		if(availableConversionComposite != null)
		{
			availableConversionComposite.setClassNameDisplayMode(classNameDisplayMode);
		}
	}
}

package ca.gc.asc_csa.apogy.common.converters.ui.composites;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

public class ConvertersUIConstants 
{
	/**
	 * Defines the class name display modes.
	 * @author pallard
	 *
	 */
	public enum ClassNameDisplayMode 
	{
		FULLY_QUALIFIED_CLASS_NAME, SIMPLE_CLASS_NAME
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.converters.ui.wizards;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;

import ca.gc.asc_csa.apogy.common.converters.IFileExporter;
import ca.gc.asc_csa.apogy.common.converters.ui.Activator;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;

public class ExportToFileWizard extends Wizard 
{
	private IFileExporter iFileExporter;
	private Object input;
	
	private ExportFileSettingsWizardPage pageOne;
	private ExportFileDestinationWizardPage pageTwo;
	
	public ExportToFileWizard(IFileExporter iFileExporter, Object input) 
	{
		super();
		setWindowTitle("Export to File");
		this.iFileExporter = iFileExporter;
		this.input = input;
	}
	
	@Override
	public boolean canFinish() 
	{		
		return pageOne.isPageComplete() && pageTwo.isPageComplete();
	}
	
	@Override
	public boolean performFinish() 
	{
		try
		{
			iFileExporter.exportToFile(input, pageTwo.getFilePath(), pageOne.getRequestedFileExtension());
			return true;
		}
		catch (Throwable t) 
		{
			MessageDialog.openError(getShell(), "Error", "Error occured during export : " + t.getMessage() );
			Logger.INSTANCE.log(Activator.PLUGIN_ID, "Error occured during export : ", EventSeverity.ERROR, t);
			return false;
		}
	}

	public List<String> getRequestedExtensions()
	{
		return pageOne.getRequestedFileExtension();
	}
	
	@Override
	public void addPages() 
	{
		pageOne = new ExportFileSettingsWizardPage("Export Settings", iFileExporter, this);
		addPage(pageOne);
		
		pageTwo = new ExportFileDestinationWizardPage("Export Destination", iFileExporter, this);
		addPage(pageTwo);
	}
}

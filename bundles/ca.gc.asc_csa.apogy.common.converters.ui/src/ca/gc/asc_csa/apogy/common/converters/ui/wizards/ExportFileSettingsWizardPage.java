/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.converters.ui.wizards;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.converters.IFileExporter;

public class ExportFileSettingsWizardPage extends WizardPage 
{
	private IFileExporter iFileExporter;
	
	private Composite container;
	private TableViewer availableExtensionsViewer;
	private TableViewer toExportExtensionsViewer;
	private Text txtDescription;
	
	private Button addButton;
	private Button removeButton;
	
	private List<String> availableExtensions = new ArrayList<String>();
	private List<String> toExportExtensions = new ArrayList<String>();
	
	private List<String> selectedExtensions = new ArrayList<String>();
	
	public ExportFileSettingsWizardPage(String pageName, IFileExporter iFileExporter, ExportToFileWizard wizard) 
	{
		super(pageName);		
		setDescription("Select the file extension to use for the export.");
		this.iFileExporter = iFileExporter;
		
		
		availableExtensions.addAll(iFileExporter.getSupportedFileExtensions());
	}

	@Override
	public void createControl(Composite parent) 
	{
		container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(3, false);
		container.setLayout(layout);
	
		Label lblAvailableExtensions = new Label(container, SWT.NONE);
		lblAvailableExtensions.setText("Available Extensions");
		lblAvailableExtensions.setAlignment(SWT.CENTER);
		lblAvailableExtensions.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));
		
		// Filler
		new Label(container, SWT.NONE);
		
		Label lblToExportExtensions = new Label(container, SWT.NONE);
		lblToExportExtensions.setText("Extension To Export");
		lblToExportExtensions.setAlignment(SWT.CENTER);
		lblToExportExtensions.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));
		
		// Available Extension Viewer
		availableExtensionsViewer = new TableViewer(container, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		Table table = availableExtensionsViewer.getTable();		
		GridData gd_table = new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 2);
		gd_table.minimumWidth = 300;
		gd_table.widthHint = 300;
		table.setLayoutData(gd_table);
		table.setLinesVisible(true);
		ColumnViewerToolTipSupport.enableFor(availableExtensionsViewer);
		availableExtensionsViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				addButton.setEnabled(true);
				removeButton.setEnabled(false);
				
				if(event.getSelection() instanceof IStructuredSelection)
				{
					selectedExtensions.clear();
					
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					@SuppressWarnings("unchecked")
					Iterator<Object> it = iStructuredSelection.iterator();
					while(it.hasNext())
					{
						Object item = it.next();
						if(item instanceof String)
						{
							String extension  = (String) item;
							selectedExtensions.add(extension);
							
							String description = iFileExporter.getDescription(extension);
							if(description == null) description = "No description available.";
							txtDescription.setText(description);
						}
					}
				}
			}
		});

		TableViewerColumn tableViewerColumnItem_Name = new TableViewerColumn(availableExtensionsViewer, SWT.NONE);
		TableColumn trclmnItemName = tableViewerColumnItem_Name.getColumn();
		trclmnItemName.setText("Extension");
		trclmnItemName.setWidth(100);	
				
		availableExtensionsViewer.setContentProvider(new ArrayContentProvider());
		availableExtensionsViewer.setLabelProvider(new CustomLabelProvider());
		availableExtensionsViewer.setInput(availableExtensions);
		
		// Add Button
		addButton = new Button(container, SWT.PUSH);
		addButton.setText("->");		
		addButton.setEnabled(false);
		addButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				toExportExtensions.addAll(selectedExtensions);
				availableExtensions.removeAll(selectedExtensions);
				
				selectedExtensions.clear();
				
				availableExtensionsViewer.setInput(availableExtensions);
				availableExtensionsViewer.refresh();
				
				toExportExtensionsViewer.setInput(toExportExtensions);
				toExportExtensionsViewer.refresh();
				
				setPageComplete(!toExportExtensions.isEmpty());				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {				
			}
		});
		
		// Available Extension Viewer
		toExportExtensionsViewer = new TableViewer(container, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		Table table1 = toExportExtensionsViewer.getTable();
		GridData gd_table1 = new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 2);
		gd_table1.minimumWidth = 300;
		gd_table1.widthHint = 300;
		table1.setLayoutData(gd_table1);
		table1.setLinesVisible(true);
		ColumnViewerToolTipSupport.enableFor(toExportExtensionsViewer);
		toExportExtensionsViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				addButton.setEnabled(false);
				removeButton.setEnabled(true);
				
				if(event.getSelection() instanceof IStructuredSelection)
				{
					selectedExtensions.clear();
					
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					@SuppressWarnings("unchecked")
					Iterator<Object> it = iStructuredSelection.iterator();
					while(it.hasNext())
					{
						Object item = it.next();
						if(item instanceof String)
						{
							String extension  = (String) item;
							selectedExtensions.add(extension);
							
							String description = iFileExporter.getDescription(extension);
							if(description == null) description = "No description available.";
							txtDescription.setText(description);
						}
					}
				}
			}
		});		

		TableViewerColumn tableViewerColumnItem_Name1 = new TableViewerColumn(toExportExtensionsViewer, SWT.NONE);
		TableColumn trclmnItemName1 = tableViewerColumnItem_Name1.getColumn();
		trclmnItemName1.setText("Extension");
		trclmnItemName1.setWidth(100);	
				
		toExportExtensionsViewer.setContentProvider(new ArrayContentProvider());
		toExportExtensionsViewer.setLabelProvider(new CustomLabelProvider());
		toExportExtensionsViewer.setInput(toExportExtensions);
		
		// RemoveSS Button
		removeButton = new Button(container, SWT.PUSH);
		removeButton.setText("<-");
		removeButton.setEnabled(false);
		removeButton.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				toExportExtensions.removeAll(selectedExtensions);
				availableExtensions.addAll(selectedExtensions);
				
				selectedExtensions.clear();
				
				availableExtensionsViewer.setInput(availableExtensions);
				toExportExtensionsViewer.setInput(toExportExtensions);
				
				setPageComplete(!toExportExtensions.isEmpty());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {				
			}
		});
				
		Label lblDescription = new Label(container, SWT.NONE);
		lblDescription.setText("Description :");
		
		// Filler
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		txtDescription = new Text(container, SWT.MULTI);
		GridData txtDescriptionGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
		txtDescriptionGridData.minimumHeight = 100;
		txtDescriptionGridData.heightHint = 100;
		txtDescription.setLayoutData(txtDescriptionGridData);
		
		setControl(container);
	}
	
	@Override
	public boolean isPageComplete() 
	{
		return !toExportExtensions.isEmpty();
	}
	
	public List<String> getRequestedFileExtension()
	{
		return toExportExtensions;
	}

	private class CustomLabelProvider extends ColumnLabelProvider
	{
		@Override
		public String getText(Object element) 
		{
			if(element instanceof String) return (String) element;
			else return null;				
		}
		
	}
}

package ca.gc.asc_csa.apogy.common.converters.ui.utils;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.util.Comparator;

public class FullyQualifiedTypesNameComparator implements Comparator <Class<?>>{

	@Override
	public int compare(Class<?> o1, Class<?> o2) 
	{
		String name1 = o1.getName();
		String name2 = o2.getName();
		return name1.compareTo(name2);
	}

}

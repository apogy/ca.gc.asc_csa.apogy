/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.converters.ui.wizards;

import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.converters.IFileExporter;

public class ExportFileDestinationWizardPage extends WizardPage 
{
	private ExportToFileWizard wizard;	
	private String filePath = null;
	
	private Composite container;
	private Text txtPath;

	public ExportFileDestinationWizardPage(String pageName, IFileExporter iFileExporter, ExportToFileWizard wizard) 
	{
		super(pageName);	
		setDescription("Select the destination for the export.");		
		this.wizard = wizard;
	}	
	
	@Override
	public void createControl(Composite parent) 
	{				
		container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(3, false);
		container.setLayout(layout);
	
		Label lblPath = new Label(container, SWT.NONE);
		lblPath.setText("Destination Path :");
		GridData gd_lblPath = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		lblPath.setLayoutData(gd_lblPath);

		txtPath = new Text(container, SWT.BORDER);
		GridData gd_txtPath = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 3);
		txtPath.setLayoutData(gd_txtPath);

		Button btnSelect = new Button(container, SWT.PUSH);
		btnSelect.setText("Select...");
		GridData btnSelectGridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		btnSelect.setLayoutData(btnSelectGridData);
		
		btnSelect.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{				
				String currentDir = System.getProperty("user.dir");
				
				FileDialog fileChooser = new FileDialog(container.getShell(), SWT.SAVE);
				fileChooser.setText("Select a file:");
				fileChooser.setFilterPath(currentDir);
				
				String[] extensions = getFileExtensions();
				if(extensions == null || extensions.length == 0)
				{
					extensions = new String[]{"*.*"};
				}
				fileChooser.setFilterExtensions(getFileExtensions());		
				filePath = fileChooser.open();
				
				if(filePath != null)
				{
					// TODO : Strips extension.
					int index = filePath.lastIndexOf(".");
					if(index > 0)
					{
						filePath = filePath.substring(0, index);
					}
					txtPath.setText(filePath);
				}
				else
				{
					txtPath.setText("");
				}
								
				
				if(filePath != null && filePath.length() > 0) setPageComplete(true);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			private String[] getFileExtensions()
			{
				List<String> requestedFileExtension = wizard.getRequestedExtensions();
				
				if(requestedFileExtension.isEmpty())
				{
					return new String[0];
				}
				else
				{
					String[] extensions = new String[requestedFileExtension.size()];
					Iterator<String> it = requestedFileExtension.iterator();
					int i = 0;
					while(it.hasNext())
					{
						String s = it.next();
						s.trim();																	
						extensions[i] = "*." + s;
						i++;
					}
					
					return extensions;
					
				}
			}
		});
		
		
		setControl(container);
	}
	
	public String getFilePath()
	{
		return filePath;
	}
	
	@Override
	public boolean isPageComplete() 
	{
		return filePath != null && filePath.length() > 0;
	}
}

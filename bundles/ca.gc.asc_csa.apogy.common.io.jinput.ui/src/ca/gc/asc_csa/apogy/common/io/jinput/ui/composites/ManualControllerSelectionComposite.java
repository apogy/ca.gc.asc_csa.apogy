/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.io.jinput.ui.composites;

import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.io.jinput.ApogyCommonIOJInputFacade;
import ca.gc.asc_csa.apogy.common.io.jinput.ApogyCommonIOJInputPackage;
import ca.gc.asc_csa.apogy.common.io.jinput.EComponent;
import ca.gc.asc_csa.apogy.common.io.jinput.EComponentQualifier;
import ca.gc.asc_csa.apogy.common.io.jinput.EController;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class ManualControllerSelectionComposite extends Composite 
{
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	private EComponentQualifier eComponentQualifier = null;
	private DataBindingContext m_bindingContext;

	private ComboViewer controllerComboViewer;
	private ComboViewer componentComboViewer;
	
	public ManualControllerSelectionComposite(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new FillLayout());
		
		Composite manualComposite = new Composite(this, SWT.None);
		manualComposite.setLayout(new GridLayout(2, false));		
		
		Label controllerLabel = new Label(manualComposite, SWT.None);
		controllerLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));		
		controllerLabel.setText("Controller :");
		
		controllerComboViewer = new ComboViewer(manualComposite, SWT.NONE);
		controllerComboViewer.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		controllerComboViewer.setContentProvider(new ArrayContentProvider());
		controllerComboViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));			
		controllerComboViewer.setInput(getControllers());
	
		Label componentLabel = new Label(manualComposite, SWT.None);
		componentLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		componentLabel.setText("Component :");
		
		componentComboViewer = new ComboViewer(manualComposite, SWT.NONE);
		componentComboViewer.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		componentComboViewer.setContentProvider(new ArrayContentProvider());
		componentComboViewer.setLabelProvider(new LabelProvider() 
		{
		    @Override
		    public String getText(Object element) 
		    {
		        if (element instanceof EComponent) 
		        {
		        	EComponent eComponent = (EComponent) element;
		            return eComponent.getName();
		        }
		        return super.getText(element);
		    }
		});
					
		// Disabled automatic selection.
		ApogyCommonIOJInputFacade.INSTANCE.setSelectingComponent(false);

		
		addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent arg0) 
			{				
				if (m_bindingContext != null) 
				{
					m_bindingContext.dispose();
				}
			}
		});
	}
	
	public EComponentQualifier getEComponentQualifier() 
	{
		return eComponentQualifier;
	}

	public void setEComponentQualifier(EComponentQualifier eComponentQualifier) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		this.eComponentQualifier = eComponentQualifier;

		if (eComponentQualifier != null) 
		{
			initDataBindings();
		}						
	}
	
	protected void newSelection(ISelection selection) 
	{
	}
	
	protected List<EController> getControllers()
	{
		return ca.gc.asc_csa.apogy.common.io.jinput.Activator.getEControllerEnvironment().getControllers();
	}

	@SuppressWarnings({ "unchecked" })
	protected DataBindingContext initDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Controller.*/
		IObservableValue<?> observeController = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(),
				   								FeaturePath.fromList(ApogyCommonIOJInputPackage.Literals.ECOMPONENT_QUALIFIER__ECONTROLLER_NAME)).observe(getEComponentQualifier());

		IObservableValue<?> observeControllerComboViewerViewer = ViewerProperties.singleSelection().observe(controllerComboViewer);
		
		UpdateValueStrategy controllerUpdateValueStrategy = new UpdateValueStrategy()
				{
					@SuppressWarnings("rawtypes")
					@Override
					protected IStatus doSet(IObservableValue observableValue, Object value) 
					{
						EController eController = (EController) value;	
						if(eController != null)
						{														
							ApogyCommonTransactionFacade.INSTANCE.basicSet(getEComponentQualifier(), ApogyCommonIOJInputPackage.Literals.ECOMPONENT_QUALIFIER__ECONTROLLER_NAME, eController.getName());
							
							// Update component list
							componentComboViewer.setInput(eController.getEComponents().getComponents());
							
							if(getEComponentQualifier().getEComponentName() != null)
							{
								newSelection(new StructuredSelection(getEComponentQualifier()));
							}
							else
							{
								newSelection(null);
							}
						}
						else
						{
							ApogyCommonTransactionFacade.INSTANCE.basicSet(getEComponentQualifier(), ApogyCommonIOJInputPackage.Literals.ECOMPONENT_QUALIFIER__ECONTROLLER_NAME, null);
							newSelection(null);
						}
						return Status.OK_STATUS;
					}
				};
		
		bindingContext.bindValue(observeControllerComboViewerViewer, 
								 observeController,
								 controllerUpdateValueStrategy, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, EController.class) 
								 {								
									@Override
									public Object convert(Object arg0) 
									{
										String controllerName = (String) arg0;
										if(controllerName != null)
										{
											EController eController = ca.gc.asc_csa.apogy.common.io.jinput.Activator.getEControllerEnvironment().resolveController(controllerName);
											return eController;
										}
										else
										{
											return null;
										}
									}
								}));
		
		/* Component*/
		EController eController = (EController) controllerComboViewer.getStructuredSelection().getFirstElement();
		
		if(eController != null)
		{
			componentComboViewer.setInput(eController.getEComponents().getComponents());			
			if(eController.getEComponents().getComponents().size() > 0)
			{
				componentComboViewer.setSelection(new StructuredSelection(eController.getEComponents().getComponents().get(0)));
			}
		}
					
		
		IObservableValue<?> observeComponent= EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(),
																	  FeaturePath.fromList(ApogyCommonIOJInputPackage.Literals.ECOMPONENT_QUALIFIER__ECOMPONENT_NAME)).observe(getEComponentQualifier());
		
		UpdateValueStrategy componentUpdateValueStrategy = new UpdateValueStrategy()
		{
			@SuppressWarnings("rawtypes")
			@Override
			protected IStatus doSet(IObservableValue observableValue, Object value) 
			{
				EComponent eComponent = (EComponent) value;
				if(eComponent != null)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet(getEComponentQualifier(), ApogyCommonIOJInputPackage.Literals.ECOMPONENT_QUALIFIER__ECOMPONENT_NAME, eComponent.getName());
					newSelection(new StructuredSelection(getEComponentQualifier()));
				}
				else
				{					
					ApogyCommonTransactionFacade.INSTANCE.basicSet(getEComponentQualifier(), ApogyCommonIOJInputPackage.Literals.ECOMPONENT_QUALIFIER__ECOMPONENT_NAME, null);
					newSelection(null);
				}
				return Status.OK_STATUS;
			}
		};
								
		
		IObservableValue<?> observeComponentComboViewerViewer = ViewerProperties.singleSelection().observe(componentComboViewer);
		bindingContext.bindValue(observeComponentComboViewerViewer, 
								 observeComponent,
								 componentUpdateValueStrategy, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, EComponent.class) 
								 {								
									@Override
									public Object convert(Object arg0) 
									{
										String identifier = (String) arg0;
										if(identifier != null)
										{
											EComponent eComponent = ca.gc.asc_csa.apogy.common.io.jinput.Activator.getEControllerEnvironment().resolveEComponent(getEComponentQualifier());
											return eComponent;
										}
										else
										{
											return null;
										}
									}
								}));
		
		return bindingContext;
	}
	
}

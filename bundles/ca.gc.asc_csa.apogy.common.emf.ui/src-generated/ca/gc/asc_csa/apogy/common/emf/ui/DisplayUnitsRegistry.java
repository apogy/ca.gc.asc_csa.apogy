/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui;

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.common.emf.ui.impl.DisplayUnitsRegistryImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Display Units Registry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 *  -------------------------------------------------------------------------
 * Units preferences registry
 * -------------------------------------------------------------------------
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry#getEntriesMap <em>Entries Map</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getDisplayUnitsRegistry()
 * @model
 * @generated
 */
public interface DisplayUnitsRegistry extends EObject {
	
	/**
	 * Returns the value of the '<em><b>Entries Map</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entries Map</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Maps types to units.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Entries Map</em>' reference.
	 * @see #setEntriesMap(ETypedElementToUnitsMap)
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getDisplayUnitsRegistry_EntriesMap()
	 * @model
	 * @generated
	 */
	ETypedElementToUnitsMap getEntriesMap();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry#getEntriesMap <em>Entries Map</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entries Map</em>' reference.
	 * @see #getEntriesMap()
	 * @generated
	 */
	void setEntriesMap(ETypedElementToUnitsMap value);

	/**
	 * @generated_NOT
	 */
	public DisplayUnitsRegistry INSTANCE = DisplayUnitsRegistryImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Saves the registry in the preference.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void save();

} // DisplayUnitsRegistry

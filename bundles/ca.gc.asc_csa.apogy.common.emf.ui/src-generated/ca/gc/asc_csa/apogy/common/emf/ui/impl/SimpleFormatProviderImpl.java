/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Format Provider</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.SimpleFormatProviderImpl#getFormatPattern <em>Format Pattern</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SimpleFormatProviderImpl extends MinimalEObjectImpl.Container implements SimpleFormatProvider {
	/**
	 * The default value of the '{@link #getFormatPattern() <em>Format Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormatPattern()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAT_PATTERN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormatPattern() <em>Format Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormatPattern()
	 * @generated
	 * @ordered
	 */
	protected String formatPattern = FORMAT_PATTERN_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleFormatProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUIPackage.Literals.SIMPLE_FORMAT_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormatPattern() {
		return formatPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormatPattern(String newFormatPattern) {
		String oldFormatPattern = formatPattern;
		formatPattern = newFormatPattern;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFUIPackage.SIMPLE_FORMAT_PROVIDER__FORMAT_PATTERN, oldFormatPattern, formatPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setFormat(DecimalFormat format) {
		if(format.equals(new DecimalFormat(""))){
			setFormatPattern("");
		}else{
			setFormatPattern(format.toLocalizedPattern());
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public DecimalFormat getProvidedFormat(FormatProviderParameters parameters) {
		return getFormatPattern() != null ? new DecimalFormat(getFormatPattern()) : null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.SIMPLE_FORMAT_PROVIDER__FORMAT_PATTERN:
				return getFormatPattern();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.SIMPLE_FORMAT_PROVIDER__FORMAT_PATTERN:
				setFormatPattern((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.SIMPLE_FORMAT_PROVIDER__FORMAT_PATTERN:
				setFormatPattern(FORMAT_PATTERN_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.SIMPLE_FORMAT_PROVIDER__FORMAT_PATTERN:
				return FORMAT_PATTERN_EDEFAULT == null ? formatPattern != null : !FORMAT_PATTERN_EDEFAULT.equals(formatPattern);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCommonEMFUIPackage.SIMPLE_FORMAT_PROVIDER___GET_PROVIDED_FORMAT__FORMATPROVIDERPARAMETERS:
				return getProvidedFormat((FormatProviderParameters)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (formatPattern: ");
		result.append(formatPattern);
		result.append(')');
		return result.toString();
	}

} //SimpleFormatProviderImpl

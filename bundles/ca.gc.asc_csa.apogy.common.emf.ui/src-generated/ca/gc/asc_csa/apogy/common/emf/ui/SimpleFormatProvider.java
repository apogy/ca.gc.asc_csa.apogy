/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Format Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Simple format provider. This type of provider will return the referenced format.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider#getFormatPattern <em>Format Pattern</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getSimpleFormatProvider()
 * @model
 * @generated
 */
public interface SimpleFormatProvider extends FormatProvider {
	/**
	 * Returns the value of the '<em><b>Format Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format Pattern</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The format string.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Format Pattern</em>' attribute.
	 * @see #setFormatPattern(String)
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getSimpleFormatProvider_FormatPattern()
	 * @model unique="false"
	 * @generated
	 */
	String getFormatPattern();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider#getFormatPattern <em>Format Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format Pattern</em>' attribute.
	 * @see #getFormatPattern()
	 * @generated
	 */
	void setFormatPattern(String value);

} // SimpleFormatProvider

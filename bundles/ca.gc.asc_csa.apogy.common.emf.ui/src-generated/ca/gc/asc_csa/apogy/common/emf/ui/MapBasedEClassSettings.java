/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui;

import java.util.HashMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Map Based EClass Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Specialization of EClassSettings that provide a key-value storage of user settings.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings#getUserDataMap <em>User Data Map</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getMapBasedEClassSettings()
 * @model
 * @generated
 */
public interface MapBasedEClassSettings extends EClassSettings {
	/**
	 * Returns the value of the '<em><b>User Data Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Map that stores user data.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>User Data Map</em>' attribute.
	 * @see #setUserDataMap(HashMap)
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getMapBasedEClassSettings_UserDataMap()
	 * @model unique="false" dataType="ca.gc.asc_csa.apogy.common.emf.ui.HashMap<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EJavaObject>"
	 * @generated
	 */
	HashMap<String, Object> getUserDataMap();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings#getUserDataMap <em>User Data Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Data Map</em>' attribute.
	 * @see #getUserDataMap()
	 * @generated
	 */
	void setUserDataMap(HashMap<String, Object> value);

} // MapBasedEClassSettings

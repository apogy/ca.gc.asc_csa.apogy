/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderImpl#getPages <em>Pages</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderImpl#getEObject <em>EObject</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WizardPagesProviderImpl extends MinimalEObjectImpl.Container implements WizardPagesProvider {
	/**
	 * The cached value of the '{@link #getPages() <em>Pages</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPages()
	 * @generated
	 * @ordered
	 */
	protected EList<WizardPage> pages;
	/**
	 * The cached value of the '{@link #getEObject() <em>EObject</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEObject()
	 * @generated
	 * @ordered
	 */
	protected EObject eObject;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUIPackage.Literals.WIZARD_PAGES_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WizardPage> getPages() {
		if (pages == null) {
			pages = new EDataTypeEList<WizardPage>(WizardPage.class, this, ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__PAGES);
		}
		return pages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getEObject() {
		if (eObject != null && eObject.eIsProxy()) {
			InternalEObject oldEObject = (InternalEObject)eObject;
			eObject = eResolveProxy(oldEObject);
			if (eObject != oldEObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__EOBJECT, oldEObject, eObject));
			}
		}
		return eObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetEObject() {
		return eObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEObject(EObject newEObject) {
		EObject oldEObject = eObject;
		eObject = newEObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__EOBJECT, oldEObject, eObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EList<WizardPage> getPages(EClass eClass, EClassSettings settings) {
		this.eObject = createEObject(eClass, settings);

		if (this.eObject.eResource() == null) {
			ApogyCommonTransactionFacade.INSTANCE.addInTempTransactionalEditingDomain(this.eObject);
		}
		this.pages = instantiateWizardPages(this.eObject, settings);

		return this.pages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EObject createEObject(EClass eClass, EClassSettings settings) {
		this.eObject = EcoreUtil.create(eClass);
		ApogyCommonTransactionFacade.INSTANCE.addInTempTransactionalEditingDomain(this.eObject);
		
		return this.eObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) {
		return new BasicEList<WizardPage>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public CompoundCommand getPerformFinishCommands(EObject eObject, EClassSettings settings, EditingDomain editingDomain) {
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public IWizardPage getNextPage(IWizardPage page) {
		if (getPages().contains(page) && getPages().indexOf(page) +1 < getPages().size()) {
			return getPages().get(getPages().indexOf(page) + 1);
		} else {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__PAGES:
				return getPages();
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__EOBJECT:
				if (resolve) return getEObject();
				return basicGetEObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__PAGES:
				getPages().clear();
				getPages().addAll((Collection<? extends WizardPage>)newValue);
				return;
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__EOBJECT:
				setEObject((EObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__PAGES:
				getPages().clear();
				return;
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__EOBJECT:
				setEObject((EObject)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__PAGES:
				return pages != null && !pages.isEmpty();
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__EOBJECT:
				return eObject != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS:
				return getPages((EClass)arguments.get(0), (EClassSettings)arguments.get(1));
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS:
				return createEObject((EClass)arguments.get(0), (EClassSettings)arguments.get(1));
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS:
				return instantiateWizardPages((EObject)arguments.get(0), (EClassSettings)arguments.get(1));
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN:
				return getPerformFinishCommands((EObject)arguments.get(0), (EClassSettings)arguments.get(1), (EditingDomain)arguments.get(2));
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE:
				return getNextPage((IWizardPage)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (pages: ");
		result.append(pages);
		result.append(')');
		return result.toString();
	}

} //WizardPagesProviderImpl

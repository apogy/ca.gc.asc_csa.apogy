package ca.gc.asc_csa.apogy.common.emf.ui.util;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.ETypedElement;

import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Disposable;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.emf.Timed;
import ca.gc.asc_csa.apogy.common.emf.ui.*;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToUnitsMap;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting;
import ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage
 * @generated
 */
public class ApogyCommonEMFUIAdapterFactory extends AdapterFactoryImpl
{
  /**
	 * The cached model package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected static ApogyCommonEMFUIPackage modelPackage;

  /**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public ApogyCommonEMFUIAdapterFactory()
  {
		if (modelPackage == null) {
			modelPackage = ApogyCommonEMFUIPackage.eINSTANCE;
		}
	}

  /**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
  @Override
  public boolean isFactoryForType(Object object)
  {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

  /**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected ApogyCommonEMFUISwitch<Adapter> modelSwitch =
    new ApogyCommonEMFUISwitch<Adapter>() {
			@Override
			public Adapter caseApogyCommonEMFUIFacade(ApogyCommonEMFUIFacade object) {
				return createApogyCommonEMFUIFacadeAdapter();
			}
			@Override
			public Adapter caseSelectionBasedTimeSource(SelectionBasedTimeSource object) {
				return createSelectionBasedTimeSourceAdapter();
			}
			@Override
			public Adapter caseEClassSettings(EClassSettings object) {
				return createEClassSettingsAdapter();
			}
			@Override
			public Adapter caseMapBasedEClassSettings(MapBasedEClassSettings object) {
				return createMapBasedEClassSettingsAdapter();
			}
			@Override
			public Adapter caseWizardPagesProvider(WizardPagesProvider object) {
				return createWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseWizardPagesProviderRegistry(WizardPagesProviderRegistry object) {
				return createWizardPagesProviderRegistryAdapter();
			}
			@Override
			public Adapter caseNamedDescribedWizardPagesProvider(NamedDescribedWizardPagesProvider object) {
				return createNamedDescribedWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseNamedSetting(NamedSetting object) {
				return createNamedSettingAdapter();
			}
			@Override
			public Adapter caseDisplayUnitsRegistry(DisplayUnitsRegistry object) {
				return createDisplayUnitsRegistryAdapter();
			}
			@Override
			public Adapter caseETypedElementToUnitsMap(ETypedElementToUnitsMap object) {
				return createETypedElementToUnitsMapAdapter();
			}
			@Override
			public Adapter caseETypedElementToUnitsKeyValue(Map.Entry<ETypedElement, UnitsProvider> object) {
				return createETypedElementToUnitsKeyValueAdapter();
			}
			@Override
			public Adapter caseUnitsProvider(UnitsProvider object) {
				return createUnitsProviderAdapter();
			}
			@Override
			public Adapter caseUnitsProviderParameters(UnitsProviderParameters object) {
				return createUnitsProviderParametersAdapter();
			}
			@Override
			public Adapter caseSimpleUnitsProvider(SimpleUnitsProvider object) {
				return createSimpleUnitsProviderAdapter();
			}
			@Override
			public Adapter caseEOperationEParametersUnitsProvider(EOperationEParametersUnitsProvider object) {
				return createEOperationEParametersUnitsProviderAdapter();
			}
			@Override
			public Adapter caseEOperationEParametersUnitsProviderParameters(EOperationEParametersUnitsProviderParameters object) {
				return createEOperationEParametersUnitsProviderParametersAdapter();
			}
			@Override
			public Adapter caseDecimalFormatRegistry(DecimalFormatRegistry object) {
				return createDecimalFormatRegistryAdapter();
			}
			@Override
			public Adapter caseETypedElementToFormatStringMap(ETypedElementToFormatStringMap object) {
				return createETypedElementToFormatStringMapAdapter();
			}
			@Override
			public Adapter caseETypedElementToFormatStringKeyValue(Map.Entry<ETypedElement, FormatProvider> object) {
				return createETypedElementToFormatStringKeyValueAdapter();
			}
			@Override
			public Adapter caseFormatProvider(FormatProvider object) {
				return createFormatProviderAdapter();
			}
			@Override
			public Adapter caseFormatProviderParameters(FormatProviderParameters object) {
				return createFormatProviderParametersAdapter();
			}
			@Override
			public Adapter caseSimpleFormatProvider(SimpleFormatProvider object) {
				return createSimpleFormatProviderAdapter();
			}
			@Override
			public Adapter caseEOperationEParametersFormatProvider(EOperationEParametersFormatProvider object) {
				return createEOperationEParametersFormatProviderAdapter();
			}
			@Override
			public Adapter caseEOperationEParametersFormatProviderParameters(EOperationEParametersFormatProviderParameters object) {
				return createEOperationEParametersFormatProviderParametersAdapter();
			}
			@Override
			public Adapter caseTreeFeatureNodeWizardPageProvider(TreeFeatureNodeWizardPageProvider object) {
				return createTreeFeatureNodeWizardPageProviderAdapter();
			}
			@Override
			public Adapter caseNamed(Named object) {
				return createNamedAdapter();
			}
			@Override
			public Adapter caseDescribed(Described object) {
				return createDescribedAdapter();
			}
			@Override
			public Adapter caseTimed(Timed object) {
				return createTimedAdapter();
			}
			@Override
			public Adapter caseDisposable(Disposable object) {
				return createDisposableAdapter();
			}
			@Override
			public Adapter caseTimeSource(TimeSource object) {
				return createTimeSourceAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

  /**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
  @Override
  public Adapter createAdapter(Notifier target)
  {
		return modelSwitch.doSwitch((EObject)target);
	}


  /**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade <em>Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade
	 * @generated
	 */
	public Adapter createApogyCommonEMFUIFacadeAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource <em>Selection Based Time Source</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource
	 * @generated
	 */
	public Adapter createSelectionBasedTimeSourceAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings <em>EClass Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings
	 * @generated
	 */
	public Adapter createEClassSettingsAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings <em>Map Based EClass Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings
	 * @generated
	 */
	public Adapter createMapBasedEClassSettingsAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider <em>Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider
	 * @generated
	 */
	public Adapter createWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry <em>Wizard Pages Provider Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry
	 * @generated
	 */
	public Adapter createWizardPagesProviderRegistryAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider <em>Named Described Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider
	 * @generated
	 */
	public Adapter createNamedDescribedWizardPagesProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting <em>Named Setting</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting
	 * @generated
	 */
	public Adapter createNamedSettingAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry <em>Display Units Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry
	 * @generated
	 */
	public Adapter createDisplayUnitsRegistryAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>ETyped Element To Units Key Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createETypedElementToUnitsKeyValueAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToUnitsMap <em>ETyped Element To Units Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToUnitsMap
	 * @generated
	 */
	public Adapter createETypedElementToUnitsMapAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider <em>Units Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider
	 * @generated
	 */
	public Adapter createUnitsProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters <em>Units Provider Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters
	 * @generated
	 */
	public Adapter createUnitsProviderParametersAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider <em>Simple Units Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider
	 * @generated
	 */
	public Adapter createSimpleUnitsProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProvider <em>EOperation EParameters Units Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProvider
	 * @generated
	 */
	public Adapter createEOperationEParametersUnitsProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters <em>EOperation EParameters Units Provider Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters
	 * @generated
	 */
	public Adapter createEOperationEParametersUnitsProviderParametersAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry <em>Decimal Format Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry
	 * @generated
	 */
	public Adapter createDecimalFormatRegistryAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap <em>ETyped Element To Format String Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap
	 * @generated
	 */
	public Adapter createETypedElementToFormatStringMapAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>ETyped Element To Format String Key Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createETypedElementToFormatStringKeyValueAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider <em>Format Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider
	 * @generated
	 */
	public Adapter createFormatProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters <em>Format Provider Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters
	 * @generated
	 */
	public Adapter createFormatProviderParametersAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider <em>Simple Format Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider
	 * @generated
	 */
	public Adapter createSimpleFormatProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider <em>EOperation EParameters Format Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider
	 * @generated
	 */
	public Adapter createEOperationEParametersFormatProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters <em>EOperation EParameters Format Provider Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters
	 * @generated
	 */
	public Adapter createEOperationEParametersFormatProviderParametersAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.TreeFeatureNodeWizardPageProvider <em>Tree Feature Node Wizard Page Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.TreeFeatureNodeWizardPageProvider
	 * @generated
	 */
	public Adapter createTreeFeatureNodeWizardPageProviderAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.Named <em>Named</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.Named
	 * @generated
	 */
	public Adapter createNamedAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.Described <em>Described</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.Described
	 * @generated
	 */
	public Adapter createDescribedAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.Timed <em>Timed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.Timed
	 * @generated
	 */
	public Adapter createTimedAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.Disposable <em>Disposable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.Disposable
	 * @generated
	 */
	public Adapter createDisposableAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.TimeSource <em>Time Source</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.TimeSource
	 * @generated
	 */
	public Adapter createTimeSourceAdapter() {
		return null;
	}

		/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
  public Adapter createEObjectAdapter()
  {
		return null;
	}

} //ApogyCommonEMFUIAdapterFactory

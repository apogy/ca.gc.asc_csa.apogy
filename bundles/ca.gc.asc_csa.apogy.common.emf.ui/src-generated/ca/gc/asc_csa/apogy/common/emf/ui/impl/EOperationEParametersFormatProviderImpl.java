/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>EOperation EParameters Format Provider</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersFormatProviderImpl#getMap
 * <em>Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EOperationEParametersFormatProviderImpl extends MinimalEObjectImpl.Container
		implements EOperationEParametersFormatProvider {
	/**
	 * The cached value of the '{@link #getMap() <em>Map</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMap()
	 * @generated
	 * @ordered
	 */
	protected ETypedElementToFormatStringMap map;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperationEParametersFormatProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUIPackage.Literals.EOPERATION_EPARAMETERS_FORMAT_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public ETypedElementToFormatStringMap getMap() {
		ETypedElementToFormatStringMap map = getMapGen();
		if(map == null){
			map = ApogyCommonEMFUIFactory.eINSTANCE.createETypedElementToFormatStringMap();
			setMap(map);
		}
		return map;
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ETypedElementToFormatStringMap getMapGen() {
		return map;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMap(ETypedElementToFormatStringMap newMap, NotificationChain msgs) {
		ETypedElementToFormatStringMap oldMap = map;
		map = newMap;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP, oldMap, newMap);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMap(ETypedElementToFormatStringMap newMap) {
		if (newMap != map) {
			NotificationChain msgs = null;
			if (map != null)
				msgs = ((InternalEObject)map).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP, null, msgs);
			if (newMap != null)
				msgs = ((InternalEObject)newMap).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP, null, msgs);
			msgs = basicSetMap(newMap, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP, newMap, newMap));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public DecimalFormat getProvidedFormat(FormatProviderParameters parameters) {
		if (parameters instanceof EOperationEParametersFormatProviderParameters) {
			FormatProvider provider = getMap().getEntries()
					.get(((EOperationEParametersFormatProviderParameters) parameters).getParam());

			if (provider instanceof SimpleFormatProvider) {
				return ((SimpleFormatProvider) provider).getProvidedFormat(null);
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP:
				return basicSetMap(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP:
				return getMap();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP:
				setMap((ETypedElementToFormatStringMap)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP:
				setMap((ETypedElementToFormatStringMap)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP:
				return map != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER___GET_PROVIDED_FORMAT__FORMATPROVIDERPARAMETERS:
				return getProvidedFormat((FormatProviderParameters)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} // EOperationEParametersFormatProviderImpl

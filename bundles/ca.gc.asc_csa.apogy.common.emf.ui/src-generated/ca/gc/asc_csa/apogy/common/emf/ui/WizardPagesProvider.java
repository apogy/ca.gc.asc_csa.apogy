/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A class providing Wizard support for the creation of EObjects.
 * It is used to provide customized EObject creation, Wizard page and post creation Commands.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getPages <em>Pages</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getEObject <em>EObject</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getWizardPagesProvider()
 * @model
 * @generated
 */
public interface WizardPagesProvider extends EObject {
	/**
	 * Returns the value of the '<em><b>Pages</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.jface.wizard.WizardPage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pages</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Wizard pages provides.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Pages</em>' attribute list.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getWizardPagesProvider_Pages()
	 * @model unique="false" dataType="ca.gc.asc_csa.apogy.common.emf.ui.WizardPage"
	 * @generated
	 */
	EList<WizardPage> getPages();

	/**
	 * Returns the value of the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EObject</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The object being created.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>EObject</em>' reference.
	 * @see #setEObject(EObject)
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getWizardPagesProvider_EObject()
	 * @model
	 * @generated
	 */
	EObject getEObject();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getEObject <em>EObject</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EObject</em>' reference.
	 * @see #getEObject()
	 * @generated
	 */
	void setEObject(EObject value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the pages needed to create or modify an {@link EObject}.
	 * It is not recommended to override this method.
	 * @param eClass the {@link EClass} of the {@link EObject} to create.
	 * @param settings the {@link EClassSettings} needed to create the {@link EObject} or to instantiate the {@link IWizardPage}s.
	 * @return An array of the {@link WizardPage}s.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.ui.WizardPage" unique="false" eClassUnique="false" settingsUnique="false"
	 * @generated
	 */
	EList<WizardPage> getPages(EClass eClass, EClassSettings settings);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates the {@link EObject} that is modified in the {@link WizardPage}s.
	 * @param eClass The {@link EClass} of the {@link EObject} to create.
	 * @param settings If needed, the {@link EClassSettings} needed to create the {@link EObject}.
	 * @return Reference to the {@link EObject} created.
	 * <!-- end-model-doc -->
	 * @model unique="false" eClassUnique="false" settingsUnique="false"
	 * @generated
	 */
	EObject createEObject(EClass eClass, EClassSettings settings);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Instantiate the {@link IWizardPage}.
	 * @param eObject reference to the {@link EObject} modified in the pages.
	 * @param settings If needed, the {@link EClassSettings} needed to instantiate the {@link WizardPage}.
	 * @return List of pages
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.ui.WizardPage" unique="false" eObjectUnique="false" settingsUnique="false"
	 * @generated
	 */
	EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets a {@link CompoundCommand} that need to be executed when the user presses performFinish() on the wizard.
	 * @param eObject reference to the {@link EObject} modified in the pages.
	 * @param settings settings used in the creation of the eObject and in the wizard pages.
	 * @param editingDomain the {@link EditingDomain} of the container of the eObject. Used to create the commands.
	 * @return {@link CompoundCommand}
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.ui.CompoundCommand" unique="false" eObjectUnique="false" settingsUnique="false" editingDomainDataType="ca.gc.asc_csa.apogy.common.emf.ui.EditingDomain" editingDomainUnique="false"
	 * @generated
	 */
	CompoundCommand getPerformFinishCommands(EObject eObject, EClassSettings settings, EditingDomain editingDomain);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the successor of the given page.
	 * @param page The {@link IWizardPage}.
	 * @return the next {@link IWizardPage}, or null if none.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.ui.IWizardPage" unique="false" pageDataType="ca.gc.asc_csa.apogy.common.emf.ui.IWizardPage" pageUnique="false"
	 * @generated
	 */
	IWizardPage getNextPage(IWizardPage page);

} // WizardPagesProvider

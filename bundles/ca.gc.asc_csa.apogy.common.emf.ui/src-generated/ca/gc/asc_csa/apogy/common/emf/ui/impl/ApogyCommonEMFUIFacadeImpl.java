package ca.gc.asc_csa.apogy.common.emf.ui.impl;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;

import javax.measure.converter.MultiplyConverter;
import javax.measure.converter.UnitConverter;
import javax.measure.unit.Unit;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.URIConverter.ReadableInputStream;
import org.eclipse.emf.ecore.resource.URIConverter.WriteableOutputStream;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.Ranges;
import ca.gc.asc_csa.apogy.common.emf.ui.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.preferences.PreferencesConstants;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Facade</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIFacadeImpl#getUnitConverterMap <em>Unit Converter Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ApogyCommonEMFUIFacadeImpl extends MinimalEObjectImpl.Container implements ApogyCommonEMFUIFacade {
	/**
	 * The cached value of the '{@link #getUnitConverterMap() <em>Unit Converter Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitConverterMap()
	 * @generated
	 * @ordered
	 */
	protected TreeMap<UnitConverter, String> unitConverterMap;
	private static ApogyCommonEMFUIFacade instance = null;

	public static ApogyCommonEMFUIFacade getInstance() {
		if (instance == null) {
			instance = new ApogyCommonEMFUIFacadeImpl();
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonEMFUIFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUIPackage.Literals.APOGY_COMMON_EMFUI_FACADE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public TreeMap<UnitConverter, String> getUnitConverterMap() {
		TreeMap<UnitConverter, String> map = getUnitConverterMapGen();

		if (map == null || map.isEmpty()) {
			map = new TreeMap<UnitConverter, String>(new Comparator<UnitConverter>() {
				@Override
				public int compare(UnitConverter arg0, UnitConverter arg1) {
					return (arg0.convert(1) - arg1.convert(1)) < 0 ? 1 : -1;
				}
			});
			
			map.put(new MultiplyConverter(1E24),"Y");
			map.put(new MultiplyConverter(1E21), "Z");
			map.put(new MultiplyConverter(1E18), "E");
			map.put(new MultiplyConverter(1E15), "P");
			map.put(new MultiplyConverter(1E12), "T");
			map.put(new MultiplyConverter(1E9), "G");
			map.put(new MultiplyConverter(1E6), "M");
			map.put(new MultiplyConverter(1E3), "k");
			map.put(new MultiplyConverter(1E2), "h");
			map.put(new MultiplyConverter(1E1), "da");
			map.put(UnitConverter.IDENTITY," ");
			map.put(new MultiplyConverter(1E-1), "d");
			map.put(new MultiplyConverter(1E-2), "c");
			map.put(new MultiplyConverter(1E-3), "m");
			map.put(new MultiplyConverter(1E-6), "μ");
			map.put(new MultiplyConverter(1E-9), "n");
			map.put(new MultiplyConverter(1E-12), "p");
			map.put(new MultiplyConverter(1E-15), "f");
			map.put(new MultiplyConverter(1E-18), "a");
			map.put(new MultiplyConverter(1E-21), "z");
			map.put(new MultiplyConverter(1E-24), "y");

			setUnitConverterMap(map);
		}

		return getUnitConverterMapGen();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TreeMap<UnitConverter, String> getUnitConverterMapGen() {
		return unitConverterMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitConverterMap(TreeMap<UnitConverter, String> newUnitConverterMap) {
		TreeMap<UnitConverter, String> oldUnitConverterMap = unitConverterMap;
		unitConverterMap = newUnitConverterMap;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE__UNIT_CONVERTER_MAP, oldUnitConverterMap, unitConverterMap));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public Color getColorForRange(Ranges range) {
		return Activator.getDefault().getRangeColor(range, Display.getDefault());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public RGB getRGBColorForRange(Ranges range) 
	{
		return Activator.getDefault().getRangeColor(range);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public Unit<?> getDisplayUnits(ETypedElement eTypedElement) {
		Unit<?> displayUnit = null;
		
		/** try the registry */
		UnitsProvider provider = DisplayUnitsRegistry.INSTANCE.getEntriesMap().getEntries().get(eTypedElement);
		if(provider != null){
			displayUnit = provider.getProvidedUnit(null);
		}
		
		/** try the display preferences */
		if (displayUnit == null) {
			displayUnit = Activator.getDefault().getDisplayUnit(eTypedElement);
		}
		
		/** try the model units*/
		if (displayUnit == null) {
			displayUnit = ApogyCommonEMFFacade.INSTANCE.getEngineeringUnits(eTypedElement);
		}

		return displayUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public Unit<?> getDisplayUnits(EOperation eOperation, EOperationEParametersUnitsProviderParameters parameters) {
		Unit<?> displayUnit = null;
		
		/** try the registry */
		UnitsProvider provider = DisplayUnitsRegistry.INSTANCE.getEntriesMap().getEntries().get(eOperation);
		if(provider != null){
			displayUnit = provider.getProvidedUnit(parameters);
		}
		
		/** try the display preferences */
		if (displayUnit == null) {
			displayUnit = getDisplayUnits(parameters.getParam());
		}

		return displayUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void addUnitsProviderToRegistry(ETypedElement eTypedElement, UnitsProvider provider) {
		DisplayUnitsRegistry.INSTANCE.getEntriesMap().getEntries().put(eTypedElement, provider);
		DisplayUnitsRegistry.INSTANCE.save();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public Number convertToNativeUnits(Number number, Unit<?> nativeUnits, Unit<?> displayUnits, EClassifier numberType) {
		if (displayUnits != null && displayUnits != nativeUnits) {
			Double doubleValue = displayUnits.getConverterTo(nativeUnits).convert(number.doubleValue());

			/** Convert to the right type */
			if (numberType == EcorePackage.Literals.EFLOAT) {
				return doubleValue.floatValue();
			} else if (numberType == EcorePackage.Literals.EBYTE) {
				return doubleValue.byteValue();
			} else if (numberType == EcorePackage.Literals.ESHORT) {
				return doubleValue.shortValue();
			} else if (numberType == EcorePackage.Literals.EINT) {
				return doubleValue.intValue();
			} else if (numberType == EcorePackage.Literals.ELONG) {
				return doubleValue.longValue();
			} else {
				return doubleValue;
			}
		}

		return number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public DecimalFormat getDisplayFormat(ETypedElement eTypedElement) {
		DecimalFormat format = null;
		
		/** try the registry*/
		FormatProvider provider = DecimalFormatRegistry.INSTANCE.getEntriesMap().getEntries().get(eTypedElement);
		if (provider != null) {
			format = provider.getProvidedFormat(null);
		}

		/** get the format from the type */
		if(format == null){
			String formatStr = "";
			EClassifier type = eTypedElement.getEType();
			if (type == EcorePackage.Literals.EFLOAT) {
				formatStr = Activator.getDefault().getPreferenceStore()
						.getString(PreferencesConstants.NATIVE_FORMAT_FLOAT_ID);
			} else if (type == EcorePackage.Literals.EBYTE) {
				formatStr = Activator.getDefault().getPreferenceStore()
						.getString(PreferencesConstants.NATIVE_FORMAT_BYTE_ID);
			} else if (type == EcorePackage.Literals.ESHORT) {
				formatStr = Activator.getDefault().getPreferenceStore()
						.getString(PreferencesConstants.NATIVE_FORMAT_SHORT_ID);
			} else if (type == EcorePackage.Literals.EINT) {
				formatStr = Activator.getDefault().getPreferenceStore()
						.getString(PreferencesConstants.NATIVE_FORMAT_INT_ID);
			} else if (type == EcorePackage.Literals.ELONG) {
				formatStr = Activator.getDefault().getPreferenceStore()
						.getString(PreferencesConstants.NATIVE_FORMAT_LONG_ID);
			} else {
				formatStr = Activator.getDefault().getPreferenceStore()
						.getString(PreferencesConstants.NATIVE_FORMAT_DOUBLE_ID);
			}
			format = new DecimalFormat(formatStr);
		}
		return format;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public DecimalFormat getDisplayFormat(EOperation eOperation, EOperationEParametersFormatProviderParameters parameters) {
		DecimalFormat format = null;
		
		/** try the registry */
		FormatProvider provider = DecimalFormatRegistry.INSTANCE.getEntriesMap().getEntries().get(eOperation);
		if (provider != null) {
			format = provider.getProvidedFormat(parameters);
		}
		
		/** try the with the eParameter */
		if(format == null){
			format = getDisplayFormat(parameters.getParam());
		}
		
		return format;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void addFormatProviderToRegistry(ETypedElement eTypedElement, FormatProvider provider) {
		DecimalFormatRegistry.INSTANCE.getEntriesMap().getEntries().put(eTypedElement, provider);
		DecimalFormatRegistry.INSTANCE.save();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void openDeleteNamedDialog(Named named) {
		List<Named> nameds = new ArrayList<Named>();
		nameds.add(named);
		this.openDeleteNamedDialog(nameds);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void openDeleteNamedDialog(List<? extends Named> namedList) {
		String eObjectsToDeleteMessage = "";

		eObjectsToDeleteMessage += ApogyCommonEMFFacade.INSTANCE.toString(namedList, ", ");

		MessageDialog dialog = new MessageDialog(null, "Delete the selected elements", null,
				"Are you sure to delete these elements: " + eObjectsToDeleteMessage + "?", MessageDialog.QUESTION,
				new String[] { "Yes", "No" }, 1);
		int result = dialog.open();
		if (result == 0) {
			for (EObject eObject : namedList) {
				ApogyCommonTransactionFacade.INSTANCE.basicDelete(eObject.eContainer(), eObject.eContainingFeature(),
						eObject);
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void saveToPersistedState(MPart mPart, String persistedStateKey, EObject eObject, ResourceSet resourceSet) {
		if (eObject != null) {
			ApogyCommonTransactionFacade.INSTANCE.removeFromEditingDomain(eObject);

			Resource childResource = resourceSet
					.createResource(org.eclipse.emf.common.util.URI.createURI(mPart.getElementId()));
			TransactionalEditingDomain transactionalEditingDomain = (TransactionalEditingDomain) AdapterFactoryEditingDomain
					.getEditingDomainFor(resourceSet);

			RecordingCommand cmd = new RecordingCommand(transactionalEditingDomain) {
				@Override
				protected void doExecute() {
					childResource.getContents().add(eObject);
				}
			};

			try {
				transactionalEditingDomain.getCommandStack().execute(cmd);
			} catch (Throwable t) {
			}

			StringWriter stringWriter = new StringWriter();
			WriteableOutputStream os = new URIConverter.WriteableOutputStream(stringWriter,
					XMLResource.OPTION_ENCODING);
			try {
				childResource.save(os, Collections.EMPTY_MAP);
			} catch (Throwable t) {
				Logger.INSTANCE.log(Activator.ID, this, "Failed to save EObject to Part <" + mPart.getElementId()
						+ "> Persisted State at key <" + persistedStateKey + ">", EventSeverity.ERROR, t);
			}

			// Saves the XML of the model in the persisted state.
			mPart.getPersistedState().put(persistedStateKey, stringWriter.getBuffer().toString());
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public EObject readFromPersistedState(MPart mPart, String persistedStateKey, ResourceSet resourceSet) {
		EObject eObject = null;
		String modelString = mPart.getPersistedState().get(persistedStateKey);

		if (modelString != null) {
			Resource childResource = resourceSet
					.createResource(org.eclipse.emf.common.util.URI.createURI(mPart.getElementId()));
			StringReader reader = new StringReader(modelString);
			ReadableInputStream is = new URIConverter.ReadableInputStream(reader);

			try {
				childResource.load(is, Collections.EMPTY_MAP);

				if (childResource.getContents().size() > 0) {
					eObject = childResource.getContents().get(0);
				} else {
					Logger.INSTANCE.log(Activator.ID, this, "No EObject found in Part <" + mPart.getElementId()
							+ "> Persisted State at key <" + persistedStateKey + ">", EventSeverity.WARNING);
				}
			} catch (Throwable t) {
				Logger.INSTANCE.log(Activator.ID, this, "Failed to load EObject from Part <" + mPart.getElementId()
						+ "> Persisted State at key <" + persistedStateKey + ">", EventSeverity.ERROR, t);
			}

		} else {
			Logger.INSTANCE.log(Activator.ID, this, "No EObject content found in Part <" + mPart.getElementId()
					+ "> Persisted State at key <" + persistedStateKey + ">", EventSeverity.INFO);
		}

		return eObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public Image getImage(EClass eClass) {		
		final String URL_PREFIX = "platform:/plugin";
		Image image = null;
		String path = new String(URL_PREFIX);
		
		/**
		 * Try getting the image from the edit directory.
		 */
		// Get the annotation
		String editDirectory = EcoreUtil.getAnnotation(eClass.getEPackage(), "http://www.eclipse.org/emf/2002/GenModel",
				"editDirectory");

		// If the edit directory was found.
		if (editDirectory != null) {
			if (editDirectory.contains("/src-generated")) {
				editDirectory = editDirectory.substring(0, editDirectory.indexOf("/src-generated"));
			}
			path += editDirectory + "/icons/full/obj16/";
		} else {
			// Otherwise try the normal edit directory used in Apogy code.
			path += "/" + eClass.getEPackage().getNsURI() + ".edit/icons/full/obj16/";
		}

		path += eClass.getName() + ".gif";
		try {
			image = ImageDescriptor.createFromURL(new URL(path)).createImage();
		} catch (MalformedURLException e) {
			Logger.INSTANCE.log(Activator.ID, this,
					" error getting the image corresponding to the eClass from the edit directory" + eClass.getName(),
					EventSeverity.ERROR);
		}

		return image;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE__UNIT_CONVERTER_MAP:
				return getUnitConverterMap();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE__UNIT_CONVERTER_MAP:
				setUnitConverterMap((TreeMap<UnitConverter, String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE__UNIT_CONVERTER_MAP:
				setUnitConverterMap((TreeMap<UnitConverter, String>)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE__UNIT_CONVERTER_MAP:
				return unitConverterMap != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___GET_COLOR_FOR_RANGE__RANGES:
				return getColorForRange((Ranges)arguments.get(0));
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___GET_RGB_COLOR_FOR_RANGE__RANGES:
				return getRGBColorForRange((Ranges)arguments.get(0));
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_UNITS__ETYPEDELEMENT:
				return getDisplayUnits((ETypedElement)arguments.get(0));
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_UNITS__EOPERATION_EOPERATIONEPARAMETERSUNITSPROVIDERPARAMETERS:
				return getDisplayUnits((EOperation)arguments.get(0), (EOperationEParametersUnitsProviderParameters)arguments.get(1));
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___ADD_UNITS_PROVIDER_TO_REGISTRY__ETYPEDELEMENT_UNITSPROVIDER:
				addUnitsProviderToRegistry((ETypedElement)arguments.get(0), (UnitsProvider)arguments.get(1));
				return null;
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___CONVERT_TO_NATIVE_UNITS__NUMBER_UNIT_UNIT_ECLASSIFIER:
				return convertToNativeUnits((Number)arguments.get(0), (Unit<?>)arguments.get(1), (Unit<?>)arguments.get(2), (EClassifier)arguments.get(3));
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_FORMAT__ETYPEDELEMENT:
				return getDisplayFormat((ETypedElement)arguments.get(0));
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_FORMAT__EOPERATION_EOPERATIONEPARAMETERSFORMATPROVIDERPARAMETERS:
				return getDisplayFormat((EOperation)arguments.get(0), (EOperationEParametersFormatProviderParameters)arguments.get(1));
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___ADD_FORMAT_PROVIDER_TO_REGISTRY__ETYPEDELEMENT_FORMATPROVIDER:
				addFormatProviderToRegistry((ETypedElement)arguments.get(0), (FormatProvider)arguments.get(1));
				return null;
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___OPEN_DELETE_NAMED_DIALOG__NAMED:
				openDeleteNamedDialog((Named)arguments.get(0));
				return null;
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___OPEN_DELETE_NAMED_DIALOG__LIST:
				openDeleteNamedDialog((List<? extends Named>)arguments.get(0));
				return null;
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___SAVE_TO_PERSISTED_STATE__MPART_STRING_EOBJECT_RESOURCESET:
				saveToPersistedState((MPart)arguments.get(0), (String)arguments.get(1), (EObject)arguments.get(2), (ResourceSet)arguments.get(3));
				return null;
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___READ_FROM_PERSISTED_STATE__MPART_STRING_RESOURCESET:
				return readFromPersistedState((MPart)arguments.get(0), (String)arguments.get(1), (ResourceSet)arguments.get(2));
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE___GET_IMAGE__ECLASS:
				return getImage((EClass)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (unitConverterMap: ");
		result.append(unitConverterMap);
		result.append(')');
		return result.toString();
	}

} // ApogyCommonEMFUIFacadeImpl

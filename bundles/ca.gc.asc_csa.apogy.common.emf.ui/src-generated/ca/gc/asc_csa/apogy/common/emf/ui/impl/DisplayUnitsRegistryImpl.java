/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToUnitsMap;
import ca.gc.asc_csa.apogy.common.emf.ui.preferences.PreferencesConstants;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Display
 * Units Registry</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.DisplayUnitsRegistryImpl#getEntriesMap <em>Entries Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DisplayUnitsRegistryImpl extends MinimalEObjectImpl.Container implements DisplayUnitsRegistry {

	/**
	 * The cached value of the '{@link #getEntriesMap() <em>Entries Map</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntriesMap()
	 * @generated
	 * @ordered
	 */
	protected ETypedElementToUnitsMap entriesMap;
	/**
	 * @generated_NOT
	 */
	private static DisplayUnitsRegistry instance = null;

	/**
	 * @generated_NOT
	 */
	public static DisplayUnitsRegistry getInstance() {
		if (instance == null) {
			instance = new DisplayUnitsRegistryImpl();
			String stringMap = Activator.getDefault().getPreferenceStore()
					.getString(PreferencesConstants.TYPED_ELEMENTS_UNITS_ID);
			if (!"".equals(stringMap)) {
				EObject object = ApogyCommonEMFFacade.INSTANCE.deserializeString(stringMap,
						PreferencesConstants.TYPED_ELEMENTS_UNITS_ID);
				if (object instanceof ETypedElementToUnitsMap) {
					instance.setEntriesMap((ETypedElementToUnitsMap) object);
				}
			}
		}

		return instance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public DisplayUnitsRegistryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUIPackage.Literals.DISPLAY_UNITS_REGISTRY;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public ETypedElementToUnitsMap getEntriesMap() {
		ETypedElementToUnitsMap map = getEntriesMapGen();
		
		if(map == null){
			map = ApogyCommonEMFUIFactory.eINSTANCE.createETypedElementToUnitsMap();
			
			setEntriesMap(map);
		}
		
		return map;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETypedElementToUnitsMap getEntriesMapGen() {
		if (entriesMap != null && entriesMap.eIsProxy()) {
			InternalEObject oldEntriesMap = (InternalEObject)entriesMap;
			entriesMap = (ETypedElementToUnitsMap)eResolveProxy(oldEntriesMap);
			if (entriesMap != oldEntriesMap) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCommonEMFUIPackage.DISPLAY_UNITS_REGISTRY__ENTRIES_MAP, oldEntriesMap, entriesMap));
			}
		}
		return entriesMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETypedElementToUnitsMap basicGetEntriesMap() {
		return entriesMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntriesMap(ETypedElementToUnitsMap newEntriesMap) {
		ETypedElementToUnitsMap oldEntriesMap = entriesMap;
		entriesMap = newEntriesMap;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFUIPackage.DISPLAY_UNITS_REGISTRY__ENTRIES_MAP, oldEntriesMap, entriesMap));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void save() {
		Activator.getDefault().getPreferenceStore().setValue(PreferencesConstants.TYPED_ELEMENTS_UNITS_ID,
				ApogyCommonEMFFacade.INSTANCE.serializeEObject(getEntriesMap(),
						PreferencesConstants.TYPED_ELEMENTS_UNITS_ID));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.DISPLAY_UNITS_REGISTRY__ENTRIES_MAP:
				if (resolve) return getEntriesMap();
				return basicGetEntriesMap();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.DISPLAY_UNITS_REGISTRY__ENTRIES_MAP:
				setEntriesMap((ETypedElementToUnitsMap)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.DISPLAY_UNITS_REGISTRY__ENTRIES_MAP:
				setEntriesMap((ETypedElementToUnitsMap)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.DISPLAY_UNITS_REGISTRY__ENTRIES_MAP:
				return entriesMap != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCommonEMFUIPackage.DISPLAY_UNITS_REGISTRY___SAVE:
				save();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} // DisplayUnitsRegistryImpl

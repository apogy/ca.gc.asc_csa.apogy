package ca.gc.asc_csa.apogy.common.emf.ui;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.text.DecimalFormat;
import java.util.List;
import java.util.TreeMap;

import javax.measure.converter.UnitConverter;
import javax.measure.unit.Unit;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

import org.eclipse.swt.graphics.RGB;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.Ranges;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Facade for EMF UI.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getUnitConverterMap <em>Unit Converter Map</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getApogyCommonEMFUIFacade()
 * @model
 * @generated
 */
public interface ApogyCommonEMFUIFacade extends EObject
{
	/**
	 * Returns the value of the '<em><b>Unit Converter Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Map that contains all the unitConverters of the SI.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Unit Converter Map</em>' attribute.
	 * @see #setUnitConverterMap(TreeMap)
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getApogyCommonEMFUIFacade_UnitConverterMap()
	 * @model unique="false" dataType="ca.gc.asc_csa.apogy.common.emf.ui.UnitConverterMap"
	 * @generated
	 */
	TreeMap<UnitConverter, String> getUnitConverterMap();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getUnitConverterMap <em>Unit Converter Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Converter Map</em>' attribute.
	 * @see #getUnitConverterMap()
	 * @generated
	 */
	void setUnitConverterMap(TreeMap<UnitConverter, String> value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the color currently associated with the specified
	 * Ranges. Can be null.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.ui.Color" unique="false" rangeUnique="false"
	 * @generated
	 */
	Color getColorForRange(Ranges range);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the color currently associated with the specified
	 * Ranges.
	 * @return RGBA color associated with the specified range, can be null.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.ui.RGB" unique="false" rangeUnique="false"
	 * @generated
	 */
	RGB getRGBColorForRange(Ranges range);

	public static ApogyCommonEMFUIFacade INSTANCE = ApogyCommonEMFUIFacadeImpl.getInstance();
	
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the {@link Unit} to be used for display for a specified {@link ETypedElement}.
	 * @param eTypedElement reference to the ETypedElement.
	 * @return the display {@link Unit}, or the native {@link Unit} if none found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.Unit" unique="false" eTypedElementUnique="false"
	 * @generated
	 */
  Unit<?> getDisplayUnits(ETypedElement eTypedElement);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the {@link Unit} to be used for display for a specified {@link ETypedElement}.
	 * @param eOperation reference to the {@link EOperation}.
	 * @param parameter Parameters to get the units.
	 * @return the display Unit, or the native {@link Unit} if none found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.Unit" unique="false" eOperationUnique="false" parametersUnique="false"
	 * @generated
	 */
	Unit<?> getDisplayUnits(EOperation eOperation, EOperationEParametersUnitsProviderParameters parameters);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Adds a unit to the registry and saves the preferences.
	 * @param eTypedElement reference to the {@link ETypedElement} to provide a unit.
	 * @param provider reference to the {@link UnitsProvider}.
	 * <!-- end-model-doc -->
	 * @model eTypedElementUnique="false" providerUnique="false"
	 * @generated
	 */
	void addUnitsProviderToRegistry(ETypedElement eTypedElement, UnitsProvider provider);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Converts the specified number from the display units to the native units.
	 * This method will also return the right type of number.
	 * @param number reference to the number to convert.
	 * @param nativeUnits {@link Unit} to convert to.
	 * @param displatUnits {@link Unit} to convert from.
	 * @param numberType the {@link EClassifier} of the number to return.
	 * @return the number value converted to the native units.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.Number" unique="false" numberDataType="ca.gc.asc_csa.apogy.common.emf.Number" numberUnique="false" nativeUnitsDataType="ca.gc.asc_csa.apogy.common.emf.Unit" nativeUnitsUnique="false" displayUnitsDataType="ca.gc.asc_csa.apogy.common.emf.Unit" displayUnitsUnique="false" numberTypeUnique="false"
	 * @generated
	 */
	Number convertToNativeUnits(Number number, Unit<?> nativeUnits, Unit<?> displayUnits, EClassifier numberType);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the {@link DecimalFormat} to be used for display for a specified {@link ETypedElement}.
	 * @param eTypedElement reference to the ETypedElement.
	 * @return the display {@link DecimalFormat}, or the native format if none found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormat" unique="false" eTypedElementUnique="false"
	 * @generated
	 */
	DecimalFormat getDisplayFormat(ETypedElement eTypedElement);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the {@link DecimalFormat} to be used for display for a specified {@link EOperation}.
	 * @param eOperation reference to the {@link EOperation}.
	 * @param parameter Parameters to get the format.
	 * @return the display {@link DecimalFormat}, or the native format if none found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormat" unique="false" eOperationUnique="false" parametersUnique="false"
	 * @generated
	 */
	DecimalFormat getDisplayFormat(EOperation eOperation, EOperationEParametersFormatProviderParameters parameters);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Adds a {@link DecimalFormat} to the registry and saves the preferences.
	 * @param eTypedElement reference to the {@link ETypedElement} to provide a {@link DecimalFormat}.
	 * @param provider reference to the {@link FormatProvider}.
	 * <!-- end-model-doc -->
	 * @model eTypedElementUnique="false" providerUnique="false"
	 * @generated
	 */
	void addFormatProviderToRegistry(ETypedElement eTypedElement, FormatProvider provider);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Opens a dialog to delete the {@link Named} object.
	 * @param named Reference to the {@link Named}
	 * <!-- end-model-doc -->
	 * @model namedUnique="false"
	 * @generated
	 */
	void openDeleteNamedDialog(Named named);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Opens a dialog to delete a {@link List} of {@link Named} objects.
	 * @param namedList Reference to the {@link List}
	 * <!-- end-model-doc -->
	 * @model namedListDataType="ca.gc.asc_csa.apogy.common.emf.ui.List" namedListUnique="false"
	 * @generated
	 */
	void openDeleteNamedDialog(List<? extends Named> namedList);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Saves a specified EObject to a part's Persisted State.
	 * @param mPart The part.
	 * @param persistedStateKey The persisted state key under which the eObject will be saved.
	 * @param eObject The EObject to save.
	 * @param resourceSet The ResourceSet containing all references used by the EObject to save. Cannot be null.
	 * <!-- end-model-doc -->
	 * @model mPartDataType="ca.gc.asc_csa.apogy.common.emf.ui.MPart" mPartUnique="false" persistedStateKeyUnique="false" eObjectUnique="false" resourceSetDataType="ca.gc.asc_csa.apogy.common.emf.ui.ResourceSet" resourceSetUnique="false"
	 * @generated
	 */
	void saveToPersistedState(MPart mPart, String persistedStateKey, EObject eObject, ResourceSet resourceSet);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Reads an EObject from a part's Persisted State.
	 * @param mPart The part.
	 * @param persistedStateKey The persisted state key under which the eObject is stored.
	 * @param resourceSet The ResourceSet containing all references used by the EObject. Cannot be null.
	 * @return The EObject read, null if none could be read.
	 * <!-- end-model-doc -->
	 * @model unique="false" mPartDataType="ca.gc.asc_csa.apogy.common.emf.ui.MPart" mPartUnique="false" persistedStateKeyUnique="false" resourceSetDataType="ca.gc.asc_csa.apogy.common.emf.ui.ResourceSet" resourceSetUnique="false"
	 * @generated
	 */
	EObject readFromPersistedState(MPart mPart, String persistedStateKey, ResourceSet resourceSet);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Tries to retrieve the image that would represent an instance of an {@link EClass}.
	 * @param eClass reference to the {@link EClass}.
	 * @return the image if found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.emf.ui.Image" unique="false" eClassUnique="false"
	 * @generated
	 */
	Image getImage(EClass eClass);

} // ApogyCommonEMFUIFacade

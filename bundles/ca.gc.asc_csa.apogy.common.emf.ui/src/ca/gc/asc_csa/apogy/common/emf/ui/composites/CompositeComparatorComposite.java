/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.CompositeComparator;
import ca.gc.asc_csa.apogy.common.emf.EComparator;
import ca.gc.asc_csa.apogy.common.emf.ui.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.NewChildWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;


public class CompositeComparatorComposite<T> extends Composite 
{
	protected CompositeComparator<T> compositeComparator;
		
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;	
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;
	
	public CompositeComparatorComposite(Composite parent, int style) 
	{
		this(parent, style, null);
	}
	
	public CompositeComparatorComposite(Composite parent, int style, CompositeComparatorComposite<T> compositeFilter) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(2, false));	
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData treeGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		tree.setLayoutData(treeGridData);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CompositeFilterContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{			
			@SuppressWarnings("unchecked")
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newSelection((EComparator<T>)((IStructuredSelection) event.getSelection()).getFirstElement());				
			}
		});
		
		// Buttons.
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(composite, SWT.NONE);
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public void handleEvent(Event event) 
			{
				if (event.type == SWT.Selection) 
				{
					CompositeComparator<T> parentCompositeComparator = getCompositeFilter();
					
					// Adding to a selected Composite Comparator.
					if( ((IStructuredSelection) treeViewer.getSelection()).getFirstElement() instanceof CompositeComparator)
					{
						parentCompositeComparator = (CompositeComparator)((IStructuredSelection) treeViewer.getSelection()).getFirstElement();
					}
										
					// If the composite comparator is not in an Editing Domain, adds it to a temporary one.
					EditingDomain domain = AdapterFactoryEditingDomain.getEditingDomainFor(parentCompositeComparator);
					if(domain == null)
					{
						ApogyCommonTransactionFacade.INSTANCE.addInTempTransactionalEditingDomain(parentCompositeComparator);
					}
					
					/**
					 * Creates and opens the wizard to create a new child
					 */
					NewChildWizard newChildWizard = new NewChildWizard(ApogyCommonEMFPackage.Literals.COMPOSITE_COMPARATOR__COMPARATORS, parentCompositeComparator);
										
					// Listener that sets the new child as the selected item
					newChildWizard.getCreatedChild().addChangeListener(new IChangeListener() 
					{						
						@Override
						public void handleChange(ChangeEvent event) 
						{
							EObject selected = ((WritableValue<EObject>)event.getObservable()).getValue();
																			
							if(!treeViewer.isBusy())
							{
								treeViewer.setSelection(new StructuredSelection(selected));
								treeViewer.refresh();
							}							
						}
					});
					WizardDialog dialog = new WizardDialog(getShell(), newChildWizard);

					dialog.open();					
				}
			}
		});
				
		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@SuppressWarnings("unchecked")
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				String toolsToDeleteMessage = "";

				List<EComparator<T>> selectedComparators = getSelectedComparators();
				Iterator<EComparator<T>> filters = selectedComparators.iterator();
				while (filters.hasNext()) 
				{
					EComparator<T> filter = filters.next();
					toolsToDeleteMessage = toolsToDeleteMessage + filter.getName();

					if (filters.hasNext()) 
					{
						toolsToDeleteMessage = toolsToDeleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected comparators", null,
						"Are you sure to delete these comparators: " + toolsToDeleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					for (EComparator<T> comparator : selectedComparators) 
					{												
						try 
						{
							CompositeComparator<T> parent = CompositeComparatorComposite.this.compositeComparator;
							if(comparator.eContainer() instanceof CompositeComparator)
							{
								parent = (CompositeComparator<T>) comparator.eContainer();
							}
							
							EditingDomain domain = AdapterFactoryEditingDomain.getEditingDomainFor(parent);
							if(domain instanceof TransactionalEditingDomain)
							{
								// Removes the tool from the list.
								ApogyCommonTransactionFacade.INSTANCE.basicRemove(parent, ApogyCommonEMFPackage.Literals.COMPOSITE_COMPARATOR__COMPARATORS, comparator);										
							}
							else
							{
								getCompositeFilter().getComparators().remove(comparator);
							}							
						} 
						catch (Exception ex)	
						{
							getCompositeFilter().getComparators().remove(comparator);
							Logger.INSTANCE.log(Activator.ID,
									"Unable to delete the comparator <"+ comparator.getName() + ">",
									EventSeverity.ERROR, ex);
						}
					}
					
					// Forces updates of the viewer.
					treeViewer.setInput(getCompositeFilter());
					
				}
			}
		});
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}		
	
	public CompositeComparator<T> getCompositeFilter() {
		return compositeComparator;
	}

	public void setCompositeComparator(CompositeComparator<T> compositeComparator)
	{
		this.compositeComparator = compositeComparator;
	
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		if(compositeComparator != null)
		{
			m_bindingContext = customInitDataBindings();
			treeViewer.setInput(compositeComparator);
		}				
	}

	@SuppressWarnings("unchecked")
	public List<EComparator<T>> getSelectedComparators()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	protected void newSelection(EComparator<T> filter)
	{
		
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		return bindingContext;
	}
	
	private class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@SuppressWarnings("rawtypes")
		@Override
		public Object[] getElements(Object inputElement) 
		{					
			Object[] elements = null;				
			if(inputElement instanceof CompositeComparator)
			{								
				return ((CompositeComparator) inputElement).getComparators().toArray();
			}			
					
			return elements;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Object[] getChildren(Object parentElement) 
		{
			Object[] children = null;			
			if(parentElement instanceof CompositeComparator)
			{
				CompositeComparator<T> compositeFilter = (CompositeComparator<T>) parentElement;
				children = compositeFilter.getComparators().toArray();
			}
			return children;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof CompositeComparator)
			{
				CompositeComparator<T> compositeFilter = (CompositeComparator<T>) element;
				return !compositeFilter.getComparators().isEmpty();
			}		
			else
			{
				return false;
			}
		}	
	}
}

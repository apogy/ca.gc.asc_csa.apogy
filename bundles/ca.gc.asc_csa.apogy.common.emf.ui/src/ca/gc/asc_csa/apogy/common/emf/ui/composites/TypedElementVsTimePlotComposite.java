/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.composites;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.measure.unit.Unit;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;
import org.jfree.experimental.chart.swt.ChartComposite;
import org.jfree.ui.RectangleInsets;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.Ranges;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.preferences.PreferencesConstants;
import ca.gc.asc_csa.apogy.common.emf.ui.utils.FeaturePathAdapter;

/**
 * A composite that plots a TypedElement value against time.
 * @author pallard
 *
 */
public class TypedElementVsTimePlotComposite extends Composite 
{
	public enum UpdatePolicy
	{
		ON_NEW_DATA,
		LIMITED_REFRESH_RATE
	}
	
	// JFreeChart attributes.
	private TimeSeriesCollection dataSet = null;
	private TimeSeries originalTimeSeries = null;
	private TimeSeries displayedTimeSeries = null;
	private JFreeChart chart;
	// JFreeChart attributes end.
	
	private FeaturePathAdapter featurePathAdapter;
	private Adapter valueAdapter = null;
	private IPropertyChangeListener propertyChangeListener;
	private UpdatePolicy updatePolicy = UpdatePolicy.ON_NEW_DATA;	
	private long maximumHistoryLength = 1000;
	private long refreshRatePeriodMilliseconds = 1000;
	
	private long lastDataTimeStamp = -1;
	
	private EObject eObject;	
	private FeaturePath featurePath;
	
	public TypedElementVsTimePlotComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new FillLayout());
		setBackground(new org.eclipse.swt.graphics.Color(getDisplay(), 0,0,0));
		
		new ChartComposite(this, SWT.NONE, getChart(), true);	
		
		// Register a listener with the preferences.
		ca.gc.asc_csa.apogy.common.emf.ui.Activator.getDefault().getPreferenceStore().addPropertyChangeListener(getPropertyChangeListener());

		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent arg0) 
			{
				// Unregister from preferences.
				ca.gc.asc_csa.apogy.common.emf.ui.Activator.getDefault().getPreferenceStore().removePropertyChangeListener(getPropertyChangeListener());
				
				getFeaturePathAdapter().dispose();
			}
		});
	}

	/**
	 * Returns the maximum number of sample to display at any given time.
	 * @return The maximum number of sample to display.
	 */
	public long getMaximumHistoryLength() 
	{
		return maximumHistoryLength;
	}

	/**
	 * Sets the maximum number of sample to display at any given time.
	 * @param maximumHistoryLength The maximum number of sample to display.
	 */
	public void setMaximumHistoryLength(long maximumHistoryLength) 
	{
		this.maximumHistoryLength = maximumHistoryLength;
	}

	/**
	 * Returns the minimum time separation between samples, in milliseconds.
	 * @return The minimum time separation between samples, in milliseconds.
	 */
	public long getMiniumSamplePeriodMilliseconds() 
	{
		return refreshRatePeriodMilliseconds;
	}
	
	/**
	 * Sets the minimum time separation between samples, in milliseconds.
	 * @param refreshRatePeriodMilliseconds The minimum time separation between samples, in milliseconds.
	 */
	public void setLimitedRefreshRatePeriodMilliseconds(long refreshRatePeriodMilliseconds) 
	{
		this.refreshRatePeriodMilliseconds = refreshRatePeriodMilliseconds;
	}
	
	/**
	 * Return the minimum time separation between samples, in milliseconds.
	 * @return refreshRatePeriodMilliseconds The minimum time separation between samples, in milliseconds.
	 */
	public long getLimitedRefreshRatePeriodMilliseconds() 
	{
		return this.refreshRatePeriodMilliseconds;
	}

	/**
	 * Sets the Update Policy used used.
	 * @return The update policy.
	 */
	public UpdatePolicy getUpdatePolicy() 
	{
		return updatePolicy;
	}

	/**
	 * Sets the Update Policy to be used.
	 * @param updatePolicy The update policy.
	 */
	public void setUpdatePolicy(UpdatePolicy updatePolicy) 
	{
		this.updatePolicy = updatePolicy;
	}

	/**
	 * Sets the value to be displayed in the composite
	 * @param eStructuralFeature The structural feature being displayed
	 * @param instance The instance that the feature refers to.
	 */
	public void setTypedElement(final EStructuralFeature eStructuralFeature, final EObject instance) 
	{
		this.eObject = instance;
		
		if(eStructuralFeature != null)
		{
			featurePath = FeaturePath.fromList(eStructuralFeature);
			getFeaturePathAdapter().setFeaturePath(featurePath);
		}
		else
		{
			featurePath = null;
			getFeaturePathAdapter().dispose();
			
		}
		
		layout();
	}
	
	/**
	 * Sets the value to be displayed in the composite
	 * @param featurePath The FeaturePath referring to a particular structural feature being displayed
	 * @param instance The instance that the feature refers to.
	 */
	public void setTypedElement(final FeaturePath featurePath, final EObject instance) 
	{
		EObject oldEObject = this.eObject;
		EStructuralFeature oldFeature = getEStructuralFeature();
		
		this.eObject = instance;
		this.featurePath = featurePath;
		
		// Clear history if the object or feature has changed.
		if((this.eObject != oldEObject) || (oldFeature != getEStructuralFeature()))
		{
			getFeaturePathAdapter().setFeaturePath(featurePath);
			setInstance(instance);
		}
		
		layout();
	}
	
	/**
	 * Set the instance that refers to the value to display.
	 * @param instance The instance.
	 */
	public void setInstance(EObject instance)
	{
		// Clear history if the object has changed.
		if(this.eObject != instance)
		{
			// Clear history.
			clearHistory();
			
			// Un-register listener.
			if(this.eObject != null)
			{
				// this.eObject.eAdapters().remove(getValueAdapter());
				getFeaturePathAdapter().dispose();
			}
		}
		
		this.eObject = instance;
		
		if(instance != null)
		{								
			//instance.eAdapters().add(getValueAdapter());
			getFeaturePathAdapter().setSource(instance);
			
			// Add the current value of the feature to the data displayed.
			if(getEStructuralFeature() != null)
			{
				Number number = convertValueToNumber(instance.eGet(getEStructuralFeature()));
				if(number != null)
				{
					Date date = new Date();
					processNewData(date, number);
				}
			}
		}
		
		layout();
	}
	
	/**
	 * Return the most specific feature being referred to.
	 * @return The EStructuralFeature.
	 */
	protected EStructuralFeature getEStructuralFeature()
	{
		EStructuralFeature feature = null;
		if(featurePath != null)
		{
			EStructuralFeature[] features = featurePath.getFeaturePath();
			if(features != null && features.length > 0)
			{				
				feature = features[features.length - 1];
			}
		}
		return feature;
	}
	
	/**
	 * Returns the native units of the feature being displayed.
	 * @return The Unit, null if none is found.
	 */
	protected Unit<?> getNativeUnits() 
	{
		return ApogyCommonEMFFacade.INSTANCE.getEngineeringUnits(getEStructuralFeature());
	}

	/**
	 * Returns the units currently used for display.
	 * @return The Unit, null if none is found.
	 */
	protected Unit<?> getDisplayUnits() 
	{
		if(getEStructuralFeature() != null)
		{
			return ApogyCommonEMFUIFacade.INSTANCE.getDisplayUnits(getEStructuralFeature());
		}
		else
		{
			return null;
		}
	}
	
	protected void clearHistory()
	{
		getOriginalTimeSeries().clear();
		getDisplayedTimeSeries().clear();
		lastDataTimeStamp = -1;
	}
	
	protected Number convertValueToNumber(Object objectValue)
	{
		if(objectValue != null)
		{
			if(objectValue instanceof Number)
			{
				return (Number) objectValue;
			}
			else if(objectValue instanceof Boolean)
			{
				Boolean b = (Boolean) objectValue;
				if(b) return new Integer(1);
				else return new Integer(0);
			}
			else if(objectValue instanceof EEnumLiteral)
			{
				EEnumLiteral eEnumLiteral = (EEnumLiteral) objectValue;
				return new Integer(eEnumLiteral.getValue());
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	
	protected Adapter getValueAdapterOld()
	{
		if(valueAdapter == null)
		{
			valueAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					Date date = new Date();
					if(msg.getNotifier() == eObject)
					{
						if(msg.getFeature() == getEStructuralFeature())
						{
							Number number = convertValueToNumber(msg.getNewValue());
							if(number != null)
							{
								processNewData(date, number);
							}
						}
					}
				}
			};
		}
		return valueAdapter;
	}
	
	protected FeaturePathAdapter getFeaturePathAdapter()
	{
		if(featurePathAdapter == null)
		{
			featurePathAdapter = new FeaturePathAdapter()
			{								
				@Override
				public void valueChanged(Object newValue) 
				{					
					Date date = new Date();
					Number number = convertValueToNumber(newValue);
					if(number != null)
					{
						processNewData(date, number);
					}
				}
			};
		}
		
		return featurePathAdapter;
	}
	
	/**
	 * Process incoming data and add it to the plot if required.
	 * @param date The time stamp of the data.
	 * @param value The value.
	 */
	protected void processNewData(Date date, Number value)
	{
		if(getUpdatePolicy() == UpdatePolicy.LIMITED_REFRESH_RATE)
		{
			// If minimum sample period is not disabled.
			if(getMiniumSamplePeriodMilliseconds() != -1)
			{
				// Check if its time to add the data.
				long now = System.currentTimeMillis();
				
				// First data comes in.
				if(lastDataTimeStamp == -1)
				{
					lastDataTimeStamp = now;
					
					// Add the data.
					addNewData(date, value);
				}				
				else if((now - lastDataTimeStamp) >= getMiniumSamplePeriodMilliseconds())
				{
					// Add the data.
					addNewData(date, value);
					
					// Update last time stamp to current one.
					lastDataTimeStamp = now;
				}
			}
			else
			{
				// Just add the data.
				addNewData(date, value);
			}
		}
		else if(getUpdatePolicy() == UpdatePolicy.ON_NEW_DATA)
		{
			// Just add the data.
			addNewData(date, value);
		}
	}
	
	/**
	 * Adds a new value to the plot directly.
	 * @param date The time stamp of the data.
	 * @param value The value.
	 */
	protected void addNewData(Date date, Number value)
	{
		getDisplay().asyncExec(new Runnable() {
			
			@Override
			public void run() 
			{				
				getOriginalTimeSeries().addOrUpdate(new Millisecond(date), value.doubleValue());
				
				if(getNativeUnits() != null)
				{
					double displayedValue = getNativeUnits().getConverterTo(getDisplayUnits()).convert(value.doubleValue());
					getDisplayedTimeSeries().addOrUpdate(new Millisecond(date), displayedValue);
				}
				else
				{
					getDisplayedTimeSeries().addOrUpdate(new Millisecond(date), value.doubleValue());
				}
				
				if(getMaximumHistoryLength() != -1)
				{
					// Check if it is time to sub-sample.
					if(getDisplayedTimeSeries().getItemCount() > getMaximumHistoryLength())
					{
						subSample();
					}
				}
			}
		});		
	}
	
	/**
	 * Fomrat the name of a EStructuralFeature for display
	 * @param eStructuralFeature The EStructuralFeature.
	 * @return The format name of the feature.
	 */
	protected String getLabelFromEStructuralFeature(EStructuralFeature eStructuralFeature)
	{
		String raw = getEStructuralFeature().getName();
		
		String formated = raw.replaceAll("(.)([A-Z])", "$1 $2");				
		formated = formated.substring(0,1).toUpperCase() + formated.substring(1);
		
		return formated;
	}
	
	/**
	 * Return the name to use to identify the data series being displayed.
	 * @return The name, never null.
	 */
	protected String getFeatureName()
	{
		if(getEStructuralFeature() != null)
		{
			return getLabelFromEStructuralFeature(getEStructuralFeature());
		}
		else
		{
			return "?";
		}
	}
	
	/**
	 * Updates the plot to properly the new displayed units.
	 */
	protected void updatePlotUnits()
	{
		if(getDisplayUnits() != null && getNativeUnits() != null)
		{
			String chartYAxisLabel = getFeatureName() + " (" + getDisplayUnits().toString() + ")";					
			XYPlot plot = (XYPlot) getChart().getPlot();
			plot.getRangeAxis().setLabel(chartYAxisLabel);
									
			double conversionFactor = getNativeUnits().getConverterTo(getDisplayUnits()).convert(1.0);			
			getDisplayedTimeSeries().clear();
			
			for(int i = 0; i < getOriginalTimeSeries().getItemCount(); i++)
			{
				TimeSeriesDataItem item = getOriginalTimeSeries().getDataItem(i);
				double altitude = item.getValue().doubleValue() * conversionFactor;				
				getDisplayedTimeSeries().add(item.getPeriod(), altitude);				
			}
		}
	}
	
	protected synchronized void subSample()
	{
		int initialSize = getDisplayedTimeSeries().getItems().size();
			
		if(initialSize > getMaximumHistoryLength())
		{
			// Sub sample displayed series by 2
			List<TimeSeriesDataItem> toKeep = new ArrayList<TimeSeriesDataItem>();
			for(int i = 0; i <initialSize; i+=2 )
			{
				TimeSeriesDataItem item = getDisplayedTimeSeries().getDataItem(i);
				toKeep.add(item);
			}
			getDisplayedTimeSeries().clear();
			for(TimeSeriesDataItem item : toKeep)
			{
				getDisplayedTimeSeries().add(item);
			}									
			
			// Sub sample original series by 2
			List<TimeSeriesDataItem> toKeepOriginal = new ArrayList<TimeSeriesDataItem>();
			for(int i = 0; i <initialSize; i+=2 )
			{
				TimeSeriesDataItem item = getOriginalTimeSeries().getDataItem(i);
				toKeepOriginal.add(item);
			}
			getOriginalTimeSeries().clear();
			for(TimeSeriesDataItem item : toKeepOriginal)
			{
				getOriginalTimeSeries().add(item);
			}
		}
	}
	
	/** Preference listener */
	private IPropertyChangeListener getPropertyChangeListener() 
	{
		if (propertyChangeListener == null) 
		{
			propertyChangeListener = new IPropertyChangeListener() 
			{
				@Override
				public void propertyChange(PropertyChangeEvent event) 
				{
					if(event != null && event.getProperty() != null)
					{
						/**
						 * Unit of format preference event, update the value text
						 */
						if (event.getProperty().equals(PreferencesConstants.TYPED_ELEMENTS_UNITS_ID)
								|| PreferencesConstants.isFormatPreference(event.getProperty())) 
						{							
							updatePlotUnits();		
						}
						/** Range preference event, update the background color */
						else if (event.getProperty().equals(Ranges.UNKNOWN.getName())
								|| event.getProperty().equals(Ranges.NOMINAL.getName())
								|| event.getProperty().equals(Ranges.WARNING.getName())
								|| event.getProperty().equals(Ranges.ALARM.getName())
								|| event.getProperty().equals(Ranges.OUT_OF_RANGE.getName())) 
						{
							// TODO
						}
					}
				}
			};
		}
		return propertyChangeListener;
	}
	
	
	
	// JFreeChart specifics
	
	private JFreeChart getChart()
	{						
		if(chart == null)
		{
		   	String chartTitle = "Altitude";   	
	    	String chartXAxisLabel = "Time";    	
	    	String chartYAxisLabel = "Altitude (m)";
	    	
	    	chart = ChartFactory.createTimeSeriesChart(
					chartTitle, // title
					chartXAxisLabel, // x-axis label
					chartYAxisLabel, // y-axis label
					getRangeDataSet() , // data set
					true, // Generate legend
					true, // generate tooltips
					false // do not generate URLs.
					);
	
	        chart.setBackgroundPaint(Color.black);	        
	        chart.getTitle().setPaint(Color.green);	        
	
	        // Creates the Plot.
	        XYPlot plot = (XYPlot) chart.getPlot();
	        	         	        	        		        
	        plot.setOutlinePaint(Color.green);
	        plot.setBackgroundPaint(Color.black);
	        plot.setDomainGridlinePaint(Color.green);
	        plot.setRangeGridlinePaint(Color.green);	   	        
	        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
	        		        
	        plot.setDomainCrosshairVisible(true);
	        plot.setRangeCrosshairVisible(true);          
	        
	        plot.setDomainMinorGridlinesVisible(false);
	        plot.setRangeMinorGridlinesVisible(false);
	        plot.setDomainGridlinesVisible(true);
	        plot.setRangeGridlinesVisible(true);            
	        
	        XYItemRenderer xyItemRenderer = plot.getRenderer();       				       
	        if (xyItemRenderer instanceof XYLineAndShapeRenderer) 
			{
	        	 // Just draw lines, no shapes.
				XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) xyItemRenderer;
				renderer.setBaseShapesVisible(false);
				renderer.setBaseShapesFilled(false);
				renderer.setSeriesStroke(0, new BasicStroke(1.0f));
				renderer.setSeriesPaint(0, Color.YELLOW);
			}
	        
	        plot.getRangeAxis().setAutoRange(true);
	        plot.getRangeAxis().setLabelPaint(Color.green);
	        plot.getRangeAxis().setTickLabelPaint(Color.green);
	        plot.getRangeAxis().setTickMarkPaint(Color.green);
	        plot.getRangeAxis().setAxisLinePaint(Color.black);
	        	        
	        plot.getDomainAxis().setAutoRange(true);    
	        plot.getDomainAxis().setLabelPaint(Color.green);
	        plot.getDomainAxis().setTickLabelPaint(Color.green);
	        plot.getDomainAxis().setTickMarkPaint(Color.green);
	        plot.getDomainAxis().setAxisLinePaint(Color.black);
	        
		}
		return chart;
	}
	
	private TimeSeriesCollection getRangeDataSet()
	{
		if(dataSet == null)
		{
			dataSet = new TimeSeriesCollection();	
			dataSet.addSeries(getDisplayedTimeSeries());			
		}
		
		return dataSet;
	}
	
	
	private TimeSeries getDisplayedTimeSeries() 
	{
		if(displayedTimeSeries == null)
		{
			displayedTimeSeries = new TimeSeries("Altitude");		
		}
		
		return displayedTimeSeries;				
	}
	
	private TimeSeries getOriginalTimeSeries() 
	{
		if(originalTimeSeries == null)
		{
			originalTimeSeries = new TimeSeries("Altitude");		
		}
		
		return originalTimeSeries;				
	}
	

}

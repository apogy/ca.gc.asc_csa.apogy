/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.parts;

import javax.annotation.PostConstruct;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.wb.swt.SWTResourceManager;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.CompositeFilter;
import ca.gc.asc_csa.apogy.common.emf.IFilter;
import ca.gc.asc_csa.apogy.common.emf.Timed;
import ca.gc.asc_csa.apogy.common.emf.TimedAfterFilter;
import ca.gc.asc_csa.apogy.common.emf.TimedBeforeFilter;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.CompositeFilterComposite;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.FilterDetailsComposite;

public class CompositeFilterTestPart 
{
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());

	private CompositeFilterComposite<Timed> compositeFilterComposite;
	private FilterDetailsComposite<Timed> filterDetailsComposite;
	
	@PostConstruct
	protected void createContentComposite(Composite parent) 
	{				
	parent.setLayout(new FillLayout());
		
		Composite top = new Composite(parent, SWT.BORDER);
		GridLayout gl_parent = new GridLayout(2, false);
		gl_parent.verticalSpacing = 10;
		top.setLayout(gl_parent);
		
		// Tools section on the left.
		Section sctnTools = formToolkit.createSection(top, Section.TITLE_BAR);
		sctnTools.setForeground(SWTResourceManager.getColor(SWT.COLOR_TITLE_BACKGROUND));
		sctnTools.setFont(SWTResourceManager.getFont("Ubuntu", 18, SWT.BOLD));
		sctnTools.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(sctnTools);
		sctnTools.setText("Filters");
				
		compositeFilterComposite = new CompositeFilterComposite<Timed>(sctnTools, SWT.NONE)
		{
			@Override
			protected void newSelection(IFilter<Timed> filter) {
				filterDetailsComposite.setFilter(filter);
			}
		};
		compositeFilterComposite.setCompositeFilter(createFilter());		
		
//		GridLayout gridLayout = (GridLayout) compositeFilterComposite.getLayout();
//		gridLayout.marginWidth = 0;
		formToolkit.adapt(compositeFilterComposite);
		formToolkit.paintBordersFor(compositeFilterComposite);
		sctnTools.setClient(compositeFilterComposite);
				
		// Tools section on the left.
		Section sctnToolsDetails = formToolkit.createSection(top, Section.TITLE_BAR);
		sctnToolsDetails.setForeground(SWTResourceManager.getColor(SWT.COLOR_TITLE_BACKGROUND));
		sctnToolsDetails.setFont(SWTResourceManager.getFont("Ubuntu", 18, SWT.BOLD));
		sctnToolsDetails.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(sctnToolsDetails);
		sctnToolsDetails.setText("Details");
				
		filterDetailsComposite = new FilterDetailsComposite<Timed>(sctnToolsDetails, SWT.NONE);
//		GridLayout gridLayout1 = (GridLayout) filterDetailsComposite.getLayout();
//		gridLayout1.marginWidth = 0;
		formToolkit.adapt(filterDetailsComposite);
		formToolkit.paintBordersFor(filterDetailsComposite);
		sctnToolsDetails.setClient(filterDetailsComposite);
	}
	
	private CompositeFilter<Timed> createFilter()
	{
		CompositeFilter<Timed> filter = ApogyCommonEMFFactory.eINSTANCE.createCompositeFilter();
		
		TimedBeforeFilter<Timed> beforeFilter = ApogyCommonEMFFactory.eINSTANCE.createTimedBeforeFilter();
		TimedAfterFilter<Timed> afterFilter = ApogyCommonEMFFactory.eINSTANCE.createTimedAfterFilter();
		
		filter.getFilters().add(beforeFilter);
		filter.getFilters().add(afterFilter);
		
		return filter;
	}

}

package ca.gc.asc_csa.apogy.common.emf.ui.preferences;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import ca.gc.asc_csa.apogy.common.emf.ui.Activator;

public class NativeFormatsPreferencePage extends PreferencePage implements IWorkbenchPreferencePage
{	
	private NativeDecimalFormatComposite nativeDecimalFormatComposite = null;
	
	/**
	 * Create the preference page.
	 */
	public NativeFormatsPreferencePage() 
	{
	}

	/**
	 * Create contents of the preference page.
	 * @param parent
	 */
	@Override
	public Control createContents(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(1, false));
			
		Group formatGroup = new Group(container,SWT.None);
		formatGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		formatGroup.setLayout(new GridLayout(2, false));
		formatGroup.setText("Default Decimal Formats");
		
		nativeDecimalFormatComposite = new NativeDecimalFormatComposite(formatGroup, SWT.NONE);
		nativeDecimalFormatComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		return container;
	}
	
	/**
	 * Initialize the preference page.
	 */
	public void init(IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}

	@Override
	public boolean performOk() 
	{	
		nativeDecimalFormatComposite.savePreferences();
		return super.performOk();
	}
	
	@Override
	protected void performApply() 
	{
		nativeDecimalFormatComposite.savePreferences();
		super.performApply();
	}
	
	@Override
	protected void performDefaults() 
	{		
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		
		// Reset to default the preferences.					
		store.setToDefault(PreferencesConstants.NATIVE_FORMAT_BYTE_ID);
		nativeDecimalFormatComposite.updateTexts();

		super.performDefaults();
	}
}

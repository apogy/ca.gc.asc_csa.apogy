package ca.gc.asc_csa.apogy.common.emf.ui.composites;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.CurrentTimeSource;
import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class CurrentTimeSourceComposite extends AbstractTimeSourceComposite 
{
	protected CurrentTimeSource currentTimeSource;
	protected DataBindingContext bindingContext;	
	
	protected Spinner updatePeriodSpinner;	
	protected Button resumeButton;
	protected Button pauseButton;
	private Composite composite;
	
	public CurrentTimeSourceComposite(Composite parent, int style) 
	{
		this(parent, style, null);				
	}
	
	public CurrentTimeSourceComposite(Composite parent, int style, CurrentTimeSource currentTimeSource) 
	{
		super(parent, style);
		addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if(bindingContext != null){
					bindingContext.dispose();
				}
			}
		});
		
		Composite top = new Composite(this, SWT.NONE);
		top.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		top.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		top.setLayout(new GridLayout(3, true));		
				
		Label updatePeriodLabel = new Label(top, SWT.None);
		updatePeriodLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		updatePeriodLabel.setText("Update period (s):");
		updatePeriodSpinner = new Spinner(top, SWT.BORDER);
		updatePeriodSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		updatePeriodSpinner.setDigits(3);
		updatePeriodSpinner.setMinimum(1);
		updatePeriodSpinner.setMaximum(60000);
		updatePeriodSpinner.setIncrement(1);
		updatePeriodSpinner.setSelection(1000);
		new Label(top, SWT.NONE);
		
		composite = new Composite(top, SWT.NONE);
		composite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		composite.setLayout(new GridLayout(2, true));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		resumeButton = new Button(composite, SWT.PUSH);
		resumeButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		resumeButton.setText("Run");
		
		pauseButton = new Button(composite, SWT.PUSH);
		pauseButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		pauseButton.setText("Pause");
		pauseButton.addSelectionListener(new SelectionListener() 
		{		
			@Override
			public void widgetSelected(SelectionEvent e) 
			{				
				getCurrentTimeSource().pause();		
				pauseButton.setEnabled(false);
				resumeButton.setEnabled(true);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {				
			}
		});
		resumeButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				getCurrentTimeSource().resume();
				pauseButton.setEnabled(true);
				resumeButton.setEnabled(false);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {	
			}
		});
		new Label(top, SWT.NONE);
		
		updatePeriodSpinner.addSelectionListener(new SelectionListener() 
		{		
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				int selection = updatePeriodSpinner.getSelection();
				int digits = updatePeriodSpinner.getDigits();
				int period = (int) Math.round((selection / Math.pow(10, digits)) * 1000);
				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(getCurrentTimeSource(), ApogyCommonEMFPackage.Literals.CURRENT_TIME_SOURCE__UPDATE_PERIOD, period);			
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {	
			}
		});
		
		setCurrentTimeSource(currentTimeSource);				
	}
	
	@Override
	public TimeSource getTimeSource() 
	{		
		return getCurrentTimeSource() ;
	}
	
	@Override
	public void activate(boolean active) 
	{
		if(active)
		{
			updatePeriodSpinner.setEnabled(true);
			pauseButton.setEnabled(true);
			resumeButton.setEnabled(true);
		}
		else
		{
			updatePeriodSpinner.setEnabled(false);
			pauseButton.setEnabled(false);
			resumeButton.setEnabled(false);
		}
	}
	
	public CurrentTimeSource getCurrentTimeSource() 
	{
		return currentTimeSource;
	}

	public void setCurrentTimeSource(CurrentTimeSource currentTimeSource) 
	{
		setCurrentTimeSource(currentTimeSource, true);
	}
	
	public void setCurrentTimeSource(CurrentTimeSource currentTimeSource, boolean update) 
	{
		this.currentTimeSource = currentTimeSource;
		
		if(update)
		{
			if(bindingContext != null)
			{
				bindingContext.dispose();
				bindingContext = null;
			}
			
			if(currentTimeSource != null)
			{
				bindingContext = initDataBindings();
				
				if(updatePeriodSpinner != null)
				{
					int periodDigits = updatePeriodSpinner.getDigits();
					updatePeriodSpinner.setSelection((int) Math.round(Math.pow(10, periodDigits) * currentTimeSource.getUpdatePeriod() * 0.001));
				}
				
				if(currentTimeSource.isPaused()) 
				{
					pauseButton.setEnabled(false);
					resumeButton.setEnabled(true);
				}
				else
				{
					pauseButton.setEnabled(true);
					resumeButton.setEnabled(false);
				}
			}
			else
			{
				pauseButton.setEnabled(false);
				resumeButton.setEnabled(false);				
			}
		}
		
		super.setTimeSource(currentTimeSource);
	}

	protected DataBindingContext initDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
						
		return bindingContext;
	}	
}

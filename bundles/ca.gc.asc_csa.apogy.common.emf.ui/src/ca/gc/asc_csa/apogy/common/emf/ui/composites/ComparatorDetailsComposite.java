/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.composites;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecp.ui.view.swt.ECPSWTViewRenderer;
import org.eclipse.emf.ecp.view.spi.model.VView;
import org.eclipse.emf.ecp.view.spi.provider.ViewProviderHelper;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import ca.gc.asc_csa.apogy.common.emf.EComparator;
import ca.gc.asc_csa.apogy.common.emf.ui.Activator;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;

public class ComparatorDetailsComposite<T> extends Composite 
{
	private EComparator<T> comparator;
	
	private Composite compositeDetails;	
	
	
	public ComparatorDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
		ScrolledComposite scrolledComposite = new ScrolledComposite(this, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		scrolledComposite.setLayout(new GridLayout());
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		
		scrolledComposite.addListener( SWT.Resize, event -> 
		{
			int width = scrolledComposite.getClientArea().width;
			scrolledComposite.setMinSize( parent.computeSize( width, SWT.DEFAULT ) );
		} );
		
		compositeDetails = new Composite(scrolledComposite, SWT.BORDER);
		compositeDetails.setLayout(new GridLayout());
		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_compositeDetails.minimumHeight = 100;
		gd_compositeDetails.heightHint = 100;
		gd_compositeDetails.minimumWidth = 250;
		gd_compositeDetails.widthHint = 250;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.layout();
		
		scrolledComposite.setContent(compositeDetails);
	}

	public EComparator<T> getComparator() {
		return comparator;
	}


	public void setComparator(EComparator<T> filter) 
	{
		this.comparator = filter;
		
		if(filter != null)
		{
			try
			{
				createEMFForms(compositeDetails, filter);
			}
			catch(Throwable t)
			{
				t.printStackTrace();
			}
		}
	}
	
	private void createEMFForms(Composite parent, EObject eObject) 
	{		
		for (Control control : parent.getChildren()) {
			control.dispose();
		}
		try 
		{
			VView viewModel = ViewProviderHelper.getView(eObject, null);
			ECPSWTViewRenderer.INSTANCE.render(parent, eObject, viewModel);
		} catch (Exception e) {
			String message = this.getClass().getSimpleName() + ".setCompositeContents(): "
					+ "Error while opening EMF Forms";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
		}
		parent.layout();
	}
	

}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.parts;

import java.util.HashMap;
import java.util.List;

import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.composites.EObjectDocumentationComposite;

public abstract class AbstractDocumentationPart extends AbstractEObjectSelectionPart{

	
	@Override
	protected void setCompositeContents(EObject eObject) {
		((EObjectDocumentationComposite)getActualComposite()).setEObject(eObject);
	}

	@Override
	protected void createContentComposite(Composite parent, int style) {
		new EObjectDocumentationComposite(parent, SWT.None){
			@Override
			protected String getCustomText(EObject eObject) {
				return eObject == null ? "" : getCustomDoc(eObject);
			}
		};
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() {
		HashMap<String, ISelectionListener> map = new HashMap<>();

		for(String id: getSelectionListenerPartIds()){
			map.put(id, DEFAULT_LISTENER);
		}
		
		return map;
	}
	
	abstract protected List<String> getSelectionListenerPartIds();
	
	/**
	 * Returns the custom string to be displayed for a specified eObject. 
	 * @param eObject The specified eObject, can be null.  
	 * @return The documentation string.
	 */
	protected String getCustomDoc(EObject eObject){
		return "";	
	}

}

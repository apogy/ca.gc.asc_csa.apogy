package ca.gc.asc_csa.apogy.common.emf.ui.wizards;
/*
 * Copyright (c) 2016, 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.ApogyCommonUiFacade;

public class ApogyEObjectWizard extends Wizard {

	private ChooseEClassImplementationWizardPage chooseEClassImplementationWizardPage;
	private EReference eReference;
	private EObject parent;

	private EClassSettings settings;
	private EClass eClass;
	private List<EClass> displayedEClasses;

	private WizardPagesProvider pagesProvider;
	private List<WizardPage> providedWizardPages;

	private boolean openWizard = true;

	public ApogyEObjectWizard(EReference eReference, EObject parent, EClassSettings settings, EClass eClass) {
		super();
		setWindowTitle("New Child");
		setNeedsProgressMonitor(true);
		setForcePreviousAndNextButtons(true);
		ImageDescriptor image = AbstractUIPlugin.imageDescriptorFromPlugin(Activator.ID,
				"icons/wizban/emf_new_child.png");
		setDefaultPageImageDescriptor(image);

		// Verification of parameters that can't be null.
		if (eReference == null || parent == null) {
			Logger.INSTANCE.log(Activator.ID, this, "No reference or parent set in wizard", EventSeverity.ERROR);
			openWizard = false;
			return;
		}

		this.eReference = eReference;
		this.parent = parent;
		this.settings = settings;

		// Get all the settable classes.
		this.displayedEClasses = new ArrayList<EClass>();
		if (eClass == null || !eReference.getEReferenceType().isSuperTypeOf(eClass)) {
			this.eClass = eReference.getEReferenceType();
			this.displayedEClasses = this.verifyEClass(this.eClass);
		} 
		else 
		{
			this.eClass = eClass;
			
			
			this.displayedEClasses.addAll(ApogyCommonEMFFacade.INSTANCE.sortAlphabetically(ApogyCommonEMFFacade.INSTANCE.getAllSubEClasses(this.eClass)));
		}

		// Decide of what pages to display depending on the number of settable
		// classes.
		if (this.displayedEClasses.isEmpty()) {
			Logger.INSTANCE.log(Activator.ID, this, "No concrete class settable in " + this.eReference.getName(),
					EventSeverity.ERROR);
			openWizard = false;
		} else if (this.displayedEClasses.size() == 1) {
			this.pagesProvider = WizardPagesProviderRegistry.INSTANCE.getProvider(this.eClass);
			this.providedWizardPages = pagesProvider.getPages(this.displayedEClasses.get(0), settings);

			if (this.providedWizardPages.isEmpty()) {
				openWizard = false;
			}
		}
	}

	/**
	 * Adds the page to the wizard.
	 */
	public void addPages() {
		if (providedWizardPages != null) {
			for (WizardPage page : providedWizardPages) {
				addPage(page);
			}
		} else {
			addPage(getChooseEClassImplementationWizardPage());
		}

		if (getPages().length > 0) {
			ApogyCommonUiFacade.INSTANCE.adjustWizardPage((WizardPage) getPages()[0], 0.5);
		}
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		if (page == getChooseEClassImplementationWizardPage()) {
			if (getChooseEClassImplementationWizardPage().getSelectedEClass() != null) {
				if (providedWizardPages != null) {
					return providedWizardPages.isEmpty() ? null : providedWizardPages.get(0);
				}
				return null;
			} else {
				return null;
			}
		}
		return pagesProvider.getNextPage(page);
	}

	/**
	 * Returns the {@link SubClassesWizardPage}. If null is returned, the page
	 * is not added to the wizard.
	 * 
	 * @return Reference to the page.
	 */
	protected ChooseEClassImplementationWizardPage getChooseEClassImplementationWizardPage() {
		if (chooseEClassImplementationWizardPage == null) {
			chooseEClassImplementationWizardPage = new ChooseEClassImplementationWizardPage(this.displayedEClasses) {
				@Override
				protected void newSelection(TreeSelection selection) {
					if (selection.getFirstElement() instanceof EClass) {
						eClass = (EClass) selection.getFirstElement();
						pagesProvider = WizardPagesProviderRegistry.INSTANCE.getProvider(eClass);
						providedWizardPages = pagesProvider.getPages(eClass, settings);

						for (WizardPage page : providedWizardPages) {
							addPage(page);
						}
					}
				}
			};
		}
		return chooseEClassImplementationWizardPage;
	}

	/**
	 * Returns false if the wizard has not found any pages for the reference and
	 * the object can be directly created.
	 */
	public boolean openWizard() {
		return openWizard;
	}

	public EReference getEReference() {
		return eReference;
	}

	public EObject getCreatedEObject() {
		return pagesProvider.getEObject();
	}

	@Override
	public boolean performFinish() 
	{
		if (pagesProvider != null) 
		{				
			EObject eObject = pagesProvider.getEObject();

			eObject.eResource().getResourceSet().getResources().remove(eObject.eResource());
			TransactionUtil.disconnectFromEditingDomain(eObject.eResource());
						
			Command command;
			if (eReference.isMany()) 
			{
				command = new AddCommand(TransactionUtil.getEditingDomain(parent), parent, eReference, eObject);
			} 
			else 
			{
				command = new SetCommand(TransactionUtil.getEditingDomain(parent), parent, eReference, eObject);
			}

			CompoundCommand compoundCommand = new CompoundCommand();
			compoundCommand.append(command);
			
			CompoundCommand usersCommand = pagesProvider.getPerformFinishCommands(eObject, settings, TransactionUtil.getEditingDomain(parent));
			
			if(usersCommand != null)
			{
				if(usersCommand.canExecute())
				{
					compoundCommand.append(usersCommand);
				}
				else
				{
					Logger.INSTANCE.log(Activator.ID, this, "The compound command returned by <" + pagesProvider.getClass().getName() + "> is not executable!", EventSeverity.ERROR);			
				}
			}
			
			// Gets the TransactionalEditingDomain associated with the parent.
			TransactionalEditingDomain transactionalEditingDomain = TransactionUtil.getEditingDomain(parent);
			boolean tempED = false;
			if(transactionalEditingDomain == null)
			{
				transactionalEditingDomain = ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain();
				ApogyCommonTransactionFacade.INSTANCE.addInTempTransactionalEditingDomain(parent);
				tempED = true;
			}					
			
			transactionalEditingDomain.getCommandStack().execute(compoundCommand);
			
			if(tempED){
				ApogyCommonTransactionFacade.INSTANCE.removeFromEditingDomain(parent);
			}
		}

		return true;
	}

	@Override
	public boolean canFinish() {
		for (IWizardPage page : getPages()) {
			if (page instanceof ChooseEClassImplementationWizardPage || this.providedWizardPages.contains(page)) {
				if (!page.isPageComplete()) {
					return false;
				}
			}
		}
		return true;
	}

	private List<EClass> verifyEClass(EClass eClass) {
		if (!getEReference().getEReferenceType().isSuperTypeOf(eClass)) {
			Logger.INSTANCE.log(Activator.ID, this, eClass + " is not settable in reference", EventSeverity.ERROR);
			eClass = getEReference().getEReferenceType();
		}

		List<EClass> subClasses = ApogyCommonEMFFacade.INSTANCE.getAllSubEClasses(eClass);

		return subClasses;
	}
}
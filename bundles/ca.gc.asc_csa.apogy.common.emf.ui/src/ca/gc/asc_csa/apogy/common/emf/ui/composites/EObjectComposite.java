package ca.gc.asc_csa.apogy.common.emf.ui.composites;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.window.ToolTip;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.EObjectReference;
import ca.gc.asc_csa.apogy.common.ui.ApogyCommonUiFacade;

public class EObjectComposite extends ScrolledComposite {
	private DataBindingContext m_bindingContext;

	private TreeViewer instanceViewer;
	ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
			ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	private ISelectionChangedListener selectionChangedListener;

	private EObject eObject;

	/**
	 * Creates the parentComposite.
	 * 
	 * @param parent
	 * @param style
	 */
	public EObjectComposite(Composite parent, int style) {
		super(parent, style);
		addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				instanceViewer.removeSelectionChangedListener(getSelectionChangedListener());
				if (m_bindingContext != null) {
					m_bindingContext.dispose();
					m_bindingContext = null;
				}
			}
		});
		setLayout(new GridLayout(1, true));
		setExpandHorizontal(true);
		setExpandVertical(true);

		Composite compositeEObject = new Composite(this, SWT.NONE);
		GridLayout gl_compositeEObject = new GridLayout(2, false);
		gl_compositeEObject.marginWidth = 0;
		gl_compositeEObject.marginHeight = 0;
		gl_compositeEObject.verticalSpacing = 0;
		gl_compositeEObject.horizontalSpacing = 0;
		compositeEObject.setLayout(gl_compositeEObject);
		
		instanceViewer = new TreeViewer(compositeEObject,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.SINGLE);

		ColumnViewerToolTipSupport.enableFor(instanceViewer, ToolTip.NO_RECREATE);
		Tree treeInstance = instanceViewer.getTree();
		treeInstance.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		treeInstance.setLinesVisible(true);

		instanceViewer.setContentProvider(getContentProvider());
		instanceViewer.setLabelProvider(getLabelProvider());

		instanceViewer.addSelectionChangedListener(getSelectionChangedListener());
		ApogyCommonUiFacade.INSTANCE.addExpandOnDoubleClick(instanceViewer);
		
		this.setContent(compositeEObject);
		this.setMinSize(compositeEObject.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	/**
	 * Binds the {@link EObject} with the parentComposite.
	 * 
	 * @param eObject
	 *            Reference to the list of eObject.
	 */
	public void setEObject(EObject eObject) {
		setEObject(eObject, true);
	}

	/**
	 * Sets the {@link EObject}.
	 * 
	 * @param eObject
	 *            Reference to the EObject.
	 * @param update
	 *            If true then data bindings are created.
	 */
	private void setEObject(EObject eObject, boolean update) {
		this.eObject = eObject;
		if (update) {
			if (m_bindingContext != null) {
				m_bindingContext.dispose();
				m_bindingContext = null;
			}

			if (eObject != null) {
				m_bindingContext = initDataBindings();
			}
			else
			{
				instanceViewer.setInput(null);
			}
		}
	}

	protected DataBindingContext initDataBindings() {
		return initDataBindingsCustom();
	}

	/**
	 * Creates and returns the data bindings.
	 * 
	 * @return Reference to the data bindings.
	 */
	private DataBindingContext initDataBindingsCustom() {
		DataBindingContext bindingContext = new DataBindingContext();
		EObjectReference eObjectReference = ApogyCommonEMFFactory.eINSTANCE.createEObjectReference();
		eObjectReference.setEObject(eObject);
		instanceViewer.setInput(eObjectReference);
		return bindingContext;
	}

	/**
	 * Listener used to listen {{@link #instanceViewer} selection changes.
	 * 
	 * @return Reference to the listener (Lazy Loaded).
	 */
	private ISelectionChangedListener getSelectionChangedListener() {
		if (selectionChangedListener == null) {
			selectionChangedListener = new ISelectionChangedListener() {
				@Override
				public void selectionChanged(SelectionChangedEvent event) {
					newSelection(event.getSelection());
				}
			};
		}
		return selectionChangedListener;
	}

	/**
	 * This method is called when a new selection is made in the parentComposite.
	 * 
	 * @param selection
	 *            Reference to the selection.
	 */
	protected void newSelection(ISelection selection) {
	}

	/**
	 * Returns the selected {@link EObject}.
	 * 
	 * @return Reference to the {@link EObject}.
	 */
	public EObject getSelectedEObject() {
		IStructuredSelection selection = (IStructuredSelection) instanceViewer.getSelection();
		return (EObject) selection.getFirstElement();
	}

	/**
	 * Sets the selected {@link EObject}.
	 * 
	 * @return Reference to the {@link EObject}.
	 */
	public void setSelectedEObject(EObject eObject) {
		instanceViewer.refresh();
		instanceViewer.setSelection(new StructuredSelection(eObject));
	}

	protected AdapterFactoryContentProvider getContentProvider() {
		return new AdapterFactoryContentProvider(adapterFactory);
	}

	protected AdapterFactoryLabelProvider getLabelProvider() {
		return new AdapterFactoryLabelProvider(adapterFactory);
	}
	
	public void refresh(){
		this.instanceViewer.refresh();
	}
}
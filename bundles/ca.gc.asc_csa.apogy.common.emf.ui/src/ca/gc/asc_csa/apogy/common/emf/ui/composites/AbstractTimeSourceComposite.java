package ca.gc.asc_csa.apogy.common.emf.ui.composites;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.text.SimpleDateFormat;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;

import ca.gc.asc_csa.apogy.common.databinding.converters.DateToStringConverter;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.TimeSource;

public abstract class AbstractTimeSourceComposite extends Composite 
{	
	public static final String DATE_FORMAT_STRING = "yyyy.MM.dd HH:mm:ss.SSS z";
	
	protected Label timeSourceTimeValueLabel;
	
	private DataBindingContext bindingContext;
	
	public AbstractTimeSourceComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(1,false));
		addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if(bindingContext != null){
					bindingContext.dispose();
				}
			}
		});
		
		Composite top = new Composite(this, SWT.NONE);
		top.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		top.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		top.setLayout(new GridLayout(2, false));
		
		Label startTimeLabel = new Label(top, SWT.NONE);
		startTimeLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		startTimeLabel.setText("Source Time : ");
		
		timeSourceTimeValueLabel = new Label(top, SWT.NONE);
		timeSourceTimeValueLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		timeSourceTimeValueLabel.setText("?");
		GridData startTimeValueLabelGridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
		startTimeValueLabelGridData.widthHint = 250;
		startTimeValueLabelGridData.minimumWidth = 250;
		timeSourceTimeValueLabel.setLayoutData(startTimeValueLabelGridData);		
	}

	public abstract void activate(boolean active);
	
	public abstract TimeSource getTimeSource();
	
	public void setTimeSource(TimeSource timeSource)
	{
		if(bindingContext != null)
		{
			bindingContext.dispose();
			bindingContext = null;
		}
		
		if(timeSource != null)
		{
			bindingContext = initDataBindings();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private DataBindingContext initDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
						
		// Bind current time if applicable
		if(timeSourceTimeValueLabel != null)
		{
			IObservableValue timeSourceTimeLabelValue = PojoProperties.value("text").observe(timeSourceTimeValueLabel);		
			IObservableValue timeSourceTimeObserveValue = EMFProperties.value(FeaturePath.fromList(ApogyCommonEMFPackage.Literals.TIMED__TIME)).observe(getTimeSource());			
			UpdateValueStrategy timeSourceTimeValueStrategy = new UpdateValueStrategy();
			timeSourceTimeValueStrategy.setConverter(new DateToStringConverter(new SimpleDateFormat(DATE_FORMAT_STRING)));
			bindingContext.bindValue(timeSourceTimeLabelValue, timeSourceTimeObserveValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), timeSourceTimeValueStrategy);
		}				
		return bindingContext;
	}	
}

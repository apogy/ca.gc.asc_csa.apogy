package ca.gc.asc_csa.apogy.common.emf.ui.composites;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource;

public class SelectionBasedTimeSourceComposite extends AbstractTimeSourceComposite 
{
	protected SelectionBasedTimeSource selectionBasedTimeSource;
	
	protected DataBindingContext bindingContext;	
			
	public SelectionBasedTimeSourceComposite(Composite parent, int style) 
	{
		this(parent, style, null);		
	}
	
	public SelectionBasedTimeSourceComposite(Composite parent, int style, SelectionBasedTimeSource selectionBasedTimeSource) 
	{
		super(parent, style);
		addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (bindingContext != null) {
					bindingContext.dispose();
				}
			}
		});

		setSelectionBasedTimeSource(selectionBasedTimeSource);
	}

	@Override
	public void activate(boolean active) 
	{
		
	}

	@Override
	public TimeSource getTimeSource() 
	{
		return getSelectionBasedTimeSource();
	}

	public SelectionBasedTimeSource getSelectionBasedTimeSource() 
	{
		return selectionBasedTimeSource;
	}

	public void setSelectionBasedTimeSource(SelectionBasedTimeSource selectionBasedTimeSource) 
	{
		setSelectionBasedTimeSource(selectionBasedTimeSource, true);
	}	
	
	public void setSelectionBasedTimeSource(SelectionBasedTimeSource selectionBasedTimeSource, boolean update) 
	{
		this.selectionBasedTimeSource = selectionBasedTimeSource;
		
		if(update)
		{
			if(bindingContext != null)
			{
				bindingContext.dispose();
				bindingContext = null;
			}
			
			if(selectionBasedTimeSource != null)
			{
				bindingContext = initDataBindings();								
			}
		}
		
		super.setTimeSource(selectionBasedTimeSource);
	}	
	
	protected DataBindingContext initDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		return bindingContext;
	}		
	
}

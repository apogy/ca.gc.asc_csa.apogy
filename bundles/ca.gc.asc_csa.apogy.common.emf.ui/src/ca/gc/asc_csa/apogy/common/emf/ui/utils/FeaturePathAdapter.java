package ca.gc.asc_csa.apogy.common.emf.ui.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class FeaturePathAdapter 
{
	private EObject eObject;
	private Adapter adapter;
	
	private List<EStructuralFeature> structuralFeatures = new ArrayList<EStructuralFeature>();
	private List<EObject> monitoredEObjects = new ArrayList<EObject>();
	private Map<EStructuralFeature, EObject> featureToEObject = new HashMap<EStructuralFeature, EObject>();
	private EStructuralFeature leafEStructuralFeature = null;

	
	public FeaturePathAdapter()
	{			
	}
	
	
	public FeaturePathAdapter(FeaturePath featurePath)
	{
		setFeaturePath(featurePath);			
	}
	
	public FeaturePathAdapter(List<EStructuralFeature> structuralFeatures)
	{
		setFeaturePath(structuralFeatures);
	}
	
	public void setFeaturePath(FeaturePath featurePath)
	{
		structuralFeatures.clear();
		
		EStructuralFeature[] features = featurePath.getFeaturePath();
		for(int i = 0; i < features.length; i++)
		{
			structuralFeatures.add(features[i]);
		}
		
		if(!structuralFeatures.isEmpty())
		{
			leafEStructuralFeature = structuralFeatures.get(structuralFeatures.size() -1);
		}
		
		initializeAdapter();
	}
	
	public void setFeaturePath(List<EStructuralFeature> newStructuralFeatures)
	{
		structuralFeatures.clear();
		
		structuralFeatures.addAll(newStructuralFeatures);
		
		if(!structuralFeatures.isEmpty())
		{
			leafEStructuralFeature = structuralFeatures.get(structuralFeatures.size() -1);
		}
		
		initializeAdapter();
	}
	
	public void setSource(EObject eObject)
	{
		this.eObject = eObject;
		
		initializeAdapter();
	}
	
	public void dispose()
	{
		setSource(null);
	}
	
	public void valueChanged(Object newValue) 
	{			
	}		
	
	private void initializeAdapter()
	{
		// Unregister from previously monitored eObjects.
		for(EObject eObject : monitoredEObjects)
		{
			eObject.eAdapters().remove(getAdapter());
		}
		
		// Clears list of monitored EObjects.
		monitoredEObjects.clear();
		featureToEObject.clear();
		
		// Register adapter to new EObject
		if(eObject != null)
		{
			Iterator<EStructuralFeature> it = structuralFeatures.iterator();
			EObject currentObject = eObject;
			while(it.hasNext())
			{
				EStructuralFeature feature = it.next();
				if(currentObject != null)
				{
					currentObject.eAdapters().add(getAdapter());
					monitoredEObjects.add(currentObject);
					featureToEObject.put(feature, currentObject);
					
					// Resolve the feature.
					Object value = currentObject.eGet(feature);
					if(value instanceof EObject)
					{
						currentObject = (EObject) value;						
					}
					else
					{
						currentObject = null;
					}
				}
			}
		}
	}
	
	private Adapter getAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					// System.out.println(msg);
					
					if(msg.getFeature() == leafEStructuralFeature)
					{
						FeaturePathAdapter.this.valueChanged(msg.getNewValue());
					}
					else
					{
						if(msg.getFeature() instanceof EStructuralFeature)
						{
							// Checks if the feature is one being monitored.
							if(structuralFeatures.contains(msg.getFeature()))
							{
								// Updates the EObject being monitored.																								
								EStructuralFeature feature = (EStructuralFeature) msg.getFeature();
								EObject eObject  = null;
								if(msg.getNewValue() instanceof EObject)
								{
									eObject = (EObject) msg.getNewValue();								
								}
								
								updateEObjectBeingMonitored(feature, eObject);
							}
						}						
					}
				}	
				
				private void updateEObjectBeingMonitored(EStructuralFeature feature, EObject newEObject)
				{
					// Get the previous EObject associated with the feature and remove it and it descendants.
					EObject previousEObject = featureToEObject.get(feature);
					
					if(!structuralFeatures.isEmpty())
					{
						if(previousEObject != null)
						{
							unregisterEObjectAndDescendants(feature);
						}											
						
						if(newEObject != null)
						{
							registerEObjectAndDescendants(feature, newEObject);
						}
					}
										
				}
				
				private void unregisterEObjectAndDescendants(EStructuralFeature feature)
				{
					EStructuralFeature currentFeature;
					
					int startIndex = structuralFeatures.indexOf(feature);
					if(startIndex >= 0)
					{
						for(int i = startIndex; i < structuralFeatures.size(); i++)
						{
							if(i != startIndex)
							{
								currentFeature = structuralFeatures.get(i);
								
								// Unregister the listener from the eOBject.
								EObject eObject = featureToEObject.get(currentFeature);
								if(eObject != null)
								{
									eObject.eAdapters().remove(getAdapter());							
									monitoredEObjects.remove(eObject);		
									featureToEObject.put(currentFeature, null);
								}
							}
						}					
					}
				}
				
				private void registerEObjectAndDescendants(EStructuralFeature feature, EObject newEObject)
				{
					EObject currentEObject = newEObject;			
					
					int featureIndex = structuralFeatures.indexOf(feature);
					int currentfeatureIndex = featureIndex + 1;
					while(newEObject != null)
					{
						newEObject.eAdapters().add(getAdapter());
						monitoredEObjects.add(newEObject);												
												
						if(currentfeatureIndex > 0 && currentfeatureIndex < structuralFeatures.size())
						{
							EStructuralFeature currentFeature = structuralFeatures.get(currentfeatureIndex);
							featureToEObject.put(currentFeature, currentEObject);
							
							Object object = currentEObject.eGet(currentFeature);
							
							if(object instanceof EObject)
							{
								newEObject = (EObject) object;
							}
							else
							{
								newEObject = null;
							}
						}
						
						currentfeatureIndex++;
					}
					
					// Check to see if the change is a new value.
					EObject lastestEObject = featureToEObject.get(leafEStructuralFeature);
					if(lastestEObject != null)
					{						
						FeaturePathAdapter.this.valueChanged(lastestEObject.eGet(leafEStructuralFeature));
					}
				}				
			};
		}
		
		return adapter;
	}
}

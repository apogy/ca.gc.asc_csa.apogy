package ca.gc.asc_csa.apogy.common.emf.ui.parts;
/*
 * Copyright (c) 2016, 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.PersistState;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIRCPConstants;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;

abstract public class AbstractPart {
	private Composite parentComposite;

	@Inject
	protected MPart mPart;

	protected String perspectiveID;

	@Inject
	protected ESelectionService selectionService;

	@Inject
	protected EModelService modelService;

	@Inject
	protected MApplication application;

	@PostConstruct
	public void createPartControl(Composite parent, @Optional MPerspective perspective) {
		userPostConstruct(mPart);
		if (perspective != null) {
			this.perspectiveID = perspective.getElementId();
		}
		parentComposite = parent;
		parentComposite.setLayout(new FillLayout());
		setNoContentComposite();
		parentComposite.layout();
		setEObject(getInitializeObject());
		
		// DEBUG
		reloadToolBarAndMenus();
	}

	@PostConstruct
	public void postConstruct(Composite parent) 
	{
		// reloadToolBarAndMenus();	
	}
	
	@PreDestroy
	public void preDestroy() {
		userPreDestroy(mPart);
	}

	@PersistState
	public void persist() {
		userPersistState(mPart);
	}

	/**
	 * Method called after the Part gas been constructed, but before its content
	 * is created.
	 * 
	 * @param mPart
	 *            The reference to the Part model.
	 */
	public void userPostConstruct(MPart mPart) {
	}

	/**
	 * Method called before the Part is destroyed.
	 * 
	 * @param mPart
	 *            The reference to the Part model.
	 */
	public void userPreDestroy(MPart mPart) {
	}

	/**
	 * Method called when "visual state persit" is performed.
	 * 
	 * @param mPart
	 *            The reference to the Part model.
	 */
	public void userPersistState(MPart mPart) {
	}

	/**
	 * Forces the tool-bar and menus to be reloaded from the model in the
	 * snippets.
	 * 
	 * FIXME This is to work around a problem where tool-bars and
	 * menus seems to become corrupted.
	 */
	public void reloadToolBarAndMenus() 
	{
		MPerspective perspectiveCopy = (MPerspective) modelService.cloneSnippet(application, perspectiveID,
				(MWindow) modelService.find(ApogyCommonEMFUIRCPConstants.MAIN_WINDOW__ID, application));
		
		if (perspectiveCopy != null) 
		{
			MPart partCopy = (MPart) modelService.find(mPart.getElementId(), perspectiveCopy);
			
			if (partCopy != null) 
			{
				// Updates the menus and tools with the ones from the part copy.
				if(mPart.getMenus() != null)
				{
					mPart.getMenus().clear();
					
					if(partCopy.getMenus() != null)
					{
						mPart.getMenus().addAll(partCopy.getMenus());
					}
				}
				
				if(mPart.getToolbar() != null && mPart.getToolbar().getChildren() != null)
				{
					mPart.getToolbar().getChildren().clear();
					
					if(partCopy.getToolbar() != null && partCopy.getToolbar().getChildren() != null)
					{
						mPart.getToolbar().getChildren().addAll(partCopy.getToolbar().getChildren());
					}
				}
			}
		}
	}

	/**
	 * Specifies the {@link EObject} to initially set in the part.
	 * 
	 * @return EObject
	 */
	abstract protected EObject getInitializeObject();

	/**
	 * Specifies the {@link Composite} to create in the part.
	 */
	abstract protected void createContentComposite(Composite parent, int style);

	/**
	 * Gets the content {@link Composite} in the part.
	 * 
	 * @return {@link Composite}
	 */
	public Composite getActualComposite() {
		if (parentComposite != null) {
			if (!parentComposite.isDisposed() && parentComposite.getChildren().length > 0) {
				return (Composite) parentComposite.getChildren()[0];
			}
		}
		return null;
	}

	/**
	 * This method is called when the {@link EObject} needs to be changed or
	 * initialized in the content parentComposite.
	 * 
	 * @param eObject
	 *            Reference to the EObject.
	 */
	abstract protected void setCompositeContent(EObject eObject);

	/**
	 * This method sets the {@link EObject} in the part. Null EObjects sets the
	 * content parentComposite to a nullSelection parentComposite according to
	 * the implemented part.
	 * 
	 * @param eObject
	 */
	protected void setEObject(EObject eObject) {
		if (parentComposite != null) {
			if (eObject != null) {
				if (getActualComposite() instanceof NoContentComposite) {
					for (Control control : parentComposite.getChildren()) {
						control.dispose();
					}
					createContentComposite(parentComposite, SWT.None);
					parentComposite.layout();
				}
				setCompositeContent(eObject);

			} else {
				setNoContentComposite();
			}
		}
	}

	/**
	 * Sets the part's parentComposite to a {@link NoActiveSessionComposite}.
	 */
	protected void setNoContentComposite() {
		if (getActualComposite() != null) {
			for (Control control : parentComposite.getChildren()) {
				control.dispose();
			}
		}
		if (!parentComposite.isDisposed()) {
			createNoContentComposite(parentComposite, SWT.None);
			parentComposite.layout();
		}
		selectionService.setSelection(null);
	}

	/**
	 * Specifies the {@link NoContentComposite} to set in the part when there is
	 * no content to display.
	 * 
	 * @return {@link NoContentComposite}
	 */
	abstract protected void createNoContentComposite(Composite parent, int style);

	@PreDestroy
	private void destroy() {
		dispose();
	}

	/**
	 * Called when the part is disposed.
	 */
	protected void dispose() {

	}
}
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.parts;

import java.util.HashMap;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.composites.EObjectDocumentationComposite;

public class GlobalDocumentationPart extends AbstractEObjectSelectionPart{

	private ISelectionListener selectionListener;
	
	
	@Override
	protected EObject getInitializeObject() {
		selectionService.addSelectionListener(getSelectionListener());
		Object obj = selectionService.getSelection();
		
		if(obj instanceof EObject){
			return (EObject)obj;
		}
		return null;
	}
	
	@Override
	protected void setCompositeContents(EObject eObject) {
		((EObjectDocumentationComposite)getActualComposite()).setEObject(eObject);
	}

	@Override
	protected void createContentComposite(Composite parent, int style) {
		new EObjectDocumentationComposite(parent, SWT.None);
	}
	
	@Override
	public void userPreDestroy(MPart mPart) {
		selectionService.removeSelectionListener(getSelectionListener());
	}	
	
	private ISelectionListener getSelectionListener() {
		if(selectionListener == null){
			selectionListener = new ISelectionListener() {
					@Override
					public void selectionChanged(MPart part, Object selection) {
						if (selection == null || selection instanceof EObject) {
							setEObject((EObject) selection);
						}
					}
				};
		}
		return selectionListener;
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() {
		return null;
	}

}

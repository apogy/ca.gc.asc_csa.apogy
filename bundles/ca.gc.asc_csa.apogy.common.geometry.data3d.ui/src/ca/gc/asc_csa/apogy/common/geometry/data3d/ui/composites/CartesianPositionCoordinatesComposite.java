/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.geometry.data3d.ui.composites;

import java.text.DecimalFormat;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.databinding.converters.DoubleToStringConverter;
import ca.gc.asc_csa.apogy.common.databinding.converters.StringToDoubleConverter;
import ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPositionCoordinates;

public class CartesianPositionCoordinatesComposite extends Composite 
{
	private CartesianPositionCoordinates cartesianPositionCoordinates;
	
	private DataBindingContext m_bindingContext;
	private EditingDomain editingDomain;
	
	private boolean enableEditing = true;
	private String formatString = "0.000";
	private DecimalFormat decimalFormat;
	
	private Text xText;
	private Text yText;
	private Text zText;
	
	public CartesianPositionCoordinatesComposite(Composite parent, int style, EditingDomain editingDomain, String formatString) 
	{		
		super(parent, style);
		this.formatString = formatString;
		this.editingDomain = editingDomain;
		
		setLayout(new GridLayout(6, false));

		Label xLabel = new Label(this, SWT.NONE);
		xLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		xLabel.setText("X (m):");
		
		xText = new Text(this, SWT.BORDER | SWT.SINGLE);
		xText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		
		Label yLabel = new Label(this, SWT.NONE);
		yLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		yLabel.setText("Y (m):");
		
		yText = new Text(this, SWT.BORDER | SWT.SINGLE);
		yText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		Label zLabel = new Label(this, SWT.NONE);
		zLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		zLabel.setText("Z (m):");
		
		zText = new Text(this, SWT.BORDER | SWT.SINGLE);
		zText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	@Override
	public void setEnabled(boolean enabled) 
	{			
		if(xText != null && !xText.isDisposed()) xText.setEnabled(enabled);
		if(yText != null && !yText.isDisposed()) yText.setEnabled(enabled);
		if(zText != null && !zText.isDisposed()) zText.setEnabled(enabled);
		
		super.setEnabled(enabled);
	}
	
	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(final boolean enableEditing) 
	{
		this.enableEditing = enableEditing;
		
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
			m_bindingContext = null;
		}		
		
		// Sets the Editing propert of the text fields.
		getDisplay().asyncExec(new Runnable()
    	{
    		public void run()
    		{
    			if(enableEditing)
    			{			
    				xText.setEditable(true);
    				yText.setEditable(true);
    				zText.setEditable(true);			
    			}
    			else
    			{
    				xText.setEditable(false);
    				yText.setEditable(false);
    				zText.setEditable(false);			
    			}
    		}
    	});
				
		if (cartesianPositionCoordinates != null) 
		{
			if(this.enableEditing)
			{				
				m_bindingContext = custom_initDataBindings();
			}
			else
			{				
				m_bindingContext = initDataBindingsNoEditing();
			}
		}				
	}
		
	public CartesianPositionCoordinates getCartesianPositionCoordinates() {
		return cartesianPositionCoordinates;
	}

	public void setCartesianPositionCoordinates(CartesianPositionCoordinates cartesianPositionCoordinates) {
		setCartesianPositionCoordinates(cartesianPositionCoordinates, true);
	}

	public void setCartesianPositionCoordinates(CartesianPositionCoordinates cartesianPositionCoordinates, boolean update) 
	{
		this.cartesianPositionCoordinates = cartesianPositionCoordinates;
		if (update) 
		{
			if (m_bindingContext != null) 
			{
				m_bindingContext.dispose();
				m_bindingContext = null;
			}
			if (cartesianPositionCoordinates != null) 
			{
				if(enableEditing)
				{
					m_bindingContext = custom_initDataBindings();
				}
				else
				{
					m_bindingContext = initDataBindingsNoEditing();
				}
			}
		}
	}
	
	protected DecimalFormat getDecimalFormat()
	{
		if(decimalFormat == null)
		{
			decimalFormat = new DecimalFormat(formatString);
		}
		return decimalFormat;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	protected DataBindingContext custom_initDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		//
		IObservableValue xObserveWidget = WidgetProperties.text(SWT.FocusOut).observe(xText);
		IObservableValue xObserveValue = (editingDomain == null ? 
				EMFProperties.value(ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__X).observe(cartesianPositionCoordinates):
				EMFEditProperties.value(editingDomain, ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__X).observe(cartesianPositionCoordinates));
		
		UpdateValueStrategy strategy = new UpdateValueStrategy();
		strategy.setConverter(new StringToDoubleConverter());
		
		UpdateValueStrategy strategy_1 = new UpdateValueStrategy();
		strategy_1.setConverter(new DoubleToStringConverter(getDecimalFormat()));
		
		bindingContext.bindValue(xObserveWidget, xObserveValue, strategy, strategy_1);
		
		//
		IObservableValue yObserveWidget = WidgetProperties.text(SWT.FocusOut).observe(yText);
		IObservableValue yObserveValue = (editingDomain == null ? 
				EMFProperties.value(ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__Y).observe(cartesianPositionCoordinates):
				EMFEditProperties.value(editingDomain, ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__Y).observe(cartesianPositionCoordinates));
					
		UpdateValueStrategy strategy_2 = new UpdateValueStrategy();
		strategy_2.setConverter(new StringToDoubleConverter());
		
		UpdateValueStrategy strategy_3 = new UpdateValueStrategy();
		strategy_3.setConverter(new DoubleToStringConverter(getDecimalFormat()));
		
		bindingContext.bindValue(yObserveWidget, yObserveValue, strategy_2, strategy_3);
		
		//
		IObservableValue zObserveWidget = WidgetProperties.text(SWT.FocusOut).observe(zText);
		IObservableValue zObserveValue = (editingDomain == null ? 
				EMFProperties.value(ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__Z).observe(cartesianPositionCoordinates):
				EMFEditProperties.value(editingDomain, ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__Z).observe(cartesianPositionCoordinates));
					
		UpdateValueStrategy strategy_4 = new UpdateValueStrategy();
		strategy_4.setConverter(new StringToDoubleConverter());
		
		UpdateValueStrategy strategy_5 = new UpdateValueStrategy();
		strategy_5.setConverter(new DoubleToStringConverter(getDecimalFormat()));
		
		bindingContext.bindValue(zObserveWidget, zObserveValue, strategy_4, strategy_5);
		//
		return bindingContext;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected DataBindingContext initDataBindingsNoEditing() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		//
		IObservableValue xObserveWidget = WidgetProperties.text(SWT.FocusOut).observe(xText);
		IObservableValue xObserveValue = (editingDomain == null ? 
				EMFProperties.value(ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__X).observe(cartesianPositionCoordinates):
				EMFEditProperties.value(editingDomain, ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__X).observe(cartesianPositionCoordinates));
		
		UpdateValueStrategy strategy_1 = new UpdateValueStrategy();
		strategy_1.setConverter(new DoubleToStringConverter(getDecimalFormat()));
		
		bindingContext.bindValue(xObserveWidget, xObserveValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), strategy_1);
		
		//
		IObservableValue yObserveWidget = WidgetProperties.text(SWT.FocusOut).observe(yText);
		IObservableValue yObserveValue = (editingDomain == null ? 
				EMFProperties.value(ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__Y).observe(cartesianPositionCoordinates):
				EMFEditProperties.value(editingDomain, ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__Y).observe(cartesianPositionCoordinates));
					
		UpdateValueStrategy strategy_3 = new UpdateValueStrategy();
		strategy_3.setConverter(new DoubleToStringConverter(getDecimalFormat()));
		
		bindingContext.bindValue(yObserveWidget, yObserveValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), strategy_3);
		
		//
		IObservableValue zObserveWidget = WidgetProperties.text(SWT.FocusOut).observe(zText);
		IObservableValue zObserveValue = (editingDomain == null ? 
				EMFProperties.value(ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__Z).observe(cartesianPositionCoordinates):
				EMFEditProperties.value(editingDomain, ApogyCommonGeometryData3DPackage.Literals.CARTESIAN_POSITION_COORDINATES__Z).observe(cartesianPositionCoordinates));
					
		UpdateValueStrategy strategy_5 = new UpdateValueStrategy();
		strategy_5.setConverter(new DoubleToStringConverter(getDecimalFormat()));
		
		bindingContext.bindValue(zObserveWidget, zObserveValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), strategy_5);
		
		return bindingContext;
	}
}

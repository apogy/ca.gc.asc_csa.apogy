/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Canadian Space Agency (CSA) - Initial API and implementation
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
 *           
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui.provider;


import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;

/**
 * This is the item provider adapter for a {@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AirspaceWorldWindLayerItemProvider extends SurfacePolygonWorldWindLayerItemProvider 
{
	protected DecimalFormat format = new DecimalFormat("0");
	
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AirspaceWorldWindLayerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLowerAltitudePropertyDescriptor(object);
			addUpperAltitudePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Lower Altitude feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLowerAltitudePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AirspaceWorldWindLayer_lowerAltitude_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AirspaceWorldWindLayer_lowerAltitude_feature", "_UI_AirspaceWorldWindLayer_type"),
				 ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Upper Altitude feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUpperAltitudePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AirspaceWorldWindLayer_upperAltitude_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AirspaceWorldWindLayer_upperAltitude_feature", "_UI_AirspaceWorldWindLayer_type"),
				 ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns AirspaceWorldWindLayer.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/AirspaceWorldWindLayer"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	@Override
	public String getText(Object object) 
	{
		AirspaceWorldWindLayer airspaceWorldWindLayer = (AirspaceWorldWindLayer) object;			
		String label = airspaceWorldWindLayer.getName();
		
		if(label == null || label.length() == 0)
		{
			label = getString("_UI_AirspaceWorldWindLayer_type");
		}
		
		String suffix = getAirspaceWorldWindLayerSuffix(airspaceWorldWindLayer);
		if(suffix.length() > 0)
		{
			label += " (" + suffix + ")";
		}
		
		return label;
	}
	
	protected String getAirspaceWorldWindLayerSuffix(AirspaceWorldWindLayer airspaceWorldWindLayer)
	{
		String suffix = "";
		
		String topSuffix = getSurfacePolygonWorldWindLayerSuffix(airspaceWorldWindLayer);
		
		if(topSuffix.length() > 0)
		{
			suffix += topSuffix;
		}
		
		String altitudesSuffix = format.format(airspaceWorldWindLayer.getLowerAltitude()) + "m, " + format.format(airspaceWorldWindLayer.getUpperAltitude()) + "m";
		
		if(topSuffix.length() > 0)
		{
			suffix += ", ";
		}
		suffix += altitudesSuffix;
		
		return suffix;		
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AirspaceWorldWindLayer.class)) {
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE:
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}

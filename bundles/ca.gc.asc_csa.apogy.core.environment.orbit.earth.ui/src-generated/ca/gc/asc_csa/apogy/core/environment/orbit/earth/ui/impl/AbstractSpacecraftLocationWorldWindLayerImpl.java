/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.Timed;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl;

import ca.gc.asc_csa.apogy.core.environment.orbit.OrbitModel;
import ca.gc.asc_csa.apogy.core.environment.orbit.SpacecraftState;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthFacade;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.OreKitBackedSpacecraftState;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractSpacecraftLocationWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.AnnotationAttributes;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.GlobeAnnotation;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.SurfaceCircle;

import java.awt.Color;
import java.awt.Font;
import java.text.DecimalFormat;

import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Spacecraft Location World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractSpacecraftLocationWorldWindLayerImpl#getOrbitModel <em>Orbit Model</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractSpacecraftLocationWorldWindLayerImpl#getTimeSource <em>Time Source</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractSpacecraftLocationWorldWindLayerImpl#isShowGroundProjection <em>Show Ground Projection</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractSpacecraftLocationWorldWindLayerImpl#isShowLatLon <em>Show Lat Lon</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractSpacecraftLocationWorldWindLayerImpl extends AbstractWorldWindLayerImpl implements AbstractSpacecraftLocationWorldWindLayer 
{
	public static final String DEGREE_STRING = 	"\u00b0";
	private Adapter timedAdapter = null;
	
	/**
	 * The cached value of the '{@link #getOrbitModel() <em>Orbit Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrbitModel()
	 * @generated
	 * @ordered
	 */
	protected OrbitModel orbitModel;

	/**
	 * The cached value of the '{@link #getTimeSource() <em>Time Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeSource()
	 * @generated
	 * @ordered
	 */
	protected Timed timeSource;

	/**
	 * The default value of the '{@link #isShowGroundProjection() <em>Show Ground Projection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowGroundProjection()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SHOW_GROUND_PROJECTION_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isShowGroundProjection() <em>Show Ground Projection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowGroundProjection()
	 * @generated
	 * @ordered
	 */
	protected boolean showGroundProjection = SHOW_GROUND_PROJECTION_EDEFAULT;

	/**
	 * The default value of the '{@link #isShowLatLon() <em>Show Lat Lon</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowLatLon()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SHOW_LAT_LON_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isShowLatLon() <em>Show Lat Lon</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowLatLon()
	 * @generated
	 * @ordered
	 */
	protected boolean showLatLon = SHOW_LAT_LON_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractSpacecraftLocationWorldWindLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreEnvironmentOrbitEarthUIPackage.Literals.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrbitModel getOrbitModel() {
		if (orbitModel != null && orbitModel.eIsProxy()) {
			InternalEObject oldOrbitModel = (InternalEObject)orbitModel;
			orbitModel = (OrbitModel)eResolveProxy(oldOrbitModel);
			if (orbitModel != oldOrbitModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__ORBIT_MODEL, oldOrbitModel, orbitModel));
			}
		}
		return orbitModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrbitModel basicGetOrbitModel() {
		return orbitModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrbitModel(OrbitModel newOrbitModel) {
		OrbitModel oldOrbitModel = orbitModel;
		orbitModel = newOrbitModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__ORBIT_MODEL, oldOrbitModel, orbitModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timed getTimeSource() {
		if (timeSource != null && timeSource.eIsProxy()) {
			InternalEObject oldTimeSource = (InternalEObject)timeSource;
			timeSource = (Timed)eResolveProxy(oldTimeSource);
			if (timeSource != oldTimeSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__TIME_SOURCE, oldTimeSource, timeSource));
			}
		}
		return timeSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timed basicGetTimeSource() {
		return timeSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setTimeSource(Timed newTimeSource)
	{
		// Unregister from previous timed if applicable.
		if(timeSource != null)
		{
			timeSource.eAdapters().remove(getTimedAdapter());
		}
		
		// Updates timesource
		setTimeSourceGen(newTimeSource);
		
		// Register to new timed if applicable.
		if(newTimeSource != null)
		{
			if(isAutoUpdateEnabled())
			{
				try {
					update();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			newTimeSource.eAdapters().add(getTimedAdapter());
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeSourceGen(Timed newTimeSource) {
		Timed oldTimeSource = timeSource;
		timeSource = newTimeSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__TIME_SOURCE, oldTimeSource, timeSource));
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isShowGroundProjection() {
		return showGroundProjection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setShowGroundProjection(boolean newShowGroundProjection)
	{
		setShowGroundProjectionGen(newShowGroundProjection);
		
		if(isAutoUpdateEnabled())
		{
			try 
			{
				update();
			} 
			catch (Exception e) 
			{			
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowGroundProjectionGen(boolean newShowGroundProjection) {
		boolean oldShowGroundProjection = showGroundProjection;
		showGroundProjection = newShowGroundProjection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_GROUND_PROJECTION, oldShowGroundProjection, showGroundProjection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isShowLatLon() {
		return showLatLon;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setShowLatLon(boolean newShowLatLon) 
	{
		setShowLatLonGen(newShowLatLon);
		
		if(isAutoUpdateEnabled())
		{
			try 
			{
				update();
			} 
			catch (Exception e) 
			{			
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowLatLonGen(boolean newShowLatLon) {
		boolean oldShowLatLon = showLatLon;
		showLatLon = newShowLatLon;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_LAT_LON, oldShowLatLon, showLatLon));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__ORBIT_MODEL:
				if (resolve) return getOrbitModel();
				return basicGetOrbitModel();
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__TIME_SOURCE:
				if (resolve) return getTimeSource();
				return basicGetTimeSource();
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_GROUND_PROJECTION:
				return isShowGroundProjection();
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_LAT_LON:
				return isShowLatLon();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__ORBIT_MODEL:
				setOrbitModel((OrbitModel)newValue);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__TIME_SOURCE:
				setTimeSource((Timed)newValue);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_GROUND_PROJECTION:
				setShowGroundProjection((Boolean)newValue);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_LAT_LON:
				setShowLatLon((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__ORBIT_MODEL:
				setOrbitModel((OrbitModel)null);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__TIME_SOURCE:
				setTimeSource((Timed)null);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_GROUND_PROJECTION:
				setShowGroundProjection(SHOW_GROUND_PROJECTION_EDEFAULT);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_LAT_LON:
				setShowLatLon(SHOW_LAT_LON_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__ORBIT_MODEL:
				return orbitModel != null;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__TIME_SOURCE:
				return timeSource != null;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_GROUND_PROJECTION:
				return showGroundProjection != SHOW_GROUND_PROJECTION_EDEFAULT;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_LAT_LON:
				return showLatLon != SHOW_LAT_LON_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (showGroundProjection: ");
		result.append(showGroundProjection);
		result.append(", showLatLon: ");
		result.append(showLatLon);
		result.append(')');
		return result.toString();
	}

	@Override
	public void setName(String newName) 
	{
		super.setName(newName);
		
		updateRenderableLayer();
	}
	
	@Override
	public void dispose() 
	{
		// Unregister from time source.
		if(getTimeSource() != null) getTimeSource().eAdapters().remove(getTimedAdapter());
		
		super.dispose();
	}
	
	protected void updateRenderableLayer()
	{				
		if(!isUpdating())
		{
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, true);		
			
			RenderableLayer layer = getRenderableLayer();
			layer.removeAllRenderables();
		
			Job job = new Job("AbstractSpacecraftLocationWorldWindLayer updateRenderableLayer")
			{
				protected org.eclipse.core.runtime.IStatus run(org.eclipse.core.runtime.IProgressMonitor monitor) 
				{								
					if(isVisible() && !isDisposed() && getOrbitModel() != null && getTimeSource() != null)
					{
						try 
						{				
							SpacecraftState ss = getOrbitModel().propagate( getTimeSource().getTime());								
							GeographicCoordinates coord = ApogyCoreEnvironmentOrbitEarthFacade.INSTANCE.convertToGeographicCoordinates((OreKitBackedSpacecraftState) ss);
	
							// Create latitude, longitude and elevation values.
				            Angle latitude = Angle.fromRadiansLatitude(coord.getLatitude());
				            Angle longitude = Angle.fromRadiansLongitude(coord.getLongitude());
				            double elevation = coord.getElevation();
				            
				            // Show the Globe annotation at 0 altitude if show ground projection is enabled.
				            if(isShowGroundProjection()) elevation = 0;
				            
				            Position position = new Position(latitude, longitude, elevation);
				            
				            // Creates an annotation.
				            String text = getDisplayedText(ss, coord);
				            GlobeAnnotation annotation = new GlobeAnnotation(text, position);	
				            
				            AnnotationAttributes annotationAttributes = new AnnotationAttributes();	            
				            annotationAttributes.setVisible(true);
				            
				            Font font = annotationAttributes.getFont();	               
				            font = font.deriveFont(Font.BOLD, 16.0f);
				            annotationAttributes.setFont(font);
				            
				            Color transparent = new Color(0, 0, 1f, 0.3f);
				            annotationAttributes.setBackgroundColor(transparent);
				            annotationAttributes.setTextColor(Color.RED);	
				            
				            annotation.setAttributes(annotationAttributes);	            
				            layer.addRenderable(annotation);
				            
				            if(isShowGroundProjection())
				            {
				    	        BasicShapeAttributes attributes = new BasicShapeAttributes();
				    	        attributes.setDrawInterior(true);
				    	        
				    	        Material mat = new Material(Color.RED);
				    	        attributes.setInteriorMaterial(mat);
				    	        SurfaceCircle surfaceCircle = new SurfaceCircle(attributes, position, 50000, 24);	    
				    	        surfaceCircle.setVisible(true);
				    	        layer.addRenderable(surfaceCircle);	    	        
				            }
				         
						} 
						catch (Exception e) 
						{				
							e.printStackTrace();
						}
					}				
					// Force layer to update.
					getRenderableLayer().firePropertyChange(AVKey.LAYER, null, this);
					
					ApogyCommonTransactionFacade.INSTANCE.basicSet(AbstractSpacecraftLocationWorldWindLayerImpl.this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, false, true);
										
					return Status.OK_STATUS;
				}
			};
			job.schedule();
		}	
	}
	
	protected String getDisplayedText(SpacecraftState ss, GeographicCoordinates coord)
	{
		String text = getName();
		
		if(isShowLatLon())
		{
			DecimalFormat latLongDecimalFormat = new DecimalFormat("0.000");
			double lat = Math.toDegrees(coord.getLatitude());
			double lon = Math.toDegrees(coord.getLongitude());		
			
			DecimalFormat altitudeDecimalFormat = new DecimalFormat("0.0");
			double altitude = coord.getElevation() * 0.001;
			
			text += "\n lat " + latLongDecimalFormat.format(lat) + DEGREE_STRING + ", lon " + latLongDecimalFormat.format(lon) + DEGREE_STRING + ", alt " +  altitudeDecimalFormat.format(altitude) + " km";
			
		}		
		
		return text;
	}

	protected Adapter getTimedAdapter() 
	{
		if(timedAdapter == null)
		{
			timedAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{				
					if(msg.getNotifier() instanceof Timed)
					{
						int featureId = msg.getFeatureID(Timed.class);
						
						switch (featureId) 
						{
							case ApogyCommonEMFPackage.TIMED__TIME:
								if(isAutoUpdateEnabled())
								{
									try 									
									{
										update();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							break;

							default:
							break;
						}
					}						
				}
			};
		}
		return timedAdapter;
	}


} //AbstractSpacecraftLocationWorldWindLayerImpl

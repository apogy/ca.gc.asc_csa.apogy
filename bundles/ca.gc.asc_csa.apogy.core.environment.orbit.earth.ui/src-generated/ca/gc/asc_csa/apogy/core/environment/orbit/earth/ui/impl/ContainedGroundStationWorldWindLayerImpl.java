/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.core.environment.orbit.earth.GroundStation;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ContainedGroundStationWorldWindLayer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Contained Ground Station World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ContainedGroundStationWorldWindLayerImpl#getContainedGroundStation <em>Contained Ground Station</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ContainedGroundStationWorldWindLayerImpl extends AbstractGroundStationWorldWindLayerImpl implements ContainedGroundStationWorldWindLayer {
	/**
	 * The cached value of the '{@link #getContainedGroundStation() <em>Contained Ground Station</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedGroundStation()
	 * @generated
	 * @ordered
	 */
	protected GroundStation containedGroundStation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContainedGroundStationWorldWindLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreEnvironmentOrbitEarthUIPackage.Literals.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroundStation getContainedGroundStation() {
		return containedGroundStation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainedGroundStation(GroundStation newContainedGroundStation, NotificationChain msgs) {
		GroundStation oldContainedGroundStation = containedGroundStation;
		containedGroundStation = newContainedGroundStation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__CONTAINED_GROUND_STATION, oldContainedGroundStation, newContainedGroundStation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setContainedGroundStation(GroundStation newContainedGroundStation) 
	{
		setContainedGroundStationGen(newContainedGroundStation);
		setGroundStation(newContainedGroundStation);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainedGroundStationGen(GroundStation newContainedGroundStation) {
		if (newContainedGroundStation != containedGroundStation) {
			NotificationChain msgs = null;
			if (containedGroundStation != null)
				msgs = ((InternalEObject)containedGroundStation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyCoreEnvironmentOrbitEarthUIPackage.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__CONTAINED_GROUND_STATION, null, msgs);
			if (newContainedGroundStation != null)
				msgs = ((InternalEObject)newContainedGroundStation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyCoreEnvironmentOrbitEarthUIPackage.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__CONTAINED_GROUND_STATION, null, msgs);
			msgs = basicSetContainedGroundStation(newContainedGroundStation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__CONTAINED_GROUND_STATION, newContainedGroundStation, newContainedGroundStation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__CONTAINED_GROUND_STATION:
				return basicSetContainedGroundStation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__CONTAINED_GROUND_STATION:
				return getContainedGroundStation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__CONTAINED_GROUND_STATION:
				setContainedGroundStation((GroundStation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__CONTAINED_GROUND_STATION:
				setContainedGroundStation((GroundStation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__CONTAINED_GROUND_STATION:
				return containedGroundStation != null;
		}
		return super.eIsSet(featureID);
	}

} //ContainedGroundStationWorldWindLayerImpl

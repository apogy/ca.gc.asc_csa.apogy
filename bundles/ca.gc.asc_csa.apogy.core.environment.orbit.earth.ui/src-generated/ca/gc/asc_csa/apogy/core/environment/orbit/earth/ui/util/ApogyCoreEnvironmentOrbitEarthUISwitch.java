package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.util;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.core.Updatable;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.*;
import ca.gc.asc_csa.apogy.core.invocator.AbstractToolsListContainer;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage
 * @generated
 */
public class ApogyCoreEnvironmentOrbitEarthUISwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected static ApogyCoreEnvironmentOrbitEarthUIPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCoreEnvironmentOrbitEarthUISwitch() {
		if (modelPackage == null) {
			modelPackage = ApogyCoreEnvironmentOrbitEarthUIPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER: {
				AbstractSpacecraftLocationWorldWindLayer abstractSpacecraftLocationWorldWindLayer = (AbstractSpacecraftLocationWorldWindLayer)theEObject;
				T result = caseAbstractSpacecraftLocationWorldWindLayer(abstractSpacecraftLocationWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(abstractSpacecraftLocationWorldWindLayer);
				if (result == null) result = caseUpdatable(abstractSpacecraftLocationWorldWindLayer);
				if (result == null) result = caseNamed(abstractSpacecraftLocationWorldWindLayer);
				if (result == null) result = caseDescribed(abstractSpacecraftLocationWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreEnvironmentOrbitEarthUIPackage.SPACECRAFT_LOCATION_WORLD_WIND_LAYER: {
				SpacecraftLocationWorldWindLayer spacecraftLocationWorldWindLayer = (SpacecraftLocationWorldWindLayer)theEObject;
				T result = caseSpacecraftLocationWorldWindLayer(spacecraftLocationWorldWindLayer);
				if (result == null) result = caseAbstractSpacecraftLocationWorldWindLayer(spacecraftLocationWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(spacecraftLocationWorldWindLayer);
				if (result == null) result = caseUpdatable(spacecraftLocationWorldWindLayer);
				if (result == null) result = caseNamed(spacecraftLocationWorldWindLayer);
				if (result == null) result = caseDescribed(spacecraftLocationWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ORBIT_MODEL_WORLD_WIND_LAYER: {
				OrbitModelWorldWindLayer orbitModelWorldWindLayer = (OrbitModelWorldWindLayer)theEObject;
				T result = caseOrbitModelWorldWindLayer(orbitModelWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(orbitModelWorldWindLayer);
				if (result == null) result = caseUpdatable(orbitModelWorldWindLayer);
				if (result == null) result = caseNamed(orbitModelWorldWindLayer);
				if (result == null) result = caseDescribed(orbitModelWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreEnvironmentOrbitEarthUIPackage.SPACECRAFT_SWATH_WORLD_WIND_LAYER: {
				SpacecraftSwathWorldWindLayer spacecraftSwathWorldWindLayer = (SpacecraftSwathWorldWindLayer)theEObject;
				T result = caseSpacecraftSwathWorldWindLayer(spacecraftSwathWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(spacecraftSwathWorldWindLayer);
				if (result == null) result = caseUpdatable(spacecraftSwathWorldWindLayer);
				if (result == null) result = caseNamed(spacecraftSwathWorldWindLayer);
				if (result == null) result = caseDescribed(spacecraftSwathWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER: {
				AbstractGroundStationWorldWindLayer abstractGroundStationWorldWindLayer = (AbstractGroundStationWorldWindLayer)theEObject;
				T result = caseAbstractGroundStationWorldWindLayer(abstractGroundStationWorldWindLayer);
				if (result == null) result = caseEarthSurfaceLocationWorldWindLayer(abstractGroundStationWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(abstractGroundStationWorldWindLayer);
				if (result == null) result = caseUpdatable(abstractGroundStationWorldWindLayer);
				if (result == null) result = caseNamed(abstractGroundStationWorldWindLayer);
				if (result == null) result = caseDescribed(abstractGroundStationWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreEnvironmentOrbitEarthUIPackage.GROUND_STATION_WORLD_WIND_LAYER: {
				GroundStationWorldWindLayer groundStationWorldWindLayer = (GroundStationWorldWindLayer)theEObject;
				T result = caseGroundStationWorldWindLayer(groundStationWorldWindLayer);
				if (result == null) result = caseAbstractGroundStationWorldWindLayer(groundStationWorldWindLayer);
				if (result == null) result = caseEarthSurfaceLocationWorldWindLayer(groundStationWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(groundStationWorldWindLayer);
				if (result == null) result = caseUpdatable(groundStationWorldWindLayer);
				if (result == null) result = caseNamed(groundStationWorldWindLayer);
				if (result == null) result = caseDescribed(groundStationWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreEnvironmentOrbitEarthUIPackage.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER: {
				ContainedGroundStationWorldWindLayer containedGroundStationWorldWindLayer = (ContainedGroundStationWorldWindLayer)theEObject;
				T result = caseContainedGroundStationWorldWindLayer(containedGroundStationWorldWindLayer);
				if (result == null) result = caseAbstractGroundStationWorldWindLayer(containedGroundStationWorldWindLayer);
				if (result == null) result = caseEarthSurfaceLocationWorldWindLayer(containedGroundStationWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(containedGroundStationWorldWindLayer);
				if (result == null) result = caseUpdatable(containedGroundStationWorldWindLayer);
				if (result == null) result = caseNamed(containedGroundStationWorldWindLayer);
				if (result == null) result = caseDescribed(containedGroundStationWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreEnvironmentOrbitEarthUIPackage.SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION_LIST: {
				SpacecraftVisibilityPassViewConfigurationList spacecraftVisibilityPassViewConfigurationList = (SpacecraftVisibilityPassViewConfigurationList)theEObject;
				T result = caseSpacecraftVisibilityPassViewConfigurationList(spacecraftVisibilityPassViewConfigurationList);
				if (result == null) result = caseAbstractToolsListContainer(spacecraftVisibilityPassViewConfigurationList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreEnvironmentOrbitEarthUIPackage.SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION: {
				SpacecraftVisibilityPassViewConfiguration spacecraftVisibilityPassViewConfiguration = (SpacecraftVisibilityPassViewConfiguration)theEObject;
				T result = caseSpacecraftVisibilityPassViewConfiguration(spacecraftVisibilityPassViewConfiguration);
				if (result == null) result = caseNamed(spacecraftVisibilityPassViewConfiguration);
				if (result == null) result = caseDescribed(spacecraftVisibilityPassViewConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreEnvironmentOrbitEarthUIPackage.EARTH_VIEW_UTILITIES: {
				EarthViewUtilities earthViewUtilities = (EarthViewUtilities)theEObject;
				T result = caseEarthViewUtilities(earthViewUtilities);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreEnvironmentOrbitEarthUIPackage.GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER: {
				GroundStationWorldWindLayerWizardPagesProvider groundStationWorldWindLayerWizardPagesProvider = (GroundStationWorldWindLayerWizardPagesProvider)theEObject;
				T result = caseGroundStationWorldWindLayerWizardPagesProvider(groundStationWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseEarthSurfaceLocationWorldWindLayerWizardPagesProvider(groundStationWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseAbstractWorldWindLayerWizardPagesProvider(groundStationWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(groundStationWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(groundStationWorldWindLayerWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreEnvironmentOrbitEarthUIPackage.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER: {
				ContainedGroundStationWorldWindLayerWizardPagesProvider containedGroundStationWorldWindLayerWizardPagesProvider = (ContainedGroundStationWorldWindLayerWizardPagesProvider)theEObject;
				T result = caseContainedGroundStationWorldWindLayerWizardPagesProvider(containedGroundStationWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseEarthSurfaceLocationWorldWindLayerWizardPagesProvider(containedGroundStationWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseAbstractWorldWindLayerWizardPagesProvider(containedGroundStationWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(containedGroundStationWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(containedGroundStationWorldWindLayerWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Spacecraft Location World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Spacecraft Location World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSpacecraftLocationWorldWindLayer(AbstractSpacecraftLocationWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Spacecraft Location World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Spacecraft Location World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSpacecraftLocationWorldWindLayer(SpacecraftLocationWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Orbit Model World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Orbit Model World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrbitModelWorldWindLayer(OrbitModelWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Spacecraft Swath World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Spacecraft Swath World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSpacecraftSwathWorldWindLayer(SpacecraftSwathWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Ground Station World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Ground Station World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractGroundStationWorldWindLayer(AbstractGroundStationWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth Surface Location World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth Surface Location World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthSurfaceLocationWorldWindLayer(EarthSurfaceLocationWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ground Station World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ground Station World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGroundStationWorldWindLayer(GroundStationWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contained Ground Station World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contained Ground Station World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainedGroundStationWorldWindLayer(ContainedGroundStationWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Spacecraft Visibility Pass View Configuration List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Spacecraft Visibility Pass View Configuration List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSpacecraftVisibilityPassViewConfigurationList(SpacecraftVisibilityPassViewConfigurationList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Spacecraft Visibility Pass View Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Spacecraft Visibility Pass View Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSpacecraftVisibilityPassViewConfiguration(SpacecraftVisibilityPassViewConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth View Utilities</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth View Utilities</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthViewUtilities(EarthViewUtilities object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ground Station World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ground Station World Wind Layer Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGroundStationWorldWindLayerWizardPagesProvider(GroundStationWorldWindLayerWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contained Ground Station World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contained Ground Station World Wind Layer Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainedGroundStationWorldWindLayerWizardPagesProvider(ContainedGroundStationWorldWindLayerWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Tools List Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Tools List Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractToolsListContainer(AbstractToolsListContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWizardPagesProvider(WizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedDescribedWizardPagesProvider(NamedDescribedWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract World Wind Layer Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractWorldWindLayerWizardPagesProvider(AbstractWorldWindLayerWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthSurfaceLocationWorldWindLayerWizardPagesProvider(EarthSurfaceLocationWorldWindLayerWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamed(Named object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Described</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Described</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDescribed(Described object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractWorldWindLayer(AbstractWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Updatable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Updatable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUpdatable(Updatable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ApogyCoreEnvironmentOrbitEarthUISwitch

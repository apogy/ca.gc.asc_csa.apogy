/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui;

import ca.gc.asc_csa.apogy.core.Updatable;

import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer;

import ca.gc.asc_csa.apogy.core.environment.orbit.earth.GroundStation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Ground Station World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#getGroundStation <em>Ground Station</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#getReferenceAltitude <em>Reference Altitude</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowVisibilityCircle <em>Show Visibility Circle</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowVisibilityCone <em>Show Visibility Cone</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowOutline <em>Show Outline</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage#getAbstractGroundStationWorldWindLayer()
 * @model abstract="true"
 * @generated
 */
public interface AbstractGroundStationWorldWindLayer extends EarthSurfaceLocationWorldWindLayer, Updatable {
	/**
	 * Returns the value of the '<em><b>Ground Station</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The groundStation currently displayed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Ground Station</em>' reference.
	 * @see #setGroundStation(GroundStation)
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage#getAbstractGroundStationWorldWindLayer_GroundStation()
	 * @model transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' property='None'"
	 * @generated
	 */
	GroundStation getGroundStation();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#getGroundStation <em>Ground Station</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ground Station</em>' reference.
	 * @see #getGroundStation()
	 * @generated
	 */
	void setGroundStation(GroundStation value);

	/**
	 * Returns the value of the '<em><b>Reference Altitude</b></em>' attribute.
	 * The default value is <code>"500"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The altitude to which to project
	 * the ElevationMask.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Reference Altitude</em>' attribute.
	 * @see #setReferenceAltitude(double)
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage#getAbstractGroundStationWorldWindLayer_ReferenceAltitude()
	 * @model default="500" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel propertyCategory='VISUALS' apogy_units='km'"
	 * @generated
	 */
	double getReferenceAltitude();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#getReferenceAltitude <em>Reference Altitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Altitude</em>' attribute.
	 * @see #getReferenceAltitude()
	 * @generated
	 */
	void setReferenceAltitude(double value);

	/**
	 * Returns the value of the '<em><b>Show Visibility Circle</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Whether or not to show the circle representing the area projected on the ground of extent of the visibility.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Show Visibility Circle</em>' attribute.
	 * @see #setShowVisibilityCircle(boolean)
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage#getAbstractGroundStationWorldWindLayer_ShowVisibilityCircle()
	 * @model default="true" unique="false"
	 * @generated
	 */
	boolean isShowVisibilityCircle();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowVisibilityCircle <em>Show Visibility Circle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Visibility Circle</em>' attribute.
	 * @see #isShowVisibilityCircle()
	 * @generated
	 */
	void setShowVisibilityCircle(boolean value);

	/**
	 * Returns the value of the '<em><b>Show Visibility Cone</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Whether or not to show the circle representing the volume of the visibility volume.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Show Visibility Cone</em>' attribute.
	 * @see #setShowVisibilityCone(boolean)
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage#getAbstractGroundStationWorldWindLayer_ShowVisibilityCone()
	 * @model default="true" unique="false"
	 * @generated
	 */
	boolean isShowVisibilityCone();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowVisibilityCone <em>Show Visibility Cone</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Visibility Cone</em>' attribute.
	 * @see #isShowVisibilityCone()
	 * @generated
	 */
	void setShowVisibilityCone(boolean value);

	/**
	 * Returns the value of the '<em><b>Show Outline</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Whether or not to show the outline of the representation visibility circle and cone.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Show Outline</em>' attribute.
	 * @see #setShowOutline(boolean)
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage#getAbstractGroundStationWorldWindLayer_ShowOutline()
	 * @model default="true" unique="false"
	 * @generated
	 */
	boolean isShowOutline();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowOutline <em>Show Outline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Outline</em>' attribute.
	 * @see #isShowOutline()
	 * @generated
	 */
	void setShowOutline(boolean value);

} // AbstractGroundStationWorldWindLayer

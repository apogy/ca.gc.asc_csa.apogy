/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ConicalFieldOfView;
import ca.gc.asc_csa.apogy.addons.sensors.fov.DistanceRange;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.EarthSurfaceLocation;
import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerImpl;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.utils.WorldWindUtils;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthPackage;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ConstantElevationMask;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ElevationMask;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.GroundStation;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.utils.MultiEObjectsAdapter;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.Earth;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Cone;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.SurfaceCircle;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Ground Station World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractGroundStationWorldWindLayerImpl#getGroundStation <em>Ground Station</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractGroundStationWorldWindLayerImpl#getReferenceAltitude <em>Reference Altitude</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractGroundStationWorldWindLayerImpl#isShowVisibilityCircle <em>Show Visibility Circle</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractGroundStationWorldWindLayerImpl#isShowVisibilityCone <em>Show Visibility Cone</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractGroundStationWorldWindLayerImpl#isShowOutline <em>Show Outline</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractGroundStationWorldWindLayerImpl extends EarthSurfaceLocationWorldWindLayerImpl implements AbstractGroundStationWorldWindLayer 
{
	/**
	 * The cached value of the '{@link #getGroundStation() <em>Ground Station</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroundStation()
	 * @generated
	 * @ordered
	 */
	protected GroundStation groundStation;

	private MultiEObjectsAdapter groundStationAdapter = null;
	
	/**
	 * The default value of the '{@link #getReferenceAltitude() <em>Reference Altitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceAltitude()
	 * @generated
	 * @ordered
	 */
	protected static final double REFERENCE_ALTITUDE_EDEFAULT = 500.0;

	/**
	 * The cached value of the '{@link #getReferenceAltitude() <em>Reference Altitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceAltitude()
	 * @generated
	 * @ordered
	 */
	protected double referenceAltitude = REFERENCE_ALTITUDE_EDEFAULT;

	/**
	 * The default value of the '{@link #isShowVisibilityCircle() <em>Show Visibility Circle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowVisibilityCircle()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SHOW_VISIBILITY_CIRCLE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isShowVisibilityCircle() <em>Show Visibility Circle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowVisibilityCircle()
	 * @generated
	 * @ordered
	 */
	protected boolean showVisibilityCircle = SHOW_VISIBILITY_CIRCLE_EDEFAULT;

	/**
	 * The default value of the '{@link #isShowVisibilityCone() <em>Show Visibility Cone</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowVisibilityCone()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SHOW_VISIBILITY_CONE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isShowVisibilityCone() <em>Show Visibility Cone</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowVisibilityCone()
	 * @generated
	 * @ordered
	 */
	protected boolean showVisibilityCone = SHOW_VISIBILITY_CONE_EDEFAULT;

	/**
	 * The default value of the '{@link #isShowOutline() <em>Show Outline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowOutline()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SHOW_OUTLINE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isShowOutline() <em>Show Outline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowOutline()
	 * @generated
	 * @ordered
	 */
	protected boolean showOutline = SHOW_OUTLINE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractGroundStationWorldWindLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreEnvironmentOrbitEarthUIPackage.Literals.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroundStation getGroundStation() {
		if (groundStation != null && groundStation.eIsProxy()) {
			InternalEObject oldGroundStation = (InternalEObject)groundStation;
			groundStation = (GroundStation)eResolveProxy(oldGroundStation);
			if (groundStation != oldGroundStation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION, oldGroundStation, groundStation));
			}
		}
		return groundStation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroundStation basicGetGroundStation() {
		return groundStation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setGroundStation(GroundStation newGroundStation) 
	{
		// Unregister from previous objects.
		getGroundStationAdapter().unregisterFromAllObjects();
		
		setGroundStationGen(newGroundStation);
		
		if(newGroundStation != null)
		{
			getGroundStationAdapter().registerToEObject(newGroundStation);			
		}
		
		if(isAutoUpdateEnabled()) 
		{
			try 
			{
				update();
			} 
			catch (Exception e) 
			{				
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroundStationGen(GroundStation newGroundStation) {
		GroundStation oldGroundStation = groundStation;
		groundStation = newGroundStation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION, oldGroundStation, groundStation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getReferenceAltitude() {
		return referenceAltitude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setReferenceAltitude(double newReferenceAltitude) 
	{
		setReferenceAltitudeGen(newReferenceAltitude);
		
		if(isAutoUpdateEnabled())
		{
			try 
			{
				update();
			} 
			catch (Exception e) {			
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceAltitudeGen(double newReferenceAltitude) {
		double oldReferenceAltitude = referenceAltitude;
		referenceAltitude = newReferenceAltitude;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE, oldReferenceAltitude, referenceAltitude));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isShowVisibilityCircle() {
		return showVisibilityCircle;
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setShowVisibilityCircle(boolean newShowVisibilityCircle) 
	{
		setShowVisibilityCircleGen(newShowVisibilityCircle);
		
		if(isAutoUpdateEnabled())
		{
			try 
			{
				update();
			} 
			catch (Exception e) {			
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowVisibilityCircleGen(boolean newShowVisibilityCircle) {
		boolean oldShowVisibilityCircle = showVisibilityCircle;
		showVisibilityCircle = newShowVisibilityCircle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE, oldShowVisibilityCircle, showVisibilityCircle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isShowVisibilityCone() {
		return showVisibilityCone;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setShowVisibilityCone(boolean newShowVisibilityCone) 
	{
		setShowVisibilityConeGen(newShowVisibilityCone);
		
		if(isAutoUpdateEnabled())
		{
			try 
			{
				update();
			} 
			catch (Exception e) {			
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowVisibilityConeGen(boolean newShowVisibilityCone) {
		boolean oldShowVisibilityCone = showVisibilityCone;
		showVisibilityCone = newShowVisibilityCone;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE, oldShowVisibilityCone, showVisibilityCone));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isShowOutline() {
		return showOutline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setShowOutline(boolean newShowOutline) 
	{
		setShowOutlineGen(newShowOutline);
		
		if(isAutoUpdateEnabled())
		{
			try 
			{
				update();
			} 
			catch (Exception e) {			
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowOutlineGen(boolean newShowOutline) {
		boolean oldShowOutline = showOutline;
		showOutline = newShowOutline;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE, oldShowOutline, showOutline));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION:
				if (resolve) return getGroundStation();
				return basicGetGroundStation();
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE:
				return getReferenceAltitude();
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE:
				return isShowVisibilityCircle();
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE:
				return isShowVisibilityCone();
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE:
				return isShowOutline();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION:
				setGroundStation((GroundStation)newValue);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE:
				setReferenceAltitude((Double)newValue);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE:
				setShowVisibilityCircle((Boolean)newValue);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE:
				setShowVisibilityCone((Boolean)newValue);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE:
				setShowOutline((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION:
				setGroundStation((GroundStation)null);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE:
				setReferenceAltitude(REFERENCE_ALTITUDE_EDEFAULT);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE:
				setShowVisibilityCircle(SHOW_VISIBILITY_CIRCLE_EDEFAULT);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE:
				setShowVisibilityCone(SHOW_VISIBILITY_CONE_EDEFAULT);
				return;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE:
				setShowOutline(SHOW_OUTLINE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION:
				return groundStation != null;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE:
				return referenceAltitude != REFERENCE_ALTITUDE_EDEFAULT;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE:
				return showVisibilityCircle != SHOW_VISIBILITY_CIRCLE_EDEFAULT;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE:
				return showVisibilityCone != SHOW_VISIBILITY_CONE_EDEFAULT;
			case ApogyCoreEnvironmentOrbitEarthUIPackage.ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE:
				return showOutline != SHOW_OUTLINE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (referenceAltitude: ");
		result.append(referenceAltitude);
		result.append(", showVisibilityCircle: ");
		result.append(showVisibilityCircle);
		result.append(", showVisibilityCone: ");
		result.append(showVisibilityCone);
		result.append(", showOutline: ");
		result.append(showOutline);
		result.append(')');
		return result.toString();
	}

	@Override
	public void setName(String newName) 
	{	
		super.setName(newName);
		
		if(isAutoUpdateEnabled())
		{
			try 
			{
				update();
			} 
			catch (Exception e) {			
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public boolean getDefaultAutoUpdateEnabled() 
	{
		return true;
	}
	
	@Override
	public EarthSurfaceLocation getEarthSurfaceLocation() 
	{
		return getGroundStation();
	}
	
	@Override
	public void dispose() 
	{		
		// Unregister from all objects.
		getGroundStationAdapter().unregisterFromAllObjects();
		
		super.dispose();
	}
		
	@Override
	protected void updateRenderableLayer() 
	{
		if(!isUpdating() && eContainer() != null)
		{
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, true, true);
			
			RenderableLayer layer = getRenderableLayer();
			layer.removeAllRenderables();
						
			if(isVisible() && !isDisposed() && getGroundStation() != null)
			{
				// Adds renderable from super;
				AbstractGroundStationWorldWindLayerImpl.super.addRenderable(layer);
				
				GeographicCoordinates coord = getGroundStation();
				
				Angle latitude = Angle.fromRadiansLatitude(coord.getLatitude());
		        Angle longitude = Angle.fromRadiansLongitude(coord.getLongitude());
		        double elevation = coord.getElevation();
		        Position position = new Position(latitude, longitude, elevation);
		        
		        // Adds the FOV.
		        if(getGroundStation().getElevationMask() instanceof ConstantElevationMask)
		        {	        	
		        	ConstantElevationMask cFOV = (ConstantElevationMask) getGroundStation().getElevationMask();
		        	
	        		// Gets the outer circle radius.
		        	double refAltitude = getReferenceAltitude() * 1000.0;
	        		double radius = computeGroundCircle(cFOV, refAltitude);
	        		        			        		
	        		BasicShapeAttributes fovAttributes = new BasicShapeAttributes();
	        		fovAttributes.setDrawInterior(true);
	        		fovAttributes.setOutlineOpacity(0.95);
	        		fovAttributes.setDrawOutline(isShowOutline());
	     	        
	     	        Material interiorMat = new Material(WorldWindUtils.convertFrom(getColor()));
	     	        fovAttributes.setInteriorMaterial(interiorMat);
	     	        fovAttributes.setDrawInterior(true);
	     	        fovAttributes.setInteriorOpacity(getOpacity());
	     	        
	     	        Material outlineMat = new Material(WorldWindUtils.convertFrom(getColor()));
	     	        fovAttributes.setOutlineMaterial(outlineMat);
	     	        
	     	        if(isShowVisibilityCircle())
	     	        {
	     	        	SurfaceCircle visibilityCircle = new SurfaceCircle(fovAttributes, position, radius, 36);	    
	     	        	visibilityCircle.setVisible(true);	   	     	        
	     	        	layer.addRenderable(visibilityCircle);	     	        	        			        			        	
	     	        }
	     	        
	     	        if(isShowVisibilityCone() && cFOV.getConstantElevation() > 0)
	     	        {
		     	        double h = getReferenceAltitude() * 1000;		     		     	        		     	        
		     	        double r = h / Math.tan(cFOV.getConstantElevation());
		     	        if(r > 0)
		     	        {
			     	        double northSouthRadius = r;
			     	        double verticalRadius   = h;
			     	        double eastWestRadius   = r;
			     	        
			     	        /*
			     	        System.out.println(" theta : " + Math.toDegrees(cFOV.getConstantElevation()));
			     	        System.out.println(" h : " + h);
			     	        System.out.println(" r : " + r);
			     	        */
			     	        
			     	        Angle heading = Angle.fromDegrees(0);
			     	        Angle tilt = Angle.fromDegrees(180);
			     	        Angle roll = Angle.fromDegrees(0);
			     	        
			     	        Cone cone = new Cone(new Position(latitude, longitude, elevation + verticalRadius), northSouthRadius, verticalRadius, eastWestRadius, heading, tilt, roll);
			     	        cone.setAttributes(fovAttributes);
			     	        layer.addRenderable(cone);	     	      
		     	        }  	    
	     	        }
		        }
			}
			
 	        getRenderableLayer().firePropertyChange(AVKey.LAYER, null, this);					
 	        ApogyCommonTransactionFacade.INSTANCE.basicSet(AbstractGroundStationWorldWindLayerImpl.this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, false, true);	
		}
	}
	
	protected double computeGroundCircle(ConstantElevationMask constantElevationMask, double range)
	{
		double radius = 0;
		
		double A = (Math.PI/2.0) + constantElevationMask.getConstantElevation();
		double a = Earth.WGS84_EQUATORIAL_RADIUS + range;
		double b = Earth.WGS84_EQUATORIAL_RADIUS;		
		double B = Math.asin(b / a * Math.sin(A));		
		double C = Math.PI - A - B;
		radius = C * Earth.WGS84_EQUATORIAL_RADIUS;		
						
		return radius;
	}

	protected MultiEObjectsAdapter getGroundStationAdapter() 
	{
		if(groundStationAdapter == null)
		{
			groundStationAdapter = new MultiEObjectsAdapter()
			{
				@Override
				public void registerToEObject(EObject eObject) 
				{
					if(eObject instanceof GroundStation)
					{
						GroundStation newGroundStation = (GroundStation) eObject;
						super.registerToEObject(newGroundStation);
						if(newGroundStation.getElevationMask() != null)
						{
							newGroundStation.getElevationMask().eAdapters().add(this);																		
						}
					}
					else
					{
						eObject.eAdapters().add(this);
					}
				}
				
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof GroundStation)
					{
						int featureId = msg.getFeatureID(GroundStation.class);
						switch (featureId) 
						{
							case ApogyCoreEnvironmentOrbitEarthPackage.GROUND_STATION__NAME:
								if(isAutoUpdateEnabled())
								{
									try 
									{
										update();
									}
									catch (Exception e) 
									{									
										e.printStackTrace();
									}
								}
							break;

							case ApogyCoreEnvironmentOrbitEarthPackage.GROUND_STATION__ELEVATION_MASK:
								
								if(msg.getOldValue() instanceof ElevationMask)
								{								
									((ElevationMask) msg.getOldValue()).eAdapters().remove(this);
								}
								
								if(msg.getNewValue() instanceof ElevationMask)
								{
									((ElevationMask) msg.getNewValue()).eAdapters().add(this);									
								}
								if(isAutoUpdateEnabled())
								{
									try {
										update();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							break;
							
							case ApogyEarthEnvironmentPackage.EARTH_SURFACE_LOCATION__ELEVATION:
							case ApogyEarthEnvironmentPackage.EARTH_SURFACE_LOCATION__LATITUDE:
							case ApogyEarthEnvironmentPackage.EARTH_SURFACE_LOCATION__LONGITUDE:								
								if(isAutoUpdateEnabled()) 
								{
									try {
										update();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							break;
							
							default:
							break;
						}
					}
					else if(msg.getNotifier() instanceof ElevationMask)
					{
						if(isAutoUpdateEnabled()) 
						{
							try {
								update();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					else if(msg.getNotifier() instanceof GeographicCoordinates)
					{
						if(isAutoUpdateEnabled())
						{
							try {
								update();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					else if(msg.getNotifier() instanceof DistanceRange)
					{
						if(isAutoUpdateEnabled())
						{
							try {
								update();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					else if(msg.getNotifier() instanceof ConicalFieldOfView)
					{
						int featureId = msg.getFeatureID(ConicalFieldOfView.class);
						
						switch (featureId) 
						{
							case ApogyAddonsSensorsFOVPackage.CONICAL_FIELD_OF_VIEW__RANGE:							
								if(msg.getOldValue() instanceof DistanceRange)
								{
									getGroundStationAdapter().unregisterFromEObject((DistanceRange) msg.getOldValue());
								}
								
								if(msg.getNewValue() instanceof DistanceRange)
								{
									getGroundStationAdapter().registerToEObject((DistanceRange) msg.getNewValue());
								}
								
								if(isAutoUpdateEnabled())
								{
									try {
										update();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								
							break;

							default:
								if(isAutoUpdateEnabled())
								{
									try {
										update();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							break;
						}
					}
					else
					{
						if(isAutoUpdateEnabled())
						{
							try {
								update();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
			};
		}
		return groundStationAdapter;
	}
	
} //AbstractGroundStationWorldWindLayerImpl

/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthFactory;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ConstantElevationMask;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.GroundStation;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIFactory;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ContainedGroundStationWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ContainedGroundStationWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.wizards.ConstantElevationMaskWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Contained Ground Station World Wind Layer Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ContainedGroundStationWorldWindLayerWizardPagesProviderImpl extends EarthSurfaceLocationWorldWindLayerWizardPagesProviderImpl implements ContainedGroundStationWorldWindLayerWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContainedGroundStationWorldWindLayerWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreEnvironmentOrbitEarthUIPackage.Literals.CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		ContainedGroundStationWorldWindLayer layer = ApogyCoreEnvironmentOrbitEarthUIFactory.eINSTANCE.createContainedGroundStationWorldWindLayer();
		
		// Initialize Ground Station to CSA location.
		GroundStation groundStation = ApogyCoreEnvironmentOrbitEarthFactory.eINSTANCE.createGroundStation();
		groundStation.setElevation(30.0);
		groundStation.setLatitude(Math.toRadians(45.518206644445));
		groundStation.setLongitude(Math.toRadians(-73.393904468182));
		
		// Initialize the elevation mask.
		ConstantElevationMask constantElevationMask = ApogyCoreEnvironmentOrbitEarthFactory.eINSTANCE.createConstantElevationMask();
		constantElevationMask.setConstantElevation(Math.toRadians(45));		
		groundStation.setElevationMask(constantElevationMask);
		
		layer.setContainedGroundStation(groundStation);		
				
		return layer;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));
		
		ContainedGroundStationWorldWindLayer layer = (ContainedGroundStationWorldWindLayer) eObject;
		
		if(layer.getGroundStation() != null && layer.getGroundStation().getElevationMask() instanceof ConstantElevationMask)
		{		
			ConstantElevationMask constantElevationMask = (ConstantElevationMask) layer.getGroundStation().getElevationMask();
			
			ConstantElevationMaskWizardPage constantElevationMaskWizardPage = new ConstantElevationMaskWizardPage(constantElevationMask);
			list.add(constantElevationMaskWizardPage);
		}
		
		return list;
	}
} //ContainedGroundStationWorldWindLayerWizardPagesProviderImpl

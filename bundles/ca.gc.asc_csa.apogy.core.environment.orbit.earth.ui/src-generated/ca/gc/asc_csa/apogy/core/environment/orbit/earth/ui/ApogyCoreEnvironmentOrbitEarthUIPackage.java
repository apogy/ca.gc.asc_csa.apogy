package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc --> <!-- begin-model-doc --> Copyright (c) 2016 Canadian
 * Space Agency (CSA) / Agence spatiale canadienne (ASC). All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Pierre Allard (Pierre.Allard@canada.ca), Regent L'Archeveque
 * (Regent.Larcheveque@canada.ca), Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * Canadian Space Agency (CSA) - Initial API and implementation <!--
 * end-model-doc -->
 * 
 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIFactory
 * @model kind="package" annotation="http://www.eclipse.org/emf/2002/GenModel
 *        prefix='ApogyCoreEnvironmentOrbitEarthUI'
 *        childCreationExtenders='true' extensibleProviderFactory='true'
 *        multipleEditorPages='false' copyrightText='Copyright (c) 2015-2016
 *        Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll
 *        rights reserved. This program and the accompanying materials\nare made
 *        available under the terms of the Eclipse Public License v1.0\nwhich
 *        accompanies this distribution, and is available
 *        at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n
 *        Pierre Allard (Pierre.Allard@canada.ca), \n Regent L\'Archeveque
 *        (Regent.Larcheveque@canada.ca),\n Sebastien Gemme
 *        (Sebastien.Gemme@canada.ca),\n Canadian Space Agency (CSA) - Initial
 *        API and implementation' modelName='ApogyCoreEnvironmentOrbitEarthUI'
 *        complianceLevel='8.0' suppressGenModelAnnotations='false'
 *        dynamicTemplates='true'
 *        templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates'
 *        modelDirectory='/ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui/src-generated'
 *        editDirectory='/ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.edit/src-generated'
 *        basePackage='ca.gc.asc_csa.apogy.core.environment.orbit.earth'"
 * @generated
 */
public interface ApogyCoreEnvironmentOrbitEarthUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	ApogyCoreEnvironmentOrbitEarthUIPackage eINSTANCE = ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl.init();

	/**
	 * The meta object id for the
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractSpacecraftLocationWorldWindLayerImpl
	 * <em>Abstract Spacecraft Location World Wind Layer</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractSpacecraftLocationWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getAbstractSpacecraftLocationWorldWindLayer()
	 * @generated
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER = 0;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__UPDATING = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__NAME = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__DESCRIPTION = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__DISPOSED = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__VISIBLE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__BLINKING = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__RENDERABLE_LAYER = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__WORLD_WINDOW = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Orbit Model</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__ORBIT_MODEL = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Time Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__TIME_SOURCE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Show Ground Projection</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_GROUND_PROJECTION = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Show Lat Lon</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_LAT_LON = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Abstract Spacecraft Location World Wind Layer</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER_FEATURE_COUNT = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER___UPDATE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER___INITIALISE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER___DISPOSE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The number of operations of the '<em>Abstract Spacecraft Location World Wind Layer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER_OPERATION_COUNT = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftLocationWorldWindLayerImpl
	 * <em>Spacecraft Location World Wind Layer</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftLocationWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getSpacecraftLocationWorldWindLayer()
	 * @generated
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER = 1;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__UPDATING = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__NAME = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__DESCRIPTION = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__DISPOSED = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__VISIBLE = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__BLINKING = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__RENDERABLE_LAYER = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__WORLD_WINDOW = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Orbit Model</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__ORBIT_MODEL = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__ORBIT_MODEL;

	/**
	 * The feature id for the '<em><b>Time Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__TIME_SOURCE = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__TIME_SOURCE;

	/**
	 * The feature id for the '<em><b>Show Ground Projection</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_GROUND_PROJECTION = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_GROUND_PROJECTION;

	/**
	 * The feature id for the '<em><b>Show Lat Lon</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_LAT_LON = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_LAT_LON;

	/**
	 * The feature id for the '<em><b>Refered Orbit Model</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER__REFERED_ORBIT_MODEL = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Spacecraft Location World Wind Layer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER_FEATURE_COUNT = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER___UPDATE = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER___INITIALISE = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER___DISPOSE = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The number of operations of the '<em>Spacecraft Location World Wind Layer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_LOCATION_WORLD_WIND_LAYER_OPERATION_COUNT = ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.OrbitModelWorldWindLayerImpl <em>Orbit Model World Wind Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.OrbitModelWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getOrbitModelWorldWindLayer()
	 * @generated
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER = 2;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__UPDATING = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__NAME = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__DESCRIPTION = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__DISPOSED = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__VISIBLE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__BLINKING = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__RENDERABLE_LAYER = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__WORLD_WINDOW = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Orbit Model</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__ORBIT_MODEL = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Time Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__TIME_SOURCE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Time Interval</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__TIME_INTERVAL = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Forward Propagation Duration</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__FORWARD_PROPAGATION_DURATION = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Backward Propagation Duration</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__BACKWARD_PROPAGATION_DURATION = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Show Ground Trace</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__SHOW_GROUND_TRACE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Show Orbit</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER__SHOW_ORBIT = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Orbit Model World Wind Layer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER_FEATURE_COUNT = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER___UPDATE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER___INITIALISE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER___DISPOSE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The number of operations of the '<em>Orbit Model World Wind Layer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORBIT_MODEL_WORLD_WIND_LAYER_OPERATION_COUNT = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftSwathWorldWindLayerImpl <em>Spacecraft Swath World Wind Layer</em>}' class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftSwathWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getSpacecraftSwathWorldWindLayer()
	 * @generated
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER = 3;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__UPDATING = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__NAME = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__DESCRIPTION = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__DISPOSED = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__VISIBLE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__BLINKING = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__RENDERABLE_LAYER = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__WORLD_WINDOW = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Orbit Model</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__ORBIT_MODEL = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Time Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__TIME_SOURCE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Forward Propagation Duration</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__FORWARD_PROPAGATION_DURATION = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Backward Propagation Duration</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__BACKWARD_PROPAGATION_DURATION = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Time Interval</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__TIME_INTERVAL = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Left Swath Angle</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__LEFT_SWATH_ANGLE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Right Swath Angle</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__RIGHT_SWATH_ANGLE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Show Ground Trace</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__SHOW_GROUND_TRACE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Forward Spacecraft Swath Corridor</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__FORWARD_SPACECRAFT_SWATH_CORRIDOR = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Backward Spacecraft Swath Corridor</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER__BACKWARD_SPACECRAFT_SWATH_CORRIDOR = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>Spacecraft Swath World Wind Layer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER_FEATURE_COUNT = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 10;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER___UPDATE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER___INITIALISE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER___DISPOSE = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The number of operations of the '<em>Spacecraft Swath World Wind Layer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_SWATH_WORLD_WIND_LAYER_OPERATION_COUNT = ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractGroundStationWorldWindLayerImpl
	 * <em>Abstract Ground Station World Wind Layer</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractGroundStationWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getAbstractGroundStationWorldWindLayer()
	 * @generated
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER = 4;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__UPDATING = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__NAME = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DESCRIPTION = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DISPOSED = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__VISIBLE = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__BLINKING = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__RENDERABLE_LAYER = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__WORLD_WINDOW = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Earth Surface Location</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION;

	/**
	 * The feature id for the '<em><b>Target Radius</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__TARGET_RADIUS = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__TARGET_RADIUS;

	/**
	 * The feature id for the '<em><b>Display Balloon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DISPLAY_BALLOON = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DISPLAY_BALLOON;

	/**
	 * The feature id for the '<em><b>Display Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DISPLAY_LOCATION = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DISPLAY_LOCATION;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__COLOR = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__COLOR;

	/**
	 * The feature id for the '<em><b>Opacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__OPACITY = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__OPACITY;

	/**
	 * The feature id for the '<em><b>Ground Station</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reference Altitude</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Show Visibility Circle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Show Visibility Cone</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Show Outline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Abstract Ground Station
	 * World Wind Layer</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER_FEATURE_COUNT = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___UPDATE = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___INITIALISE = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___DISPOSE = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The number of operations of the '<em>Abstract Ground Station World Wind Layer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER_OPERATION_COUNT = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.GroundStationWorldWindLayerImpl <em>Ground Station World Wind Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.GroundStationWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getGroundStationWorldWindLayer()
	 * @generated
	 */
	int GROUND_STATION_WORLD_WIND_LAYER = 5;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__UPDATING = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__NAME = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__DESCRIPTION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__DISPOSED = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__VISIBLE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__BLINKING = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__RENDERABLE_LAYER = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__WORLD_WINDOW = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Earth Surface Location</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION;

	/**
	 * The feature id for the '<em><b>Target Radius</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__TARGET_RADIUS = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__TARGET_RADIUS;

	/**
	 * The feature id for the '<em><b>Display Balloon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__DISPLAY_BALLOON = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DISPLAY_BALLOON;

	/**
	 * The feature id for the '<em><b>Display Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__DISPLAY_LOCATION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DISPLAY_LOCATION;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__COLOR = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__COLOR;

	/**
	 * The feature id for the '<em><b>Opacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__OPACITY = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__OPACITY;

	/**
	 * The feature id for the '<em><b>Ground Station</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION;

	/**
	 * The feature id for the '<em><b>Reference Altitude</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE;

	/**
	 * The feature id for the '<em><b>Show Visibility Circle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE;

	/**
	 * The feature id for the '<em><b>Show Visibility Cone</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE;

	/**
	 * The feature id for the '<em><b>Show Outline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE;

	/**
	 * The feature id for the '<em><b>Refered Ground Station</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER__REFERED_GROUND_STATION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ground Station World Wind Layer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_FEATURE_COUNT = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER___UPDATE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER___INITIALISE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER___DISPOSE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The number of operations of the '<em>Ground Station World Wind Layer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_OPERATION_COUNT = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ContainedGroundStationWorldWindLayerImpl <em>Contained Ground Station World Wind Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ContainedGroundStationWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getContainedGroundStationWorldWindLayer()
	 * @generated
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER = 6;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__UPDATING = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__NAME = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__DESCRIPTION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__DISPOSED = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__VISIBLE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__BLINKING = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__RENDERABLE_LAYER = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__WORLD_WINDOW = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Earth Surface Location</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION;

	/**
	 * The feature id for the '<em><b>Target Radius</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__TARGET_RADIUS = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__TARGET_RADIUS;

	/**
	 * The feature id for the '<em><b>Display Balloon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__DISPLAY_BALLOON = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DISPLAY_BALLOON;

	/**
	 * The feature id for the '<em><b>Display Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__DISPLAY_LOCATION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__DISPLAY_LOCATION;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__COLOR = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__COLOR;

	/**
	 * The feature id for the '<em><b>Opacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__OPACITY = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__OPACITY;

	/**
	 * The feature id for the '<em><b>Ground Station</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION;

	/**
	 * The feature id for the '<em><b>Reference Altitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE;

	/**
	 * The feature id for the '<em><b>Show Visibility Circle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE;

	/**
	 * The feature id for the '<em><b>Show Visibility Cone</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE;

	/**
	 * The feature id for the '<em><b>Show Outline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE;

	/**
	 * The feature id for the '<em><b>Contained Ground Station</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__CONTAINED_GROUND_STATION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Contained Ground Station World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_FEATURE_COUNT = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER___UPDATE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER___INITIALISE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER___DISPOSE = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The number of operations of the '<em>Contained Ground Station World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_OPERATION_COUNT = ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftVisibilityPassViewConfigurationListImpl
	 * <em>Spacecraft Visibility Pass View Configuration List</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftVisibilityPassViewConfigurationListImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getSpacecraftVisibilityPassViewConfigurationList()
	 * @generated
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION_LIST = 7;

	/**
	 * The feature id for the '<em><b>Tools List</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION_LIST__TOOLS_LIST = ApogyCoreInvocatorPackage.ABSTRACT_TOOLS_LIST_CONTAINER__TOOLS_LIST;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION_LIST__CONFIGURATIONS = ApogyCoreInvocatorPackage.ABSTRACT_TOOLS_LIST_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Spacecraft Visibility Pass View Configuration List</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION_LIST_FEATURE_COUNT = ApogyCoreInvocatorPackage.ABSTRACT_TOOLS_LIST_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Spacecraft Visibility Pass View
	 * Configuration List</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION_LIST_OPERATION_COUNT = ApogyCoreInvocatorPackage.ABSTRACT_TOOLS_LIST_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftVisibilityPassViewConfigurationImpl
	 * <em>Spacecraft Visibility Pass View Configuration</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftVisibilityPassViewConfigurationImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getSpacecraftVisibilityPassViewConfiguration()
	 * @generated
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION__NAME = ApogyCommonEMFPackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION__DESCRIPTION = ApogyCommonEMFPackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Configurations List</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION__CONFIGURATIONS_LIST = ApogyCommonEMFPackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Visibility Set</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION__VISIBILITY_SET = ApogyCommonEMFPackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Spacecraft Visibility Pass
	 * View Configuration</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION_FEATURE_COUNT = ApogyCommonEMFPackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Spacecraft Visibility Pass View Configuration</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION_OPERATION_COUNT = ApogyCommonEMFPackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.EarthViewUtilitiesImpl <em>Earth View Utilities</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.EarthViewUtilitiesImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getEarthViewUtilities()
	 * @generated
	 */
	int EARTH_VIEW_UTILITIES = 9;

	/**
	 * The number of structural features of the '<em>Earth View Utilities</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_UTILITIES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Camera View Configuration Identifier</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_UTILITIES___GET_CAMERA_VIEW_CONFIGURATION_IDENTIFIER__EARTHVIEWCONFIGURATION = 0;

	/**
	 * The operation id for the '<em>Get Active Earth View Configuration</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_UTILITIES___GET_ACTIVE_EARTH_VIEW_CONFIGURATION__STRING = 1;

	/**
	 * The operation id for the '<em>Get Active Earth View Configuration List</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_UTILITIES___GET_ACTIVE_EARTH_VIEW_CONFIGURATION_LIST = 2;

	/**
	 * The number of operations of the '<em>Earth View Utilities</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_UTILITIES_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.GroundStationWorldWindLayerWizardPagesProviderImpl <em>Ground Station World Wind Layer Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.GroundStationWorldWindLayerWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getGroundStationWorldWindLayerWizardPagesProvider()
	 * @generated
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = 10;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Ground Station World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Ground Station World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ContainedGroundStationWorldWindLayerWizardPagesProviderImpl <em>Contained Ground Station World Wind Layer Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ContainedGroundStationWorldWindLayerWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getContainedGroundStationWorldWindLayerWizardPagesProvider()
	 * @generated
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = 11;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Contained Ground Station World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Contained Ground Station World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '<em>Renderable Layer</em>' data type. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see gov.nasa.worldwind.layers.RenderableLayer
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getRenderableLayer()
	 * @generated
	 */
	int RENDERABLE_LAYER = 12;

	/**
	 * The meta object id for the '<em>Map</em>' data type.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see java.util.Map
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getMap()
	 * @generated
	 */
	int MAP = 13;

	/**
	 * The meta object id for the '<em>ISelection</em>' data type. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.jface.viewers.ISelection
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getISelection()
	 * @generated
	 */
	int ISELECTION = 14;

	/**
	 * Returns the meta object for class
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractSpacecraftLocationWorldWindLayer
	 * <em>Abstract Spacecraft Location World Wind Layer</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Abstract Spacecraft Location World
	 *         Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractSpacecraftLocationWorldWindLayer
	 * @generated
	 */
	EClass getAbstractSpacecraftLocationWorldWindLayer();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractSpacecraftLocationWorldWindLayer#getOrbitModel <em>Orbit Model</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Orbit Model</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractSpacecraftLocationWorldWindLayer#getOrbitModel()
	 * @see #getAbstractSpacecraftLocationWorldWindLayer()
	 * @generated
	 */
	EReference getAbstractSpacecraftLocationWorldWindLayer_OrbitModel();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractSpacecraftLocationWorldWindLayer#getTimeSource <em>Time Source</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Time Source</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractSpacecraftLocationWorldWindLayer#getTimeSource()
	 * @see #getAbstractSpacecraftLocationWorldWindLayer()
	 * @generated
	 */
	EReference getAbstractSpacecraftLocationWorldWindLayer_TimeSource();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractSpacecraftLocationWorldWindLayer#isShowGroundProjection <em>Show Ground Projection</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Ground Projection</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractSpacecraftLocationWorldWindLayer#isShowGroundProjection()
	 * @see #getAbstractSpacecraftLocationWorldWindLayer()
	 * @generated
	 */
	EAttribute getAbstractSpacecraftLocationWorldWindLayer_ShowGroundProjection();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractSpacecraftLocationWorldWindLayer#isShowLatLon <em>Show Lat Lon</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Lat Lon</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractSpacecraftLocationWorldWindLayer#isShowLatLon()
	 * @see #getAbstractSpacecraftLocationWorldWindLayer()
	 * @generated
	 */
	EAttribute getAbstractSpacecraftLocationWorldWindLayer_ShowLatLon();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftLocationWorldWindLayer <em>Spacecraft Location World Wind Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Spacecraft Location World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftLocationWorldWindLayer
	 * @generated
	 */
	EClass getSpacecraftLocationWorldWindLayer();

	/**
	 * Returns the meta object for the reference
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftLocationWorldWindLayer#getReferedOrbitModel
	 * <em>Refered Orbit Model</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Refered Orbit Model</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftLocationWorldWindLayer#getReferedOrbitModel()
	 * @see #getSpacecraftLocationWorldWindLayer()
	 * @generated
	 */
	EReference getSpacecraftLocationWorldWindLayer_ReferedOrbitModel();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer <em>Orbit Model World Wind Layer</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Orbit Model World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer
	 * @generated
	 */
	EClass getOrbitModelWorldWindLayer();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#getOrbitModel <em>Orbit Model</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Orbit Model</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#getOrbitModel()
	 * @see #getOrbitModelWorldWindLayer()
	 * @generated
	 */
	EReference getOrbitModelWorldWindLayer_OrbitModel();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#getTimeSource <em>Time Source</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Time Source</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#getTimeSource()
	 * @see #getOrbitModelWorldWindLayer()
	 * @generated
	 */
	EReference getOrbitModelWorldWindLayer_TimeSource();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#getTimeInterval <em>Time Interval</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Interval</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#getTimeInterval()
	 * @see #getOrbitModelWorldWindLayer()
	 * @generated
	 */
	EAttribute getOrbitModelWorldWindLayer_TimeInterval();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#getForwardPropagationDuration <em>Forward Propagation Duration</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Forward Propagation Duration</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#getForwardPropagationDuration()
	 * @see #getOrbitModelWorldWindLayer()
	 * @generated
	 */
	EAttribute getOrbitModelWorldWindLayer_ForwardPropagationDuration();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#getBackwardPropagationDuration <em>Backward Propagation Duration</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Backward Propagation Duration</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#getBackwardPropagationDuration()
	 * @see #getOrbitModelWorldWindLayer()
	 * @generated
	 */
	EAttribute getOrbitModelWorldWindLayer_BackwardPropagationDuration();

	/**
	 * Returns the meta object for the attribute
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#isShowGroundTrace
	 * <em>Show Ground Trace</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Show Ground Trace</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#isShowGroundTrace()
	 * @see #getOrbitModelWorldWindLayer()
	 * @generated
	 */
	EAttribute getOrbitModelWorldWindLayer_ShowGroundTrace();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#isShowOrbit <em>Show Orbit</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Orbit</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.OrbitModelWorldWindLayer#isShowOrbit()
	 * @see #getOrbitModelWorldWindLayer()
	 * @generated
	 */
	EAttribute getOrbitModelWorldWindLayer_ShowOrbit();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer <em>Spacecraft Swath World Wind Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Spacecraft Swath World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer
	 * @generated
	 */
	EClass getSpacecraftSwathWorldWindLayer();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getOrbitModel <em>Orbit Model</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Orbit Model</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getOrbitModel()
	 * @see #getSpacecraftSwathWorldWindLayer()
	 * @generated
	 */
	EReference getSpacecraftSwathWorldWindLayer_OrbitModel();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getTimeSource <em>Time Source</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Time Source</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getTimeSource()
	 * @see #getSpacecraftSwathWorldWindLayer()
	 * @generated
	 */
	EReference getSpacecraftSwathWorldWindLayer_TimeSource();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getForwardPropagationDuration <em>Forward Propagation Duration</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Forward Propagation Duration</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getForwardPropagationDuration()
	 * @see #getSpacecraftSwathWorldWindLayer()
	 * @generated
	 */
	EAttribute getSpacecraftSwathWorldWindLayer_ForwardPropagationDuration();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getBackwardPropagationDuration <em>Backward Propagation Duration</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Backward Propagation Duration</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getBackwardPropagationDuration()
	 * @see #getSpacecraftSwathWorldWindLayer()
	 * @generated
	 */
	EAttribute getSpacecraftSwathWorldWindLayer_BackwardPropagationDuration();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getTimeInterval <em>Time Interval</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Interval</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getTimeInterval()
	 * @see #getSpacecraftSwathWorldWindLayer()
	 * @generated
	 */
	EAttribute getSpacecraftSwathWorldWindLayer_TimeInterval();

	/**
	 * Returns the meta object for the attribute
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getLeftSwathAngle
	 * <em>Left Swath Angle</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Left Swath Angle</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getLeftSwathAngle()
	 * @see #getSpacecraftSwathWorldWindLayer()
	 * @generated
	 */
	EAttribute getSpacecraftSwathWorldWindLayer_LeftSwathAngle();

	/**
	 * Returns the meta object for the attribute
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getRightSwathAngle
	 * <em>Right Swath Angle</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Right Swath Angle</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getRightSwathAngle()
	 * @see #getSpacecraftSwathWorldWindLayer()
	 * @generated
	 */
	EAttribute getSpacecraftSwathWorldWindLayer_RightSwathAngle();

	/**
	 * Returns the meta object for the attribute
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#isShowGroundTrace
	 * <em>Show Ground Trace</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Show Ground Trace</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#isShowGroundTrace()
	 * @see #getSpacecraftSwathWorldWindLayer()
	 * @generated
	 */
	EAttribute getSpacecraftSwathWorldWindLayer_ShowGroundTrace();

	/**
	 * Returns the meta object for the containment reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getForwardSpacecraftSwathCorridor <em>Forward Spacecraft Swath Corridor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Forward Spacecraft Swath Corridor</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getForwardSpacecraftSwathCorridor()
	 * @see #getSpacecraftSwathWorldWindLayer()
	 * @generated
	 */
	EReference getSpacecraftSwathWorldWindLayer_ForwardSpacecraftSwathCorridor();

	/**
	 * Returns the meta object for the containment reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getBackwardSpacecraftSwathCorridor <em>Backward Spacecraft Swath Corridor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Backward Spacecraft Swath Corridor</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftSwathWorldWindLayer#getBackwardSpacecraftSwathCorridor()
	 * @see #getSpacecraftSwathWorldWindLayer()
	 * @generated
	 */
	EReference getSpacecraftSwathWorldWindLayer_BackwardSpacecraftSwathCorridor();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer <em>Abstract Ground Station World Wind Layer</em>}'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Ground Station World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer
	 * @generated
	 */
	EClass getAbstractGroundStationWorldWindLayer();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#getGroundStation <em>Ground Station</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ground Station</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#getGroundStation()
	 * @see #getAbstractGroundStationWorldWindLayer()
	 * @generated
	 */
	EReference getAbstractGroundStationWorldWindLayer_GroundStation();

	/**
	 * Returns the meta object for the attribute
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#getReferenceAltitude
	 * <em>Reference Altitude</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Reference Altitude</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#getReferenceAltitude()
	 * @see #getAbstractGroundStationWorldWindLayer()
	 * @generated
	 */
	EAttribute getAbstractGroundStationWorldWindLayer_ReferenceAltitude();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowVisibilityCircle <em>Show Visibility Circle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Visibility Circle</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowVisibilityCircle()
	 * @see #getAbstractGroundStationWorldWindLayer()
	 * @generated
	 */
	EAttribute getAbstractGroundStationWorldWindLayer_ShowVisibilityCircle();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowVisibilityCone <em>Show Visibility Cone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Visibility Cone</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowVisibilityCone()
	 * @see #getAbstractGroundStationWorldWindLayer()
	 * @generated
	 */
	EAttribute getAbstractGroundStationWorldWindLayer_ShowVisibilityCone();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowOutline <em>Show Outline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Outline</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.AbstractGroundStationWorldWindLayer#isShowOutline()
	 * @see #getAbstractGroundStationWorldWindLayer()
	 * @generated
	 */
	EAttribute getAbstractGroundStationWorldWindLayer_ShowOutline();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.GroundStationWorldWindLayer <em>Ground Station World Wind Layer</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Ground Station World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.GroundStationWorldWindLayer
	 * @generated
	 */
	EClass getGroundStationWorldWindLayer();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.GroundStationWorldWindLayer#getReferedGroundStation <em>Refered Ground Station</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference '<em>Refered Ground Station</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.GroundStationWorldWindLayer#getReferedGroundStation()
	 * @see #getGroundStationWorldWindLayer()
	 * @generated
	 */
	EReference getGroundStationWorldWindLayer_ReferedGroundStation();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ContainedGroundStationWorldWindLayer <em>Contained Ground Station World Wind Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contained Ground Station World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ContainedGroundStationWorldWindLayer
	 * @generated
	 */
	EClass getContainedGroundStationWorldWindLayer();

	/**
	 * Returns the meta object for the containment reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ContainedGroundStationWorldWindLayer#getContainedGroundStation <em>Contained Ground Station</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Contained Ground Station</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ContainedGroundStationWorldWindLayer#getContainedGroundStation()
	 * @see #getContainedGroundStationWorldWindLayer()
	 * @generated
	 */
	EReference getContainedGroundStationWorldWindLayer_ContainedGroundStation();

	/**
	 * Returns the meta object for class
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftVisibilityPassViewConfigurationList
	 * <em>Spacecraft Visibility Pass View Configuration List</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Spacecraft Visibility Pass View
	 *         Configuration List</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftVisibilityPassViewConfigurationList
	 * @generated
	 */
	EClass getSpacecraftVisibilityPassViewConfigurationList();

	/**
	 * Returns the meta object for the containment reference list '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftVisibilityPassViewConfigurationList#getConfigurations <em>Configurations</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Configurations</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftVisibilityPassViewConfigurationList#getConfigurations()
	 * @see #getSpacecraftVisibilityPassViewConfigurationList()
	 * @generated
	 */
	EReference getSpacecraftVisibilityPassViewConfigurationList_Configurations();

	/**
	 * Returns the meta object for class
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftVisibilityPassViewConfiguration
	 * <em>Spacecraft Visibility Pass View Configuration</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Spacecraft Visibility Pass View
	 *         Configuration</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftVisibilityPassViewConfiguration
	 * @generated
	 */
	EClass getSpacecraftVisibilityPassViewConfiguration();

	/**
	 * Returns the meta object for the container reference
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftVisibilityPassViewConfiguration#getConfigurationsList
	 * <em>Configurations List</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the container reference '<em>Configurations
	 *         List</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftVisibilityPassViewConfiguration#getConfigurationsList()
	 * @see #getSpacecraftVisibilityPassViewConfiguration()
	 * @generated
	 */
	EReference getSpacecraftVisibilityPassViewConfiguration_ConfigurationsList();

	/**
	 * Returns the meta object for the containment reference '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftVisibilityPassViewConfiguration#getVisibilitySet <em>Visibility Set</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Visibility Set</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftVisibilityPassViewConfiguration#getVisibilitySet()
	 * @see #getSpacecraftVisibilityPassViewConfiguration()
	 * @generated
	 */
	EReference getSpacecraftVisibilityPassViewConfiguration_VisibilitySet();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.EarthViewUtilities <em>Earth View Utilities</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Earth View Utilities</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.EarthViewUtilities
	 * @generated
	 */
	EClass getEarthViewUtilities();

	/**
	 * Returns the meta object for the
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.EarthViewUtilities#getCameraViewConfigurationIdentifier(ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration)
	 * <em>Get Camera View Configuration Identifier</em>}' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Get Camera View Configuration
	 *         Identifier</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.EarthViewUtilities#getCameraViewConfigurationIdentifier(ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration)
	 * @generated
	 */
	EOperation getEarthViewUtilities__GetCameraViewConfigurationIdentifier__EarthViewConfiguration();

	/**
	 * Returns the meta object for the
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.EarthViewUtilities#getActiveEarthViewConfiguration(java.lang.String)
	 * <em>Get Active Earth View Configuration</em>}' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Get Active Earth View
	 *         Configuration</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.EarthViewUtilities#getActiveEarthViewConfiguration(java.lang.String)
	 * @generated
	 */
	EOperation getEarthViewUtilities__GetActiveEarthViewConfiguration__String();

	/**
	 * Returns the meta object for the
	 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.EarthViewUtilities#getActiveEarthViewConfigurationList()
	 * <em>Get Active Earth View Configuration List</em>}' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Get Active Earth View Configuration
	 *         List</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.EarthViewUtilities#getActiveEarthViewConfigurationList()
	 * @generated
	 */
	EOperation getEarthViewUtilities__GetActiveEarthViewConfigurationList();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.GroundStationWorldWindLayerWizardPagesProvider <em>Ground Station World Wind Layer Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ground Station World Wind Layer Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.GroundStationWorldWindLayerWizardPagesProvider
	 * @generated
	 */
	EClass getGroundStationWorldWindLayerWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ContainedGroundStationWorldWindLayerWizardPagesProvider <em>Contained Ground Station World Wind Layer Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contained Ground Station World Wind Layer Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ContainedGroundStationWorldWindLayerWizardPagesProvider
	 * @generated
	 */
	EClass getContainedGroundStationWorldWindLayerWizardPagesProvider();

	/**
	 * Returns the meta object for data type '{@link gov.nasa.worldwind.layers.RenderableLayer <em>Renderable Layer</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Renderable Layer</em>'.
	 * @see gov.nasa.worldwind.layers.RenderableLayer
	 * @model instanceClass="gov.nasa.worldwind.layers.RenderableLayer"
	 * @generated
	 */
	EDataType getRenderableLayer();

	/**
	 * Returns the meta object for data type '{@link java.util.Map <em>Map</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Map</em>'.
	 * @see java.util.Map
	 * @model instanceClass="java.util.Map" typeParameters="K V"
	 * @generated
	 */
	EDataType getMap();

	/**
	 * Returns the meta object for data type
	 * '{@link org.eclipse.jface.viewers.ISelection <em>ISelection</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for data type '<em>ISelection</em>'.
	 * @see org.eclipse.jface.viewers.ISelection
	 * @model instanceClass="org.eclipse.jface.viewers.ISelection"
	 * @generated
	 */
	EDataType getISelection();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCoreEnvironmentOrbitEarthUIFactory getApogyCoreEnvironmentOrbitEarthUIFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each operation of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the
		 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractSpacecraftLocationWorldWindLayerImpl
		 * <em>Abstract Spacecraft Location World Wind Layer</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractSpacecraftLocationWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getAbstractSpacecraftLocationWorldWindLayer()
		 * @generated
		 */
		EClass ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER = eINSTANCE.getAbstractSpacecraftLocationWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Orbit Model</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__ORBIT_MODEL = eINSTANCE.getAbstractSpacecraftLocationWorldWindLayer_OrbitModel();

		/**
		 * The meta object literal for the '<em><b>Time Source</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__TIME_SOURCE = eINSTANCE.getAbstractSpacecraftLocationWorldWindLayer_TimeSource();

		/**
		 * The meta object literal for the '<em><b>Show Ground Projection</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_GROUND_PROJECTION = eINSTANCE.getAbstractSpacecraftLocationWorldWindLayer_ShowGroundProjection();

		/**
		 * The meta object literal for the '<em><b>Show Lat Lon</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_SPACECRAFT_LOCATION_WORLD_WIND_LAYER__SHOW_LAT_LON = eINSTANCE.getAbstractSpacecraftLocationWorldWindLayer_ShowLatLon();

		/**
		 * The meta object literal for the
		 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftLocationWorldWindLayerImpl
		 * <em>Spacecraft Location World Wind Layer</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftLocationWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getSpacecraftLocationWorldWindLayer()
		 * @generated
		 */
		EClass SPACECRAFT_LOCATION_WORLD_WIND_LAYER = eINSTANCE.getSpacecraftLocationWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Refered Orbit Model</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPACECRAFT_LOCATION_WORLD_WIND_LAYER__REFERED_ORBIT_MODEL = eINSTANCE.getSpacecraftLocationWorldWindLayer_ReferedOrbitModel();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.OrbitModelWorldWindLayerImpl <em>Orbit Model World Wind Layer</em>}' class.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.OrbitModelWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getOrbitModelWorldWindLayer()
		 * @generated
		 */
		EClass ORBIT_MODEL_WORLD_WIND_LAYER = eINSTANCE.getOrbitModelWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Orbit Model</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORBIT_MODEL_WORLD_WIND_LAYER__ORBIT_MODEL = eINSTANCE.getOrbitModelWorldWindLayer_OrbitModel();

		/**
		 * The meta object literal for the '<em><b>Time Source</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORBIT_MODEL_WORLD_WIND_LAYER__TIME_SOURCE = eINSTANCE.getOrbitModelWorldWindLayer_TimeSource();

		/**
		 * The meta object literal for the '<em><b>Time Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORBIT_MODEL_WORLD_WIND_LAYER__TIME_INTERVAL = eINSTANCE.getOrbitModelWorldWindLayer_TimeInterval();

		/**
		 * The meta object literal for the '<em><b>Forward Propagation Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EAttribute ORBIT_MODEL_WORLD_WIND_LAYER__FORWARD_PROPAGATION_DURATION = eINSTANCE.getOrbitModelWorldWindLayer_ForwardPropagationDuration();

		/**
		 * The meta object literal for the '<em><b>Backward Propagation Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EAttribute ORBIT_MODEL_WORLD_WIND_LAYER__BACKWARD_PROPAGATION_DURATION = eINSTANCE.getOrbitModelWorldWindLayer_BackwardPropagationDuration();

		/**
		 * The meta object literal for the '<em><b>Show Ground Trace</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORBIT_MODEL_WORLD_WIND_LAYER__SHOW_GROUND_TRACE = eINSTANCE.getOrbitModelWorldWindLayer_ShowGroundTrace();

		/**
		 * The meta object literal for the '<em><b>Show Orbit</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORBIT_MODEL_WORLD_WIND_LAYER__SHOW_ORBIT = eINSTANCE.getOrbitModelWorldWindLayer_ShowOrbit();

		/**
		 * The meta object literal for the
		 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftSwathWorldWindLayerImpl
		 * <em>Spacecraft Swath World Wind Layer</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftSwathWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getSpacecraftSwathWorldWindLayer()
		 * @generated
		 */
		EClass SPACECRAFT_SWATH_WORLD_WIND_LAYER = eINSTANCE.getSpacecraftSwathWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Orbit Model</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPACECRAFT_SWATH_WORLD_WIND_LAYER__ORBIT_MODEL = eINSTANCE.getSpacecraftSwathWorldWindLayer_OrbitModel();

		/**
		 * The meta object literal for the '<em><b>Time Source</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPACECRAFT_SWATH_WORLD_WIND_LAYER__TIME_SOURCE = eINSTANCE.getSpacecraftSwathWorldWindLayer_TimeSource();

		/**
		 * The meta object literal for the '<em><b>Forward Propagation Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EAttribute SPACECRAFT_SWATH_WORLD_WIND_LAYER__FORWARD_PROPAGATION_DURATION = eINSTANCE.getSpacecraftSwathWorldWindLayer_ForwardPropagationDuration();

		/**
		 * The meta object literal for the '<em><b>Backward Propagation Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EAttribute SPACECRAFT_SWATH_WORLD_WIND_LAYER__BACKWARD_PROPAGATION_DURATION = eINSTANCE.getSpacecraftSwathWorldWindLayer_BackwardPropagationDuration();

		/**
		 * The meta object literal for the '<em><b>Time Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPACECRAFT_SWATH_WORLD_WIND_LAYER__TIME_INTERVAL = eINSTANCE.getSpacecraftSwathWorldWindLayer_TimeInterval();

		/**
		 * The meta object literal for the '<em><b>Left Swath Angle</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPACECRAFT_SWATH_WORLD_WIND_LAYER__LEFT_SWATH_ANGLE = eINSTANCE.getSpacecraftSwathWorldWindLayer_LeftSwathAngle();

		/**
		 * The meta object literal for the '<em><b>Right Swath Angle</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPACECRAFT_SWATH_WORLD_WIND_LAYER__RIGHT_SWATH_ANGLE = eINSTANCE.getSpacecraftSwathWorldWindLayer_RightSwathAngle();

		/**
		 * The meta object literal for the '<em><b>Show Ground Trace</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPACECRAFT_SWATH_WORLD_WIND_LAYER__SHOW_GROUND_TRACE = eINSTANCE.getSpacecraftSwathWorldWindLayer_ShowGroundTrace();

		/**
		 * The meta object literal for the '<em><b>Forward Spacecraft Swath Corridor</b></em>' containment reference feature.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPACECRAFT_SWATH_WORLD_WIND_LAYER__FORWARD_SPACECRAFT_SWATH_CORRIDOR = eINSTANCE.getSpacecraftSwathWorldWindLayer_ForwardSpacecraftSwathCorridor();

		/**
		 * The meta object literal for the '<em><b>Backward Spacecraft Swath Corridor</b></em>' containment reference feature.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPACECRAFT_SWATH_WORLD_WIND_LAYER__BACKWARD_SPACECRAFT_SWATH_CORRIDOR = eINSTANCE.getSpacecraftSwathWorldWindLayer_BackwardSpacecraftSwathCorridor();

		/**
		 * The meta object literal for the
		 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractGroundStationWorldWindLayerImpl
		 * <em>Abstract Ground Station World Wind Layer</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.AbstractGroundStationWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getAbstractGroundStationWorldWindLayer()
		 * @generated
		 */
		EClass ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER = eINSTANCE.getAbstractGroundStationWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Ground Station</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__GROUND_STATION = eINSTANCE.getAbstractGroundStationWorldWindLayer_GroundStation();

		/**
		 * The meta object literal for the '<em><b>Reference Altitude</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__REFERENCE_ALTITUDE = eINSTANCE.getAbstractGroundStationWorldWindLayer_ReferenceAltitude();

		/**
		 * The meta object literal for the '<em><b>Show Visibility Circle</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CIRCLE = eINSTANCE.getAbstractGroundStationWorldWindLayer_ShowVisibilityCircle();

		/**
		 * The meta object literal for the '<em><b>Show Visibility Cone</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_VISIBILITY_CONE = eINSTANCE.getAbstractGroundStationWorldWindLayer_ShowVisibilityCone();

		/**
		 * The meta object literal for the '<em><b>Show Outline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_GROUND_STATION_WORLD_WIND_LAYER__SHOW_OUTLINE = eINSTANCE.getAbstractGroundStationWorldWindLayer_ShowOutline();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.GroundStationWorldWindLayerImpl <em>Ground Station World Wind Layer</em>}' class.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.GroundStationWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getGroundStationWorldWindLayer()
		 * @generated
		 */
		EClass GROUND_STATION_WORLD_WIND_LAYER = eINSTANCE.getGroundStationWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Refered Ground Station</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference GROUND_STATION_WORLD_WIND_LAYER__REFERED_GROUND_STATION = eINSTANCE.getGroundStationWorldWindLayer_ReferedGroundStation();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ContainedGroundStationWorldWindLayerImpl <em>Contained Ground Station World Wind Layer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ContainedGroundStationWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getContainedGroundStationWorldWindLayer()
		 * @generated
		 */
		EClass CONTAINED_GROUND_STATION_WORLD_WIND_LAYER = eINSTANCE.getContainedGroundStationWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Contained Ground Station</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINED_GROUND_STATION_WORLD_WIND_LAYER__CONTAINED_GROUND_STATION = eINSTANCE.getContainedGroundStationWorldWindLayer_ContainedGroundStation();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftVisibilityPassViewConfigurationListImpl <em>Spacecraft Visibility Pass View Configuration List</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftVisibilityPassViewConfigurationListImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getSpacecraftVisibilityPassViewConfigurationList()
		 * @generated
		 */
		EClass SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION_LIST = eINSTANCE.getSpacecraftVisibilityPassViewConfigurationList();

		/**
		 * The meta object literal for the '<em><b>Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION_LIST__CONFIGURATIONS = eINSTANCE.getSpacecraftVisibilityPassViewConfigurationList_Configurations();

		/**
		 * The meta object literal for the
		 * '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftVisibilityPassViewConfigurationImpl
		 * <em>Spacecraft Visibility Pass View Configuration</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftVisibilityPassViewConfigurationImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getSpacecraftVisibilityPassViewConfiguration()
		 * @generated
		 */
		EClass SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION = eINSTANCE.getSpacecraftVisibilityPassViewConfiguration();

		/**
		 * The meta object literal for the '<em><b>Configurations List</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION__CONFIGURATIONS_LIST = eINSTANCE.getSpacecraftVisibilityPassViewConfiguration_ConfigurationsList();

		/**
		 * The meta object literal for the '<em><b>Visibility Set</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference SPACECRAFT_VISIBILITY_PASS_VIEW_CONFIGURATION__VISIBILITY_SET = eINSTANCE.getSpacecraftVisibilityPassViewConfiguration_VisibilitySet();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.EarthViewUtilitiesImpl <em>Earth View Utilities</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.EarthViewUtilitiesImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getEarthViewUtilities()
		 * @generated
		 */
		EClass EARTH_VIEW_UTILITIES = eINSTANCE.getEarthViewUtilities();

		/**
		 * The meta object literal for the '<em><b>Get Camera View Configuration Identifier</b></em>' operation.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EOperation EARTH_VIEW_UTILITIES___GET_CAMERA_VIEW_CONFIGURATION_IDENTIFIER__EARTHVIEWCONFIGURATION = eINSTANCE.getEarthViewUtilities__GetCameraViewConfigurationIdentifier__EarthViewConfiguration();

		/**
		 * The meta object literal for the '<em><b>Get Active Earth View Configuration</b></em>' operation.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EOperation EARTH_VIEW_UTILITIES___GET_ACTIVE_EARTH_VIEW_CONFIGURATION__STRING = eINSTANCE.getEarthViewUtilities__GetActiveEarthViewConfiguration__String();

		/**
		 * The meta object literal for the '<em><b>Get Active Earth View Configuration List</b></em>' operation.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EOperation EARTH_VIEW_UTILITIES___GET_ACTIVE_EARTH_VIEW_CONFIGURATION_LIST = eINSTANCE.getEarthViewUtilities__GetActiveEarthViewConfigurationList();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.GroundStationWorldWindLayerWizardPagesProviderImpl <em>Ground Station World Wind Layer Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.GroundStationWorldWindLayerWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getGroundStationWorldWindLayerWizardPagesProvider()
		 * @generated
		 */
		EClass GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = eINSTANCE.getGroundStationWorldWindLayerWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ContainedGroundStationWorldWindLayerWizardPagesProviderImpl <em>Contained Ground Station World Wind Layer Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ContainedGroundStationWorldWindLayerWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getContainedGroundStationWorldWindLayerWizardPagesProvider()
		 * @generated
		 */
		EClass CONTAINED_GROUND_STATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = eINSTANCE.getContainedGroundStationWorldWindLayerWizardPagesProvider();

		/**
		 * The meta object literal for the '<em>Renderable Layer</em>' data type.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see gov.nasa.worldwind.layers.RenderableLayer
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getRenderableLayer()
		 * @generated
		 */
		EDataType RENDERABLE_LAYER = eINSTANCE.getRenderableLayer();

		/**
		 * The meta object literal for the '<em>Map</em>' data type. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see java.util.Map
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getMap()
		 * @generated
		 */
		EDataType MAP = eINSTANCE.getMap();

		/**
		 * The meta object literal for the '<em>ISelection</em>' data type. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.eclipse.jface.viewers.ISelection
		 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.ApogyCoreEnvironmentOrbitEarthUIPackageImpl#getISelection()
		 * @generated
		 */
		EDataType ISELECTION = eINSTANCE.getISelection();

	}

} // ApogyCoreEnvironmentOrbitEarthUIPackage

package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.core.environment.orbit.OrbitModel;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.SpacecraftLocationWorldWindLayer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Spacecraft Location World Wind Layer</b></em>'.
 * <!-- end-user-doc --> * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.SpacecraftLocationWorldWindLayerImpl#getReferedOrbitModel <em>Refered Orbit Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SpacecraftLocationWorldWindLayerImpl extends AbstractSpacecraftLocationWorldWindLayerImpl implements SpacecraftLocationWorldWindLayer 
{
	/**
	 * The cached value of the '{@link #getReferedOrbitModel() <em>Refered Orbit Model</em>}' reference.
	 * <!-- begin-user-doc -->	 * <!-- end-user-doc -->	 * @see #getReferedOrbitModel()
	 * @generated
	 * @ordered
	 */
	protected OrbitModel referedOrbitModel;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected SpacecraftLocationWorldWindLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreEnvironmentOrbitEarthUIPackage.Literals.SPACECRAFT_LOCATION_WORLD_WIND_LAYER;
	}
	
	/**
	 * <!-- begin-user-doc -->	 * <!-- end-user-doc -->	 * @generated
	 */
	public OrbitModel getReferedOrbitModel() {
		if (referedOrbitModel != null && referedOrbitModel.eIsProxy()) {
			InternalEObject oldReferedOrbitModel = (InternalEObject)referedOrbitModel;
			referedOrbitModel = (OrbitModel)eResolveProxy(oldReferedOrbitModel);
			if (referedOrbitModel != oldReferedOrbitModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCoreEnvironmentOrbitEarthUIPackage.SPACECRAFT_LOCATION_WORLD_WIND_LAYER__REFERED_ORBIT_MODEL, oldReferedOrbitModel, referedOrbitModel));
			}
		}
		return referedOrbitModel;
	}

	/**
	 * <!-- begin-user-doc -->	 * <!-- end-user-doc -->	 * @generated
	 */
	public OrbitModel basicGetReferedOrbitModel() {
		return referedOrbitModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generatedNOT
	 */

	public void setReferedOrbitModel(OrbitModel newReferedOrbitModel) 
	{
		setReferedOrbitModelGen(newReferedOrbitModel);
		setOrbitModel(newReferedOrbitModel);
	}
	
	/**
	 * <!-- begin-user-doc -->	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setReferedOrbitModelGen(OrbitModel newReferedOrbitModel) {
		OrbitModel oldReferedOrbitModel = referedOrbitModel;
		referedOrbitModel = newReferedOrbitModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.SPACECRAFT_LOCATION_WORLD_WIND_LAYER__REFERED_ORBIT_MODEL, oldReferedOrbitModel, referedOrbitModel));
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.SPACECRAFT_LOCATION_WORLD_WIND_LAYER__REFERED_ORBIT_MODEL:
				if (resolve) return getReferedOrbitModel();
				return basicGetReferedOrbitModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.SPACECRAFT_LOCATION_WORLD_WIND_LAYER__REFERED_ORBIT_MODEL:
				setReferedOrbitModel((OrbitModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.SPACECRAFT_LOCATION_WORLD_WIND_LAYER__REFERED_ORBIT_MODEL:
				setReferedOrbitModel((OrbitModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.SPACECRAFT_LOCATION_WORLD_WIND_LAYER__REFERED_ORBIT_MODEL:
				return referedOrbitModel != null;
		}
		return super.eIsSet(featureID);
	}	
} //SpacecraftLocationWorldWindLayerImpl

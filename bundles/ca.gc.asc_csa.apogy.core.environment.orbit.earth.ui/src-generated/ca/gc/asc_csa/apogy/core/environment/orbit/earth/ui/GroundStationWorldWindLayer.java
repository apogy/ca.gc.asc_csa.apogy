package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.core.environment.orbit.earth.GroundStation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ground Station World Wind Layer</b></em>'.
 * <!-- end-user-doc --> *
 * <!-- begin-model-doc -->
 * *
 * GroundStationWorldWindLayer which refers to a GroundStation.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.GroundStationWorldWindLayer#getReferedGroundStation <em>Refered Ground Station</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage#getGroundStationWorldWindLayer()
 * @model
 * @generated
 */
public interface GroundStationWorldWindLayer extends AbstractGroundStationWorldWindLayer {
	/**
	 * Returns the value of the '<em><b>Refered Ground Station</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refered Ground Station</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refered Ground Station</em>' reference.
	 * @see #setReferedGroundStation(GroundStation)
	 * @see ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage#getGroundStationWorldWindLayer_ReferedGroundStation()
	 * @model
	 * @generated
	 */
	GroundStation getReferedGroundStation();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.GroundStationWorldWindLayer#getReferedGroundStation <em>Refered Ground Station</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refered Ground Station</em>' reference.
	 * @see #getReferedGroundStation()
	 * @generated
	 */
	void setReferedGroundStation(GroundStation value);

} // GroundStationWorldWindLayer

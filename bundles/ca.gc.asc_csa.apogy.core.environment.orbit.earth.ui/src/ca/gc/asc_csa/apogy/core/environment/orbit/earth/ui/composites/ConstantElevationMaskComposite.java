/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.composites;

import java.text.DecimalFormat;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthPackage;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ConstantElevationMask;
import ca.gc.asc_csa.apogy.core.environment.ui.databindings.DegreeStringToRadiansConverter;
import ca.gc.asc_csa.apogy.core.environment.ui.databindings.RadiansToDegreesStringConverter;


public class ConstantElevationMaskComposite extends Composite 
{
	public static final String DEGREE_STRING = 	"\u00b0";
	
	protected ConstantElevationMask constantElevationMask;
	protected DecimalFormat decimalFormat;
	
	private DataBindingContext m_bindingContext;
	
	private Text elevationAngleText;
	
	private EditingDomain editingDomain;
	private boolean enableEditing = true;
	
	public ConstantElevationMaskComposite(Composite parent, int style) 
	{
		this(parent, style, null);		
	}

	public ConstantElevationMaskComposite(Composite parent, int style, EditingDomain editingDomain)
	{
		super(parent, style);
		this.editingDomain = editingDomain;
		
		setLayout(new GridLayout(3, true));

		Label elevationLabel = new Label(this, SWT.NONE);
		elevationLabel.setText("Elevation Angle (deg)");
		elevationLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
	
		elevationAngleText = new Text(this, SWT.BORDER | SWT.SINGLE);
		elevationAngleText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();				
			}
		});
	}
	
	@Override
	protected void checkSubclass() 
	{
		// Disable the check that prevents subclassing of SWT components
	}
	
	public boolean isEnableEditing() 
	{
		return enableEditing;
	}
	
	public void setEnableEditing(final boolean enableEditing) 
	{
		this.enableEditing = enableEditing;
		
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
			m_bindingContext = null;
		}		
		
		// Sets the Editing propert of the text fields.
		getDisplay().asyncExec(new Runnable()
    	{
    		public void run()
    		{
    			if(enableEditing)
    			{			
    				elevationAngleText.setEditable(true);    						
    			}
    			else
    			{
    				elevationAngleText.setEditable(false);    						
    			}
    		}
    	});
				
		if (constantElevationMask != null) 
		{
			if(this.enableEditing)
			{				
				m_bindingContext = custom_initDataBindings();
			}
			else
			{				
				m_bindingContext = initDataBindingsNoEditing();
			}
		}				
	}
	
	public ConstantElevationMask getConstantElevationMask() 
	{
		return constantElevationMask;
	}

	public void setConstantElevationMask(ConstantElevationMask newGeographicCoordinates) 
	{
		setConstantElevationMask(newGeographicCoordinates, true);
	}

	public void setConstantElevationMask(ConstantElevationMask newGeographicCoordinates, boolean update) 
	{
		constantElevationMask = newGeographicCoordinates;
		if (update) 
		{
			if (m_bindingContext != null) 
			{
				m_bindingContext.dispose();
				m_bindingContext = null;
			}
			if (constantElevationMask != null) 
			{
				if(enableEditing)
				{
					m_bindingContext = custom_initDataBindings();
				}
				else
				{
					m_bindingContext = initDataBindingsNoEditing();
				}
			}
		}
	}
	
	protected DecimalFormat getDecimalFormat()
	{
		if(decimalFormat == null)
		{
			decimalFormat = new DecimalFormat("0.0000000");
		}
		return decimalFormat;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected DataBindingContext custom_initDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		//
		IObservableValue longitudeObserveWidget = WidgetProperties.text(SWT.FocusOut).observe(elevationAngleText);
		IObservableValue xObserveValue = (editingDomain == null ? 
				EMFProperties.value(ApogyCoreEnvironmentOrbitEarthPackage.Literals.CONSTANT_ELEVATION_MASK__CONSTANT_ELEVATION).observe(constantElevationMask):
				EMFEditProperties.value(editingDomain, ApogyCoreEnvironmentOrbitEarthPackage.Literals.CONSTANT_ELEVATION_MASK__CONSTANT_ELEVATION).observe(constantElevationMask));
		
		UpdateValueStrategy strategy = new UpdateValueStrategy();
		strategy.setConverter(new DegreeStringToRadiansConverter());
		
		UpdateValueStrategy strategy_1 = new UpdateValueStrategy();
		strategy_1.setConverter(new RadiansToDegreesStringConverter(getDecimalFormat()));
		
		bindingContext.bindValue(longitudeObserveWidget, xObserveValue, strategy, strategy_1);
	
		//
		return bindingContext;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected DataBindingContext initDataBindingsNoEditing() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		//
		IObservableValue xObserveWidget = WidgetProperties.text(SWT.FocusOut).observe(elevationAngleText);
		IObservableValue xObserveValue = (editingDomain == null ? 
				EMFProperties.value(ApogyCoreEnvironmentOrbitEarthPackage.Literals.CONSTANT_ELEVATION_MASK__CONSTANT_ELEVATION).observe(constantElevationMask):
				EMFEditProperties.value(editingDomain, ApogyCoreEnvironmentOrbitEarthPackage.Literals.CONSTANT_ELEVATION_MASK__CONSTANT_ELEVATION).observe(constantElevationMask));
		
		UpdateValueStrategy strategy_1 = new UpdateValueStrategy();
		strategy_1.setConverter(new RadiansToDegreesStringConverter(getDecimalFormat()));
		
		bindingContext.bindValue(xObserveWidget, xObserveValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), strategy_1);				
		
		return bindingContext;
	}
}

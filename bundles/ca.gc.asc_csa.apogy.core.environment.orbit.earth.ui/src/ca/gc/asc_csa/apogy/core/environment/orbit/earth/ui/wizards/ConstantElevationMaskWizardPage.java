package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ConstantElevationMask;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.composites.ConstantElevationMaskComposite;

public class ConstantElevationMaskWizardPage extends WizardPage {

	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.wizards.ConstantElevationMaskWizardPage";
	
	private ConstantElevationMask constantElevationMask;
	private ConstantElevationMaskComposite constantElevationMaskComposite;
	
	public ConstantElevationMaskWizardPage(ConstantElevationMask constantElevationMask) 
	{
		super(WIZARD_PAGE_ID);
		this.constantElevationMask = constantElevationMask;
			
		setTitle("Constant Elevation Mask.");
		setDescription("Set the Constant Elevation Mask elevation angle.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(1,false));
		setControl(container);
		
		constantElevationMaskComposite = new ConstantElevationMaskComposite(container, SWT.NONE);
		constantElevationMaskComposite.setConstantElevationMask(constantElevationMask);
	}
	
	protected void validate()
	{
		setErrorMessage(null);								
		setPageComplete(getErrorMessage() == null);
	}

}

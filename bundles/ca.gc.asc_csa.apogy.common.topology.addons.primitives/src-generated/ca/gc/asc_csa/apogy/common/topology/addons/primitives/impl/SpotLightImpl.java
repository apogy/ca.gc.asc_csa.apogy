package ca.gc.asc_csa.apogy.common.topology.addons.primitives.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Spot Light</b></em>'.
 * <!-- end-user-doc --> * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.impl.SpotLightImpl#getSpotRange <em>Spot Range</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.impl.SpotLightImpl#getSpreadAngle <em>Spread Angle</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SpotLightImpl extends LightImpl implements SpotLight {
	/**
	 * The default value of the '{@link #getSpotRange() <em>Spot Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getSpotRange()
	 * @generated
	 * @ordered
	 */
	protected static final float SPOT_RANGE_EDEFAULT = 10.0F;

	/**
	 * The cached value of the '{@link #getSpotRange() <em>Spot Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getSpotRange()
	 * @generated
	 * @ordered
	 */
	protected float spotRange = SPOT_RANGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpreadAngle() <em>Spread Angle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getSpreadAngle()
	 * @generated
	 * @ordered
	 */
	protected static final float SPREAD_ANGLE_EDEFAULT = 0.52F;

	/**
	 * The cached value of the '{@link #getSpreadAngle() <em>Spread Angle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getSpreadAngle()
	 * @generated
	 * @ordered
	 */
	protected float spreadAngle = SPREAD_ANGLE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected SpotLightImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyAddonsPrimitivesPackage.Literals.SPOT_LIGHT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public float getSpotRange() {
		return spotRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setSpotRange(float newSpotRange) {
		float oldSpotRange = spotRange;
		spotRange = newSpotRange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPOT_RANGE, oldSpotRange, spotRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public float getSpreadAngle() {
		return spreadAngle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setSpreadAngle(float newSpreadAngle) {
		float oldSpreadAngle = spreadAngle;
		spreadAngle = newSpreadAngle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPREAD_ANGLE, oldSpreadAngle, spreadAngle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPOT_RANGE:
				return getSpotRange();
			case ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPREAD_ANGLE:
				return getSpreadAngle();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPOT_RANGE:
				setSpotRange((Float)newValue);
				return;
			case ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPREAD_ANGLE:
				setSpreadAngle((Float)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPOT_RANGE:
				setSpotRange(SPOT_RANGE_EDEFAULT);
				return;
			case ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPREAD_ANGLE:
				setSpreadAngle(SPREAD_ANGLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPOT_RANGE:
				return spotRange != SPOT_RANGE_EDEFAULT;
			case ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPREAD_ANGLE:
				return spreadAngle != SPREAD_ANGLE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (spotRange: ");
		result.append(spotRange);
		result.append(", spreadAngle: ");
		result.append(spreadAngle);
		result.append(')');
		return result.toString();
	}

} //SpotLightImpl

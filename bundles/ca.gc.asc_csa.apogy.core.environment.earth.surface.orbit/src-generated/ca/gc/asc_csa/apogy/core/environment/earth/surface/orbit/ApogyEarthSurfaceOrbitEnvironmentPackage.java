/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ApogyEarthSurfaceOrbitEnvironmentFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyEarthSurfaceOrbitEnvironment' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' modelName='ApogyEarthSurfaceOrbitEnvironment' complianceLevel='6.0' suppressGenModelAnnotations='false' dynamicTemplates='true' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' modelDirectory='/ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit/src-generated' editDirectory='/ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.core.environment.earth.surface'"
 * @generated
 */
public interface ApogyEarthSurfaceOrbitEnvironmentPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "orbit";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "orbit";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyEarthSurfaceOrbitEnvironmentPackage eINSTANCE = ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.impl.ApogyEarthSurfaceOrbitEnvironmentPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.impl.ApogyEarthSurfaceOrbitFacadeImpl <em>Apogy Earth Surface Orbit Facade</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.impl.ApogyEarthSurfaceOrbitFacadeImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.impl.ApogyEarthSurfaceOrbitEnvironmentPackageImpl#getApogyEarthSurfaceOrbitFacade()
	 * @generated
	 */
	int APOGY_EARTH_SURFACE_ORBIT_FACADE = 0;

	/**
	 * The feature id for the '<em><b>Dummy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_EARTH_SURFACE_ORBIT_FACADE__DUMMY = 0;

	/**
	 * The number of structural features of the '<em>Apogy Earth Surface Orbit Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_EARTH_SURFACE_ORBIT_FACADE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Apogy Earth Surface Orbit Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_EARTH_SURFACE_ORBIT_FACADE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ApogyEarthSurfaceOrbitFacade <em>Apogy Earth Surface Orbit Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apogy Earth Surface Orbit Facade</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ApogyEarthSurfaceOrbitFacade
	 * @generated
	 */
	EClass getApogyEarthSurfaceOrbitFacade();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ApogyEarthSurfaceOrbitFacade#getDummy <em>Dummy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dummy</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ApogyEarthSurfaceOrbitFacade#getDummy()
	 * @see #getApogyEarthSurfaceOrbitFacade()
	 * @generated
	 */
	EAttribute getApogyEarthSurfaceOrbitFacade_Dummy();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyEarthSurfaceOrbitEnvironmentFactory getApogyEarthSurfaceOrbitEnvironmentFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.impl.ApogyEarthSurfaceOrbitFacadeImpl <em>Apogy Earth Surface Orbit Facade</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.impl.ApogyEarthSurfaceOrbitFacadeImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.impl.ApogyEarthSurfaceOrbitEnvironmentPackageImpl#getApogyEarthSurfaceOrbitFacade()
		 * @generated
		 */
		EClass APOGY_EARTH_SURFACE_ORBIT_FACADE = eINSTANCE.getApogyEarthSurfaceOrbitFacade();
		/**
		 * The meta object literal for the '<em><b>Dummy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APOGY_EARTH_SURFACE_ORBIT_FACADE__DUMMY = eINSTANCE.getApogyEarthSurfaceOrbitFacade_Dummy();

	}

} //ApogyEarthSurfaceOrbitEnvironmentPackage

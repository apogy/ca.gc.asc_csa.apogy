/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.impl;

import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyEarthSurfaceOrbitEnvironmentFactoryImpl extends EFactoryImpl implements ApogyEarthSurfaceOrbitEnvironmentFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyEarthSurfaceOrbitEnvironmentFactory init() {
		try {
			ApogyEarthSurfaceOrbitEnvironmentFactory theApogyEarthSurfaceOrbitEnvironmentFactory = (ApogyEarthSurfaceOrbitEnvironmentFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyEarthSurfaceOrbitEnvironmentPackage.eNS_URI);
			if (theApogyEarthSurfaceOrbitEnvironmentFactory != null) {
				return theApogyEarthSurfaceOrbitEnvironmentFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyEarthSurfaceOrbitEnvironmentFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthSurfaceOrbitEnvironmentFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyEarthSurfaceOrbitEnvironmentPackage.APOGY_EARTH_SURFACE_ORBIT_FACADE: return createApogyEarthSurfaceOrbitFacade();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthSurfaceOrbitFacade createApogyEarthSurfaceOrbitFacade() {
		ApogyEarthSurfaceOrbitFacadeImpl apogyEarthSurfaceOrbitFacade = new ApogyEarthSurfaceOrbitFacadeImpl();
		return apogyEarthSurfaceOrbitFacade;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthSurfaceOrbitEnvironmentPackage getApogyEarthSurfaceOrbitEnvironmentPackage() {
		return (ApogyEarthSurfaceOrbitEnvironmentPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyEarthSurfaceOrbitEnvironmentPackage getPackage() {
		return ApogyEarthSurfaceOrbitEnvironmentPackage.eINSTANCE;
	}

} //ApogyEarthSurfaceOrbitEnvironmentFactoryImpl

//*******************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
//
// Contributors:
// 	 	 Pierre Allard - initial API and implementation
// 	 	 
// SPDX-License-Identifier: EPL-1.0
//*******************************************************************************
#import "Common/ShaderLib/Optics.glsllib"

uniform ENVMAP m_Texture;

uniform vec3 m_SunPosition;
uniform float m_Alpha;
uniform sampler2D m_SunGlowTexture;

varying vec3 direction;

varying vec3 vertex;


const float pi = 3.1415926535;

void main() 
{
    // Computes the dot product between the vertex position and the Sun position.
	vec3 vertexVector = normalize(vertex);
	vec3 sunVector = normalize(m_SunPosition);
	float dotProduct = dot(vertexVector,sunVector);

	// Gets the background color.
    vec3 dir = normalize(direction);
    vec4 color = Optics_GetEnvColor(m_Texture, dir);  
    
    // Computes the texture coordinates used to get the glow color of the sun
    // as a function of angular distance between the vertex and the sun.    
    float u = acos(dotProduct) / pi;        
    float v = 1.0 - abs(sunVector.z);                  
       
    // Gets the sun glow color from the glow texture.
    vec4 glowColor = texture2D(m_SunGlowTexture,  vec2(u, v));    
    
    // Blends the color of the background with the sun glow. 
    vec4 blendedColor = vec4(color.rgb + glowColor.rgb * glowColor.a / 2.0, color.a);    
    
    // Applies the alpha of the material to the color.
    vec4 scale = vec4(m_Alpha, m_Alpha, m_Alpha, 1.0);     
      
    gl_FragColor = blendedColor * scale;
}


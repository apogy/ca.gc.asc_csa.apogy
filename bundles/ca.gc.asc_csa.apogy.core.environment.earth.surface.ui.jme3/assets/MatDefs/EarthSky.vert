//*******************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
//
// Contributors:
// 	 	 Pierre Allard - initial API and implementation
// 	 	 
// SPDX-License-Identifier: EPL-1.0
//*******************************************************************************
uniform mat4 g_ViewMatrix;
uniform mat4 g_ProjectionMatrix;
uniform mat4 g_WorldMatrix;

uniform vec3 m_NormalScale;

attribute vec3 inPosition;
attribute vec3 inNormal;

varying vec3 direction;
varying vec3 vertex;

void main()
{
	// Gets the position.
	vertex = inPosition.xyz;

    // set w coordinate to 0
    vec4 pos = vec4(inPosition, 0.0);

    // compute rotation only for view matrix
    pos = g_ViewMatrix * pos;

    // now find projection
    pos.w = 1.0;
    gl_Position = g_ProjectionMatrix * pos;

    vec4 normal = vec4(inNormal * m_NormalScale, 0.0);
    direction = (g_WorldMatrix * normal).xyz;
}

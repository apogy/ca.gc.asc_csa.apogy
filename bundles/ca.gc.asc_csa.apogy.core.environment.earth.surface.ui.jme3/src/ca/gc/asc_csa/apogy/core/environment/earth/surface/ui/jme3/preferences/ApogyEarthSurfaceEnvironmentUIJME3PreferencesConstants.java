package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.jme3.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/


public class ApogyEarthSurfaceEnvironmentUIJME3PreferencesConstants 
{	
	// Earth Sky Constants.	
	public static final String DEFAULT_HORIZON_VISIBLE_ID = "DEFAULT_HORIZON_VISIBLE_ID";	
	public static final Boolean DEFAULT_HORIZON_VISIBLE_DEFAULT = new Boolean(true);	

	
	// Bloom Constant.
	public static final String DEFAULT_BLOOM_ENABLED_ID = "DEFAULT_BLOOM_ENABLED_ID";	
	public static final Boolean DEFAULT_BLOOM_ENABLED_DEFAULT = new Boolean(false);	
	
	// Shadows Constant.
	public static final String DEFAULT_SHADOW_MAP_SIZE_ID = "DEFAULT_SHADOW_MAP_SIZE_ID";	
	public static final Integer DEFAULT_SHADOW_MAP_SIZE_DEFAULT = new Integer(512);	

	// Sun Constants.
	public static final String DEFAULT_SUN_CAST_SHADOWS_ENABLED_ID = "DEFAULT_SUN_CAST_SHADOWS_ENABLED_ID";	
	public static final Boolean DEFAULT_SUN_CAST_SHADOWS_ENABLED_DEFAULT = new Boolean(false);	
	
	// Moon Constants.
	public static final String DEFAULT_MOON_CAST_SHADOWS_ENABLED_ID = "DEFAULT_MOON_CAST_SHADOWS_ENABLED_ID";	
	public static final Boolean DEFAULT_MOON_CAST_SHADOWS_ENABLED_DEFAULT =  new Boolean(false);	
	
}

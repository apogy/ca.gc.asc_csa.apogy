/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dComposite;

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.DirectionalLight;

public class DirectionalLightWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.wizards.DirectionalLightWizardPage";

	private DirectionalLight directionalLight;
	
	private Tuple3dComposite tuple3dComposite;
	
	public DirectionalLightWizardPage(DirectionalLight directionalLight)
	{
		super(WIZARD_PAGE_ID);
		this.directionalLight = directionalLight;
				
		setTitle("Direction");
		setDescription("Select the direction.");		
	}
	
	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(2, false));
		setControl(top);
		
		tuple3dComposite = new Tuple3dComposite(top, SWT.NONE);
		tuple3dComposite.setTuple3d(directionalLight.getDirection());		
	}

}

package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.swt.graphics.RGB;
import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;

public class TopologyPrimitivesUIPreferencesConstants 
{
	// Visibility Constants.
	public static final String DEFAULT_SPOT_LIGHT_VISIBILITY_ID = "DEFAULT_SPOT_LIGHT_VISIBILITY_ID";	
	public static final Boolean DEFAULT_SPOT_LIGHT_VISIBILITY = new Boolean(false);
	
	// FOV visibility Constants.
	public static final String DEFAULT_SPOT_LIGHT_LIGHT_CONE_VISIBILITY_ID = "DEFAULT_SPOT_LIGHT_LIGHT_CONE_VISIBILITY_ID";	
	public static final Boolean DEFAULT_SPOT_LIGHT_LIGHT_CONE_VISIBILITY = new Boolean(false);

	// Presentation Mode Constants.
	public static final String DEFAULT_SPOT_LIGHT_LIGHT_CONE_PRESENTATION_MODE_ID = "DEFAULT_SPOT_LIGHT_LIGHT_CONE_PRESENTATION_MODE_ID";	
	public static final MeshPresentationMode DEFAULT_SPOT_LIGHT_LIGHT_CONE_PRESENTATION_MODE = MeshPresentationMode.WIREFRAME;
	
	// Color Constants.
	public static final String DEFAULT_SPOT_LIGHT_LIGHT_CONE_COLOR_ID = "DEFAULT_SPOT_LIGHT_LIGHT_CONE_COLOR_ID";
			
	public static final RGB DEFAULT_SPOT_LIGHT_LIGHT_CONE_COLOR          = new RGB(255,255,255);
	
	// Axis Constants
	public static final String DEFAULT_SPOT_LIGHT_AXIS_VISIBLE_ID 		= "DEFAULT_SPOT_LIGHT_AXIS_VISIBLE_ID";
	public static final Boolean DEFAULT_SPOT_LIGHT_AXIS_VISIBLE        = new Boolean(false);	
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.wizards;

import org.eclipse.jface.preference.ColorSelector;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFactory;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathPackage;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class ColorWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.wizards.ColorWizardPage";
	
	private Tuple3d color;
	
	public ColorWizardPage(Tuple3d color)
	{
		super(WIZARD_PAGE_ID);
		this.color = color;
				
		setTitle("Color");
		setDescription("Select the color.");		
	}
	
	@Override
	public void createControl(Composite parent) 
	{		
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(2, false));
		setControl(top);
		
		Label lblColor = new Label(top, SWT.None);
		lblColor.setText("Color:");
		
		final ColorSelector colorSelector = new ColorSelector(top);
        
		colorSelector.getButton().addSelectionListener(new SelectionListener() 
        {
            @Override
            public void widgetSelected(SelectionEvent e) 
            {
            	RGB rgb = colorSelector.getColorValue();       
            	Tuple3d tmp = fromRGB(rgb);
            	if(color != null)
            	{
            		ApogyCommonTransactionFacade.INSTANCE.basicSet(color, ApogyCommonMathPackage.Literals.TUPLE3D__X, tmp.getX(), true);
            		ApogyCommonTransactionFacade.INSTANCE.basicSet(color, ApogyCommonMathPackage.Literals.TUPLE3D__Y, tmp.getY(), true);
            		ApogyCommonTransactionFacade.INSTANCE.basicSet(color, ApogyCommonMathPackage.Literals.TUPLE3D__Z, tmp.getZ(), true);            		
            	}
            }
            @Override
            public void widgetDefaultSelected(SelectionEvent e) 
            {           
            }
        });
		
	}
	
	protected Tuple3d fromRGB(RGB rgb)
	{
		Tuple3d color = ApogyCommonMathFactory.eINSTANCE.createTuple3d();
		color.setX((1.0 * rgb.red / 255.0));
		color.setY((1.0 * rgb.green / 255.0));
		color.setZ((1.0 * rgb.blue / 255.0));
		
		return color;
	}
	
	protected RGB fromTuple3d(Tuple3d tuple3d)
	{
		int red = (int) Math.round(tuple3d.getX() * 255.0f);
		if(red > 255) red = 255;

		int green = (int) Math.round(tuple3d.getY() * 255.0f);
		if(green > 255) green = 255;

		int blue = (int) Math.round(tuple3d.getZ() * 255.0f);
		if(blue > 255) blue = 255;

		
		return new RGB(red, green, blue);
	}
}

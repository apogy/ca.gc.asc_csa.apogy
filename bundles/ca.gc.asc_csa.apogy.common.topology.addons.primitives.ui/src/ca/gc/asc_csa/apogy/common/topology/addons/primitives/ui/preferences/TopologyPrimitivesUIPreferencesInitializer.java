package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.Activator;

public class TopologyPrimitivesUIPreferencesInitializer extends AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() 
	{			
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		
		// Initialize default visibility for the various Field of view Types.
		store.setDefault(TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_VISIBILITY_ID, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_VISIBILITY);
		
		// Initialize default FOV visibility for the various Field of view Types.
		store.setDefault(TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_VISIBILITY_ID, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_VISIBILITY);
		
		// Initialize default presentation mode for the various Field of view Types.
		store.setDefault(TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_PRESENTATION_MODE_ID, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_PRESENTATION_MODE.getValue());
		
		// Initialize default colors for the various Field of view Types.
		PreferenceConverter.setDefault(store, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_COLOR_ID, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_COLOR);

		// Initialize default visibility for the various Field of view Types.
		store.setDefault(TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_AXIS_VISIBLE_ID, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_AXIS_VISIBLE);

	}
}

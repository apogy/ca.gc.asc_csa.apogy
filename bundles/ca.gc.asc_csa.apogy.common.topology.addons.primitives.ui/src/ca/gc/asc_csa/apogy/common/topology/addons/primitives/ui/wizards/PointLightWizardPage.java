/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.wizards;

import java.text.DecimalFormat;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.PointLight;

public class PointLightWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.wizards.PointLightWizardPage";

	public static String NO_VALUE_AVAILABLE_STRING = "N/A";
	private int LABEL_WIDTH = 200; 
	private int VALUE_WIDTH = 100; 
	private int BUTTON_WIDTH = 50; 	
	
	private PointLight pointLight;
	
	private Adapter adapter;
	
	private TypedElementSimpleUnitsComposite radiusComposite;
	
	public PointLightWizardPage(PointLight pointLight)
	{
		super(WIZARD_PAGE_ID);
		this.pointLight = pointLight;
				
		setTitle("Point Light");
		setDescription("Select the Point Light radius.");		
	}
	
	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
		setControl(top);
		
		radiusComposite = new TypedElementSimpleUnitsComposite(top, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.0");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Radius :";
			}
		};
		radiusComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		radiusComposite.setTypedElement(FeaturePath.fromList(ApogyCommonTopologyAddonsPrimitivesPackage.Literals.POINT_LIGHT__RADIUS), pointLight);
		
		if(pointLight != null)  pointLight.eAdapters().add(getAdapter());		
		top.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent arg0) {				
				if(pointLight != null) pointLight.eAdapters().remove(getAdapter());
			}
		});
		
		validate();
	}

	protected void validate()
	{
		setErrorMessage(null);
		
		if(pointLight.getRadius() <= 0)
		{
			setErrorMessage("The specified radius must be greater than zero !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	protected Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof PointLight)
					{
						validate();
					}
				}
			};
		}
		return adapter;
	}
}

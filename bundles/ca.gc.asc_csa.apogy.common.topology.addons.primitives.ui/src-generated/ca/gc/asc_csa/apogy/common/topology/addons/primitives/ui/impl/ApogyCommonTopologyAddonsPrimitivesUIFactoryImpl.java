package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.LabelPresentation;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.SpherePrimitivePresentation;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.ApogyCommonTopologyAddonsPrimitivesUIFactory;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.ApogyCommonTopologyAddonsPrimitivesUIPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.VectorPresentation;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.WayPointPresentation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc --> * @generated
 */
public class ApogyCommonTopologyAddonsPrimitivesUIFactoryImpl extends EFactoryImpl implements ApogyCommonTopologyAddonsPrimitivesUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public static ApogyCommonTopologyAddonsPrimitivesUIFactory init() {
		try {
			ApogyCommonTopologyAddonsPrimitivesUIFactory theApogyCommonTopologyAddonsPrimitivesUIFactory = (ApogyCommonTopologyAddonsPrimitivesUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyCommonTopologyAddonsPrimitivesUIPackage.eNS_URI);
			if (theApogyCommonTopologyAddonsPrimitivesUIFactory != null) {
				return theApogyCommonTopologyAddonsPrimitivesUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyCommonTopologyAddonsPrimitivesUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCommonTopologyAddonsPrimitivesUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.VECTOR_PRESENTATION: return createVectorPresentation();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.WAY_POINT_PRESENTATION: return createWayPointPresentation();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.LABEL_PRESENTATION: return createLabelPresentation();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPHERE_PRIMITIVE_PRESENTATION: return createSpherePrimitivePresentation();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION: return createSpotLightPresentation();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPHERE_PRIMITIVE_WIZARD_PAGES_PROVIDER: return createSpherePrimitiveWizardPagesProvider();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.LIGHT_WIZARD_PAGES_PROVIDER: return createLightWizardPagesProvider();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.AMBIENT_LIGHT_WIZARD_PAGES_PROVIDER: return createAmbientLightWizardPagesProvider();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.DIRECTIONAL_LIGHT_WIZARD_PAGES_PROVIDER: return createDirectionalLightWizardPagesProvider();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.POINT_LIGHT_WIZARD_PAGES_PROVIDER: return createPointLightWizardPagesProvider();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_WIZARD_PAGES_PROVIDER: return createSpotLightWizardPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public VectorPresentation createVectorPresentation() {
		VectorPresentationImpl vectorPresentation = new VectorPresentationImpl();
		return vectorPresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public WayPointPresentation createWayPointPresentation() {
		WayPointPresentationImpl wayPointPresentation = new WayPointPresentationImpl();
		return wayPointPresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public LabelPresentation createLabelPresentation() {
		LabelPresentationImpl labelPresentation = new LabelPresentationImpl();
		return labelPresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public SpherePrimitivePresentation createSpherePrimitivePresentation() {
		SpherePrimitivePresentationImpl spherePrimitivePresentation = new SpherePrimitivePresentationImpl();
		return spherePrimitivePresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public SpotLightPresentation createSpotLightPresentation() {
		SpotLightPresentationImpl spotLightPresentation = new SpotLightPresentationImpl();
		return spotLightPresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpherePrimitiveWizardPagesProvider createSpherePrimitiveWizardPagesProvider() {
		SpherePrimitiveWizardPagesProviderImpl spherePrimitiveWizardPagesProvider = new SpherePrimitiveWizardPagesProviderImpl();
		return spherePrimitiveWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LightWizardPagesProvider createLightWizardPagesProvider() {
		LightWizardPagesProviderImpl lightWizardPagesProvider = new LightWizardPagesProviderImpl();
		return lightWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AmbientLightWizardPagesProvider createAmbientLightWizardPagesProvider() {
		AmbientLightWizardPagesProviderImpl ambientLightWizardPagesProvider = new AmbientLightWizardPagesProviderImpl();
		return ambientLightWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectionalLightWizardPagesProvider createDirectionalLightWizardPagesProvider() {
		DirectionalLightWizardPagesProviderImpl directionalLightWizardPagesProvider = new DirectionalLightWizardPagesProviderImpl();
		return directionalLightWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PointLightWizardPagesProvider createPointLightWizardPagesProvider() {
		PointLightWizardPagesProviderImpl pointLightWizardPagesProvider = new PointLightWizardPagesProviderImpl();
		return pointLightWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpotLightWizardPagesProvider createSpotLightWizardPagesProvider() {
		SpotLightWizardPagesProviderImpl spotLightWizardPagesProvider = new SpotLightWizardPagesProviderImpl();
		return spotLightWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCommonTopologyAddonsPrimitivesUIPackage getApogyCommonTopologyAddonsPrimitivesUIPackage() {
		return (ApogyCommonTopologyAddonsPrimitivesUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyCommonTopologyAddonsPrimitivesUIPackage getPackage() {
		return ApogyCommonTopologyAddonsPrimitivesUIPackage.eINSTANCE;
	}

} //ApogyCommonTopologyAddonsPrimitivesUIFactoryImpl

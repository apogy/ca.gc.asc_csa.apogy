/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.RGB;

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.Activator;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.ApogyCommonTopologyAddonsPrimitivesUIPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.SpotLightPresentation;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.preferences.TopologyPrimitivesUIPreferencesConstants;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.scene_objects.SpotLightSceneObject;
import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;
import ca.gc.asc_csa.apogy.common.topology.ui.impl.NodePresentationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Spot Light Presentation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.impl.SpotLightPresentationImpl#getPresentationMode <em>Presentation Mode</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.impl.SpotLightPresentationImpl#isLightConeVisible <em>Light Cone Visible</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.impl.SpotLightPresentationImpl#isAxisVisible <em>Axis Visible</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.impl.SpotLightPresentationImpl#getAxisLength <em>Axis Length</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SpotLightPresentationImpl extends NodePresentationImpl implements SpotLightPresentation 
{
	protected IPropertyChangeListener preferencesListener = null;

	
	/**
	 * The default value of the '{@link #getPresentationMode() <em>Presentation Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPresentationMode()
	 * @generated
	 * @ordered
	 */
	protected static final MeshPresentationMode PRESENTATION_MODE_EDEFAULT = MeshPresentationMode.POINTS;

	/**
	 * The cached value of the '{@link #getPresentationMode() <em>Presentation Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPresentationMode()
	 * @generated
	 * @ordered
	 */
	protected MeshPresentationMode presentationMode = PRESENTATION_MODE_EDEFAULT;


	/**
	 * The default value of the '{@link #isLightConeVisible() <em>Light Cone Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLightConeVisible()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LIGHT_CONE_VISIBLE_EDEFAULT = false;


	/**
	 * The cached value of the '{@link #isLightConeVisible() <em>Light Cone Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLightConeVisible()
	 * @generated
	 * @ordered
	 */
	protected boolean lightConeVisible = LIGHT_CONE_VISIBLE_EDEFAULT;


	/**
	 * The default value of the '{@link #isAxisVisible() <em>Axis Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAxisVisible()
	 * @generated
	 * @ordered
	 */
	protected static final boolean AXIS_VISIBLE_EDEFAULT = false;


	/**
	 * The cached value of the '{@link #isAxisVisible() <em>Axis Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAxisVisible()
	 * @generated
	 * @ordered
	 */
	protected boolean axisVisible = AXIS_VISIBLE_EDEFAULT;


	/**
	 * The default value of the '{@link #getAxisLength() <em>Axis Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxisLength()
	 * @generated
	 * @ordered
	 */
	protected static final double AXIS_LENGTH_EDEFAULT = 1.0;


	/**
	 * The cached value of the '{@link #getAxisLength() <em>Axis Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxisLength()
	 * @generated
	 * @ordered
	 */
	protected double axisLength = AXIS_LENGTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	protected SpotLightPresentationImpl() 
	{
		super();
		
		// Register a listener to the preference store
		Activator.getDefault().getPreferenceStore().addPropertyChangeListener(getPreferencesListener());	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyAddonsPrimitivesUIPackage.Literals.SPOT_LIGHT_PRESENTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAxisVisible() {
		return axisVisible;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAxisVisible(boolean newAxisVisible) {
		boolean oldAxisVisible = axisVisible;
		axisVisible = newAxisVisible;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_VISIBLE, oldAxisVisible, axisVisible));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getAxisLength() {
		return axisLength;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAxisLength(double newAxisLength) {
		double oldAxisLength = axisLength;
		axisLength = newAxisLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_LENGTH, oldAxisLength, axisLength));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLightConeVisible() {
		return lightConeVisible;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLightConeVisible(boolean newLightConeVisible) {
		boolean oldLightConeVisible = lightConeVisible;
		lightConeVisible = newLightConeVisible;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__LIGHT_CONE_VISIBLE, oldLightConeVisible, lightConeVisible));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MeshPresentationMode getPresentationMode() {
		return presentationMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPresentationMode(MeshPresentationMode newPresentationMode) {
		MeshPresentationMode oldPresentationMode = presentationMode;
		presentationMode = newPresentationMode == null ? PRESENTATION_MODE_EDEFAULT : newPresentationMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__PRESENTATION_MODE, oldPresentationMode, presentationMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__PRESENTATION_MODE:
				return getPresentationMode();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__LIGHT_CONE_VISIBLE:
				return isLightConeVisible();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_VISIBLE:
				return isAxisVisible();
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_LENGTH:
				return getAxisLength();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__PRESENTATION_MODE:
				setPresentationMode((MeshPresentationMode)newValue);
				return;
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__LIGHT_CONE_VISIBLE:
				setLightConeVisible((Boolean)newValue);
				return;
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_VISIBLE:
				setAxisVisible((Boolean)newValue);
				return;
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_LENGTH:
				setAxisLength((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__PRESENTATION_MODE:
				setPresentationMode(PRESENTATION_MODE_EDEFAULT);
				return;
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__LIGHT_CONE_VISIBLE:
				setLightConeVisible(LIGHT_CONE_VISIBLE_EDEFAULT);
				return;
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_VISIBLE:
				setAxisVisible(AXIS_VISIBLE_EDEFAULT);
				return;
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_LENGTH:
				setAxisLength(AXIS_LENGTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__PRESENTATION_MODE:
				return presentationMode != PRESENTATION_MODE_EDEFAULT;
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__LIGHT_CONE_VISIBLE:
				return lightConeVisible != LIGHT_CONE_VISIBLE_EDEFAULT;
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_VISIBLE:
				return axisVisible != AXIS_VISIBLE_EDEFAULT;
			case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_LENGTH:
				return axisLength != AXIS_LENGTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (presentationMode: ");
		result.append(presentationMode);
		result.append(", lightConeVisible: ");
		result.append(lightConeVisible);
		result.append(", axisVisible: ");
		result.append(axisVisible);
		result.append(", axisLength: ");
		result.append(axisLength);
		result.append(')');
		return result.toString();
	}

	@Override
	protected void initialSceneObject() 
	{
		SpotLightSceneObject spotLightSceneObject = (SpotLightSceneObject) getSceneObject();				
		
		spotLightSceneObject.setPresentationMode(this.getPresentationMode());		
		spotLightSceneObject.setLightConeVisible(this.isLightConeVisible());
		
		spotLightSceneObject.setAxisVisible(this.axisVisible);
		spotLightSceneObject.setAxisLength(this.getAxisLength());
		
		
		super.initialSceneObject();
	}
	
	@Override
	protected void updateSceneObject(Notification notification) 
	{			
		SpotLightSceneObject spotLightSceneObject = (SpotLightSceneObject) getSceneObject();
		
		if(notification.getNotifier() instanceof SpotLightPresentation)
		{
			int featureId = notification.getFeatureID(SpotLightPresentation.class);
			
			switch (featureId) 
			{
				case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__PRESENTATION_MODE:
					spotLightSceneObject.setPresentationMode(this.getPresentationMode());
				break;
			
				case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__LIGHT_CONE_VISIBLE:
					spotLightSceneObject.setLightConeVisible(this.isLightConeVisible());
				break;
				
				case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_VISIBLE:
					spotLightSceneObject.setAxisVisible(this.isAxisVisible());
				break;

				case ApogyCommonTopologyAddonsPrimitivesUIPackage.SPOT_LIGHT_PRESENTATION__AXIS_LENGTH:
					spotLightSceneObject.setAxisLength(this.getAxisLength());
				break;

				default:
				break;
			}
		}		
		
		super.updateSceneObject(notification);
	}
	
	
	private IPropertyChangeListener getPreferencesListener()
	{
		if(preferencesListener == null)
		{
			preferencesListener = new IPropertyChangeListener() 
			{
				public void propertyChange(PropertyChangeEvent event) 
				{	
					applyPreferences();		
				}	
			};
		}
		
		return preferencesListener;
	}
	
	@Override
	protected void finalize() throws Throwable 
	{
		Activator.getDefault().getPreferenceStore().removePropertyChangeListener(getPreferencesListener());
		super.finalize();
	}
	
	protected void applyPreferences()
	{
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		
		// Change visibility.
		setVisible(store.getBoolean(TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_VISIBILITY_ID));
		
		// Change FOV Visibility
		setLightConeVisible(store.getBoolean(TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_VISIBILITY_ID));
		
		// Change Axis visibility.
		setAxisVisible(store.getBoolean(TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_AXIS_VISIBLE_ID));
		
		// Change color.
		RGB rgb = PreferenceConverter.getColor(store, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_COLOR_ID);
		if(rgb != null) setColor(rgb);
						
		// Change mode.
		setPresentationMode(MeshPresentationMode.get(store.getInt(TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_PRESENTATION_MODE_ID)));		
		
		super.applyPreferences();
	}
} //SpotLightPresentationImpl

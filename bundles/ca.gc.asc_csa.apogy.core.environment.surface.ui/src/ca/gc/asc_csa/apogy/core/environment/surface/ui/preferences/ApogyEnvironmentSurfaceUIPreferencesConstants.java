package ca.gc.asc_csa.apogy.core.environment.surface.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/


public class ApogyEnvironmentSurfaceUIPreferencesConstants 
{
	public static final String DEFAULT_MAP_VIEW_CONFIGURATION_X_MIN_ID = "DEFAULT_MAP_VIEW_CONFIGURATION_X_MIN_ID";
	public static final double DEFAULT_MAP_VIEW_CONFIGURATION_X_MIN = -20.0;
		
	public static final String DEFAULT_MAP_VIEW_CONFIGURATION_X_MAX_ID = "DEFAULT_MAP_VIEW_CONFIGURATION_X_MAX_ID";
	public static final double DEFAULT_MAP_VIEW_CONFIGURATION_X_MAX = 20.0;

	public static final String DEFAULT_MAP_VIEW_CONFIGURATION_Y_MIN_ID = "DEFAULT_MAP_VIEW_CONFIGURATION_Y_MIN_ID";
	public static final double DEFAULT_MAP_VIEW_CONFIGURATION_Y_MIN = -20.0;
		
	public static final String DEFAULT_MAP_VIEW_CONFIGURATION_Y_MAX_ID = "DEFAULT_MAP_VIEW_CONFIGURATION_Y_MAX_ID";
	public static final double DEFAULT_MAP_VIEW_CONFIGURATION_Y_MAX = 20.0;	
	
	public static final String DEFAULT_MAP_VIEW_UPDATE_PERIOD_MILLISECONDS_ID = "DEFAULT_MAP_VIEW_UPDATE_PERIOD_MILLISECONDS_ID";
	public static final long DEFAULT_MAP_VIEW_UPDATE_PERIOD_MILLISECONDS = 1000;
	
	public static final String DEFAULT_MAP_VIEW_IMAGE_MAXIMUM_NUMBER_OF_PIXELS_ID = "DEFAULT_MAP_VIEW_IMAGE_MAXIMUM_NUMBER_OF_PIXELS_ID";
	public static final int DEFAULT_MAP_VIEW_IMAGE_MAXIMUM_NUMBER_OF_PIXELS = 1024 * 1024;

	public static final String DEFAULT_STAR_MAGNITUDE_CUTOFF_ID = "DEFAULT_STAR_MAGNITUDE_CUTOFF_ID";
	public static final double DEFAULT_STAR_MAGNITUDE_CUTOFF = 6.0;
	
	
	// Earth Surface Worksite Settings.
	public static final String DEFAULT_SURFACE_WORKSITE_AXIS_LENGTH_ID = "DEFAULT_SURFACE_WORSITE_AXIS_LENGTH_ID";
	public static final double DEFAULT_SURFACE_WORKSITE_AXIS_LENGTH = 5.0;

	public static final String DEFAULT_SURFACE_WORKSITE_AXIS_VISIBLE_ID = "DEFAULT_SURFACE_WORSITE_AXIS_VISIBLE_ID";
	public static final boolean DEFAULT_SURFACE_WORKSITE_AXIS_VISIBLE = false;
	
	public static final String DEFAULT_SURFACE_WORKSITE_AZIMUTH_VISIBLE_ID = "DEFAULT_SURFACE_WORSITE_AZIMUTH_VISIBLE_ID";
	public static final boolean DEFAULT_SURFACE_WORKSITE_AZIMUTH_VISIBLE = true;
	
	public static final String DEFAULT_SURFACE_WORKSITE_AZIMUTH_LINES_VISIBLE_ID = "DEFAULT_SURFACE_WORSITE_AZIMUTH_LINES_VISIBLE_ID";
	public static final boolean DEFAULT_SURFACE_WORKSITE_AZIMUTH_LINES_VISIBLE = true;
	
	public static final String DEFAULT_SURFACE_WORKSITE_ELEVATION_LINES_VISIBLE_ID = "DEFAULT_SURFACE_WORSITE_ELEVATION_LINES_VISIBLE_ID";
	public static final boolean DEFAULT_SURFACE_WORKSITE_ELEVATION_LINES_VISIBLE = true;
	
	public static final String DEFAULT_SURFACE_WORKSITE_PLANE_VISIBLE_ID = "DEFAULT_SURFACE_WORSITE_PLANE_VISIBLE_ID";
	public static final boolean DEFAULT_SURFACE_WORKSITE_PLANE_VISIBLE = false;
	
	public static final String DEFAULT_SURFACE_WORKSITE_PLANE_GRID_SIZE_ID = "DEFAULT_SURFACE_WORSITE_PLANE_GRID_SIZE_ID";
	public static final double DEFAULT_SURFACE_WORKSITE_PLANE_GRID_SIZE = 1.0;
	
	public static final String DEFAULT_SURFACE_WORKSITE_PLANE_SIZE_ID = "DEFAULT_SURFACE_WORSITE_PLANE_SIZE_ID";
	public static final double DEFAULT_SURFACE_WORKSITE_PLANE_SIZE = 20.0;

	// Earth Sky Settings
	public static final String DEFAULT_SKY_HORIZON_VISIBLE_ID = "DEFAULT_SKY_HORIZON_VISIBLE_ID";
	public static final boolean DEFAULT_SKY_HORIZON_VISIBLE = true;
	
	// Feature Of Interest
	public static final String DEFAULT_FOI_FLAG_POLE_HEIGHT_ID = "DEFAULT_FOI_FLAG_POLE_HEIGHT_ID";
	public static final double DEFAULT_FOI_FLAG_POLE_HEIGHT = 2.0;
	
	public static final String DEFAULT_FOI_FLAG_VISIBLE_ID = "DEFAULT_FOI_FLAG_VISIBLE_ID";
	public static final boolean DEFAULT_FOI_FLAG_VISIBLE = true;
	
	public static final String DEFAULT_FOI_FONT_SIZE_ID = "DEFAULT_FOI_FONT_SIZE_ID";
	public static final int DEFAULT_FOI_FONT_SIZE = 16;

}

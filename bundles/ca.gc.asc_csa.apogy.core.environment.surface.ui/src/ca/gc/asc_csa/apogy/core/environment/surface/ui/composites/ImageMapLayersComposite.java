package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;

public class ImageMapLayersComposite extends Composite 
{
	private Map map;
	
	private ImageMapLayersListComposite imageMapLayersListComposite;
	private ImageMapLayerOverviewComposite imageMapLayerOverviewComposite;
	private ImageMapLayerPreviewComposite imageMapLayerPreviewComposite;
	private ImageMapLayerDetailsComposite imageMapLayerDetailsComposite;
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
		
	public ImageMapLayersComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		ScrolledForm scrldfrmImagesLayers = formToolkit.createScrolledForm(this);
		scrldfrmImagesLayers.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrldfrmImagesLayers);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			tableWrapLayout.numColumns = 2;
			scrldfrmImagesLayers.getBody().setLayout(tableWrapLayout);
		}
		
		Section sctnImageLayers = formToolkit.createSection(scrldfrmImagesLayers.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnImageLayers = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 3, 1);
		twd_sctnImageLayers.grabVertical = true;
		twd_sctnImageLayers.valign = TableWrapData.FILL;
		sctnImageLayers.setLayoutData(twd_sctnImageLayers);
		formToolkit.paintBordersFor(sctnImageLayers);
		sctnImageLayers.setText("Image Layers");
		
		imageMapLayersListComposite = new ImageMapLayersListComposite(sctnImageLayers, SWT.NONE)
		{
			@Override
			protected void newImageMapLayerSelected(ImageMapLayer newImageMapLayer)
			{		
				imageMapLayerOverviewComposite.setImageMapLayer(newImageMapLayer);
				imageMapLayerPreviewComposite.setImageMapLayer(newImageMapLayer);
				imageMapLayerDetailsComposite.setImageMapLayer(newImageMapLayer);				
			}
		};
		formToolkit.adapt(imageMapLayersListComposite);
		formToolkit.paintBordersFor(imageMapLayersListComposite);
		sctnImageLayers.setClient(imageMapLayersListComposite);
	
		
		Section sctnImageLayerOverview = formToolkit.createSection(scrldfrmImagesLayers.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnImageLayerOverview);
		sctnImageLayerOverview.setText("Image Layer Overview");
		
		imageMapLayerOverviewComposite = new ImageMapLayerOverviewComposite(sctnImageLayerOverview, SWT.NONE);
		formToolkit.adapt(imageMapLayerOverviewComposite);
		formToolkit.paintBordersFor(imageMapLayerOverviewComposite);
		sctnImageLayerOverview.setClient(imageMapLayerOverviewComposite);			
								
		Section sctnImagePreview = formToolkit.createSection(scrldfrmImagesLayers.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnImagePreview);
		sctnImagePreview.setText("Image Preview");
	
		imageMapLayerPreviewComposite = new ImageMapLayerPreviewComposite(sctnImagePreview, SWT.NONE)
		{
			@Override
			public void setImageMapLayer(ImageMapLayer newImageMapLayer) 
			{				
				super.setImageMapLayer(newImageMapLayer);
				
				// Forces the section to update its layout.
				sctnImagePreview.layout();
			}
		};

		formToolkit.adapt(imageMapLayerPreviewComposite);
		formToolkit.paintBordersFor(imageMapLayerPreviewComposite);
		sctnImagePreview.setClient(imageMapLayerPreviewComposite);
		
		Section sctnImageLayerDetails = formToolkit.createSection(scrldfrmImagesLayers.getBody(), Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnImageLayerDetails);
		sctnImageLayerDetails.setText("Image Layer Details");
		
		imageMapLayerDetailsComposite = new ImageMapLayerDetailsComposite(sctnImageLayerDetails, SWT.NONE);
		formToolkit.adapt(imageMapLayerDetailsComposite);
		formToolkit.paintBordersFor(imageMapLayerDetailsComposite);
		sctnImageLayerDetails.setClient(imageMapLayerDetailsComposite);		
		
		scrldfrmImagesLayers.reflow(true);
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map newMap) 
	{		
		this.map = newMap;
		imageMapLayersListComposite.setMap(newMap);
		//imageMapLayerOverviewComposite.setImageMapLayer(null);		
		//imageMapLayerDetailsComposite.setImageMapLayer(null);		
	}
}

package ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.URLSelectionComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshURLMapLayer;

public class CartesianTriangularMeshURLMapLayerURLWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.CartesianTriangularMeshURLMapLayerURLWizardPage";
	
	private CartesianTriangularMeshURLMapLayer cartesianTriangularMeshURLMapLayer;
	private URLSelectionComposite urlSelectionComposite;
	private String urlString = null;
	
	public CartesianTriangularMeshURLMapLayerURLWizardPage(CartesianTriangularMeshURLMapLayer cartesianTriangularMeshURLMapLayer) 
	{
		super(WIZARD_PAGE_ID);
		this.cartesianTriangularMeshURLMapLayer = cartesianTriangularMeshURLMapLayer;
		
		if(cartesianTriangularMeshURLMapLayer != null)
		{
			this.urlString = cartesianTriangularMeshURLMapLayer.getUrl();
		}
		
		setTitle("Cartesian Triangular Mesh : URL selection");
		setDescription("Sets the Cartesian Triangular Mesh URL.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(1, false));
		
		urlSelectionComposite = new URLSelectionComposite(container, SWT.None, new String[]{"*.tri"}, true, true, true)
		{
			@Override
			protected void urlStringSelected(String newURLString) 
			{		
				CartesianTriangularMeshURLMapLayerURLWizardPage.this.urlString = newURLString;
				
				validate();
				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(cartesianTriangularMeshURLMapLayer, ApogySurfaceEnvironmentPackage.Literals.URL_MAP_LAYER__URL, newURLString);
			}
		};
		urlSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		if(cartesianTriangularMeshURLMapLayer != null && cartesianTriangularMeshURLMapLayer.getUrl() != null)
		{
			urlSelectionComposite.setUrlString(cartesianTriangularMeshURLMapLayer.getUrl());
		}
		
		setControl(container);
		urlSelectionComposite.setFocus();			
	}	
		
	protected void validate()
	{
		boolean urlValid = (urlString!= null) && (urlString.length() > 0);
		setPageComplete(urlValid);
		
		if(urlValid)
		{
			setErrorMessage(null);
		}
		else
		{
			setErrorMessage("Invalid URL specified !");
		}		
	}
}

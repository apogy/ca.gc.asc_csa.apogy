package ca.gc.asc_csa.apogy.core.environment.surface.ui.adapters;

import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentationAdapter;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayerNode;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.CartesianTriangularMeshMapLayerNodePresentation;

public class CartesianTriangularMeshMapLayerNodePresentationAdapter implements NodePresentationAdapter
{
	public CartesianTriangularMeshMapLayerNodePresentationAdapter() 
	{
	}
	
	@Override
	public NodePresentation getAdapter(Node node, Object context) {

		if (!isAdapterFor(node)) 
		{
			throw new IllegalArgumentException();
		}

		CartesianTriangularMeshMapLayerNodePresentation presentation = ApogySurfaceEnvironmentUIFactory.eINSTANCE.createCartesianTriangularMeshMapLayerNodePresentation();

		CartesianTriangularMeshMapLayerNode cNode = (CartesianTriangularMeshMapLayerNode) node;

		presentation.setNode(cNode);

		return presentation;
	}

	@Override
	public boolean isAdapterFor(Node node) {

		boolean adapterFor = false;

		if (node instanceof CartesianTriangularMeshMapLayerNode) 
		{
			adapterFor = true;
		}
		return adapterFor;
	}

	@Override
	public Class<?> getAdaptedClass() {
		return CartesianTriangularMeshMapLayerNode.class;
	}
}

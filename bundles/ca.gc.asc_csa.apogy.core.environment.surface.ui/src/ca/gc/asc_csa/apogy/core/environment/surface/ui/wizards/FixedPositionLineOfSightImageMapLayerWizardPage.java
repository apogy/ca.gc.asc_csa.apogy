package ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.Color3fComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.AbstractMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.FixedPositionLineOfSightImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ImageMapLayerUISettings;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.ImageMapLayerPreviewComposite;

public class FixedPositionLineOfSightImageMapLayerWizardPage extends WizardPage 
{	
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.LineOfSightImageMapLayerWizardPage";
	
	private FixedPositionLineOfSightImageMapLayer lineOfSightImageMapLayer;
	@SuppressWarnings("unused")
	private ImageMapLayerUISettings uiSettings;
		
	@SuppressWarnings("unused")
	private Color3fComposite lineOfSightColor3fComposite;
	@SuppressWarnings("unused")
	private Color3fComposite noLineOfSightColor3fComposite;
	
	private Tuple3dComposite positionComposite;
	private Text txtTargetHeightAboveGround;
	private Button btnUseHeightPerpendicularToGround;
	
	private ImageMapLayerPreviewComposite imagePreviewComposite;		
	private DataBindingContext m_bindingContext;
					
	public FixedPositionLineOfSightImageMapLayerWizardPage(FixedPositionLineOfSightImageMapLayer lineOfSightImageMapLayer, 	
																 ImageMapLayerUISettings uiSettings) 
	{
		super(WIZARD_PAGE_ID);
		
		this.lineOfSightImageMapLayer = lineOfSightImageMapLayer;
		this.uiSettings = uiSettings;
		
		setTitle("Line Of Sight Image Layer");
		setDescription("Configure the line of sight colors and position settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(2, false));
					
		Group colorGroup = new Group(container, SWT.BORDER);
		colorGroup.setLayout(new GridLayout(2, false));
		colorGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		colorGroup.setText("Colors");		
	
		Label lblLineOfSightColor = new Label(colorGroup, SWT.NONE);
		lblLineOfSightColor.setAlignment(SWT.RIGHT);
		lblLineOfSightColor.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLineOfSightColor.setText("Line Of Sight Color :");
		lineOfSightColor3fComposite = new Color3fComposite(colorGroup, SWT.NONE, "", true, lineOfSightImageMapLayer, ApogySurfaceEnvironmentPackage.Literals.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_AVAILABLE_COLOR);
		
		Label lblNoLineOfSightColor = new Label(colorGroup, SWT.NONE);
		lblNoLineOfSightColor.setAlignment(SWT.RIGHT);
		lblNoLineOfSightColor.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNoLineOfSightColor.setText("No Line Of Sight Color :");
		noLineOfSightColor3fComposite = new Color3fComposite(colorGroup, SWT.NONE, "", true, lineOfSightImageMapLayer, ApogySurfaceEnvironmentPackage.Literals.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_NOT_AVAILABLE_COLOR);
		
		Group positionGroup = new Group(container, SWT.BORDER);
		positionGroup.setLayout(new GridLayout(2, false));
		positionGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		positionGroup.setText("Position and Target Height");		
		
		Label lblObserverPosition = new Label(positionGroup, SWT.NONE);
		lblObserverPosition.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblObserverPosition.setText("Observer Position (m):");
		
		positionComposite = new Tuple3dComposite(positionGroup, SWT.BORDER, ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(lineOfSightImageMapLayer));
		positionComposite.setTuple3d(lineOfSightImageMapLayer.getObserverPosition());
		
		Label lblTargetHeightAboveGround = new Label(positionGroup, SWT.NONE);
		lblTargetHeightAboveGround.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTargetHeightAboveGround.setText("Target Height Above Ground (m):");

		txtTargetHeightAboveGround = new Text(positionGroup, SWT.BORDER);
		GridData gd_txtTargetHeightAboveGround = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtTargetHeightAboveGround.minimumWidth = 100;
		gd_txtTargetHeightAboveGround.widthHint = 100;
		txtTargetHeightAboveGround.setLayoutData(gd_txtTargetHeightAboveGround);
		txtTargetHeightAboveGround.setToolTipText("The height of the target above ground.");
		
		Label lblAutoScale = new Label(positionGroup, SWT.NONE);
		lblAutoScale.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAutoScale.setText("Height Perpendicular To Ground :");
		
		btnUseHeightPerpendicularToGround = new Button(positionGroup, SWT.CHECK);
		btnUseHeightPerpendicularToGround.setAlignment(SWT.RIGHT);
		btnUseHeightPerpendicularToGround.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		btnUseHeightPerpendicularToGround.setText("");
		btnUseHeightPerpendicularToGround.setToolTipText("Whether or not to find each target position using the local normal. Using the local normal requires more processing time.");
		
		// Image Preview.
		Group imageGroup = new Group(container, SWT.BORDER);
		imageGroup.setLayout(new GridLayout(1, false));
		imageGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1));
		imageGroup.setText("Derived Image");		

		imagePreviewComposite = new ImageMapLayerPreviewComposite(imageGroup, SWT.NONE, lineOfSightImageMapLayer);
		GridData gd_imagePreviewComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_imagePreviewComposite.widthHint = 200;
		gd_imagePreviewComposite.heightHint = 200;
		gd_imagePreviewComposite.minimumHeight = 200;
		gd_imagePreviewComposite.minimumWidth = 200;
		imagePreviewComposite.setLayoutData(gd_imagePreviewComposite);
		imagePreviewComposite.setImageMapLayer(lineOfSightImageMapLayer);

		setControl(container);				
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
				
		validate();
		
		// Dispose
		container.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
		
	protected void validate()
	{
		setErrorMessage(null);
								
		// TODO Validate the Slope Ranges.
		
		setPageComplete(getErrorMessage() == null);				
	}
		
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		/* Target Height above Ground Value. */
		IObservableValue<Double> observeTargetHeightAboveGround = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(lineOfSightImageMapLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__TARGET_HEIGHT_ABOVE_GROUND)).observe(lineOfSightImageMapLayer);
		IObservableValue<String> observeTargetHeightAboveGroundText = WidgetProperties.text(SWT.Modify).observe(txtTargetHeightAboveGround);

		bindingContext.bindValue(observeTargetHeightAboveGroundText,
								observeTargetHeightAboveGround, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		

		/* Perpendicular.*/
		IObservableValue<Double> observeHeightPerpendicular = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(lineOfSightImageMapLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__USE_HEIGHT_PERPENDICULAR_TO_GROUND)).observe(lineOfSightImageMapLayer);
		IObservableValue<String> observeHeightPerpendicularButtton = WidgetProperties.selection().observe(btnUseHeightPerpendicularToGround);
				
		bindingContext.bindValue(observeHeightPerpendicularButtton,
								 observeHeightPerpendicular, 
								 new UpdateValueStrategy(),	
								 new UpdateValueStrategy());	
		
		return bindingContext;
	}
	
	public class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof Map)
			{								
				Map map = (Map) inputElement;
								
				// Keeps only ImageMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof Map)
			{								
				Map map = (Map) parentElement;
				
				// Keeps only ImageMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof Map)
			{
				Map map = (Map) element;		
				return !filterMap(map).isEmpty();
			}		
			else
			{
				return false;
			}
		}
		
		protected List<CartesianTriangularMeshMapLayer> filterMap(Map map)
		{
			List<CartesianTriangularMeshMapLayer> imageMapLayers = new ArrayList<CartesianTriangularMeshMapLayer>();
			for(AbstractMapLayer layer : map.getLayers())
			{
				if(layer instanceof CartesianTriangularMeshMapLayer)
				{
					imageMapLayers.add(((CartesianTriangularMeshMapLayer) layer));
				}
			}
			
			return imageMapLayers;
		}
	}	
}

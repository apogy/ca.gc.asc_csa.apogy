package ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dComposite;
import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dListComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianCoordinatesPolygonShapeImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.AbstractShapeImageLayerComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.ImageMapLayerPreviewComposite;

public class CartesianCoordinatesPolygonShapeImageMapLayerWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.CartesianCoordinatesPolygonShapeImageMapLayerWizardPage";

	@SuppressWarnings("unused")
	private AbstractShapeImageLayerComposite  abstractShapeImageLayerComposite;
		
	private CartesianCoordinatesPolygonShapeImageMapLayer cartesianCoordinatesPolygonShapeImageMapLayer;
		
	private Tuple3dListComposite tuple3dListComposite;
	private Tuple3dComposite tuple3dComposite;
	private ImageMapLayerPreviewComposite imageMapLayerPreviewComposite;						
	private DataBindingContext m_bindingContext;
	
	public CartesianCoordinatesPolygonShapeImageMapLayerWizardPage(CartesianCoordinatesPolygonShapeImageMapLayer cartesianCoordinatesPolygonShapeImageMapLayer) 
	{
		super(WIZARD_PAGE_ID);
		this.cartesianCoordinatesPolygonShapeImageMapLayer = cartesianCoordinatesPolygonShapeImageMapLayer;
			
		setTitle("Ellipse Image Layer Settings.");
		setDescription("Sets the Ellipse Image Layer settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(2, false));
		
		Group layerSizeGroup = new Group(top, SWT.None);
		layerSizeGroup.setLayout(new GridLayout(2, false));
		layerSizeGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		layerSizeGroup.setText("Layer General Settings");
		
		abstractShapeImageLayerComposite = new AbstractShapeImageLayerComposite(layerSizeGroup, SWT.NONE, cartesianCoordinatesPolygonShapeImageMapLayer, false)
		{
			@Override
			protected void imageResolutionChanged(double newImageWidth) {
				validate();
			}
			
			@Override
			protected void imageHeightChanged(double newImageWidth) {
				validate();
			}
			
			@Override
			protected void imageWidthChanged(double newImageWidth) {
				validate();
			}
		};
		
		// Coordinates.
		Group coordinatesGroup = new Group(top, SWT.None);
		coordinatesGroup.setLayout(new GridLayout(2, false));
		coordinatesGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		coordinatesGroup.setText("Polygon Coordinates");
	
		tuple3dListComposite = new Tuple3dListComposite(coordinatesGroup, SWT.NONE, "", cartesianCoordinatesPolygonShapeImageMapLayer, ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_COORDINATES_POLYGON_SHAPE_IMAGE_MAP_LAYER__POLYGON_VERTICES)
		{
			@Override
			protected void newTuple3dSelected(Tuple3d tuple3d) 
			{
				tuple3dComposite.setTuple3d(tuple3d);
			}
		};
		GridData gd_tuple3dListComposite = new GridData(SWT.FILL, SWT.FILL, false, true);
		gd_tuple3dListComposite.verticalSpan = 2;
		tuple3dListComposite.setLayoutData(gd_tuple3dListComposite);
		
		Label lblSelectedTuple3d = new Label(coordinatesGroup, SWT.NONE);
		lblSelectedTuple3d.setText("Selected Point:");
		tuple3dComposite = new Tuple3dComposite(coordinatesGroup, SWT.NONE, ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(cartesianCoordinatesPolygonShapeImageMapLayer), "0.000");
		tuple3dComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		
		// Image Preview.
		Group grImagePreview = new Group(top, SWT.BORDER);
		grImagePreview.setText("Image Preview");
		grImagePreview.setLayout(new GridLayout(3, false));
		grImagePreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		imageMapLayerPreviewComposite = new ImageMapLayerPreviewComposite(grImagePreview, SWT.NONE, cartesianCoordinatesPolygonShapeImageMapLayer);
		imageMapLayerPreviewComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));		
					
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	

	protected void validate()
	{
		setErrorMessage(null);
		
		if(cartesianCoordinatesPolygonShapeImageMapLayer.getRequiredResolution() <= 0)
		{
			setErrorMessage("Invalid image resolution specified <" + cartesianCoordinatesPolygonShapeImageMapLayer.getRequiredResolution() + "> must be larger than zero.");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
		// TODO
		
		return bindingContext;
	}
}

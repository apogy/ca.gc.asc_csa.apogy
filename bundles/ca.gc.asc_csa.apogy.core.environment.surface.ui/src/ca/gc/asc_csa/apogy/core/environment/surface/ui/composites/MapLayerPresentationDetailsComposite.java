package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.MapLayerPresentation;

public class MapLayerPresentationDetailsComposite extends Composite 
{
	private MapLayerPresentation mapLayerPresentation;
	private Composite compositeDetails;
	
	public MapLayerPresentationDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.minimumWidth = 300;
		gd_compositeDetails.widthHint = 300;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));	
	}

	public MapLayerPresentation getMapLayerPresentation() {
		return mapLayerPresentation;
	}

	public void setMapLayerPresentation(MapLayerPresentation newMapLayerPresentation) 
	{
		this.mapLayerPresentation = newMapLayerPresentation;
				
		// Update Details EMFForm.
		if(newMapLayerPresentation != null)
		{
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, newMapLayerPresentation);
		}
	}		
}

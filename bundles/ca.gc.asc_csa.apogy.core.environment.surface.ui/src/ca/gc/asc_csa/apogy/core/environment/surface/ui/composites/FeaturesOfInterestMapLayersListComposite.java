package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCoreFacade;
import ca.gc.asc_csa.apogy.core.ApogyCoreFactory;
import ca.gc.asc_csa.apogy.core.FeatureOfInterest;
import ca.gc.asc_csa.apogy.core.environment.surface.AbstractMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.FeaturesOfInterestMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.Activator;


public class FeaturesOfInterestMapLayersListComposite extends Composite 
{	
	public static String path = System.getProperty("user.home");
	
	private Map map;
	
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;
	private Button btnImport;
	private Button btnExport;
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;
	
	public FeaturesOfInterestMapLayersListComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(2, false));
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_tree.minimumWidth = 200;
		gd_tree.widthHint = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CustomContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newFeaturesOfInterestMapLayerSelected((FeaturesOfInterestMapLayer)((IStructuredSelection) event.getSelection()).getFirstElement());					
			}
		});
		
		// Buttons.
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(composite, SWT.NONE);
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{
				if (event.type == SWT.Selection) 
				{					
					Wizard wizard = new ApogyEObjectWizard(ApogySurfaceEnvironmentPackage.Literals.MAP__LAYERS, getMap(), null, ApogySurfaceEnvironmentPackage.Literals.FEATURES_OF_INTEREST_MAP_LAYER);								
					WizardDialog dialog = new WizardDialog(getShell(), wizard);
					dialog.open();
										
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(getMap());
					}
				}			
			}
		});
				
		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				String foiMapLayersToDeleteMessage = "";

				Iterator<FeaturesOfInterestMapLayer> foiMapsLayers = getSelectedFeatureOfInterestMapLayers().iterator();
				while (foiMapsLayers.hasNext()) 
				{
					FeaturesOfInterestMapLayer foiMapLayer = foiMapsLayers.next();
					foiMapLayersToDeleteMessage = foiMapLayersToDeleteMessage + foiMapLayer.getName();

					if (foiMapsLayers.hasNext()) 
					{
						foiMapLayersToDeleteMessage = foiMapLayersToDeleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected FOI Map Layers", null,
						"Are you sure to delete these FOI Map Layers: " + foiMapLayersToDeleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					for (FeaturesOfInterestMapLayer layer :  getSelectedFeatureOfInterestMapLayers()) 
					{
						try 
						{
							ApogySurfaceEnvironmentFacade.INSTANCE.deleteLayerFromMap(layer.getMap(), layer);
						} 
						catch (Exception e)	
						{
							Logger.INSTANCE.log(Activator.ID,
									"Unable to delete the FOI Map Layer <"+ layer.getName() + ">",
									EventSeverity.ERROR, e);
						}
					}
				}
				
				// Forces the viewer to refresh its input.
				if(!treeViewer.isBusy())
				{					
					treeViewer.setInput(getMap());
				}					
			}
		});
		
		// Spacer
		new Label(composite, SWT.NONE);
		
		btnImport = new Button(composite, SWT.NONE);
		btnImport.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnImport.setSize(74, 29);
		btnImport.setText("Import");
		btnImport.setToolTipText("Import a list of Feature Of Interest from file.");
		btnImport.setEnabled(true);
		btnImport.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				FileDialog fd = new FileDialog(getShell(), SWT.OPEN);
				fd.setFilterPath(path);
				fd.setFilterExtensions(new String[]{"*.csv"});				
				fd.setOverwrite(true);
				
				String filePath = fd.open();
				
				if(filePath != null)
				{
					String urlString = "file://localhost/" + filePath.replace("\\", "/");	
					
					try
					{		
						List<FeatureOfInterest> foiList = ApogyCoreFacade.INSTANCE.loadFeatureOfInterestFromFile(urlString);
						
						if(foiList.size() > 0)
						{
							Logger.INSTANCE.log(Activator.ID, this, "Loaded <" + foiList.size() + "> Feature of Interest from file<" + urlString + ">.", EventSeverity.OK);							
							
							FeaturesOfInterestMapLayer layer = ApogySurfaceEnvironmentFactory.eINSTANCE.createFeaturesOfInterestMapLayer();
							layer.setFeatures(ApogyCoreFactory.eINSTANCE.createFeatureOfInterestList());
							layer.getFeatures().getFeaturesOfInterest().addAll(foiList);
							layer.setName(ApogyCommonEMFFacade.INSTANCE.getDefaultName(getMap(), layer, ApogySurfaceEnvironmentPackage.Literals.MAP__LAYERS));
							layer.setDescription("Feature Of Interest imported from file <" + filePath + ">.");
							
							ApogyCommonTransactionFacade.INSTANCE.basicAdd(getMap(), ApogySurfaceEnvironmentPackage.Literals.MAP__LAYERS, layer);							
							
							// Forces the viewer to refresh its input.
							if(!treeViewer.isBusy())
							{					
								treeViewer.setInput(getMap());
							}
						}
						else
						{
							Logger.INSTANCE.log(Activator.ID, this, "Could not load any Feature of Interest from file<" + urlString + ">!", EventSeverity.WARNING);
						}
						
					}
					catch (Exception ex) 
					{
						Logger.INSTANCE.log(Activator.ID, this, "Failed to import Feature of Interests from file<" + urlString + ">!", EventSeverity.ERROR, ex);
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {				
			}
		});
		
		btnExport = new Button(composite, SWT.NONE);
		btnExport.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnExport.setSize(74, 29);
		btnExport.setText("Export");
		btnExport.setToolTipText("Export the selected list of Feature Of Interest to file.");
		btnExport.setEnabled(false);
		btnExport.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{				
				
				Iterator<FeaturesOfInterestMapLayer> foiMapsLayers = getSelectedFeatureOfInterestMapLayers().iterator();
				while (foiMapsLayers.hasNext()) 
				{
					FeaturesOfInterestMapLayer foiMapLayer = foiMapsLayers.next();
					
					FileDialog fd = new FileDialog(getShell(), SWT.SAVE);
					fd.setText("Save Features Of Interest Map Layer named <" + foiMapLayer.getName() + ">.");
					fd.setFilterPath(path);
					fd.setFilterExtensions(new String[]{"*.csv"});
					fd.setFileName("unnamed.csv");
					fd.setOverwrite(true);										
					
					String filePath = fd.open();
					
					if(filePath != null)
					{
						String urlString = "file://localhost/" + filePath.replace("\\", "/");	
						
						try
						{	
							ApogyCoreFacade.INSTANCE.saveFeatureOfInterestToFile(filePath, foiMapLayer.getFeatures().getFeaturesOfInterest());
						}
						catch (Exception ex) 
						{
							Logger.INSTANCE.log(Activator.ID, this, "Failed to export Feature of Interests from file<" + urlString + ">!", EventSeverity.ERROR, ex);
						}
					}
				}								
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {				
			}
		});
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
	
	public Map getMap() {
		return map;
	}

	public void setMap(Map newMap) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.map = newMap;
		
		if(newMap != null)
		{
			m_bindingContext = customInitDataBindings();
			treeViewer.setInput(newMap);
			
			List<FeaturesOfInterestMapLayer> layers = filterMap(newMap);
			if(!layers.isEmpty())
			{
				treeViewer.setSelection(new StructuredSelection(layers.get(0)), true);
			}
			else
			{
				treeViewer.setSelection(null, true);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<FeaturesOfInterestMapLayer> getSelectedFeatureOfInterestMapLayers()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	

	protected void newFeaturesOfInterestMapLayerSelected(FeaturesOfInterestMapLayer newFeaturesOfInterestMapLayer)
	{		
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		/* Export Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnExportObserveWidget = WidgetProperties.enabled().observe(btnExport);
		bindingContext.bindValue(observeEnabledBtnExportObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		return bindingContext;
	}
	
	protected List<FeaturesOfInterestMapLayer> filterMap(Map map)
	{
		List<FeaturesOfInterestMapLayer> layers = new ArrayList<FeaturesOfInterestMapLayer>();
		for(AbstractMapLayer layer : map.getLayers())
		{
			if(layer instanceof FeaturesOfInterestMapLayer)
			{
				layers.add(((FeaturesOfInterestMapLayer) layer));
			}
		}
		
		return layers;
	}
	
	private class CustomContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof Map)
			{								
				Map map = (Map) inputElement;
								
				// Keeps only FeaturesOfInterestMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof Map)
			{								
				Map map = (Map) parentElement;
				
				// Keeps only FeaturesOfInterestMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof Map)
			{
				Map map = (Map) element;		
				return !filterMap(map).isEmpty();
			}		
			else
			{
				return false;
			}
		}
	}
}

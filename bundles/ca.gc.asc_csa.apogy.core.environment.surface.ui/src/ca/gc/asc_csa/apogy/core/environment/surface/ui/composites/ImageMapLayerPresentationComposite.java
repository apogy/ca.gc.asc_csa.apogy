package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayerPresentation;

public class ImageMapLayerPresentationComposite extends Composite 
{
	private ImageMapLayerPresentation imageMapLayerPresentation;
	
	private Button btnVisible;
	private Spinner spinnerAlpha;
	
	private DataBindingContext m_bindingContext;
	
	public ImageMapLayerPresentationComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));
		
		Label lblVisible = new Label(this, SWT.NONE);
		lblVisible.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblVisible.setText("Visible :");
		
		btnVisible = new Button(this, SWT.CHECK);
		
		Label lblAplha = new Label(this, SWT.NONE);
		lblAplha.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAplha.setText("Alpha (%):");
		
		spinnerAlpha = new Spinner(this, SWT.BORDER);
		GridData gd_spinner = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_spinner.minimumWidth = 50;
		gd_spinner.widthHint = 50;
		spinnerAlpha.setLayoutData(gd_spinner);
		
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{				
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}

	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Visible.*/
		IObservableValue<Double> observeVisible = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getImageMapLayerPresentation()), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.MAP_LAYER_PRESENTATION__VISIBLE)).observe(getImageMapLayerPresentation());
		IObservableValue<String> observeVisibleButton = WidgetProperties.selection().observe(btnVisible);
				
		bindingContext.bindValue(observeVisibleButton,
								 observeVisible, 
								 new UpdateValueStrategy(),	
								 new UpdateValueStrategy());
	
		/* Alpha.*/

		IObservableValue<Double> observeAlpha = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getImageMapLayerPresentation()), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.IMAGE_MAP_LAYER_PRESENTATION__ALPHA)).observe(getImageMapLayerPresentation());
		IObservableValue<String> observeAlphaSpinner = WidgetProperties.selection().observe(spinnerAlpha);				
				
		bindingContext.bindValue(observeAlphaSpinner,
								observeAlpha, 
				 new UpdateValueStrategy().setConverter(new Converter(Integer.class, Float.class)
				 {																		
					@Override
					public Object convert(Object fromObject) 
					{												
						Integer spinnerValue = (Integer) fromObject;
						return spinnerValue * 0.01f;
					}

					}), 
				 	new UpdateValueStrategy().setConverter(new Converter(Float.class, Integer.class)
				 	{																		 											
						@Override
						public Object convert(Object fromObject) 
						{														
							Float value = (Float) fromObject;
							Integer intValue = Math.round(value * 100.0f);
							return intValue;
						}

					}));
		
		return bindingContext;
	}
	
	public ImageMapLayerPresentation getImageMapLayerPresentation() {
		return imageMapLayerPresentation;
	}

	public void setImageMapLayerPresentation(ImageMapLayerPresentation imageMapLayerPresentation) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.imageMapLayerPresentation = imageMapLayerPresentation;
		
		if(imageMapLayerPresentation != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}
	
}

package ca.gc.asc_csa.apogy.core.environment.surface.ui.dialogs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;

import ca.gc.asc_csa.apogy.core.FeatureOfInterest;
import ca.gc.asc_csa.apogy.core.environment.surface.FeaturesOfInterestMapLayer;

public class FeatureOfInterestSelectionDialog extends Dialog 
{
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);	
	private List<FeaturesOfInterestMapLayer> availableLayers = null;
	private List<FeatureOfInterest> selectedFeatureOfInterestList = new ArrayList<FeatureOfInterest>();	
	private TreeViewer treeViewer;
	
	/**
	 * Creates an FeatureOfInterestSelectionDialog.
	 * @param parentShell The parent Shell.
	 * @param layers The list of FeaturesOfInterestMapLayer from which to select FeaturesOfInterest.
	 */
	public FeatureOfInterestSelectionDialog(Shell parentShell, List<FeaturesOfInterestMapLayer> layers) 
	{
		super(parentShell);
		this.availableLayers = layers;
	}
	
	@Override
	protected void configureShell(Shell newShell) 
	{	
		super.configureShell(newShell);
		newShell.setText("Select one or more Feature Of Interest(s)");
	}
	
	@Override
	protected Control createDialogArea(Composite parent) 
	{
		Composite area = (Composite) super.createDialogArea(parent);
		
	    Composite container = new Composite(area, SWT.NONE);	    
	    container.setLayoutData(new GridData(GridData.FILL_BOTH));
	    
	    GridData containerGridData = new GridData(SWT.FILL, SWT.FILL, true, true);
	    containerGridData.minimumHeight = 300;
	    containerGridData.heightHint = 300;	  
	    
	    container.setLayoutData(containerGridData);
	    
	    GridLayout layout = new GridLayout(1, false);
	    container.setLayout(layout);	    
		
		treeViewer = new TreeViewer(container, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION | SWT.V_SCROLL);
		Tree tree = treeViewer.getTree();
		tree.setHeaderVisible(true);
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tree.setLinesVisible(true);
		ColumnViewerToolTipSupport.enableFor(treeViewer);
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				ISelection selection = event.getSelection();
				if(selection instanceof IStructuredSelection)
				{
					selectedFeatureOfInterestList.clear();
					IStructuredSelection iStructuredSelection = (IStructuredSelection) selection;
					
					Iterator<?> it = iStructuredSelection.iterator();
					while(it.hasNext())
					{
						Object next = it.next();
						if(next instanceof FeatureOfInterest)
						{
							selectedFeatureOfInterestList.add((FeatureOfInterest) next);
						}
					}					
				}
			}
		});	    
	    			
		TreeViewerColumn treeViewerColumnName = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnName = treeViewerColumnName.getColumn();
		trclmnName.setText("Layer");
		trclmnName.setWidth(350);
				
		TreeViewerColumn treeViewerFoiDescription = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnFOIDescription = treeViewerFoiDescription.getColumn();
		trclmnFOIDescription.setText("FOI Description");
		trclmnFOIDescription.setWidth(150);
				
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory) 
		{
			private final int LAYER_COLUMN_ID = 0;					
			private final int FOI_COLUMN__DESCRIPTION_ID = 1;

			@Override
			public String getColumnText(Object object, int columnIndex) 
			{
				String str = "<undefined>";
								
				switch (columnIndex) 
				{
					case LAYER_COLUMN_ID:
						str = super.getColumnText(object, columnIndex);
					break;
					case FOI_COLUMN__DESCRIPTION_ID:
						if (object instanceof FeatureOfInterest) 
						{
							str = ((FeatureOfInterest) object).getDescription();
						}
						else
						{
							str = "";
						}
					break;	
					
					default:
					break;
				}
				return str;
			}
			
			@Override
			public Image getColumnImage(Object object, int columnIndex) 			
			{
				Image image = null;
				switch (columnIndex) 
				{
					case LAYER_COLUMN_ID:
						image = super.getColumnImage(object, columnIndex);					
					break;
					
					case FOI_COLUMN__DESCRIPTION_ID:					
					break;					
					
					default:
					break;
				}
				return image;			
			}
		});

		treeViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory)
		{
			@Override
			public Object[] getElements(Object object) 
			{
				if(object instanceof List)
				{
					return ((List<?>) object).toArray();
				}
				else if(object instanceof FeatureOfInterest)
				{
					return null;
				}
				else
				{
					return super.getChildren(object);
				}
			}
			
			@Override
			public Object[] getChildren(Object object) 
			{
				if(object instanceof List)
				{
					return ((List<?>) object).toArray();
				}
				else if(object instanceof FeatureOfInterest)
				{
					return null;
				}
				else
				{
					return super.getChildren(object);
				}
			}
		});
		
		if(availableLayers != null) treeViewer.setInput(availableLayers);
		
		
		return area;
	}
	
	/**
	 * Returns the list of selected FeatureOfInterest.
	 * @return The list of selected FeatureOfInterest, never null, can be empty.
	 */
	public List<FeatureOfInterest> getSelectedFeatureOfInterest()
	{
		return selectedFeatureOfInterestList;
	}
	
	@Override
	protected void cancelPressed() 
	{
		selectedFeatureOfInterestList.clear();
		super.cancelPressed();
	}		
}

package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.Color3fComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.SlopeRange;

public class SlopeRangeComposite extends Composite 
{
	private SlopeRange slopeRange;
	
	private DataBindingContext m_bindingContext;
	private Text txtNamevalue;
	private Text txtDescriptionvalue;
	private Text txtLowerSlopeValue;
	private Text txtUpperSlopeValue;
	private Color3fComposite color3fComposite;
	
	public SlopeRangeComposite(Composite parent, int style) {
		super(parent, style);	
		
		setLayout(new GridLayout(2, false));		
		
		Label lblName = new Label(this, SWT.NONE);
		lblName.setAlignment(SWT.RIGHT);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name:");
		
		txtNamevalue = new Text(this, SWT.BORDER);
		GridData gd_txtNamevalue = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_txtNamevalue.minimumWidth = 300;
		gd_txtNamevalue.widthHint = 300;
		txtNamevalue.setLayoutData(gd_txtNamevalue);
		
		Label lblDescription = new Label(this, SWT.NONE);
		lblDescription.setAlignment(SWT.RIGHT);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDescription.setText("Description:");
		
		txtDescriptionvalue = new Text(this, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtDescriptionvalue = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_txtDescriptionvalue.minimumWidth = 300;
		gd_txtDescriptionvalue.widthHint = 300;
		gd_txtDescriptionvalue.minimumHeight = 50;
		gd_txtDescriptionvalue.heightHint = 50;
		txtDescriptionvalue.setLayoutData(gd_txtDescriptionvalue);	
		
		Label lblLowerSlope = new Label(this, SWT.NONE);
		lblLowerSlope.setAlignment(SWT.RIGHT);
		lblLowerSlope.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLowerSlope.setText("Slope Lower Bound (deg):");
		
		txtLowerSlopeValue = new Text(this, SWT.BORDER );
		GridData gd_txtLowerSlopeValue = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_txtLowerSlopeValue.minimumWidth = 100;
		gd_txtLowerSlopeValue.widthHint = 100;		
		txtLowerSlopeValue.setLayoutData(gd_txtLowerSlopeValue);	
		
		Label lblUpperSlope = new Label(this, SWT.NONE);
		lblUpperSlope.setAlignment(SWT.RIGHT);
		lblUpperSlope.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblUpperSlope.setText("Slope Upper Bound (deg):");
		
		txtUpperSlopeValue = new Text(this, SWT.BORDER);
		GridData gd_txtxUpperSlope = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_txtxUpperSlope.minimumWidth = 100;
		gd_txtxUpperSlope.widthHint = 100;		
		txtUpperSlopeValue.setLayoutData(gd_txtxUpperSlope);	
		
		Label lblColor = new Label(this, SWT.NONE);
		lblColor.setAlignment(SWT.RIGHT);
		lblColor.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblColor.setText("Color :");
	
		color3fComposite = new Color3fComposite(this, SWT.NONE, "", true, getSlopeRange(), ApogySurfaceEnvironmentPackage.Literals.SLOPE_RANGE__COLOR);	
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
	
	public SlopeRange getSlopeRange() 
	{
		return slopeRange;
	}

	public void setSlopeRange(SlopeRange newSlopeRange) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.slopeRange = newSlopeRange;
		
		if(newSlopeRange != null)
		{
			m_bindingContext = initDataBindingsCustom();
			
			if(color3fComposite != null)
			{
				color3fComposite.setOwner(newSlopeRange);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Name Value. */
		IObservableValue<Double> observeName = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getSlopeRange()), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.NAMED__NAME)).observe(getSlopeRange());
		IObservableValue<String> observeNameValueText = WidgetProperties.text(SWT.Modify).observe(txtNamevalue);

		bindingContext.bindValue(observeNameValueText,
								observeName, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		/* Description Value. */
		IObservableValue<Double> observeDescription = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getSlopeRange()), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.DESCRIBED__DESCRIPTION)).observe(getSlopeRange());
		IObservableValue<String> observeDescriptionText = WidgetProperties.text(SWT.Modify).observe(txtDescriptionvalue);

		bindingContext.bindValue(observeDescriptionText,
								 observeDescription, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		/* Lower Slope value. */
		IObservableValue<Double> observeLowerSlope = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getSlopeRange()), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.SLOPE_RANGE__SLOPE_LOWER_BOUND)).observe(getSlopeRange());
		IObservableValue<String> observeLowerSlopeText = WidgetProperties.text(SWT.Modify).observe(txtLowerSlopeValue);

		bindingContext.bindValue(observeLowerSlopeText,
								observeLowerSlope, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		/* Upper Slope value. */
		IObservableValue<Double> observeUpperSlope = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getSlopeRange()), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.SLOPE_RANGE__SLOPE_UPPER_BOUND)).observe(getSlopeRange());
		IObservableValue<String> observeUpperSlopeText = WidgetProperties.text(SWT.Modify).observe(txtUpperSlopeValue);

		bindingContext.bindValue(observeUpperSlopeText,
								observeUpperSlope, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));		
		return bindingContext;
	}
}

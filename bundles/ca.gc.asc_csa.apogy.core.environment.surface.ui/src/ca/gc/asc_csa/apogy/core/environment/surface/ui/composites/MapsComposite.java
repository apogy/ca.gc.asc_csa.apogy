package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.SurfaceWorksite;

public class MapsComposite extends Composite 
{
	private SurfaceWorksite surfaceWorksite;
	
	private FormToolkit formToolkit = new FormToolkit(Display.getCurrent());	
	private MapsListComposite mapsListComposite;
	private MapOverviewComposite mapOverviewComposite;
	private MapDetailsComposite mapDetailsComposite;
	
	public MapsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		ScrolledForm scrldfrmNewScrolledform = formToolkit.createScrolledForm(this);
		scrldfrmNewScrolledform.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrldfrmNewScrolledform.setShowFocusedControl(true);
		formToolkit.paintBordersFor(scrldfrmNewScrolledform);
		scrldfrmNewScrolledform.setText(null);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			tableWrapLayout.numColumns = 2;
			scrldfrmNewScrolledform.getBody().setLayout(tableWrapLayout);
		}
		
		// Worksites Section.
		Section sctnMapList = formToolkit.createSection(scrldfrmNewScrolledform.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnWorksites = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 2, 1);
		twd_sctnWorksites.valign = TableWrapData.FILL;		
		twd_sctnWorksites.grabVertical = true;
		sctnMapList.setLayoutData(twd_sctnWorksites);
		formToolkit.paintBordersFor(sctnMapList);
		sctnMapList.setText("Maps");
		
		mapsListComposite = new MapsListComposite(sctnMapList, SWT.NONE)
		{
			protected void newMapSelected(Map map)
			{		
				mapOverviewComposite.setMap(map);
				mapDetailsComposite.setMap(map);
				
				MapsComposite.this.newMapSelected(map);
			}
		};
		formToolkit.adapt(mapsListComposite);
		formToolkit.paintBordersFor(mapsListComposite);
		sctnMapList.setClient(mapsListComposite);		
		
		Section sctnOverview = formToolkit.createSection(scrldfrmNewScrolledform.getBody(), Section.TWISTIE | Section.TITLE_BAR| Section.EXPANDED);
		formToolkit.paintBordersFor(sctnOverview);
		sctnOverview.setText("Map Overview");
		sctnOverview.setExpanded(true);
		
		mapOverviewComposite = new MapOverviewComposite(sctnOverview, SWT.NONE);
		formToolkit.adapt(mapOverviewComposite);
		formToolkit.paintBordersFor(mapOverviewComposite);
		sctnOverview.setClient(mapOverviewComposite);
		
		Section sctnDetails = formToolkit.createSection(scrldfrmNewScrolledform.getBody(), Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnDetails = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 1, 1);
		twd_sctnDetails.align = TableWrapData.FILL;
		sctnDetails.setLayoutData(twd_sctnDetails);
		formToolkit.paintBordersFor(sctnDetails);
		sctnDetails.setText("Map Details");
		sctnDetails.setExpanded(false);
		
		mapDetailsComposite = new MapDetailsComposite(sctnDetails, SWT.NONE);
		formToolkit.adapt(mapDetailsComposite);
		formToolkit.paintBordersFor(mapDetailsComposite);
		sctnDetails.setClient(mapDetailsComposite);
		
		scrldfrmNewScrolledform.reflow(true);
	}

	public SurfaceWorksite getSurfaceWorksite() 
	{
		return surfaceWorksite;
	}

	public void setSurfaceWorksite(SurfaceWorksite newSurfaceWorksite) 
	{
		this.surfaceWorksite = newSurfaceWorksite;
		
		if(newSurfaceWorksite != null)
		{
			mapsListComposite.setMapsList(newSurfaceWorksite.getMapsList());
		}
		else
		{
			mapsListComposite.setMapsList(null);
		}	
	}
	
	protected void newMapSelected(Map map)
	{		
	}
}

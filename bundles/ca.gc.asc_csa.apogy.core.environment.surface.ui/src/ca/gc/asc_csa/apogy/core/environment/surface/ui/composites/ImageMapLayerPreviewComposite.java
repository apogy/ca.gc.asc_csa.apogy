package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.images.ui.ImagesUiUtilities;
import ca.gc.asc_csa.apogy.common.images.ui.composites.ImageDisplayComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayer;

public class ImageMapLayerPreviewComposite extends Composite 
{
	private Adapter adapter = null;
	private ImageMapLayer imageMapLayer;
	
	private Label lblImageWidthValue;	
	private Button btnGenerateImage;
	private Label lblImageHeightValue;
	private Button btnExportImage;

	private Composite containerComposite;	
	private ImageDisplayComposite imageDisplayComposite;	
	
	private DataBindingContext m_bindingContext;
	
	public ImageMapLayerPreviewComposite(Composite parent, int style) 
	{
		this(parent, style, null);
	}
	public ImageMapLayerPreviewComposite(Composite parent, int style, ImageMapLayer imageMapLayer) 
	{
		super(parent, style);
		setLayout(new GridLayout(3,false));
				
		Label lblImageWidth = new Label(this, SWT.NONE);
		lblImageWidth.setText("Image Width (pixels):");
		lblImageWidth.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		
		lblImageWidthValue = new Label(this, SWT.BORDER);		
		GridData gd_lblImageWidthValue = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_lblImageWidthValue.widthHint = 100;
		gd_lblImageWidthValue.minimumWidth = 100;
		lblImageWidthValue.setLayoutData(gd_lblImageWidthValue);
				
		btnGenerateImage = new Button(this, SWT.PUSH);
		GridData gd_btnGenerateImage = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnGenerateImage.minimumWidth = 120;
		gd_btnGenerateImage.widthHint = 120;
		btnGenerateImage.setLayoutData(gd_btnGenerateImage);
		btnGenerateImage.setText("Update Image");
		btnGenerateImage.setToolTipText("Update the image based on the selected setting. Note that this can take several minutes depending on the processing involved.");
		btnGenerateImage.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				IRunnableWithProgress runnable = new IRunnableWithProgress() {
					
					@Override
					public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException 
					{					
						if(getImageMapLayer() != null)
						{
							getImageMapLayer().updateImage(monitor);							
						}
					}
				};
				
				try 
				{
					new ProgressMonitorDialog(getShell()).run(true, true, runnable);
				} 
				catch (Throwable t) 
				{
					t.printStackTrace();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {						
			}
		});
		
		Label lblImageHeight = new Label(this, SWT.NONE);	
		lblImageHeight.setText("Image Height (pixels):");
		lblImageHeight.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		
		lblImageHeightValue = new Label(this, SWT.BORDER);
		GridData gd_lblImageHeightValue = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_lblImageHeightValue.widthHint = 100;
		gd_lblImageHeightValue.minimumWidth = 100;
		lblImageHeightValue.setLayoutData(gd_lblImageHeightValue);		

		btnExportImage = new Button(this, SWT.PUSH);
		GridData gd_btnExportImage = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnExportImage.minimumWidth = 120;
		gd_btnExportImage.widthHint = 120;
		btnExportImage.setLayoutData(gd_btnExportImage);
		btnExportImage.setText("Export Image...");
		btnExportImage.setToolTipText("Exports the layer image to file.");
		btnExportImage.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				try
				{
					if(getImageMapLayer() != null)
					{						
						ImageData imageData = EImagesUtilities.INSTANCE.convertToImageData(getImageMapLayer().getImage().asBufferedImage());
						
						String defaultName = null;
						if(getImageMapLayer().getName() != null && getImageMapLayer().getName().length() > 1)
						{
							defaultName = getImageMapLayer().getName() + ".jpg";
						}
						
						ImagesUiUtilities.export(imageData, defaultName);
					}
				}
				catch (Throwable t) 
				{
					t.printStackTrace();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {						
			}
		});
		
		containerComposite = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 3, 1);
		gd_compositeDetails.widthHint = 398;
		gd_compositeDetails.minimumWidth = 398;
		containerComposite.setLayoutData(gd_compositeDetails);
		containerComposite.setLayout(new GridLayout(1, false));	
			
		imageDisplayComposite = new ImageDisplayComposite(containerComposite, SWT.BORDER);
		GridData gd_imageDisplayComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_imageDisplayComposite.heightHint = 200;
		gd_imageDisplayComposite.minimumHeight = 200;
		gd_imageDisplayComposite.widthHint = 200;
		gd_imageDisplayComposite.minimumWidth = 200;
		imageDisplayComposite.setLayoutData(gd_imageDisplayComposite);
		imageDisplayComposite.addListener(SWT.Resize, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{
				imageDisplayComposite.fitImage();
			}
		});		
		
		setImageMapLayer(imageMapLayer);
		
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (m_bindingContext != null) 
				{
					m_bindingContext.dispose();
				}
				
				if(imageMapLayer != null)
				{
					imageMapLayer.eAdapters().remove(getAdapter());
				}
				
			}
		});
	}
	
	public ImageMapLayer getImageMapLayer() {
		return imageMapLayer;
	}

	public void setImageMapLayer(ImageMapLayer newImageMapLayer) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		if(this.imageMapLayer != null)
		{
			this.imageMapLayer.eAdapters().remove(getAdapter());
		}
		
		this.imageMapLayer = newImageMapLayer;
		
		if(newImageMapLayer != null)
		{
			m_bindingContext = customInitDataBindings();
			
			newImageMapLayer.eAdapters().add(getAdapter());
			updateImage(newImageMapLayer.getImage());			
		}
		else
		{
			updateImage(null);
		}
	}
	
	private void updateImage(AbstractEImage abstractEImage)
	{
		if(abstractEImage != null)
		{
			lblImageWidthValue.setText("0");
			lblImageHeightValue.setText("0");
			
			ImageData imageData = EImagesUtilities.INSTANCE.convertToImageData(imageMapLayer.getImage().asBufferedImage());
			
			if(imageData != null)
			{
				lblImageWidthValue.setText(Integer.toString(imageMapLayer.getImage().getWidth()));
				lblImageHeightValue.setText(Integer.toString(imageMapLayer.getImage().getHeight()));
				setImagePreview(imageData);
			}
			else
			{
				setNoImageToDisplayMessage("Map layer contains no image.");	
			}
		}
		else
		{
			setNoImageToDisplayMessage("Map layer contains no image.");	
		}
	}
	
	private void setImagePreview(ImageData imageData)
	{
		for (Control control : containerComposite.getChildren()) 
		{
			control.dispose();
		}	
		
		imageDisplayComposite = new ImageDisplayComposite(containerComposite, SWT.NONE);		
		GridData gd = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd.minimumWidth = 200;
		gd.widthHint = 200;
		gd.heightHint = 200;
		gd.minimumWidth = 200;		
		imageDisplayComposite.setLayoutData(gd);
		imageDisplayComposite.addListener(SWT.Resize, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{
				imageDisplayComposite.fitImage();
			}
		});		
		
		imageDisplayComposite.setImageData(imageData);
		imageDisplayComposite.fitImage();
		containerComposite.layout();
		containerComposite.redraw();
	}
	
	private void setNoImageToDisplayMessage(String message)
	{		
		for (Control control : containerComposite.getChildren()) 
		{
			control.dispose();
		}		
		NoContentComposite noContentComposite = new NoContentComposite(containerComposite, SWT.None) {
			@Override
			protected String getMessage() {
				return message;
			}
		};
		
		GridData gd = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd.minimumWidth = 200;
		gd.widthHint = 200;
		gd.heightHint = 200;
		gd.minimumWidth = 200;	
		noContentComposite.setLayoutData(gd);	
		containerComposite.layout();
	}
	
	private Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof ImageMapLayer)
					{
						int featureID = msg.getFeatureID(ImageMapLayer.class);
						if(featureID == ApogySurfaceEnvironmentPackage.IMAGE_MAP_LAYER__IMAGE)
						{										
							if(!ImageMapLayerPreviewComposite.this.isDisposed())
							{
								ImageMapLayerPreviewComposite.this.getDisplay().asyncExec(new Runnable() 
								{								
									@Override
									public void run() 
									{									
										if(msg.getNewValue() instanceof AbstractEImage)
										{
											updateImage((AbstractEImage) msg.getNewValue());
										}
										else
										{
											updateImage(null);
										}
									}
								});
							}
						}
					}
				}
			};
		}
		return adapter;
	}
	
	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<Double> observeImageMapLayerImage = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getImageMapLayer()), 
				  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.IMAGE_MAP_LAYER__IMAGE)).observe(getImageMapLayer());
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnExportImage);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeImageMapLayerImage, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		return bindingContext;
	}
}

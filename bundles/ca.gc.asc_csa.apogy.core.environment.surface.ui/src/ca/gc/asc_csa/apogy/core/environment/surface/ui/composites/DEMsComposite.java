package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.MapLayerPresentation;

public class DEMsComposite extends Composite 
{
	private Map map;
	
	private DEMListComposite demListComposite;
	private DEMOverviewComposite demOverviewComposite;
	private DEMDetailsComposite demDetailsComposite;
	private DEMPresentationComposite demPresentationComposite;
	
	private MapLayerPresentationListComposite mapLayerPresentationListComposite;
	private MapLayerPresentationDetailsComposite mapLayerPresentationDetailsComposite;
	private MapLayerPresentationOverviewComposite mapLayerPresentationOverviewComposite;
	
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	public DEMsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, true));
		
		ScrolledForm scrldfrmDEM = formToolkit.createScrolledForm(this);
		scrldfrmDEM.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrldfrmDEM);
		scrldfrmDEM.setText("DEM Layers");
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.numColumns = 2;
			tableWrapLayout.makeColumnsEqualWidth = true;
			scrldfrmDEM.getBody().setLayout(tableWrapLayout);
		}
		
		Section sctnDemlist = formToolkit.createSection(scrldfrmDEM.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnDemlist = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 3, 1);
		twd_sctnDemlist.valign = TableWrapData.FILL;
		twd_sctnDemlist.grabVertical = true;
		sctnDemlist.setLayoutData(twd_sctnDemlist);
		formToolkit.paintBordersFor(sctnDemlist);
		sctnDemlist.setText("DEMs");
		
		demListComposite = new DEMListComposite(sctnDemlist, SWT.NONE)
		{
			@Override
			protected void newCartesianTriangularMeshMapLayerSelected(CartesianTriangularMeshMapLayer cartesianTriangularMeshMapLayer) 
			{
				demOverviewComposite.setCartesianTriangularMeshMapLayer(cartesianTriangularMeshMapLayer);
				demDetailsComposite.setCartesianTriangularMeshMapLayer(cartesianTriangularMeshMapLayer);
				demPresentationComposite.setCartesianTriangularMeshMapLayer(cartesianTriangularMeshMapLayer);
				mapLayerPresentationListComposite.setCartesianTriangularMeshMapLayer(cartesianTriangularMeshMapLayer);
			}
		};		
		formToolkit.adapt(demListComposite);
		formToolkit.paintBordersFor(demListComposite);
		sctnDemlist.setClient(demListComposite);
		
		
		Section sctnDemoverview = formToolkit.createSection(scrldfrmDEM.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnDemoverview);
		sctnDemoverview.setText("DEM Overview");
		
		demOverviewComposite = new DEMOverviewComposite(sctnDemoverview, SWT.NONE);
		formToolkit.adapt(demOverviewComposite);
		formToolkit.paintBordersFor(demOverviewComposite);
		sctnDemoverview.setClient(demOverviewComposite);				
				
		Section sctnDemdetails = formToolkit.createSection(scrldfrmDEM.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnDemdetails);
		sctnDemdetails.setText("DEM Details");
		
		demDetailsComposite = new DEMDetailsComposite(sctnDemdetails, SWT.NONE);
		formToolkit.adapt(demDetailsComposite);
		formToolkit.paintBordersFor(demDetailsComposite);
		sctnDemdetails.setClient(demDetailsComposite);		
		
		Section sctnDemPresentation = formToolkit.createSection(scrldfrmDEM.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnDemPresentation);
		sctnDemPresentation.setText("DEM Presentation");
		
		demPresentationComposite = new DEMPresentationComposite(sctnDemPresentation, SWT.NONE);
		formToolkit.adapt(demPresentationComposite);
		formToolkit.paintBordersFor(demPresentationComposite);
		sctnDemPresentation.setClient(demPresentationComposite);		
		
		/////////////// Layers
		
		ScrolledForm scrldfrmLayers = formToolkit.createScrolledForm(this);
		scrldfrmLayers.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrldfrmLayers);
		scrldfrmLayers.setText("Layer Presentations");
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.numColumns = 2;
			tableWrapLayout.makeColumnsEqualWidth = true;
			scrldfrmLayers.getBody().setLayout(tableWrapLayout);
		}
		
		Section sctnMapLayersList = formToolkit.createSection(scrldfrmLayers.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnMapLayersList = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 2, 1);
		twd_sctnMapLayersList.valign = TableWrapData.FILL;
		twd_sctnMapLayersList.grabVertical = true;
		sctnMapLayersList.setLayoutData(twd_sctnMapLayersList);
		formToolkit.paintBordersFor(sctnMapLayersList);
		sctnMapLayersList.setText("Layers");
		
		mapLayerPresentationListComposite = new MapLayerPresentationListComposite(sctnMapLayersList, SWT.NONE)
		{
			@Override
			protected void newMapLayerPresentationSelected(MapLayerPresentation newMapLayerPresentation) 
			{
				mapLayerPresentationDetailsComposite.setMapLayerPresentation(newMapLayerPresentation);
				mapLayerPresentationOverviewComposite.setMapLayerPresentation(newMapLayerPresentation);
			}
		};
		formToolkit.adapt(mapLayerPresentationListComposite);
		formToolkit.paintBordersFor(mapLayerPresentationListComposite);
		sctnMapLayersList.setClient(mapLayerPresentationListComposite);
		
		Section sctnLayerOverview = formToolkit.createSection(scrldfrmLayers.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnLayerOverview);
		sctnLayerOverview.setText("Layer Overview");
				
		mapLayerPresentationOverviewComposite = new MapLayerPresentationOverviewComposite(sctnLayerOverview, SWT.NONE);
		formToolkit.adapt(mapLayerPresentationOverviewComposite);
		formToolkit.paintBordersFor(mapLayerPresentationOverviewComposite);
		sctnLayerOverview.setClient(mapLayerPresentationOverviewComposite);	
		
		Section sctnLayerDetails = formToolkit.createSection(scrldfrmLayers.getBody(), Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnNewSection_1 = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 1, 1);
		twd_sctnNewSection_1.align = TableWrapData.FILL;
		sctnLayerDetails.setLayoutData(twd_sctnNewSection_1);
		formToolkit.paintBordersFor(sctnLayerDetails);
		sctnLayerDetails.setText("Layer Details");
				
		mapLayerPresentationDetailsComposite = new MapLayerPresentationDetailsComposite(sctnLayerDetails,  SWT.NONE);
		formToolkit.adapt(mapLayerPresentationDetailsComposite);
		formToolkit.paintBordersFor(mapLayerPresentationDetailsComposite);
		sctnLayerDetails.setClient(mapLayerPresentationDetailsComposite);
			

	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map newMap) 
	{		
		this.map = newMap;
		demListComposite.setMap(newMap);
		demDetailsComposite.setCartesianTriangularMeshMapLayer(null);	
		demPresentationComposite.setCartesianTriangularMeshMapLayer(null);
	}
}

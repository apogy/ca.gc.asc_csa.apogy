package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.math.ui.composites.TransformMatrixComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;

public class MapOverviewComposite extends Composite 
{
	private ca.gc.asc_csa.apogy.core.environment.surface.Map map;
	
	private DataBindingContext m_bindingContext;
	private Text txtNamevalue;
	private Text txtDescriptionvalue;
	private TransformMatrixComposite transformMatrixComposite;
	
	public MapOverviewComposite(Composite parent, int style) 
	{
		super(parent, SWT.NO_BACKGROUND);	
		setLayout(new GridLayout(2, false));		
		
		Label lblName = new Label(this, SWT.NONE);
		lblName.setAlignment(SWT.RIGHT);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblName.setText("Name:");
		
		txtNamevalue = new Text(this, SWT.BORDER);
		GridData gd_txtNamevalue = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_txtNamevalue.minimumWidth = 300;
		gd_txtNamevalue.widthHint = 300;
		txtNamevalue.setLayoutData(gd_txtNamevalue);
		
		Label lblDescription = new Label(this, SWT.NONE);
		lblDescription.setAlignment(SWT.RIGHT);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblDescription.setText("Description:");
		
		txtDescriptionvalue = new Text(this, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtDescriptionvalue = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_txtDescriptionvalue.minimumWidth = 300;
		gd_txtDescriptionvalue.widthHint = 300;
		gd_txtDescriptionvalue.minimumHeight = 50;
		gd_txtDescriptionvalue.heightHint = 50;
		txtDescriptionvalue.setLayoutData(gd_txtDescriptionvalue);
		
		Label lblPositionm = new Label(this, SWT.RIGHT);
		lblPositionm.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblPositionm.setAlignment(SWT.RIGHT);
		lblPositionm.setText("Pose :");
		transformMatrixComposite = new TransformMatrixComposite(this, SWT.NONE, ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain());
		GridData gd_transformMatrixComposite = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_transformMatrixComposite.widthHint = 300;
		gd_transformMatrixComposite.minimumWidth = 300;
		transformMatrixComposite.setLayoutData(gd_transformMatrixComposite);
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();				
			}
		});
			
	}

	public Map getMap() 
	{
		return map;
	}

	public void setMap(Map map) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.map = map;
		
		if(map != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		/* Name Value. */
		IObservableValue<Double> observeName = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.NAMED__NAME)).observe(getMap());
		IObservableValue<String> observeNameValueText = WidgetProperties.text(SWT.Modify).observe(txtNamevalue);

		bindingContext.bindValue(observeNameValueText,
								observeName, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		/* Description Value. */
		IObservableValue<Double> observeDescription = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.DESCRIBED__DESCRIPTION)).observe(getMap());
		IObservableValue<String> observeDescriptionText = WidgetProperties.text(SWT.Modify).observe(txtDescriptionvalue);

		bindingContext.bindValue(observeDescriptionText,
								 observeDescription, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		// Pose
		transformMatrixComposite.setMatrix4x4(getMap().getTransformation());
		
		return bindingContext;
	}
}

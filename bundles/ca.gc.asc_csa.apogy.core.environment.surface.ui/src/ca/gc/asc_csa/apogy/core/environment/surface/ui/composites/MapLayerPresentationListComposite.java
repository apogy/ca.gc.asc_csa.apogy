package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.MapLayerPresentation;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.Activator;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings;


public class MapLayerPresentationListComposite extends Composite 
{	
	private CartesianTriangularMeshMapLayer cartesianTriangularMeshMapLayer;
	
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;	
	
	private Button btnUp;
	private Button btnDown;
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;
	
	public MapLayerPresentationListComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(2, false));
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_tree.minimumWidth = 200;
		gd_tree.widthHint = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CompositeFilterContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newMapLayerPresentationSelected((MapLayerPresentation)((IStructuredSelection) event.getSelection()).getFirstElement());					
			}
		});				

		
		// Buttons.
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(composite, SWT.NONE);
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{
				if (event.type == SWT.Selection) 
				{					
					MapLayerPresentationUISettings settings = ApogySurfaceEnvironmentUIFactory.eINSTANCE.createMapLayerPresentationUISettings();						
					settings.setName(ApogyCommonEMFFacade.INSTANCE.getDefaultName(getCartesianTriangularMeshMapLayer(), null, ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_MAP_LAYER__MAP_LAYER_PRESENTATIONS));
					settings.setCartesianTriangularMeshMapLayer(getCartesianTriangularMeshMapLayer());
					
					Wizard wizard = new ApogyEObjectWizard(ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_MAP_LAYER__MAP_LAYER_PRESENTATIONS, getCartesianTriangularMeshMapLayer(), settings, ApogySurfaceEnvironmentPackage.Literals.IMAGE_MAP_LAYER_PRESENTATION); 
					WizardDialog dialog = new WizardDialog(getShell(), wizard);
					dialog.open();
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(getCartesianTriangularMeshMapLayer());
					}					
				}
			}
		});
				
		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				String mapLayerPresentationsToDeleteMessage = "";

				Iterator<MapLayerPresentation> mapLayerPresentations = getSelectedMapLayerPresentations().iterator();
				while (mapLayerPresentations.hasNext()) 
				{
					MapLayerPresentation mapLayerPresentation = mapLayerPresentations.next();
					mapLayerPresentationsToDeleteMessage = mapLayerPresentationsToDeleteMessage + mapLayerPresentation.getName();

					if (mapLayerPresentations.hasNext()) 
					{
						mapLayerPresentationsToDeleteMessage = mapLayerPresentationsToDeleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected Map Layer Presentations", null,
						"Are you sure to delete these Map Layer Presentations: " + mapLayerPresentationsToDeleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					for (MapLayerPresentation mapLayerPresentation :  getSelectedMapLayerPresentations()) 
					{
						try 
						{
							ApogyCommonTransactionFacade.INSTANCE.basicRemove(getCartesianTriangularMeshMapLayer(), ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_MAP_LAYER__MAP_LAYER_PRESENTATIONS, mapLayerPresentation);
						} 
						catch (Exception e)	
						{
							Logger.INSTANCE.log(Activator.ID,
									"Unable to delete the Map Layer Presentations <"+ mapLayerPresentation.getName() + ">",
									EventSeverity.ERROR, e);
						}
					}
				}
				
				// Forces the viewer to refresh its input.
				if(!treeViewer.isBusy())
				{					
					treeViewer.setInput(getCartesianTriangularMeshMapLayer());
				}					
			}
		});
		
		btnUp = new Button(composite, SWT.NONE);
		btnUp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnUp.setSize(74, 29);
		btnUp.setText("Up");
		btnUp.setEnabled(false);
		btnUp.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				if(getSelectedMapLayerPresentations().size() > 0)
				{
					MapLayerPresentation mapLayerPresentation = getSelectedMapLayerPresentations().get(0);
					
					int index = getCartesianTriangularMeshMapLayer().getMapLayerPresentations().indexOf(mapLayerPresentation);					
					
					if(index > 0)
					{
						getCartesianTriangularMeshMapLayer().getMapLayerPresentations().move(index-1, index);
						
						// Forces the viewer to refresh its input.
						if(!treeViewer.isBusy())
						{					
							treeViewer.setInput(getCartesianTriangularMeshMapLayer());
						}	
					}						
				}					
			}
		});
		
		btnDown = new Button(composite, SWT.NONE);
		btnDown.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDown.setSize(74, 29);
		btnDown.setText("Down");
		btnDown.setEnabled(false);
		btnDown.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				if(getSelectedMapLayerPresentations().size() > 0)
				{
					MapLayerPresentation mapLayerPresentation = getSelectedMapLayerPresentations().get(0);
					
					int listSize = getCartesianTriangularMeshMapLayer().getMapLayerPresentations().size();
					int index = getCartesianTriangularMeshMapLayer().getMapLayerPresentations().indexOf(mapLayerPresentation);					
					
					if(index < (listSize -1))
					{
						getCartesianTriangularMeshMapLayer().getMapLayerPresentations().move(index+1, index);
						
						// Forces the viewer to refresh its input.
						if(!treeViewer.isBusy())
						{					
							treeViewer.setInput(getCartesianTriangularMeshMapLayer());
						}	
					}						
				}	
			}
		});
		
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	public CartesianTriangularMeshMapLayer getCartesianTriangularMeshMapLayer() {
		return cartesianTriangularMeshMapLayer;
	}

	public void setCartesianTriangularMeshMapLayer(CartesianTriangularMeshMapLayer newMap) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();		
		}
		
		this.cartesianTriangularMeshMapLayer = newMap;
		
		if(newMap != null)
		{
			m_bindingContext = customInitDataBindings();
			treeViewer.setInput(newMap);
			
			if(!newMap.getMapLayerPresentations().isEmpty())
			{
				treeViewer.setSelection(new StructuredSelection(newMap.getMapLayerPresentations().get(0)), true); 
			}
			else
			{
				treeViewer.setSelection(null, true);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<MapLayerPresentation> getSelectedMapLayerPresentations()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	protected void newMapLayerPresentationSelected(MapLayerPresentation newMapLayerPresentation)
	{		
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		/* Up Button Enabled Binding. */
		IObservableValue<?> observeBtnUpObserveWidget = WidgetProperties.enabled().observe(btnUp);
		bindingContext.bindValue(observeBtnUpObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		/* Down Button Enabled Binding. */
		IObservableValue<?> observeBtnDownObserveWidget = WidgetProperties.enabled().observe(btnDown);
		bindingContext.bindValue(observeBtnDownObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		return bindingContext;
	}
	
	private class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof CartesianTriangularMeshMapLayer)
			{								
				CartesianTriangularMeshMapLayer meshMapLayer = (CartesianTriangularMeshMapLayer) inputElement;												
				return meshMapLayer.getMapLayerPresentations().toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof CartesianTriangularMeshMapLayer)
			{								
				CartesianTriangularMeshMapLayer meshMapLayer = (CartesianTriangularMeshMapLayer) parentElement;												
				return meshMapLayer.getMapLayerPresentations().toArray();
			}								
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof CartesianTriangularMeshMapLayer)
			{								
				CartesianTriangularMeshMapLayer meshMapLayer = (CartesianTriangularMeshMapLayer) element;												
				return !meshMapLayer.getMapLayerPresentations().isEmpty();
			}
			else
			{
				return false;
			}
		}				
	}
}

package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.core.environment.surface.AbstractMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.Activator;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.CartesianTriangularMeshMapLayerUISettings;


public class DEMListComposite extends Composite 
{	
	private Map map;
	
	private boolean enableEditing = true;
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;	
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;
	
	public DEMListComposite(Composite parent, int style) 
	{
		this(parent, style, true);
	}
	
	public DEMListComposite(Composite parent, int style, boolean enableEditing) 
	{
		super(parent, style);
		this.enableEditing = enableEditing;
		
		if(enableEditing)
		{
			setLayout(new GridLayout(2, false));
		}
		else
		{
			setLayout(new GridLayout(1, false));
		}
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CompositeFilterContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newCartesianTriangularMeshMapLayerSelected((CartesianTriangularMeshMapLayer)((IStructuredSelection) event.getSelection()).getFirstElement());					
			}
		});
		
		if(enableEditing)
		{
			// Buttons.
			Composite composite = new Composite(this, SWT.NONE);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
			composite.setLayout(new GridLayout(1, false));	
			
			btnNew = new Button(composite, SWT.NONE);
			btnNew.setSize(74, 29);
			btnNew.setText("New");
			btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnNew.setEnabled(true);
			btnNew.addListener(SWT.Selection, new Listener() 
			{		
				@Override
				public void handleEvent(Event event) 
				{				
					if (event.type == SWT.Selection) 
					{					
						CartesianTriangularMeshMapLayerUISettings settings = ApogySurfaceEnvironmentUIFactory.eINSTANCE.createCartesianTriangularMeshMapLayerUISettings();						
						settings.setName(ApogyCommonEMFFacade.INSTANCE.getDefaultName(getMap(), null, ApogySurfaceEnvironmentPackage.Literals.MAP__LAYERS));
						
						Wizard wizard = new ApogyEObjectWizard(ApogySurfaceEnvironmentPackage.Literals.MAP__LAYERS, getMap(), settings, ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_MAP_LAYER); 
						WizardDialog dialog = new WizardDialog(getShell(), wizard);
						dialog.open();
						
						// Forces the viewer to refresh its input.
						if(!treeViewer.isBusy())
						{					
							treeViewer.setInput(getMap());
						}					
					}
				}
			});
					
			btnDelete = new Button(composite, SWT.NONE);
			btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnDelete.setSize(74, 29);
			btnDelete.setText("Delete");
			btnDelete.setEnabled(false);
			btnDelete.addSelectionListener(new SelectionAdapter() 
			{
				@Override
				public void widgetSelected(SelectionEvent event) 
				{
					String meshLayersToDeleteMessage = "";
	
					Iterator<CartesianTriangularMeshMapLayer> meshLayers = getSelectedCartesianTriangularMeshMapLayers().iterator();
					while (meshLayers.hasNext()) 
					{
						CartesianTriangularMeshMapLayer meshLayer = meshLayers.next();
						meshLayersToDeleteMessage = meshLayersToDeleteMessage + meshLayer.getName();
	
						if (meshLayers.hasNext()) 
						{
							meshLayersToDeleteMessage = meshLayersToDeleteMessage + ", ";
						}
					}
	
					MessageDialog dialog = new MessageDialog(null, "Delete the selected Cartesian Triangular Mesh Map Layers", null,
							"Are you sure to delete these Cartesian Triangular Mesh Map Layers: " + meshLayersToDeleteMessage, MessageDialog.QUESTION,
							new String[] { "Yes", "No" }, 1);
					int result = dialog.open();
					if (result == 0) 
					{
						for (CartesianTriangularMeshMapLayer layer :  getSelectedCartesianTriangularMeshMapLayers()) 
						{
							try 
							{
								ApogySurfaceEnvironmentFacade.INSTANCE.deleteLayerFromMap(layer.getMap(), layer);
							} 
							catch (Exception e)	
							{
								Logger.INSTANCE.log(Activator.ID,
										"Unable to delete the Cartesian Triangular Mesh Map Layer <"+ layer.getName() + ">",
										EventSeverity.ERROR, e);
							}
						}
					}
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(getMap());
					}					
				}
			});
		}
		
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
	
	public Map getMap() {
		return map;
	}

	public void setMap(Map newMap) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.map = newMap;
		
		if(newMap != null)
		{
			m_bindingContext = customInitDataBindings();
			treeViewer.setInput(newMap);
			
			CartesianTriangularMeshMapLayer layer = getFirstCartesianTriangularMeshMapLayer(newMap);
			if(layer != null)
			{
				treeViewer.setSelection(new StructuredSelection(layer), true);
			}
		}
	}

	public void setSelectedCartesianTriangularMeshMapLayers(CartesianTriangularMeshMapLayer layer)
	{
		if(layer != null)
		{
			treeViewer.setSelection(new StructuredSelection(layer), true);
		}
		else
		{
			treeViewer.setSelection(null, true);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CartesianTriangularMeshMapLayer> getSelectedCartesianTriangularMeshMapLayers()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	
	protected CartesianTriangularMeshMapLayer getFirstCartesianTriangularMeshMapLayer(Map map)
	{
		List<CartesianTriangularMeshMapLayer> maps = filterMap(map);
		if(!maps.isEmpty())
		{
			return maps.get(0);
		}
		else
		{
			return null;
		}
	}
	
	protected void newCartesianTriangularMeshMapLayerSelected(CartesianTriangularMeshMapLayer cartesianTriangularMeshMapLayer)
	{		
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		if(enableEditing)
		{
			IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
			
			/* Delete Button Enabled Binding. */
			IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
			bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
					new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
							.setConverter(new Converter(Object.class, Boolean.class) {
								@Override
								public Object convert(Object fromObject) {
									return fromObject != null;
								}
							}));
		}
		return bindingContext;
	}
		
	private List<CartesianTriangularMeshMapLayer> filterMap(Map map)
	{
		List<CartesianTriangularMeshMapLayer> maps = new ArrayList<CartesianTriangularMeshMapLayer>();
		for(AbstractMapLayer layer : map.getLayers())
		{
			if(layer instanceof CartesianTriangularMeshMapLayer)
			{
				maps.add(((CartesianTriangularMeshMapLayer) layer));
			}
		}
		
		return maps;
	}
	
	private class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof Map)
			{								
				Map map = (Map) inputElement;
								
				// Keeps only CartesianTriangularMeshMapLayers.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof Map)
			{								
				Map map = (Map) parentElement;
				
				// Keeps only CartesianTriangularMeshMapLayers.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof Map)
			{
				Map map = (Map) element;		
				return !filterMap(map).isEmpty();
			}		
			else
			{
				return false;
			}
		}
	}
}

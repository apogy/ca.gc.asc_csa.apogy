package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayer;

public class ImageMapLayerDetailsComposite extends Composite 
{
	private ImageMapLayer imageMapLayer;
		
	private Composite compositeDetails;
	
	public ImageMapLayerDetailsComposite(Composite parent, int style) 
	{		
		super(parent, style);
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.minimumWidth = 398;
		gd_compositeDetails.widthHint = 398;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));		
	}

	public ImageMapLayer getImageMapLayer() {
		return imageMapLayer;
	}

	public void setImageMapLayer(ImageMapLayer newImageMapLayer) 
	{
		this.imageMapLayer = newImageMapLayer;
	
		// Update Details EMFForm.
		if(newImageMapLayer != null)
		{
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, newImageMapLayer);
		}
	}		
}

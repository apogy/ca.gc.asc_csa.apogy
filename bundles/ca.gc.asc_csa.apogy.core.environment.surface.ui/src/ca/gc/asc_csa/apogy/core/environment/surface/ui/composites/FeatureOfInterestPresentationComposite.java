package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.converters.ApogyCommonConvertersFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import ca.gc.asc_csa.apogy.core.FeatureOfInterest;
import ca.gc.asc_csa.apogy.core.FeatureOfInterestNode;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIFacade;

public class FeatureOfInterestPresentationComposite extends Composite
{
	private FeatureOfInterest featureOfInterest;		
	private Composite compositeDetails;

	public FeatureOfInterestPresentationComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.widthHint = 402;
		gd_compositeDetails.minimumWidth = 402;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));			
	}

	public FeatureOfInterest getFeatureOfInterest() {
		return featureOfInterest;
	}

	public void setFeatureOfInterest(FeatureOfInterest newFeatureOfInterest) 
	{
		this.featureOfInterest = newFeatureOfInterest;
		
		// Update Details EMFForm.
		NodePresentation presentation = null;
		if(newFeatureOfInterest != null)
		{
			FeatureOfInterestNode node = ApogyCoreUIFacade.INSTANCE.getFeatureOfInterestNode(featureOfInterest);
			
			if(node != null)
			{
				presentation = (NodePresentation) ApogyCommonConvertersFacade.INSTANCE.convert(node, NodePresentation.class);				
			}			
		}
		ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, presentation, "No presentation information available.");
	}
}

package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;

public class DEMDetailsComposite extends Composite 
{
	private CartesianTriangularMeshMapLayer cartesianTriangularMeshMapLayer;
	
	private Composite compositeDetails;
	
	// CartesianTriangularMeshDerivedImageMapLayerImagePreviewComposite
	
	public DEMDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.widthHint = 398;
		gd_compositeDetails.minimumWidth = 398;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));	
	}

	public CartesianTriangularMeshMapLayer getCartesianTriangularMeshMapLayer() {
		return cartesianTriangularMeshMapLayer;
	}

	public void setCartesianTriangularMeshMapLayer(CartesianTriangularMeshMapLayer newCartesianTriangularMeshMapLayer) 
	{
		this.cartesianTriangularMeshMapLayer = newCartesianTriangularMeshMapLayer;
		// Update Details EMFForm.
		if(newCartesianTriangularMeshMapLayer != null)
		{
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, newCartesianTriangularMeshMapLayer);
		}
	}		
}

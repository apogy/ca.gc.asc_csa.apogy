package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.core.FeatureOfInterest;
import ca.gc.asc_csa.apogy.core.environment.surface.FeaturesOfInterestMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.ui.composites.FeatureOfInterestDetailsComposite;
import ca.gc.asc_csa.apogy.core.ui.composites.FeatureOfInterestListComposite;
import ca.gc.asc_csa.apogy.core.ui.composites.FeatureOfInterestOverviewComposite;

public class FOIsComposite extends Composite 
{
	private Map map;
	
	private FeaturesOfInterestMapLayersListComposite featuresOfInterestMapLayersListComposite;
	private FeaturesOfInterestMapLayerOverviewComposite featuresOfInterestMapLayerOverviewComposite;
	private FeaturesOfInterestMapLayerDetailsComposite featuresOfInterestMapLayerDetailsComposite;
	private FeaturesOfInterestMapLayerPresentationComposite featuresOfInterestMapLayerPresentationComposite;
	
	private FeatureOfInterestListComposite featureOfInterestListComposite;	
	private FeatureOfInterestOverviewComposite featureOfInterestOverviewComposite;
	private FeatureOfInterestDetailsComposite featureOfInterestDetailsComposite;
	private FeatureOfInterestPresentationComposite featureOfInterestPresentationComposite;
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	public FOIsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, true));
		
		ScrolledForm scrldfrmFeatureofinterestlayers = formToolkit.createScrolledForm(this);
		scrldfrmFeatureofinterestlayers.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrldfrmFeatureofinterestlayers);
		scrldfrmFeatureofinterestlayers.setText("Feature Of Interest (FOI) Layers");
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.numColumns = 2;
			tableWrapLayout.makeColumnsEqualWidth = true;
			scrldfrmFeatureofinterestlayers.getBody().setLayout(tableWrapLayout);
		}
		
		Section sctnFeatureofinterestlist = formToolkit.createSection(scrldfrmFeatureofinterestlayers.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnFeatureofinterestlist = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 3, 1);
		twd_sctnFeatureofinterestlist.valign = TableWrapData.FILL;
		twd_sctnFeatureofinterestlist.grabVertical = true;
		sctnFeatureofinterestlist.setLayoutData(twd_sctnFeatureofinterestlist);
		formToolkit.paintBordersFor(sctnFeatureofinterestlist);
		sctnFeatureofinterestlist.setText("FOI Layers");
		
		featuresOfInterestMapLayersListComposite = new FeaturesOfInterestMapLayersListComposite(sctnFeatureofinterestlist, SWT.NONE)
		{
			@Override
			protected void newFeaturesOfInterestMapLayerSelected(FeaturesOfInterestMapLayer newFeaturesOfInterestMapLayer)
			{		
				featuresOfInterestMapLayerDetailsComposite.setFeaturesOfInterestMapLayer(newFeaturesOfInterestMapLayer);
				featuresOfInterestMapLayerOverviewComposite.setFeaturesOfInterestMapLayer(newFeaturesOfInterestMapLayer);
				featuresOfInterestMapLayerPresentationComposite.setFeaturesOfInterestMapLayer(newFeaturesOfInterestMapLayer);
				
				if(newFeaturesOfInterestMapLayer != null)
				{
					featureOfInterestListComposite.setFeatureOfInterestList(newFeaturesOfInterestMapLayer.getFeatures());
				}
			}
		};
		formToolkit.adapt(featuresOfInterestMapLayersListComposite);
		formToolkit.paintBordersFor(featuresOfInterestMapLayersListComposite);
		sctnFeatureofinterestlist.setClient(featuresOfInterestMapLayersListComposite);
	
				
		Section sctnFoiLayerOverview = formToolkit.createSection(scrldfrmFeatureofinterestlayers.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnFoiLayerOverview);
		sctnFoiLayerOverview.setText("FOI Layer Overview");		
		
		featuresOfInterestMapLayerOverviewComposite = new FeaturesOfInterestMapLayerOverviewComposite(sctnFoiLayerOverview, SWT.NONE);
		formToolkit.adapt(featuresOfInterestMapLayerOverviewComposite);
		formToolkit.paintBordersFor(featuresOfInterestMapLayerOverviewComposite);
		sctnFoiLayerOverview.setClient(featuresOfInterestMapLayerOverviewComposite);
				
		Section sctnFoiLayerDetails = formToolkit.createSection(scrldfrmFeatureofinterestlayers.getBody(), Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnFoiLayerDetails);
		sctnFoiLayerDetails.setText("FOI Layer Details");
		
		featuresOfInterestMapLayerDetailsComposite = new FeaturesOfInterestMapLayerDetailsComposite(sctnFoiLayerDetails, SWT.NONE);
		formToolkit.adapt(featuresOfInterestMapLayerDetailsComposite);
		formToolkit.paintBordersFor(featuresOfInterestMapLayerDetailsComposite);
		sctnFoiLayerDetails.setClient(featuresOfInterestMapLayerDetailsComposite);
			
		Section sctnFoiLayerPresentation = formToolkit.createSection(scrldfrmFeatureofinterestlayers.getBody(), Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnFoiLayerPresentation);
		sctnFoiLayerPresentation.setText("FOI Layer Presentation");

		featuresOfInterestMapLayerPresentationComposite = new FeaturesOfInterestMapLayerPresentationComposite(sctnFoiLayerPresentation, SWT.NONE);
		formToolkit.adapt(featuresOfInterestMapLayerPresentationComposite);
		formToolkit.paintBordersFor(featuresOfInterestMapLayerPresentationComposite);
		sctnFoiLayerPresentation.setClient(featuresOfInterestMapLayerPresentationComposite);
		
		///////////////////
						
		ScrolledForm scrldfrmNewScrolledform = formToolkit.createScrolledForm(this);
		scrldfrmNewScrolledform.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrldfrmNewScrolledform);
		scrldfrmNewScrolledform.setText("Features Of Interest");
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.numColumns = 2;
			tableWrapLayout.makeColumnsEqualWidth = true;
			scrldfrmNewScrolledform.getBody().setLayout(tableWrapLayout);
		}
		
		Section sctnFois = formToolkit.createSection(scrldfrmNewScrolledform.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnFois = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 3, 1);
		twd_sctnFois.grabVertical = true;
		twd_sctnFois.valign = TableWrapData.FILL;
		sctnFois.setLayoutData(twd_sctnFois);
		formToolkit.paintBordersFor(sctnFois);
		sctnFois.setText("FOIs");
		
		featureOfInterestListComposite = new FeatureOfInterestListComposite(sctnFois, SWT.NONE)
		{
			@Override
			protected void newFeatureOfInterestSelected(FeatureOfInterest newFeatureOfInterest) 
			{
				featureOfInterestOverviewComposite.setFeatureOfInterest(newFeatureOfInterest);
				featureOfInterestDetailsComposite.setFeatureOfInterest(newFeatureOfInterest);
				featureOfInterestPresentationComposite.setFeatureOfInterest(newFeatureOfInterest);
			}
		};
		formToolkit.adapt(featureOfInterestListComposite);
		formToolkit.paintBordersFor(featureOfInterestListComposite);
		sctnFois.setClient(featureOfInterestListComposite);
		
		Section sctnFoiOverview = formToolkit.createSection(scrldfrmNewScrolledform.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnFoiOverview);
		sctnFoiOverview.setText("FOI Overview");
		
		featureOfInterestOverviewComposite = new FeatureOfInterestOverviewComposite(sctnFoiOverview, SWT.NONE);
		formToolkit.adapt(featureOfInterestOverviewComposite);
		formToolkit.paintBordersFor(featureOfInterestOverviewComposite);
		sctnFoiOverview.setClient(featureOfInterestOverviewComposite);		
			
					
		Section sctnFoiDetails = formToolkit.createSection(scrldfrmNewScrolledform.getBody(), Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnFoiDetails);
		sctnFoiDetails.setText("FOI Details");
		
		featureOfInterestDetailsComposite = new FeatureOfInterestDetailsComposite(sctnFoiDetails, SWT.NONE);
		formToolkit.adapt(featureOfInterestDetailsComposite);
		formToolkit.paintBordersFor(featureOfInterestDetailsComposite);
		sctnFoiDetails.setClient(featureOfInterestDetailsComposite);
		
		Section sctnFoiPresentation = formToolkit.createSection(scrldfrmNewScrolledform.getBody(), Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnFoiPresentation);
		sctnFoiPresentation.setText("FOI Presentation");
		
		featureOfInterestPresentationComposite = new FeatureOfInterestPresentationComposite(sctnFoiPresentation, SWT.NONE);
		formToolkit.adapt(featureOfInterestPresentationComposite);
		formToolkit.paintBordersFor(featureOfInterestPresentationComposite);
		sctnFoiPresentation.setClient(featureOfInterestPresentationComposite);
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map newMap) 
	{		
		this.map = newMap;
		featuresOfInterestMapLayersListComposite.setMap(newMap);
		featuresOfInterestMapLayerDetailsComposite.setFeaturesOfInterestMapLayer(null);	
		featureOfInterestPresentationComposite.setFeatureOfInterest(null);
	}
}

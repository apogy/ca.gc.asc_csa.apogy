package ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.AbstractMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshHeightImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ImageMapLayerUISettings;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.ImageMapLayerPreviewComposite;

public class CartesianTriangularMeshHeightImageMapLayerWizardPage extends WizardPage 
{	
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.CartesianTriangularMeshHeightImageMapLayerWizardPage";
	
	private CartesianTriangularMeshHeightImageMapLayer cartesianTriangularMeshHeightImageMapLayer;
	@SuppressWarnings("unused")
	private ImageMapLayerUISettings uiSettings;
		
	private Button btnAutoScale;
	private Text txtMinimumHeight;
	private Text txtMaximumHeight;
		
	private ImageMapLayerPreviewComposite imagePreviewComposite;
	
	
	private DataBindingContext m_bindingContext;
					
	public CartesianTriangularMeshHeightImageMapLayerWizardPage(CartesianTriangularMeshHeightImageMapLayer cartesianTriangularMeshHeightImageMapLayer, 	
																 ImageMapLayerUISettings uiSettings) 
	{
		super(WIZARD_PAGE_ID);
		
		setTitle("Mesh Height Image Layer Settings.");
		
		this.cartesianTriangularMeshHeightImageMapLayer = cartesianTriangularMeshHeightImageMapLayer;
		this.uiSettings = uiSettings;
		
		setTitle("Height Image Layer");
		setDescription("Configure the height image generation settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(2, false));
		
		Label lblMinimumSlope = new Label(container, SWT.NONE);
		lblMinimumSlope.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMinimumSlope.setText("Minimum Height (m):");
		
		txtMinimumHeight = new Text(container, SWT.BORDER);
		GridData gd_txtMinimumSlope = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_txtMinimumSlope.minimumWidth = 100;
		gd_txtMinimumSlope.widthHint = 100;
		txtMinimumHeight.setLayoutData(gd_txtMinimumSlope);
		txtMinimumHeight.setToolTipText("The minimum height to use to generate the color scale. This is applicable only if autoScale is enabled.");
		txtMinimumHeight.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {				
				validate();
			}
			
			@Override
			public void keyPressed(KeyEvent e) {				
				validate();
			}
		});
		
		Label lblMaximumSlope = new Label(container, SWT.NONE);
		lblMaximumSlope.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMaximumSlope.setText("Maximum Height (m):");
			
		txtMaximumHeight = new Text(container, SWT.BORDER);
		GridData gd_txtMaximumSlope = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtMaximumSlope.minimumWidth = 100;
		gd_txtMaximumSlope.widthHint = 100;
		txtMaximumHeight.setLayoutData(gd_txtMaximumSlope);
		txtMaximumHeight.setToolTipText("The maximum height to use to generate the color scale. This is applicable only if autoScale is enabled.");
		txtMaximumHeight.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {				
				validate();
			}
			
			@Override
			public void keyPressed(KeyEvent e) {				
				validate();
			}
		});
		
		Label lblAutoScale = new Label(container, SWT.NONE);
		lblAutoScale.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAutoScale.setText("Auto Scale:");
		
		btnAutoScale = new Button(container, SWT.CHECK);
		btnAutoScale.setAlignment(SWT.RIGHT);
		btnAutoScale.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		btnAutoScale.setText("");
		btnAutoScale.setToolTipText("Whether or not to use autoscalling to generate the color scale.");
			
		Group imageGroup = new Group(container, SWT.BORDER);
		imageGroup.setLayout(new GridLayout(1, false));
		imageGroup.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 2, 1));
		imageGroup.setText("Derived Image");		
		
		imagePreviewComposite = new ImageMapLayerPreviewComposite(imageGroup, SWT.NONE, cartesianTriangularMeshHeightImageMapLayer);
		GridData gd_imagePreviewComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_imagePreviewComposite.widthHint = 500;
		gd_imagePreviewComposite.heightHint = 350;
		gd_imagePreviewComposite.minimumHeight = 350;
		gd_imagePreviewComposite.minimumWidth = 500;
		imagePreviewComposite.setLayoutData(gd_imagePreviewComposite);
		imagePreviewComposite.setImageMapLayer(cartesianTriangularMeshHeightImageMapLayer);
		
		setControl(container);				
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
				
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		
		validate();
		
		// Dispose
		container.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);
				
		if(cartesianTriangularMeshHeightImageMapLayer.getMinimumHeight() >= cartesianTriangularMeshHeightImageMapLayer.getMaximumHeight())
		{
			setErrorMessage("Maximum height must be greater than the minimum height !");	
		}		
		
		setPageComplete(getErrorMessage() == null);
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Minimum Slope Value. */
		IObservableValue<Double> observeMinimumSlope = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(cartesianTriangularMeshHeightImageMapLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_HEIGHT_IMAGE_MAP_LAYER__MINIMUM_HEIGHT)).observe(cartesianTriangularMeshHeightImageMapLayer);
		IObservableValue<String> observeMinimumSlopeText = WidgetProperties.text(SWT.Modify).observe(txtMinimumHeight);

		bindingContext.bindValue(observeMinimumSlopeText,
								observeMinimumSlope, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		/* Maximum Slope Value. */
		IObservableValue<Double> observeMaximumSlope = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(cartesianTriangularMeshHeightImageMapLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_HEIGHT_IMAGE_MAP_LAYER__MAXIMUM_HEIGHT)).observe(cartesianTriangularMeshHeightImageMapLayer);
		IObservableValue<String> observeMaximumSlopeText = WidgetProperties.text(SWT.Modify).observe(txtMaximumHeight);

		bindingContext.bindValue(observeMaximumSlopeText,
								observeMaximumSlope, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		/* Autoscale.*/
		IObservableValue<Double> observeAutoscale = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(cartesianTriangularMeshHeightImageMapLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_HEIGHT_IMAGE_MAP_LAYER__AUTO_SCALE)).observe(cartesianTriangularMeshHeightImageMapLayer);
		IObservableValue<String> observeAutoscaleButtton = WidgetProperties.selection().observe(btnAutoScale);
				
		bindingContext.bindValue(observeAutoscaleButtton,
								 observeAutoscale, 
								 new UpdateValueStrategy(),	
								 new UpdateValueStrategy());		
		
		return bindingContext;
	}
	
	public class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof Map)
			{								
				Map map = (Map) inputElement;
								
				// Keeps only ImageMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof Map)
			{								
				Map map = (Map) parentElement;
				
				// Keeps only ImageMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof Map)
			{
				Map map = (Map) element;		
				return !filterMap(map).isEmpty();
			}		
			else
			{
				return false;
			}
		}
		
		protected List<CartesianTriangularMeshMapLayer> filterMap(Map map)
		{
			List<CartesianTriangularMeshMapLayer> imageMapLayers = new ArrayList<CartesianTriangularMeshMapLayer>();
			for(AbstractMapLayer layer : map.getLayers())
			{
				if(layer instanceof CartesianTriangularMeshMapLayer)
				{
					imageMapLayers.add(((CartesianTriangularMeshMapLayer) layer));
				}
			}
			
			return imageMapLayers;
		}
	}	
}

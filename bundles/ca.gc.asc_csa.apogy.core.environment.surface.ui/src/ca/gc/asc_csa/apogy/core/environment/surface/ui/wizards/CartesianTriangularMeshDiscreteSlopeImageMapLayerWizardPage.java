package ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import ca.gc.asc_csa.apogy.core.environment.surface.AbstractMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshDiscreteSlopeImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.SlopeRange;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ImageMapLayerUISettings;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.ImageMapLayerPreviewComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.SlopeRangeComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.SlopeRangeListComposite;

public class CartesianTriangularMeshDiscreteSlopeImageMapLayerWizardPage extends WizardPage 
{	
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.CartesianTriangularMeshDiscreteSlopeImageMapLayerWizardPage";
	
	private CartesianTriangularMeshDiscreteSlopeImageMapLayer cartesianTriangularMeshDiscreteSlopeImageMapLayer;
	@SuppressWarnings("unused")
	private ImageMapLayerUISettings uiSettings;
		
	private ImageMapLayerPreviewComposite imagePreviewComposite;	
	private SlopeRangeListComposite slopeRangeListComposite;
	private SlopeRangeComposite slopeRangeComposite;
	
	private DataBindingContext m_bindingContext;
					
	public CartesianTriangularMeshDiscreteSlopeImageMapLayerWizardPage(CartesianTriangularMeshDiscreteSlopeImageMapLayer cartesianTriangularMeshDiscreteSlopeImageMapLayer, 	
																 ImageMapLayerUISettings uiSettings) 
	{
		super(WIZARD_PAGE_ID);
		
		this.cartesianTriangularMeshDiscreteSlopeImageMapLayer = cartesianTriangularMeshDiscreteSlopeImageMapLayer;
		this.uiSettings = uiSettings;
		
		setTitle("Discrete Slope Image Layer");
		setDescription("Configure the discrete slope ranges used to generate the slope image.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(2, false));
					
		Group slopeRangesListGroup = new Group(container, SWT.BORDER);
		slopeRangesListGroup.setLayout(new GridLayout(1, false));
		slopeRangesListGroup.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		slopeRangesListGroup.setText("Slope Ranges");		
	
		slopeRangeListComposite = new SlopeRangeListComposite(slopeRangesListGroup, SWT.NONE)
		{
			@Override
			protected void newSlopeRangeSelected(SlopeRange newSlopeRange) 
			{				
				if(slopeRangeComposite != null)
				{
					slopeRangeComposite.setSlopeRange(newSlopeRange);
				}
			}
		};
		GridData gd_slopeRangeListComposite = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_slopeRangeListComposite.heightHint = 150;
		gd_slopeRangeListComposite.minimumHeight = 150;
		slopeRangeListComposite.setLayoutData(gd_slopeRangeListComposite);
		slopeRangeListComposite.setCartesianTriangularMeshDiscreteSlopeImageMapLayer(cartesianTriangularMeshDiscreteSlopeImageMapLayer);
		
		Group selectedSlopeRangeGroup = new Group(container, SWT.BORDER);
		selectedSlopeRangeGroup.setLayout(new GridLayout(1, false));
		selectedSlopeRangeGroup.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		selectedSlopeRangeGroup.setText("Slope Range Details");		

		slopeRangeComposite = new SlopeRangeComposite(selectedSlopeRangeGroup, SWT.NONE);
		slopeRangeComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		slopeRangeComposite.setSlopeRange(slopeRangeListComposite.getSelectedSlopeRange());
		
		Group imageGroup = new Group(container, SWT.BORDER);
		imageGroup.setLayout(new GridLayout(1, false));
		imageGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1));
		imageGroup.setText("Derived Image");		
		
		imagePreviewComposite = new ImageMapLayerPreviewComposite(imageGroup, SWT.NONE, cartesianTriangularMeshDiscreteSlopeImageMapLayer);
		GridData gd_imagePreviewComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_imagePreviewComposite.widthHint = 350;
		gd_imagePreviewComposite.heightHint = 350;
		gd_imagePreviewComposite.minimumHeight = 350;
		gd_imagePreviewComposite.minimumWidth = 350;
		imagePreviewComposite.setLayoutData(gd_imagePreviewComposite);
		imagePreviewComposite.setImageMapLayer(cartesianTriangularMeshDiscreteSlopeImageMapLayer);
		
		setControl(container);				
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
				
		validate();
		
		// Dispose
		container.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
		
	protected void validate()
	{
		setErrorMessage(null);
								
		// TODO Validate the Slope Ranges.
		
		setPageComplete(getErrorMessage() == null);				
	}
		
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		

		return bindingContext;
	}
	
	public class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof Map)
			{								
				Map map = (Map) inputElement;
								
				// Keeps only ImageMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof Map)
			{								
				Map map = (Map) parentElement;
				
				// Keeps only ImageMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof Map)
			{
				Map map = (Map) element;		
				return !filterMap(map).isEmpty();
			}		
			else
			{
				return false;
			}
		}
		
		protected List<CartesianTriangularMeshMapLayer> filterMap(Map map)
		{
			List<CartesianTriangularMeshMapLayer> imageMapLayers = new ArrayList<CartesianTriangularMeshMapLayer>();
			for(AbstractMapLayer layer : map.getLayers())
			{
				if(layer instanceof CartesianTriangularMeshMapLayer)
				{
					imageMapLayers.add(((CartesianTriangularMeshMapLayer) layer));
				}
			}
			
			return imageMapLayers;
		}
	}	
}

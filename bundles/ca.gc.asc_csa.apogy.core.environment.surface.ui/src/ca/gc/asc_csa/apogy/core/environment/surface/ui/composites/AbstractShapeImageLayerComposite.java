package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.Color3fComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.AbstractShapeImageLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;

public class AbstractShapeImageLayerComposite extends Composite 
{
	private AbstractShapeImageLayer abstractShapeImageLayer;
	@SuppressWarnings("unused")
	private boolean enableImageDimensions = true;
	
	private Text imageWidthInMeters;
	private Text imageHeightInMeters;
	private Text txtRequiredResolution;
	private Button btnShapedFilled;
	@SuppressWarnings("unused")
	private Color3fComposite color3fComposite;

	private DataBindingContext m_bindingContext;
	
	public AbstractShapeImageLayerComposite(Composite parent, int style) {
		this(parent, style, null, true);
	}
	
	public AbstractShapeImageLayerComposite(Composite parent, int style, AbstractShapeImageLayer abstractShapeImageLayer, boolean enableImageDimensions) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, false));
		this.enableImageDimensions = enableImageDimensions;
			
		Label lblimageWidthInMeters = new Label(this, SWT.NONE);
		lblimageWidthInMeters.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblimageWidthInMeters.setText("Image width (m):");
		
		imageWidthInMeters = new Text(this, SWT.BORDER);
		imageWidthInMeters.setEditable(enableImageDimensions);
		imageWidthInMeters.setEnabled(enableImageDimensions);
		imageWidthInMeters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		imageWidthInMeters.addKeyListener(new KeyListener() 
		{
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				try
				{				
					imageWidthChanged(Double.parseDouble(imageWidthInMeters.getText()));
				}
				catch(Throwable t)
				{					
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{	
				try
				{				
					imageWidthChanged(Double.parseDouble(imageWidthInMeters.getText()));
				}
				catch(Throwable t)
				{					
				}
			}
		});
		
		Label lblimageHeightInMeters = new Label(this, SWT.NONE);
		lblimageHeightInMeters.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblimageHeightInMeters.setText("Image height (m):");
		
		imageHeightInMeters = new Text(this, SWT.BORDER);
		imageHeightInMeters.setEditable(enableImageDimensions);
		imageHeightInMeters.setEnabled(enableImageDimensions);
		imageHeightInMeters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		imageHeightInMeters.addKeyListener(new KeyListener() 
		{
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				try
				{				
					imageHeightChanged(Double.parseDouble(imageHeightInMeters.getText()));
				}
				catch(Throwable t)
				{					
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{	
				try
				{				
					imageHeightChanged(Double.parseDouble(imageHeightInMeters.getText()));
				}
				catch(Throwable t)
				{					
				}
			}
		});		
					
		Label lblResolution = new Label(this, SWT.NONE);
		lblResolution.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblResolution.setText("Required Resolution (m/pixel):");
			
		txtRequiredResolution = new Text(this, SWT.BORDER);
		txtRequiredResolution.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtRequiredResolution.addKeyListener(new KeyListener() 
		{
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				try
				{				
					imageResolutionChanged(Double.parseDouble(txtRequiredResolution.getText()));
				}
				catch(Throwable t)
				{					
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{	
				try
				{				
					imageResolutionChanged(Double.parseDouble(txtRequiredResolution.getText()));
				}
				catch(Throwable t)
				{					
				}
			}
		});
		
		Label lblFillShape = new Label(this, SWT.NONE);
		lblFillShape.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFillShape.setText("Fill Shape:");

		
		btnShapedFilled = new Button(this, SWT.CHECK);
		btnShapedFilled.setAlignment(SWT.RIGHT);
		btnShapedFilled.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		btnShapedFilled.setText("");
		btnShapedFilled.setToolTipText("Whether or not to fill the shape.");		

		
		Label lblColor = new Label(this, SWT.NONE);
		lblColor.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblColor.setText("Layer Color:");

		color3fComposite = new Color3fComposite(this, SWT.NONE, "", true, abstractShapeImageLayer, ApogySurfaceEnvironmentPackage.Literals.ABSTRACT_SHAPE_IMAGE_LAYER__COLOR);
	
		setAbstractShapeImageLayer(abstractShapeImageLayer);
	
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
	
	public AbstractShapeImageLayer getAbstractShapeImageLayer() {
		return abstractShapeImageLayer;
	}

	public void setAbstractShapeImageLayer(AbstractShapeImageLayer newAbstractShapeImageLayer) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
						
		this.abstractShapeImageLayer = newAbstractShapeImageLayer;
		
		if(newAbstractShapeImageLayer != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}
	
	protected void imageResolutionChanged(double newImageWidth)
	{		
	}
	
	
	protected void imageWidthChanged(double newImageWidth)
	{		
	}
	
	protected void imageHeightChanged(double newImageWidth)
	{		
	}

	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Image Width Value. */
		IObservableValue<Double> observeImageWidthResolution = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(abstractShapeImageLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.IMAGE_MAP_LAYER__WIDTH)).observe(abstractShapeImageLayer);
		IObservableValue<String> observeImageWidthText = WidgetProperties.text(SWT.Modify).observe(imageWidthInMeters);

		bindingContext.bindValue(observeImageWidthText,
								observeImageWidthResolution, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		/* Image height Value. */
		IObservableValue<Double> observeImageHeight = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(abstractShapeImageLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.IMAGE_MAP_LAYER__HEIGHT)).observe(abstractShapeImageLayer);
		IObservableValue<String> observeImageHeightText = WidgetProperties.text(SWT.Modify).observe(imageHeightInMeters);

		bindingContext.bindValue(observeImageHeightText,
								 observeImageHeight, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		
		/* Required Resolution Value. */
		IObservableValue<Double> observeImageResolution = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(abstractShapeImageLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.ABSTRACT_SHAPE_IMAGE_LAYER__REQUIRED_RESOLUTION)).observe(abstractShapeImageLayer);
		IObservableValue<String> observeImageResolutionText = WidgetProperties.text(SWT.Modify).observe(txtRequiredResolution);

		bindingContext.bindValue(observeImageResolutionText,
								observeImageResolution, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		

		/* Shape Filled.*/
		IObservableValue<Double> observeShapeFilled = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(abstractShapeImageLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.ABSTRACT_SHAPE_IMAGE_LAYER__SHAPED_FILLED)).observe(abstractShapeImageLayer);
		IObservableValue<String> observeShapeFilledButtton = WidgetProperties.selection().observe(btnShapedFilled);
				
		bindingContext.bindValue(observeShapeFilledButtton,
								 observeShapeFilled, 
								 new UpdateValueStrategy(),	
								 new UpdateValueStrategy());
		
		return bindingContext;
	}
}

package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.MapsList;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.Activator;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.MapUISettings;


public class MapsListComposite extends Composite 
{		
	private MapsList mapsList;
	
	private boolean enableEditing = true;
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;	
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;
	
	public MapsListComposite(Composite parent, int style) 
	{
		this(parent, style, true);
	}
	
	public MapsListComposite(Composite parent, int style, boolean enableEditing) 
	{
		super(parent, style);	
		this.enableEditing = enableEditing;
		
		if(enableEditing)
		{
			setLayout(new GridLayout(2, false));
		}
		else
		{
			setLayout(new GridLayout(1, false));
		}
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CompositeFilterContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newMapSelected((Map)((IStructuredSelection) event.getSelection()).getFirstElement());					
			}
		});
		
		if(enableEditing)
		{
			// Buttons.
			Composite compositeButtons = new Composite(this, SWT.NONE);
			compositeButtons.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
			compositeButtons.setLayout(new GridLayout(1, false));	
			
			btnNew = new Button(compositeButtons, SWT.NONE);
			btnNew.setSize(74, 29);
			btnNew.setText("New");
			btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnNew.setEnabled(true);
			btnNew.addListener(SWT.Selection, new Listener() 
			{		
				@Override
				public void handleEvent(Event event) 
				{
					if (event.type == SWT.Selection) 
					{					
						MapUISettings mapUISettings = ApogySurfaceEnvironmentUIFactory.eINSTANCE.createMapUISettings();
						mapUISettings.setName(ApogyCommonEMFFacade.INSTANCE.getDefaultName(geMapsList(), null, ApogySurfaceEnvironmentPackage.Literals.MAPS_LIST__MAPS));
						
						Wizard wizard = new ApogyEObjectWizard(ApogySurfaceEnvironmentPackage.Literals.MAPS_LIST__MAPS, geMapsList(), mapUISettings, ApogySurfaceEnvironmentPackage.Literals.MAP);								
						WizardDialog dialog = new WizardDialog(getShell(), wizard);
						dialog.open();
						
						// Forces the viewer to refresh its input.
						if(!treeViewer.isBusy())
						{					
							treeViewer.setInput(geMapsList());
						}
					}
				}
			});
					
			btnDelete = new Button(compositeButtons, SWT.NONE);
			btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnDelete.setSize(74, 29);
			btnDelete.setText("Delete");
			btnDelete.setEnabled(false);
			btnDelete.addSelectionListener(new SelectionAdapter() 
			{
				@Override
				public void widgetSelected(SelectionEvent event) 
				{
					String mapsToDeleteMessage = "";
	
					Iterator<Map> maps = getSelectedMaps().iterator();
					while (maps.hasNext()) 
					{
						Map map = maps.next();
						mapsToDeleteMessage = mapsToDeleteMessage + map.getName();
	
						if (maps.hasNext()) 
						{
							mapsToDeleteMessage = mapsToDeleteMessage + ", ";
						}
					}
	
					MessageDialog dialog = new MessageDialog(null, "Delete the selected Maps", null,
							"Are you sure to delete these Maps: " + mapsToDeleteMessage, MessageDialog.QUESTION,
							new String[] { "Yes", "No" }, 1);
					int result = dialog.open();
					if (result == 0) 
					{
						for (Map map :  getSelectedMaps()) 
						{
							try 
							{
								ApogyCommonTransactionFacade.INSTANCE.basicRemove(map.eContainer(), ApogySurfaceEnvironmentPackage.Literals.MAPS_LIST__MAPS, map);
							} 
							catch (Exception e)	
							{
								Logger.INSTANCE.log(Activator.ID,
										"Unable to delete the Map <"+ map.getName() + ">",
										EventSeverity.ERROR, e);
							}
						}
					}
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(geMapsList());
					}					
				}
			});
		}
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
	
	public MapsList geMapsList() 
	{
		return mapsList;
	}

	public void setMapsList(MapsList worksitesList) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.mapsList = worksitesList;
		
		if(worksitesList != null)
		{
			m_bindingContext = customInitDataBindings();
			treeViewer.setInput(worksitesList);
			
			if(!worksitesList.getMaps().isEmpty())
			{
				treeViewer.setSelection(new StructuredSelection(worksitesList.getMaps().get(0)), true);
			}
			else
			{
				treeViewer.setSelection(null, true);
			}
		}
	}

	public void setSelectedMap(Map map)
	{
		if(map != null)
		{
			treeViewer.setSelection(new StructuredSelection(map), true);
		}
		else
		{			
			treeViewer.setSelection(null);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Map> getSelectedMaps()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		if(enableEditing)
		{
			IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
			
			/* Delete Button Enabled Binding. */
			IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
			bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
					new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
							.setConverter(new Converter(Object.class, Boolean.class) {
								@Override
								public Object convert(Object fromObject) {
									return fromObject != null;
								}
							}));
		}
		return bindingContext;
	}
	
	protected void newMapSelected(Map map)
	{		
	}

	private class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof MapsList)
			{								
				MapsList worksitesList = (MapsList) inputElement;								
				return worksitesList.getMaps().toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof MapsList)
			{								
				MapsList mapsList = (MapsList) parentElement;								
				return mapsList.getMaps().toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof MapsList)
			{
				MapsList mapsList = (MapsList) element;			
				return !mapsList.getMaps().isEmpty();
			}		
			else
			{
				return false;
			}
		}		
	}
}

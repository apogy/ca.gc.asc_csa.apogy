/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.ui.impl;

import javax.vecmath.Color3f;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshDiscreteSlopeImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.SlopeRange;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.CartesianTriangularMeshDiscreteSlopeImageMapLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ImageMapLayerUISettings;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.CartesianTriangularMeshDiscreteSlopeImageMapLayerWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cartesian Triangular Mesh Discrete Slope Image Map Layer Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CartesianTriangularMeshDiscreteSlopeImageMapLayerWizardPagesProviderImpl extends CartesianTriangularMeshDerivedImageMapLayerWizardPagesProviderImpl implements CartesianTriangularMeshDiscreteSlopeImageMapLayerWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CartesianTriangularMeshDiscreteSlopeImageMapLayerWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogySurfaceEnvironmentUIPackage.Literals.CARTESIAN_TRIANGULAR_MESH_DISCRETE_SLOPE_IMAGE_MAP_LAYER_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		CartesianTriangularMeshDiscreteSlopeImageMapLayer imageLayer = ApogySurfaceEnvironmentFactory.eINSTANCE.createCartesianTriangularMeshDiscreteSlopeImageMapLayer();
		
		SlopeRange lowSlopeRange = ApogySurfaceEnvironmentFactory.eINSTANCE.createSlopeRange();
		lowSlopeRange.setName("Low");
		lowSlopeRange.setSlopeLowerBound(0);
		lowSlopeRange.setSlopeUpperBound(10.0);
		lowSlopeRange.setColor(new Color3f(0.0f, 1.0f, 0.0f));		
		imageLayer.getSlopeRanges().add(lowSlopeRange);
		
		SlopeRange mediumSlopeRange = ApogySurfaceEnvironmentFactory.eINSTANCE.createSlopeRange();
		mediumSlopeRange.setName("Medium");
		mediumSlopeRange.setSlopeLowerBound(10);
		mediumSlopeRange.setSlopeUpperBound(20.0);
		mediumSlopeRange.setColor(new Color3f(1.0f, 1.0f, 0.0f));		
		imageLayer.getSlopeRanges().add(mediumSlopeRange);
		
		SlopeRange highSlopeRange = ApogySurfaceEnvironmentFactory.eINSTANCE.createSlopeRange();
		highSlopeRange.setName("High");
		highSlopeRange.setSlopeLowerBound(20.0);
		highSlopeRange.setSlopeUpperBound(90.0);
		highSlopeRange.setColor(new Color3f(1.0f, 0.0f, 0.0f));		
		imageLayer.getSlopeRanges().add(highSlopeRange);
		
		ImageMapLayerUISettings layerUISettings = (ImageMapLayerUISettings) settings;		
		imageLayer.setName(layerUISettings.getName());		
		
		return imageLayer;
	}

	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		ImageMapLayerUISettings layerUISettings = (ImageMapLayerUISettings) settings;							
		list.add(new CartesianTriangularMeshDiscreteSlopeImageMapLayerWizardPage((CartesianTriangularMeshDiscreteSlopeImageMapLayer) eObject, layerUISettings));
		
		return list;
	}
} //CartesianTriangularMeshDiscreteSlopeImageMapLayerWizardPagesProviderImpl

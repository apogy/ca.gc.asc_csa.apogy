/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.ui.impl;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayerPresentation;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ImageMapLayerPresentationWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.ImageMapLayerPresentationWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Image Map Layer Presentation Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ImageMapLayerPresentationWizardPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements ImageMapLayerPresentationWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImageMapLayerPresentationWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogySurfaceEnvironmentUIPackage.Literals.IMAGE_MAP_LAYER_PRESENTATION_WIZARD_PAGES_PROVIDER;
	}
	
	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		ImageMapLayerPresentation imageMapLayerPresentation = ApogySurfaceEnvironmentFactory.eINSTANCE.createImageMapLayerPresentation();
		imageMapLayerPresentation.setAlpha(1.0f);
		imageMapLayerPresentation.setVisible(true);		
				
		MapLayerPresentationUISettings mapLayerPresentationUISettings = (MapLayerPresentationUISettings) settings;
		mapLayerPresentationUISettings.setImageMapLayerPresentation(imageMapLayerPresentation);
		
		imageMapLayerPresentation.setName(mapLayerPresentationUISettings.getName());
						
		return imageMapLayerPresentation;
	}
		
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{				
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		MapLayerPresentationUISettings mapLayerPresentationUISettings = (MapLayerPresentationUISettings) settings;		
		list.add(new ImageMapLayerPresentationWizardPage(mapLayerPresentationUISettings));
						
		return list;
	}
		
	@Override
	public CompoundCommand getPerformFinishCommands(EObject eObject, EClassSettings settings, EditingDomain editingDomain) 
	{
		MapLayerPresentationUISettings mapLayerPresentationUISettings = (MapLayerPresentationUISettings) settings;		
		
		ImageMapLayerPresentation imlp = (ImageMapLayerPresentation) eObject;
		imlp.setMapLayer(mapLayerPresentationUISettings.getImageMapLayer());
		
		return null;
	}

} //ImageMapLayerPresentationWizardPagesProviderImpl

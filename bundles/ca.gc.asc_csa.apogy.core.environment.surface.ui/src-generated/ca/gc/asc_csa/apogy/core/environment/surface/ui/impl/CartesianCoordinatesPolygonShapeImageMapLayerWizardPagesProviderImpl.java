/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.ui.impl;

import javax.vecmath.Color3f;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianCoordinatesPolygonShapeImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.CartesianCoordinatesPolygonShapeImageMapLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ImageMapLayerUISettings;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.CartesianCoordinatesPolygonShapeImageMapLayerWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cartesian Coordinates Polygon Shape Image Map Layer Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CartesianCoordinatesPolygonShapeImageMapLayerWizardPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements CartesianCoordinatesPolygonShapeImageMapLayerWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CartesianCoordinatesPolygonShapeImageMapLayerWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogySurfaceEnvironmentUIPackage.Literals.CARTESIAN_COORDINATES_POLYGON_SHAPE_IMAGE_MAP_LAYER_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		CartesianCoordinatesPolygonShapeImageMapLayer layer = ApogySurfaceEnvironmentFactory.eINSTANCE.createCartesianCoordinatesPolygonShapeImageMapLayer();		
		layer.setColor(new Color3f(0.0f, 1.0f, 0.0f));
		layer.getPolygonVertices().add(ApogyCommonMathFacade.INSTANCE.createTuple3d(0, 0, 0));
		layer.getPolygonVertices().add(ApogyCommonMathFacade.INSTANCE.createTuple3d(10, 0, 0));
		layer.getPolygonVertices().add(ApogyCommonMathFacade.INSTANCE.createTuple3d(10, 20, 0));
		
		if(settings instanceof ImageMapLayerUISettings)
		{
			ImageMapLayerUISettings imageMapLayerUISettings = (ImageMapLayerUISettings) settings;
			layer.setName(imageMapLayerUISettings.getName());
		}
		
		return layer;
	}

	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) {
		EList<WizardPage> list = new BasicEList<>();
		list.addAll(super.instantiateWizardPages(eObject, settings));

		CartesianCoordinatesPolygonShapeImageMapLayer layer = (CartesianCoordinatesPolygonShapeImageMapLayer) eObject;
		CartesianCoordinatesPolygonShapeImageMapLayerWizardPage page = new CartesianCoordinatesPolygonShapeImageMapLayerWizardPage(layer);
		list.add(page);

		return list;
	}
} //CartesianCoordinatesPolygonShapeImageMapLayerWizardPagesProviderImpl

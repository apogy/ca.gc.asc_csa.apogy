/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.ui;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayerPresentation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Map Layer Presentation UI Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings#getName <em>Name</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings#getCartesianTriangularMeshMapLayer <em>Cartesian Triangular Mesh Map Layer</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings#getImageMapLayer <em>Image Map Layer</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings#getImageMapLayerPresentation <em>Image Map Layer Presentation</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage#getMapLayerPresentationUISettings()
 * @model
 * @generated
 */
public interface MapLayerPresentationUISettings extends EClassSettings {

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage#getMapLayerPresentationUISettings_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Cartesian Triangular Mesh Map Layer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cartesian Triangular Mesh Map Layer</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cartesian Triangular Mesh Map Layer</em>' reference.
	 * @see #setCartesianTriangularMeshMapLayer(CartesianTriangularMeshMapLayer)
	 * @see ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage#getMapLayerPresentationUISettings_CartesianTriangularMeshMapLayer()
	 * @model
	 * @generated
	 */
	CartesianTriangularMeshMapLayer getCartesianTriangularMeshMapLayer();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings#getCartesianTriangularMeshMapLayer <em>Cartesian Triangular Mesh Map Layer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cartesian Triangular Mesh Map Layer</em>' reference.
	 * @see #getCartesianTriangularMeshMapLayer()
	 * @generated
	 */
	void setCartesianTriangularMeshMapLayer(CartesianTriangularMeshMapLayer value);

	/**
	 * Returns the value of the '<em><b>Image Map Layer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Map Layer</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Map Layer</em>' reference.
	 * @see #setImageMapLayer(ImageMapLayer)
	 * @see ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage#getMapLayerPresentationUISettings_ImageMapLayer()
	 * @model
	 * @generated
	 */
	ImageMapLayer getImageMapLayer();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings#getImageMapLayer <em>Image Map Layer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Map Layer</em>' reference.
	 * @see #getImageMapLayer()
	 * @generated
	 */
	void setImageMapLayer(ImageMapLayer value);

	/**
	 * Returns the value of the '<em><b>Image Map Layer Presentation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Map Layer Presentation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Map Layer Presentation</em>' reference.
	 * @see #setImageMapLayerPresentation(ImageMapLayerPresentation)
	 * @see ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage#getMapLayerPresentationUISettings_ImageMapLayerPresentation()
	 * @model
	 * @generated
	 */
	ImageMapLayerPresentation getImageMapLayerPresentation();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings#getImageMapLayerPresentation <em>Image Map Layer Presentation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Map Layer Presentation</em>' reference.
	 * @see #getImageMapLayerPresentation()
	 * @generated
	 */
	void setImageMapLayerPresentation(ImageMapLayerPresentation value);
} // MapLayerPresentationUISettings

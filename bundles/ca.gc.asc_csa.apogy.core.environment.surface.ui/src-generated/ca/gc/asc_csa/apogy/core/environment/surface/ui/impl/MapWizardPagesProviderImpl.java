/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.ui.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.MapUISettings;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.MapWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Map Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MapWizardPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements MapWizardPagesProvider 
{
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public MapWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogySurfaceEnvironmentUIPackage.Literals.MAP_WIZARD_PAGES_PROVIDER;
	}
	
	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		Map map = ApogySurfaceEnvironmentFactory.eINSTANCE.createMap();
		
		if(settings instanceof MapUISettings)
		{
			MapUISettings mapUISettings = (MapUISettings) settings;
			map.setName(mapUISettings.getName());
		}
		
		return map;
	}
} //MapWizardPagesProviderImpl

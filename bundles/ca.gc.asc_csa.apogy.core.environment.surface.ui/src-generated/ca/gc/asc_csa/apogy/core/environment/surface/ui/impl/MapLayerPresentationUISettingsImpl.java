/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.ui.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.emf.ui.impl.EClassSettingsImpl;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayerPresentation;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Map Layer Presentation UI Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.impl.MapLayerPresentationUISettingsImpl#getName <em>Name</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.impl.MapLayerPresentationUISettingsImpl#getCartesianTriangularMeshMapLayer <em>Cartesian Triangular Mesh Map Layer</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.impl.MapLayerPresentationUISettingsImpl#getImageMapLayer <em>Image Map Layer</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.ui.impl.MapLayerPresentationUISettingsImpl#getImageMapLayerPresentation <em>Image Map Layer Presentation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MapLayerPresentationUISettingsImpl extends EClassSettingsImpl implements MapLayerPresentationUISettings {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCartesianTriangularMeshMapLayer() <em>Cartesian Triangular Mesh Map Layer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCartesianTriangularMeshMapLayer()
	 * @generated
	 * @ordered
	 */
	protected CartesianTriangularMeshMapLayer cartesianTriangularMeshMapLayer;

	/**
	 * The cached value of the '{@link #getImageMapLayer() <em>Image Map Layer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImageMapLayer()
	 * @generated
	 * @ordered
	 */
	protected ImageMapLayer imageMapLayer;
	/**
	 * The cached value of the '{@link #getImageMapLayerPresentation() <em>Image Map Layer Presentation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImageMapLayerPresentation()
	 * @generated
	 * @ordered
	 */
	protected ImageMapLayerPresentation imageMapLayerPresentation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MapLayerPresentationUISettingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogySurfaceEnvironmentUIPackage.Literals.MAP_LAYER_PRESENTATION_UI_SETTINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CartesianTriangularMeshMapLayer getCartesianTriangularMeshMapLayer() {
		if (cartesianTriangularMeshMapLayer != null && cartesianTriangularMeshMapLayer.eIsProxy()) {
			InternalEObject oldCartesianTriangularMeshMapLayer = (InternalEObject)cartesianTriangularMeshMapLayer;
			cartesianTriangularMeshMapLayer = (CartesianTriangularMeshMapLayer)eResolveProxy(oldCartesianTriangularMeshMapLayer);
			if (cartesianTriangularMeshMapLayer != oldCartesianTriangularMeshMapLayer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__CARTESIAN_TRIANGULAR_MESH_MAP_LAYER, oldCartesianTriangularMeshMapLayer, cartesianTriangularMeshMapLayer));
			}
		}
		return cartesianTriangularMeshMapLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CartesianTriangularMeshMapLayer basicGetCartesianTriangularMeshMapLayer() {
		return cartesianTriangularMeshMapLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCartesianTriangularMeshMapLayer(CartesianTriangularMeshMapLayer newCartesianTriangularMeshMapLayer) {
		CartesianTriangularMeshMapLayer oldCartesianTriangularMeshMapLayer = cartesianTriangularMeshMapLayer;
		cartesianTriangularMeshMapLayer = newCartesianTriangularMeshMapLayer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__CARTESIAN_TRIANGULAR_MESH_MAP_LAYER, oldCartesianTriangularMeshMapLayer, cartesianTriangularMeshMapLayer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImageMapLayer getImageMapLayer() {
		if (imageMapLayer != null && imageMapLayer.eIsProxy()) {
			InternalEObject oldImageMapLayer = (InternalEObject)imageMapLayer;
			imageMapLayer = (ImageMapLayer)eResolveProxy(oldImageMapLayer);
			if (imageMapLayer != oldImageMapLayer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER, oldImageMapLayer, imageMapLayer));
			}
		}
		return imageMapLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImageMapLayer basicGetImageMapLayer() {
		return imageMapLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImageMapLayer(ImageMapLayer newImageMapLayer) {
		ImageMapLayer oldImageMapLayer = imageMapLayer;
		imageMapLayer = newImageMapLayer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER, oldImageMapLayer, imageMapLayer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImageMapLayerPresentation getImageMapLayerPresentation() {
		if (imageMapLayerPresentation != null && imageMapLayerPresentation.eIsProxy()) {
			InternalEObject oldImageMapLayerPresentation = (InternalEObject)imageMapLayerPresentation;
			imageMapLayerPresentation = (ImageMapLayerPresentation)eResolveProxy(oldImageMapLayerPresentation);
			if (imageMapLayerPresentation != oldImageMapLayerPresentation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER_PRESENTATION, oldImageMapLayerPresentation, imageMapLayerPresentation));
			}
		}
		return imageMapLayerPresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImageMapLayerPresentation basicGetImageMapLayerPresentation() {
		return imageMapLayerPresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImageMapLayerPresentation(ImageMapLayerPresentation newImageMapLayerPresentation) {
		ImageMapLayerPresentation oldImageMapLayerPresentation = imageMapLayerPresentation;
		imageMapLayerPresentation = newImageMapLayerPresentation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER_PRESENTATION, oldImageMapLayerPresentation, imageMapLayerPresentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__NAME:
				return getName();
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__CARTESIAN_TRIANGULAR_MESH_MAP_LAYER:
				if (resolve) return getCartesianTriangularMeshMapLayer();
				return basicGetCartesianTriangularMeshMapLayer();
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER:
				if (resolve) return getImageMapLayer();
				return basicGetImageMapLayer();
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER_PRESENTATION:
				if (resolve) return getImageMapLayerPresentation();
				return basicGetImageMapLayerPresentation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__NAME:
				setName((String)newValue);
				return;
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__CARTESIAN_TRIANGULAR_MESH_MAP_LAYER:
				setCartesianTriangularMeshMapLayer((CartesianTriangularMeshMapLayer)newValue);
				return;
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER:
				setImageMapLayer((ImageMapLayer)newValue);
				return;
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER_PRESENTATION:
				setImageMapLayerPresentation((ImageMapLayerPresentation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__CARTESIAN_TRIANGULAR_MESH_MAP_LAYER:
				setCartesianTriangularMeshMapLayer((CartesianTriangularMeshMapLayer)null);
				return;
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER:
				setImageMapLayer((ImageMapLayer)null);
				return;
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER_PRESENTATION:
				setImageMapLayerPresentation((ImageMapLayerPresentation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__CARTESIAN_TRIANGULAR_MESH_MAP_LAYER:
				return cartesianTriangularMeshMapLayer != null;
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER:
				return imageMapLayer != null;
			case ApogySurfaceEnvironmentUIPackage.MAP_LAYER_PRESENTATION_UI_SETTINGS__IMAGE_MAP_LAYER_PRESENTATION:
				return imageMapLayerPresentation != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //MapLayerPresentationUISettingsImpl

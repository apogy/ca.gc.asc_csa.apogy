package ca.gc.asc_csa.apogy.core.programs.javascript.ui.wizards;

import java.io.File;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.programs.javascript.ApogyCoreJavaScriptProgramsPackage;
import ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram;
import ca.gc.asc_csa.apogy.workspace.ApogyWorkspaceFacade;

public class JavaScriptWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.programs.javascript.ui.wizards.JavaScriptWizardPage";	
	
	private JavaScriptProgram javaScriptProgram;
	
	private Text scriptPath;
	private Button browseButton;
	
	private DataBindingContext bindingContext;
	
	public JavaScriptWizardPage(JavaScriptProgram javaScriptProgram)
	{
		super(WIZARD_PAGE_ID);
		this.javaScriptProgram = javaScriptProgram;
		
		setTitle("Java Script Program.");
		setDescription("Select the path to the javascript file.");
		
		validate();			
	}
	
	@Override
	public void createControl(Composite parent) 
	{		
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(3, false));
		
		// Script Path
		Label scriptPathLabel = new Label(top, SWT.NONE);
		scriptPathLabel.setText("Script Path : ");
		
		scriptPath = new Text(top, SWT.None);	
		scriptPath.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));				
		
		browseButton =  new Button(top, SWT.PUSH);
		browseButton.setText("Browse..."); 
		browseButton.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false));
		browseButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				FileDialog fd = new FileDialog(getShell(), SWT.SAVE | SWT.OPEN);
		        fd.setText("Select Javascript File");
		        
		        String relativePath  = ApogyWorkspaceFacade.INSTANCE.getActiveProject().getFullPath().toOSString() + File.separator + ApogyWorkspaceFacade.INSTANCE.getDefaultProgramsFolderName() + File.separator;			        
		        IPath workspacePath  = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		        IPath absolutePath   = workspacePath.append(relativePath);
		        String absolutePathString = absolutePath.toOSString();		        
		        fd.setFilterPath(absolutePathString);
		        
		        String[] filterExt = { "*.js" };
		        fd.setFilterExtensions(filterExt);
		        
		        String selected = fd.open();
		        if(selected != null)
		        {
		        	try
		        	{
		        		// Keeps what is after the workspace location,
		        		String path = relativePath + selected.substring(selected.lastIndexOf(File.separator) + 1);
		        		
		        		ApogyCommonTransactionFacade.INSTANCE.basicSet(javaScriptProgram, ApogyCoreJavaScriptProgramsPackage.Literals.JAVA_SCRIPT_PROGRAM__SCRIPT_PATH, path, true);
		        	}
		        	catch (Throwable t) 
		        	{
						t.printStackTrace();
					}
		        } 		        
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
		
		
		setControl(top);
		
		// Bindings
		bindingContext = customInitDataBindings();
	}
	
	@Override
	public void dispose() 
	{	
		if(bindingContext != null) bindingContext.dispose();
		
		super.dispose();
	}

	protected void validate()
	{		
		setPageComplete(getErrorMessage() == null);
	}
	
	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<String> observeScriptPath = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  										FeaturePath.fromList(ApogyCoreJavaScriptProgramsPackage.Literals.JAVA_SCRIPT_PROGRAM__SCRIPT_PATH)).observe(javaScriptProgram);
		
		IObservableValue<String> observeScriptPathTxt = WidgetProperties.text(SWT.Modify).observe(scriptPath);

		bindingContext.bindValue(observeScriptPathTxt, 
								 observeScriptPath,
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		return bindingContext;
	}
}

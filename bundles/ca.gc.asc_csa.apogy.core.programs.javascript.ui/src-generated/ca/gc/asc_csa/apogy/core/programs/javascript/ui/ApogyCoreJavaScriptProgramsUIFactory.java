/**
 * Canadian Space Agency / Agence spatiale canadienne 2017 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript.ui;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.ApogyCoreJavaScriptProgramsUIPackage
 * @generated
 */
public interface ApogyCoreJavaScriptProgramsUIFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCoreJavaScriptProgramsUIFactory eINSTANCE = ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.ApogyCoreJavaScriptProgramsUIFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Java Script Program UI Factory</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Script Program UI Factory</em>'.
	 * @generated
	 */
	JavaScriptProgramUIFactory createJavaScriptProgramUIFactory();

	/**
	 * Returns a new object of class '<em>Java Script Program Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Script Program Pages Provider</em>'.
	 * @generated
	 */
	JavaScriptProgramPagesProvider createJavaScriptProgramPagesProvider();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyCoreJavaScriptProgramsUIPackage getApogyCoreJavaScriptProgramsUIPackage();

} //ApogyCoreJavaScriptProgramsUIFactory

/**
 * Canadian Space Agency / Agence spatiale canadienne 2017 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript.ui;

import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Script Program Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.ApogyCoreJavaScriptProgramsUIPackage#getJavaScriptProgramPagesProvider()
 * @model
 * @generated
 */
public interface JavaScriptProgramPagesProvider extends NamedDescribedWizardPagesProvider {
} // JavaScriptProgramPagesProvider

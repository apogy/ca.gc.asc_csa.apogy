/**
 * Canadian Space Agency / Agence spatiale canadienne 2017 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript.ui;

import ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactory;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Script Program UI Factory</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.ApogyCoreJavaScriptProgramsUIPackage#getJavaScriptProgramUIFactory()
 * @model
 * @generated
 */
public interface JavaScriptProgramUIFactory extends ProgramUIFactory {
} // JavaScriptProgramUIFactory

/**
 * Canadian Space Agency / Agence spatiale canadienne 2017 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl;

import ca.gc.asc_csa.apogy.core.programs.javascript.ui.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCoreJavaScriptProgramsUIFactoryImpl extends EFactoryImpl implements ApogyCoreJavaScriptProgramsUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyCoreJavaScriptProgramsUIFactory init() {
		try {
			ApogyCoreJavaScriptProgramsUIFactory theApogyCoreJavaScriptProgramsUIFactory = (ApogyCoreJavaScriptProgramsUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyCoreJavaScriptProgramsUIPackage.eNS_URI);
			if (theApogyCoreJavaScriptProgramsUIFactory != null) {
				return theApogyCoreJavaScriptProgramsUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyCoreJavaScriptProgramsUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCoreJavaScriptProgramsUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyCoreJavaScriptProgramsUIPackage.JAVA_SCRIPT_PROGRAM_UI_FACTORY: return createJavaScriptProgramUIFactory();
			case ApogyCoreJavaScriptProgramsUIPackage.JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER: return createJavaScriptProgramPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaScriptProgramUIFactory createJavaScriptProgramUIFactory() {
		JavaScriptProgramUIFactoryImpl javaScriptProgramUIFactory = new JavaScriptProgramUIFactoryImpl();
		return javaScriptProgramUIFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaScriptProgramPagesProvider createJavaScriptProgramPagesProvider() {
		JavaScriptProgramPagesProviderImpl javaScriptProgramPagesProvider = new JavaScriptProgramPagesProviderImpl();
		return javaScriptProgramPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCoreJavaScriptProgramsUIPackage getApogyCoreJavaScriptProgramsUIPackage() {
		return (ApogyCoreJavaScriptProgramsUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyCoreJavaScriptProgramsUIPackage getPackage() {
		return ApogyCoreJavaScriptProgramsUIPackage.eINSTANCE;
	}

} //ApogyCoreJavaScriptProgramsUIFactoryImpl

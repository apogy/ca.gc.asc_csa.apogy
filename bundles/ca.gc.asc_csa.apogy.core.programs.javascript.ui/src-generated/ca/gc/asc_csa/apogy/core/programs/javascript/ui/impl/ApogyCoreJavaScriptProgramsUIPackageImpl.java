/**
 * Canadian Space Agency / Agence spatiale canadienne 2017 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage;
import ca.gc.asc_csa.apogy.core.programs.javascript.ui.ApogyCoreJavaScriptProgramsUIFactory;
import ca.gc.asc_csa.apogy.core.programs.javascript.ui.ApogyCoreJavaScriptProgramsUIPackage;
import ca.gc.asc_csa.apogy.core.programs.javascript.ui.JavaScriptProgramPagesProvider;

import ca.gc.asc_csa.apogy.core.programs.javascript.ui.JavaScriptProgramUIFactory;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCoreJavaScriptProgramsUIPackageImpl extends EPackageImpl implements ApogyCoreJavaScriptProgramsUIPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaScriptProgramUIFactoryEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaScriptProgramPagesProviderEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.ApogyCoreJavaScriptProgramsUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyCoreJavaScriptProgramsUIPackageImpl() {
		super(eNS_URI, ApogyCoreJavaScriptProgramsUIFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCoreJavaScriptProgramsUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyCoreJavaScriptProgramsUIPackage init() {
		if (isInited) return (ApogyCoreJavaScriptProgramsUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCoreJavaScriptProgramsUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCoreJavaScriptProgramsUIPackageImpl theApogyCoreJavaScriptProgramsUIPackage = (ApogyCoreJavaScriptProgramsUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCoreJavaScriptProgramsUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCoreJavaScriptProgramsUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCoreInvocatorUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCoreJavaScriptProgramsUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCoreJavaScriptProgramsUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCoreJavaScriptProgramsUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCoreJavaScriptProgramsUIPackage.eNS_URI, theApogyCoreJavaScriptProgramsUIPackage);
		return theApogyCoreJavaScriptProgramsUIPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaScriptProgramUIFactory() {
		return javaScriptProgramUIFactoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaScriptProgramPagesProvider() {
		return javaScriptProgramPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCoreJavaScriptProgramsUIFactory getApogyCoreJavaScriptProgramsUIFactory() {
		return (ApogyCoreJavaScriptProgramsUIFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		javaScriptProgramUIFactoryEClass = createEClass(JAVA_SCRIPT_PROGRAM_UI_FACTORY);

		javaScriptProgramPagesProviderEClass = createEClass(JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCoreInvocatorUIPackage theApogyCoreInvocatorUIPackage = (ApogyCoreInvocatorUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCoreInvocatorUIPackage.eNS_URI);
		ApogyCommonEMFUIPackage theApogyCommonEMFUIPackage = (ApogyCommonEMFUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFUIPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		javaScriptProgramUIFactoryEClass.getESuperTypes().add(theApogyCoreInvocatorUIPackage.getProgramUIFactory());
		javaScriptProgramPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(javaScriptProgramUIFactoryEClass, JavaScriptProgramUIFactory.class, "JavaScriptProgramUIFactory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(javaScriptProgramPagesProviderEClass, JavaScriptProgramPagesProvider.class, "JavaScriptProgramPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "prefix", "ApogyCoreJavaScriptProgramsUI",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyCoreJavaScriptProgramsUI",
			 "suppressGenModelAnnotations", "false",
			 "publicConstructors", "true",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.core.programs.javascript.ui/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.core.programs.javascript.ui.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.core.programs.javascript"
		   });
	}

} //ApogyCoreJavaScriptProgramsUIPackageImpl

// *****************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
//     Pierre Allard - initial API and implementation
//     Regent L'Archeveque
//        
// SPDX-License-Identifier: EPL-1.0
// *****************************************************************************
@GenModel(prefix="ApogyAddons",
 copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Pierre Allard - initial API and implementation
     Regent L'Archeveque
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",	
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  multipleEditorPages="false",
		  modelName="ApogyAddons",
		  complianceLevel="6.0",
		  suppressGenModelAnnotations="false",
		  dynamicTemplates="true", 
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.addons/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.addons.edit/src-generated")

package ca.gc.asc_csa.apogy.addons

import ca.gc.asc_csa.apogy.addons.geometry.paths.WayPointPath
import ca.gc.asc_csa.apogy.common.emf.Described
import ca.gc.asc_csa.apogy.common.emf.Named
import ca.gc.asc_csa.apogy.common.math.Matrix4d
import ca.gc.asc_csa.apogy.common.math.Matrix4x4
import ca.gc.asc_csa.apogy.common.math.Tuple3d
import ca.gc.asc_csa.apogy.common.topology.AggregateGroupNode
import ca.gc.asc_csa.apogy.common.topology.Node
import ca.gc.asc_csa.apogy.common.topology.NodePath
import ca.gc.asc_csa.apogy.common.topology.TransformNode
import ca.gc.asc_csa.apogy.common.topology.URLNode
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.MouseButton
import ca.gc.asc_csa.apogy.core.FeatureOfInterest
import ca.gc.asc_csa.apogy.core.FeatureOfInterestList
import ca.gc.asc_csa.apogy.core.FeatureOfInterestNode
import ca.gc.asc_csa.apogy.core.invocator.AbstractToolsListContainer
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession
import ca.gc.asc_csa.apogy.core.invocator.Variable

// Types.

type Color3f wraps javax.vecmath.Color3f
type NodeSelection wraps ca.gc.asc_csa.apogy.common.topology.ui.NodeSelection
type Collection<T> wraps java.util.Collection

class ApogyAddonsFacade
{
	/**
	 * Deletes a Simple tool.
	 * @param simpleTool The tool to delete.
	 */
	op void deleteTool(SimpleTool simpleTool)
	
	/**
	 * Return all the FeatureOfInterestList found in the specified. 
	 */
	op Collection<FeatureOfInterestList> getAllFeatureOfInterestLists(InvocatorSession invocatorSession)
	
	/*
	 * Gets the first {@link AbstractToolsListContainer} that is an instance of {@link SimpleToolList}, if none exist, creates one.
	 * @return Reference to the SimpleToolList.
	 */
	op SimpleToolList getSimpleToolList()
}

/**
 * Base class for all Tools.
 */
abstract class AbstractTool extends Named, Described
{
}

/**
 * List of Tools.
 */
class SimpleToolList extends AbstractToolsListContainer
{
	@GenModel(property="None")	
	contains SimpleTool[0..*] simpleTools opposite toolList
}

/**
 * Base class of Simple Tools. Exposes a few call backs that can be used by the tools to manage their life cycles.
 */
abstract class SimpleTool extends AbstractTool
{		
	/**
	 * The SimpleToolList containing the tool.
	 */
	@GenModel(notify="false", children="false", property="None")	
	refers SimpleToolList toolList opposite simpleTools
	
	/**
	 * Whether or  not the tool is active.
	 */
	transient boolean active = "false"
	
	/**
	 * Whether or  not the tool is disposed.
	 */
	@GenModel(property="Readonly")
	transient boolean disposed = "false"
	
	/**
	 * Whether or  not the tool is initialized.
	 */
	@GenModel(property="None")
	transient boolean initialized = "false"
	
	/**
	 * Method called when the tool is added to its toolList.
	 */
	op void initialise()
	
	/**
	 * Method called when the tool is remove from its toolList.
	 */
	op void dispose()
	
	/**
	 * Method called when the Variables are instantiated.
	 */
	op void variablesInstantiated()
	
	/**
	 * Method called when the Variables are cleared.
	 */
	op void variablesCleared()
}

/**
 * Base calls for Simple Tool that are used within the 3D environment.
 */
abstract class Simple3DTool extends SimpleTool
{		
	/**
	 * Whether or  not the tool is visible.
	 */
	boolean visible = "true"		

	/** 
	 * The root node of the current topology. Is set automatically by Apogy after the tool is registered.
	 */
	@GenModel(notify="false", children="false", property="None")	
	refers transient Node rootNode
	
	/** 
	 * Method called upon a user selection in the 3D viewer. Sub-classes should implement.
	 * nodeSelection The node selection that has been raised by the 3D viewer.
	 */
	op void selectionChanged(NodeSelection nodeSelection)
	
	/** 
	 * Method called upon a mouse click is detected in the 3D Viewer. Sub-classes should implement.
	 * @param mouseButtonClicked The mouse button that was clicked.
	 */
	op void mouseButtonClicked(MouseButton mouseButtonClicked)
}

/**
 * Base class for 3D tools that deal with 2 selected points : from and to Nodes.
 */
abstract class AbstractTwoPoints3DTool extends Simple3DTool
{
	/**
	 * Absolute position of the from Node.
	 */
	@GenModel(notify="true", children="true", propertyCategory="FROM_NODE")
	contains Tuple3d fromAbsolutePosition
		
	/**
	 * Relative position of the from point relative to the from Node.
	 */
	@GenModel(notify="true", children="true", propertyCategory="FROM_NODE")
	contains Tuple3d fromRelativePosition
	
	/**
	 * From Node.
	 */
	@GenModel(notify="true", children="false", property="Readonly", propertyCategory="FROM_NODE")	
	refers transient Node fromNode
		
	/**
	 * NodePath referring to the From Node.
	 */
	@GenModel(notify="false", children="false", property="None")	
	contains NodePath[0..1] fromNodeNodePath
		
	/**
	 * Whether or not the selection of the From node is locked.
	 */
	@GenModel(notify="true", propertyCategory="FROM_NODE")	
	boolean fromNodeLock = "false"
				
	/**
	 * Absolute position of the to Node.
	 */		
	@GenModel(notify="true", children="false", property="None", propertyCategory="TO_NODE")
	contains Tuple3d toAbsolutePosition
	
	/**
	 * Relative position of the to point relative to the to Node.
	 */
	@GenModel(notify="true", children="false", property="None", propertyCategory="TO_NODE")	
	contains Tuple3d toRelativePosition
			
	/**
	 * To Node.
	 */
	@GenModel(notify="true", children="false", property="Readonly", propertyCategory="TO_NODE")	
	refers transient Node toNode	
		
	/**
	 * NodePath referring to the To Node.
	 */
	@GenModel(notify="false", children="false", property="None")
	contains NodePath[0..1] toNodeNodePath
	
	/**
	 * Whether or not the selection of the To node is locked.
	 */
	@GenModel(notify="true", propertyCategory="TO_NODE")	
	boolean toNodeLock = "false"	
		
	/**
	 * Distance between From and To points.
	 */
	@GenModel(notify="true", property="Readonly", apogy_units = "m")	
	transient double distance = "0"
		
	/**
	 * Method called when the pose between from and to changes.
	 */
	op void pointsRelativePoseChanged(Matrix4d newPose)
}

/**
 * A 3D tool used to measure distance between two points. The FROM location is displayed in red, the TO location is displayed in blue.
 */
class Ruler3DTool extends AbstractTwoPoints3DTool
{		
	/**
	 * The ruler color.
	 */
	@GenModel(propertyCategory="RULER_PROPERTIES")
	Color3f rulerColor = "0.0,1.0,0.0"
	
	/**
	 * Radius of sphere used as ruler end points.
	 */
	@GenModel(notify="true", apogy_units = "m", propertyCategory="RULER_PROPERTIES")	
	double extremitiesRadius = "0.01"
	
	// MINOR TICKS
	
	/**
	 * Minor tick color.
	 */
	@GenModel(propertyCategory="MINOR_TICKS_PROPERTIES")
	Color3f minorTickColor = "0.0,1.0,0.0"
	
	/**
	 *  Spacing between minor tick mark.
	 */
	@GenModel(notify="true", apogy_units = "m", propertyCategory="MINOR_TICKS_PROPERTIES")	
	double minorTickSpacing = "0.1"
	
	/**
	 *  Length of the minor tick marks as displayed on the ruler.
	 */
	@GenModel(notify="true", apogy_units = "m", propertyCategory="MINOR_TICKS_PROPERTIES")	
	double minorTickLength = "0.1"
	
	// MAJOR TICKS
	
	/**
	 * Major tick color.
	 */
	@GenModel(propertyCategory="MAJOR_TICKS_PROPERTIES")
	Color3f majorTickColor = "0.0,1.0,0.0"
	
	/**
	 * Spacing between major tick mark.
	 */
	@GenModel(notify="true", apogy_units = "m", propertyCategory="MAJOR_TICKS_PROPERTIES")	
	double majorTickSpacing = "1.0"

	/**
	 * Length of the major tick marks as displayed on the ruler.
	 */
	@GenModel(notify="true", apogy_units = "m", propertyCategory="MAJOR_TICKS_PROPERTIES")	
	double majorTickLength = "0.2"
		
	/**
	 * Topology Node associated with the tool. This is the Node that represent the Ruler3DTool in the topology.
	 */
	@GenModel(notify="true", children="true", property="Readonly", propertyCategory="RULER_PROPERTIES")	
	refers transient Ruler3dToolNode ruler3dToolNode opposite ruler3DTool
}

/**
 * Node that represent the Ruler3DTool in the topology.
 */
class Ruler3dToolNode extends Node
{	
	refers transient Ruler3DTool ruler3DTool opposite ruler3dToolNode	
}

/**
 * A 3D tool used to create FeatureOfInterest.
 */
class FeatureOfInterestPickingTool extends Simple3DTool
{
	/**
	 *  List of Feature Of Interest where to add Feature Of Interest.
	 */
	@GenModel(notify="true", children="false")
	refers FeatureOfInterestList featureOfInterestList
	
	/**
	 * 
	 */
	@GenModel(notify="true", children="false", property="Readonly", propertyCategory="PICKED_NODE")
	refers transient Tuple3d pickAbsolutePosition
	
	/**
	 * Position of the picked point relative to the picked Node. 
	 */		
	@GenModel(notify="true", children="false", propertyCategory="PICKED_NODE")
	refers transient Tuple3d pickRelativePosition
	
	/**
	 * Node that as been picked through the 3D Viewer.
	 */
	@GenModel(notify="true", children="false", propertyCategory="PICKED_NODE")	
	refers transient Node pickedNode
	
	/**
	 * Absolute normal of the surface at the picked point.
	 */
	@GenModel(notify="true", children="false", propertyCategory="PICKED_NODE")
	refers transient Tuple3d pickAbsoluteNormal
	
	/**
	 * Normal of the surface at the picked point, relative to the picked Node. 
	 */
	@GenModel(notify="true", children="false", propertyCategory="PICKED_NODE")
	refers transient Tuple3d pickRelativeNormal	
}

/**
 * A 3D tool used to define trajectories by piking point in the 3D world.
 * The tool support the creation of multiple path.
 */
 
class TrajectoryPickingTool extends Simple3DTool
{	
	/** 
	 * Offset to apply to the point to keep a waypoint path above ground.
	 */
	@GenModel(notify="true", apogy_units = "m", propertyCategory="TRAJECTORY_PICKING_TOOL")
	double altitudeOffset = "0"
	
	/** 
	 * All the  WayPointPath created with this tool.
	 */
	@GenModel(notify="true", propertyCategory="TRAJECTORY_PICKING_TOOL")
	contains WayPointPath[1..*] paths
	
	/**
	 * The WayPointPath to which selected points are to be added.
	 */
	@GenModel(notify="true", propertyCategory="TRAJECTORY_PICKING_TOOL")
	refers WayPointPath[1] activePath
	
	/**
	 * The node representing the tool in the topology.
	 */
	@GenModel(notify="true", children="false", property="Readonly", propertyCategory="TRAJECTORY_PICKING_TOOL")
	refers transient TrajectoryPickingToolNode trajectoryPickingToolNode opposite trajectoryPickingTool
	
	/** 
	 * The total length of all the paths contained in the tool.
	 */
	@GenModel(notify="true", property="Readonly", apogy_units = "m")
	transient double totalLength = "0.0"
		
	/**
	 * Clears all points from the active path. 
	 */
	op void clearActivePath()	
}

/**
  * Node representing the TrajectoryPickingTool in the topology.
  */
class TrajectoryPickingToolNode extends AggregateGroupNode
{
	/** 
	 * The  TrajectoryPickingTool associated with this node.
	 */
	refers transient TrajectoryPickingTool trajectoryPickingTool opposite trajectoryPickingToolNode
}

/**
 * A 3D tool used to trace the trajectory of a Variable.
 */
 
 class Trajectory3DTool extends Simple3DTool
 {
 	/** 
 	 * Whether of not the trajectory is traced.
 	 */
 	@GenModel(propertyCategory="TRAJECTORY_3D_TOOL")
 	boolean penDown = "true"
 	 	 	
 	/**
 	 * The Apogy System being displayed.
 	 */
 	@GenModel(propertyCategory="TRAJECTORY_3D_TOOL")
	refers Variable[1] variable
		
	/** 
	 * Minimum distance used to trigger a new point being accepted.
	 */
	@GenModel(propertyCategory="TRAJECTORY_3D_TOOL", apogy_units="m")
	double distanceThreshold = "0.01"
	
	/**
	 * The total length of all the trajectory segments.
	 */
	@GenModel(notify="true", property="Readonly", propertyCategory="TRAJECTORY_3D_TOOL", apogy_units="m")
	double totalDistance = "0.0"
	
	/** 
	 * The list of WayPointPath in which new position are being recorded.
	 */
	contains WayPointPath[0..*] paths 	
	
	/**
	 * The Node representing the Trajectory3DTool in the topology.
	 */
	@GenModel(notify="true", children="false", property="None", propertyCategory="TRAJECTORY_3D_TOOL")
	refers transient Trajectory3DToolNode trajectory3DToolNode opposite trajectory3DTool
	
	/**
	 * Removes the last trajectory segment if one is available. 
	 */
	op void clearLastPathSegment()	
	
	/**
	 * Clears all paths. 
	 */
	op void clearPaths()	
 }
 
 /**
  * Node representing the Trajectory3DTool in the topology.
  */
 class Trajectory3DToolNode extends AggregateGroupNode
 {
 	/**
 	 * The Trajectory3DTool being represented by this Node.
 	 */
 	refers transient Trajectory3DTool trajectory3DTool opposite trajectory3DToolNode
 }
 
 
 /**
  * Base class for tools that allows a user to pick a location.
  */
class AbstractPickLocationTool extends Simple3DTool
{
	/**
	 * Normal of the surface at the picked point, relative to the selected Node. 
	 */
	@GenModel(notify="true", children="false", property="None")
	contains Tuple3d relativeIntersectionNormal
	
	/**
	 * Relative position of the from point relative to the selected Node.
	 */
	@GenModel(notify="true", children="false", property="None")
	contains Tuple3d selectedRelativePosition
	
	/**
	 * The selected Node.
	 */
	@GenModel(notify="true", children="false", property="Readonly")	
	refers transient Node selectedNode
	
	/**
	 * NodePath referring to the picked Node.
	 */
	@GenModel(notify="false", children="false", property="None")	
	contains NodePath[0..1] selectedNodeNodePath
					
 	/**
 	 * Topology Node associated with the tool.
 	 */
	@GenModel(notify="true", children="true", property="None")	
	refers transient AbstractPickLocationToolNode abstractPickLocationToolNode opposite abstractPickLocationToolNode 	
}

/**
 * Node representing an AbstractGeometryPlacementTool in a topology.
 */ 
class AbstractPickLocationToolNode extends TransformNode
{	
	/**
	 * AbstractPickLocationTool being represented by this Node.
	 */
	refers transient AbstractPickLocationTool abstractPickLocationToolNode opposite abstractPickLocationToolNode	
}
 
 /**
  * A tool that allows a user to attach a geometry at a specified FeatureOfInterest
  */
 abstract class GeometryPlacementAtFeatureOfInterestTool extends Simple3DTool
 {
 	/**
 	 * The selected feature of interest.
 	 */
 	refers FeatureOfInterest featureOfInterest
 	
 	/**
 	 * The FeatureOfInterestNode associated with the FeatureOfInterest. Updated whenever 
 	 * the featureOfInterest is changed.
 	 */
 	@GenModel(notify="true", children="false", property="Readonly")
 	refers transient FeatureOfInterestNode featureOfInterestNode
 	
 	/**
 	 * The TransformNode between the geometry and the selected feature of interest.
 	 */
 	@GenModel(notify="true", children="true", property="Readonly")
 	refers transient volatile readonly TransformNode transformNode
 	
 	/**
 	 * Operations that load the geometry. The resulting node is attached under the transformNode.
 	 * Sub-classes should overwrite this method.
 	 */
 	op Node loadGeometry() 	 	
 }
 
 /**
  * Specialization of GeometryPlacementAtFeatureOfInterestTool that attaches a geometry to the specified FOI.
  */
 abstract class AbstractURLNodeGeometryPlacementAtFeatureOfInterestTool extends GeometryPlacementAtFeatureOfInterestTool
 {
 	/**
 	 * Return the offset transformation to be applied between the transformNode and the geometry.
 	 */ 	
 	op Matrix4x4 getGeometryOffsets() 	
 	
 	/**
 	 * Return the URL where the geometry is to be found.
 	 */
 	op String getGeometryURL()
 	
 	/**
 	 * The TransformNode applying the geometry offsets between the tool transform and the geometry.
 	 */
 	 @GenModel(notify="true", children="false", property="None")
 	 refers transient derived volatile readonly TransformNode cadTransformNode
 	
 	/**
 	 * The URLNode containing the geometry.
 	 */
 	 @GenModel(notify="true", children="true", property="Readonly")
 	 refers transient derived volatile readonly URLNode urlNode 	  	
 }
 
 /**
  * A simple GeometryPlacementAtFeatureOfInterestTool that contains attributes that are returned by the 
  * getGeometryOffsets() and getGeometryURL() methods.
  */
 class URLNodeGeometryPlacementAtFeatureOfInterestTool extends AbstractURLNodeGeometryPlacementAtFeatureOfInterestTool
 {
 	/**
 	 * URL to the file containing the geometry to be displayed.
 	 */
 	String URL
 	 	 	
 	/**
 	 * The transform required between the tool transformNode and the URLNode.This can be used to offset the 
 	 * geometry on the URLNode to center it on the tool origin.
 	 */
 	contains Matrix4x4 cadToToolTransform
 }

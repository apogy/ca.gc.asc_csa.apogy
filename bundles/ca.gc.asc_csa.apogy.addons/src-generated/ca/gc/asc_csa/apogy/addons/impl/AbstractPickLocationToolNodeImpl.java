/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.impl;

import ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.AbstractPickLocationToolNode;
import ca.gc.asc_csa.apogy.common.topology.impl.TransformNodeImpl;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Geometry Placement Tool Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractPickLocationToolNodeImpl#getAbstractPickLocationToolNode <em>Abstract Pick Location Tool Node</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AbstractPickLocationToolNodeImpl extends TransformNodeImpl implements AbstractPickLocationToolNode {
	/**
	 * The cached value of the '{@link #getAbstractPickLocationToolNode() <em>Abstract Pick Location Tool Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstractPickLocationToolNode()
	 * @generated
	 * @ordered
	 */
	protected AbstractPickLocationTool abstractPickLocationToolNode;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractPickLocationToolNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsPackage.Literals.ABSTRACT_PICK_LOCATION_TOOL_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractPickLocationTool getAbstractPickLocationToolNode() {
		if (abstractPickLocationToolNode != null && abstractPickLocationToolNode.eIsProxy()) {
			InternalEObject oldAbstractPickLocationToolNode = (InternalEObject)abstractPickLocationToolNode;
			abstractPickLocationToolNode = (AbstractPickLocationTool)eResolveProxy(oldAbstractPickLocationToolNode);
			if (abstractPickLocationToolNode != oldAbstractPickLocationToolNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE, oldAbstractPickLocationToolNode, abstractPickLocationToolNode));
			}
		}
		return abstractPickLocationToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractPickLocationTool basicGetAbstractPickLocationToolNode() {
		return abstractPickLocationToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstractPickLocationToolNode(AbstractPickLocationTool newAbstractPickLocationToolNode, NotificationChain msgs) {
		AbstractPickLocationTool oldAbstractPickLocationToolNode = abstractPickLocationToolNode;
		abstractPickLocationToolNode = newAbstractPickLocationToolNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE, oldAbstractPickLocationToolNode, newAbstractPickLocationToolNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractPickLocationToolNode(AbstractPickLocationTool newAbstractPickLocationToolNode) {
		if (newAbstractPickLocationToolNode != abstractPickLocationToolNode) {
			NotificationChain msgs = null;
			if (abstractPickLocationToolNode != null)
				msgs = ((InternalEObject)abstractPickLocationToolNode).eInverseRemove(this, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE, AbstractPickLocationTool.class, msgs);
			if (newAbstractPickLocationToolNode != null)
				msgs = ((InternalEObject)newAbstractPickLocationToolNode).eInverseAdd(this, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE, AbstractPickLocationTool.class, msgs);
			msgs = basicSetAbstractPickLocationToolNode(newAbstractPickLocationToolNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE, newAbstractPickLocationToolNode, newAbstractPickLocationToolNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				if (abstractPickLocationToolNode != null)
					msgs = ((InternalEObject)abstractPickLocationToolNode).eInverseRemove(this, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE, AbstractPickLocationTool.class, msgs);
				return basicSetAbstractPickLocationToolNode((AbstractPickLocationTool)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				return basicSetAbstractPickLocationToolNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				if (resolve) return getAbstractPickLocationToolNode();
				return basicGetAbstractPickLocationToolNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				setAbstractPickLocationToolNode((AbstractPickLocationTool)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				setAbstractPickLocationToolNode((AbstractPickLocationTool)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				return abstractPickLocationToolNode != null;
		}
		return super.eIsSet(featureID);
	}

} //GeometryPlacementToolNodeImpl

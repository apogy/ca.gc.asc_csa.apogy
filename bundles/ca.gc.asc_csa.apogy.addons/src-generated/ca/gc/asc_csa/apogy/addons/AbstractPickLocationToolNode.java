/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons;

import ca.gc.asc_csa.apogy.common.topology.TransformNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Geometry Placement Tool Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Node representing an AbstractGeometryPlacementTool in a topology.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationToolNode#getAbstractPickLocationToolNode <em>Abstract Pick Location Tool Node</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getAbstractPickLocationToolNode()
 * @model
 * @generated
 */
public interface AbstractPickLocationToolNode extends TransformNode {
	/**
	 * Returns the value of the '<em><b>Abstract Pick Location Tool Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getAbstractPickLocationToolNode <em>Abstract Pick Location Tool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Pick Location Tool Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * AbstractPickLocationTool being represented by this Node.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Abstract Pick Location Tool Node</em>' reference.
	 * @see #setAbstractPickLocationToolNode(AbstractPickLocationTool)
	 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getAbstractPickLocationToolNode_AbstractPickLocationToolNode()
	 * @see ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getAbstractPickLocationToolNode
	 * @model opposite="abstractPickLocationToolNode" transient="true"
	 * @generated
	 */
	AbstractPickLocationTool getAbstractPickLocationToolNode();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationToolNode#getAbstractPickLocationToolNode <em>Abstract Pick Location Tool Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Pick Location Tool Node</em>' reference.
	 * @see #getAbstractPickLocationToolNode()
	 * @generated
	 */
	void setAbstractPickLocationToolNode(AbstractPickLocationTool value);

} // GeometryPlacementToolNode

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons;

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.addons.impl.ApogyAddonsFacadeImpl;
import ca.gc.asc_csa.apogy.core.FeatureOfInterestList;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import java.util.Collection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getApogyAddonsFacade()
 * @model
 * @generated
 */
public interface ApogyAddonsFacade extends EObject 
{
	/**
	 * @generated_NOT
	 */
	public static ApogyAddonsFacade INSTANCE = ApogyAddonsFacadeImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Deletes a Simple tool.
	 * @param simpleTool The tool to delete.
	 * <!-- end-model-doc -->
	 * @model simpleToolUnique="false"
	 * @generated
	 */
	void deleteTool(SimpleTool simpleTool);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return all the FeatureOfInterestList found in the specified.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.addons.Collection<ca.gc.asc_csa.apogy.core.FeatureOfInterestList>" unique="false" invocatorSessionUnique="false"
	 * @generated
	 */
	Collection<FeatureOfInterestList> getAllFeatureOfInterestLists(InvocatorSession invocatorSession);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Gets the first {@link AbstractToolsListContainer} that is an instance of {@link SimpleToolList}, if none exist, creates one.
	 * @return Reference to the SimpleToolList.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	SimpleToolList getSimpleToolList();

} // ApogyAddonsFacade

package ca.gc.asc_csa.apogy.addons;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.NodeSelection;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.MouseButton;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple3 DTool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Base calls for Simple Tool that are used within the 3D environment.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.Simple3DTool#isVisible <em>Visible</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.Simple3DTool#getRootNode <em>Root Node</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getSimple3DTool()
 * @model abstract="true"
 * @generated
 */
public interface Simple3DTool extends SimpleTool {

	/**
	 * Returns the value of the '<em><b>Visible</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Whether or  not the tool is visible.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Visible</em>' attribute.
	 * @see #setVisible(boolean)
	 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getSimple3DTool_Visible()
	 * @model default="true" unique="false"
	 * @generated
	 */
	boolean isVisible();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.Simple3DTool#isVisible <em>Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visible</em>' attribute.
	 * @see #isVisible()
	 * @generated
	 */
	void setVisible(boolean value);

	/**
	 * Returns the value of the '<em><b>Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The root node of the current topology. Is set automatically by Apogy after the tool is registered.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Root Node</em>' reference.
	 * @see #setRootNode(Node)
	 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getSimple3DTool_RootNode()
	 * @model transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='false' children='false' property='None'"
	 * @generated
	 */
	Node getRootNode();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.Simple3DTool#getRootNode <em>Root Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Node</em>' reference.
	 * @see #getRootNode()
	 * @generated
	 */
	void setRootNode(Node value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method called upon a user selection in the 3D viewer. Sub-classes should implement.
	 * nodeSelection The node selection that has been raised by the 3D viewer.
	 * <!-- end-model-doc -->
	 * @model nodeSelectionDataType="ca.gc.asc_csa.apogy.addons.NodeSelection" nodeSelectionUnique="false"
	 * @generated
	 */
	void selectionChanged(NodeSelection nodeSelection);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method called upon a mouse click is detected in the 3D Viewer. Sub-classes should implement.
	 * @param mouseButtonClicked The mouse button that was clicked.
	 * <!-- end-model-doc -->
	 * @model mouseButtonClickedUnique="false"
	 * @generated
	 */
	void mouseButtonClicked(MouseButton mouseButtonClicked);
} // Simple3DTool

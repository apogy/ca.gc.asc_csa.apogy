/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

package ca.gc.asc_csa.apogy.addons;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.NodeSelection;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.ApogyCommonTopologyUIViewerPackage;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.MouseButton;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.TopologyViewerRegistry;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyTopology;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyFacade;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyPackage;

public class TopologyViewerListenerManager
{			
	private Adapter topologyViewerRegistryAdapter = null;
	private Adapter sessionAdapter = null;
	
	private TopologyViewerRegistry topologyViewerRegistry;
	private Node currentRootNode = null;
	private List<Simple3DTool> simple3DTools = new ArrayList<Simple3DTool>();
	
	public TopologyViewerListenerManager(TopologyViewerRegistry topologyViewerRegistry)
	{
		this.topologyViewerRegistry = topologyViewerRegistry;
		
		topologyViewerRegistry.eAdapters().add(getTopologyViewerRegistryAdapter());	
		
		// Register to the Apogy Topology.
		ApogyCoreTopologyFacade.INSTANCE.eAdapters().add(getApogyTopologyAdapter());
		
		// Initialize the root node.
		setAllSimple3DToolRootNode(resolveRootNode());		
	}
	
	public void dispose()
	{
		if(topologyViewerRegistry != null)
		{
			topologyViewerRegistry.eAdapters().remove(getTopologyViewerRegistryAdapter());
		}		
	}
	
	public void registerSimple3DTool(Simple3DTool tool)
	{
		if(!simple3DTools.contains(tool)) simple3DTools.add(tool);
		
		try
		{
			// Set the tool root Node in a Transaction friendly way.			
			ApogyCommonTransactionFacade.INSTANCE.basicSet(tool, ApogyAddonsPackage.Literals.SIMPLE3_DTOOL__ROOT_NODE, currentRootNode);			
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
	}
	
	public void unRegisterSimple3DTool(Simple3DTool tool)
	{
		simple3DTools.remove(tool);
	}
	
	public void setAllSimple3DToolRootNode(Node root)
	{		
		Logger.INSTANCE.log(Activator.ID, this, "Updating Simple3DTool  Root Node to <" + root + ">...", EventSeverity.INFO);
		currentRootNode = root;
		
		if(currentRootNode != null)
		{
			for(Simple3DTool tool : simple3DTools)
			{
				try
				{
					// Set the tool root Node in a Transaction friendly way.
					ApogyCommonTransactionFacade.INSTANCE.basicSet(tool, ApogyAddonsPackage.Literals.SIMPLE3_DTOOL__ROOT_NODE, currentRootNode);
				}
				catch(Throwable t)
				{
					t.printStackTrace();
				}
			}
		}
	}
	
	protected Node resolveRootNode()
	{
		Node root = null;
								
		InvocatorSession invocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession(); 
		if(invocatorSession != null)
		{
			if(ApogyCoreTopologyFacade.INSTANCE.getApogyTopology() != null)
			{
				root = ApogyCoreTopologyFacade.INSTANCE.getApogyTopology().getRootNode();
			}	
		}
				
		return root;
	}
	
	protected void notifyAllSimple3DTool(final NodeSelection nodeSelection)
	{
		for(Simple3DTool tool : simple3DTools)
		{
			try
			{								
				if(tool.isActive())
				{					
					tool.selectionChanged(nodeSelection);
				}
			}
			catch(Throwable t)
			{
				t.printStackTrace();
			}
		}
	}
	
	protected void notifyAllSimple3DTool(MouseButton mouseButtonClicked)
	{
		for(Simple3DTool tool : simple3DTools)
		{
			try
			{								
				if(tool.isActive())
				{					
					tool.mouseButtonClicked(mouseButtonClicked);
				}
			}
			catch(Throwable t)
			{
				t.printStackTrace();
			}
		}
	}

	protected Adapter getTopologyViewerRegistryAdapter() 
	{
		if(topologyViewerRegistryAdapter == null)
		{
			topologyViewerRegistryAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof TopologyViewerRegistry)
					{
						int featureId = msg.getFeatureID(TopologyViewerRegistry.class);
						
						switch (featureId) 
						{
							case ApogyCommonTopologyUIViewerPackage.TOPOLOGY_VIEWER_REGISTRY__LATEST_NODE_SELECTION:
								
								if(msg.getNewValue() instanceof NodeSelection)
								{
									NodeSelection nodeSelection = (NodeSelection) msg.getNewValue();
									notifyAllSimple3DTool(nodeSelection);
								}
							
							break;
							
							case ApogyCommonTopologyUIViewerPackage.TOPOLOGY_VIEWER_REGISTRY__LATEST_MOUSE_BUTTON_CLIKED:
								
								if(msg.getNewValue() instanceof MouseButton)
								{
									MouseButton mouseButton = (MouseButton) msg.getNewValue();
									notifyAllSimple3DTool(mouseButton);
								}
							
							break;

						default:
							break;
						}
					}
				}
			};
		}
		return topologyViewerRegistryAdapter;
	}		
	
	private Adapter getApogyTopologyAdapter()
	{
		if(sessionAdapter == null)
		{
			sessionAdapter = new ApogyTopologyAdapter();				
		}
		
		return sessionAdapter;
	}
	
	private class ApogyTopologyAdapter extends AdapterImpl
	{
		@Override
		public void notifyChanged(Notification msg) 
		{
			if(msg.getNotifier() instanceof ApogyCoreTopologyFacade)
			{
				int featureId = msg.getFeatureID(ApogyCoreTopologyFacade.class);
				switch (featureId) 
				{
					case ApogyCoreTopologyPackage.APOGY_CORE_TOPOLOGY_FACADE__APOGY_TOPOLOGY:
					{	
						ApogyTopology newApogyTopology = (ApogyTopology) msg.getNewValue();
						if(newApogyTopology != null)
						{							
							setAllSimple3DToolRootNode(newApogyTopology.getRootNode());
						}
						else
						{						
							setAllSimple3DToolRootNode(null);
						}
						
					}
					break;

				default:
					break;
				}
			}

		}
	}
}

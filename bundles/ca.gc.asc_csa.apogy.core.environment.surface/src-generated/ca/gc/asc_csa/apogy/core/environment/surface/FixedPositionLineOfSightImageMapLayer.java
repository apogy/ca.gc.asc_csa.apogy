/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface;

import ca.gc.asc_csa.apogy.common.math.Tuple3d;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Line Of Sight Image Map Layer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A Line Of Sight Map Layer that generated a image showing whether or not line of sight is present
 * between a point at a certain height above the mesh and a specified fixed location.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.FixedPositionLineOfSightImageMapLayer#getObserverPosition <em>Observer Position</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage#getFixedPositionLineOfSightImageMapLayer()
 * @model
 * @generated
 */
public interface FixedPositionLineOfSightImageMapLayer extends AbstractLineOfSightImageMapLayer {
	/**
	 * Returns the value of the '<em><b>Observer Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The position of the observer with which we want line of sight.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Observer Position</em>' containment reference.
	 * @see #setObserverPosition(Tuple3d)
	 * @see ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage#getFixedPositionLineOfSightImageMapLayer_ObserverPosition()
	 * @model containment="true" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Editable' propertyCategory='IMAGE_GENERATION_SETTINGS'"
	 * @generated
	 */
	Tuple3d getObserverPosition();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.surface.FixedPositionLineOfSightImageMapLayer#getObserverPosition <em>Observer Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Observer Position</em>' containment reference.
	 * @see #getObserverPosition()
	 * @generated
	 */
	void setObserverPosition(Tuple3d value);

} // LineOfSightImageMapLayer

/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.impl;

import java.util.Collection;

import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangle;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangularMesh;
import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.Activator;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshDiscreteSlopeImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.RectangularVolumeRegion;
import ca.gc.asc_csa.apogy.core.environment.surface.SlopeRange;
import edu.wlu.cs.levy.CG.KDTree;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cartesian Triangular Mesh Discrete Slope Image Map Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.impl.CartesianTriangularMeshDiscreteSlopeImageMapLayerImpl#getSlopeRanges <em>Slope Ranges</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CartesianTriangularMeshDiscreteSlopeImageMapLayerImpl extends CartesianTriangularMeshDerivedImageMapLayerImpl implements CartesianTriangularMeshDiscreteSlopeImageMapLayer {
	/**
	 * The cached value of the '{@link #getSlopeRanges() <em>Slope Ranges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlopeRanges()
	 * @generated
	 * @ordered
	 */
	protected EList<SlopeRange> slopeRanges;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CartesianTriangularMeshDiscreteSlopeImageMapLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_DISCRETE_SLOPE_IMAGE_MAP_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SlopeRange> getSlopeRanges() {
		if (slopeRanges == null) {
			slopeRanges = new EObjectContainmentEList<SlopeRange>(SlopeRange.class, this, ApogySurfaceEnvironmentPackage.CARTESIAN_TRIANGULAR_MESH_DISCRETE_SLOPE_IMAGE_MAP_LAYER__SLOPE_RANGES);
		}
		return slopeRanges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.CARTESIAN_TRIANGULAR_MESH_DISCRETE_SLOPE_IMAGE_MAP_LAYER__SLOPE_RANGES:
				return ((InternalEList<?>)getSlopeRanges()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.CARTESIAN_TRIANGULAR_MESH_DISCRETE_SLOPE_IMAGE_MAP_LAYER__SLOPE_RANGES:
				return getSlopeRanges();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.CARTESIAN_TRIANGULAR_MESH_DISCRETE_SLOPE_IMAGE_MAP_LAYER__SLOPE_RANGES:
				getSlopeRanges().clear();
				getSlopeRanges().addAll((Collection<? extends SlopeRange>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.CARTESIAN_TRIANGULAR_MESH_DISCRETE_SLOPE_IMAGE_MAP_LAYER__SLOPE_RANGES:
				getSlopeRanges().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.CARTESIAN_TRIANGULAR_MESH_DISCRETE_SLOPE_IMAGE_MAP_LAYER__SLOPE_RANGES:
				return slopeRanges != null && !slopeRanges.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public void updateImage(IProgressMonitor progressMonitor) 
	{
		if(getCartesianTriangularMeshMapLayer() != null)
		{
			CartesianTriangularMesh mesh = getCartesianTriangularMeshMapLayer().getCurrentMesh();
			
			if(mesh != null)
			{				
				SubMonitor subMonitor = SubMonitor.convert(progressMonitor, 4);
				
				long startTime = System.currentTimeMillis();		
				
				// Updates the mesh volume
				RectangularVolumeRegion meshRegion = getRectangularVolumeRegion();				
			
				if(meshRegion.getXDimension() > 0 && meshRegion.getYDimension() > 0)
				{													
					// Generate the pixel locations.	
					if(progressMonitor.isCanceled()) return;
					Point3d[][] pixelsLocation = getPixelsLocation(getCartesianTriangularMeshMapLayer().getCurrentMesh(), subMonitor.newChild(1));							
					
					int numberPixelAlongX = pixelsLocation.length;
					int numberPixelAlongY = pixelsLocation[0].length;
								
					double xIncrement = meshRegion.getXDimension() / numberPixelAlongX;
					double yIncrement = meshRegion.getYDimension() / numberPixelAlongY;										
					double averagingRadius = Math.sqrt(xIncrement*xIncrement + yIncrement*yIncrement);
					
					// Create KD Tree.		
					// Bail out if cancel is requested.
					if(progressMonitor.isCanceled()) return;					
					KDTree kdTree = createTriangleKDTree(mesh, subMonitor.newChild(1));
					
					// Find intersections.
					// Bail out if cancel is requested.
					if(progressMonitor.isCanceled()) return;
					
					CartesianTriangle[][] pixelsIntersectionTriangles = getPixelsIntersectionTriangle(pixelsLocation, mesh, kdTree, averagingRadius, progressMonitor);
	
					// Finds the normals.
					// Bail out if cancel is requested.
					if(progressMonitor.isCanceled()) return;					
					Vector3d normalsArray[][] = getNormals(pixelsLocation, pixelsIntersectionTriangles, mesh,  kdTree, averagingRadius, progressMonitor);															
					
					// Generate the color
					// Bail out if cancel is requested.
					if(progressMonitor.isCanceled()) return;
					int[][] pixelColors = getPixelsColor(normalsArray);
					
					// Generate the image.				
					// Bail out if cancel is requested.
					if(progressMonitor.isCanceled()) return;
					AbstractEImage image = convertToImage(pixelColors, subMonitor.newChild(1));
					
					long endTime = System.currentTimeMillis();
					double duration =  (endTime - startTime) * 0.001;
					Logger.INSTANCE.log(Activator.ID, this, "Updated image in <" + duration + "> seconds.", EventSeverity.INFO);
					
					// Sets image.
					ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogySurfaceEnvironmentPackage.Literals.IMAGE_MAP_LAYER__IMAGE, image);
				}
			}
		}
	}
	
	protected int[][] getPixelsColor(Vector3d normalsArray[][])
	{
		int numberPixelAlongX = normalsArray.length;
		int numberPixelAlongY = normalsArray[0].length;
		
		int[][] pixelColors = new int[numberPixelAlongX][numberPixelAlongY];					
		for(int i=0; i< numberPixelAlongX; i++)
		{
			for(int j=0; j< numberPixelAlongY; j++)
			{
				Vector3d normal = normalsArray[i][j];
				if(normal != null)
				{							
					double slope =  Math.acos(Math.abs(normal.z));
					pixelColors[i][j] = getColor(slope);					
				}
			}	
		}	
		
		return pixelColors;
	}
	
	protected int getColor(double slope) 
	{
		// Finds in which Slope Range the slope falls.
		SlopeRange slopeRange = getSlopeRange(slope);
		
		if(slopeRange != null)
		{
			Color3f color3f = slopeRange.getColor();
			return convertColor(color3f);
		}
		
		return TRANSPARENT_COLOR;
	}
	
	/**
	 * Given a slope value, return the SlopeRange in which the slope falls.
	 * @param slope The slope, in radians.
	 * @return The SlopeRange in which the slope falls, null if none is found.
	 */
	protected SlopeRange getSlopeRange(double slope)
	{
		double slopeInDegrees = Math.toDegrees(slope);
		
		for(SlopeRange slopeRange : getSlopeRanges())
		{
			if((slopeRange.getSlopeLowerBound() <= slopeInDegrees) &&
			   (slopeInDegrees < slopeRange.getSlopeUpperBound()))
			{
				return slopeRange;
			}
		}
		return null;
	}
} //CartesianTriangularMeshDiscreteSlopeImageMapLayerImpl

/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.impl;

import java.util.Iterator;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangle;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangularMesh;
import ca.gc.asc_csa.apogy.common.geometry.data3d.Geometry3DUtilities;
import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.Activator;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.FixedPositionLineOfSightImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.RectangularVolumeRegion;
import edu.wlu.cs.levy.CG.KDTree;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Line Of Sight Image Map Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.impl.FixedPositionLineOfSightImageMapLayerImpl#getObserverPosition <em>Observer Position</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FixedPositionLineOfSightImageMapLayerImpl extends AbstractLineOfSightImageMapLayerImpl implements FixedPositionLineOfSightImageMapLayer 
{
	/**
	 * The cached value of the '{@link #getObserverPosition() <em>Observer Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObserverPosition()
	 * @generated
	 * @ordered
	 */
	protected Tuple3d observerPosition;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixedPositionLineOfSightImageMapLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogySurfaceEnvironmentPackage.Literals.FIXED_POSITION_LINE_OF_SIGHT_IMAGE_MAP_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tuple3d getObserverPosition() {
		return observerPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObserverPosition(Tuple3d newObserverPosition, NotificationChain msgs) {
		Tuple3d oldObserverPosition = observerPosition;
		observerPosition = newObserverPosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogySurfaceEnvironmentPackage.FIXED_POSITION_LINE_OF_SIGHT_IMAGE_MAP_LAYER__OBSERVER_POSITION, oldObserverPosition, newObserverPosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObserverPosition(Tuple3d newObserverPosition) {
		if (newObserverPosition != observerPosition) {
			NotificationChain msgs = null;
			if (observerPosition != null)
				msgs = ((InternalEObject)observerPosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogySurfaceEnvironmentPackage.FIXED_POSITION_LINE_OF_SIGHT_IMAGE_MAP_LAYER__OBSERVER_POSITION, null, msgs);
			if (newObserverPosition != null)
				msgs = ((InternalEObject)newObserverPosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogySurfaceEnvironmentPackage.FIXED_POSITION_LINE_OF_SIGHT_IMAGE_MAP_LAYER__OBSERVER_POSITION, null, msgs);
			msgs = basicSetObserverPosition(newObserverPosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogySurfaceEnvironmentPackage.FIXED_POSITION_LINE_OF_SIGHT_IMAGE_MAP_LAYER__OBSERVER_POSITION, newObserverPosition, newObserverPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.FIXED_POSITION_LINE_OF_SIGHT_IMAGE_MAP_LAYER__OBSERVER_POSITION:
				return basicSetObserverPosition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.FIXED_POSITION_LINE_OF_SIGHT_IMAGE_MAP_LAYER__OBSERVER_POSITION:
				return getObserverPosition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.FIXED_POSITION_LINE_OF_SIGHT_IMAGE_MAP_LAYER__OBSERVER_POSITION:
				setObserverPosition((Tuple3d)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.FIXED_POSITION_LINE_OF_SIGHT_IMAGE_MAP_LAYER__OBSERVER_POSITION:
				setObserverPosition((Tuple3d)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.FIXED_POSITION_LINE_OF_SIGHT_IMAGE_MAP_LAYER__OBSERVER_POSITION:
				return observerPosition != null;
		}
		return super.eIsSet(featureID);
	}

	@Override
	public void updateImage(IProgressMonitor progressMonitor) 
	{
		if(getCartesianTriangularMeshMapLayer() != null)
		{
			CartesianTriangularMesh mesh = getCartesianTriangularMeshMapLayer().getCurrentMesh();
			
			if(mesh != null)
			{				
				SubMonitor subMonitor = null;
				if(isUseHeightPerpendicularToGround())
				{
					subMonitor = SubMonitor.convert(progressMonitor, 6);
				}
				else
				{
					subMonitor = SubMonitor.convert(progressMonitor, 4);
				}				
				
				long startTime = System.currentTimeMillis();												
				
				// Updates the mesh volume
				RectangularVolumeRegion meshRegion = getRectangularVolumeRegion();				
			
				if(meshRegion.getXDimension() > 0 && meshRegion.getYDimension() > 0)
				{
					// Generate the pixel locations.
					progressMonitor.subTask("Generating pixel locations");										
					Point3d[][] pixelsLocation = getPixelsLocation(getCartesianTriangularMeshMapLayer().getCurrentMesh(), subMonitor.newChild(1));	
					progressMonitor.worked(1);							
										
					int numberPixelAlongX = pixelsLocation.length;
					int numberPixelAlongY = pixelsLocation[0].length;
								
					double xIncrement = meshRegion.getXDimension() / numberPixelAlongX;
					double yIncrement = meshRegion.getYDimension() / numberPixelAlongY;
					double averagingRadius = Math.sqrt(xIncrement*xIncrement + yIncrement*yIncrement);
					
					// Create KD Tree.		
					progressMonitor.subTask("Create KD tree");
					KDTree kdTree = createTriangleKDTree(mesh, subMonitor.newChild(1));
					subMonitor.worked(1);		
					
					// Find intersections.
					// Bail out if cancel is requested.
					if(progressMonitor.isCanceled()) return;
					
					progressMonitor.subTask("Find pixel intersection");
					pixelsIntersectionPoints = getPixelsIntersectionPoints(pixelsLocation, mesh, kdTree, averagingRadius, subMonitor.newChild(1));
					subMonitor.worked(1);
						
					// Bail out if cancel is requested.
					if(progressMonitor.isCanceled()) return;
					
					lineOfSights = null;
					if(isUseHeightPerpendicularToGround())
					{
						// We need to computes the pixel triangle.
						CartesianTriangle[][] pixelsIntersectionTriangles = getPixelsIntersectionTriangle(pixelsLocation, mesh, kdTree, averagingRadius, progressMonitor);
						subMonitor.worked(1);
						
						// Bail out if cancel is requested.
						if(progressMonitor.isCanceled()) return;
						
						// We need to compute the normals.						
						pixelNormals = getNormals(pixelsLocation, pixelsIntersectionTriangles, mesh, kdTree, averagingRadius, progressMonitor);
						subMonitor.worked(1);
						
						// Bail out if cancel is requested.
						if(progressMonitor.isCanceled()) return;
						
						lineOfSights = getLineOfSights(pixelsIntersectionPoints, pixelNormals, mesh, getObserverPosition(), targetHeightAboveGround, progressMonitor);					
						progressMonitor.worked(1);
					}
					else
					{
						progressMonitor.subTask("Find line of sight");
						lineOfSights = getLineOfSights(pixelsIntersectionPoints, mesh, getObserverPosition(), getTargetHeightAboveGround(), progressMonitor);					
						progressMonitor.worked(1);
					}
					
					
					// Generate the color
					// Bail out if cancel is requested.
					if(progressMonitor.isCanceled()) return;
					
					int[][] pixelColors = getPixelsColor(lineOfSights);
					
					// Generate the image.				
					// Bail out if cancel is requested.
					if(progressMonitor.isCanceled()) return;
					
					AbstractEImage image = convertToImage(pixelColors, subMonitor.newChild(1));
					
					long endTime = System.currentTimeMillis();
					double duration =  (endTime - startTime) * 0.001;
					Logger.INSTANCE.log(Activator.ID, this, "Updated image in <" + duration + "> seconds.", EventSeverity.INFO);
					
					// Sets image.
					ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogySurfaceEnvironmentPackage.Literals.IMAGE_MAP_LAYER__IMAGE, image);									
				}
			}
		}
	}
	
	/**
	 * Return an array representing the line of sight state at the pixel intersection points.
	 * @param pixelsIntersectionPoints The array of pixel intersection point on the mesh.
	 * @param mesh The mesh.
	 * @param observerPose The observer pose relative to the mesh.
	 * @param targetHeightAboveGround The height above the mesh from the pixel intersectionj point.
	 * @param progressMonitor The progress monitor used to report progress.
	 * @return The array representing the line of sight state.
	 */
	protected short[][] getLineOfSights(Point3d[][] pixelsIntersectionPoints, CartesianTriangularMesh mesh, Tuple3d observerPose, double targetHeightAboveGround, IProgressMonitor progressMonitor)
	{
		progressMonitor.subTask("Finding line of sights.");
	
		Logger.INSTANCE.log(Activator.ID, this, "getLineOfSights() starts...", EventSeverity.INFO);
		
		int numberPixelAlongX = pixelsIntersectionPoints.length;
		int numberPixelAlongY = pixelsIntersectionPoints[0].length;
		
		short[][] lineOfSights = new short[numberPixelAlongX][numberPixelAlongY];
		
		int numberOfJobs = getNumberOfProcessorToUse();		
		int xIndexSlotSize = Math.floorDiv(numberPixelAlongX, numberOfJobs);

		int xStartIndex = 0;
		int xEndIndex = xIndexSlotSize;
		String family = "GetLineOfSight";
		
		for(int jobNumber = 0; jobNumber < numberOfJobs; jobNumber++)
		{						
			String jobName = "Get Line Of Sight (" + Integer.toString(jobNumber + 1) + " of " +  numberOfJobs + ") [" + xStartIndex + " ," + xEndIndex + "]";
			new GetLineOfSightsJob(jobName, family, xStartIndex, xEndIndex, pixelsIntersectionPoints, mesh, lineOfSights).schedule();
			
			// Increment the indices.
			xStartIndex += xIndexSlotSize + 1 ;
			xEndIndex = xStartIndex + xIndexSlotSize;
			if(xEndIndex >= numberPixelAlongX) xEndIndex = numberPixelAlongX - 1;
		}
		
		try 
		{
			IJobManager manager = Job.getJobManager();
			manager.join(family, progressMonitor);
		} 
		catch (Exception e) 
		{			
		}
		progressMonitor.done();
		
		Logger.INSTANCE.log(Activator.ID, this, "getLineOfSights() completed.", EventSeverity.OK);
		
		
		return lineOfSights;
	}		
	
	/**
	 * Return an array representing the line of sight state at the pixel intersection points.
	 * @param pixelsIntersectionPoints The array of pixel intersection point on the mesh.
	 * @param pixelsNormals The array containing the mesh normal at the intersection points.
	 * @param mesh The mesh.
	 * @param observerPose  The observer pose relative to the mesh.
	 * @param targetHeightAboveGround The height above the mesh, along the mesh normal, from the pixel intersectionj point.
	 * @param progressMonitor The progress monitor used to report progress.
	 * @return The array representing the line of sight state.
	 */
	protected short[][] getLineOfSights(Point3d[][] pixelsIntersectionPoints, Vector3d[][] pixelsNormals, CartesianTriangularMesh mesh, Tuple3d observerPose, double targetHeightAboveGround, IProgressMonitor progressMonitor)
	{		
		progressMonitor.subTask("Finding line of sights.");
		
		int numberPixelAlongX = pixelsNormals.length;
		int numberPixelAlongY = pixelsNormals[0].length;
		
		short[][] lineOfSights = new short[numberPixelAlongX][numberPixelAlongY];
		
		int numberOfJobs = getNumberOfProcessorToUse();		
		int xIndexSlotSize = Math.floorDiv(numberPixelAlongX, numberOfJobs);

		int xStartIndex = 0;
		int xEndIndex = xIndexSlotSize;
		String family = "GetLineOfSightJob";
		
		for(int jobNumber = 0; jobNumber < numberOfJobs; jobNumber++)
		{						
			String jobName = "Get Line Of Sight (" + Integer.toString(jobNumber + 1) + " of " +  numberOfJobs + ") [" + xStartIndex + " ," + xEndIndex + "]";
			new GetLineOfSightsPerpendicularToGroundJob(jobName, family, xStartIndex, xEndIndex, pixelsIntersectionPoints, pixelsNormals, mesh, lineOfSights).schedule();
						
			// Increment the indices.
			xStartIndex += xIndexSlotSize + 1 ;
			xEndIndex = xStartIndex + xIndexSlotSize;
			if(xEndIndex >= numberPixelAlongX) xEndIndex = numberPixelAlongX - 1;
		}
		
		try 
		{
			IJobManager manager = Job.getJobManager();
			manager.join(family, progressMonitor);
		} 
		catch (Exception e) 
		{			
		}
		progressMonitor.done();
		
		Logger.INSTANCE.log(Activator.ID, this, "getLineOfSights() completed.", EventSeverity.OK);
		
		
		return lineOfSights;
	}
	
	/**
	 * Return the array of color values representing an array of line of sight states.
	 * @param lineOfSights The array of line of sight states.
	 * @return The  array of color value.
	 */
	protected int[][] getPixelsColor(short[][] lineOfSights)
	{
		int numberPixelAlongX = lineOfSights.length;
		int numberPixelAlongY = lineOfSights[0].length;
				
		int lineOfSightRGB = getLineOfSightColor();				
		int noLineOfSightRGB = noLineOfSightColor();
		
		int[][] pixelColors = new int[numberPixelAlongX][numberPixelAlongY];					
		for(int i=0; i< numberPixelAlongX; i++)
		{
			for(int j=0; j< numberPixelAlongY; j++)
			{
				short data = lineOfSights[i][j];
				switch (data) 
				{
					case LINE_OF_SIGHT:
						pixelColors[i][j] = lineOfSightRGB;
					break;
					case NO_LINE_OF_SIGHT:
						pixelColors[i][j] = noLineOfSightRGB;
					break;	
					case NO_MESH_PROJECTION:
						pixelColors[i][j] = TRANSPARENT_COLOR;
					break;
				}
			}	
		}	
		
		return pixelColors;
	}
		
	protected int getLineOfSightColor()
	{
		return convertColor(getLineOfSightAvailableColor());		
	}
	
	protected int noLineOfSightColor()
	{
		return convertColor(getLineOfSightNotAvailableColor());		
	}
	
	protected class GetLineOfSightsJob extends ProcessPixelArrayJob<short[][]>
	{
		protected Point3d[][] pixelsIntersectionPoints;
		protected short[][] lineOfSights;
		
		
		public GetLineOfSightsJob(String name, String family, int xStartIndex, int xEndIndex, Point3d[][] pixelsIntersectionPoints, CartesianTriangularMesh mesh, short[][] lineOfSights)
		{
			super(name, family, xStartIndex, xEndIndex,mesh, null);	
			this.pixelsIntersectionPoints = pixelsIntersectionPoints;
			this.lineOfSights = lineOfSights;
			numberOfTicks = xEndIndex - xStartIndex; 
		}
		
		@Override
		protected IStatus run(IProgressMonitor monitor) 
		{
			SubMonitor subMonitor = SubMonitor.convert(monitor, getNumberOfTicks());			
			subMonitor.beginTask("Get Line Of Sight", numberOfTicks);
			int numberPixelAlongY = pixelsIntersectionPoints[0].length;
						
			Vector3d observerPosition = new Vector3d(getObserverPosition().asTuple3d());			
			
			double heightAboveGround = getTargetHeightAboveGround();
			
			for(int i = xStartIndex; i <= xEndIndex; i++)
			{	
				// Bail out if cancel is requested.
				if(monitor.isCanceled()) return Status.CANCEL_STATUS;

				for(int j = 0; j < numberPixelAlongY; j++)
				{
					// Bail out if cancel is requested.
					if(monitor.isCanceled()) return Status.CANCEL_STATUS;
					
					Point3d positionOnSurface = pixelsIntersectionPoints[i][j];
					if(positionOnSurface != null)
					{
						// Creates a vector between the observer and the location above ground.
						Vector3d target = new Vector3d(positionOnSurface.x, positionOnSurface.y, positionOnSurface.z + heightAboveGround);
						
						Point3d intersection = null;
						Iterator<CartesianTriangle> it = mesh.getPolygons().iterator();
						while(intersection == null && it.hasNext())
						{
							CartesianTriangle triangle = it.next();
							intersection = Geometry3DUtilities.getLineAndPolygonIntersectionPoint(observerPosition, target, triangle);														
						}
						
						if(intersection == null)
						{
							// No intersection, we have line of sight.
							lineOfSights[i][j] = LINE_OF_SIGHT;
						}
						else
						{
							// One Intersection, we have NO line of sight.
							lineOfSights[i][j] = NO_LINE_OF_SIGHT;							
						}
					}
					else
					{						
						lineOfSights[i][j] = NO_MESH_PROJECTION;
					}
				}
				
				subMonitor.worked(1);
			}			
			
			return Status.OK_STATUS;
		}
		
		@Override
		public short[][] getOutput() 
		{		
			return lineOfSights;
		}
	}
	
	protected class GetLineOfSightsPerpendicularToGroundJob extends ProcessPixelArrayJob<short[][]>
	{
		protected Point3d[][] pixelsIntersectionPoints;
		protected Vector3d[][] pixelsNormals;
		protected short[][] lineOfSights;
		
		
		public GetLineOfSightsPerpendicularToGroundJob(String name, String family, int xStartIndex, int xEndIndex, Point3d[][] pixelsIntersectionPoints, Vector3d[][] pixelsNormals, CartesianTriangularMesh mesh, short[][] lineOfSights)
		{
			super(name, family, xStartIndex, xEndIndex,mesh, null);	
			this.pixelsIntersectionPoints = pixelsIntersectionPoints;
			this.pixelsNormals = pixelsNormals;
			this.lineOfSights = lineOfSights;
			numberOfTicks = xEndIndex - xStartIndex; 
		}
		
		@Override
		protected IStatus run(IProgressMonitor monitor) 
		{
			SubMonitor subMonitor = SubMonitor.convert(monitor, getNumberOfTicks());			
			subMonitor.beginTask("Get Line Of Sight Perpendicular To Ground", numberOfTicks);
			int numberPixelAlongY = pixelsIntersectionPoints[0].length;
				
			Vector3d observerPosition = new Vector3d(getObserverPosition().asTuple3d());
			
			double heightAboveGround = getTargetHeightAboveGround();
			
			for(int i = xStartIndex; i <= xEndIndex; i++)
			{	
				// Bail out if cancel is requested.
				if(monitor.isCanceled()) return Status.CANCEL_STATUS;

				for(int j = 0; j < numberPixelAlongY; j++)
				{
					// Bail out if cancel is requested.
					if(monitor.isCanceled()) return Status.CANCEL_STATUS;
					
					Point3d positionOnSurface = pixelsIntersectionPoints[i][j];
					if(positionOnSurface != null)
					{
						// Creates a vector between the observer and the location above ground.						
						Vector3d target = new Vector3d(positionOnSurface.x, positionOnSurface.y, positionOnSurface.z + heightAboveGround);
						Vector3d antennaPosition = new Vector3d(pixelsNormals[i][j]);
						antennaPosition.scale(heightAboveGround);
						target.add(antennaPosition);
						
						
						Point3d intersection = null;
						Iterator<CartesianTriangle> it = mesh.getPolygons().iterator();
						while(intersection == null && it.hasNext())
						{
							CartesianTriangle triangle = it.next();
							intersection = Geometry3DUtilities.getLineAndPolygonIntersectionPoint(observerPosition, target, triangle);														
						}
						
						if(intersection == null)
						{
							// No intersection, we have line of sight.
							lineOfSights[i][j] = LINE_OF_SIGHT;
						}
						else
						{
							// One Intersection, we have NO line of sight.
							lineOfSights[i][j] = NO_LINE_OF_SIGHT;
							
						}
					}
					else
					{
						// System.err.println("NO MESH at " + pixelsIntersectionPoints[i][j]);
						lineOfSights[i][j] = NO_MESH_PROJECTION;
					}
				}
				
				subMonitor.worked(1);
			}			
			
			return Status.OK_STATUS;
		}
		
		@Override
		public short[][] getOutput() 
		{		
			return lineOfSights;
		}
	}
} //LineOfSightImageMapLayerImpl

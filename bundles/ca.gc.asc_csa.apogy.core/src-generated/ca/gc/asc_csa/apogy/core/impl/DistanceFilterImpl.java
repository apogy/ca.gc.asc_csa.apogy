/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.impl;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.emf.impl.IFilterImpl;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.DistanceFilter;
import ca.gc.asc_csa.apogy.core.Positioned;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Distance Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.impl.DistanceFilterImpl#isInclusive <em>Inclusive</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.impl.DistanceFilterImpl#getMaximumDistance <em>Maximum Distance</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.impl.DistanceFilterImpl#getPosition <em>Position</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DistanceFilterImpl<T extends Positioned> extends IFilterImpl<T> implements DistanceFilter<T> {
	/**
	 * The default value of the '{@link #isInclusive() <em>Inclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInclusive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INCLUSIVE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isInclusive() <em>Inclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInclusive()
	 * @generated
	 * @ordered
	 */
	protected boolean inclusive = INCLUSIVE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaximumDistance() <em>Maximum Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumDistance()
	 * @generated
	 * @ordered
	 */
	protected static final double MAXIMUM_DISTANCE_EDEFAULT = 10.0;

	/**
	 * The cached value of the '{@link #getMaximumDistance() <em>Maximum Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumDistance()
	 * @generated
	 * @ordered
	 */
	protected double maximumDistance = MAXIMUM_DISTANCE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPosition() <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected Tuple3d position;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DistanceFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCorePackage.Literals.DISTANCE_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInclusive() {
		return inclusive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInclusive(boolean newInclusive) {
		boolean oldInclusive = inclusive;
		inclusive = newInclusive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCorePackage.DISTANCE_FILTER__INCLUSIVE, oldInclusive, inclusive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMaximumDistance() {
		return maximumDistance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaximumDistance(double newMaximumDistance) {
		double oldMaximumDistance = maximumDistance;
		maximumDistance = newMaximumDistance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCorePackage.DISTANCE_FILTER__MAXIMUM_DISTANCE, oldMaximumDistance, maximumDistance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public Tuple3d getPosition() 
	{
		Tuple3d tmp = getPositionGen();
		if(tmp == null)
		{
			tmp = ApogyCommonMathFacade.INSTANCE.createTuple3d(0, 0, 0);
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.DISTANCE_FILTER__POSITION, tmp);
		}
		return tmp;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tuple3d getPositionGen() {
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPosition(Tuple3d newPosition, NotificationChain msgs) {
		Tuple3d oldPosition = position;
		position = newPosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyCorePackage.DISTANCE_FILTER__POSITION, oldPosition, newPosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(Tuple3d newPosition) {
		if (newPosition != position) {
			NotificationChain msgs = null;
			if (position != null)
				msgs = ((InternalEObject)position).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyCorePackage.DISTANCE_FILTER__POSITION, null, msgs);
			if (newPosition != null)
				msgs = ((InternalEObject)newPosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyCorePackage.DISTANCE_FILTER__POSITION, null, msgs);
			msgs = basicSetPosition(newPosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCorePackage.DISTANCE_FILTER__POSITION, newPosition, newPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyCorePackage.DISTANCE_FILTER__POSITION:
				return basicSetPosition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCorePackage.DISTANCE_FILTER__INCLUSIVE:
				return isInclusive();
			case ApogyCorePackage.DISTANCE_FILTER__MAXIMUM_DISTANCE:
				return getMaximumDistance();
			case ApogyCorePackage.DISTANCE_FILTER__POSITION:
				return getPosition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCorePackage.DISTANCE_FILTER__INCLUSIVE:
				setInclusive((Boolean)newValue);
				return;
			case ApogyCorePackage.DISTANCE_FILTER__MAXIMUM_DISTANCE:
				setMaximumDistance((Double)newValue);
				return;
			case ApogyCorePackage.DISTANCE_FILTER__POSITION:
				setPosition((Tuple3d)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCorePackage.DISTANCE_FILTER__INCLUSIVE:
				setInclusive(INCLUSIVE_EDEFAULT);
				return;
			case ApogyCorePackage.DISTANCE_FILTER__MAXIMUM_DISTANCE:
				setMaximumDistance(MAXIMUM_DISTANCE_EDEFAULT);
				return;
			case ApogyCorePackage.DISTANCE_FILTER__POSITION:
				setPosition((Tuple3d)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCorePackage.DISTANCE_FILTER__INCLUSIVE:
				return inclusive != INCLUSIVE_EDEFAULT;
			case ApogyCorePackage.DISTANCE_FILTER__MAXIMUM_DISTANCE:
				return maximumDistance != MAXIMUM_DISTANCE_EDEFAULT;
			case ApogyCorePackage.DISTANCE_FILTER__POSITION:
				return position != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (inclusive: ");
		result.append(inclusive);
		result.append(", maximumDistance: ");
		result.append(maximumDistance);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean matches(Positioned object) 
	{
		Point3d point = new Point3d(getPosition().asTuple3d());
		
		Matrix4d m1 = object.getPose().asMatrix4d();
		Vector3d p1 = new Vector3d();
		m1.get(p1);
		
		double distance1 = point.distance(new Point3d(p1));
		
		if(isInclusive())
		{
			return distance1 <= Math.abs(getMaximumDistance());
		}
		else
		{
			return distance1 < Math.abs(getMaximumDistance());
		}
	}

} //DistanceFilterImpl

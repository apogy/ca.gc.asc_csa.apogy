/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core;

import ca.gc.asc_csa.apogy.common.emf.IFilter;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Distance Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 *  -------------------------------------------------------------------------
 * 
 * Filters.
 * 
 * -------------------------------------------------------------------------
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.DistanceFilter#isInclusive <em>Inclusive</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.DistanceFilter#getMaximumDistance <em>Maximum Distance</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.DistanceFilter#getPosition <em>Position</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.ApogyCorePackage#getDistanceFilter()
 * @model
 * @generated
 */
public interface DistanceFilter<T extends Positioned> extends IFilter<T> {
	/**
	 * Returns the value of the '<em><b>Inclusive</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Whether or not a Positioned at exactly maximumDistance is allowed thru.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Inclusive</em>' attribute.
	 * @see #setInclusive(boolean)
	 * @see ca.gc.asc_csa.apogy.core.ApogyCorePackage#getDistanceFilter_Inclusive()
	 * @model default="true" unique="false"
	 * @generated
	 */
	boolean isInclusive();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.DistanceFilter#isInclusive <em>Inclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inclusive</em>' attribute.
	 * @see #isInclusive()
	 * @generated
	 */
	void setInclusive(boolean value);

	/**
	 * Returns the value of the '<em><b>Maximum Distance</b></em>' attribute.
	 * The default value is <code>"10.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The maximum distance from the position.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Maximum Distance</em>' attribute.
	 * @see #setMaximumDistance(double)
	 * @see ca.gc.asc_csa.apogy.core.ApogyCorePackage#getDistanceFilter_MaximumDistance()
	 * @model default="10.0" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' apogy_units='m'"
	 * @generated
	 */
	double getMaximumDistance();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.DistanceFilter#getMaximumDistance <em>Maximum Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maximum Distance</em>' attribute.
	 * @see #getMaximumDistance()
	 * @generated
	 */
	void setMaximumDistance(double value);

	/**
	 * Returns the value of the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The position from which the distance is computed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Position</em>' containment reference.
	 * @see #setPosition(Tuple3d)
	 * @see ca.gc.asc_csa.apogy.core.ApogyCorePackage#getDistanceFilter_Position()
	 * @model containment="true"
	 * @generated
	 */
	Tuple3d getPosition();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.DistanceFilter#getPosition <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' containment reference.
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(Tuple3d value);

} // DistanceFilter

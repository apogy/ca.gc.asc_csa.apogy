package ca.gc.asc_csa.apogy.core;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.core.invocator.Type;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Apogy Type</b></em>'. <!-- end-user-doc -->
 * 
 * <!-- begin-model-doc -->
 * -------------------------------------------------------------------------
 * 
 * List of registered Apogy Systems.
 * 
 * -------------------------------------------------------------------------
 * <!-- end-model-doc -->
 * 
 * 
 * @see ca.gc.asc_csa.apogy.core.ApogyCorePackage#getApogyType()
 * @model abstract="true"
 * @generated
 */
public interface ApogyType extends Type {
} // ApogyType

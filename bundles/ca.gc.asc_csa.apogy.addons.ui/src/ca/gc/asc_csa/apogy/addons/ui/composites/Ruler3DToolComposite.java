/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.Ruler3DTool;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.Color3fComposite;

public class Ruler3DToolComposite extends Composite 
{
	private Ruler3DTool ruler3DTool;
	private DataBindingContext m_bindingContext;
	
	private AbstractTwoPoints3DToolComposite abstractTwoPoints3DToolComposite;
	
	private Text txtMinorTickLength;
	private Text txtMinorTickSpacing;
	
	private Text txtMajorTickLength;
	private Text txtMajorTickSpacing;
	
	private Text txtExtremitiesRadius;
	
	private Color3fComposite rulerColorComposite;
	private Color3fComposite minorTickColorComposite;
	private Color3fComposite majorTickColorComposite;
	
	
	public Ruler3DToolComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(2, false));
		
		// TICKS
		Group ticksGroup = new Group(this, SWT.BORDER);
		ticksGroup.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		ticksGroup.setText("Ticks And Extremeties Settings");
		ticksGroup.setLayout(new GridLayout(2, false));
		
		Label lblMinorTicksSpacing = new Label(ticksGroup, SWT.NONE);
		lblMinorTicksSpacing.setText("Minor Ticks Spacing:");
		txtMinorTickSpacing = new Text(ticksGroup, SWT.BORDER);
		GridData gd_txtMinorTickSpacing = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtMinorTickSpacing.minimumWidth = 100;
		gd_txtMinorTickSpacing.widthHint = 100;
		txtMinorTickSpacing.setLayoutData(gd_txtMinorTickSpacing);
		
		Label lblMajorTicksSpacing = new Label(ticksGroup, SWT.NONE);
		lblMajorTicksSpacing.setText("Major Ticks Spacing:");	
		txtMajorTickSpacing = new Text(ticksGroup, SWT.BORDER);
		GridData gd_txtMajorTickSpacing = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtMajorTickSpacing.minimumWidth = 100;
		gd_txtMajorTickSpacing.widthHint = 100;
		txtMajorTickSpacing.setLayoutData(gd_txtMajorTickSpacing);
		
		Label lblMinorTicksLenght = new Label(ticksGroup, SWT.NONE);
		lblMinorTicksLenght.setText("Minor Ticks Lenght (m):");	
		txtMinorTickLength = new Text(ticksGroup, SWT.BORDER);
		GridData gd_txtMinorTickLength = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtMinorTickLength.widthHint = 100;
		gd_txtMinorTickLength.minimumWidth = 100;
		txtMinorTickLength.setLayoutData(gd_txtMinorTickLength);
		
		Label lblMajorTicksLenght = new Label(ticksGroup, SWT.NONE);
		lblMajorTicksLenght.setText("Major Ticks Lenght (m):");	
		txtMajorTickLength = new Text(ticksGroup, SWT.BORDER);
		GridData gd_txtMajorTickLength = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtMajorTickLength.minimumWidth = 100;
		gd_txtMajorTickLength.widthHint = 100;
		txtMajorTickLength.setLayoutData(gd_txtMajorTickLength);
		
		Label lblExtremitiesRadius = new Label(ticksGroup, SWT.NONE);
		lblExtremitiesRadius.setText("Extremities Radius (m):");	
		txtExtremitiesRadius = new Text(ticksGroup, SWT.BORDER);
		GridData gd_txtExtremitiesRadius = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtExtremitiesRadius.widthHint = 100;
		gd_txtExtremitiesRadius.minimumWidth = 100;
		txtExtremitiesRadius.setLayoutData(gd_txtExtremitiesRadius);
		
		// Colors
		Group colorsGroup = new Group(this, SWT.BORDER);
		colorsGroup.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		colorsGroup.setText("Colors Settings");
		colorsGroup.setLayout(new GridLayout(2, false));
		
		Label lblRulerColor = new Label(colorsGroup, SWT.NONE);
		lblRulerColor.setText("Ruler Color:");		
		rulerColorComposite = new Color3fComposite(colorsGroup, SWT.NONE);
		
		Label lblMinorTicksColor = new Label(colorsGroup, SWT.NONE);
		lblMinorTicksColor.setText("Minor Ticks Color:");		
		minorTickColorComposite = new Color3fComposite(colorsGroup, SWT.NONE);
		
		Label lblMajorTicksColor = new Label(colorsGroup, SWT.NONE);
		lblMajorTicksColor.setText("Major Ticks Color:");		
		majorTickColorComposite = new Color3fComposite(colorsGroup, SWT.NONE);
		
		Group endsGroup = new Group(this, SWT.NONE);
		endsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		endsGroup.setText("Attachement");
		endsGroup.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		abstractTwoPoints3DToolComposite = new AbstractTwoPoints3DToolComposite(endsGroup, SWT.NONE);	
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
		
	public Ruler3DTool getRuler3DTool() 
	{
		return ruler3DTool;
	}

	public void setRuler3DTool(Ruler3DTool ruler3dTool) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.ruler3DTool = ruler3dTool;
		
		if(ruler3dTool != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}

	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		// Ruler Color.
		rulerColorComposite.setOwner(ruler3DTool);
		rulerColorComposite.setFeature(ApogyAddonsPackage.Literals.RULER3_DTOOL__RULER_COLOR);
		
		// Minor Tick Color.
		minorTickColorComposite.setOwner(ruler3DTool);
		minorTickColorComposite.setFeature(ApogyAddonsPackage.Literals.RULER3_DTOOL__MINOR_TICK_COLOR);
				
		// Major Tick Color.
		majorTickColorComposite.setOwner(ruler3DTool);
		majorTickColorComposite.setFeature(ApogyAddonsPackage.Literals.RULER3_DTOOL__MAJOR_TICK_COLOR);
				
		abstractTwoPoints3DToolComposite.setAbstractTwoPoints3DTool(ruler3DTool);
		
		/* Minor Tick Length. */
		IObservableValue<Double> observeMinorTickLength = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(ruler3DTool), 
																	  FeaturePath.fromList(ApogyAddonsPackage.Literals.RULER3_DTOOL__MINOR_TICK_LENGTH)).observe(ruler3DTool);
		IObservableValue<String> observeMinorTickLengthTxt = WidgetProperties.text(SWT.Modify).observe(txtMinorTickLength);

		bindingContext.bindValue(observeMinorTickLengthTxt,
									observeMinorTickLength, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}
									}));
		
		/* Minor Tick Spacing. */
		IObservableValue<Double> observeMinorTickSpacing = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(ruler3DTool), 
																	  FeaturePath.fromList(ApogyAddonsPackage.Literals.RULER3_DTOOL__MINOR_TICK_SPACING)).observe(ruler3DTool);
		IObservableValue<String> observeMinorTickSpacingTxt = WidgetProperties.text(SWT.Modify).observe(txtMinorTickSpacing);

		bindingContext.bindValue(observeMinorTickSpacingTxt,
								 observeMinorTickSpacing, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}
									}));
		
		
		/* Major Tick Length. */
		IObservableValue<Double> observeMajorTickLength = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(ruler3DTool), 
																	  FeaturePath.fromList(ApogyAddonsPackage.Literals.RULER3_DTOOL__MAJOR_TICK_LENGTH)).observe(ruler3DTool);
		IObservableValue<String> observeMajorTickLengthTxt = WidgetProperties.text(SWT.Modify).observe(txtMajorTickLength);

		bindingContext.bindValue(observeMajorTickLengthTxt,
									observeMajorTickLength, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}
									}));
		
		/* Major Tick Length. */
		IObservableValue<Double> observeMajorTickSpacing = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(ruler3DTool), 
																	  FeaturePath.fromList(ApogyAddonsPackage.Literals.RULER3_DTOOL__MAJOR_TICK_SPACING)).observe(ruler3DTool);
		IObservableValue<String> observeMajorTickSpacingTxt = WidgetProperties.text(SWT.Modify).observe(txtMajorTickSpacing);

		bindingContext.bindValue(observeMajorTickSpacingTxt,
								 observeMajorTickSpacing, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}
									}));
		

		/* Major Tick Length. */
		IObservableValue<Double> observetxtExtremitiesRadius = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(ruler3DTool), 
																	  FeaturePath.fromList(ApogyAddonsPackage.Literals.RULER3_DTOOL__EXTREMITIES_RADIUS)).observe(ruler3DTool);
		IObservableValue<String> observetxtExtremitiesRadiusTxt = WidgetProperties.text(SWT.Modify).observe(txtExtremitiesRadius);

		bindingContext.bindValue(observetxtExtremitiesRadiusTxt,
								observetxtExtremitiesRadius, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}
									}));
				
		
		return bindingContext;
	}
}

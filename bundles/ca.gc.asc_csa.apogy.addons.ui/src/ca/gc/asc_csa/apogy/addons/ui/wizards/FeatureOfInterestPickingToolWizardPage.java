/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsFacade;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.FeatureOfInterestPickingTool;
import ca.gc.asc_csa.apogy.addons.ui.composites.ListOfFeatureOfInterestListComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.FeatureOfInterestList;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;

public class FeatureOfInterestPickingToolWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.ui.wizards.FeatureOfInterestPickingToolWizardPage";
		
	private FeatureOfInterestPickingTool featureOfInterestPickingTool;
	
	private ListOfFeatureOfInterestListComposite listOfFeatureOfInterestListComposite;
	
	private DataBindingContext m_bindingContext;
	
	public FeatureOfInterestPickingToolWizardPage(FeatureOfInterestPickingTool featureOfInterestPickingTool) 
	{
		super(WIZARD_PAGE_ID);
		this.featureOfInterestPickingTool = featureOfInterestPickingTool;
			
		setTitle("Feature Of Interest Picking Tool.");
		setDescription("Select the Feature Of Interest List where the FOI created with the tool will be saved.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		listOfFeatureOfInterestListComposite = new ListOfFeatureOfInterestListComposite(top, SWT.BORDER)
		{
			@Override
			protected void newFeatureOfInterestListSelected(FeatureOfInterestList featureOfInterestList) 
			{			
				ApogyCommonTransactionFacade.INSTANCE.basicSet(featureOfInterestPickingTool, ApogyAddonsPackage.Literals.FEATURE_OF_INTEREST_PICKING_TOOL__FEATURE_OF_INTEREST_LIST, featureOfInterestList);
				validate();
			}
		};		
		listOfFeatureOfInterestListComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		InvocatorSession invocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();
		listOfFeatureOfInterestListComposite.setList(ApogyAddonsFacade.INSTANCE.getAllFeatureOfInterestLists(invocatorSession));
		
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
	}	
	
	@Override
	public void dispose() 
	{	
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		super.dispose();
	}
	
	protected void validate()
	{
		setErrorMessage(null);
		
		if(featureOfInterestPickingTool.getFeatureOfInterestList() == null)
		{
			setErrorMessage("The Feature Of Interest List is not set !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
	
	
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.TrajectoryPickingTool;
import ca.gc.asc_csa.apogy.addons.geometry.paths.WayPointPath;
import ca.gc.asc_csa.apogy.addons.geometry.paths.ui.composites.WayPointPathComboComposite;
import ca.gc.asc_csa.apogy.addons.geometry.paths.ui.composites.WayPointPathListComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class TrajectoryPickingToolComposite extends Composite 
{
	private TrajectoryPickingTool trajectoryPickingTool;
	private DataBindingContext m_bindingContext;
		
	private Text txtHeightOffset;
	private Button btnClearActivePath;
	private Text txtTotalLenght;
	
	private WayPointPathComboComposite wayPointPathComboComposite;
	private WayPointPathListComposite wayPointPathListComposite;
			
	public TrajectoryPickingToolComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(1, false));
		
		// Settings
		Group settingsGroup = new Group(this, SWT.BORDER);
		settingsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		settingsGroup.setText("Path Settings");
		settingsGroup.setLayout(new GridLayout(3, false));
				
		Label lblMinorTicksLenght = new Label(settingsGroup, SWT.NONE);
		lblMinorTicksLenght.setText("Height Offset (m):");	
		txtHeightOffset = new Text(settingsGroup, SWT.BORDER);
		txtHeightOffset.setToolTipText("Offset to apply to the point to keep a waypoint path above ground.");
		GridData gd_txtMinorTickLength = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtMinorTickLength.widthHint = 100;
		gd_txtMinorTickLength.minimumWidth = 100;
		txtHeightOffset.setLayoutData(gd_txtMinorTickLength);
		new Label(settingsGroup, SWT.NONE);
		
		Label lblTotalLenghtm = new Label(settingsGroup, SWT.NONE);
		lblTotalLenghtm.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTotalLenghtm.setText("Total Lenght (m):");
		
		txtTotalLenght = new Text(settingsGroup, SWT.BORDER);
		txtTotalLenght.setEditable(false);
		GridData gd_txtTotalLenght = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtTotalLenght.widthHint = 100;
		gd_txtTotalLenght.minimumWidth = 100;
		txtTotalLenght.setLayoutData(gd_txtTotalLenght);
		new Label(settingsGroup, SWT.NONE);
		
		Label lblActivePath = new Label(settingsGroup, SWT.NONE);
		lblActivePath.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblActivePath.setText("Active Path:");
		
		wayPointPathComboComposite = new WayPointPathComboComposite(settingsGroup, SWT.NONE)
		{
			@Override
			protected void newSelection(WayPointPath selectedWayPointPath) 
			{
				if(ApogyCommonTransactionFacade.INSTANCE.areEditingDomainsValid(trajectoryPickingTool, ApogyAddonsPackage.Literals.TRAJECTORY_PICKING_TOOL__ACTIVE_PATH, selectedWayPointPath, false) == ApogyCommonTransactionFacade.EXECUTE_COMMAND_ON_OWNER_DOMAIN)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet(trajectoryPickingTool, ApogyAddonsPackage.Literals.TRAJECTORY_PICKING_TOOL__ACTIVE_PATH, selectedWayPointPath);
				}
				else
				{
					trajectoryPickingTool.setActivePath(selectedWayPointPath);
				}
			}
		};
		GridData gd_wayPointPathComboComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_wayPointPathComboComposite.widthHint = 250;
		gd_wayPointPathComboComposite.minimumWidth = 250;
		wayPointPathComboComposite.setLayoutData(gd_wayPointPathComboComposite);
		
		btnClearActivePath = new Button(settingsGroup, SWT.PUSH);
		btnClearActivePath.setText("Clear Active Path");	
		btnClearActivePath.setToolTipText("Remove all points from active path.");
		btnClearActivePath.addSelectionListener(new SelectionListener() 
		{		
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				if(trajectoryPickingTool != null)
				{
					trajectoryPickingTool.clearActivePath();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) 
			{					
			}
		});
			
		Group pathsListGroup = new Group(this, SWT.BORDER);
		pathsListGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		pathsListGroup.setText("Path Segments");
		pathsListGroup.setLayout(new GridLayout(3, false));

		wayPointPathListComposite = new WayPointPathListComposite(pathsListGroup, SWT.NONE, true, trajectoryPickingTool, ApogyAddonsPackage.Literals.TRAJECTORY_PICKING_TOOL__PATHS);
		wayPointPathListComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));		
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
		
	public TrajectoryPickingTool getTrajectoryPickingTool() 
	{
		return trajectoryPickingTool;
	}

	public void setTrajectoryPickingTool(TrajectoryPickingTool trajectoryPickingTool) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.trajectoryPickingTool = trajectoryPickingTool;
		
		if(trajectoryPickingTool != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}

	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
						
		/* Offset Height. */
		IObservableValue<Double> observeHeightOffset = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(trajectoryPickingTool), 
																	  FeaturePath.fromList(ApogyAddonsPackage.Literals.TRAJECTORY_PICKING_TOOL__ALTITUDE_OFFSET)).observe(trajectoryPickingTool);
		IObservableValue<String> observeHeightOffsetTxt = WidgetProperties.text(SWT.Modify).observe(txtHeightOffset);

		bindingContext.bindValue(observeHeightOffsetTxt,
									observeHeightOffset, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}
									}));
		
		/* Total Length. */
		IObservableValue<Double> observeTotalLength = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(trajectoryPickingTool), 
																	  FeaturePath.fromList(ApogyAddonsPackage.Literals.TRAJECTORY_PICKING_TOOL__TOTAL_LENGTH)).observe(trajectoryPickingTool);
		IObservableValue<String> observeTotalLengthTxt = WidgetProperties.text(SWT.Modify).observe(txtTotalLenght);

		bindingContext.bindValue(observeTotalLengthTxt,
								 observeTotalLength, 
								 	new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}
									}));
		
		// Active Path.
		wayPointPathComboComposite.setList(trajectoryPickingTool.getPaths());
		wayPointPathComboComposite.setSelectedWayPointPath(trajectoryPickingTool.getActivePath());
		
		// Path Segments
		wayPointPathListComposite.setOwner(trajectoryPickingTool);
		wayPointPathListComposite.setFeature(ApogyAddonsPackage.Literals.TRAJECTORY_PICKING_TOOL__PATHS);
		
		return bindingContext;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ui.parts;

import java.util.Iterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.SimpleToolList;
import ca.gc.asc_csa.apogy.addons.ui.composites.SimpleToolsComposite;
import ca.gc.asc_csa.apogy.core.invocator.AbstractToolsListContainer;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;

public class SimpleToolsPart extends AbstractSessionBasedPart
{
	
	private SimpleToolsComposite simpleToolsComposite;
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{
		simpleToolsComposite.setSimpleToolList(resolveSimpleToolList());
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{						
		simpleToolsComposite = new SimpleToolsComposite(parent, SWT.NONE);
	}

	protected SimpleToolList resolveSimpleToolList()
	{
		InvocatorSession invocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();
		
		SimpleToolList simpleToolList = null;
		if(invocatorSession != null)
		{			
			Iterator<AbstractToolsListContainer> it = invocatorSession.getToolsList().getToolsListContainers().iterator();
			
			while(it.hasNext() && simpleToolList == null)
			{
				AbstractToolsListContainer list = it.next();
				if(list instanceof SimpleToolList)
				{
					simpleToolList = (SimpleToolList) list;
				}
			}
		}
		
		return simpleToolList;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.TrajectoryPickingTool;
import ca.gc.asc_csa.apogy.addons.ui.composites.TrajectoryPickingToolComposite;

public class TrajectoryPickingToolWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.ui.wizards.Trajectory3DToolWizardPage";
		
	private TrajectoryPickingTool trajectoryPickingTool;
	private TrajectoryPickingToolComposite trajectoryPickingToolComposite;
	private DataBindingContext m_bindingContext;
	
	public TrajectoryPickingToolWizardPage(TrajectoryPickingTool trajectoryPickingTool) 
	{
		super(WIZARD_PAGE_ID);
		this.trajectoryPickingTool = trajectoryPickingTool;
			
		setTitle("Trajectory Picking Tool.");
		setDescription("Configure the Trajectory Picking Tool.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		trajectoryPickingToolComposite = new TrajectoryPickingToolComposite(top, SWT.NONE);
		trajectoryPickingToolComposite.setTrajectoryPickingTool(trajectoryPickingTool);
		
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
	}	
	
	@Override
	public void dispose() 
	{	
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		super.dispose();
	}
	
	protected void validate()
	{
		setErrorMessage(null);		
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}

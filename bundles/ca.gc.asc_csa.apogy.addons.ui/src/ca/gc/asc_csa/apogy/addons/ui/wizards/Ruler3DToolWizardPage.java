/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.Ruler3DTool;
import ca.gc.asc_csa.apogy.addons.ui.composites.Ruler3DToolComposite;

public class Ruler3DToolWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.ui.wizards.FeatureOfInterestPickingToolWizardPage";
		
	private Ruler3DTool ruler3DTool;
	
	private Ruler3DToolComposite ruler3DToolComposite;
	
	private DataBindingContext m_bindingContext;
	
	public Ruler3DToolWizardPage(Ruler3DTool ruler3DTool) 
	{
		super(WIZARD_PAGE_ID);
		this.ruler3DTool = ruler3DTool;
			
		setTitle("Ruler 3D Tool.");
		setDescription("Configure the Ruler end points.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		ruler3DToolComposite = new Ruler3DToolComposite(top, SWT.NONE);
		ruler3DToolComposite.setRuler3DTool(ruler3DTool);
	
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
	}	
	
	@Override
	public void dispose() 
	{	
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		super.dispose();
	}
	
	protected void validate()
	{
		setErrorMessage(null);		
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
	
	
}

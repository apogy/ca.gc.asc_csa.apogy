/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.AbstractTwoPoints3DTool;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dComposite;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.NodePath;
import ca.gc.asc_csa.apogy.common.topology.ui.dialogs.NodeSelectionDialog;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyFacade;

public class AbstractTwoPoints3DToolComposite extends Composite 
{
	private AbstractTwoPoints3DTool abstractTwoPoints3DTool;
	
	private Text txtFromNodeID;
	private Button btnSelectFromNode;
	private Tuple3dComposite fromRelativePositionComposite;
	private Button btnFromLocked;
	
	private Text txtToNodeID;
	private Button btnSelectToNode;
	private Tuple3dComposite toRelativePositionComposite;
	private Button btnToLocked;
	
	private DataBindingContext m_bindingContext;
	
	public AbstractTwoPoints3DToolComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, true));
		
		Group grpFrom = new Group(this, SWT.NONE);
		grpFrom.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpFrom.setText("From");
		grpFrom.setLayout(new GridLayout(3, false));
		
		Label lblFromNode = new Label(grpFrom, SWT.NONE);
		lblFromNode.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFromNode.setText("Node:");
		
		txtFromNodeID = new Text(grpFrom, SWT.BORDER);
		txtFromNodeID.setEditable(false);
		GridData gd_lblFromNodeID = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblFromNodeID.minimumWidth = 250;
		gd_lblFromNodeID.widthHint = 250;
		txtFromNodeID.setLayoutData(gd_lblFromNodeID);
		
		btnSelectFromNode = new Button(grpFrom, SWT.NONE);
		btnSelectFromNode.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnSelectFromNode.setText("Select");
		btnSelectFromNode.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				// Opens the Node Selection Dialog.
				NodeSelectionDialog nodeSelectionDialog = new NodeSelectionDialog(getShell(), getRootNode());
				
				if(nodeSelectionDialog.open() == Window.OK)		
				{
					Node fromNode = nodeSelectionDialog.getSelectedNode();
					NodePath nodePath = ApogyCommonTopologyFacade.INSTANCE.createNodePath(getRootNode(), fromNode);
					
					if(ApogyCommonTransactionFacade.INSTANCE.areEditingDomainsValid(getAbstractTwoPoints3DTool(), ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE, fromNode, false) == ApogyCommonTransactionFacade.EXECUTE_COMMAND_ON_OWNER_DOMAIN)
					{
						ApogyCommonTransactionFacade.INSTANCE.basicSet(getAbstractTwoPoints3DTool(), ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE, fromNode);
						ApogyCommonTransactionFacade.INSTANCE.basicSet(getAbstractTwoPoints3DTool(), ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH, nodePath);
					}
					else
					{
						getAbstractTwoPoints3DTool().setFromNode(fromNode);
						getAbstractTwoPoints3DTool().setFromNodeNodePath(nodePath);
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) 
			{			
			}
		});
		
		Label lblRelativePositionm = new Label(grpFrom, SWT.NONE);
		lblRelativePositionm.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRelativePositionm.setText("Relative Position (m):");
		
		fromRelativePositionComposite = new Tuple3dComposite(grpFrom, SWT.NONE, "0.000");
		fromRelativePositionComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		Label lblNodeLocked = new Label(grpFrom, SWT.NONE);
		lblNodeLocked.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNodeLocked.setText("Node Locked:");
		
		btnFromLocked = new Button(grpFrom, SWT.CHECK);
		new Label(grpFrom, SWT.NONE);
		
		// TO
		
		Group grpTo = new Group(this, SWT.NONE);
		grpTo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpTo.setText("To");
		grpTo.setLayout(new GridLayout(3, false));	
		
		Label lblToNode = new Label(grpTo, SWT.NONE);
		lblToNode.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblToNode.setText("Node:");
		
		txtToNodeID = new Text(grpTo, SWT.BORDER);
		txtToNodeID.setEditable(false);
		GridData gd_lblToNodeID = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblToNodeID.minimumWidth = 250;
		gd_lblToNodeID.widthHint = 250;
		txtToNodeID.setLayoutData(gd_lblToNodeID);
		
		btnSelectToNode = new Button(grpTo, SWT.NONE);
		btnSelectToNode.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnSelectToNode.setText("Select");
		btnSelectToNode.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				// Opens the Node Selection Dialog.
				NodeSelectionDialog nodeSelectionDialog = new NodeSelectionDialog(getShell(), getRootNode());
				
				if(nodeSelectionDialog.open() == Window.OK)		
				{
					Node toNode = nodeSelectionDialog.getSelectedNode();
					NodePath nodePath = ApogyCommonTopologyFacade.INSTANCE.createNodePath(getRootNode(), toNode);
					
					if(ApogyCommonTransactionFacade.INSTANCE.areEditingDomainsValid(getAbstractTwoPoints3DTool(), ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE, toNode, false) == ApogyCommonTransactionFacade.EXECUTE_COMMAND_ON_OWNER_DOMAIN)
					{
						ApogyCommonTransactionFacade.INSTANCE.basicSet(getAbstractTwoPoints3DTool(), ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE, toNode);
						ApogyCommonTransactionFacade.INSTANCE.basicSet(getAbstractTwoPoints3DTool(), ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH, nodePath);
					}
					else
					{
						getAbstractTwoPoints3DTool().setToNode(toNode);
						getAbstractTwoPoints3DTool().setToNodeNodePath(nodePath);
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) 
			{			
			}
		});
		
		Label lblToRelativePosition = new Label(grpTo, SWT.NONE);
		lblToRelativePosition.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblToRelativePosition.setText("Relative Position (m):");
		
		toRelativePositionComposite = new Tuple3dComposite(grpTo, SWT.NONE, "0.000");
		toRelativePositionComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
					
		Label lblFromNodeLocked = new Label(grpTo, SWT.NONE);
		lblFromNodeLocked.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFromNodeLocked.setText("Node Locked:");
		
		btnToLocked = new Button(grpTo, SWT.CHECK);		
		new Label(grpTo, SWT.NONE);
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	@Override
	public void dispose() 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		super.dispose();
	}
	
	public AbstractTwoPoints3DTool getAbstractTwoPoints3DTool() 
	{
		return abstractTwoPoints3DTool;
	}

	public void setAbstractTwoPoints3DTool(AbstractTwoPoints3DTool newAbstractTwoPoints3DTool) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.abstractTwoPoints3DTool = newAbstractTwoPoints3DTool;
		
		if(newAbstractTwoPoints3DTool != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}	
	
	private Node getRootNode()
	{
		if(ApogyCoreTopologyFacade.INSTANCE.getApogyTopology() != null)
		{
			return ApogyCoreTopologyFacade.INSTANCE.getApogyTopology().getRootNode();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		// From Node ID
		IObservableValue<Node> observeFromNode = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getAbstractTwoPoints3DTool()), 
				  FeaturePath.fromList(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE)).observe(getAbstractTwoPoints3DTool());
	
		IObservableValue<String> observeFromNodeLabel = WidgetProperties.text(SWT.Modify).observe(txtFromNodeID);
		
		bindingContext.bindValue(observeFromNodeLabel,
								observeFromNode, 
								new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				 new UpdateValueStrategy().setConverter(new Converter(Node.class, String.class)
				 {																		
					@Override
					public Object convert(Object fromObject) 
					{			
						if(fromObject instanceof Node)
						{
							return ((Node) fromObject).getNodeId();
						}
						else
						{
							return "";
						}
					}
					}));
		
		
		// From Node Lock Button
		IObservableValue<Double> observeFromNodeLock = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getAbstractTwoPoints3DTool()), 
				  FeaturePath.fromList(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_LOCK)).observe(getAbstractTwoPoints3DTool());
		
		IObservableValue<String> observeFromNodeLockButton = WidgetProperties.selection().observe(btnFromLocked);
		
		bindingContext.bindValue(observeFromNodeLockButton,
								 observeFromNodeLock, 
								 new UpdateValueStrategy(),	
								 new UpdateValueStrategy());
		
		// From Relative Position		
		fromRelativePositionComposite.setTuple3d(abstractTwoPoints3DTool.getFromRelativePosition());	
		
		// From To ID
		IObservableValue<Node> observeToNode = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getAbstractTwoPoints3DTool()), 
				  FeaturePath.fromList(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE)).observe(getAbstractTwoPoints3DTool());
	
		IObservableValue<String> observeToNodeLabel = WidgetProperties.text(SWT.Modify).observe(txtToNodeID);
		
		bindingContext.bindValue(observeToNodeLabel,
								observeToNode, 
								new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				 new UpdateValueStrategy().setConverter(new Converter(Node.class, String.class)
				 {																		
					@Override
					public Object convert(Object fromObject) 
					{			
						if(fromObject instanceof Node)
						{
							return ((Node) fromObject).getNodeId();
						}
						else
						{
							return "";
						}
					}
					}));
		
		
		// From To Lock Button
		IObservableValue<Double> observeToNodeLock = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getAbstractTwoPoints3DTool()), 
				  FeaturePath.fromList(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_LOCK)).observe(getAbstractTwoPoints3DTool());
		
		IObservableValue<String> observeToNodeLockButton = WidgetProperties.selection().observe(btnToLocked);
		
		bindingContext.bindValue(observeToNodeLockButton,
								 observeToNodeLock, 
								 new UpdateValueStrategy(),	
								 new UpdateValueStrategy());
		
		// To Relative Position
		toRelativePositionComposite.setTuple3d(abstractTwoPoints3DTool.getToRelativePosition());	

								
		return bindingContext;
	}
}

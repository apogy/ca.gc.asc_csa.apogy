package ca.gc.asc_csa.apogy.addons.ui;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import javax.vecmath.Color3f;

import ca.gc.asc_csa.apogy.common.topology.ui.SceneObject;

public interface Ruler3dToolSceneObject extends SceneObject 
{
	public void setRulerColor(Color3f color);
	
	public void setMinorTicksColor(Color3f color);
		
	public void setMajorTicksColor(Color3f color);
	
	public void setMinorTickSpacing(float minorTickSpacing);

	public void setMajorTickSpacing(float majorTickSpacing);
		
	public void setMinorTickLength(float minorTickLength);
	
	public void setMajorTickLength(float minorTickLength);
	
	public void setExtremitiesRadius(float newRadius);	
}

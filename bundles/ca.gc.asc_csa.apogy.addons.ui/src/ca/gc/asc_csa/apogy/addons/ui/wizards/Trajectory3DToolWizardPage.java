/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.Trajectory3DTool;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.Variable;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.VariablesListComposite;

public class Trajectory3DToolWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.ui.wizards.Trajectory3DToolWizardPage";
		
	private Trajectory3DTool trajectory3DTool;
		
	private VariablesListComposite variablesListComposite;
	
	private DataBindingContext m_bindingContext;
	
	public Trajectory3DToolWizardPage(Trajectory3DTool trajectory3DTool) 
	{
		super(WIZARD_PAGE_ID);
		this.trajectory3DTool = trajectory3DTool;
			
		setTitle("Trajectory 3D Tool.");
		setDescription("Select the variable to track.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		variablesListComposite =  new VariablesListComposite(top, SWT.NONE)
		{
			@Override
			protected void newSelection(ISelection selection) 
			{
				Variable variable = variablesListComposite.getSelectedVariable();
				
				if(ApogyCommonTransactionFacade.INSTANCE.areEditingDomainsValid(trajectory3DTool, ApogyAddonsPackage.Literals.TRAJECTORY3_DTOOL__VARIABLE, variable, false) == ApogyCommonTransactionFacade.EXECUTE_COMMAND_ON_OWNER_DOMAIN)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet(trajectory3DTool, ApogyAddonsPackage.Literals.TRAJECTORY3_DTOOL__VARIABLE, variable);
				}
				else
				{				
					trajectory3DTool.setVariable(variable);
				}
			}
		};
		
		InvocatorSession invocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();		
		if(invocatorSession != null && invocatorSession.getEnvironment() != null)
		{
			variablesListComposite.setVariablesList(invocatorSession.getEnvironment().getVariablesList());
		}
		
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
	}	
	
	@Override
	public void dispose() 
	{	
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		super.dispose();
	}
	
	protected void validate()
	{
		setErrorMessage(null);		
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.SimpleTool;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class SimpleToolsDetailsComposite extends Composite 
{
	private Button btnActivate;
	private Button btnDeactivate;
	private Composite compositeDetails;
	
	private DataBindingContext m_bindingContext;
	
	private SimpleTool simpleTool;
	
	public SimpleToolsDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, false));
		
		btnActivate = new Button(this, SWT.NONE);
		btnActivate.setText("Activate");
		btnActivate.setEnabled(false);
		btnActivate.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{				
				if(getSimpleTool() != null)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet(getSimpleTool(), ApogyAddonsPackage.Literals.SIMPLE_TOOL__ACTIVE, true);
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
		btnDeactivate = new Button(this, SWT.NONE);
		btnDeactivate.setText("De-Activate");
		btnDeactivate.setEnabled(false);
		btnDeactivate.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{				
				if(getSimpleTool() != null)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet(getSimpleTool(), ApogyAddonsPackage.Literals.SIMPLE_TOOL__ACTIVE, false);					
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
		
		compositeDetails = new Composite(this, SWT.BORDER);
		compositeDetails.setLayout(new GridLayout());
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gd_compositeDetails.minimumWidth = 400;
		gd_compositeDetails.widthHint = 400;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.layout();
		
		m_bindingContext = customInitDataBindings();
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
	
	public SimpleTool getSimpleTool() {
		return simpleTool;
	}

	public void setSimpleTool(SimpleTool simpleTool) 
	{
		// Dispose of current bindings if applicable.
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.simpleTool = simpleTool;		
		
		if(simpleTool != null)
		{
			// Updates bindings
			m_bindingContext = customInitDataBindings();
			
			try
			{				
				ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, simpleTool);
			}
			catch(Throwable t)
			{
				t.printStackTrace();
			}
		}
	}	
	
	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{		
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> simpleToolObservable = EMFProperties.value(FeaturePath.fromList(ApogyAddonsPackage.Literals.SIMPLE_TOOL__ACTIVE)).observe(getSimpleTool());

		/* Activate Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnActivateWidget = WidgetProperties.enabled().observe(btnActivate);
		UpdateValueStrategy btnActivateEnableStrategy = new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
		{
			@Override
			public Object convert(Object value) 
			{
				return !((Boolean) value).booleanValue();
			}
		};
		
		bindingContext.bindValue(observeEnabledBtnActivateWidget, simpleToolObservable, new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), btnActivateEnableStrategy);

		/* Activate Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDectivateWidget = WidgetProperties.enabled().observe(btnDeactivate);
		UpdateValueStrategy btnDeactivateEnableStrategy = new UpdateValueStrategy();
		bindingContext.bindValue(observeEnabledBtnDectivateWidget, simpleToolObservable, new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), btnDeactivateEnableStrategy);
		
		return bindingContext;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.addons.SimpleTool;
import ca.gc.asc_csa.apogy.addons.SimpleToolList;

public class SimpleToolsComposite extends Composite 
{
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());

	private SimpleToolList simpleToolList;
	
	private SimpleToolListComposite simpleToolListComposite;
	private SimpleToolsDetailsComposite simpleToolsDetailsComposite;
	
	public SimpleToolsComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new GridLayout(1, false));
		
		ScrolledForm scrldfrmSimpleTools = formToolkit.createScrolledForm(this);
		scrldfrmSimpleTools.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrldfrmSimpleTools);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.numColumns = 2;
			scrldfrmSimpleTools.getBody().setLayout(tableWrapLayout);
		}
		
		Section sctnSimpleTools = formToolkit.createSection(scrldfrmSimpleTools.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnSimpleTools = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 1, 1);
		twd_sctnSimpleTools.align = TableWrapData.FILL;
		twd_sctnSimpleTools.valign = TableWrapData.FILL;
		twd_sctnSimpleTools.grabVertical = true;
		sctnSimpleTools.setLayoutData(twd_sctnSimpleTools);
		formToolkit.paintBordersFor(sctnSimpleTools);
		sctnSimpleTools.setText("Simple Tools");
		
		simpleToolListComposite = new SimpleToolListComposite(sctnSimpleTools, SWT.NONE)
		{
			@Override
			protected void newSimpleToolSelected(SimpleTool simpleTool) 
			{
				simpleToolsDetailsComposite.setSimpleTool(simpleTool);				
			}
		};
		formToolkit.adapt(simpleToolListComposite);
		formToolkit.paintBordersFor(simpleToolListComposite);
		sctnSimpleTools.setClient(simpleToolListComposite);
		
		Section sctnToolDetails = formToolkit.createSection(scrldfrmSimpleTools.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnToolDetails = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 1, 1);
		twd_sctnToolDetails.align = TableWrapData.FILL;
		twd_sctnToolDetails.grabVertical = true;
		twd_sctnToolDetails.grabHorizontal = true;
		twd_sctnToolDetails.valign = TableWrapData.FILL;
		sctnToolDetails.setLayoutData(twd_sctnToolDetails);
		formToolkit.paintBordersFor(sctnToolDetails);
		sctnToolDetails.setText("Tool Details");
		
		simpleToolsDetailsComposite = new SimpleToolsDetailsComposite(sctnToolDetails, SWT.NONE);
		formToolkit.adapt(simpleToolsDetailsComposite);
		formToolkit.paintBordersFor(simpleToolsDetailsComposite);
		sctnToolDetails.setClient(simpleToolsDetailsComposite);
					
	}
	
	public void setSimpleToolList(SimpleToolList newSimpleToolList)
	{
		this.simpleToolList = newSimpleToolList;
		simpleToolListComposite.setSimpleToolList(newSimpleToolList);
	}

	public SimpleToolList getSimpleToolList() {
		return simpleToolList;
	}		
	
}

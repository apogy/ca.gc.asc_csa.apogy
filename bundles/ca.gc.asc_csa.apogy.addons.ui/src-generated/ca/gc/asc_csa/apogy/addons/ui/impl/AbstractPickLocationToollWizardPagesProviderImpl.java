/**
 * Copyright (c) 2015-2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.ui.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsFactory;
import ca.gc.asc_csa.apogy.addons.ui.AbstractPickLocationToollWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings;
import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Pick Location Tooll Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AbstractPickLocationToollWizardPagesProviderImpl extends Simple3DToolWizardPagesProviderImpl implements AbstractPickLocationToollWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractPickLocationToollWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsUIPackage.Literals.ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER;
	}
	
	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		AbstractPickLocationTool tool = ApogyAddonsFactory.eINSTANCE.createAbstractPickLocationTool();		
		
		if(settings instanceof AbstractToolEClassSettings)
		{
			AbstractToolEClassSettings abstractToolEClassSettings = (AbstractToolEClassSettings) settings;
			tool.setName(abstractToolEClassSettings.getName());
			tool.setDescription(abstractToolEClassSettings.getDescription());
		}
		
		return tool;
	}
} //AbstractPickLocationToollWizardPagesProviderImpl

/**
 * Copyright (c) 2015-2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsFactory;
import ca.gc.asc_csa.apogy.addons.FeatureOfInterestPickingTool;
import ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings;
import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage;
import ca.gc.asc_csa.apogy.addons.ui.FeatureOfInterestPickingToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.wizards.FeatureOfInterestPickingToolWizardPage;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Of Interest Picking Tool Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FeatureOfInterestPickingToolWizardPagesProviderImpl extends Simple3DToolWizardPagesProviderImpl implements FeatureOfInterestPickingToolWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureOfInterestPickingToolWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsUIPackage.Literals.FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		FeatureOfInterestPickingTool tool = ApogyAddonsFactory.eINSTANCE.createFeatureOfInterestPickingTool();
		
		if(settings instanceof AbstractToolEClassSettings)
		{
			AbstractToolEClassSettings abstractToolEClassSettings = (AbstractToolEClassSettings) settings;
			tool.setName(abstractToolEClassSettings.getName());
			tool.setDescription(abstractToolEClassSettings.getDescription());
		}
		
		return tool;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();
		list.addAll(super.instantiateWizardPages(eObject, settings));

		FeatureOfInterestPickingTool tool = (FeatureOfInterestPickingTool) eObject;		
		list.add(new FeatureOfInterestPickingToolWizardPage(tool));

		return list;
	}

} //FeatureOfInterestPickingToolWizardPagesProviderImpl

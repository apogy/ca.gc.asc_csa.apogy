package ca.gc.asc_csa.apogy.addons.ui;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage
 * @generated
 */
public interface ApogyAddonsUIFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyAddonsUIFactory eINSTANCE = ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Ruler3d Tool Node Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ruler3d Tool Node Presentation</em>'.
	 * @generated
	 */
	Ruler3dToolNodePresentation createRuler3dToolNodePresentation();

	/**
	 * Returns a new object of class '<em>Trajectory3 DTool Node Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trajectory3 DTool Node Presentation</em>'.
	 * @generated
	 */
	Trajectory3DToolNodePresentation createTrajectory3DToolNodePresentation();

	/**
	 * Returns a new object of class '<em>Abstract Tool EClass Settings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Tool EClass Settings</em>'.
	 * @generated
	 */
	AbstractToolEClassSettings createAbstractToolEClassSettings();

	/**
	 * Returns a new object of class '<em>Abstract Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Tool Wizard Pages Provider</em>'.
	 * @generated
	 */
	AbstractToolWizardPagesProvider createAbstractToolWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Simple Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Tool Wizard Pages Provider</em>'.
	 * @generated
	 */
	SimpleToolWizardPagesProvider createSimpleToolWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Simple3 DTool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple3 DTool Wizard Pages Provider</em>'.
	 * @generated
	 */
	Simple3DToolWizardPagesProvider createSimple3DToolWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Feature Of Interest Picking Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Of Interest Picking Tool Wizard Pages Provider</em>'.
	 * @generated
	 */
	FeatureOfInterestPickingToolWizardPagesProvider createFeatureOfInterestPickingToolWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Abstract Two Points3 DTool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Two Points3 DTool Wizard Pages Provider</em>'.
	 * @generated
	 */
	AbstractTwoPoints3DToolWizardPagesProvider createAbstractTwoPoints3DToolWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Ruler3 DTool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ruler3 DTool Wizard Pages Provider</em>'.
	 * @generated
	 */
	Ruler3DToolWizardPagesProvider createRuler3DToolWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Trajectory3 DTool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trajectory3 DTool Wizard Pages Provider</em>'.
	 * @generated
	 */
	Trajectory3DToolWizardPagesProvider createTrajectory3DToolWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * @generated
	 */
	TrajectoryPickingToolWizardPagesProvider createTrajectoryPickingToolWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Abstract Pick Location Tooll Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Pick Location Tooll Wizard Pages Provider</em>'.
	 * @generated
	 */
	AbstractPickLocationToollWizardPagesProvider createAbstractPickLocationToollWizardPagesProvider();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyAddonsUIPackage getApogyAddonsUIPackage();

} //ApogyAddonsUIFactory

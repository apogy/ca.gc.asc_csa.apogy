package ca.gc.asc_csa.apogy.addons.ui.impl;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.addons.ui.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings;
import ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.AbstractTwoPoints3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIFactory;
import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage;
import ca.gc.asc_csa.apogy.addons.ui.FeatureOfInterestPickingToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Ruler3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Ruler3dToolNodePresentation;
import ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Trajectory3DToolNodePresentation;
import ca.gc.asc_csa.apogy.addons.ui.Trajectory3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.TrajectoryPickingToolWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyAddonsUIFactoryImpl extends EFactoryImpl implements ApogyAddonsUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyAddonsUIFactory init() {
		try {
			ApogyAddonsUIFactory theApogyAddonsUIFactory = (ApogyAddonsUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyAddonsUIPackage.eNS_URI);
			if (theApogyAddonsUIFactory != null) {
				return theApogyAddonsUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyAddonsUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyAddonsUIPackage.RULER3D_TOOL_NODE_PRESENTATION: return createRuler3dToolNodePresentation();
			case ApogyAddonsUIPackage.TRAJECTORY3_DTOOL_NODE_PRESENTATION: return createTrajectory3DToolNodePresentation();
			case ApogyAddonsUIPackage.ABSTRACT_TOOL_ECLASS_SETTINGS: return createAbstractToolEClassSettings();
			case ApogyAddonsUIPackage.ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER: return createAbstractToolWizardPagesProvider();
			case ApogyAddonsUIPackage.SIMPLE_TOOL_WIZARD_PAGES_PROVIDER: return createSimpleToolWizardPagesProvider();
			case ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER: return createSimple3DToolWizardPagesProvider();
			case ApogyAddonsUIPackage.FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER: return createFeatureOfInterestPickingToolWizardPagesProvider();
			case ApogyAddonsUIPackage.ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER: return createAbstractTwoPoints3DToolWizardPagesProvider();
			case ApogyAddonsUIPackage.RULER3_DTOOL_WIZARD_PAGES_PROVIDER: return createRuler3DToolWizardPagesProvider();
			case ApogyAddonsUIPackage.TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER: return createTrajectory3DToolWizardPagesProvider();
			case ApogyAddonsUIPackage.TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER: return createTrajectoryPickingToolWizardPagesProvider();
			case ApogyAddonsUIPackage.ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER: return createAbstractPickLocationToollWizardPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ruler3dToolNodePresentation createRuler3dToolNodePresentation() {
		Ruler3dToolNodePresentationImpl ruler3dToolNodePresentation = new Ruler3dToolNodePresentationImpl();
		return ruler3dToolNodePresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Trajectory3DToolNodePresentation createTrajectory3DToolNodePresentation() {
		Trajectory3DToolNodePresentationImpl trajectory3DToolNodePresentation = new Trajectory3DToolNodePresentationImpl();
		return trajectory3DToolNodePresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractToolEClassSettings createAbstractToolEClassSettings() {
		AbstractToolEClassSettingsImpl abstractToolEClassSettings = new AbstractToolEClassSettingsImpl();
		return abstractToolEClassSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractToolWizardPagesProvider createAbstractToolWizardPagesProvider() {
		AbstractToolWizardPagesProviderImpl abstractToolWizardPagesProvider = new AbstractToolWizardPagesProviderImpl();
		return abstractToolWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleToolWizardPagesProvider createSimpleToolWizardPagesProvider() {
		SimpleToolWizardPagesProviderImpl simpleToolWizardPagesProvider = new SimpleToolWizardPagesProviderImpl();
		return simpleToolWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simple3DToolWizardPagesProvider createSimple3DToolWizardPagesProvider() {
		Simple3DToolWizardPagesProviderImpl simple3DToolWizardPagesProvider = new Simple3DToolWizardPagesProviderImpl();
		return simple3DToolWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureOfInterestPickingToolWizardPagesProvider createFeatureOfInterestPickingToolWizardPagesProvider() {
		FeatureOfInterestPickingToolWizardPagesProviderImpl featureOfInterestPickingToolWizardPagesProvider = new FeatureOfInterestPickingToolWizardPagesProviderImpl();
		return featureOfInterestPickingToolWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractTwoPoints3DToolWizardPagesProvider createAbstractTwoPoints3DToolWizardPagesProvider() {
		AbstractTwoPoints3DToolWizardPagesProviderImpl abstractTwoPoints3DToolWizardPagesProvider = new AbstractTwoPoints3DToolWizardPagesProviderImpl();
		return abstractTwoPoints3DToolWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ruler3DToolWizardPagesProvider createRuler3DToolWizardPagesProvider() {
		Ruler3DToolWizardPagesProviderImpl ruler3DToolWizardPagesProvider = new Ruler3DToolWizardPagesProviderImpl();
		return ruler3DToolWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Trajectory3DToolWizardPagesProvider createTrajectory3DToolWizardPagesProvider() {
		Trajectory3DToolWizardPagesProviderImpl trajectory3DToolWizardPagesProvider = new Trajectory3DToolWizardPagesProviderImpl();
		return trajectory3DToolWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrajectoryPickingToolWizardPagesProvider createTrajectoryPickingToolWizardPagesProvider() {
		TrajectoryPickingToolWizardPagesProviderImpl trajectoryPickingToolWizardPagesProvider = new TrajectoryPickingToolWizardPagesProviderImpl();
		return trajectoryPickingToolWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractPickLocationToollWizardPagesProvider createAbstractPickLocationToollWizardPagesProvider() {
		AbstractPickLocationToollWizardPagesProviderImpl abstractPickLocationToollWizardPagesProvider = new AbstractPickLocationToollWizardPagesProviderImpl();
		return abstractPickLocationToollWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsUIPackage getApogyAddonsUIPackage() {
		return (ApogyAddonsUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyAddonsUIPackage getPackage() {
		return ApogyAddonsUIPackage.eINSTANCE;
	}

} //ApogyAddonsUIFactoryImpl

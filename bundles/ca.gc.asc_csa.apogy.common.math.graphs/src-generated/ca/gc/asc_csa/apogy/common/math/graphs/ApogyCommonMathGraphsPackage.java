/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *      Sushanth Sankaran
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.common.math.graphs;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.common.math.graphs.ApogyCommonMathGraphsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyCommonMathGraphs' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Sushanth Sankaran\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' modelName='ApogyCommonMathGraphs' complianceLevel='6.0' suppressGenModelAnnotations='false' dynamicTemplates='true' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' testSuiteClass='ca.gc.asc_csa.apogy.common.math.graphs.tests.AllTests' modelDirectory='/ca.gc.asc_csa.apogy.common.math.graphs/src-generated' editDirectory='/ca.gc.asc_csa.apogy.common.math.graphs.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.common.math'"
 * @generated
 */
public interface ApogyCommonMathGraphsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "graphs";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.common.math.graphs";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "graphs";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCommonMathGraphsPackage eINSTANCE = ca.gc.asc_csa.apogy.common.math.graphs.impl.ApogyCommonMathGraphsPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.math.graphs.impl.KDTreeImpl <em>KD Tree</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.impl.KDTreeImpl
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.impl.ApogyCommonMathGraphsPackageImpl#getKDTree()
	 * @generated
	 */
	int KD_TREE = 0;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KD_TREE__DIMENSION = 0;

	/**
	 * The number of structural features of the '<em>KD Tree</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KD_TREE_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Add Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KD_TREE___ADD_NODE__DOUBLE_OBJECT = 0;

	/**
	 * The operation id for the '<em>Initialize Tree</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KD_TREE___INITIALIZE_TREE__LIST_LIST = 1;

	/**
	 * The operation id for the '<em>Search</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KD_TREE___SEARCH__DOUBLE = 2;

	/**
	 * The operation id for the '<em>Remove Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KD_TREE___REMOVE_NODE__DOUBLE = 3;

	/**
	 * The operation id for the '<em>Find Nearest</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KD_TREE___FIND_NEAREST__DOUBLE = 4;

	/**
	 * The operation id for the '<em>Find Nearest</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KD_TREE___FIND_NEAREST__DOUBLE_INT = 5;

	/**
	 * The operation id for the '<em>Perform Range Search</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KD_TREE___PERFORM_RANGE_SEARCH__DOUBLE_DOUBLE = 6;

	/**
	 * The number of operations of the '<em>KD Tree</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KD_TREE_OPERATION_COUNT = 7;

	/**
	 * The meta object id for the '<em>Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Exception
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.impl.ApogyCommonMathGraphsPackageImpl#getException()
	 * @generated
	 */
	int EXCEPTION = 1;


	/**
	 * The meta object id for the '<em>List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.impl.ApogyCommonMathGraphsPackageImpl#getList()
	 * @generated
	 */
	int LIST = 2;


	/**
	 * The meta object id for the '<em>Double Array</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.impl.ApogyCommonMathGraphsPackageImpl#getDoubleArray()
	 * @generated
	 */
	int DOUBLE_ARRAY = 3;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree <em>KD Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>KD Tree</em>'.
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree
	 * @generated
	 */
	EClass getKDTree();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#getDimension <em>Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dimension</em>'.
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#getDimension()
	 * @see #getKDTree()
	 * @generated
	 */
	EAttribute getKDTree_Dimension();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#addNode(double[], java.lang.Object) <em>Add Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Node</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#addNode(double[], java.lang.Object)
	 * @generated
	 */
	EOperation getKDTree__AddNode__double_Object();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#initializeTree(java.util.List, java.util.List) <em>Initialize Tree</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initialize Tree</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#initializeTree(java.util.List, java.util.List)
	 * @generated
	 */
	EOperation getKDTree__InitializeTree__List_List();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#search(double[]) <em>Search</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Search</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#search(double[])
	 * @generated
	 */
	EOperation getKDTree__Search__double();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#removeNode(double[]) <em>Remove Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Node</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#removeNode(double[])
	 * @generated
	 */
	EOperation getKDTree__RemoveNode__double();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#findNearest(double[]) <em>Find Nearest</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Find Nearest</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#findNearest(double[])
	 * @generated
	 */
	EOperation getKDTree__FindNearest__double();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#findNearest(double[], int) <em>Find Nearest</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Find Nearest</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#findNearest(double[], int)
	 * @generated
	 */
	EOperation getKDTree__FindNearest__double_int();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#performRangeSearch(double[], double[]) <em>Perform Range Search</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Perform Range Search</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#performRangeSearch(double[], double[])
	 * @generated
	 */
	EOperation getKDTree__PerformRangeSearch__double_double();

	/**
	 * Returns the meta object for data type '{@link java.lang.Exception <em>Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Exception</em>'.
	 * @see java.lang.Exception
	 * @model instanceClass="java.lang.Exception"
	 * @generated
	 */
	EDataType getException();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>List</em>'.
	 * @see java.util.List
	 * @model instanceClass="java.util.List" typeParameters="T"
	 * @generated
	 */
	EDataType getList();

	/**
	 * Returns the meta object for data type '<em>Double Array</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Double Array</em>'.
	 * @model instanceClass="double[]"
	 * @generated
	 */
	EDataType getDoubleArray();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCommonMathGraphsFactory getApogyCommonMathGraphsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.math.graphs.impl.KDTreeImpl <em>KD Tree</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.math.graphs.impl.KDTreeImpl
		 * @see ca.gc.asc_csa.apogy.common.math.graphs.impl.ApogyCommonMathGraphsPackageImpl#getKDTree()
		 * @generated
		 */
		EClass KD_TREE = eINSTANCE.getKDTree();

		/**
		 * The meta object literal for the '<em><b>Dimension</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KD_TREE__DIMENSION = eINSTANCE.getKDTree_Dimension();

		/**
		 * The meta object literal for the '<em><b>Add Node</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation KD_TREE___ADD_NODE__DOUBLE_OBJECT = eINSTANCE.getKDTree__AddNode__double_Object();

		/**
		 * The meta object literal for the '<em><b>Initialize Tree</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation KD_TREE___INITIALIZE_TREE__LIST_LIST = eINSTANCE.getKDTree__InitializeTree__List_List();

		/**
		 * The meta object literal for the '<em><b>Search</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation KD_TREE___SEARCH__DOUBLE = eINSTANCE.getKDTree__Search__double();

		/**
		 * The meta object literal for the '<em><b>Remove Node</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation KD_TREE___REMOVE_NODE__DOUBLE = eINSTANCE.getKDTree__RemoveNode__double();

		/**
		 * The meta object literal for the '<em><b>Find Nearest</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation KD_TREE___FIND_NEAREST__DOUBLE = eINSTANCE.getKDTree__FindNearest__double();

		/**
		 * The meta object literal for the '<em><b>Find Nearest</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation KD_TREE___FIND_NEAREST__DOUBLE_INT = eINSTANCE.getKDTree__FindNearest__double_int();

		/**
		 * The meta object literal for the '<em><b>Perform Range Search</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation KD_TREE___PERFORM_RANGE_SEARCH__DOUBLE_DOUBLE = eINSTANCE.getKDTree__PerformRangeSearch__double_double();

		/**
		 * The meta object literal for the '<em>Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Exception
		 * @see ca.gc.asc_csa.apogy.common.math.graphs.impl.ApogyCommonMathGraphsPackageImpl#getException()
		 * @generated
		 */
		EDataType EXCEPTION = eINSTANCE.getException();

		/**
		 * The meta object literal for the '<em>List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see ca.gc.asc_csa.apogy.common.math.graphs.impl.ApogyCommonMathGraphsPackageImpl#getList()
		 * @generated
		 */
		EDataType LIST = eINSTANCE.getList();

		/**
		 * The meta object literal for the '<em>Double Array</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.math.graphs.impl.ApogyCommonMathGraphsPackageImpl#getDoubleArray()
		 * @generated
		 */
		EDataType DOUBLE_ARRAY = eINSTANCE.getDoubleArray();

	}

} //ApogyCommonMathGraphsPackage

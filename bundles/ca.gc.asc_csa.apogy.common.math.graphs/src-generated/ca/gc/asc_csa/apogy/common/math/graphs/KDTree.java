/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *      Sushanth Sankaran
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.common.math.graphs;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>KD Tree</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A KD Tree.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#getDimension <em>Dimension</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.math.graphs.ApogyCommonMathGraphsPackage#getKDTree()
 * @model
 * @generated
 */
public interface KDTree extends EObject {
	/**
	 * Returns the value of the '<em><b>Dimension</b></em>' attribute.
	 * The default value is <code>"3"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The number of dimension of the tree. Should be equal or greater than one.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Dimension</em>' attribute.
	 * @see #setDimension(int)
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.ApogyCommonMathGraphsPackage#getKDTree_Dimension()
	 * @model default="3" unique="false"
	 * @generated
	 */
	int getDimension();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#getDimension <em>Dimension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dimension</em>' attribute.
	 * @see #getDimension()
	 * @generated
	 */
	void setDimension(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Adds the node to the existing tree with the specified key. This could make the tree become un-balanced.
	 * @param key The key of the node.
	 * @param values The objects at the specified key.
	 * @return True if the node was added, false if the specified key already exists.
	 * @throw An exception if the key is improperly sized.
	 * <!-- end-model-doc -->
	 * @model unique="false" exceptions="ca.gc.asc_csa.apogy.common.math.graphs.Exception" keyDataType="ca.gc.asc_csa.apogy.common.math.graphs.DoubleArray" keyUnique="false" valueUnique="false"
	 * @generated
	 */
	boolean addNode(double[] key, Object value) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Initialize the tree with a list of keys and values. Existing tree will be wiped. The tree will be balanced.
	 * @param key The keys of the nodes.
	 * @param values The list of objects at the specified keys.
	 * @throw An exception if the keys is improperly sized or the number of keys does not match the number of values.
	 * <!-- end-model-doc -->
	 * @model exceptions="ca.gc.asc_csa.apogy.common.math.graphs.Exception" keyDataType="ca.gc.asc_csa.apogy.common.math.graphs.List<ca.gc.asc_csa.apogy.common.math.graphs.DoubleArray>" keyUnique="false" keyMany="false" valuesDataType="ca.gc.asc_csa.apogy.common.math.graphs.List<org.eclipse.emf.ecore.EJavaObject>" valuesUnique="false" valuesMany="false"
	 * @generated
	 */
	void initializeTree(List<double[]> key, List<Object> values) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the node with a key identical to the specified key.
	 * @param key The key of the node.
	 * @return The Object at the specified key, null if none is found.
	 * @throw An exception if the key is improperly sized.
	 * <!-- end-model-doc -->
	 * @model unique="false" exceptions="ca.gc.asc_csa.apogy.common.math.graphs.Exception" keyDataType="ca.gc.asc_csa.apogy.common.math.graphs.DoubleArray" keyUnique="false"
	 * @generated
	 */
	Object search(double[] key) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Remove the node with the specified key.
	 * @param key The key of the node to remove.
	 * @return True if the node was removed, false if no node with the specified key was found.
	 * @throw An exception if the key is improperly sized.
	 * <!-- end-model-doc -->
	 * @model unique="false" exceptions="ca.gc.asc_csa.apogy.common.math.graphs.Exception" keyDataType="ca.gc.asc_csa.apogy.common.math.graphs.DoubleArray" keyUnique="false"
	 * @generated
	 */
	boolean removeNode(double[] key) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Find the nearest node to the specified key.
	 * @param key The specified key.
	 * @return The Object closest to the specified key.
	 * @throw An exception if the key is improperly sized.
	 * <!-- end-model-doc -->
	 * @model unique="false" exceptions="ca.gc.asc_csa.apogy.common.math.graphs.Exception" keyDataType="ca.gc.asc_csa.apogy.common.math.graphs.DoubleArray" keyUnique="false"
	 * @generated
	 */
	Object findNearest(double[] key) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Find the nearest nodes to the specified key.
	 * @param key The specified key.
	 * @param numberOfNodes The number of neighbours to find.
	 * @return The list of neighbours.
	 * @throw An exception if the key is improperly sized.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.math.graphs.List<org.eclipse.emf.ecore.EJavaObject>" unique="false" many="false" exceptions="ca.gc.asc_csa.apogy.common.math.graphs.Exception" keyDataType="ca.gc.asc_csa.apogy.common.math.graphs.DoubleArray" keyUnique="false" numberOfNodesUnique="false"
	 * @generated
	 */
	List<Object> findNearest(double[] key, int numberOfNodes) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Performs an range search.
	 * @param lowerKey The key specifying the lower range.
	 * @param upperKey The key specifying the upper range.
	 * @return The list of Object found in the specified range.
	 * @throw An exception if the key is improperly sized.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.math.graphs.List<org.eclipse.emf.ecore.EJavaObject>" unique="false" many="false" exceptions="ca.gc.asc_csa.apogy.common.math.graphs.Exception" lowerKeyDataType="ca.gc.asc_csa.apogy.common.math.graphs.DoubleArray" lowerKeyUnique="false" upperKeyDataType="ca.gc.asc_csa.apogy.common.math.graphs.DoubleArray" upperKeyUnique="false"
	 * @generated
	 */
	List<Object> performRangeSearch(double[] lowerKey, double[] upperKey) throws Exception;

} // KDTree

/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *      Sushanth Sankaran
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.common.math.graphs.impl;

import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ca.gc.asc_csa.apogy.common.math.graphs.ApogyCommonMathGraphsFactory;
import ca.gc.asc_csa.apogy.common.math.graphs.ApogyCommonMathGraphsPackage;
import ca.gc.asc_csa.apogy.common.math.graphs.KDTree;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCommonMathGraphsPackageImpl extends EPackageImpl implements ApogyCommonMathGraphsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass kdTreeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType exceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType listEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType doubleArrayEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.ApogyCommonMathGraphsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyCommonMathGraphsPackageImpl() {
		super(eNS_URI, ApogyCommonMathGraphsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCommonMathGraphsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyCommonMathGraphsPackage init() {
		if (isInited) return (ApogyCommonMathGraphsPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonMathGraphsPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCommonMathGraphsPackageImpl theApogyCommonMathGraphsPackage = (ApogyCommonMathGraphsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCommonMathGraphsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCommonMathGraphsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCommonMathGraphsPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCommonMathGraphsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCommonMathGraphsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCommonMathGraphsPackage.eNS_URI, theApogyCommonMathGraphsPackage);
		return theApogyCommonMathGraphsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKDTree() {
		return kdTreeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getKDTree_Dimension() {
		return (EAttribute)kdTreeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getKDTree__AddNode__double_Object() {
		return kdTreeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getKDTree__InitializeTree__List_List() {
		return kdTreeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getKDTree__Search__double() {
		return kdTreeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getKDTree__RemoveNode__double() {
		return kdTreeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getKDTree__FindNearest__double() {
		return kdTreeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getKDTree__FindNearest__double_int() {
		return kdTreeEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getKDTree__PerformRangeSearch__double_double() {
		return kdTreeEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getException() {
		return exceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getList() {
		return listEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDoubleArray() {
		return doubleArrayEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonMathGraphsFactory getApogyCommonMathGraphsFactory() {
		return (ApogyCommonMathGraphsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		kdTreeEClass = createEClass(KD_TREE);
		createEAttribute(kdTreeEClass, KD_TREE__DIMENSION);
		createEOperation(kdTreeEClass, KD_TREE___ADD_NODE__DOUBLE_OBJECT);
		createEOperation(kdTreeEClass, KD_TREE___INITIALIZE_TREE__LIST_LIST);
		createEOperation(kdTreeEClass, KD_TREE___SEARCH__DOUBLE);
		createEOperation(kdTreeEClass, KD_TREE___REMOVE_NODE__DOUBLE);
		createEOperation(kdTreeEClass, KD_TREE___FIND_NEAREST__DOUBLE);
		createEOperation(kdTreeEClass, KD_TREE___FIND_NEAREST__DOUBLE_INT);
		createEOperation(kdTreeEClass, KD_TREE___PERFORM_RANGE_SEARCH__DOUBLE_DOUBLE);

		// Create data types
		exceptionEDataType = createEDataType(EXCEPTION);
		listEDataType = createEDataType(LIST);
		doubleArrayEDataType = createEDataType(DOUBLE_ARRAY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters
		addETypeParameter(listEDataType, "T");

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(kdTreeEClass, KDTree.class, "KDTree", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getKDTree_Dimension(), theEcorePackage.getEInt(), "dimension", "3", 0, 1, KDTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getKDTree__AddNode__double_Object(), theEcorePackage.getEBoolean(), "addNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDoubleArray(), "key", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getException());

		op = initEOperation(getKDTree__InitializeTree__List_List(), null, "initializeTree", 0, 1, !IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(this.getList());
		EGenericType g2 = createEGenericType(this.getDoubleArray());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "key", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "values", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getException());

		op = initEOperation(getKDTree__Search__double(), theEcorePackage.getEJavaObject(), "search", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDoubleArray(), "key", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getException());

		op = initEOperation(getKDTree__RemoveNode__double(), theEcorePackage.getEBoolean(), "removeNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDoubleArray(), "key", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getException());

		op = initEOperation(getKDTree__FindNearest__double(), theEcorePackage.getEJavaObject(), "findNearest", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDoubleArray(), "key", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getException());

		op = initEOperation(getKDTree__FindNearest__double_int(), null, "findNearest", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDoubleArray(), "key", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "numberOfNodes", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getException());
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getKDTree__PerformRangeSearch__double_double(), null, "performRangeSearch", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDoubleArray(), "lowerKey", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDoubleArray(), "upperKey", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getException());
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		// Initialize data types
		initEDataType(exceptionEDataType, Exception.class, "Exception", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(listEDataType, List.class, "List", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(doubleArrayEDataType, double[].class, "DoubleArray", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "prefix", "ApogyCommonMathGraphs",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Sushanth Sankaran\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyCommonMathGraphs",
			 "complianceLevel", "6.0",
			 "suppressGenModelAnnotations", "false",
			 "dynamicTemplates", "true",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "testSuiteClass", "ca.gc.asc_csa.apogy.common.math.graphs.tests.AllTests",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.common.math.graphs/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.common.math.graphs.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.common.math"
		   });	
		addAnnotation
		  (kdTreeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA KD Tree."
		   });	
		addAnnotation
		  (getKDTree__AddNode__double_Object(), 
		   source, 
		   new String[] {
			 "documentation", "*\nAdds the node to the existing tree with the specified key. This could make the tree become un-balanced.\n@param key The key of the node.\n@param values The objects at the specified key.\n@return True if the node was added, false if the specified key already exists.\n@throw An exception if the key is improperly sized."
		   });	
		addAnnotation
		  (getKDTree__InitializeTree__List_List(), 
		   source, 
		   new String[] {
			 "documentation", "*\nInitialize the tree with a list of keys and values. Existing tree will be wiped. The tree will be balanced.\n@param key The keys of the nodes.\n@param values The list of objects at the specified keys.\n@throw An exception if the keys is improperly sized or the number of keys does not match the number of values."
		   });	
		addAnnotation
		  (getKDTree__Search__double(), 
		   source, 
		   new String[] {
			 "documentation", "*\nGets the node with a key identical to the specified key.\n@param key The key of the node.\n@return The Object at the specified key, null if none is found.\n@throw An exception if the key is improperly sized."
		   });	
		addAnnotation
		  (getKDTree__RemoveNode__double(), 
		   source, 
		   new String[] {
			 "documentation", "*\nRemove the node with the specified key.\n@param key The key of the node to remove.\n@return True if the node was removed, false if no node with the specified key was found.\n@throw An exception if the key is improperly sized."
		   });	
		addAnnotation
		  (getKDTree__FindNearest__double(), 
		   source, 
		   new String[] {
			 "documentation", "*\nFind the nearest node to the specified key.\n@param key The specified key.\n@return The Object closest to the specified key.\n@throw An exception if the key is improperly sized."
		   });	
		addAnnotation
		  (getKDTree__FindNearest__double_int(), 
		   source, 
		   new String[] {
			 "documentation", "*\nFind the nearest nodes to the specified key.\n@param key The specified key.\n@param numberOfNodes The number of neighbours to find.\n@return The list of neighbours.\n@throw An exception if the key is improperly sized."
		   });	
		addAnnotation
		  (getKDTree__PerformRangeSearch__double_double(), 
		   source, 
		   new String[] {
			 "documentation", "*\nPerforms an range search.\n@param lowerKey The key specifying the lower range.\n@param upperKey The key specifying the upper range.\n@return The list of Object found in the specified range.\n@throw An exception if the key is improperly sized."
		   });	
		addAnnotation
		  (getKDTree_Dimension(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe number of dimension of the tree. Should be equal or greater than one."
		   });
	}

} //ApogyCommonMathGraphsPackageImpl

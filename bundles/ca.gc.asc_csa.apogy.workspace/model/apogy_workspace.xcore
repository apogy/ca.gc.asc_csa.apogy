// *****************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
//     Regent L'Archeveque - initial API and implementation
//        
// SPDX-License-Identifier: EPL-1.0
// *****************************************************************************
@GenModel(prefix="ApogyWorkspace",
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Regent L'Archeveque - initial API and implementation
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",
		  modelName="ApogyWorkspace")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.workspace/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.workspace.edit/src-generated")

package ca.gc.asc_csa.apogy.workspace

import ca.gc.asc_csa.apogy.core.Exception
import java.util.List
import org.eclipse.core.resources.IProject
import org.osgi.framework.Bundle

/* Types definitions. */
type IProject wraps IProject
type ListApogyProjects wraps List<IProject>
type ListApogyProjectBundles wraps List<Bundle>
type Bundle wraps Bundle


/* -------------------------------------------------------------------------
 * 
 * Apogy Workspace Facade.
 * 
 * ------------------------------------------------------------------------- */ 
class ApogyWorkspaceFacade{
	/*
	 * Default Apogy Project Name Prefix.
	 */
	readonly transient String defaultProjectNamePrefix = "Project"

	/*
	 * Default Apogy Session Filename Prefix.
	 */
	readonly transient String defaultSessionFilename = "session"
	
	/*
	 * Default Apogy Session Filename Extension Prefix.
	 */
	readonly transient String defaultSessionFilenameExtension = "sym"
	
	/*
	 * Default Apogy Sessions Directory.
	 */
	readonly transient String defaultSessionsFolderName = "sessions"

	/*
	 * Default Apogy Programs Directory.
	 */
	readonly transient String defaultProgramsFolderName = "scripts"

	/*
	 * Default Apogy Data Directory.
	 */
	readonly transient String defaultDataFolderName = "data"
	
	/*
	 * Reference to the deleted workspace project.
	 */
    @GenModel(property="None")
	IProject deletedWorskpaceProject

	/*
	 * Reference to the new workspace project.
	 */	
	@GenModel(property="None")
	IProject newWorkspaceProject

	/*
	 * Returns an available project name according to the current workspace.
	 */
	op String getDefaultProjectName()	
	
	/*
	 * Creates and initialized a new Apogy project template without the session.
	 * @param name Name of the project.
	 * @param description Description of the project.
	 * @return Reference to the project or null if not created.
	 * @throws Exception Reference to the exception stack.
	 */
	op IProject createApogyProjectTemplate(String name, String description)	throws Exception	
	
	/*
	 * Creates and initialized a new Apogy project with a session file.
	 * @param name Name of the project.
	 * @param description Description of the project.
	 * @return Reference to the project or null if not created.
	 * @throws Exception Reference to the exception stack.
	 */
	op IProject createApogyProject(String name, String description)	throws Exception
	
		
	/*
	 * Returns the list of projects with the Apogy nature in the active workspace.
	 * @return List of Apogy projects stored in the workspace.
	 */
	op ListApogyProjects getWorkspaceApogyProjects()
		
	/*
	 * Searches the projects for a project with a specific name in the active workspace.
	 * @param name Name of the project.
	 * @return Reference to the project or null if there is not project with the specified name.
	 */
	op IProject getWorkspaceApogyProject(String name)
		
	/*
	 * Opens the specified Apogy Project.
	 * @param project Reference to the project to open.
	 * @throws Exception Reference to the exception.
	 */
	op void openApogyProject(IProject project) throws Exception
	
	/*
	 * Save the active Apogy Project.
	 * @throws Exception Reference to the exception.
	 */
	op void saveActiveApogyProject() throws Exception	

	/*
	 * Deletes the specified Apogy Project.
	 * @param project Reference to the project to open.
	 * @throws Exception Reference to the exception.
	 */
	op void deleteApogyProject(IProject project) throws Exception

	/*
	 * Imports the specified Apogy Project Bundle.
	 * @param project Reference to the bundle that contains the Apogy project.
	 * @param projectName Name of the project to create.
	 */
	op void importApogyProject(Bundle bundle, String projectName) throws Exception	

	/*
	 * Imports the specified Apogy Project Bundle.
	 * @param sessionFilePath File path to the session file to import.
	 * @param projectName Name of the project to create.
	 */
	op void importApogySession(String sessionFilePath, String projectName) throws Exception	

	/*
	 * Closes the active Apogy Project.
	 * @param project Reference to the project to close.
	 * @throws Exception Reference to the exception. 
	 */
	op void closeActiveApogyProject() throws Exception

	/*
	 * Returns if the project exists or not. 
	 * @param name Reference to the project name.
	 * @return Returns true if the project exists. 
	 */
	op boolean isProjectExists(String name)
		
	/*
	 * Indicates which project is opened.
	 */
	transient IProject activeProject
}

/*
 * This registry holds all the bundles that contain Apogy projects.  This class is a singleton.
 */
class ProjectProvidersRegistry{	
	/*
	 * Eclipse Extension Point Id.
	 */
	readonly transient String PROJECT_PROVIDERS_CONTRIBUTORS_POINT_ID = "ca.gc.asc_csa.apogy.workspace.projectProvider"
	
	/*
	 * Contains all the bundles that provides an Apogy project.
	 */
	ListApogyProjectBundles apogyProjectProviders
}
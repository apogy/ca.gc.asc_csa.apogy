package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.jme3.utils;

import java.awt.image.BufferedImage;

public class SurfaceImage 
{
	public double minLatitude;
	public double maxLatitude;
	public double minLongitude;
	public double maxLongitude;
	public BufferedImage image;
}

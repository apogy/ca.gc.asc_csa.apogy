/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.jme3.scene_objects;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.vecmath.Matrix4d;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.swt.graphics.RGB;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;

import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects.DefaultJME3SceneObject;
import ca.gc.asc_csa.apogy.core.environment.Worksite;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.jme3.Activator;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.jme3.utils.AtmosphereJME3Utilities;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.scene_objects.EarthAtmosphereWorksiteSceneObject;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksiteNode;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.jme3.EarthSurfaceEnvironmentJMEConstants;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.jme3.EnvironmentUIJME3Utilities;


public class EarthAtmosphereWorksiteNodeJME3Object extends DefaultJME3SceneObject<EarthAtmosphereWorksiteNode> implements EarthAtmosphereWorksiteSceneObject
{
	public static long IMAGE_RETRY_WAIT_TIME_MS = 1*60*1000;
	public static float CUTTOFF_HEIGHT_METERS = 250.0f;
	private static ColorRGBA HORIZON_COLOR = new ColorRGBA(0f, 1f, 0f, 1.0f);
	private AWTLoader awtLoader = new AWTLoader();
	
	private Adapter adapter;
	
	private boolean axisVisible = false;
	private float axisLength = 1.0f;
	
	private boolean planeVisible = true;
	private float gridSize = 10.0f;
	private float planeSize = 100.0f;	
	
	private boolean azimuthVisible = true;
	private boolean azimuthLinesVisible = true;
	private boolean elevationLinesVisible = true;
	
	private float altitude = 0.0f;

	
	private AssetManager assetManager;	
	
	private Geometry horizonGeometry = null;
	private Geometry gridGeometry = null;
	private Geometry axisGeometry = null;	
	
	private Node skyNode = null;
	private Node azimuthDisplayNode = null;
	private Node azimuthDisplayCirclesNode = null;
	private Node elevationDisplayCirclesNode = null;
	private Node horizon;
	
	private long lastImageTryTime = -1;
	
	public EarthAtmosphereWorksiteNodeJME3Object(EarthAtmosphereWorksiteNode node,JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{
		super(node, jme3RenderEngineDelegate);
		
		this.assetManager = jme3Application.getAssetManager();		
		
		// Creates geometry.
		requestUpdate();			
				
		// Listens for changes on the Worksite.
		node.eAdapters().add(getAdapter());
		if(node.getWorksite() instanceof EarthAtmosphereWorksite)
		{
			EarthAtmosphereWorksite earthAtmosphereWorksite = (EarthAtmosphereWorksite) node.getWorksite();
			earthAtmosphereWorksite.eAdapters().add(getAdapter());						
		}		
	}
	
	@Override
	public void updateGeometry(float tpf) 
	{
		if(horizon != null) getAttachmentNode().detachChild(horizon);		
		if(gridGeometry != null) getAttachmentNode().detachChild(gridGeometry);				
		if(axisGeometry != null) getAttachmentNode().detachChild(axisGeometry);								
		if(skyNode != null)  getAttachmentNode().detachChild(skyNode);

		// Adds the axis				
		axisGeometry = JME3Utilities.createAxis3D(axisLength, assetManager);
		
		if(axisVisible) getAttachmentNode().attachChild(axisGeometry);
		
		// Adds the grid				
		gridGeometry = createGridGeometry();
		if(planeVisible) getAttachmentNode().attachChild(gridGeometry);
															
		skyNode = createSkyNode();
		getAttachmentNode().attachChild(skyNode);
		
		horizon = createHorizon(altitude);
		getAttachmentNode().attachChild(horizon);
	}
	
	@Override
	public List<Geometry> getGeometries() 
	{		
		List<Geometry> geometries = new ArrayList<Geometry>();
		geometries.add(gridGeometry);		
		geometries.add(axisGeometry);
		geometries.add(horizonGeometry);
		return geometries;
	}
			
	@Override
	public void setAxisVisible(boolean newAxisVisible) 
	{		
		this.axisVisible = newAxisVisible;		
		
		Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Setting Axis visibility to <" + newAxisVisible + ">", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				if(axisGeometry != null) 
				{
					if(axisVisible)
					{
						getAttachmentNode().attachChild(axisGeometry);
					}
					else
					{
						getAttachmentNode().detachChild(axisGeometry);
					}	
				}	
				else
				{
					Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Failed to set Axis visibility to <" + newAxisVisible + ">", EventSeverity.ERROR);
				}
				
				return null;
			}
		});	
	}

	@Override
	public void setAxisLength(double newLength) 
	{		
		this.axisLength = (float) newLength;
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Setting Axis Length to <" + newLength + ">", EventSeverity.INFO);
				try
				{					
					if(axisGeometry != null)
					{
						getAttachmentNode().detachChild(axisGeometry);
					}
					
					// Updates axis geometry
					axisGeometry = JME3Utilities.createAxis3D(axisLength, assetManager);
					
					// If axis geometry is visible, attach the new geometry.
					if(axisVisible)
					{
						getAttachmentNode().attachChild(axisGeometry);
					}
				}
				catch(Throwable t)
				{
					Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Failed to setAxisLength(" + newLength + ")!", EventSeverity.ERROR, t);
				}	
				
				return null;
			}
		});						
	}

	@Override
	public void setPlaneVisible(boolean newPlaneVisible) {
		this.planeVisible  = newPlaneVisible;
		
		Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Setting Plane visibility to <" + newPlaneVisible + ">", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				if(gridGeometry != null) 
				{
					if(planeVisible)
					{
						getAttachmentNode().attachChild(gridGeometry);
					}
					else
					{
						getAttachmentNode().detachChild(gridGeometry);
					}	
				}
				
				return null;
			}
		});		
	}

	@Override
	public void setPlaneParameters(double newGridSize, double newPlaneSize) 
	{		
		Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Setting Plane grid size to <" + newGridSize + "> and grid size to <" + newPlaneSize + ">", EventSeverity.INFO);
		
		if(newGridSize > 0 && newPlaneSize > 0)
		{
			this.gridSize = (float) newGridSize;
			this.planeSize = (float) newPlaneSize;		
			
			jme3Application.enqueue(new Callable<Object>() 
			{
				@Override
				public Object call() throws Exception 
				{									
					// Detach previous geometry.
					if(gridGeometry != null)
					{
						getAttachmentNode().detachChild(gridGeometry);
					}
					
					gridGeometry = createGridGeometry();
					
					if(planeVisible)
					{
						getAttachmentNode().attachChild(gridGeometry);
					}
					else
					{
						getAttachmentNode().detachChild(gridGeometry);
					}			
					
					return null;
				}
			});	
		}
		else
		{
			Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Failed to set Plane grid size to <" + newGridSize + "> and grid size to <" + newPlaneSize + ">", EventSeverity.ERROR);

		}
	}
	
	@Override
	public void setAzimuthVisible(boolean newAzimuthVisible) 
	{
		this.azimuthVisible = newAzimuthVisible;			
		
		Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Setting Azimuth visibility to <" + newAzimuthVisible + ">", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{		
				if(skyNode != null) 
				{
					if(azimuthVisible)
					{
						skyNode.attachChild(azimuthDisplayNode);
					}
					else
					{
						skyNode.detachChild(azimuthDisplayNode);
					}
				}
				else
				{
					Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Failed to set Azimuth visibility to <" + newAzimuthVisible + ">", EventSeverity.ERROR);
				}
				
				return null;
			}	
		});	
		
	}

	@Override
	public void setElevationLinesVisible(boolean newElevationLinesVisible) 
	{		
		this.elevationLinesVisible = newElevationLinesVisible;		
		
		Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Setting Elevation Lines visibility to <" + newElevationLinesVisible + ">", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{		
				if(skyNode != null) 
				{
					if(elevationLinesVisible)
					{
						skyNode.attachChild(elevationDisplayCirclesNode);
					}
					else
					{
						skyNode.detachChild(elevationDisplayCirclesNode);
					}
				}
				else
				{
					Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Failed to set Elevation Lines visibility to <" + newElevationLinesVisible + ">", EventSeverity.ERROR);
				}
				return null;
			}	
		});	
	}

	@Override
	public void setAzimuthLinesVisible(boolean newAzimuthLinesVisible) 
	{
		this.azimuthLinesVisible = newAzimuthLinesVisible;
		
		Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Setting Azimuth Lines visibility to <" + newAzimuthLinesVisible + ">", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{		
				if(skyNode != null) 
				{
					if(azimuthLinesVisible)
					{
						skyNode.attachChild(azimuthDisplayCirclesNode);
					}
					else
					{
						skyNode.detachChild(azimuthDisplayCirclesNode);
					}
				}
				else
				{
					Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Failed to set Azimuth Lines visibility to <" + newAzimuthLinesVisible + ">", EventSeverity.ERROR);
				}
				return null;
			}	
		});		
		
	}
	
	private Node createSkyNode()
	{
		Node node =  new Node("Worksite Sky");			
									
		azimuthDisplayNode = EnvironmentUIJME3Utilities.createAzimuthDisplay(assetManager);
		node.attachChild(azimuthDisplayNode);
		
		// Azimuth Circle displays
		azimuthDisplayCirclesNode = EnvironmentUIJME3Utilities.createAzimuthCirclesDisplay(assetManager);
		node.attachChild(azimuthDisplayCirclesNode);
		
		// Elevation circle display
		elevationDisplayCirclesNode = EnvironmentUIJME3Utilities.createElevationCirclesDisplay(assetManager);
		node.attachChild(elevationDisplayCirclesNode);		
		
		Matrix4d m = new Matrix4d();
		m.setIdentity();										
		node.setLocalTransform(JME3Utilities.createTransform(m));
		
		return node;
	}
	
	private Geometry createGridGeometry()
	{
		Mesh gridMesh = EnvironmentUIJME3Utilities.createGrid(gridSize, planeSize);
		
		Geometry geometry = new Geometry("Grid", gridMesh);
		
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", EarthSurfaceEnvironmentJMEConstants.DEFAULT_GRID_COLOR.clone());
        geometry.setMaterial(mat);
		
		return geometry;
	}	
	
	/**
	 * Returns the Spherical Cap (half-sphere used to represent the horizon and hide the sky).
	 * @return The geometry.
	 */
	private Node createHorizon(float altitude)
	{				
		Node node = new Node("Worksite Earth Surface");		
					
		node.attachChild(createCurvedHorizonGeometry(altitude));
		
		return node;
	}

	private Node createCurvedHorizonGeometry(float altitude)
	{
		double gamma = AtmosphereJME3Utilities.getHorizonAngle(altitude);
	
		double a = CUTTOFF_HEIGHT_METERS;
		double b = a*Math.tan(Math.toRadians(90) - Math.abs(gamma));
				
//		System.out.println("gamma " + Math.toDegrees(gamma));
//		System.out.println("a " + a);
//		System.out.println("b " + b);
		
		List<Vector3f> profile = AtmosphereJME3Utilities.getHyperbola(a, b, EarthSurfaceEnvironmentJMEConstants.AZIMUTH_DISPLAY_RADIUS);
		Mesh mesh = AtmosphereJME3Utilities.getParabolaGeometry(profile);
				
		Geometry geometry = new Geometry("Horizon", mesh);							
		geometry.setMaterial(createHorizonMaterial());
		geometry.setShadowMode(ShadowMode.Off);
		
		geometry.getMaterial().getAdditionalRenderState().setWireframe(false);
		
		Node node = new Node("Curved Horizon");						
		node.attachChild(geometry);
		
		return node;
	}
	
	/**
	 * Creates the material used on the horizon geometry.
	 * @return The Material.
	 */
	private Material createHorizonMaterial()
	{		
		Material mat = new Material(assetManager,  "Common/MatDefs/Misc/Unshaded.j3md");			
		AbstractEImage textureImage = null;
		
		// If it failed last time.
		if(lastImageTryTime != -1)
		{
			// Check if it is time to try again.
			long now = System.currentTimeMillis();
			if((now - lastImageTryTime) > IMAGE_RETRY_WAIT_TIME_MS)
			{
				textureImage = getTextureImage();
			}			
		}
		else
		{
			textureImage = getTextureImage();
		}
				
		if(textureImage != null)
		{
			Texture2D texture = createTexture(textureImage);							
			mat.setTexture("ColorMap", texture);
		}
		else
		{
			mat.setColor("Color", HORIZON_COLOR.clone());
		}
		
		mat.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
						
		return mat;
	}

	private AbstractEImage getTextureImage()
	{				
		try
		{
			if(getTopologyNode() != null && getTopologyNode().getWorksite() instanceof EarthAtmosphereWorksite)
			{
				EarthAtmosphereWorksite earthAtmosphereWorksite = (EarthAtmosphereWorksite) getTopologyNode().getWorksite();
				if(earthAtmosphereWorksite.getGeographicalCoordinates() != null)
				{										
					AbstractEImage image = AtmosphereJME3Utilities.getImageFromWMS(this.getTopologyNode(), earthAtmosphereWorksite.getGeographicalCoordinates(), 50000);
					
					if(image != null)
					{
						lastImageTryTime = -1;
					}
					else
					{
						lastImageTryTime = System.currentTimeMillis();
					}
					
					return image;
				}
			}
		}
		catch (Exception e) 
		{
			lastImageTryTime = System.currentTimeMillis();
			e.printStackTrace();
		}		
				
		return null;
	}
	
	private Texture2D createTexture(AbstractEImage textureImage)
	{		
		int width = textureImage.getWidth();
		int height = textureImage.getHeight();
		
		RGB rgb = new RGB(255, 255, 255);
		AbstractEImage background = EImagesUtilities.INSTANCE.createUniformColorImage(width, height, rgb.red, rgb.green, rgb.blue, 255);				
		AbstractEImage newImage = EImagesUtilities.INSTANCE.applyOverlay(background, textureImage, false);		
		AbstractEImage rotatedImage = EImagesUtilities.INSTANCE.rotate(newImage, Math.PI /2, false);
		com.jme3.texture.Image img = awtLoader.load(rotatedImage.asBufferedImage(), true);						
		Texture2D texture = new Texture2D(img);							
		
		return texture;
	}
	
	private Adapter getAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{					
					if(msg.getNotifier() instanceof EarthSurfaceWorksiteNode)
					{
						// Worksite has changed.
						if(msg.getFeatureID(EarthSurfaceWorksiteNode.class) == ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE__WORKSITE_NODE)
						{
							// Unregister from old Worksite.
							if(msg.getOldValue() instanceof Worksite)
							{
								Worksite oldWorksite = (Worksite) msg.getOldValue();
								oldWorksite.eAdapters().remove(getAdapter());
							}
																					
							if(msg.getNewValue() instanceof Worksite)
							{
								Worksite newWorksite = (Worksite) msg.getNewValue();
								newWorksite.eAdapters().add(getAdapter());
								
								if(newWorksite instanceof EarthAtmosphereWorksite)
								{
									// EarthAtmosphereWorksite earthAtmosphereWorksite = (EarthAtmosphereWorksite) newWorksite;										
									requestUpdate();
								}
							}							
						}												
					}
					else if(msg.getNotifier() instanceof EarthAtmosphereWorksite)
					{
						if(msg.getFeatureID(EarthSurfaceWorksite.class) == ApogyEarthEnvironmentPackage.EARTH_WORKSITE__GEOGRAPHICAL_COORDINATES)
						{
							GeographicCoordinates coord = (GeographicCoordinates) msg.getNewValue();
							if(coord != null)
							{
								altitude = (float) coord.getElevation();
								requestUpdate();									
							}
						}
					}
				}
			};
		}
		
		return adapter;
	}



	
}

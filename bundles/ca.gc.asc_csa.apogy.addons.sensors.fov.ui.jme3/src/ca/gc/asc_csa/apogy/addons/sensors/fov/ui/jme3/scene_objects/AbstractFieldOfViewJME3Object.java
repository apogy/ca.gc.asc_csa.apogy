package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.jme3.scene_objects;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.util.concurrent.Callable;

import org.eclipse.swt.graphics.RGB;
import ca.gc.asc_csa.apogy.addons.sensors.fov.FieldOfView;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.jme3.Activator;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.jme3.utils.AbstractFieldOfViewImageProjectorControl;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.scene_objects.FieldOfViewSceneObject;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects.DefaultJME3SceneObject;

import com.jme3.math.ColorRGBA;

public abstract class AbstractFieldOfViewJME3Object<R extends FieldOfView> extends DefaultJME3SceneObject<R> implements FieldOfViewSceneObject
{
	protected MeshPresentationMode meshPresentationMode = MeshPresentationMode.WIREFRAME;
	protected float transparency = 0.0f;
	protected boolean showProjection = false;
	protected boolean fovVisible = false;
	protected boolean showOutlineOnly = false;
	protected ColorRGBA fovColor = new ColorRGBA(0f, 1f, 0.0f, 0.25f);
	protected RGB rgb = new  RGB(1, 1, 1);	
	protected RGB projectionColor = new  RGB(1, 1, 1);
	protected com.jme3.scene.Node fovNode;
	
	public AbstractFieldOfViewJME3Object(R topologyNode, JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{
		super(topologyNode, jme3RenderEngineDelegate);		
	}

	@Override
	public void setColor(RGB rgb) 
	{
		this.rgb = rgb;
		fovColor = JME3Utilities.convertToColorRGBA(rgb);
	}
	
	@Override
	public RGB getColor() 
	{	
		return rgb;
	}
			
	@Override
	public MeshPresentationMode getPresentationMode() 
	{		
		return meshPresentationMode;
	}
	
	@Override
	public float getTransparency() {	
		return transparency;
	}
	
	@Override
	public void setFOVVisible(boolean visible) 
	{		
		Logger.INSTANCE.log(Activator.ID, this, "Set FOV Visible <" + visible + ")", EventSeverity.OK);
		
		this.fovVisible = visible;
		
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{
				if (!fovVisible && getAttachmentNode().hasChild(getFovNode())) 
				{
					getAttachmentNode().detachChild(getFovNode());
				} 
				else if (fovVisible && !getAttachmentNode().hasChild(getFovNode())) 
				{
					getAttachmentNode().attachChild(getFovNode());
				}
				return null;
			}
		});	
	}
	
	@Override
	public boolean isFOVVisible() 
	{		
		return fovVisible;
	}
	
	@Override
	public boolean isShowProjection() 
	{		
		return showProjection;
	}
	
	@Override
	public boolean isShowOutlineOnly() 
	{		
		return showOutlineOnly;
	}
	
	@Override
	public void setProjectionColor(RGB color) 
	{	
		this.projectionColor = color;
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{
				getAbstractFieldOfViewImageProjectorControl().setProjectionColor(color);
				return null;
			}
		});					
	}
	
	@Override
	public RGB getProjectionColor() 
	{		
		return projectionColor;
	}
	
	public abstract AbstractFieldOfViewImageProjectorControl<R> getAbstractFieldOfViewImageProjectorControl();
	
	/**
	 * Return the n ode to which the FOV representation should be attached.
	 * @return The node.
	 */
	protected com.jme3.scene.Node getFovNode()
	{
		if (fovNode == null) 
		{
			String name = null;
			if(getTopologyNode() != null && getTopologyNode().getNodeId() != null)
			{
				name = getTopologyNode().getNodeId();
			}
			else
			{
				name = getClass().getSimpleName();
			}		
			name += "_fov";
			
			fovNode = new com.jme3.scene.Node(name);	
			
			// Attaches the FOV node to the attachment node. 
			getAttachmentNode().attachChild(fovNode);
		}
		return fovNode;
	}
}

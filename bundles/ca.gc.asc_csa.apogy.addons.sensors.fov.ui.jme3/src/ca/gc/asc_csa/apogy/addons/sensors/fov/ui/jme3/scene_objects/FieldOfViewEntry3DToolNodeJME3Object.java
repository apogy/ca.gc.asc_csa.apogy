package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.jme3.scene_objects;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.vecmath.Color3f;
import javax.vecmath.Vector3d;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.swt.graphics.RGB;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Cylinder;
import com.jme3.scene.shape.Dome;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.jme3.Activator;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects.DefaultJME3SceneObject;

public class FieldOfViewEntry3DToolNodeJME3Object extends DefaultJME3SceneObject<FieldOfViewEntry3DToolNode>
{									
	public static final float TIP_BASE_TO_VECTOR_BASE_RATIO =  2.0f;
	public static final float TIP_LENGHT_TO_BASE_RATIO =  0.2f / 0.050f;
	
	private FieldOfViewEntry3DTool tool = null;
	private Adapter adapter;	
		
	@SuppressWarnings("unused")
	private AssetManager assetManager;	
			
	private Node fromTransformNode;		
	private Node vectorBodyNode = null;
	private Geometry vectorBodyGeometry = null;		
	
	private Node tipTransformNode;
	private Node vectorTipNode = null;
	private Geometry vectorTipGeometry = null;
	
	private ColorRGBA vectorColor = ColorRGBA.Red;
	
	
	public FieldOfViewEntry3DToolNodeJME3Object(FieldOfViewEntry3DToolNode node, JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{
		super(node, jme3RenderEngineDelegate);
		this.tool = getTopologyNode().getFieldOfViewEntry3DTool();
		this.assetManager = jme3Application.getAssetManager();					
		this.vectorColor = JME3Utilities.convertToColorRGBA(convertToRGB(tool.getCurrentVectorColor()));
		
		// Call this on viewer thread.
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{
				createGeometry();	
				tool.eAdapters().add(FieldOfViewEntry3DToolNodeJME3Object.this.getAdapter());
				
				return null;
			}
		});						
	}
	
	@Override
	public void updateGeometry(float tpf) 
	{
		if(tool != null)
		{
			if(tool.getFromNode() != null)
			{						
				updateDirection();
				updateVectorBody();
				updateVectorTip();
				
				// Re-Attaches the 
				attachTransformNode();
			}
			else
			{
				detachTransformNode();
			}
		}			
	}
	
	@Override
	public List<Geometry> getGeometries() 
	{		
		List<Geometry> geometries = new ArrayList<Geometry>();		
		geometries.add(vectorBodyGeometry);
		return geometries;
	}
	
	@Override
	public void setVisible(boolean visible) 
	{	
		Logger.INSTANCE.log(Activator.ID, this, "Setting visible to <" + visible + ">...", EventSeverity.INFO);		
		
		// Call this on viewer thread.
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{								
				if(fromTransformNode != null)
				{
					if (!visible) 
					{					
						detachTransformNode();
					} 
					else 
					{						
						attachTransformNode();
					}
				}
				
				return null;
			}
		});						
	}
	
	@Override
	public void setColor(final RGB rgb) 
	{
		if(rgb != null)
		{
			Logger.INSTANCE.log(Activator.ID, this, "Setting color to <" + rgb + ">...", EventSeverity.INFO);
			
			this.vectorColor = JME3Utilities.convertToColorRGBA(rgb);
			jme3Application.enqueue(new Callable<Object>() 
			{
				@Override
				public Object call() throws Exception 
				{															
					if(vectorBodyGeometry != null)
					{				
						ColorRGBA color = vectorColor.clone();
						color.a = 0.5f;
						
						Material mat = vectorBodyGeometry.getMaterial();
						mat.setColor("Diffuse", vectorColor.clone());
						mat.setColor("Ambient", vectorColor.clone());
						mat.setColor("Specular", vectorColor.clone());															
					}
					
					if(vectorTipGeometry != null)
					{				
						ColorRGBA color = vectorColor.clone();
						color.a = 0.5f;
						
						Material mat = vectorTipGeometry.getMaterial();
						mat.setColor("Diffuse", vectorColor.clone());
						mat.setColor("Ambient", vectorColor.clone());
						mat.setColor("Specular", vectorColor.clone());															
					}
									
					return null;
				}	
			});	
		}		
	}	
	
	@Override
	public void dispose()
	{		
		if(tool != null)
		{
			tool.eAdapters().remove(FieldOfViewEntry3DToolNodeJME3Object.this.getAdapter());
		}
		
		jme3Application.enqueue(new Callable<Object>() 
		{
				@Override
				public Object call() throws Exception 
				{
					detachTransformNode();
					
					return null;
				}
		});	
						
		super.dispose();	
	}
	
	/**
	 * Creates the geometry associated with the tool.
	 */
	private void createGeometry()
	{
		vectorColor = JME3Utilities.convertToColorRGBA(tool.getCurrentVectorColor());
		
		// Creates the to node.
		fromTransformNode = new Node("Vector From");
		attachTransformNode();
		
		tipTransformNode = new Node("Vector Tip");
		fromTransformNode.attachChild(tipTransformNode);
		
		// Creates the vector body.
		vectorBodyNode = createVectorBodyNode();
		vectorBodyGeometry = createVectorBodyGeometry();
		vectorBodyNode.attachChild(vectorBodyGeometry);		
		fromTransformNode.attachChild(vectorBodyNode);
			
		// Creates Vector Tip
		vectorTipNode = createVectorTipNode();
		vectorTipGeometry = createVectorTipGeometry();
		vectorTipNode.attachChild(vectorTipGeometry);						
		fromTransformNode.attachChild(vectorTipNode);

		
		// Updates the geometry.
		updateVectorBody();	
		updateVectorTip();		
	}
	
	/**
	 * Attaches the fromTransformNode. Not thread safe.
	 */
	private void attachTransformNode()
	{
		if(fromTransformNode != null)
		{
			if(!jme3Application.getRootNode().hasChild(fromTransformNode))
			{
				jme3Application.getRootNode().attachChild(fromTransformNode);
			}
		}
	}
	
	/**
	 * Detaches the fromTransformNode. Not thread safe.
	 */
	private void detachTransformNode()
	{
		if(fromTransformNode != null)
		{
			jme3Application.getRootNode().detachChild(fromTransformNode);
		}
	}
		
	// Sun vector cylinder
	
	private Node createVectorBodyNode()
	{
		Node node = new Node("Field Of View Vector");
		return node;
	}
	
	private Geometry createVectorBodyGeometry()
	{		
		Geometry geometry = new Geometry("Field Of View Vector Body", getVectorBodyMesh());
		geometry.setMaterial(createMaterial());
		
		geometry.setQueueBucket(Bucket.Transparent);
		
		Quaternion q = new Quaternion();
		q = q.fromAngleAxis(FastMath.PI / 2, new Vector3f(0, 1, 0));		
		geometry.setLocalRotation(q);
				
		return geometry;
	}
	
	private Material createMaterial()
	{
		ColorRGBA color = vectorColor.clone();
		color.a = 1.0f;
		
		Material mat = new Material(getApplication().getAssetManager(), "Common/MatDefs/Light/Lighting.j3md");							
		mat.setColor("Diffuse", color);
		mat.setColor("Ambient", color);
		mat.setColor("Specular", color);
		mat.setFloat("Shininess",64f); 	
		mat.setBoolean("UseMaterialColors",true);  	
		
		return mat;
	}
	
	private Mesh getVectorBodyMesh() 
	{
		Cylinder cylinder = new Cylinder(4, 10, 1.0f, 1.0f, true);
		return cylinder;
	}
	
	private void updateVectorBody()
	{
		if(getTopologyNode() != null)
		{			
			if(tool != null)
			{
				float length = (float) tool.getCurrentVectorLength();
				float diameter = (float) tool.getVectorDiameter();
				vectorBodyGeometry.setLocalScale(diameter, diameter, length);
				vectorBodyNode.setLocalTranslation(length / 2.0f, 0, 0);
			}
		}
	}
		
	// Vector Tip
	
	// Vector Tip	
	private Node createVectorTipNode()
	{
		Node node = new Node("Vector Tip");						
		return node;
	}
	
	private Geometry createVectorTipGeometry()
	{
		Geometry geometry = new Geometry("Vector Tip Geometry", createVectorTipMesh());
		geometry.setMaterial(createMaterial());
		
		// Scale the basic cone to make it more pointy.		
		geometry.setLocalScale(1, 1 , 1);
		geometry.setLocalTranslation(0, 0, 0);
		
		Quaternion q = new Quaternion();
		q = q.fromAngleAxis(-FastMath.PI / 2, new Vector3f(0, 0, 1));		
		geometry.setLocalRotation(q);
		
		return geometry;
	}
	
	private Mesh createVectorTipMesh() 
	{
		Dome dome = new Dome(2, 12, 1);				
		return dome;
	}
	
	private void updateVectorTip()
	{
		if(getTopologyNode() != null)
		{			
			if(tool != null)
			{
				float length = (float) tool.getCurrentVectorLength();								
				float diameter = (float) tool.getVectorDiameter() * TIP_BASE_TO_VECTOR_BASE_RATIO;
				
				vectorTipGeometry.setLocalScale(diameter, diameter, diameter);
				vectorTipNode.setLocalTranslation(length, 0, 0);
			}
		}
	}
	
	
	private void updateDirection()
	{		
		if(getTopologyNode() != null)
		{			
			if(tool != null)
			{
				ca.gc.asc_csa.apogy.common.topology.Node fromNode = tool.getFromNode();
				if(fromNode != null)
				{
					fromTransformNode.setLocalTranslation(JME3Utilities.convertToVector3f(tool.getFromAbsolutePosition().asTuple3d()));
					
					// Update the tip transform.
					float tipLenght = (float) (tool.getVectorDiameter() * TIP_LENGHT_TO_BASE_RATIO);
					tipTransformNode.setLocalTranslation((float)(tool.getCurrentVectorLength() + tipLenght), 0.0f, 0.0f);
				}
			}
		}	
				
		Vector3f v = getVectorDirection();		
		if(v != null)
		{
			v = v.normalize();
								
			// Orients the geometry
			Quaternion rot = getQuarternionV1ToV2(new Vector3f(1, 0, 0), v);						
			fromTransformNode.setLocalRotation(rot);
		}
	}
	
	private Vector3f getVectorDirection()
	{		
		if(getTopologyNode() != null)
		{
			if(tool.getFromNode() != null && tool.getToNode() != null)
			{
				if(tool.getFromAbsolutePosition() != null && tool.getToAbsolutePosition() != null)
				{
					Vector3d from = new Vector3d(tool.getFromAbsolutePosition().asTuple3d());
					Vector3d to = new Vector3d(tool.getToAbsolutePosition().asTuple3d());				
					to.sub(from);
					
					return new Vector3f((float) to.x, (float) to.y, (float) to.z);
				}						
			}
		}
				
		return null;
	}
	
	private Quaternion getQuarternionV1ToV2(Vector3f v1, Vector3f v2)
	{
		float angle = v2.angleBetween(v1);
		
		// Cross product gives the vector about which to rotate.
		Vector3f axis = v1.cross(v2);
		axis = axis.normalize();
		Quaternion q = new Quaternion();
		q = q.fromAngleAxis(angle, axis);
		return q;
	}
	
	
	private RGB convertToRGB(Color3f color)
	{
		int red = (int) Math.round((color.x * 255.0f));
		int green = (int) Math.round((color.y * 255.0f));
		int blue = (int) Math.round((color.z * 255.0f));
		
		return new RGB(red, green, blue);				
	}
	
	
	private Adapter getAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{					
					if(msg.getNotifier() instanceof FieldOfViewEntry3DToolNode)
					{												
						// int featureId = msg.getFeatureID(FieldOfViewEntry3DToolNode.class);
						
						// TODO.			
					}
					else if(msg.getNotifier() instanceof FieldOfViewEntry3DTool)
					{
						FieldOfViewEntry3DTool sunVector3DTool = (FieldOfViewEntry3DTool) msg.getNotifier();
						
						if(!sunVector3DTool.isDisposed())
						{
							int featureId = msg.getFeatureID(FieldOfViewEntry3DTool.class);
							switch(featureId)
							{
								// Positions or Nodes have changed.
								case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH:
								case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FROM_RELATIVE_POSITION:
								case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FROM_ABSOLUTE_POSITION:
								case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__TO_ABSOLUTE_POSITION:
								case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__TO_RELATIVE_POSITION:
								case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE:
								case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE:
										requestUpdate();
								break;
								
								case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_DIAMETER:									
									requestUpdate();
								break;
								
								// Displayed color has changed.
								case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR:
										if(msg.getNewValue() instanceof Color3f)
										{
											Color3f color = (Color3f) msg.getNewValue();
											setColor(convertToRGB(color));								
										}
								break;							
																
								// Visibility has changed.
								case ApogyAddonsPackage.SIMPLE3_DTOOL__VISIBLE:
									setVisible(msg.getNewBooleanValue());
								break;
								
								case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE:
									
									// Removes adapter if applicable.
									if(getTopologyNode().getFieldOfViewEntry3DTool() != null)
									{
										getTopologyNode().getFieldOfViewEntry3DTool().eAdapters().remove(getAdapter());
									}
									
									// Dispose of this node if new ToolNode is null.
									if(msg.getNewValue() == null)
									{
										dispose();
									}
								break;
								
								// Disposed has changed.
								case ApogyAddonsPackage.SIMPLE3_DTOOL__DISPOSED:
									dispose();								
								break;
							}
						}
					}
				}
			};
		}
		return adapter;
	}
}

package ca.gc.asc_csa.apogy.addons.sensors.range.ui.scene_objects;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;
import ca.gc.asc_csa.apogy.common.topology.ui.SceneObject;

public interface RasterScanDataSceneObject extends SceneObject 
{		
	public boolean isFovVisible();

	public void setFovVisible(boolean fovVisible);
	
	public boolean isDataVisible();

	public void setDataVisible(boolean dataVisible);
	
	public MeshPresentationMode getFovMeshPresentationMode();

	public void setFovMeshPresentationMode(MeshPresentationMode fovMeshPresentationMode);
}

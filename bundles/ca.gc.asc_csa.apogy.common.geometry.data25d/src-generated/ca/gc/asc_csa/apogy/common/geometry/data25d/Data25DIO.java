package ca.gc.asc_csa.apogy.common.geometry.data25d;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.io.IOException;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Utilities class used to load and save VolumetricCoordinatesSet25D to and from file.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.geometry.data25d.ApogyCommonGeometryData25DPackage#getData25DIO()
 * @model
 * @generated
 */
public interface Data25DIO extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Saves a VolumetricCoordinatesSet25D to a file.
	 * @param points The VolumetricCoordinatesSet25D to save to file.
	 * @param fileName The absolute path of the file to save.
	 * @throws An Exception if the save fails.
	 * <!-- end-model-doc -->
	 * @model exceptions="ca.gc.asc_csa.apogy.common.geometry.data25d.IOException" pointsUnique="false" fileNameUnique="false"
	 * @generated
	 */
	void saveXYZ(VolumetricCoordinatesSet25D points, String fileName) throws IOException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Saves a VolumetricCoordinatesSet25D to a file.
	 * @param fileName The absolute path of the file to load.
	 * @return The VolumetricCoordinatesSet25D loaded from the file.
	 * @throws An Exception if the load fails.
	 * <!-- end-model-doc -->
	 * @model unique="false" exceptions="ca.gc.asc_csa.apogy.common.geometry.data25d.IOException" fileNameUnique="false"
	 * @generated
	 */
	VolumetricCoordinatesSet25D loadXYZ(String fileName) throws IOException;

} // Data25DIO

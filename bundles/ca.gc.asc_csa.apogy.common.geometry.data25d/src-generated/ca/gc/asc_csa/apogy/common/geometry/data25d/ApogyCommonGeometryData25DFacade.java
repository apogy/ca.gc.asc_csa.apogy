package ca.gc.asc_csa.apogy.common.geometry.data25d;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.common.geometry.data25d.impl.ApogyCommonGeometryData25DFacadeImpl;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianCoordinatesSet;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Facade for Geometry Data 2.5D.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.geometry.data25d.ApogyCommonGeometryData25DPackage#getApogyCommonGeometryData25DFacade()
 * @model
 * @generated
 */
public interface ApogyCommonGeometryData25DFacade extends EObject {
	
	public static ApogyCommonGeometryData25DFacade INSTANCE = ApogyCommonGeometryData25DFacadeImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Coordinates25D from an original Coordinates25D.
	 * @param coordinates25D The original Coordinates25D.
	 * @return A Coordinates25D with the same coordimates as the original.
	 * <!-- end-model-doc -->
	 * @model unique="false" coordinates25DUnique="false"
	 * @generated
	 */
	Coordinates25D createCoordinates25D(Coordinates25D coordinates25D);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Coordinates25D from coodinates.
	 * @param u The u coordinate.
	 * @param v The v coordinate.
	 * @param w The w coordinate.
	 * @return A Coordinates25D with the specified coordinates.
	 * <!-- end-model-doc -->
	 * @model unique="false" uUnique="false" vUnique="false" wUnique="false"
	 * @generated
	 */
	Coordinates25D createCoordinates25D(double u, double v, double w);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a VolumetricCoordinatesSet25D from an original VolumetricCoordinatesSet25D.
	 * @param volumetricCoordinatesSet25D The original VolumetricCoordinatesSet25D.
	 * @return A VolumetricCoordinatesSet25D containing copies of all coordinates found in the original VolumetricCoordinatesSet25D.
	 * <!-- end-model-doc -->
	 * @model unique="false" volumetricCoordinatesSet25DUnique="false"
	 * @generated
	 */
	<T extends VolumetricCoordinatesSet25D> CartesianCoordinatesSet createCartesianCoordinatesSet(T volumetricCoordinatesSet25D);

} // ApogyCommonGeometryData25DFacade

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.transaction.impl;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFactory;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionPackage;

import java.util.Collection;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.edit.command.AbstractOverrideableCommand;

import org.eclipse.emf.transaction.TransactionalEditingDomain;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCommonTransactionPackageImpl extends EPackageImpl implements ApogyCommonTransactionPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass apogyCommonTransactionFacadeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType transactionalEditingDomainEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType collectionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType abstractOverrideableCommandEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyCommonTransactionPackageImpl() {
		super(eNS_URI, ApogyCommonTransactionFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCommonTransactionPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyCommonTransactionPackage init() {
		if (isInited) return (ApogyCommonTransactionPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTransactionPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCommonTransactionPackageImpl theApogyCommonTransactionPackage = (ApogyCommonTransactionPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCommonTransactionPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCommonTransactionPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCommonTransactionPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCommonTransactionPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCommonTransactionPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCommonTransactionPackage.eNS_URI, theApogyCommonTransactionPackage);
		return theApogyCommonTransactionPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApogyCommonTransactionFacade() {
		return apogyCommonTransactionFacadeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__GetDefaultEditingDomain() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__AddInTempTransactionalEditingDomain__EObject() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__RemoveFromEditingDomain__EObject() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__AreEditingDomainsValid__EObject_EStructuralFeature_Object_boolean() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicSet__EObject_EStructuralFeature_Object_boolean() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicSet__EObject_EStructuralFeature_Object() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Object_boolean() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Object() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Collection_boolean() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Collection() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Object_boolean() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Object() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Collection_boolean() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Collection() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicClear__EObject_EStructuralFeature_boolean() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicClear__EObject_EStructuralFeature() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicDelete__EObject_EStructuralFeature_Object_boolean() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__BasicDelete__EObject_EStructuralFeature_Object() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__ExecuteCommand__AbstractOverrideableCommand() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonTransactionFacade__GetTransactionalEditingDomain__EObject() {
		return apogyCommonTransactionFacadeEClass.getEOperations().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getTransactionalEditingDomain() {
		return transactionalEditingDomainEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getCollection() {
		return collectionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAbstractOverrideableCommand() {
		return abstractOverrideableCommandEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTransactionFactory getApogyCommonTransactionFactory() {
		return (ApogyCommonTransactionFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		apogyCommonTransactionFacadeEClass = createEClass(APOGY_COMMON_TRANSACTION_FACADE);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___GET_DEFAULT_EDITING_DOMAIN);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___ADD_IN_TEMP_TRANSACTIONAL_EDITING_DOMAIN__EOBJECT);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___REMOVE_FROM_EDITING_DOMAIN__EOBJECT);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___ARE_EDITING_DOMAINS_VALID__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_SET__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_SET__EOBJECT_ESTRUCTURALFEATURE_OBJECT);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_OBJECT);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_COLLECTION_BOOLEAN);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_COLLECTION);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_OBJECT);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_COLLECTION_BOOLEAN);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_COLLECTION);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_CLEAR__EOBJECT_ESTRUCTURALFEATURE_BOOLEAN);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_CLEAR__EOBJECT_ESTRUCTURALFEATURE);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_DELETE__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___BASIC_DELETE__EOBJECT_ESTRUCTURALFEATURE_OBJECT);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___EXECUTE_COMMAND__ABSTRACTOVERRIDEABLECOMMAND);
		createEOperation(apogyCommonTransactionFacadeEClass, APOGY_COMMON_TRANSACTION_FACADE___GET_TRANSACTIONAL_EDITING_DOMAIN__EOBJECT);

		// Create data types
		transactionalEditingDomainEDataType = createEDataType(TRANSACTIONAL_EDITING_DOMAIN);
		collectionEDataType = createEDataType(COLLECTION);
		abstractOverrideableCommandEDataType = createEDataType(ABSTRACT_OVERRIDEABLE_COMMAND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(apogyCommonTransactionFacadeEClass, ApogyCommonTransactionFacade.class, "ApogyCommonTransactionFacade", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getApogyCommonTransactionFacade__GetDefaultEditingDomain(), this.getTransactionalEditingDomain(), "getDefaultEditingDomain", 0, 1, !IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getApogyCommonTransactionFacade__AddInTempTransactionalEditingDomain__EObject(), null, "addInTempTransactionalEditingDomain", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__RemoveFromEditingDomain__EObject(), null, "removeFromEditingDomain", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__AreEditingDomainsValid__EObject_EStructuralFeature_Object_boolean(), theEcorePackage.getEInt(), "areEditingDomainsValid", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "fix", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicSet__EObject_EStructuralFeature_Object_boolean(), null, "basicSet", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "fix", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicSet__EObject_EStructuralFeature_Object(), null, "basicSet", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Object_boolean(), null, "basicAdd", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "fix", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Object(), null, "basicAdd", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Collection_boolean(), null, "basicAdd", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCollection(), "collection", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "fix", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Collection(), null, "basicAdd", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCollection(), "collection", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Object_boolean(), null, "basicRemove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "fix", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Object(), null, "basicRemove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Collection_boolean(), null, "basicRemove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCollection(), "collection", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "fix", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Collection(), null, "basicRemove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCollection(), "collection", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicClear__EObject_EStructuralFeature_boolean(), null, "basicClear", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "fix", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicClear__EObject_EStructuralFeature(), null, "basicClear", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicDelete__EObject_EStructuralFeature_Object_boolean(), null, "basicDelete", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "fix", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__BasicDelete__EObject_EStructuralFeature_Object(), null, "basicDelete", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "owner", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "feature", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__ExecuteCommand__AbstractOverrideableCommand(), null, "executeCommand", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAbstractOverrideableCommand(), "command", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTransactionFacade__GetTransactionalEditingDomain__EObject(), this.getTransactionalEditingDomain(), "getTransactionalEditingDomain", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(transactionalEditingDomainEDataType, TransactionalEditingDomain.class, "TransactionalEditingDomain", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(collectionEDataType, Collection.class, "Collection", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.Collection<?>");
		initEDataType(abstractOverrideableCommandEDataType, AbstractOverrideableCommand.class, "AbstractOverrideableCommand", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //ApogyCommonTransactionPackageImpl

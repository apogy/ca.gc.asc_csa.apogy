/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.transaction;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.command.AbstractOverrideableCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

import ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Facade for Transactions. This provides tools to process and manage EMF Transaction.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionPackage#getApogyCommonTransactionFacade()
 * @model
 * @generated
 */
public interface ApogyCommonTransactionFacade extends EObject {
	
	/**
	 * Facade Singleton.
	 * @generated_NOT
	 */
	public static ApogyCommonTransactionFacade INSTANCE = ApogyCommonTransactionFacadeImpl.getInstance();
	
	public static final int EXECUTE_COMMAND_ON_OWNER_DOMAIN = 0;
	public static final int EXECUTE_EMF_METHOD = 1;
	public static final int DONT_EXECUTE = 2;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns the default Apogy {@link TransactionalEditingDomain).
	 * @return Reference to the Apogy {@link TransactionalEditingDomain).
	 * <!-- end-model-doc -->
	 * @model kind="operation" dataType="ca.gc.asc_csa.apogy.common.transaction.TransactionalEditingDomain" unique="false"
	 * @generated
	 */
	TransactionalEditingDomain getDefaultEditingDomain();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Puts the specified {@link EObject} inside a {@link ResourceSet} that uses a {@link TransactionalEditingDomain}.
	 * It is important to remove the {@link EObject} from the temporary {@link TransactionalEditingDomain} with
	 * {@link #ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionFacadeImpl.removeFromEditingDomain(EObject eObject) removeFromEditingDomain(EObject eObject)}
	 * when the temporary {@link TransactionalEditingDomain}
	 * is no longer needed to be able to save the modification made to the {@link EObject}.
	 * @param eObject The reference to the {@link EObject}.
	 * <!-- end-model-doc -->
	 * @model eObjectUnique="false"
	 * @generated
	 */
	void addInTempTransactionalEditingDomain(EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Removes the specified {@link EObject}'s {@link Resource} from it's {@link ResourceSet}.
	 * Then, disconnect the {@link Resource} from it's {@link TransactionalEditingDomain}.
	 * @param eObject The reference to the {@link EObject}.
	 * <!-- end-model-doc -->
	 * @model eObjectUnique="false"
	 * @generated
	 */
	void removeFromEditingDomain(EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Verifies if child can be set in parent. Intended for internal use.
	 * @param owner The owner of the feature.
	 * @param feature The feature. Used to determine if the feature is a containment or a reference.
	 * @param value The value to be assigned to the feature.
	 * @param fix True will attempt to remove the value from it current editing domain to make the set operation valid.
	 * @return true if a set or add can be executed, false otherwise.
	 * <!-- end-model-doc -->
	 * @model unique="false" ownerUnique="false" featureUnique="false" valueUnique="false" fixUnique="false"
	 * @generated
	 */
	int areEditingDomainsValid(EObject owner, EStructuralFeature feature, Object value, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Set an eObject's feature to a specified value through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to set.
	 * @param value The new value of the feature.
	 * @param fix If true will ensure the value and owner editing domains are forced to make the transaction valid,
	 * false will attempt the transaction without forcing the editing domains.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicSet(EObject owner, EStructuralFeature feature, Object value, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Set an eObject's feature to a specified value through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to set.
	 * @param value The new value of the feature.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false"
	 * @generated
	 */
	void basicSet(EObject owner, EStructuralFeature feature, Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Adds a value to an eObject's feature through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to add the value to. This feature needs to be a list.
	 * @param value The value to add.
	 * @param fix If true will ensure the value and owner editing domains are forced to make the transaction valid,
	 * false will attempt the transaction without forcing the editing domains.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicAdd(EObject owner, EStructuralFeature feature, Object value, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Adds a value to an eObject's feature through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to add the value to. This feature needs to be a list.
	 * @param value The value to add.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false"
	 * @generated
	 */
	void basicAdd(EObject owner, EStructuralFeature feature, Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Adds a collection of values to an eObject's feature through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to add the values to. This feature needs to be a list.
	 * @param value The collection of values to add.
	 * @param fix If true will ensure the value and owner editing domains are forced to make the transaction valid,
	 * false will attempt the transaction without forcing the editing domains.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" collectionDataType="ca.gc.asc_csa.apogy.common.transaction.Collection" collectionUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicAdd(EObject owner, EStructuralFeature feature, Collection<?> collection, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Adds a collection of values to an eObject's feature through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to add the values to. This feature needs to be a list.
	 * @param value The collection of values to add.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" collectionDataType="ca.gc.asc_csa.apogy.common.transaction.Collection" collectionUnique="false"
	 * @generated
	 */
	void basicAdd(EObject owner, EStructuralFeature feature, Collection<?> collection);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Removes a value to an eObject's feature through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to remove the value from. This feature needs to be a list.
	 * @param value The value to remove.
	 * @param fix If true will ensure the value and owner editing domains are forced to make the transaction valid,
	 * false will attempt the transaction without forcing the editing domains.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicRemove(EObject owner, EStructuralFeature feature, Object value, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Removes a value to an eObject's feature through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to remove the value from. This feature needs to be a list.
	 * @param value The value to remove.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false"
	 * @generated
	 */
	void basicRemove(EObject owner, EStructuralFeature feature, Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Removes a collection of values to an eObject's feature through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to remove the values from. This feature needs to be a list.
	 * @param value The collection of values to remove.
	 * @param fix If true will ensure the value and owner editing domains are forced to make the transaction valid,
	 * false will attempt the transaction without forcing the editing domains.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" collectionDataType="ca.gc.asc_csa.apogy.common.transaction.Collection" collectionUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicRemove(EObject owner, EStructuralFeature feature, Collection<?> collection, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Removes a collection of values to an eObject's feature through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to remove the values from. This feature needs to be a list.
	 * @param value The collection of values to remove.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" collectionDataType="ca.gc.asc_csa.apogy.common.transaction.Collection" collectionUnique="false"
	 * @generated
	 */
	void basicRemove(EObject owner, EStructuralFeature feature, Collection<?> collection);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Removes all values to an eObject's feature through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to remove the all the values from. This feature needs to be a list.
	 * @param fix If true will ensure the value and owner editing domains are forced to make the transaction valid,
	 * false will attempt the transaction without forcing the editing domains.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicClear(EObject owner, EStructuralFeature feature, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Removes all values to an eObject's feature through a transaction.
	 * @param owner The owner of the feature.
	 * @param feature The feature to remove the all the values from. This feature needs to be a list.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false"
	 * @generated
	 */
	void basicClear(EObject owner, EStructuralFeature feature);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Delete an eObject's feature through a transaction. If the feature is a list, the value specified is remove from the
	 * list, if the feaure is a single value, it is set to null.
	 * @param owner The owner of the feature.
	 * @param feature The feature to delete the value from. This feature can be a single element or a list.
	 * @param value The value to remove if the feature is a list. Ignored if the feature is a single value.
	 * @param fix If true will ensure the value and owner editing domains are forced to make the transaction valid,
	 * false will attempt the transaction without forcing the editing domains.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicDelete(EObject owner, EStructuralFeature feature, Object value, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Delete an eObject's feature through a transaction. If the feature is a list, the value specified is remove from the
	 * list, if the feaure is a single value, it is set to null.
	 * @param owner The owner of the feature.
	 * @param feature The feature to delete the value from. This feature can be a single element or a list.
	 * @param value The value to remove if the feature is a list. Ignored if the feature is a single value.
	 * <!-- end-model-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false"
	 * @generated
	 */
	void basicDelete(EObject owner, EStructuralFeature feature, Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Execute a AbstractOverrideableCommand which implements a transaction on a TransactionalEditingDomain.
	 * @param The AbstractOverrideableCommand.
	 * <!-- end-model-doc -->
	 * @model commandDataType="ca.gc.asc_csa.apogy.common.transaction.AbstractOverrideableCommand" commandUnique="false"
	 * @generated
	 */
	void executeCommand(AbstractOverrideableCommand command);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets the TransactionalEditingDomain associated with an EObject.
	 * @param eObject The EObject.
	 * @return The TransactionalEditingDomain, null if none is found.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.transaction.TransactionalEditingDomain" unique="false" eObjectUnique="false"
	 * @generated
	 */
	TransactionalEditingDomain getTransactionalEditingDomain(EObject eObject);

} // ApogyCommonTransactionFacade

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Olivier L. Larouche
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.transaction;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyCommonTransaction' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque, \n     Olivier L. Larouche \n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' childCreationExtenders='true' modelName='ApogyCommonTransaction' modelDirectory='/ca.gc.asc_csa.apogy.common.transaction/src-generated' basePackage='ca.gc.asc_csa.apogy.common'"
 * @generated
 */
public interface ApogyCommonTransactionPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "transaction";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.common.transaction";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "transaction";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCommonTransactionPackage eINSTANCE = ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionFacadeImpl <em>Facade</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionFacadeImpl
	 * @see ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionPackageImpl#getApogyCommonTransactionFacade()
	 * @generated
	 */
	int APOGY_COMMON_TRANSACTION_FACADE = 0;

	/**
	 * The number of structural features of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Default Editing Domain</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___GET_DEFAULT_EDITING_DOMAIN = 0;

	/**
	 * The operation id for the '<em>Add In Temp Transactional Editing Domain</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___ADD_IN_TEMP_TRANSACTIONAL_EDITING_DOMAIN__EOBJECT = 1;

	/**
	 * The operation id for the '<em>Remove From Editing Domain</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___REMOVE_FROM_EDITING_DOMAIN__EOBJECT = 2;

	/**
	 * The operation id for the '<em>Are Editing Domains Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___ARE_EDITING_DOMAINS_VALID__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN = 3;

	/**
	 * The operation id for the '<em>Basic Set</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_SET__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN = 4;

	/**
	 * The operation id for the '<em>Basic Set</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_SET__EOBJECT_ESTRUCTURALFEATURE_OBJECT = 5;

	/**
	 * The operation id for the '<em>Basic Add</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN = 6;

	/**
	 * The operation id for the '<em>Basic Add</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_OBJECT = 7;

	/**
	 * The operation id for the '<em>Basic Add</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_COLLECTION_BOOLEAN = 8;

	/**
	 * The operation id for the '<em>Basic Add</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_COLLECTION = 9;

	/**
	 * The operation id for the '<em>Basic Remove</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN = 10;

	/**
	 * The operation id for the '<em>Basic Remove</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_OBJECT = 11;

	/**
	 * The operation id for the '<em>Basic Remove</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_COLLECTION_BOOLEAN = 12;

	/**
	 * The operation id for the '<em>Basic Remove</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_COLLECTION = 13;

	/**
	 * The operation id for the '<em>Basic Clear</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_CLEAR__EOBJECT_ESTRUCTURALFEATURE_BOOLEAN = 14;

	/**
	 * The operation id for the '<em>Basic Clear</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_CLEAR__EOBJECT_ESTRUCTURALFEATURE = 15;

	/**
	 * The operation id for the '<em>Basic Delete</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_DELETE__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN = 16;

	/**
	 * The operation id for the '<em>Basic Delete</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___BASIC_DELETE__EOBJECT_ESTRUCTURALFEATURE_OBJECT = 17;

	/**
	 * The operation id for the '<em>Execute Command</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___EXECUTE_COMMAND__ABSTRACTOVERRIDEABLECOMMAND = 18;

	/**
	 * The operation id for the '<em>Get Transactional Editing Domain</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE___GET_TRANSACTIONAL_EDITING_DOMAIN__EOBJECT = 19;

	/**
	 * The number of operations of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TRANSACTION_FACADE_OPERATION_COUNT = 20;

	/**
	 * The meta object id for the '<em>Transactional Editing Domain</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.transaction.TransactionalEditingDomain
	 * @see ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionPackageImpl#getTransactionalEditingDomain()
	 * @generated
	 */
	int TRANSACTIONAL_EDITING_DOMAIN = 1;

	/**
	 * The meta object id for the '<em>Collection</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Collection
	 * @see ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionPackageImpl#getCollection()
	 * @generated
	 */
	int COLLECTION = 2;

	/**
	 * The meta object id for the '<em>Abstract Overrideable Command</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.edit.command.AbstractOverrideableCommand
	 * @see ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionPackageImpl#getAbstractOverrideableCommand()
	 * @generated
	 */
	int ABSTRACT_OVERRIDEABLE_COMMAND = 3;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade <em>Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Facade</em>'.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade
	 * @generated
	 */
	EClass getApogyCommonTransactionFacade();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#getDefaultEditingDomain() <em>Get Default Editing Domain</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Default Editing Domain</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#getDefaultEditingDomain()
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__GetDefaultEditingDomain();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#addInTempTransactionalEditingDomain(org.eclipse.emf.ecore.EObject) <em>Add In Temp Transactional Editing Domain</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add In Temp Transactional Editing Domain</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#addInTempTransactionalEditingDomain(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__AddInTempTransactionalEditingDomain__EObject();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#removeFromEditingDomain(org.eclipse.emf.ecore.EObject) <em>Remove From Editing Domain</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove From Editing Domain</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#removeFromEditingDomain(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__RemoveFromEditingDomain__EObject();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#areEditingDomainsValid(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object, boolean) <em>Are Editing Domains Valid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Are Editing Domains Valid</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#areEditingDomainsValid(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object, boolean)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__AreEditingDomainsValid__EObject_EStructuralFeature_Object_boolean();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicSet(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object, boolean) <em>Basic Set</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Set</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicSet(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object, boolean)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicSet__EObject_EStructuralFeature_Object_boolean();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicSet(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object) <em>Basic Set</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Set</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicSet(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicSet__EObject_EStructuralFeature_Object();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicAdd(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object, boolean) <em>Basic Add</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Add</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicAdd(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object, boolean)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Object_boolean();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicAdd(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object) <em>Basic Add</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Add</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicAdd(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Object();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicAdd(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection, boolean) <em>Basic Add</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Add</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicAdd(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection, boolean)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Collection_boolean();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicAdd(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection) <em>Basic Add</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Add</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicAdd(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Collection();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicRemove(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object, boolean) <em>Basic Remove</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Remove</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicRemove(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object, boolean)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Object_boolean();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicRemove(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object) <em>Basic Remove</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Remove</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicRemove(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Object();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicRemove(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection, boolean) <em>Basic Remove</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Remove</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicRemove(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection, boolean)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Collection_boolean();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicRemove(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection) <em>Basic Remove</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Remove</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicRemove(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Collection();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicClear(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, boolean) <em>Basic Clear</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Clear</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicClear(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, boolean)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicClear__EObject_EStructuralFeature_boolean();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicClear(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature) <em>Basic Clear</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Clear</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicClear(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicClear__EObject_EStructuralFeature();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicDelete(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object, boolean) <em>Basic Delete</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Delete</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicDelete(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object, boolean)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicDelete__EObject_EStructuralFeature_Object_boolean();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicDelete(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object) <em>Basic Delete</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Basic Delete</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#basicDelete(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__BasicDelete__EObject_EStructuralFeature_Object();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#executeCommand(org.eclipse.emf.edit.command.AbstractOverrideableCommand) <em>Execute Command</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Execute Command</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#executeCommand(org.eclipse.emf.edit.command.AbstractOverrideableCommand)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__ExecuteCommand__AbstractOverrideableCommand();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#getTransactionalEditingDomain(org.eclipse.emf.ecore.EObject) <em>Get Transactional Editing Domain</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Transactional Editing Domain</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade#getTransactionalEditingDomain(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getApogyCommonTransactionFacade__GetTransactionalEditingDomain__EObject();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.transaction.TransactionalEditingDomain <em>Transactional Editing Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Transactional Editing Domain</em>'.
	 * @see org.eclipse.emf.transaction.TransactionalEditingDomain
	 * @model instanceClass="org.eclipse.emf.transaction.TransactionalEditingDomain"
	 * @generated
	 */
	EDataType getTransactionalEditingDomain();

	/**
	 * Returns the meta object for data type '{@link java.util.Collection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Collection</em>'.
	 * @see java.util.Collection
	 * @model instanceClass="java.util.Collection<?>"
	 * @generated
	 */
	EDataType getCollection();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.edit.command.AbstractOverrideableCommand <em>Abstract Overrideable Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Abstract Overrideable Command</em>'.
	 * @see org.eclipse.emf.edit.command.AbstractOverrideableCommand
	 * @model instanceClass="org.eclipse.emf.edit.command.AbstractOverrideableCommand"
	 * @generated
	 */
	EDataType getAbstractOverrideableCommand();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCommonTransactionFactory getApogyCommonTransactionFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionFacadeImpl <em>Facade</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionFacadeImpl
		 * @see ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionPackageImpl#getApogyCommonTransactionFacade()
		 * @generated
		 */
		EClass APOGY_COMMON_TRANSACTION_FACADE = eINSTANCE.getApogyCommonTransactionFacade();

		/**
		 * The meta object literal for the '<em><b>Get Default Editing Domain</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___GET_DEFAULT_EDITING_DOMAIN = eINSTANCE.getApogyCommonTransactionFacade__GetDefaultEditingDomain();

		/**
		 * The meta object literal for the '<em><b>Add In Temp Transactional Editing Domain</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___ADD_IN_TEMP_TRANSACTIONAL_EDITING_DOMAIN__EOBJECT = eINSTANCE.getApogyCommonTransactionFacade__AddInTempTransactionalEditingDomain__EObject();

		/**
		 * The meta object literal for the '<em><b>Remove From Editing Domain</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___REMOVE_FROM_EDITING_DOMAIN__EOBJECT = eINSTANCE.getApogyCommonTransactionFacade__RemoveFromEditingDomain__EObject();

		/**
		 * The meta object literal for the '<em><b>Are Editing Domains Valid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___ARE_EDITING_DOMAINS_VALID__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN = eINSTANCE.getApogyCommonTransactionFacade__AreEditingDomainsValid__EObject_EStructuralFeature_Object_boolean();

		/**
		 * The meta object literal for the '<em><b>Basic Set</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_SET__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN = eINSTANCE.getApogyCommonTransactionFacade__BasicSet__EObject_EStructuralFeature_Object_boolean();

		/**
		 * The meta object literal for the '<em><b>Basic Set</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_SET__EOBJECT_ESTRUCTURALFEATURE_OBJECT = eINSTANCE.getApogyCommonTransactionFacade__BasicSet__EObject_EStructuralFeature_Object();

		/**
		 * The meta object literal for the '<em><b>Basic Add</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN = eINSTANCE.getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Object_boolean();

		/**
		 * The meta object literal for the '<em><b>Basic Add</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_OBJECT = eINSTANCE.getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Object();

		/**
		 * The meta object literal for the '<em><b>Basic Add</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_COLLECTION_BOOLEAN = eINSTANCE.getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Collection_boolean();

		/**
		 * The meta object literal for the '<em><b>Basic Add</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_ADD__EOBJECT_ESTRUCTURALFEATURE_COLLECTION = eINSTANCE.getApogyCommonTransactionFacade__BasicAdd__EObject_EStructuralFeature_Collection();

		/**
		 * The meta object literal for the '<em><b>Basic Remove</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN = eINSTANCE.getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Object_boolean();

		/**
		 * The meta object literal for the '<em><b>Basic Remove</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_OBJECT = eINSTANCE.getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Object();

		/**
		 * The meta object literal for the '<em><b>Basic Remove</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_COLLECTION_BOOLEAN = eINSTANCE.getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Collection_boolean();

		/**
		 * The meta object literal for the '<em><b>Basic Remove</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_REMOVE__EOBJECT_ESTRUCTURALFEATURE_COLLECTION = eINSTANCE.getApogyCommonTransactionFacade__BasicRemove__EObject_EStructuralFeature_Collection();

		/**
		 * The meta object literal for the '<em><b>Basic Clear</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_CLEAR__EOBJECT_ESTRUCTURALFEATURE_BOOLEAN = eINSTANCE.getApogyCommonTransactionFacade__BasicClear__EObject_EStructuralFeature_boolean();

		/**
		 * The meta object literal for the '<em><b>Basic Clear</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_CLEAR__EOBJECT_ESTRUCTURALFEATURE = eINSTANCE.getApogyCommonTransactionFacade__BasicClear__EObject_EStructuralFeature();

		/**
		 * The meta object literal for the '<em><b>Basic Delete</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_DELETE__EOBJECT_ESTRUCTURALFEATURE_OBJECT_BOOLEAN = eINSTANCE.getApogyCommonTransactionFacade__BasicDelete__EObject_EStructuralFeature_Object_boolean();

		/**
		 * The meta object literal for the '<em><b>Basic Delete</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___BASIC_DELETE__EOBJECT_ESTRUCTURALFEATURE_OBJECT = eINSTANCE.getApogyCommonTransactionFacade__BasicDelete__EObject_EStructuralFeature_Object();

		/**
		 * The meta object literal for the '<em><b>Execute Command</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___EXECUTE_COMMAND__ABSTRACTOVERRIDEABLECOMMAND = eINSTANCE.getApogyCommonTransactionFacade__ExecuteCommand__AbstractOverrideableCommand();

		/**
		 * The meta object literal for the '<em><b>Get Transactional Editing Domain</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TRANSACTION_FACADE___GET_TRANSACTIONAL_EDITING_DOMAIN__EOBJECT = eINSTANCE.getApogyCommonTransactionFacade__GetTransactionalEditingDomain__EObject();

		/**
		 * The meta object literal for the '<em>Transactional Editing Domain</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.transaction.TransactionalEditingDomain
		 * @see ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionPackageImpl#getTransactionalEditingDomain()
		 * @generated
		 */
		EDataType TRANSACTIONAL_EDITING_DOMAIN = eINSTANCE.getTransactionalEditingDomain();

		/**
		 * The meta object literal for the '<em>Collection</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Collection
		 * @see ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionPackageImpl#getCollection()
		 * @generated
		 */
		EDataType COLLECTION = eINSTANCE.getCollection();

		/**
		 * The meta object literal for the '<em>Abstract Overrideable Command</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.edit.command.AbstractOverrideableCommand
		 * @see ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionPackageImpl#getAbstractOverrideableCommand()
		 * @generated
		 */
		EDataType ABSTRACT_OVERRIDEABLE_COMMAND = eINSTANCE.getAbstractOverrideableCommand();

	}

} //ApogyCommonTransactionPackage

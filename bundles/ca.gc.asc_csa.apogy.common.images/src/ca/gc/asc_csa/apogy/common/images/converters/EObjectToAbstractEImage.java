package ca.gc.asc_csa.apogy.common.images.converters;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.common.converters.IConverter;
import ca.gc.asc_csa.apogy.common.images.AbstractEImage;

public class EObjectToAbstractEImage implements IConverter {

	@Override
	public Class<?> getOutputType() { 
		return AbstractEImage.class;
	}

	@Override
	public Class<?> getInputType() {		
		return EObject.class;
	}

	@Override
	public boolean canConvert(Object input) 
	{		
		return input instanceof AbstractEImage;
	}

	@Override
	public Object convert(Object input) throws Exception 
	{		
		return (AbstractEImage) input;
	}

}

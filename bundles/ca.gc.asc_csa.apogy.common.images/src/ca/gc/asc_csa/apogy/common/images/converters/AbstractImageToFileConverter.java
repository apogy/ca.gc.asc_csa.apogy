/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.images.converters;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ca.gc.asc_csa.apogy.common.converters.FileExporterUtilities;
import ca.gc.asc_csa.apogy.common.converters.IFileExporter;
import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;

public class AbstractImageToFileConverter implements IFileExporter 
{	
	public static final String JPEG_FILE_EXTENSION = "jpg";
	public static final String PNG_FILE_EXTENSION = "png";
	
	@Override
	public Class<?> getOutputType() {		
		return File.class;
	}

	@Override
	public Class<?> getInputType() {		
		return AbstractEImage.class;
	}

	@Override
	public boolean canConvert(Object input) 
	{
		if(input instanceof AbstractEImage)
		{
			return ((AbstractEImage) input).asBufferedImage() != null;
		}
		return false;
	}

	@Override
	public Object convert(Object input) throws Exception 
	{
		AbstractEImage eImage = (AbstractEImage) input;		
		
		// Saves the image in a file in the tmp folder .
		String tmpFolder = System.getProperty("user.home") + File.separator + System.getProperty("java.io.tmpdir");
		Date now = new Date();				
		
		
		// Tries all format until one that works is found.
		String fileName = null;
		try
		{			
			fileName = tmpFolder + File.separator + now.getTime() + "." + JPEG_FILE_EXTENSION;										
			EImagesUtilities.INSTANCE.saveImageAsJPEG(fileName, eImage);				
			
			return new File(fileName);	
		}
		catch(Exception e)
		{
			File toDelete = new File(fileName);
			toDelete.delete();
			
			e.printStackTrace();
			
			return null;
		}							
	}

	@Override
	public void exportToFile(Object input, String filePath, List<String> extensions) throws Exception 
	{		
		AbstractEImage eImage = (AbstractEImage) input;	
		for(String extension : extensions)
		{
			String fullPathString = filePath + "." + extension;			
			if(extension.equalsIgnoreCase(JPEG_FILE_EXTENSION))
			{									
				EImagesUtilities.INSTANCE.saveImageAsJPEG(fullPathString, eImage);
			}
			else if(extension.equalsIgnoreCase(PNG_FILE_EXTENSION))
			{										
				EImagesUtilities.INSTANCE.saveImageAsPNG(fullPathString, eImage);				
			}
			else if(extension.equalsIgnoreCase(METADATA_FILE_EXTENSION))
			{								
				FileExporterUtilities.saveMetaDataToFile(fullPathString, getMetaData(eImage));
			}
		}
	}

	@Override
	public List<String> getSupportedFileExtensions() 
	{
		List<String> extensions = new ArrayList<String>();
		extensions.add(JPEG_FILE_EXTENSION);
		extensions.add(PNG_FILE_EXTENSION);
		extensions.add(METADATA_FILE_EXTENSION);
		return extensions;
	}

	@Override
	public String getDescription(String fileExtension) 
	{
		if(fileExtension.contains(JPEG_FILE_EXTENSION))
		{
			return "Image file in JPEG format.";
		}
		else if(fileExtension.contains(PNG_FILE_EXTENSION))
		{
			return "Image file in PNG format.";
		}
		else if(fileExtension.contains(METADATA_FILE_EXTENSION))
		{
			return "The Image metadata.";
		}
		return null;		
	}
	
	/**
	 * Gets the metadata as a String for a given AbstractEImage.
	 * @param eImage The AbstractEImage.
	 * @return The metadata string.
	 */
	public static String getMetaData(AbstractEImage eImage)
	{
		String metadata = "";
		
		if(eImage != null)
		{
			metadata += "Image Width = " + eImage.getWidth() + "\n";
			metadata += "Image Height = " + eImage.getHeight() + "\n";
		}
		
		return metadata;
	}
}

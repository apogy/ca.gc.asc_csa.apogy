package ca.gc.asc_csa.apogy.common.images;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.awt.Color;
import java.awt.Font;

import java.awt.image.BufferedImage;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.swt.graphics.ImageData;
import ca.gc.asc_csa.apogy.common.images.impl.EImagesUtilitiesImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EImages Utilities</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Image Utilities functions.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.images.ApogyCommonImagesPackage#getEImagesUtilities()
 * @model
 * @generated
 */
public interface EImagesUtilities extends EObject
{
	public static EImagesUtilities INSTANCE = EImagesUtilitiesImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create a copy of an AbstractEImage.
	 * @param originalImage The original AbstractEImage.
	 * @return The copy.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false"
	 * @generated
	 */
	AbstractEImage copy(AbstractEImage originalImage);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create a grey scaled copy of an AbstractEImage.
	 * @param originalImage The original AbstractEImage.
	 * @return The grey scale copy.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false"
	 * @generated
	 */
	AbstractEImage convertToGrayScale(AbstractEImage originalImage);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a resized copy of an AbstractEImage.
	 * @param originalImage The original AbstractEImage.
	 * @param scaleFactor The scaling factor. Must be greater than zero.
	 * @return The resized copy.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" scaleFactorUnique="false"
	 * @generated
	 */
	AbstractEImage resize(AbstractEImage originalImage, double scaleFactor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a resized copy of an AbstractEImag with one scale factor per image dimensions.
	 * @param originalImage The original AbstractEImage.
	 * @param widthScaleFactor The scaling factor to be applied along the width of the image. Must be greater than zero.
	 * @param heightScaleFactor The scaling factor to be applied along the height of the image. Must be greater than zero.
	 * @return The resized copy.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" widthScaleFactorUnique="false" heightScaleFactorUnique="false"
	 * @generated
	 */
	AbstractEImage resize(AbstractEImage originalImage, double widthScaleFactor, double heightScaleFactor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a resized copy of an AbstractEImag to specified width and height.
	 * @param originalImage The original AbstractEImage.
	 * @param newWidth The target width of the copied. Must be greater than zero.
	 * @param newHeight The target height of the copied image. Must be greater than zero.
	 * @return The resized copy.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" newWidthUnique="false"
	 *        newWidthAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'" newHeightUnique="false"
	 *        newHeightAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'"
	 * @generated
	 */
	AbstractEImage resize(AbstractEImage originalImage, int newWidth, int newHeight);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a transparent image of specified dimensions.
	 * @param width Width of the image.
	 * @param height Height of the image.
	 * @return The transparent image.
	 * <!-- end-model-doc -->
	 * @model unique="false" widthUnique="false" heightUnique="false"
	 * @generated
	 */
	AbstractEImage createTransparentImage(int width, int height);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Saves a given image to file in JPEG format.
	 * @param destinationFilePath Absolute path of the file where to save the image, should not include the file extension.
	 * @param image The AbstractEImage to save.
	 * @throws An exception if the save fails.
	 * <!-- end-model-doc -->
	 * @model exceptions="ca.gc.asc_csa.apogy.common.images.Exception" destinationFilePathUnique="false" imageUnique="false"
	 * @generated
	 */
	void saveImageAsJPEG(String destinationFilePath, AbstractEImage image) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Saves a given image to file in PNG format.
	 * @param destinationFilePath Absolute path of the file where to save the image, should not include the file extension.
	 * @param image The AbstractEImage to save.
	 * @throws An exception if the save fails.
	 * <!-- end-model-doc -->
	 * @model exceptions="ca.gc.asc_csa.apogy.common.images.Exception" destinationFilePathUnique="false" imageUnique="false"
	 * @generated
	 */
	void saveImageAsPNG(String destinationFilePath, AbstractEImage image) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return an image that is the result of overlaying an image on top of an original image.
	 * @param originalImage The original image.
	 * @param overlayImage The overlaid image (i.e. the put on top of the original image).
	 * @param allowOverlayResize Whether or not to allow the overlay image to be resized to match the size of the original image (
	 * The overlay image aspect ratio may be changed).
	 * @return The resulting image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" overlayImageUnique="false" allowOverlayResizeUnique="false"
	 * @generated
	 */
	AbstractEImage applyOverlay(AbstractEImage originalImage, AbstractEImage overlayImage, boolean allowOverlayResize);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns an image that is a copy of the original flipped about the horizontal axis.
	 * @param originalImage The original image.
	 * @return The flipped image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false"
	 * @generated
	 */
	AbstractEImage flipHorizontal(AbstractEImage originalImage);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns an image that is a copy of the original flipped about the vertical axis.
	 * @param originalImage The original image.
	 * @return The flipped image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false"
	 * @generated
	 */
	AbstractEImage flipVertical(AbstractEImage originalImage);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns an image that is a copy of the original rotated about its upper left corner by a specified angle.
	 * @param originalImage The original image.
	 * @param angle The rotation angle in radians.
	 * @param enableImageResize Whether or not to allow the rotated image to be resized to contain all the rotated pixels.
	 * @return The rotated image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" angleUnique="false"
	 *        angleAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" enableImageResizeUnique="false"
	 * @generated
	 */
	AbstractEImage rotate(AbstractEImage originalImage, double angle, boolean enableImageResize);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns an image that is a copy of the original translated by a number of pixel in the vertical and horizontal directions.
	 * @param originalImage The original image.
	 * @param widthTranslation The translation along the width of the image, in pixels.
	 * @param heightTranslation The translation along the height of the image, in pixels.
	 * @return The translated image. This image is made large enough to contain the original image + the translations.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" widthTranslationUnique="false"
	 *        widthTranslationAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'" heightTranslationUnique="false"
	 *        heightTranslationAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'"
	 * @generated
	 */
	AbstractEImage translate(AbstractEImage originalImage, int widthTranslation, int heightTranslation);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Return the image size that would envelop all images in a list,
	 * @param images The list of images.
	 * @return The image size.
	 * <!-- end-model-doc -->
	 * @model unique="false" imagesDataType="ca.gc.asc_csa.apogy.common.images.List<? extends ca.gc.asc_csa.apogy.common.images.AbstractEImage>" imagesUnique="false" imagesMany="false"
	 * @generated
	 */
	ImageSize getAllEncompassingImageSize(List<? extends AbstractEImage> images);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create an image that is the result of stacking a list of images on top of each other.
	 * @param images The list of images.
	 * @param allowImageResize Whether or not to allow images being resized to match the previous one size.
	 * @param alignment Alignment to be used when stacking images.
	 * @return The resulting image.
	 * <!-- end-model-doc -->
	 * @model unique="false" imagesDataType="ca.gc.asc_csa.apogy.common.images.List<? extends ca.gc.asc_csa.apogy.common.images.AbstractEImage>" imagesUnique="false" imagesMany="false" allowImageResizeUnique="false" alignmentUnique="false"
	 * @generated
	 */
	AbstractEImage superPoseImages(List<? extends AbstractEImage> images, boolean allowImageResize, ImageAlignment alignment);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create an image that is the result of stacking two images on top of each other.
	 * @param imageA The first image.
	 * @param imageB The second image. Will be stacked on top of the first one.
	 * @param allowImageResize Whether or not to allow imageB being resized to match imageA size.
	 * @param alignment Alignment to be used when stacking imageB onto imageA.
	 * @return The resulting image.
	 * <!-- end-model-doc -->
	 * @model unique="false" imageAUnique="false" imageBUnique="false" allowImageResizeUnique="false" alignmentUnique="false"
	 * @generated
	 */
	AbstractEImage superPoseImages(AbstractEImage imageA, AbstractEImage imageB, boolean allowImageResize, ImageAlignment alignment);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates an image that is a copy of the original with a border drawn on the inside of its perimeter (i.e. the image size is not changed).
	 * @param originalImage The original image.
	 * @param borderWidth The width of the border, in pixel.
	 * @param red The red component of the border color, between 0 and 255.
	 * @param green The green component of the border color, between 0 and 255.
	 * @param blue The blue component of the border color, between 0 and 255.
	 * @return The resulting image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" borderWidthUnique="false"
	 *        borderWidthAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'" redUnique="false" greenUnique="false" blueUnique="false"
	 * @generated
	 */
	AbstractEImage addBorder(AbstractEImage originalImage, int borderWidth, int red, int green, int blue);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Gets a sub image from a specified image. Note that the widthOffset and heightOffset are relative to the upper left corner of the image.
	 * @param originalImage The original image.
	 * @param widthOffset The sub-image offset along the width of the original, in pixels.
	 * @param heightOffset The sub-image offset along the height of the original, in pixels.
	 * @param subImageWidth The width of the sub-image, in pixels.
	 * @param subImageHeight The height of the sub-image, in pixels.
	 * @return The sub image.
	 * @throws An exception if the offsets of the sub-image does not fall inside the original.
	 * <!-- end-model-doc -->
	 * @model unique="false" exceptions="ca.gc.asc_csa.apogy.common.images.Exception" originalImageUnique="false" widthOffsetUnique="false"
	 *        widthOffsetAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'" heightOffsetUnique="false"
	 *        heightOffsetAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'" subImageWidthUnique="false"
	 *        subImageWidthAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'" subImageHeightUnique="false"
	 *        subImageHeightAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'"
	 * @generated
	 */
	AbstractEImage getSubImage(AbstractEImage originalImage, int widthOffset, int heightOffset, int subImageWidth, int subImageHeight) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Converts an ImageData to a BufferedImage.
	 * @param imageData The ImageData to convert.
	 * @return The BufferedImage.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.images.BufferedImage" unique="false" imageDataDataType="ca.gc.asc_csa.apogy.common.images.ImageData" imageDataUnique="false"
	 * @generated
	 */
	BufferedImage convertToBufferedImage(ImageData imageData);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Converts an BufferedImage to an ImageData.
	 * @param bufferedImage The BufferedImage to convert.
	 * @return The ImageData.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.images.ImageData" unique="false" bufferedImageDataType="ca.gc.asc_csa.apogy.common.images.BufferedImage" bufferedImageUnique="false"
	 * @generated
	 */
	ImageData convertToImageData(BufferedImage bufferedImage);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates an image of uniform color of specified dimensions.
	 * @param width Width of the image, in pixels.
	 * @param height Height of the image, in pixels.
	 * @param red The red component of the image color, between 0 and 255.
	 * @param green The green component of the image color, between 0 and 255.
	 * @param blue The blue component of the image color, between 0 and 255.
	 * @param alpha The alpha component of the image color, between 0 (transparent) and 255 (opaque).
	 * <!-- end-model-doc -->
	 * @model unique="false" widthUnique="false"
	 *        widthAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'" heightUnique="false"
	 *        heightAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'" redUnique="false" greenUnique="false" blueUnique="false" alphaUnique="false"
	 * @generated
	 */
	AbstractEImage createUniformColorImage(int width, int height, int red, int green, int blue, int alpha);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates an image which is a copy of the original where the alpha component of the color of each pixel is set to a specified value.
	 * @param originalImage The original image.
	 * @param alpha The alpha component, from 0.0 (transparent) to 1.0 (opaque).
	 * @return The resulting image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" alphaUnique="false"
	 * @generated
	 */
	AbstractEImage applyAlpha(AbstractEImage originalImage, float alpha);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create an image which is a copy of the original on which an edge filter has been applied.
	 * @param originalImage The original image.
	 * @return The filtered image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false"
	 * @generated
	 */
	AbstractEImage applyEdgeFilter(AbstractEImage originalImage);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create an image which is a copy of the original on which an contrast and brightness filter has been applied.
	 * @param originalImage The original image.
	 * @param contrast The contrast gain.
	 * @param brightness The brightness gain.
	 * @return The filtered image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" contrastUnique="false" brightnessUnique="false"
	 * @generated
	 */
	AbstractEImage applyContrastAndBrightnessFilter(AbstractEImage originalImage, double contrast, double brightness);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create an image which is a copy of the original on which an exposure filter has been applied.
	 * @param originalImage The original image.
	 * @param exposure The exposure gain.
	 * @return The filtered image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" exposureUnique="false"
	 * @generated
	 */
	AbstractEImage applyExposureFilter(AbstractEImage originalImage, double exposure);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create an image which is a copy of the original on which an color inverting filter has been applied.
	 * @param originalImage The original image.
	 * @return The filtered image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false"
	 * @generated
	 */
	AbstractEImage applyInvertFilter(AbstractEImage originalImage);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create an image which is a copy of the original on which an rescaling of intensity filter has been applied.
	 * @param originalImage The original image.
	 * @param scale The scaling gain.
	 * @return The filtered image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" scaleUnique="false"
	 * @generated
	 */
	AbstractEImage applyRescaleFilter(AbstractEImage originalImage, double scale);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Create an image which is a copy of the original on which an gain of intensity filter has been applied.
	 * @param originalImage The original image.
	 * @param gain The intensity gain.
	 * @param bias Bias of the intensity.
	 * @return The filtered image.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalImageUnique="false" gainUnique="false" biasUnique="false"
	 * @generated
	 */
	AbstractEImage applyGainFilter(AbstractEImage originalImage, double gain, double bias);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates an image of uniform color onto which text is rendered.
	 * @param text The text to render.
	 * @param font The font to use.
	 * @param textColor The color of the text.
	 * @param backgroundColor The color of the image background.
	 * @param borderWidth The width left empty (background color) around the text, in pixel.
	 * @return The resulting image.
	 * <!-- end-model-doc -->
	 * @model unique="false" textUnique="false" fontDataType="ca.gc.asc_csa.apogy.common.images.Font" fontUnique="false" textColorDataType="ca.gc.asc_csa.apogy.common.images.Color" textColorUnique="false" backgroundColorDataType="ca.gc.asc_csa.apogy.common.images.Color" backgroundColorUnique="false" borderWidthUnique="false"
	 *        borderWidthAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='pixel'"
	 * @generated
	 */
	AbstractEImage createTextImage(String text, Font font, Color textColor, Color backgroundColor, int borderWidth);

} // EImagesUtilities

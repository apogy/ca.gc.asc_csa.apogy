// *****************************************************************************
// Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
//     Pierre Allard - initial API and implementation
//        
// SPDX-License-Identifier: EPL-1.0
// *****************************************************************************
@GenModel(prefix="ApogyEarthAtmosphereEnvironment",
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  multipleEditorPages="false",
          copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Pierre Allard - initial API and implementation
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",
		  modelName="ApogyEarthAtmosphereEnvironment",
		  complianceLevel="6.0",
		  suppressGenModelAnnotations="false",
		  dynamicTemplates="true", 
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.core.environment.earth.atmosphere/src-generated")
@GenModel(editDirectory= "/ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.edit/src-generated")
//@GenModel(testsDirectory="/ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.tests/src-generated")

package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere

import ca.gc.asc_csa.apogy.common.topology.TransformNode
import ca.gc.asc_csa.apogy.core.environment.WorksiteNode
import ca.gc.asc_csa.apogy.core.environment.earth.EarthWorksite
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky

/**
 * Defines a worksite above the surface, in the atmosphere, below 100km altitude.
 */
class EarthAtmosphereWorksite extends EarthWorksite
{	
	/*
	 * The EarthSky associated with the worksite,
	 */
	refers derived transient volatile EarthSky[1] earthSky	
}

/*
 * SurfaceWorksiteNode specialized for the Earth Surface.
 */
class EarthAtmosphereWorksiteNode extends WorksiteNode
{
	refers transient TransformNode skyTransformNode
}


class ApogyEarthAtmosphereFacade
{
	/*
	 * Refers to the active EarthSurfaceWorksite. May be null.
	 */	
	refers transient EarthAtmosphereWorksite activeEarthAtmosphereWorksite
	
	/*
	 * Create an empty EarthAtmosphereWorksite with the CSA Mars Yard coordinates.
	 */
	op EarthAtmosphereWorksite createAndInitializeDefaultCSAEarthAtmosphereWorksite()
	
}
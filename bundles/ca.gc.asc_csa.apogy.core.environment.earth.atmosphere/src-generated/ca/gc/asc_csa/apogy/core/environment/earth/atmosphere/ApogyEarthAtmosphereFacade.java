/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere;

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apogy Earth Atmosphere Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereFacade#getActiveEarthAtmosphereWorksite <em>Active Earth Atmosphere Worksite</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage#getApogyEarthAtmosphereFacade()
 * @model
 * @generated
 */
public interface ApogyEarthAtmosphereFacade extends EObject 
{
	public static ApogyEarthAtmosphereFacade INSTANCE = ApogyEarthAtmosphereFacadeImpl.getInstance();


	/**
	 * Returns the value of the '<em><b>Active Earth Atmosphere Worksite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Refers to the active EarthSurfaceWorksite. May be null.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Active Earth Atmosphere Worksite</em>' reference.
	 * @see #setActiveEarthAtmosphereWorksite(EarthAtmosphereWorksite)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage#getApogyEarthAtmosphereFacade_ActiveEarthAtmosphereWorksite()
	 * @model transient="true"
	 * @generated
	 */
	EarthAtmosphereWorksite getActiveEarthAtmosphereWorksite();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereFacade#getActiveEarthAtmosphereWorksite <em>Active Earth Atmosphere Worksite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active Earth Atmosphere Worksite</em>' reference.
	 * @see #getActiveEarthAtmosphereWorksite()
	 * @generated
	 */
	void setActiveEarthAtmosphereWorksite(EarthAtmosphereWorksite value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Create an empty EarthAtmosphereWorksite with the CSA Mars Yard coordinates.
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 * @generated
	 */
	EarthAtmosphereWorksite createAndInitializeDefaultCSAEarthAtmosphereWorksite();
} // ApogyEarthAtmosphereFacade

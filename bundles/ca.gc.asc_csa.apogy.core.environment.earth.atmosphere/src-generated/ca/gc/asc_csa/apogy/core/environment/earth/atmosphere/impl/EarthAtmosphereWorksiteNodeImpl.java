/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFactory;
import ca.gc.asc_csa.apogy.common.topology.TransformNode;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode;
import ca.gc.asc_csa.apogy.core.environment.impl.WorksiteNodeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Atmosphere Worksite Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.EarthAtmosphereWorksiteNodeImpl#getSkyTransformNode <em>Sky Transform Node</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EarthAtmosphereWorksiteNodeImpl extends WorksiteNodeImpl implements EarthAtmosphereWorksiteNode {
	/**
	 * The cached value of the '{@link #getSkyTransformNode() <em>Sky Transform Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSkyTransformNode()
	 * @generated
	 * @ordered
	 */
	protected TransformNode skyTransformNode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EarthAtmosphereWorksiteNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthAtmosphereEnvironmentPackage.Literals.EARTH_ATMOSPHERE_WORKSITE_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public TransformNode getSkyTransformNode() 
	{
		TransformNode tmp =  getSkyTransformNodeGen();
		
		if(tmp == null)
		{
			tmp = ApogyCommonTopologyFactory.eINSTANCE.createTransformNode();			 
			tmp.setTransformation(ApogyCommonMathFacade.INSTANCE.createIdentityMatrix4x4().asMatrix4d());			 
			tmp.setNodeId("SKY_TRANSFORM");			
			getChildren().add(tmp);
			
			setSkyTransformNode(tmp);
		}
		
		return tmp;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformNode getSkyTransformNodeGen() {
		if (skyTransformNode != null && skyTransformNode.eIsProxy()) {
			InternalEObject oldSkyTransformNode = (InternalEObject)skyTransformNode;
			skyTransformNode = (TransformNode)eResolveProxy(oldSkyTransformNode);
			if (skyTransformNode != oldSkyTransformNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE_NODE__SKY_TRANSFORM_NODE, oldSkyTransformNode, skyTransformNode));
			}
		}
		return skyTransformNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformNode basicGetSkyTransformNode() {
		return skyTransformNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSkyTransformNode(TransformNode newSkyTransformNode) {
		TransformNode oldSkyTransformNode = skyTransformNode;
		skyTransformNode = newSkyTransformNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE_NODE__SKY_TRANSFORM_NODE, oldSkyTransformNode, skyTransformNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE_NODE__SKY_TRANSFORM_NODE:
				if (resolve) return getSkyTransformNode();
				return basicGetSkyTransformNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE_NODE__SKY_TRANSFORM_NODE:
				setSkyTransformNode((TransformNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE_NODE__SKY_TRANSFORM_NODE:
				setSkyTransformNode((TransformNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE_NODE__SKY_TRANSFORM_NODE:
				return skyTransformNode != null;
		}
		return super.eIsSet(featureID);
	}

} //EarthAtmosphereWorksiteNodeImpl

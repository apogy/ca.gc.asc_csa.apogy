/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl;

import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyEarthAtmosphereEnvironmentFactoryImpl extends EFactoryImpl implements ApogyEarthAtmosphereEnvironmentFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyEarthAtmosphereEnvironmentFactory init() {
		try {
			ApogyEarthAtmosphereEnvironmentFactory theApogyEarthAtmosphereEnvironmentFactory = (ApogyEarthAtmosphereEnvironmentFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyEarthAtmosphereEnvironmentPackage.eNS_URI);
			if (theApogyEarthAtmosphereEnvironmentFactory != null) {
				return theApogyEarthAtmosphereEnvironmentFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyEarthAtmosphereEnvironmentFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthAtmosphereEnvironmentFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE: return createEarthAtmosphereWorksite();
			case ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE_NODE: return createEarthAtmosphereWorksiteNode();
			case ApogyEarthAtmosphereEnvironmentPackage.APOGY_EARTH_ATMOSPHERE_FACADE: return createApogyEarthAtmosphereFacade();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthAtmosphereWorksite createEarthAtmosphereWorksite() {
		EarthAtmosphereWorksiteImpl earthAtmosphereWorksite = new EarthAtmosphereWorksiteImpl();
		return earthAtmosphereWorksite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthAtmosphereWorksiteNode createEarthAtmosphereWorksiteNode() {
		EarthAtmosphereWorksiteNodeImpl earthAtmosphereWorksiteNode = new EarthAtmosphereWorksiteNodeImpl();
		return earthAtmosphereWorksiteNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthAtmosphereFacade createApogyEarthAtmosphereFacade() {
		ApogyEarthAtmosphereFacadeImpl apogyEarthAtmosphereFacade = new ApogyEarthAtmosphereFacadeImpl();
		return apogyEarthAtmosphereFacade;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthAtmosphereEnvironmentPackage getApogyEarthAtmosphereEnvironmentPackage() {
		return (ApogyEarthAtmosphereEnvironmentPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyEarthAtmosphereEnvironmentPackage getPackage() {
		return ApogyEarthAtmosphereEnvironmentPackage.eINSTANCE;
	}

} //ApogyEarthAtmosphereEnvironmentFactoryImpl

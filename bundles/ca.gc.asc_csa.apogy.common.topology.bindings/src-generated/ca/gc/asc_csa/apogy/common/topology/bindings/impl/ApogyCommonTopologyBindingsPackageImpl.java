package ca.gc.asc_csa.apogy.common.topology.bindings.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.Axis;
import ca.gc.asc_csa.apogy.common.topology.bindings.BindingsList;
import ca.gc.asc_csa.apogy.common.topology.bindings.BindingsSet;
import ca.gc.asc_csa.apogy.common.topology.bindings.BooleanBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.BooleanCase;
import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationCase;
import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationSwitchBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.FalseBooleanCase;
import ca.gc.asc_csa.apogy.common.topology.bindings.FeatureRootsList;
import ca.gc.asc_csa.apogy.common.topology.bindings.RotationBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsFacade;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsFactory;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.TransformMatrixBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.TranslationBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.TrueBooleanCase;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc --> * @generated
 */
public class ApogyCommonTopologyBindingsPackageImpl extends EPackageImpl implements ApogyCommonTopologyBindingsPackage
{
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EClass abstractTopologyBindingEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EClass rotationBindingEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EClass translationBindingEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EClass transformMatrixBindingEClass = null;

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass booleanBindingEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass booleanCaseEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass trueBooleanCaseEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass falseBooleanCaseEClass = null;

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EClass enumerationSwitchBindingEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EClass enumerationCaseEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EClass bindingsListEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EClass featureRootsListEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EClass bindingsSetEClass = null;

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass apogyCommonTopologyBindingsFacadeEClass = null;

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EEnum axisEEnum = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EDataType mapEDataType = null;

  /**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
  private ApogyCommonTopologyBindingsPackageImpl()
  {
		super(eNS_URI, ApogyCommonTopologyBindingsFactory.eINSTANCE);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private static boolean isInited = false;

  /**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCommonTopologyBindingsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
  public static ApogyCommonTopologyBindingsPackage init()
  {
		if (isInited) return (ApogyCommonTopologyBindingsPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyBindingsPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCommonTopologyBindingsPackageImpl theApogyCommonTopologyBindingsPackage = (ApogyCommonTopologyBindingsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCommonTopologyBindingsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCommonTopologyBindingsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCommonTopologyPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCommonTopologyBindingsPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCommonTopologyBindingsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCommonTopologyBindingsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCommonTopologyBindingsPackage.eNS_URI, theApogyCommonTopologyBindingsPackage);
		return theApogyCommonTopologyBindingsPackage;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EClass getAbstractTopologyBinding()
  {
		return abstractTopologyBindingEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EAttribute getAbstractTopologyBinding_Binded()
  {
		return (EAttribute)abstractTopologyBindingEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EAttribute getAbstractTopologyBinding_Description()
  {
		return (EAttribute)abstractTopologyBindingEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EAttribute getAbstractTopologyBinding_Name()
  {
		return (EAttribute)abstractTopologyBindingEClass.getEStructuralFeatures().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getAbstractTopologyBinding_Source()
  {
		return (EReference)abstractTopologyBindingEClass.getEStructuralFeatures().get(3);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getAbstractTopologyBinding_FeatureNode()
  {
		return (EReference)abstractTopologyBindingEClass.getEStructuralFeatures().get(4);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getAbstractTopologyBinding_SupportedFeatureType() {
		return (EAttribute)abstractTopologyBindingEClass.getEStructuralFeatures().get(5);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getAbstractTopologyBinding_FeatureNodeAdapter()
  {
		return (EReference)abstractTopologyBindingEClass.getEStructuralFeatures().get(6);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EOperation getAbstractTopologyBinding__Bind()
  {
		return abstractTopologyBindingEClass.getEOperations().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EOperation getAbstractTopologyBinding__Unbind()
  {
		return abstractTopologyBindingEClass.getEOperations().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EOperation getAbstractTopologyBinding__Clone__Map()
  {
		return abstractTopologyBindingEClass.getEOperations().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EClass getRotationBinding()
  {
		return rotationBindingEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getRotationBinding_RotationNode()
  {
		return (EReference)rotationBindingEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EAttribute getRotationBinding_RotationAxis()
  {
		return (EAttribute)rotationBindingEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EClass getTranslationBinding()
  {
		return translationBindingEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EAttribute getTranslationBinding_TranslationAxis()
  {
		return (EAttribute)translationBindingEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getTranslationBinding_PositionNode()
  {
		return (EReference)translationBindingEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EClass getTransformMatrixBinding()
  {
		return transformMatrixBindingEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getTransformMatrixBinding_TransformNode()
  {
		return (EReference)transformMatrixBindingEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getBooleanBinding() {
		return booleanBindingEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getBooleanBinding_CurrentValue() {
		return (EAttribute)booleanBindingEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getBooleanBinding_ParentNode() {
		return (EReference)booleanBindingEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getBooleanBinding_TrueCase() {
		return (EReference)booleanBindingEClass.getEStructuralFeatures().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getBooleanBinding_FalseCase() {
		return (EReference)booleanBindingEClass.getEStructuralFeatures().get(3);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getBooleanCase() {
		return booleanCaseEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getBooleanCase_TopologyRoot() {
		return (EReference)booleanCaseEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getTrueBooleanCase() {
		return trueBooleanCaseEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getFalseBooleanCase() {
		return falseBooleanCaseEClass;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EClass getEnumerationSwitchBinding()
  {
		return enumerationSwitchBindingEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getEnumerationSwitchBinding_ParentNode()
  {
		return (EReference)enumerationSwitchBindingEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getEnumerationSwitchBinding_Cases()
  {
		return (EReference)enumerationSwitchBindingEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getEnumerationSwitchBinding_ActiveCase()
  {
		return (EReference)enumerationSwitchBindingEClass.getEStructuralFeatures().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EClass getEnumerationCase()
  {
		return enumerationCaseEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getEnumerationCase_EnumerationLiterals()
  {
		return (EReference)enumerationCaseEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getEnumerationCase_TopologyRoot()
  {
		return (EReference)enumerationCaseEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EClass getBindingsList()
  {
		return bindingsListEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getBindingsList_Bindings()
  {
		return (EReference)bindingsListEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EClass getFeatureRootsList()
  {
		return featureRootsListEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getFeatureRootsList_FeatureRoots()
  {
		return (EReference)featureRootsListEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EClass getBindingsSet()
  {
		return bindingsSetEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getBindingsSet_FeatureRootsList()
  {
		return (EReference)bindingsSetEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EReference getBindingsSet_BindingsList()
  {
		return (EReference)bindingsSetEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EOperation getBindingsSet__Bind()
  {
		return bindingsSetEClass.getEOperations().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EOperation getBindingsSet__Unbind()
  {
		return bindingsSetEClass.getEOperations().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getApogyCommonTopologyBindingsFacade() {
		return apogyCommonTopologyBindingsFacadeEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyBindingsFacade__Copy__AbstractTopologyBinding_Map() {
		return apogyCommonTopologyBindingsFacadeEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EEnum getAxis() {
		return axisEEnum;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EDataType getMap()
  {
		return mapEDataType;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCommonTopologyBindingsFactory getApogyCommonTopologyBindingsFactory() {
		return (ApogyCommonTopologyBindingsFactory)getEFactoryInstance();
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private boolean isCreated = false;

  /**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public void createPackageContents()
  {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		abstractTopologyBindingEClass = createEClass(ABSTRACT_TOPOLOGY_BINDING);
		createEAttribute(abstractTopologyBindingEClass, ABSTRACT_TOPOLOGY_BINDING__BINDED);
		createEAttribute(abstractTopologyBindingEClass, ABSTRACT_TOPOLOGY_BINDING__DESCRIPTION);
		createEAttribute(abstractTopologyBindingEClass, ABSTRACT_TOPOLOGY_BINDING__NAME);
		createEReference(abstractTopologyBindingEClass, ABSTRACT_TOPOLOGY_BINDING__SOURCE);
		createEReference(abstractTopologyBindingEClass, ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE);
		createEAttribute(abstractTopologyBindingEClass, ABSTRACT_TOPOLOGY_BINDING__SUPPORTED_FEATURE_TYPE);
		createEReference(abstractTopologyBindingEClass, ABSTRACT_TOPOLOGY_BINDING__FEATURE_NODE_ADAPTER);
		createEOperation(abstractTopologyBindingEClass, ABSTRACT_TOPOLOGY_BINDING___BIND);
		createEOperation(abstractTopologyBindingEClass, ABSTRACT_TOPOLOGY_BINDING___UNBIND);
		createEOperation(abstractTopologyBindingEClass, ABSTRACT_TOPOLOGY_BINDING___CLONE__MAP);

		rotationBindingEClass = createEClass(ROTATION_BINDING);
		createEReference(rotationBindingEClass, ROTATION_BINDING__ROTATION_NODE);
		createEAttribute(rotationBindingEClass, ROTATION_BINDING__ROTATION_AXIS);

		translationBindingEClass = createEClass(TRANSLATION_BINDING);
		createEReference(translationBindingEClass, TRANSLATION_BINDING__POSITION_NODE);
		createEAttribute(translationBindingEClass, TRANSLATION_BINDING__TRANSLATION_AXIS);

		transformMatrixBindingEClass = createEClass(TRANSFORM_MATRIX_BINDING);
		createEReference(transformMatrixBindingEClass, TRANSFORM_MATRIX_BINDING__TRANSFORM_NODE);

		booleanBindingEClass = createEClass(BOOLEAN_BINDING);
		createEAttribute(booleanBindingEClass, BOOLEAN_BINDING__CURRENT_VALUE);
		createEReference(booleanBindingEClass, BOOLEAN_BINDING__PARENT_NODE);
		createEReference(booleanBindingEClass, BOOLEAN_BINDING__TRUE_CASE);
		createEReference(booleanBindingEClass, BOOLEAN_BINDING__FALSE_CASE);

		booleanCaseEClass = createEClass(BOOLEAN_CASE);
		createEReference(booleanCaseEClass, BOOLEAN_CASE__TOPOLOGY_ROOT);

		trueBooleanCaseEClass = createEClass(TRUE_BOOLEAN_CASE);

		falseBooleanCaseEClass = createEClass(FALSE_BOOLEAN_CASE);

		enumerationSwitchBindingEClass = createEClass(ENUMERATION_SWITCH_BINDING);
		createEReference(enumerationSwitchBindingEClass, ENUMERATION_SWITCH_BINDING__PARENT_NODE);
		createEReference(enumerationSwitchBindingEClass, ENUMERATION_SWITCH_BINDING__CASES);
		createEReference(enumerationSwitchBindingEClass, ENUMERATION_SWITCH_BINDING__ACTIVE_CASE);

		enumerationCaseEClass = createEClass(ENUMERATION_CASE);
		createEReference(enumerationCaseEClass, ENUMERATION_CASE__ENUMERATION_LITERALS);
		createEReference(enumerationCaseEClass, ENUMERATION_CASE__TOPOLOGY_ROOT);

		bindingsListEClass = createEClass(BINDINGS_LIST);
		createEReference(bindingsListEClass, BINDINGS_LIST__BINDINGS);

		featureRootsListEClass = createEClass(FEATURE_ROOTS_LIST);
		createEReference(featureRootsListEClass, FEATURE_ROOTS_LIST__FEATURE_ROOTS);

		bindingsSetEClass = createEClass(BINDINGS_SET);
		createEReference(bindingsSetEClass, BINDINGS_SET__FEATURE_ROOTS_LIST);
		createEReference(bindingsSetEClass, BINDINGS_SET__BINDINGS_LIST);
		createEOperation(bindingsSetEClass, BINDINGS_SET___BIND);
		createEOperation(bindingsSetEClass, BINDINGS_SET___UNBIND);

		apogyCommonTopologyBindingsFacadeEClass = createEClass(APOGY_COMMON_TOPOLOGY_BINDINGS_FACADE);
		createEOperation(apogyCommonTopologyBindingsFacadeEClass, APOGY_COMMON_TOPOLOGY_BINDINGS_FACADE___COPY__ABSTRACTTOPOLOGYBINDING_MAP);

		// Create enums
		axisEEnum = createEEnum(AXIS);

		// Create data types
		mapEDataType = createEDataType(MAP);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private boolean isInitialized = false;

  /**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public void initializePackageContents()
  {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogyCommonEMFPackage theApogyCommonEMFPackage = (ApogyCommonEMFPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFPackage.eNS_URI);
		ApogyCommonTopologyPackage theApogyCommonTopologyPackage = (ApogyCommonTopologyPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyPackage.eNS_URI);

		// Create type parameters
		addETypeParameter(mapEDataType, "K");
		addETypeParameter(mapEDataType, "V");

		// Set bounds for type parameters

		// Add supertypes to classes
		rotationBindingEClass.getESuperTypes().add(this.getAbstractTopologyBinding());
		translationBindingEClass.getESuperTypes().add(this.getAbstractTopologyBinding());
		transformMatrixBindingEClass.getESuperTypes().add(this.getAbstractTopologyBinding());
		booleanBindingEClass.getESuperTypes().add(this.getAbstractTopologyBinding());
		trueBooleanCaseEClass.getESuperTypes().add(this.getBooleanCase());
		falseBooleanCaseEClass.getESuperTypes().add(this.getBooleanCase());
		enumerationSwitchBindingEClass.getESuperTypes().add(this.getAbstractTopologyBinding());

		// Initialize classes, features, and operations; add parameters
		initEClass(abstractTopologyBindingEClass, AbstractTopologyBinding.class, "AbstractTopologyBinding", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractTopologyBinding_Binded(), theEcorePackage.getEBoolean(), "binded", null, 0, 1, AbstractTopologyBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractTopologyBinding_Description(), theEcorePackage.getEString(), "description", null, 0, 1, AbstractTopologyBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractTopologyBinding_Name(), theEcorePackage.getEString(), "name", null, 0, 1, AbstractTopologyBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractTopologyBinding_Source(), theEcorePackage.getEObject(), null, "source", null, 0, 1, AbstractTopologyBinding.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractTopologyBinding_FeatureNode(), theApogyCommonEMFPackage.getAbstractFeatureNode(), null, "featureNode", null, 0, 1, AbstractTopologyBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(theEcorePackage.getEJavaClass());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getAbstractTopologyBinding_SupportedFeatureType(), g1, "supportedFeatureType", null, 0, 1, AbstractTopologyBinding.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractTopologyBinding_FeatureNodeAdapter(), theApogyCommonEMFPackage.getFeatureNodeAdapter(), null, "featureNodeAdapter", null, 0, 1, AbstractTopologyBinding.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getAbstractTopologyBinding__Bind(), null, "bind", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getAbstractTopologyBinding__Unbind(), null, "unbind", 0, 1, !IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getAbstractTopologyBinding__Clone__Map(), this.getAbstractTopologyBinding(), "clone", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getMap());
		g2 = createEGenericType(theApogyCommonTopologyPackage.getNode());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theApogyCommonTopologyPackage.getNode());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "originalToCopyNodeMap", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(rotationBindingEClass, RotationBinding.class, "RotationBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRotationBinding_RotationNode(), theApogyCommonTopologyPackage.getRotationNode(), null, "rotationNode", null, 0, 1, RotationBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRotationBinding_RotationAxis(), this.getAxis(), "rotationAxis", "X_AXIS", 0, 1, RotationBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(translationBindingEClass, TranslationBinding.class, "TranslationBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTranslationBinding_PositionNode(), theApogyCommonTopologyPackage.getPositionNode(), null, "positionNode", null, 0, 1, TranslationBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTranslationBinding_TranslationAxis(), this.getAxis(), "translationAxis", "X_AXIS", 0, 1, TranslationBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(transformMatrixBindingEClass, TransformMatrixBinding.class, "TransformMatrixBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransformMatrixBinding_TransformNode(), theApogyCommonTopologyPackage.getTransformNode(), null, "transformNode", null, 0, 1, TransformMatrixBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanBindingEClass, BooleanBinding.class, "BooleanBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanBinding_CurrentValue(), theEcorePackage.getEBoolean(), "currentValue", null, 0, 1, BooleanBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBooleanBinding_ParentNode(), theApogyCommonTopologyPackage.getGroupNode(), null, "parentNode", null, 1, 1, BooleanBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBooleanBinding_TrueCase(), this.getTrueBooleanCase(), null, "trueCase", null, 0, 1, BooleanBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBooleanBinding_FalseCase(), this.getFalseBooleanCase(), null, "falseCase", null, 0, 1, BooleanBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanCaseEClass, BooleanCase.class, "BooleanCase", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBooleanCase_TopologyRoot(), theApogyCommonTopologyPackage.getAggregateGroupNode(), null, "topologyRoot", null, 0, 1, BooleanCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(trueBooleanCaseEClass, TrueBooleanCase.class, "TrueBooleanCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(falseBooleanCaseEClass, FalseBooleanCase.class, "FalseBooleanCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(enumerationSwitchBindingEClass, EnumerationSwitchBinding.class, "EnumerationSwitchBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnumerationSwitchBinding_ParentNode(), theApogyCommonTopologyPackage.getGroupNode(), null, "parentNode", null, 1, 1, EnumerationSwitchBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnumerationSwitchBinding_Cases(), this.getEnumerationCase(), null, "cases", null, 1, -1, EnumerationSwitchBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnumerationSwitchBinding_ActiveCase(), this.getEnumerationCase(), null, "activeCase", null, 0, 1, EnumerationSwitchBinding.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(enumerationCaseEClass, EnumerationCase.class, "EnumerationCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnumerationCase_EnumerationLiterals(), theEcorePackage.getEEnumLiteral(), null, "enumerationLiterals", null, 1, -1, EnumerationCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnumerationCase_TopologyRoot(), theApogyCommonTopologyPackage.getNode(), null, "topologyRoot", null, 0, 1, EnumerationCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bindingsListEClass, BindingsList.class, "BindingsList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBindingsList_Bindings(), this.getAbstractTopologyBinding(), null, "bindings", null, 0, -1, BindingsList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureRootsListEClass, FeatureRootsList.class, "FeatureRootsList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeatureRootsList_FeatureRoots(), theApogyCommonEMFPackage.getTreeRootNode(), null, "featureRoots", null, 0, -1, FeatureRootsList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bindingsSetEClass, BindingsSet.class, "BindingsSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBindingsSet_FeatureRootsList(), this.getFeatureRootsList(), null, "featureRootsList", null, 1, 1, BindingsSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBindingsSet_BindingsList(), this.getBindingsList(), null, "bindingsList", null, 1, 1, BindingsSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getBindingsSet__Bind(), null, "bind", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getBindingsSet__Unbind(), null, "unbind", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(apogyCommonTopologyBindingsFacadeEClass, ApogyCommonTopologyBindingsFacade.class, "ApogyCommonTopologyBindingsFacade", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getApogyCommonTopologyBindingsFacade__Copy__AbstractTopologyBinding_Map(), this.getAbstractTopologyBinding(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAbstractTopologyBinding(), "originalBinding", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getMap());
		g2 = createEGenericType(theApogyCommonTopologyPackage.getNode());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theApogyCommonTopologyPackage.getNode());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "originalToCopyNodeMap", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(axisEEnum, Axis.class, "Axis");
		addEEnumLiteral(axisEEnum, Axis.XAXIS);
		addEEnumLiteral(axisEEnum, Axis.YAXIS);
		addEEnumLiteral(axisEEnum, Axis.ZAXIS);
		addEEnumLiteral(axisEEnum, Axis.MINUS_XAXIS);
		addEEnumLiteral(axisEEnum, Axis.MINUS_YAXIS);
		addEEnumLiteral(axisEEnum, Axis.MINUS_ZAXIS);

		// Initialize data types
		initEDataType(mapEDataType, Map.class, "Map", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

		/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "prefix", "ApogyCommonTopologyBindings",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque \n     Sebastien Gemme\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyCommonTopologyBindings",
			 "suppressGenModelAnnotations", "false",
			 "complianceLevel", "6.0",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.common.topology.bindings/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.common.topology.bindings.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.common.topology"
		   });	
		addAnnotation
		  (axisEEnum, 
		   source, 
		   new String[] {
			 "documentation", "*\nCartesian Axis enumeration."
		   });	
		addAnnotation
		  (abstractTopologyBindingEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAbstract base class representing a binding relationship between a topology and a feature."
		   });	
		addAnnotation
		  (getAbstractTopologyBinding__Bind(), 
		   source, 
		   new String[] {
			 "documentation", "*\nBinds the binding to its feature. If successful, binded will change to true."
		   });	
		addAnnotation
		  (getAbstractTopologyBinding__Unbind(), 
		   source, 
		   new String[] {
			 "documentation", "*\nUn-binds the binding from its feature. If successful, binded will change to false."
		   });	
		addAnnotation
		  (getAbstractTopologyBinding__Clone__Map(), 
		   source, 
		   new String[] {
			 "documentation", "*\nClones the current Binding. This method must copy this Binding parameters and set\nthe Binding copy associated Node(s) to the Node(s) copy(ies) associated with\nthis Binding.\n@param originalToCopyNodeMap Maps original Nodes to copied ones.\n@return A copy of this Binding."
		   });	
		addAnnotation
		  (getAbstractTopologyBinding_Binded(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not the binding is bound.",
			 "property", "None",
			 "propertyCategory", "INFORMATION"
		   });	
		addAnnotation
		  (getAbstractTopologyBinding_Description(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe description of the binding.",
			 "propertyCategory", "INFORMATION"
		   });	
		addAnnotation
		  (getAbstractTopologyBinding_Name(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe name of the binding.",
			 "propertyCategory", "INFORMATION"
		   });	
		addAnnotation
		  (getAbstractTopologyBinding_Source(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe source object onto which the feature must be resolved.",
			 "property", "None",
			 "propertyCategory", "SOURCE"
		   });	
		addAnnotation
		  (getAbstractTopologyBinding_FeatureNode(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe feature the binding is associated with.",
			 "propertyCategory", "SOURCE"
		   });	
		addAnnotation
		  (getAbstractTopologyBinding_SupportedFeatureType(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe type referred by the featureNode that this binding supports.",
			 "property", "None"
		   });	
		addAnnotation
		  (getAbstractTopologyBinding_FeatureNodeAdapter(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe adapter used to listens for changes of the feature value.",
			 "property", "None"
		   });	
		addAnnotation
		  (rotationBindingEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBinding that binds a RotationNode rotation around one axis to the feature value."
		   });	
		addAnnotation
		  (getRotationBinding_RotationNode(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe RotationNode that will be updated.",
			 "propertyCategory", "DESTINATION"
		   });	
		addAnnotation
		  (getRotationBinding_RotationAxis(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe axis around which to revolve.",
			 "propertyCategory", "DESTINATION"
		   });	
		addAnnotation
		  (translationBindingEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBinding that binds a PositionNode translation along one axis to the feature value."
		   });	
		addAnnotation
		  (getTranslationBinding_PositionNode(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe PositionNode that will be updated.",
			 "propertyCategory", "DESTINATION"
		   });	
		addAnnotation
		  (getTranslationBinding_TranslationAxis(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe axis along which the translation occurs.",
			 "propertyCategory", "DESTINATION"
		   });	
		addAnnotation
		  (transformMatrixBindingEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBinding that binds a TransformNode transform to a feature value.\nThe feature value must be of type Matrix4x4."
		   });	
		addAnnotation
		  (getTransformMatrixBinding_TransformNode(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe TransformNode that will be updated.",
			 "propertyCategory", "DESTINATION"
		   });	
		addAnnotation
		  (booleanBindingEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBinding that attaches two different sub-topology to a GroupNode based\non the feature value. The feature must be a Boolean."
		   });	
		addAnnotation
		  (getBooleanBinding_ParentNode(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe parent node under which to attach the sub-topology.",
			 "propertyCategory", "DESTINATION"
		   });	
		addAnnotation
		  (getBooleanBinding_TrueCase(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe true case.",
			 "children", "true"
		   });	
		addAnnotation
		  (getBooleanBinding_FalseCase(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe false case.",
			 "children", "true"
		   });	
		addAnnotation
		  (booleanCaseEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBoolean cases used in the BooleanBinding."
		   });	
		addAnnotation
		  (getBooleanCase_TopologyRoot(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe root of the topology to add to the parent Node\nwhen this case is activated.",
			 "children", "true"
		   });	
		addAnnotation
		  (trueBooleanCaseEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nTrue case for BooleanBinding."
		   });	
		addAnnotation
		  (falseBooleanCaseEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nFalse case for BooleanBinding."
		   });	
		addAnnotation
		  (enumerationSwitchBindingEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBinding that attached different sub-topology to a GroupNode based\non the feature value. The feature must be an EEnum."
		   });	
		addAnnotation
		  (getEnumerationSwitchBinding_ParentNode(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe parent node under which the switch should attached sub-topology.",
			 "propertyCategory", "DESTINATION"
		   });	
		addAnnotation
		  (getEnumerationSwitchBinding_Cases(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of cases defined for the switch.",
			 "propertyCategory", "ENUMERATION"
		   });	
		addAnnotation
		  (getEnumerationSwitchBinding_ActiveCase(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe case currently active.",
			 "property", "Readonly",
			 "propertyCategory", "ENUMERATION"
		   });	
		addAnnotation
		  (enumerationCaseEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nClass that defines Enumeration Case used by the EnumerationSwitchBinding.\nIts associates a sub-topology to an enumeration literal value(s)."
		   });	
		addAnnotation
		  (getEnumerationCase_EnumerationLiterals(), 
		   source, 
		   new String[] {
			 "documentation", " The enumeration value(s) for this case."
		   });	
		addAnnotation
		  (getEnumerationCase_TopologyRoot(), 
		   source, 
		   new String[] {
			 "documentation", "The root of the topology to add to the parent Node\nwhen this case is activated.",
			 "children", "true"
		   });	
		addAnnotation
		  (bindingsListEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nDefines a list of Bindings."
		   });	
		addAnnotation
		  (featureRootsListEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nDefines Feature Roots Lists used to define all the\nfeature used by the Bindings in a BindingSet."
		   });	
		addAnnotation
		  (bindingsSetEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nDefines a set of bindings. The Binding set can be bound\nand unbound as a group."
		   });	
		addAnnotation
		  (getBindingsSet__Bind(), 
		   source, 
		   new String[] {
			 "documentation", "*\nBinds all the Bindings contained in the bindingsList."
		   });	
		addAnnotation
		  (getBindingsSet__Unbind(), 
		   source, 
		   new String[] {
			 "documentation", "*\nUnbinds all the Bindings contained in the bindingsList."
		   });	
		addAnnotation
		  (getBindingsSet_FeatureRootsList(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of features used in the Bindings definitions."
		   });	
		addAnnotation
		  (getBindingsSet_BindingsList(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of Bindings."
		   });	
		addAnnotation
		  (apogyCommonTopologyBindingsFacadeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nUtility Binding Facade."
		   });	
		addAnnotation
		  (getApogyCommonTopologyBindingsFacade__Copy__AbstractTopologyBinding_Map(), 
		   source, 
		   new String[] {
			 "documentation", "*\nConvenience method that copies a Binding.\n@param originalBinding The original binding to copy.\n@param originalToCopyNodeMap Maps original Nodes to copied ones.\n@return The copy of the original Binding."
		   });
	}

} //ApogyCommonTopologyBindingsPackageImpl

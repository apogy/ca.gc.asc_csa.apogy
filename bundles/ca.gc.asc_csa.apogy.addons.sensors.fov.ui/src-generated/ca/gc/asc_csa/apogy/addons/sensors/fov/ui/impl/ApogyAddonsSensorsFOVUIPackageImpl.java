package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewPresentation;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewPresentation;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolTargetVisibility;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIFactory;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage;
import ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewPresentation;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyAddonsSensorsFOVUIPackageImpl extends EPackageImpl implements ApogyAddonsSensorsFOVUIPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fieldOfViewPresentationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass circularSectorFieldOfViewPresentationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conicalFieldOfViewPresentationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rectangularFrustrumFieldOfViewPresentationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fieldOfViewEntry3DToolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fieldOfViewEntry3DToolNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	private EClass fieldOfViewWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	private EClass circularSectorFieldOfViewWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	private EClass conicalFieldOfViewWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	private EClass rectangularFrustrumFieldOfViewWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fieldOfViewEntry3DToolWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fieldOfViewEntry3DToolTargetVisibilityEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyAddonsSensorsFOVUIPackageImpl() {
		super(eNS_URI, ApogyAddonsSensorsFOVUIFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyAddonsSensorsFOVUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyAddonsSensorsFOVUIPackage init() {
		if (isInited) return (ApogyAddonsSensorsFOVUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsSensorsFOVUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyAddonsSensorsFOVUIPackageImpl theApogyAddonsSensorsFOVUIPackage = (ApogyAddonsSensorsFOVUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyAddonsSensorsFOVUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyAddonsSensorsFOVUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyAddonsPackage.eINSTANCE.eClass();
		ApogyAddonsSensorsFOVPackage.eINSTANCE.eClass();
		ApogyAddonsUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyAddonsSensorsFOVUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyAddonsSensorsFOVUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyAddonsSensorsFOVUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyAddonsSensorsFOVUIPackage.eNS_URI, theApogyAddonsSensorsFOVUIPackage);
		return theApogyAddonsSensorsFOVUIPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFieldOfViewPresentation() {
		return fieldOfViewPresentationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewPresentation_Transparency() {
		return (EAttribute)fieldOfViewPresentationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewPresentation_PresentationMode() {
		return (EAttribute)fieldOfViewPresentationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewPresentation_ShowOutlineOnly() {
		return (EAttribute)fieldOfViewPresentationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewPresentation_ShowProjection() {
		return (EAttribute)fieldOfViewPresentationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewPresentation_ProjectionColor() {
		return (EAttribute)fieldOfViewPresentationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewPresentation_FovVisible() {
		return (EAttribute)fieldOfViewPresentationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewPresentation_AxisVisible() {
		return (EAttribute)fieldOfViewPresentationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewPresentation_AxisLength() {
		return (EAttribute)fieldOfViewPresentationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCircularSectorFieldOfViewPresentation() {
		return circularSectorFieldOfViewPresentationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConicalFieldOfViewPresentation() {
		return conicalFieldOfViewPresentationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRectangularFrustrumFieldOfViewPresentation() {
		return rectangularFrustrumFieldOfViewPresentationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFieldOfViewEntry3DTool() {
		return fieldOfViewEntry3DToolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewEntry3DTool_TargetVisibility() {
		return (EAttribute)fieldOfViewEntry3DToolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFieldOfViewEntry3DTool_Fov() {
		return (EReference)fieldOfViewEntry3DToolEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFieldOfViewEntry3DTool_FieldOfViewEntry3DToolNode() {
		return (EReference)fieldOfViewEntry3DToolEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewEntry3DTool_CurrentVectorColor() {
		return (EAttribute)fieldOfViewEntry3DToolEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewEntry3DTool_DefaultVectorColor() {
		return (EAttribute)fieldOfViewEntry3DToolEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewEntry3DTool_VectorColorSelectionVisible() {
		return (EAttribute)fieldOfViewEntry3DToolEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewEntry3DTool_VectorColorSelectionEntered() {
		return (EAttribute)fieldOfViewEntry3DToolEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewEntry3DTool_MaximumVectorLength() {
		return (EAttribute)fieldOfViewEntry3DToolEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewEntry3DTool_VectorDiameter() {
		return (EAttribute)fieldOfViewEntry3DToolEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldOfViewEntry3DTool_CurrentVectorLength() {
		return (EAttribute)fieldOfViewEntry3DToolEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFieldOfViewEntry3DToolNode() {
		return fieldOfViewEntry3DToolNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFieldOfViewEntry3DToolNode_FieldOfViewEntry3DTool() {
		return (EReference)fieldOfViewEntry3DToolNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	public EClass getFieldOfViewWizardPagesProvider() {
		return fieldOfViewWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	public EClass getCircularSectorFieldOfViewWizardPagesProvider() {
		return circularSectorFieldOfViewWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	public EClass getConicalFieldOfViewWizardPagesProvider() {
		return conicalFieldOfViewWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	public EClass getRectangularFrustrumFieldOfViewWizardPagesProvider() {
		return rectangularFrustrumFieldOfViewWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFieldOfViewEntry3DToolWizardPagesProvider() {
		return fieldOfViewEntry3DToolWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFieldOfViewEntry3DToolTargetVisibility() {
		return fieldOfViewEntry3DToolTargetVisibilityEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsSensorsFOVUIFactory getApogyAddonsSensorsFOVUIFactory() {
		return (ApogyAddonsSensorsFOVUIFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		fieldOfViewPresentationEClass = createEClass(FIELD_OF_VIEW_PRESENTATION);
		createEAttribute(fieldOfViewPresentationEClass, FIELD_OF_VIEW_PRESENTATION__TRANSPARENCY);
		createEAttribute(fieldOfViewPresentationEClass, FIELD_OF_VIEW_PRESENTATION__PRESENTATION_MODE);
		createEAttribute(fieldOfViewPresentationEClass, FIELD_OF_VIEW_PRESENTATION__SHOW_OUTLINE_ONLY);
		createEAttribute(fieldOfViewPresentationEClass, FIELD_OF_VIEW_PRESENTATION__SHOW_PROJECTION);
		createEAttribute(fieldOfViewPresentationEClass, FIELD_OF_VIEW_PRESENTATION__PROJECTION_COLOR);
		createEAttribute(fieldOfViewPresentationEClass, FIELD_OF_VIEW_PRESENTATION__FOV_VISIBLE);
		createEAttribute(fieldOfViewPresentationEClass, FIELD_OF_VIEW_PRESENTATION__AXIS_VISIBLE);
		createEAttribute(fieldOfViewPresentationEClass, FIELD_OF_VIEW_PRESENTATION__AXIS_LENGTH);

		circularSectorFieldOfViewPresentationEClass = createEClass(CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION);

		conicalFieldOfViewPresentationEClass = createEClass(CONICAL_FIELD_OF_VIEW_PRESENTATION);

		rectangularFrustrumFieldOfViewPresentationEClass = createEClass(RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION);

		fieldOfViewEntry3DToolEClass = createEClass(FIELD_OF_VIEW_ENTRY3_DTOOL);
		createEAttribute(fieldOfViewEntry3DToolEClass, FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY);
		createEReference(fieldOfViewEntry3DToolEClass, FIELD_OF_VIEW_ENTRY3_DTOOL__FOV);
		createEReference(fieldOfViewEntry3DToolEClass, FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE);
		createEAttribute(fieldOfViewEntry3DToolEClass, FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR);
		createEAttribute(fieldOfViewEntry3DToolEClass, FIELD_OF_VIEW_ENTRY3_DTOOL__DEFAULT_VECTOR_COLOR);
		createEAttribute(fieldOfViewEntry3DToolEClass, FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_VISIBLE);
		createEAttribute(fieldOfViewEntry3DToolEClass, FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_ENTERED);
		createEAttribute(fieldOfViewEntry3DToolEClass, FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH);
		createEAttribute(fieldOfViewEntry3DToolEClass, FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_DIAMETER);
		createEAttribute(fieldOfViewEntry3DToolEClass, FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_LENGTH);

		fieldOfViewEntry3DToolNodeEClass = createEClass(FIELD_OF_VIEW_ENTRY3_DTOOL_NODE);
		createEReference(fieldOfViewEntry3DToolNodeEClass, FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL);

		fieldOfViewWizardPagesProviderEClass = createEClass(FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER);

		circularSectorFieldOfViewWizardPagesProviderEClass = createEClass(CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER);

		conicalFieldOfViewWizardPagesProviderEClass = createEClass(CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER);

		rectangularFrustrumFieldOfViewWizardPagesProviderEClass = createEClass(RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER);

		fieldOfViewEntry3DToolWizardPagesProviderEClass = createEClass(FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER);

		// Create enums
		fieldOfViewEntry3DToolTargetVisibilityEEnum = createEEnum(FIELD_OF_VIEW_ENTRY3_DTOOL_TARGET_VISIBILITY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCommonTopologyUIPackage theApogyCommonTopologyUIPackage = (ApogyCommonTopologyUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyUIPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogyAddonsPackage theApogyAddonsPackage = (ApogyAddonsPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsPackage.eNS_URI);
		ApogyAddonsSensorsFOVPackage theApogyAddonsSensorsFOVPackage = (ApogyAddonsSensorsFOVPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsSensorsFOVPackage.eNS_URI);
		ApogyCommonTopologyPackage theApogyCommonTopologyPackage = (ApogyCommonTopologyPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyPackage.eNS_URI);
		ApogyAddonsUIPackage theApogyAddonsUIPackage = (ApogyAddonsUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsUIPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		fieldOfViewPresentationEClass.getESuperTypes().add(theApogyCommonTopologyUIPackage.getNodePresentation());
		circularSectorFieldOfViewPresentationEClass.getESuperTypes().add(this.getFieldOfViewPresentation());
		conicalFieldOfViewPresentationEClass.getESuperTypes().add(this.getFieldOfViewPresentation());
		rectangularFrustrumFieldOfViewPresentationEClass.getESuperTypes().add(this.getFieldOfViewPresentation());
		fieldOfViewEntry3DToolEClass.getESuperTypes().add(theApogyAddonsPackage.getAbstractTwoPoints3DTool());
		fieldOfViewEntry3DToolNodeEClass.getESuperTypes().add(theApogyCommonTopologyPackage.getNode());
		fieldOfViewWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyUIPackage.getNodeWizardPagesProvider());
		circularSectorFieldOfViewWizardPagesProviderEClass.getESuperTypes().add(this.getFieldOfViewWizardPagesProvider());
		conicalFieldOfViewWizardPagesProviderEClass.getESuperTypes().add(this.getFieldOfViewWizardPagesProvider());
		rectangularFrustrumFieldOfViewWizardPagesProviderEClass.getESuperTypes().add(this.getFieldOfViewWizardPagesProvider());
		fieldOfViewEntry3DToolWizardPagesProviderEClass.getESuperTypes().add(theApogyAddonsUIPackage.getSimple3DToolWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(fieldOfViewPresentationEClass, FieldOfViewPresentation.class, "FieldOfViewPresentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFieldOfViewPresentation_Transparency(), theEcorePackage.getEFloat(), "transparency", null, 0, 1, FieldOfViewPresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewPresentation_PresentationMode(), theApogyCommonTopologyUIPackage.getMeshPresentationMode(), "presentationMode", null, 0, 1, FieldOfViewPresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewPresentation_ShowOutlineOnly(), theEcorePackage.getEBoolean(), "showOutlineOnly", "true", 0, 1, FieldOfViewPresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewPresentation_ShowProjection(), theEcorePackage.getEBoolean(), "showProjection", "false", 0, 1, FieldOfViewPresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewPresentation_ProjectionColor(), theApogyCommonTopologyUIPackage.getRGB(), "projectionColor", null, 0, 1, FieldOfViewPresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewPresentation_FovVisible(), theEcorePackage.getEBoolean(), "fovVisible", "false", 0, 1, FieldOfViewPresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewPresentation_AxisVisible(), theEcorePackage.getEBoolean(), "axisVisible", "false", 0, 1, FieldOfViewPresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewPresentation_AxisLength(), theEcorePackage.getEDouble(), "axisLength", "1.0", 0, 1, FieldOfViewPresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(circularSectorFieldOfViewPresentationEClass, CircularSectorFieldOfViewPresentation.class, "CircularSectorFieldOfViewPresentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(conicalFieldOfViewPresentationEClass, ConicalFieldOfViewPresentation.class, "ConicalFieldOfViewPresentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(rectangularFrustrumFieldOfViewPresentationEClass, RectangularFrustrumFieldOfViewPresentation.class, "RectangularFrustrumFieldOfViewPresentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fieldOfViewEntry3DToolEClass, FieldOfViewEntry3DTool.class, "FieldOfViewEntry3DTool", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFieldOfViewEntry3DTool_TargetVisibility(), this.getFieldOfViewEntry3DToolTargetVisibility(), "targetVisibility", null, 0, 1, FieldOfViewEntry3DTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFieldOfViewEntry3DTool_Fov(), theApogyAddonsSensorsFOVPackage.getFieldOfView(), null, "fov", null, 0, 1, FieldOfViewEntry3DTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFieldOfViewEntry3DTool_FieldOfViewEntry3DToolNode(), this.getFieldOfViewEntry3DToolNode(), this.getFieldOfViewEntry3DToolNode_FieldOfViewEntry3DTool(), "fieldOfViewEntry3DToolNode", null, 0, 1, FieldOfViewEntry3DTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewEntry3DTool_CurrentVectorColor(), theApogyAddonsPackage.getColor3f(), "currentVectorColor", "0.0,1.0,0.0", 0, 1, FieldOfViewEntry3DTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewEntry3DTool_DefaultVectorColor(), theApogyAddonsPackage.getColor3f(), "defaultVectorColor", "0.0,1.0,0.0", 0, 1, FieldOfViewEntry3DTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewEntry3DTool_VectorColorSelectionVisible(), theApogyAddonsPackage.getColor3f(), "vectorColorSelectionVisible", "1.0,0.0,0.0", 0, 1, FieldOfViewEntry3DTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewEntry3DTool_VectorColorSelectionEntered(), theApogyAddonsPackage.getColor3f(), "vectorColorSelectionEntered", "1.0,1.0,0.0", 0, 1, FieldOfViewEntry3DTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewEntry3DTool_MaximumVectorLength(), theEcorePackage.getEDouble(), "maximumVectorLength", "10.0", 0, 1, FieldOfViewEntry3DTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewEntry3DTool_VectorDiameter(), theEcorePackage.getEDouble(), "vectorDiameter", "0.025", 0, 1, FieldOfViewEntry3DTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFieldOfViewEntry3DTool_CurrentVectorLength(), theEcorePackage.getEDouble(), "currentVectorLength", "0", 0, 1, FieldOfViewEntry3DTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fieldOfViewEntry3DToolNodeEClass, FieldOfViewEntry3DToolNode.class, "FieldOfViewEntry3DToolNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFieldOfViewEntry3DToolNode_FieldOfViewEntry3DTool(), this.getFieldOfViewEntry3DTool(), this.getFieldOfViewEntry3DTool_FieldOfViewEntry3DToolNode(), "fieldOfViewEntry3DTool", null, 0, 1, FieldOfViewEntry3DToolNode.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fieldOfViewWizardPagesProviderEClass, FieldOfViewWizardPagesProvider.class, "FieldOfViewWizardPagesProvider", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(circularSectorFieldOfViewWizardPagesProviderEClass, CircularSectorFieldOfViewWizardPagesProvider.class, "CircularSectorFieldOfViewWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(conicalFieldOfViewWizardPagesProviderEClass, ConicalFieldOfViewWizardPagesProvider.class, "ConicalFieldOfViewWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(rectangularFrustrumFieldOfViewWizardPagesProviderEClass, RectangularFrustrumFieldOfViewWizardPagesProvider.class, "RectangularFrustrumFieldOfViewWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fieldOfViewEntry3DToolWizardPagesProviderEClass, FieldOfViewEntry3DToolWizardPagesProvider.class, "FieldOfViewEntry3DToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(fieldOfViewEntry3DToolTargetVisibilityEEnum, FieldOfViewEntry3DToolTargetVisibility.class, "FieldOfViewEntry3DToolTargetVisibility");
		addEEnumLiteral(fieldOfViewEntry3DToolTargetVisibilityEEnum, FieldOfViewEntry3DToolTargetVisibility.UNKNOWN);
		addEEnumLiteral(fieldOfViewEntry3DToolTargetVisibilityEEnum, FieldOfViewEntry3DToolTargetVisibility.NOT_VISIBLE);
		addEEnumLiteral(fieldOfViewEntry3DToolTargetVisibilityEEnum, FieldOfViewEntry3DToolTargetVisibility.VISIBLE);
		addEEnumLiteral(fieldOfViewEntry3DToolTargetVisibilityEEnum, FieldOfViewEntry3DToolTargetVisibility.INSIDE_VOLUME);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "prefix", "ApogyAddonsSensorsFOVUI",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyAddonsSensorsFOVUI",
			 "complianceLevel", "6.0",
			 "dynamicTemplates", "true",
			 "suppressGenModelAnnotations", "false",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.addons.sensors.fov.ui/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.addons.sensors.fov.ui.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.addons.sensors.fov"
		   });	
		addAnnotation
		  (fieldOfViewPresentationEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBase class for specialization of NodePresentation for Field Of View."
		   });	
		addAnnotation
		  (getFieldOfViewPresentation_Transparency(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe transparency of the FOV geometry."
		   });	
		addAnnotation
		  (getFieldOfViewPresentation_PresentationMode(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe presentation mode of the geometry representing the FOV."
		   });	
		addAnnotation
		  (getFieldOfViewPresentation_ShowOutlineOnly(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not to show the outline only. Currently not supported."
		   });	
		addAnnotation
		  (getFieldOfViewPresentation_ShowProjection(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not to show the projection of the FOV in the 3D environment."
		   });	
		addAnnotation
		  (getFieldOfViewPresentation_ProjectionColor(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe color of the FOV projection."
		   });	
		addAnnotation
		  (getFieldOfViewPresentation_FovVisible(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not to show the FOV geometry.",
			 "notify", "true",
			 "property", "Editable"
		   });	
		addAnnotation
		  (getFieldOfViewPresentation_AxisVisible(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not to show the the reference frame axis of the FOV.",
			 "notify", "true",
			 "property", "Editable",
			 "propertyCategory", "VISUAL_INFORMATION"
		   });	
		addAnnotation
		  (getFieldOfViewPresentation_AxisLength(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe length of the reference frame axis of the FOV, in meters.",
			 "apogy_units", "m",
			 "notify", "true",
			 "property", "Editable",
			 "propertyCategory", "VISUAL_INFORMATION"
		   });	
		addAnnotation
		  (circularSectorFieldOfViewPresentationEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecialization of Field Of View Presentation for CircularSectorFieldOfView."
		   });	
		addAnnotation
		  (conicalFieldOfViewPresentationEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecialization of Field Of View Presentation for ConicalFieldOfView."
		   });	
		addAnnotation
		  (rectangularFrustrumFieldOfViewPresentationEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecialization of Field Of View Presentation for RectangularFrustrumFieldOfView."
		   });	
		addAnnotation
		  (fieldOfViewEntry3DToolEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nSimple 3D tool used to monitor when a selected point enters or becomes visible from a  FieldOfView.\nThe From node is the FieldOfView, the To node the point selected."
		   });	
		addAnnotation
		  (getFieldOfViewEntry3DTool_TargetVisibility(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe state of the target visibility.",
			 "notify", "true",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (getFieldOfViewEntry3DTool_Fov(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe  FieldOfView for which to monitor entry.",
			 "notify", "true",
			 "children", "false",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (getFieldOfViewEntry3DTool_FieldOfViewEntry3DToolNode(), 
		   source, 
		   new String[] {
			 "documentation", "*\nTopology Node associated with the tool. This is the Node that represent the FieldOfViewEntry3DTool in the topology.",
			 "notify", "true",
			 "children", "false",
			 "property", "None",
			 "propertyCategory", "RULER_PROPERTIES"
		   });	
		addAnnotation
		  (getFieldOfViewEntry3DTool_CurrentVectorColor(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe current displayed vector color.",
			 "property", "None"
		   });	
		addAnnotation
		  (getFieldOfViewEntry3DTool_DefaultVectorColor(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe vector color when not no entry detected.",
			 "property", "Editable"
		   });	
		addAnnotation
		  (getFieldOfViewEntry3DTool_VectorColorSelectionVisible(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe vector color when the selected point visible from the FieldOfView.",
			 "property", "Editable"
		   });	
		addAnnotation
		  (getFieldOfViewEntry3DTool_VectorColorSelectionEntered(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe vector color when the selected point in inside the volume (or area) of the FieldOfView.",
			 "property", "Editable"
		   });	
		addAnnotation
		  (getFieldOfViewEntry3DTool_MaximumVectorLength(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMaximum length of the vector. When the current length becomes smaller than this value, the actual distance to the selected point is used.",
			 "property", "Editable",
			 "notify", "true",
			 "apogy_units", "m"
		   });	
		addAnnotation
		  (getFieldOfViewEntry3DTool_VectorDiameter(), 
		   source, 
		   new String[] {
			 "documentation", "*\nDiameter of the cylinder used to display the vector.",
			 "property", "Editable",
			 "notify", "true",
			 "apogy_units", "m"
		   });	
		addAnnotation
		  (getFieldOfViewEntry3DTool_CurrentVectorLength(), 
		   source, 
		   new String[] {
			 "documentation", "*\nCurrent length of the vector.",
			 "property", "None",
			 "apogy_units", "m"
		   });	
		addAnnotation
		  (fieldOfViewEntry3DToolTargetVisibilityEEnum, 
		   source, 
		   new String[] {
			 "documentation", "*\nStates of target visibility foe the the FieldOfViewEntry3DTool."
		   });	
		addAnnotation
		  (fieldOfViewEntry3DToolNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nNode that represent the FieldOfViewVectorEntry3DTool in the topology."
		   });	
		addAnnotation
		  (fieldOfViewWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBase class for FieldOfView Wizard Pages Providers."
		   });	
		addAnnotation
		  (circularSectorFieldOfViewWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for CircularSectorFieldOfView."
		   });	
		addAnnotation
		  (conicalFieldOfViewWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for ConicalFieldOfView."
		   });	
		addAnnotation
		  (rectangularFrustrumFieldOfViewWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for RectangularFrustrumFieldOfView."
		   });	
		addAnnotation
		  (fieldOfViewEntry3DToolWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard support for RectangularFrustrumFieldOfView."
		   });
	}

} //ApogyAddonsSensorsFOVUIPackageImpl

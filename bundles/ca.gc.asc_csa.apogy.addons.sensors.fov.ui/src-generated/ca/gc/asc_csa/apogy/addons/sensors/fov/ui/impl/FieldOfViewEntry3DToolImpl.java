/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *      Regent L'Archeveque
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsFactory;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl;
import ca.gc.asc_csa.apogy.addons.sensors.fov.FieldOfView;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIFactory;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolTargetVisibility;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.GroupNode;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.NodePath;
import ca.gc.asc_csa.apogy.common.topology.ui.NodeSelection;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyFacade;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Field
 * Of View Entry3 DTool</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl#getTargetVisibility <em>Target Visibility</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl#getFov <em>Fov</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl#getFieldOfViewEntry3DToolNode <em>Field Of View Entry3 DTool Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl#getCurrentVectorColor <em>Current Vector Color</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl#getDefaultVectorColor <em>Default Vector Color</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl#getVectorColorSelectionVisible <em>Vector Color Selection Visible</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl#getVectorColorSelectionEntered <em>Vector Color Selection Entered</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl#getMaximumVectorLength <em>Maximum Vector Length</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl#getVectorDiameter <em>Vector Diameter</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl#getCurrentVectorLength <em>Current Vector Length</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FieldOfViewEntry3DToolImpl extends AbstractTwoPoints3DToolImpl implements FieldOfViewEntry3DTool {
	/**
	 * The default value of the '{@link #getTargetVisibility() <em>Target Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetVisibility()
	 * @generated
	 * @ordered
	 */
	protected static final FieldOfViewEntry3DToolTargetVisibility TARGET_VISIBILITY_EDEFAULT = FieldOfViewEntry3DToolTargetVisibility.UNKNOWN;

	/**
	 * The cached value of the '{@link #getTargetVisibility() <em>Target Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetVisibility()
	 * @generated
	 * @ordered
	 */
	protected FieldOfViewEntry3DToolTargetVisibility targetVisibility = TARGET_VISIBILITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFov() <em>Fov</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getFov()
	 * @generated
	 * @ordered
	 */
	protected FieldOfView fov;

	/**
	 * The cached value of the '{@link #getFieldOfViewEntry3DToolNode() <em>Field Of View Entry3 DTool Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFieldOfViewEntry3DToolNode()
	 * @generated
	 * @ordered
	 */
	protected FieldOfViewEntry3DToolNode fieldOfViewEntry3DToolNode;

	/**
	 * The default value of the '{@link #getCurrentVectorColor() <em>Current
	 * Vector Color</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getCurrentVectorColor()
	 * @generated
	 * @ordered
	 */
	protected static final Color3f CURRENT_VECTOR_COLOR_EDEFAULT = (Color3f)ApogyAddonsFactory.eINSTANCE.createFromString(ApogyAddonsPackage.eINSTANCE.getColor3f(), "0.0,1.0,0.0");

	/**
	 * The cached value of the '{@link #getCurrentVectorColor() <em>Current
	 * Vector Color</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getCurrentVectorColor()
	 * @generated
	 * @ordered
	 */
	protected Color3f currentVectorColor = CURRENT_VECTOR_COLOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefaultVectorColor() <em>Default
	 * Vector Color</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDefaultVectorColor()
	 * @generated
	 * @ordered
	 */
	protected static final Color3f DEFAULT_VECTOR_COLOR_EDEFAULT = (Color3f)ApogyAddonsFactory.eINSTANCE.createFromString(ApogyAddonsPackage.eINSTANCE.getColor3f(), "0.0,1.0,0.0");

	/**
	 * The cached value of the '{@link #getDefaultVectorColor() <em>Default
	 * Vector Color</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDefaultVectorColor()
	 * @generated
	 * @ordered
	 */
	protected Color3f defaultVectorColor = DEFAULT_VECTOR_COLOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getVectorColorSelectionVisible() <em>Vector Color Selection Visible</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getVectorColorSelectionVisible()
	 * @generated
	 * @ordered
	 */
	protected static final Color3f VECTOR_COLOR_SELECTION_VISIBLE_EDEFAULT = (Color3f)ApogyAddonsFactory.eINSTANCE.createFromString(ApogyAddonsPackage.eINSTANCE.getColor3f(), "1.0,0.0,0.0");

	/**
	 * The cached value of the '{@link #getVectorColorSelectionVisible() <em>Vector Color Selection Visible</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getVectorColorSelectionVisible()
	 * @generated
	 * @ordered
	 */
	protected Color3f vectorColorSelectionVisible = VECTOR_COLOR_SELECTION_VISIBLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getVectorColorSelectionEntered() <em>Vector Color Selection Entered</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getVectorColorSelectionEntered()
	 * @generated
	 * @ordered
	 */
	protected static final Color3f VECTOR_COLOR_SELECTION_ENTERED_EDEFAULT = (Color3f)ApogyAddonsFactory.eINSTANCE.createFromString(ApogyAddonsPackage.eINSTANCE.getColor3f(), "1.0,1.0,0.0");

	/**
	 * The cached value of the '{@link #getVectorColorSelectionEntered() <em>Vector Color Selection Entered</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getVectorColorSelectionEntered()
	 * @generated
	 * @ordered
	 */
	protected Color3f vectorColorSelectionEntered = VECTOR_COLOR_SELECTION_ENTERED_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaximumVectorLength() <em>Maximum
	 * Vector Length</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getMaximumVectorLength()
	 * @generated
	 * @ordered
	 */
	protected static final double MAXIMUM_VECTOR_LENGTH_EDEFAULT = 10.0;

	/**
	 * The cached value of the '{@link #getMaximumVectorLength() <em>Maximum
	 * Vector Length</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getMaximumVectorLength()
	 * @generated
	 * @ordered
	 */
	protected double maximumVectorLength = MAXIMUM_VECTOR_LENGTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getVectorDiameter() <em>Vector Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVectorDiameter()
	 * @generated
	 * @ordered
	 */
	protected static final double VECTOR_DIAMETER_EDEFAULT = 0.025;

	/**
	 * The cached value of the '{@link #getVectorDiameter() <em>Vector Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVectorDiameter()
	 * @generated
	 * @ordered
	 */
	protected double vectorDiameter = VECTOR_DIAMETER_EDEFAULT;

	/**
	 * The default value of the '{@link #getCurrentVectorLength() <em>Current
	 * Vector Length</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getCurrentVectorLength()
	 * @generated
	 * @ordered
	 */
	protected static final double CURRENT_VECTOR_LENGTH_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCurrentVectorLength() <em>Current
	 * Vector Length</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getCurrentVectorLength()
	 * @generated
	 * @ordered
	 */
	protected double currentVectorLength = CURRENT_VECTOR_LENGTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected FieldOfViewEntry3DToolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfViewEntry3DToolTargetVisibility getTargetVisibility() {
		return targetVisibility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetVisibility(FieldOfViewEntry3DToolTargetVisibility newTargetVisibility) {
		FieldOfViewEntry3DToolTargetVisibility oldTargetVisibility = targetVisibility;
		targetVisibility = newTargetVisibility == null ? TARGET_VISIBILITY_EDEFAULT : newTargetVisibility;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY, oldTargetVisibility, targetVisibility));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfView getFov() {
		if (fov != null && fov.eIsProxy()) {
			InternalEObject oldFov = (InternalEObject)fov;
			fov = (FieldOfView)eResolveProxy(oldFov);
			if (fov != oldFov) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FOV, oldFov, fov));
			}
		}
		return fov;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfView basicGetFov() {
		return fov;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setFov(FieldOfView newFov) {
		FieldOfView oldFov = fov;
		fov = newFov;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FOV, oldFov, fov));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfViewEntry3DToolNode getFieldOfViewEntry3DToolNode() {
		if (fieldOfViewEntry3DToolNode != null && fieldOfViewEntry3DToolNode.eIsProxy()) {
			InternalEObject oldFieldOfViewEntry3DToolNode = (InternalEObject)fieldOfViewEntry3DToolNode;
			fieldOfViewEntry3DToolNode = (FieldOfViewEntry3DToolNode)eResolveProxy(oldFieldOfViewEntry3DToolNode);
			if (fieldOfViewEntry3DToolNode != oldFieldOfViewEntry3DToolNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE, oldFieldOfViewEntry3DToolNode, fieldOfViewEntry3DToolNode));
			}
		}
		return fieldOfViewEntry3DToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfViewEntry3DToolNode basicGetFieldOfViewEntry3DToolNode() {
		return fieldOfViewEntry3DToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFieldOfViewEntry3DToolNode(FieldOfViewEntry3DToolNode newFieldOfViewEntry3DToolNode, NotificationChain msgs) {
		FieldOfViewEntry3DToolNode oldFieldOfViewEntry3DToolNode = fieldOfViewEntry3DToolNode;
		fieldOfViewEntry3DToolNode = newFieldOfViewEntry3DToolNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE, oldFieldOfViewEntry3DToolNode, newFieldOfViewEntry3DToolNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFieldOfViewEntry3DToolNode(FieldOfViewEntry3DToolNode newFieldOfViewEntry3DToolNode) {
		if (newFieldOfViewEntry3DToolNode != fieldOfViewEntry3DToolNode) {
			NotificationChain msgs = null;
			if (fieldOfViewEntry3DToolNode != null)
				msgs = ((InternalEObject)fieldOfViewEntry3DToolNode).eInverseRemove(this, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL, FieldOfViewEntry3DToolNode.class, msgs);
			if (newFieldOfViewEntry3DToolNode != null)
				msgs = ((InternalEObject)newFieldOfViewEntry3DToolNode).eInverseAdd(this, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL, FieldOfViewEntry3DToolNode.class, msgs);
			msgs = basicSetFieldOfViewEntry3DToolNode(newFieldOfViewEntry3DToolNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE, newFieldOfViewEntry3DToolNode, newFieldOfViewEntry3DToolNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Color3f getCurrentVectorColor() {
		return currentVectorColor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentVectorColor(Color3f newCurrentVectorColor) {
		Color3f oldCurrentVectorColor = currentVectorColor;
		currentVectorColor = newCurrentVectorColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR, oldCurrentVectorColor, currentVectorColor));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Color3f getDefaultVectorColor() {
		return defaultVectorColor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void setDefaultVectorColor(Color3f newDefaultVectorColor) 
	{
		setDefaultVectorColorGen(newDefaultVectorColor);
		
		// Force states to update to reflect the color change.
		updateEntryStates();
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultVectorColorGen(Color3f newDefaultVectorColor) {
		Color3f oldDefaultVectorColor = defaultVectorColor;
		defaultVectorColor = newDefaultVectorColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__DEFAULT_VECTOR_COLOR, oldDefaultVectorColor, defaultVectorColor));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Color3f getVectorColorSelectionVisible() {
		return vectorColorSelectionVisible;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void setVectorColorSelectionVisible(Color3f newVectorColorSelectionVisible) 
	{
		setVectorColorSelectionVisibleGen(newVectorColorSelectionVisible);	
		
		// Force states to update to reflect the color change.
		updateEntryStates();
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setVectorColorSelectionVisibleGen(Color3f newVectorColorSelectionVisible) {
		Color3f oldVectorColorSelectionVisible = vectorColorSelectionVisible;
		vectorColorSelectionVisible = newVectorColorSelectionVisible;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_VISIBLE, oldVectorColorSelectionVisible, vectorColorSelectionVisible));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Color3f getVectorColorSelectionEntered() {
		return vectorColorSelectionEntered;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void setVectorColorSelectionEntered(Color3f newVectorColorSelectionEntered) 
	{
		setVectorColorSelectionEnteredGen(newVectorColorSelectionEntered);
		
		// Force states to update to reflect the color change.
		updateEntryStates();
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setVectorColorSelectionEnteredGen(Color3f newVectorColorSelectionEntered) {
		Color3f oldVectorColorSelectionEntered = vectorColorSelectionEntered;
		vectorColorSelectionEntered = newVectorColorSelectionEntered;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_ENTERED, oldVectorColorSelectionEntered, vectorColorSelectionEntered));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public double getMaximumVectorLength() {
		return maximumVectorLength;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaximumVectorLength(double newMaximumVectorLength) 
	{
		double oldMaximumVectorLength = maximumVectorLength;
		maximumVectorLength = newMaximumVectorLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH, oldMaximumVectorLength, maximumVectorLength));
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaximumVectorLengthGen(double newMaximumVectorLength) {
		double oldMaximumVectorLength = maximumVectorLength;
		maximumVectorLength = newMaximumVectorLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH, oldMaximumVectorLength, maximumVectorLength));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getVectorDiameter() {
		return vectorDiameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVectorDiameter(double newVectorDiameter) {
		double oldVectorDiameter = vectorDiameter;
		vectorDiameter = newVectorDiameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_DIAMETER, oldVectorDiameter, vectorDiameter));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public double getCurrentVectorLength() {
		return currentVectorLength;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentVectorLength(double newCurrentVectorLength) {
		double oldCurrentVectorLength = currentVectorLength;
		currentVectorLength = newCurrentVectorLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_LENGTH, oldCurrentVectorLength, currentVectorLength));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE:
				if (fieldOfViewEntry3DToolNode != null)
					msgs = ((InternalEObject)fieldOfViewEntry3DToolNode).eInverseRemove(this, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL, FieldOfViewEntry3DToolNode.class, msgs);
				return basicSetFieldOfViewEntry3DToolNode((FieldOfViewEntry3DToolNode)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE:
				return basicSetFieldOfViewEntry3DToolNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY:
				return getTargetVisibility();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FOV:
				if (resolve) return getFov();
				return basicGetFov();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE:
				if (resolve) return getFieldOfViewEntry3DToolNode();
				return basicGetFieldOfViewEntry3DToolNode();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR:
				return getCurrentVectorColor();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__DEFAULT_VECTOR_COLOR:
				return getDefaultVectorColor();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_VISIBLE:
				return getVectorColorSelectionVisible();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_ENTERED:
				return getVectorColorSelectionEntered();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH:
				return getMaximumVectorLength();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_DIAMETER:
				return getVectorDiameter();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_LENGTH:
				return getCurrentVectorLength();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY:
				setTargetVisibility((FieldOfViewEntry3DToolTargetVisibility)newValue);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FOV:
				setFov((FieldOfView)newValue);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE:
				setFieldOfViewEntry3DToolNode((FieldOfViewEntry3DToolNode)newValue);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR:
				setCurrentVectorColor((Color3f)newValue);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__DEFAULT_VECTOR_COLOR:
				setDefaultVectorColor((Color3f)newValue);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_VISIBLE:
				setVectorColorSelectionVisible((Color3f)newValue);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_ENTERED:
				setVectorColorSelectionEntered((Color3f)newValue);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH:
				setMaximumVectorLength((Double)newValue);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_DIAMETER:
				setVectorDiameter((Double)newValue);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_LENGTH:
				setCurrentVectorLength((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY:
				setTargetVisibility(TARGET_VISIBILITY_EDEFAULT);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FOV:
				setFov((FieldOfView)null);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE:
				setFieldOfViewEntry3DToolNode((FieldOfViewEntry3DToolNode)null);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR:
				setCurrentVectorColor(CURRENT_VECTOR_COLOR_EDEFAULT);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__DEFAULT_VECTOR_COLOR:
				setDefaultVectorColor(DEFAULT_VECTOR_COLOR_EDEFAULT);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_VISIBLE:
				setVectorColorSelectionVisible(VECTOR_COLOR_SELECTION_VISIBLE_EDEFAULT);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_ENTERED:
				setVectorColorSelectionEntered(VECTOR_COLOR_SELECTION_ENTERED_EDEFAULT);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH:
				setMaximumVectorLength(MAXIMUM_VECTOR_LENGTH_EDEFAULT);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_DIAMETER:
				setVectorDiameter(VECTOR_DIAMETER_EDEFAULT);
				return;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_LENGTH:
				setCurrentVectorLength(CURRENT_VECTOR_LENGTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY:
				return targetVisibility != TARGET_VISIBILITY_EDEFAULT;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FOV:
				return fov != null;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE:
				return fieldOfViewEntry3DToolNode != null;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR:
				return CURRENT_VECTOR_COLOR_EDEFAULT == null ? currentVectorColor != null : !CURRENT_VECTOR_COLOR_EDEFAULT.equals(currentVectorColor);
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__DEFAULT_VECTOR_COLOR:
				return DEFAULT_VECTOR_COLOR_EDEFAULT == null ? defaultVectorColor != null : !DEFAULT_VECTOR_COLOR_EDEFAULT.equals(defaultVectorColor);
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_VISIBLE:
				return VECTOR_COLOR_SELECTION_VISIBLE_EDEFAULT == null ? vectorColorSelectionVisible != null : !VECTOR_COLOR_SELECTION_VISIBLE_EDEFAULT.equals(vectorColorSelectionVisible);
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_ENTERED:
				return VECTOR_COLOR_SELECTION_ENTERED_EDEFAULT == null ? vectorColorSelectionEntered != null : !VECTOR_COLOR_SELECTION_ENTERED_EDEFAULT.equals(vectorColorSelectionEntered);
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH:
				return maximumVectorLength != MAXIMUM_VECTOR_LENGTH_EDEFAULT;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_DIAMETER:
				return vectorDiameter != VECTOR_DIAMETER_EDEFAULT;
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_LENGTH:
				return currentVectorLength != CURRENT_VECTOR_LENGTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (targetVisibility: ");
		result.append(targetVisibility);
		result.append(", currentVectorColor: ");
		result.append(currentVectorColor);
		result.append(", defaultVectorColor: ");
		result.append(defaultVectorColor);
		result.append(", vectorColorSelectionVisible: ");
		result.append(vectorColorSelectionVisible);
		result.append(", vectorColorSelectionEntered: ");
		result.append(vectorColorSelectionEntered);
		result.append(", maximumVectorLength: ");
		result.append(maximumVectorLength);
		result.append(", vectorDiameter: ");
		result.append(vectorDiameter);
		result.append(", currentVectorLength: ");
		result.append(currentVectorLength);
		result.append(')');
		return result.toString();
	}

	@Override
	public void initialise() 
	{
		super.initialise();

		// First, initialize the FieldOfViewEntry3DToolNode.
		if(getFieldOfViewEntry3DToolNode() == null)
		{
			FieldOfViewEntry3DToolNode toolNode = ApogyAddonsSensorsFOVUIFactory.eINSTANCE.createFieldOfViewEntry3DToolNode();
			
			if (getName() != null) 
			{
				toolNode.setDescription("Node associated with the FieldOfViewEntry3DTool named <" + getName() + ">");
				toolNode.setNodeId("FieldOfViewEntry3DTool_" + getName().replaceAll(" ", "_"));
			}
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE,toolNode, true);
		}
		
		// Forces To and FROM node to be resolved.
		forceToAndFromReload();
				
		// Attaches the FieldOfViewEntry3DToolNode to the from node.
		attachFieldOfViewEntry3DToolNode();

		// Updates the vector.
		updateVector();
	}

	@Override
	public void setRootNode(Node newRootNode) 
	{
		super.setRootNode(newRootNode);

		// Attaches the FieldOfViewEntry3DToolNode to the from node.
		attachFieldOfViewEntry3DToolNode();

		// Updates the vector.
		updateVector();
	}

	@Override
	public void variablesInstantiated() 
	{
		super.variablesInstantiated();

		// Forces To and FROM node to be resolved.
		forceToAndFromReload();

		// Updates the vector.
		updateVector();
	}

	@Override
	public void variablesCleared() 
	{		
		super.variablesCleared();
		
		// Updates FROM Node to null.
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE, null, true);
	}
	
	@Override
	public void setActive(boolean newActive) 
	{	
		super.setActive(newActive);
		
		if(newActive)
		{
			// Forces To and FROM node to be resolved.
			forceToAndFromReload();						
		}
	}
	
	@Override
	public void dispose() {
		super.dispose();

		if (fieldOfViewEntry3DToolNode != null) 
		{
			detachFieldOfViewEntry3DToolNode();
		}				

		// Clears the FieldOfViewEntry3DToolNode.
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
				ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE,
				null);
	}

	@Override
	public void selectionChanged(NodeSelection nodeSelection) 
	{		
		if (!isDisposed()) 
		{
			Node node = nodeSelection.getSelectedNode();

			Tuple3d relativePosition = null;
			if (nodeSelection.getRelativeIntersectionPoint() != null) 
			{
				relativePosition = ApogyCommonMathFacade.INSTANCE.createTuple3d(nodeSelection.getRelativeIntersectionPoint());
			}

			if (!isFromNodeLock()) 
			{
				updateFromNode(node);
			} 
			else if (!isToNodeLock()) 
			{
				updateToNode(node, relativePosition);
			}
		}	
	}

	@Override
	public void pointsRelativePoseChanged(Matrix4d newPose) 
	{
		if (!isDisposed()) 
		{
			updateVector();
		}
	}

	protected void updateEntryStates() 
	{
		if (getFov() != null && getToNode() != null && getFromNode() != null) 
		{
			// Converts the To position to the From coordinates frame.
			Tuple3d point = getFromPositionInFOVFrame();

			// Updates the visible and inside flags.
			boolean visible = getFov().isPointVisible(point); 
			boolean inside = getFov().isPointInside(point);
						
			if(inside)
			{
				if(getCurrentVectorColor() != getVectorColorSelectionEntered())
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet(this,ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR, getVectorColorSelectionEntered(), true);
				}
				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this,ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY, FieldOfViewEntry3DToolTargetVisibility.INSIDE_VOLUME, true);
			}
			else if(visible)
			{
				if(getCurrentVectorColor() != getVectorColorSelectionVisible())
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet(this,ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR, getVectorColorSelectionVisible(), true);
				}
				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this,ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY, FieldOfViewEntry3DToolTargetVisibility.VISIBLE, true);
			}
			else
			{
				if(getCurrentVectorColor() != getDefaultVectorColor())
				{				
					ApogyCommonTransactionFacade.INSTANCE.basicSet(this,ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR, getDefaultVectorColor(), true);
				}
				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this,ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY, FieldOfViewEntry3DToolTargetVisibility.NOT_VISIBLE, true);
			}
		}
	}

	protected Tuple3d getFromPositionInFOVFrame() 
	{
		// Gets the transform between the TO Node and the FROM node.	
		Matrix4d transform = ApogyCommonTopologyFacade.INSTANCE.expressInFrame(toNode, fromNode);
		
		// Converts the To position to the From coordinates frame.
		javax.vecmath.Point3d position = new Point3d();				
		transform.transform(new Point3d(getToRelativePosition().asTuple3d()), position);
		
		return ApogyCommonMathFacade.INSTANCE.createTuple3d(position);
	}

	protected void updateVector() 
	{
		// Update Vector length.
		if (getToAbsolutePosition() != null && getFromAbsolutePosition() != null) 
		{
			Vector3d to = new Vector3d(getToAbsolutePosition().asTuple3d());
			Vector3d from = new Vector3d(getFromAbsolutePosition().asTuple3d());
			to.sub(from);

			double length = to.length();
			
			System.out.println(length);
			
			if (length < getMaximumVectorLength()) 
			{
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_LENGTH,	length, true);
			} 
			else 
			{
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_LENGTH, getMaximumVectorLength(), true);
			}
		}

		// Update entry states.
		updateEntryStates();
	}

	protected void updateFromNode(Node node)
	{
		if (node instanceof FieldOfView) 
		{
			// Updates FROM Node.
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE, node);

			// Updates FROM relative position to zero.
			Tuple3d zero = ApogyCommonMathFacade.INSTANCE.createTuple3d(0, 0, 0);
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION, zero, true);

			// Updates FOV
			FieldOfView fieldOfView = (FieldOfView) node;
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL__FOV, fieldOfView, true);

			// Update the node path.
			NodePath nodePath = null;
			if(node != null)
			{
				Node root = ApogyCoreTopologyFacade.INSTANCE.getApogyTopology().getRootNode();
				nodePath = ApogyCommonTopologyFacade.INSTANCE.createNodePath(root, node);
			}
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH, nodePath);						
			
			// Attaches the FieldOfViewEntry3DToolNode to the from node.
			attachFieldOfViewEntry3DToolNode();
		}
				
		// Updates the vector.
		updateVector();
		
	}
	
	protected void updateToNode(Node node, Tuple3d relativePosition)
	{
		// Updates TO node
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this,ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE, node, true);

		// Updates TO Node Path
		if(ApogyCoreTopologyFacade.INSTANCE.getApogyTopology() != null)
		{
			Node root = ApogyCoreTopologyFacade.INSTANCE.getApogyTopology().getRootNode();
			NodePath nodePath = ApogyCommonTopologyFacade.INSTANCE.createNodePath(root, node);
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH, nodePath, true);
		}		
		if(relativePosition != null)
		{
			Tuple3d newRelativePosition = EcoreUtil.copy(relativePosition);
			
			// Update ToRelativePosition.
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION, newRelativePosition, true);
		}
				
		// Updates the vector.
		updateVector();
	}
	
	protected void attachFieldOfViewEntry3DToolNode() 
	{
		if(getFieldOfViewEntry3DToolNode() != null)
		{
			if(getRootNode() instanceof GroupNode)
			{
				((GroupNode) getRootNode()).getChildren().add(getFieldOfViewEntry3DToolNode());
			}
		}
	}

	protected void detachFieldOfViewEntry3DToolNode() 
	{
		
		if(getRootNode() instanceof GroupNode)
		{
			GroupNode groupNode = (GroupNode) getRootNode();
			
			groupNode.getChildren().remove(getFieldOfViewEntry3DToolNode());
		}
	}

	@Override
	protected void forceToAndFromReload() 
	{
		super.forceToAndFromReload();
		
		// Calls the updates from both Nodes.
		updateFromNode(getFromNode());
		updateToNode(getToNode(), getToRelativePosition());
	}

} // FieldOfViewEntry3DToolImpl

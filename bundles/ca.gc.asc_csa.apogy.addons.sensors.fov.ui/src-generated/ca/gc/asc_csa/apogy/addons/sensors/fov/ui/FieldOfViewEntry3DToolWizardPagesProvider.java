/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *      Regent L'Archeveque
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui;

import ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Of View Entry3 DTool Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Wizard support for RectangularFrustrumFieldOfView.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DToolWizardPagesProvider()
 * @model
 * @generated
 */
public interface FieldOfViewEntry3DToolWizardPagesProvider extends Simple3DToolWizardPagesProvider {
} // FieldOfViewEntry3DToolWizardPagesProvider

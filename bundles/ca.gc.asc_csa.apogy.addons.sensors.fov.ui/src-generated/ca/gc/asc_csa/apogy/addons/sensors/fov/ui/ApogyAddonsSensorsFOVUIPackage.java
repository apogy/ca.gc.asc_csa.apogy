package ca.gc.asc_csa.apogy.addons.sensors.fov.ui;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyAddonsSensorsFOVUI' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n     Regent L\'Archeveque\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' modelName='ApogyAddonsSensorsFOVUI' complianceLevel='6.0' dynamicTemplates='true' suppressGenModelAnnotations='false' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' modelDirectory='/ca.gc.asc_csa.apogy.addons.sensors.fov.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.addons.sensors.fov.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.addons.sensors.fov'"
 * @generated
 */
public interface ApogyAddonsSensorsFOVUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.addons.sensors.fov.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyAddonsSensorsFOVUIPackage eINSTANCE = ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewPresentationImpl <em>Field Of View Presentation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewPresentationImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewPresentation()
	 * @generated
	 */
	int FIELD_OF_VIEW_PRESENTATION = 0;

	/**
	 * The feature id for the '<em><b>Topology Presentation Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__TOPOLOGY_PRESENTATION_SET = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__TOPOLOGY_PRESENTATION_SET;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__NODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__NODE;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__COLOR = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__COLOR;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__VISIBLE;

	/**
	 * The feature id for the '<em><b>Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__SELECTED = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SELECTED;

	/**
	 * The feature id for the '<em><b>Shadow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__SHADOW_MODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SHADOW_MODE;

	/**
	 * The feature id for the '<em><b>Use In Bounding Calculation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__USE_IN_BOUNDING_CALCULATION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__USE_IN_BOUNDING_CALCULATION;

	/**
	 * The feature id for the '<em><b>Id Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__ID_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ID_VISIBLE;

	/**
	 * The feature id for the '<em><b>Enable Texture Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__ENABLE_TEXTURE_PROJECTION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ENABLE_TEXTURE_PROJECTION;

	/**
	 * The feature id for the '<em><b>Centroid</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__CENTROID = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__CENTROID;

	/**
	 * The feature id for the '<em><b>Min</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__MIN = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MIN;

	/**
	 * The feature id for the '<em><b>Max</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__MAX = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MAX;

	/**
	 * The feature id for the '<em><b>XRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__XRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__XRANGE;

	/**
	 * The feature id for the '<em><b>YRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__YRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__YRANGE;

	/**
	 * The feature id for the '<em><b>ZRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__ZRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ZRANGE;

	/**
	 * The feature id for the '<em><b>Scene Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__SCENE_OBJECT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SCENE_OBJECT;

	/**
	 * The feature id for the '<em><b>Transparency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__TRANSPARENCY = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Presentation Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__PRESENTATION_MODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Show Outline Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__SHOW_OUTLINE_ONLY = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Show Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__SHOW_PROJECTION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Projection Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__PROJECTION_COLOR = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Fov Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__FOV_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Axis Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__AXIS_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Axis Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION__AXIS_LENGTH = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Field Of View Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION_FEATURE_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Field Of View Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_PRESENTATION_OPERATION_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.CircularSectorFieldOfViewPresentationImpl <em>Circular Sector Field Of View Presentation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.CircularSectorFieldOfViewPresentationImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getCircularSectorFieldOfViewPresentation()
	 * @generated
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION = 1;

	/**
	 * The feature id for the '<em><b>Topology Presentation Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__TOPOLOGY_PRESENTATION_SET = FIELD_OF_VIEW_PRESENTATION__TOPOLOGY_PRESENTATION_SET;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__NODE = FIELD_OF_VIEW_PRESENTATION__NODE;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__COLOR = FIELD_OF_VIEW_PRESENTATION__COLOR;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__VISIBLE = FIELD_OF_VIEW_PRESENTATION__VISIBLE;

	/**
	 * The feature id for the '<em><b>Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__SELECTED = FIELD_OF_VIEW_PRESENTATION__SELECTED;

	/**
	 * The feature id for the '<em><b>Shadow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__SHADOW_MODE = FIELD_OF_VIEW_PRESENTATION__SHADOW_MODE;

	/**
	 * The feature id for the '<em><b>Use In Bounding Calculation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__USE_IN_BOUNDING_CALCULATION = FIELD_OF_VIEW_PRESENTATION__USE_IN_BOUNDING_CALCULATION;

	/**
	 * The feature id for the '<em><b>Id Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__ID_VISIBLE = FIELD_OF_VIEW_PRESENTATION__ID_VISIBLE;

	/**
	 * The feature id for the '<em><b>Enable Texture Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__ENABLE_TEXTURE_PROJECTION = FIELD_OF_VIEW_PRESENTATION__ENABLE_TEXTURE_PROJECTION;

	/**
	 * The feature id for the '<em><b>Centroid</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__CENTROID = FIELD_OF_VIEW_PRESENTATION__CENTROID;

	/**
	 * The feature id for the '<em><b>Min</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__MIN = FIELD_OF_VIEW_PRESENTATION__MIN;

	/**
	 * The feature id for the '<em><b>Max</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__MAX = FIELD_OF_VIEW_PRESENTATION__MAX;

	/**
	 * The feature id for the '<em><b>XRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__XRANGE = FIELD_OF_VIEW_PRESENTATION__XRANGE;

	/**
	 * The feature id for the '<em><b>YRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__YRANGE = FIELD_OF_VIEW_PRESENTATION__YRANGE;

	/**
	 * The feature id for the '<em><b>ZRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__ZRANGE = FIELD_OF_VIEW_PRESENTATION__ZRANGE;

	/**
	 * The feature id for the '<em><b>Scene Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__SCENE_OBJECT = FIELD_OF_VIEW_PRESENTATION__SCENE_OBJECT;

	/**
	 * The feature id for the '<em><b>Transparency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__TRANSPARENCY = FIELD_OF_VIEW_PRESENTATION__TRANSPARENCY;

	/**
	 * The feature id for the '<em><b>Presentation Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__PRESENTATION_MODE = FIELD_OF_VIEW_PRESENTATION__PRESENTATION_MODE;

	/**
	 * The feature id for the '<em><b>Show Outline Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__SHOW_OUTLINE_ONLY = FIELD_OF_VIEW_PRESENTATION__SHOW_OUTLINE_ONLY;

	/**
	 * The feature id for the '<em><b>Show Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__SHOW_PROJECTION = FIELD_OF_VIEW_PRESENTATION__SHOW_PROJECTION;

	/**
	 * The feature id for the '<em><b>Projection Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__PROJECTION_COLOR = FIELD_OF_VIEW_PRESENTATION__PROJECTION_COLOR;

	/**
	 * The feature id for the '<em><b>Fov Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__FOV_VISIBLE = FIELD_OF_VIEW_PRESENTATION__FOV_VISIBLE;

	/**
	 * The feature id for the '<em><b>Axis Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__AXIS_VISIBLE = FIELD_OF_VIEW_PRESENTATION__AXIS_VISIBLE;

	/**
	 * The feature id for the '<em><b>Axis Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION__AXIS_LENGTH = FIELD_OF_VIEW_PRESENTATION__AXIS_LENGTH;

	/**
	 * The number of structural features of the '<em>Circular Sector Field Of View Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION_FEATURE_COUNT = FIELD_OF_VIEW_PRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Circular Sector Field Of View Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION_OPERATION_COUNT = FIELD_OF_VIEW_PRESENTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ConicalFieldOfViewPresentationImpl <em>Conical Field Of View Presentation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ConicalFieldOfViewPresentationImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getConicalFieldOfViewPresentation()
	 * @generated
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION = 2;

	/**
	 * The feature id for the '<em><b>Topology Presentation Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__TOPOLOGY_PRESENTATION_SET = FIELD_OF_VIEW_PRESENTATION__TOPOLOGY_PRESENTATION_SET;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__NODE = FIELD_OF_VIEW_PRESENTATION__NODE;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__COLOR = FIELD_OF_VIEW_PRESENTATION__COLOR;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__VISIBLE = FIELD_OF_VIEW_PRESENTATION__VISIBLE;

	/**
	 * The feature id for the '<em><b>Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__SELECTED = FIELD_OF_VIEW_PRESENTATION__SELECTED;

	/**
	 * The feature id for the '<em><b>Shadow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__SHADOW_MODE = FIELD_OF_VIEW_PRESENTATION__SHADOW_MODE;

	/**
	 * The feature id for the '<em><b>Use In Bounding Calculation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__USE_IN_BOUNDING_CALCULATION = FIELD_OF_VIEW_PRESENTATION__USE_IN_BOUNDING_CALCULATION;

	/**
	 * The feature id for the '<em><b>Id Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__ID_VISIBLE = FIELD_OF_VIEW_PRESENTATION__ID_VISIBLE;

	/**
	 * The feature id for the '<em><b>Enable Texture Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__ENABLE_TEXTURE_PROJECTION = FIELD_OF_VIEW_PRESENTATION__ENABLE_TEXTURE_PROJECTION;

	/**
	 * The feature id for the '<em><b>Centroid</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__CENTROID = FIELD_OF_VIEW_PRESENTATION__CENTROID;

	/**
	 * The feature id for the '<em><b>Min</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__MIN = FIELD_OF_VIEW_PRESENTATION__MIN;

	/**
	 * The feature id for the '<em><b>Max</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__MAX = FIELD_OF_VIEW_PRESENTATION__MAX;

	/**
	 * The feature id for the '<em><b>XRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__XRANGE = FIELD_OF_VIEW_PRESENTATION__XRANGE;

	/**
	 * The feature id for the '<em><b>YRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__YRANGE = FIELD_OF_VIEW_PRESENTATION__YRANGE;

	/**
	 * The feature id for the '<em><b>ZRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__ZRANGE = FIELD_OF_VIEW_PRESENTATION__ZRANGE;

	/**
	 * The feature id for the '<em><b>Scene Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__SCENE_OBJECT = FIELD_OF_VIEW_PRESENTATION__SCENE_OBJECT;

	/**
	 * The feature id for the '<em><b>Transparency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__TRANSPARENCY = FIELD_OF_VIEW_PRESENTATION__TRANSPARENCY;

	/**
	 * The feature id for the '<em><b>Presentation Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__PRESENTATION_MODE = FIELD_OF_VIEW_PRESENTATION__PRESENTATION_MODE;

	/**
	 * The feature id for the '<em><b>Show Outline Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__SHOW_OUTLINE_ONLY = FIELD_OF_VIEW_PRESENTATION__SHOW_OUTLINE_ONLY;

	/**
	 * The feature id for the '<em><b>Show Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__SHOW_PROJECTION = FIELD_OF_VIEW_PRESENTATION__SHOW_PROJECTION;

	/**
	 * The feature id for the '<em><b>Projection Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__PROJECTION_COLOR = FIELD_OF_VIEW_PRESENTATION__PROJECTION_COLOR;

	/**
	 * The feature id for the '<em><b>Fov Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__FOV_VISIBLE = FIELD_OF_VIEW_PRESENTATION__FOV_VISIBLE;

	/**
	 * The feature id for the '<em><b>Axis Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__AXIS_VISIBLE = FIELD_OF_VIEW_PRESENTATION__AXIS_VISIBLE;

	/**
	 * The feature id for the '<em><b>Axis Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION__AXIS_LENGTH = FIELD_OF_VIEW_PRESENTATION__AXIS_LENGTH;

	/**
	 * The number of structural features of the '<em>Conical Field Of View Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION_FEATURE_COUNT = FIELD_OF_VIEW_PRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Conical Field Of View Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_PRESENTATION_OPERATION_COUNT = FIELD_OF_VIEW_PRESENTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.RectangularFrustrumFieldOfViewPresentationImpl <em>Rectangular Frustrum Field Of View Presentation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.RectangularFrustrumFieldOfViewPresentationImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getRectangularFrustrumFieldOfViewPresentation()
	 * @generated
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION = 3;

	/**
	 * The feature id for the '<em><b>Topology Presentation Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__TOPOLOGY_PRESENTATION_SET = FIELD_OF_VIEW_PRESENTATION__TOPOLOGY_PRESENTATION_SET;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__NODE = FIELD_OF_VIEW_PRESENTATION__NODE;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__COLOR = FIELD_OF_VIEW_PRESENTATION__COLOR;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__VISIBLE = FIELD_OF_VIEW_PRESENTATION__VISIBLE;

	/**
	 * The feature id for the '<em><b>Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__SELECTED = FIELD_OF_VIEW_PRESENTATION__SELECTED;

	/**
	 * The feature id for the '<em><b>Shadow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__SHADOW_MODE = FIELD_OF_VIEW_PRESENTATION__SHADOW_MODE;

	/**
	 * The feature id for the '<em><b>Use In Bounding Calculation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__USE_IN_BOUNDING_CALCULATION = FIELD_OF_VIEW_PRESENTATION__USE_IN_BOUNDING_CALCULATION;

	/**
	 * The feature id for the '<em><b>Id Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__ID_VISIBLE = FIELD_OF_VIEW_PRESENTATION__ID_VISIBLE;

	/**
	 * The feature id for the '<em><b>Enable Texture Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__ENABLE_TEXTURE_PROJECTION = FIELD_OF_VIEW_PRESENTATION__ENABLE_TEXTURE_PROJECTION;

	/**
	 * The feature id for the '<em><b>Centroid</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__CENTROID = FIELD_OF_VIEW_PRESENTATION__CENTROID;

	/**
	 * The feature id for the '<em><b>Min</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__MIN = FIELD_OF_VIEW_PRESENTATION__MIN;

	/**
	 * The feature id for the '<em><b>Max</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__MAX = FIELD_OF_VIEW_PRESENTATION__MAX;

	/**
	 * The feature id for the '<em><b>XRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__XRANGE = FIELD_OF_VIEW_PRESENTATION__XRANGE;

	/**
	 * The feature id for the '<em><b>YRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__YRANGE = FIELD_OF_VIEW_PRESENTATION__YRANGE;

	/**
	 * The feature id for the '<em><b>ZRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__ZRANGE = FIELD_OF_VIEW_PRESENTATION__ZRANGE;

	/**
	 * The feature id for the '<em><b>Scene Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__SCENE_OBJECT = FIELD_OF_VIEW_PRESENTATION__SCENE_OBJECT;

	/**
	 * The feature id for the '<em><b>Transparency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__TRANSPARENCY = FIELD_OF_VIEW_PRESENTATION__TRANSPARENCY;

	/**
	 * The feature id for the '<em><b>Presentation Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__PRESENTATION_MODE = FIELD_OF_VIEW_PRESENTATION__PRESENTATION_MODE;

	/**
	 * The feature id for the '<em><b>Show Outline Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__SHOW_OUTLINE_ONLY = FIELD_OF_VIEW_PRESENTATION__SHOW_OUTLINE_ONLY;

	/**
	 * The feature id for the '<em><b>Show Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__SHOW_PROJECTION = FIELD_OF_VIEW_PRESENTATION__SHOW_PROJECTION;

	/**
	 * The feature id for the '<em><b>Projection Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__PROJECTION_COLOR = FIELD_OF_VIEW_PRESENTATION__PROJECTION_COLOR;

	/**
	 * The feature id for the '<em><b>Fov Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__FOV_VISIBLE = FIELD_OF_VIEW_PRESENTATION__FOV_VISIBLE;

	/**
	 * The feature id for the '<em><b>Axis Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__AXIS_VISIBLE = FIELD_OF_VIEW_PRESENTATION__AXIS_VISIBLE;

	/**
	 * The feature id for the '<em><b>Axis Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION__AXIS_LENGTH = FIELD_OF_VIEW_PRESENTATION__AXIS_LENGTH;

	/**
	 * The number of structural features of the '<em>Rectangular Frustrum Field Of View Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION_FEATURE_COUNT = FIELD_OF_VIEW_PRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Rectangular Frustrum Field Of View Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION_OPERATION_COUNT = FIELD_OF_VIEW_PRESENTATION_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl <em>Field Of View Entry3 DTool</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewEntry3DTool()
	 * @generated
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__NAME = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__DESCRIPTION = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tool List</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__TOOL_LIST = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TOOL_LIST;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__ACTIVE = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__ACTIVE;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__DISPOSED = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__DISPOSED;

	/**
	 * The feature id for the '<em><b>Initialized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__INITIALIZED = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__INITIALIZED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__VISIBLE = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__VISIBLE;

	/**
	 * The feature id for the '<em><b>Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__ROOT_NODE = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__ROOT_NODE;

	/**
	 * The feature id for the '<em><b>From Absolute Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__FROM_ABSOLUTE_POSITION = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION;

	/**
	 * The feature id for the '<em><b>From Relative Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__FROM_RELATIVE_POSITION = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION;

	/**
	 * The feature id for the '<em><b>From Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__FROM_NODE = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE;

	/**
	 * The feature id for the '<em><b>From Node Node Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__FROM_NODE_NODE_PATH = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH;

	/**
	 * The feature id for the '<em><b>From Node Lock</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__FROM_NODE_LOCK = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_LOCK;

	/**
	 * The feature id for the '<em><b>To Absolute Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__TO_ABSOLUTE_POSITION = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION;

	/**
	 * The feature id for the '<em><b>To Relative Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__TO_RELATIVE_POSITION = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION;

	/**
	 * The feature id for the '<em><b>To Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__TO_NODE = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE;

	/**
	 * The feature id for the '<em><b>To Node Node Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__TO_NODE_NODE_PATH = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH;

	/**
	 * The feature id for the '<em><b>To Node Lock</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__TO_NODE_LOCK = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_LOCK;

	/**
	 * The feature id for the '<em><b>Distance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__DISTANCE = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__DISTANCE;

	/**
	 * The feature id for the '<em><b>Target Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Fov</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__FOV = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Field Of View Entry3 DTool Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Current Vector Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Default Vector Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__DEFAULT_VECTOR_COLOR = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Vector Color Selection Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_VISIBLE = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Vector Color Selection Entered</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_ENTERED = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Maximum Vector Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Vector Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_DIAMETER = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Current Vector Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_LENGTH = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>Field Of View Entry3 DTool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_FEATURE_COUNT = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_FEATURE_COUNT + 10;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL___INITIALISE = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL___DISPOSE = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL___DISPOSE;

	/**
	 * The operation id for the '<em>Variables Instantiated</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL___VARIABLES_INSTANTIATED = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL___VARIABLES_INSTANTIATED;

	/**
	 * The operation id for the '<em>Variables Cleared</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL___VARIABLES_CLEARED = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL___VARIABLES_CLEARED;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL___SELECTION_CHANGED__NODESELECTION = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL___SELECTION_CHANGED__NODESELECTION;

	/**
	 * The operation id for the '<em>Mouse Button Clicked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL___MOUSE_BUTTON_CLICKED__MOUSEBUTTON = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL___MOUSE_BUTTON_CLICKED__MOUSEBUTTON;

	/**
	 * The operation id for the '<em>Points Relative Pose Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL___POINTS_RELATIVE_POSE_CHANGED__MATRIX4D = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL___POINTS_RELATIVE_POSE_CHANGED__MATRIX4D;

	/**
	 * The number of operations of the '<em>Field Of View Entry3 DTool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_OPERATION_COUNT = ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolNodeImpl <em>Field Of View Entry3 DTool Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolNodeImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewEntry3DToolNode()
	 * @generated
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_NODE = 5;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__PARENT = ApogyCommonTopologyPackage.NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__DESCRIPTION = ApogyCommonTopologyPackage.NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__NODE_ID = ApogyCommonTopologyPackage.NODE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Field Of View Entry3 DTool</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL = ApogyCommonTopologyPackage.NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Field Of View Entry3 DTool Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_NODE_FEATURE_COUNT = ApogyCommonTopologyPackage.NODE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Accept</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_NODE___ACCEPT__INODEVISITOR = ApogyCommonTopologyPackage.NODE___ACCEPT__INODEVISITOR;

	/**
	 * The number of operations of the '<em>Field Of View Entry3 DTool Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_NODE_OPERATION_COUNT = ApogyCommonTopologyPackage.NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewWizardPagesProviderImpl <em>Field Of View Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewWizardPagesProvider()
	 * @generated
	 */
	int FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER = 6;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Field Of View Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Field Of View Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyUIPackage.NODE_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.CircularSectorFieldOfViewWizardPagesProviderImpl <em>Circular Sector Field Of View Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.CircularSectorFieldOfViewWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getCircularSectorFieldOfViewWizardPagesProvider()
	 * @generated
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER = 7;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__PAGES = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__EOBJECT = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Circular Sector Field Of View Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Circular Sector Field Of View Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ConicalFieldOfViewWizardPagesProviderImpl <em>Conical Field Of View Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ConicalFieldOfViewWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getConicalFieldOfViewWizardPagesProvider()
	 * @generated
	 */
	int CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER = 8;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__PAGES = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__EOBJECT = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Conical Field Of View Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Conical Field Of View Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.RectangularFrustrumFieldOfViewWizardPagesProviderImpl <em>Rectangular Frustrum Field Of View Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.RectangularFrustrumFieldOfViewWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getRectangularFrustrumFieldOfViewWizardPagesProvider()
	 * @generated
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER = 9;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__PAGES = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__EOBJECT = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Rectangular Frustrum Field Of View Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Rectangular Frustrum Field Of View Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolWizardPagesProviderImpl <em>Field Of View Entry3 DTool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewEntry3DToolWizardPagesProvider()
	 * @generated
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER = 10;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Field Of View Entry3 DTool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Field Of View Entry3 DTool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolTargetVisibility <em>Field Of View Entry3 DTool Target Visibility</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolTargetVisibility
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewEntry3DToolTargetVisibility()
	 * @generated
	 */
	int FIELD_OF_VIEW_ENTRY3_DTOOL_TARGET_VISIBILITY = 11;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation <em>Field Of View Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field Of View Presentation</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation
	 * @generated
	 */
	EClass getFieldOfViewPresentation();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#getTransparency <em>Transparency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transparency</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#getTransparency()
	 * @see #getFieldOfViewPresentation()
	 * @generated
	 */
	EAttribute getFieldOfViewPresentation_Transparency();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#getPresentationMode <em>Presentation Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Presentation Mode</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#getPresentationMode()
	 * @see #getFieldOfViewPresentation()
	 * @generated
	 */
	EAttribute getFieldOfViewPresentation_PresentationMode();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#isShowOutlineOnly <em>Show Outline Only</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Outline Only</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#isShowOutlineOnly()
	 * @see #getFieldOfViewPresentation()
	 * @generated
	 */
	EAttribute getFieldOfViewPresentation_ShowOutlineOnly();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#isShowProjection <em>Show Projection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Projection</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#isShowProjection()
	 * @see #getFieldOfViewPresentation()
	 * @generated
	 */
	EAttribute getFieldOfViewPresentation_ShowProjection();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#getProjectionColor <em>Projection Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Projection Color</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#getProjectionColor()
	 * @see #getFieldOfViewPresentation()
	 * @generated
	 */
	EAttribute getFieldOfViewPresentation_ProjectionColor();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#isFovVisible <em>Fov Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fov Visible</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#isFovVisible()
	 * @see #getFieldOfViewPresentation()
	 * @generated
	 */
	EAttribute getFieldOfViewPresentation_FovVisible();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#isAxisVisible <em>Axis Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Axis Visible</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#isAxisVisible()
	 * @see #getFieldOfViewPresentation()
	 * @generated
	 */
	EAttribute getFieldOfViewPresentation_AxisVisible();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#getAxisLength <em>Axis Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Axis Length</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation#getAxisLength()
	 * @see #getFieldOfViewPresentation()
	 * @generated
	 */
	EAttribute getFieldOfViewPresentation_AxisLength();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewPresentation <em>Circular Sector Field Of View Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Circular Sector Field Of View Presentation</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewPresentation
	 * @generated
	 */
	EClass getCircularSectorFieldOfViewPresentation();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewPresentation <em>Conical Field Of View Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conical Field Of View Presentation</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewPresentation
	 * @generated
	 */
	EClass getConicalFieldOfViewPresentation();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewPresentation <em>Rectangular Frustrum Field Of View Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rectangular Frustrum Field Of View Presentation</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewPresentation
	 * @generated
	 */
	EClass getRectangularFrustrumFieldOfViewPresentation();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool <em>Field Of View Entry3 DTool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field Of View Entry3 DTool</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool
	 * @generated
	 */
	EClass getFieldOfViewEntry3DTool();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getTargetVisibility <em>Target Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Visibility</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getTargetVisibility()
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 */
	EAttribute getFieldOfViewEntry3DTool_TargetVisibility();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getFov <em>Fov</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Fov</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getFov()
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 */
	EReference getFieldOfViewEntry3DTool_Fov();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getFieldOfViewEntry3DToolNode <em>Field Of View Entry3 DTool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Field Of View Entry3 DTool Node</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getFieldOfViewEntry3DToolNode()
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 */
	EReference getFieldOfViewEntry3DTool_FieldOfViewEntry3DToolNode();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getCurrentVectorColor <em>Current Vector Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Current Vector Color</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getCurrentVectorColor()
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 */
	EAttribute getFieldOfViewEntry3DTool_CurrentVectorColor();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getDefaultVectorColor <em>Default Vector Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Vector Color</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getDefaultVectorColor()
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 */
	EAttribute getFieldOfViewEntry3DTool_DefaultVectorColor();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorColorSelectionVisible <em>Vector Color Selection Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vector Color Selection Visible</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorColorSelectionVisible()
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 */
	EAttribute getFieldOfViewEntry3DTool_VectorColorSelectionVisible();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorColorSelectionEntered <em>Vector Color Selection Entered</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vector Color Selection Entered</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorColorSelectionEntered()
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 */
	EAttribute getFieldOfViewEntry3DTool_VectorColorSelectionEntered();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getMaximumVectorLength <em>Maximum Vector Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maximum Vector Length</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getMaximumVectorLength()
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 */
	EAttribute getFieldOfViewEntry3DTool_MaximumVectorLength();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorDiameter <em>Vector Diameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vector Diameter</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorDiameter()
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 */
	EAttribute getFieldOfViewEntry3DTool_VectorDiameter();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getCurrentVectorLength <em>Current Vector Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Current Vector Length</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getCurrentVectorLength()
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 */
	EAttribute getFieldOfViewEntry3DTool_CurrentVectorLength();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode <em>Field Of View Entry3 DTool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field Of View Entry3 DTool Node</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode
	 * @generated
	 */
	EClass getFieldOfViewEntry3DToolNode();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode#getFieldOfViewEntry3DTool <em>Field Of View Entry3 DTool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Field Of View Entry3 DTool</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode#getFieldOfViewEntry3DTool()
	 * @see #getFieldOfViewEntry3DToolNode()
	 * @generated
	 */
	EReference getFieldOfViewEntry3DToolNode_FieldOfViewEntry3DTool();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewWizardPagesProvider <em>Field Of View Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return the meta object for class '<em>Field Of View Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewWizardPagesProvider
	 * @generated
	 */
	EClass getFieldOfViewWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewWizardPagesProvider <em>Circular Sector Field Of View Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return the meta object for class '<em>Circular Sector Field Of View Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewWizardPagesProvider
	 * @generated
	 */
	EClass getCircularSectorFieldOfViewWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewWizardPagesProvider <em>Conical Field Of View Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return the meta object for class '<em>Conical Field Of View Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewWizardPagesProvider
	 * @generated
	 */
	EClass getConicalFieldOfViewWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewWizardPagesProvider <em>Rectangular Frustrum Field Of View Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return the meta object for class '<em>Rectangular Frustrum Field Of View Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewWizardPagesProvider
	 * @generated
	 */
	EClass getRectangularFrustrumFieldOfViewWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolWizardPagesProvider <em>Field Of View Entry3 DTool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field Of View Entry3 DTool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolWizardPagesProvider
	 * @generated
	 */
	EClass getFieldOfViewEntry3DToolWizardPagesProvider();

	/**
	 * Returns the meta object for enum '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolTargetVisibility <em>Field Of View Entry3 DTool Target Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Field Of View Entry3 DTool Target Visibility</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolTargetVisibility
	 * @generated
	 */
	EEnum getFieldOfViewEntry3DToolTargetVisibility();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyAddonsSensorsFOVUIFactory getApogyAddonsSensorsFOVUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewPresentationImpl <em>Field Of View Presentation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewPresentationImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewPresentation()
		 * @generated
		 */
		EClass FIELD_OF_VIEW_PRESENTATION = eINSTANCE.getFieldOfViewPresentation();

		/**
		 * The meta object literal for the '<em><b>Transparency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_PRESENTATION__TRANSPARENCY = eINSTANCE.getFieldOfViewPresentation_Transparency();

		/**
		 * The meta object literal for the '<em><b>Presentation Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_PRESENTATION__PRESENTATION_MODE = eINSTANCE.getFieldOfViewPresentation_PresentationMode();

		/**
		 * The meta object literal for the '<em><b>Show Outline Only</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_PRESENTATION__SHOW_OUTLINE_ONLY = eINSTANCE.getFieldOfViewPresentation_ShowOutlineOnly();

		/**
		 * The meta object literal for the '<em><b>Show Projection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_PRESENTATION__SHOW_PROJECTION = eINSTANCE.getFieldOfViewPresentation_ShowProjection();

		/**
		 * The meta object literal for the '<em><b>Projection Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_PRESENTATION__PROJECTION_COLOR = eINSTANCE.getFieldOfViewPresentation_ProjectionColor();

		/**
		 * The meta object literal for the '<em><b>Fov Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_PRESENTATION__FOV_VISIBLE = eINSTANCE.getFieldOfViewPresentation_FovVisible();

		/**
		 * The meta object literal for the '<em><b>Axis Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_PRESENTATION__AXIS_VISIBLE = eINSTANCE.getFieldOfViewPresentation_AxisVisible();

		/**
		 * The meta object literal for the '<em><b>Axis Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_PRESENTATION__AXIS_LENGTH = eINSTANCE.getFieldOfViewPresentation_AxisLength();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.CircularSectorFieldOfViewPresentationImpl <em>Circular Sector Field Of View Presentation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.CircularSectorFieldOfViewPresentationImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getCircularSectorFieldOfViewPresentation()
		 * @generated
		 */
		EClass CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION = eINSTANCE.getCircularSectorFieldOfViewPresentation();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ConicalFieldOfViewPresentationImpl <em>Conical Field Of View Presentation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ConicalFieldOfViewPresentationImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getConicalFieldOfViewPresentation()
		 * @generated
		 */
		EClass CONICAL_FIELD_OF_VIEW_PRESENTATION = eINSTANCE.getConicalFieldOfViewPresentation();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.RectangularFrustrumFieldOfViewPresentationImpl <em>Rectangular Frustrum Field Of View Presentation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.RectangularFrustrumFieldOfViewPresentationImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getRectangularFrustrumFieldOfViewPresentation()
		 * @generated
		 */
		EClass RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION = eINSTANCE.getRectangularFrustrumFieldOfViewPresentation();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl <em>Field Of View Entry3 DTool</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewEntry3DTool()
		 * @generated
		 */
		EClass FIELD_OF_VIEW_ENTRY3_DTOOL = eINSTANCE.getFieldOfViewEntry3DTool();

		/**
		 * The meta object literal for the '<em><b>Target Visibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_ENTRY3_DTOOL__TARGET_VISIBILITY = eINSTANCE.getFieldOfViewEntry3DTool_TargetVisibility();

		/**
		 * The meta object literal for the '<em><b>Fov</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIELD_OF_VIEW_ENTRY3_DTOOL__FOV = eINSTANCE.getFieldOfViewEntry3DTool_Fov();

		/**
		 * The meta object literal for the '<em><b>Field Of View Entry3 DTool Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE = eINSTANCE.getFieldOfViewEntry3DTool_FieldOfViewEntry3DToolNode();

		/**
		 * The meta object literal for the '<em><b>Current Vector Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_COLOR = eINSTANCE.getFieldOfViewEntry3DTool_CurrentVectorColor();

		/**
		 * The meta object literal for the '<em><b>Default Vector Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_ENTRY3_DTOOL__DEFAULT_VECTOR_COLOR = eINSTANCE.getFieldOfViewEntry3DTool_DefaultVectorColor();

		/**
		 * The meta object literal for the '<em><b>Vector Color Selection Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_VISIBLE = eINSTANCE.getFieldOfViewEntry3DTool_VectorColorSelectionVisible();

		/**
		 * The meta object literal for the '<em><b>Vector Color Selection Entered</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_COLOR_SELECTION_ENTERED = eINSTANCE.getFieldOfViewEntry3DTool_VectorColorSelectionEntered();

		/**
		 * The meta object literal for the '<em><b>Maximum Vector Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_ENTRY3_DTOOL__MAXIMUM_VECTOR_LENGTH = eINSTANCE.getFieldOfViewEntry3DTool_MaximumVectorLength();

		/**
		 * The meta object literal for the '<em><b>Vector Diameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_ENTRY3_DTOOL__VECTOR_DIAMETER = eINSTANCE.getFieldOfViewEntry3DTool_VectorDiameter();

		/**
		 * The meta object literal for the '<em><b>Current Vector Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_OF_VIEW_ENTRY3_DTOOL__CURRENT_VECTOR_LENGTH = eINSTANCE.getFieldOfViewEntry3DTool_CurrentVectorLength();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolNodeImpl <em>Field Of View Entry3 DTool Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolNodeImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewEntry3DToolNode()
		 * @generated
		 */
		EClass FIELD_OF_VIEW_ENTRY3_DTOOL_NODE = eINSTANCE.getFieldOfViewEntry3DToolNode();

		/**
		 * The meta object literal for the '<em><b>Field Of View Entry3 DTool</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL = eINSTANCE.getFieldOfViewEntry3DToolNode_FieldOfViewEntry3DTool();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewWizardPagesProviderImpl <em>Field Of View Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->

		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewWizardPagesProvider()
		 * @generated
		 */
		EClass FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER = eINSTANCE.getFieldOfViewWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.CircularSectorFieldOfViewWizardPagesProviderImpl <em>Circular Sector Field Of View Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->

		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.CircularSectorFieldOfViewWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getCircularSectorFieldOfViewWizardPagesProvider()
		 * @generated
		 */
		EClass CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER = eINSTANCE.getCircularSectorFieldOfViewWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ConicalFieldOfViewWizardPagesProviderImpl <em>Conical Field Of View Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->

		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ConicalFieldOfViewWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getConicalFieldOfViewWizardPagesProvider()
		 * @generated
		 */
		EClass CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER = eINSTANCE.getConicalFieldOfViewWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.RectangularFrustrumFieldOfViewWizardPagesProviderImpl <em>Rectangular Frustrum Field Of View Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->

		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.RectangularFrustrumFieldOfViewWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getRectangularFrustrumFieldOfViewWizardPagesProvider()
		 * @generated
		 */
		EClass RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER = eINSTANCE.getRectangularFrustrumFieldOfViewWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolWizardPagesProviderImpl <em>Field Of View Entry3 DTool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewEntry3DToolWizardPagesProvider()
		 * @generated
		 */
		EClass FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getFieldOfViewEntry3DToolWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolTargetVisibility <em>Field Of View Entry3 DTool Target Visibility</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolTargetVisibility
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIPackageImpl#getFieldOfViewEntry3DToolTargetVisibility()
		 * @generated
		 */
		EEnum FIELD_OF_VIEW_ENTRY3_DTOOL_TARGET_VISIBILITY = eINSTANCE.getFieldOfViewEntry3DToolTargetVisibility();

	}

} //ApogyAddonsSensorsFOVUIPackage

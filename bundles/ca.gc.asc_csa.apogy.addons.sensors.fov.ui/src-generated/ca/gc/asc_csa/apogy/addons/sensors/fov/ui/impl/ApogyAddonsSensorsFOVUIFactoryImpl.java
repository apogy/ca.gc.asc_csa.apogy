package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewPresentation;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewPresentation;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIFactory;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewPresentation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyAddonsSensorsFOVUIFactoryImpl extends EFactoryImpl implements ApogyAddonsSensorsFOVUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyAddonsSensorsFOVUIFactory init() {
		try {
			ApogyAddonsSensorsFOVUIFactory theApogyAddonsSensorsFOVUIFactory = (ApogyAddonsSensorsFOVUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyAddonsSensorsFOVUIPackage.eNS_URI);
			if (theApogyAddonsSensorsFOVUIFactory != null) {
				return theApogyAddonsSensorsFOVUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyAddonsSensorsFOVUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsSensorsFOVUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_PRESENTATION: return createFieldOfViewPresentation();
			case ApogyAddonsSensorsFOVUIPackage.CIRCULAR_SECTOR_FIELD_OF_VIEW_PRESENTATION: return createCircularSectorFieldOfViewPresentation();
			case ApogyAddonsSensorsFOVUIPackage.CONICAL_FIELD_OF_VIEW_PRESENTATION: return createConicalFieldOfViewPresentation();
			case ApogyAddonsSensorsFOVUIPackage.RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_PRESENTATION: return createRectangularFrustrumFieldOfViewPresentation();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL: return createFieldOfViewEntry3DTool();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE: return createFieldOfViewEntry3DToolNode();
			case ApogyAddonsSensorsFOVUIPackage.CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER: return createCircularSectorFieldOfViewWizardPagesProvider();
			case ApogyAddonsSensorsFOVUIPackage.CONICAL_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER: return createConicalFieldOfViewWizardPagesProvider();
			case ApogyAddonsSensorsFOVUIPackage.RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER: return createRectangularFrustrumFieldOfViewWizardPagesProvider();
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER: return createFieldOfViewEntry3DToolWizardPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_TARGET_VISIBILITY:
				return createFieldOfViewEntry3DToolTargetVisibilityFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_TARGET_VISIBILITY:
				return convertFieldOfViewEntry3DToolTargetVisibilityToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfViewPresentation createFieldOfViewPresentation() {
		FieldOfViewPresentationImpl fieldOfViewPresentation = new FieldOfViewPresentationImpl();
		return fieldOfViewPresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CircularSectorFieldOfViewPresentation createCircularSectorFieldOfViewPresentation() {
		CircularSectorFieldOfViewPresentationImpl circularSectorFieldOfViewPresentation = new CircularSectorFieldOfViewPresentationImpl();
		return circularSectorFieldOfViewPresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConicalFieldOfViewPresentation createConicalFieldOfViewPresentation() {
		ConicalFieldOfViewPresentationImpl conicalFieldOfViewPresentation = new ConicalFieldOfViewPresentationImpl();
		return conicalFieldOfViewPresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RectangularFrustrumFieldOfViewPresentation createRectangularFrustrumFieldOfViewPresentation() {
		RectangularFrustrumFieldOfViewPresentationImpl rectangularFrustrumFieldOfViewPresentation = new RectangularFrustrumFieldOfViewPresentationImpl();
		return rectangularFrustrumFieldOfViewPresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfViewEntry3DTool createFieldOfViewEntry3DTool() {
		FieldOfViewEntry3DToolImpl fieldOfViewEntry3DTool = new FieldOfViewEntry3DToolImpl();
		return fieldOfViewEntry3DTool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfViewEntry3DToolNode createFieldOfViewEntry3DToolNode() {
		FieldOfViewEntry3DToolNodeImpl fieldOfViewEntry3DToolNode = new FieldOfViewEntry3DToolNodeImpl();
		return fieldOfViewEntry3DToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	public CircularSectorFieldOfViewWizardPagesProvider createCircularSectorFieldOfViewWizardPagesProvider() {
		CircularSectorFieldOfViewWizardPagesProviderImpl circularSectorFieldOfViewWizardPagesProvider = new CircularSectorFieldOfViewWizardPagesProviderImpl();
		return circularSectorFieldOfViewWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	public ConicalFieldOfViewWizardPagesProvider createConicalFieldOfViewWizardPagesProvider() {
		ConicalFieldOfViewWizardPagesProviderImpl conicalFieldOfViewWizardPagesProvider = new ConicalFieldOfViewWizardPagesProviderImpl();
		return conicalFieldOfViewWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @generated
	 */
	public RectangularFrustrumFieldOfViewWizardPagesProvider createRectangularFrustrumFieldOfViewWizardPagesProvider() {
		RectangularFrustrumFieldOfViewWizardPagesProviderImpl rectangularFrustrumFieldOfViewWizardPagesProvider = new RectangularFrustrumFieldOfViewWizardPagesProviderImpl();
		return rectangularFrustrumFieldOfViewWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfViewEntry3DToolWizardPagesProvider createFieldOfViewEntry3DToolWizardPagesProvider() {
		FieldOfViewEntry3DToolWizardPagesProviderImpl fieldOfViewEntry3DToolWizardPagesProvider = new FieldOfViewEntry3DToolWizardPagesProviderImpl();
		return fieldOfViewEntry3DToolWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfViewEntry3DToolTargetVisibility createFieldOfViewEntry3DToolTargetVisibilityFromString(EDataType eDataType, String initialValue) {
		FieldOfViewEntry3DToolTargetVisibility result = FieldOfViewEntry3DToolTargetVisibility.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFieldOfViewEntry3DToolTargetVisibilityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsSensorsFOVUIPackage getApogyAddonsSensorsFOVUIPackage() {
		return (ApogyAddonsSensorsFOVUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyAddonsSensorsFOVUIPackage getPackage() {
		return ApogyAddonsSensorsFOVUIPackage.eINSTANCE;
	}

} //ApogyAddonsSensorsFOVUIFactoryImpl

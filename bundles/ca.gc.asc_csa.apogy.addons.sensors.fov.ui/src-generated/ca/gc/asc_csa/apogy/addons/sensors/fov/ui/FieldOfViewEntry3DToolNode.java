/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *      Regent L'Archeveque
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui;

import ca.gc.asc_csa.apogy.common.topology.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Of View Entry3 DTool Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Node that represent the FieldOfViewVectorEntry3DTool in the topology.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode#getFieldOfViewEntry3DTool <em>Field Of View Entry3 DTool</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DToolNode()
 * @model
 * @generated
 */
public interface FieldOfViewEntry3DToolNode extends Node {
	/**
	 * Returns the value of the '<em><b>Field Of View Entry3 DTool</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getFieldOfViewEntry3DToolNode <em>Field Of View Entry3 DTool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Field Of View Entry3 DTool</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Field Of View Entry3 DTool</em>' reference.
	 * @see #setFieldOfViewEntry3DTool(FieldOfViewEntry3DTool)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DToolNode_FieldOfViewEntry3DTool()
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getFieldOfViewEntry3DToolNode
	 * @model opposite="fieldOfViewEntry3DToolNode" transient="true"
	 * @generated
	 */
	FieldOfViewEntry3DTool getFieldOfViewEntry3DTool();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode#getFieldOfViewEntry3DTool <em>Field Of View Entry3 DTool</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Field Of View Entry3 DTool</em>' reference.
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 */
	void setFieldOfViewEntry3DTool(FieldOfViewEntry3DTool value);

} // FieldOfViewEntry3DToolNode

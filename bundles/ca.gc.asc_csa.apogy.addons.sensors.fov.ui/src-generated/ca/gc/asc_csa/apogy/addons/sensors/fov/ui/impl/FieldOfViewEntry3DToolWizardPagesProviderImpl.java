/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *      Regent L'Archeveque
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIFactory;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards.FieldOfViewEntry3DToolWizardPage;
import ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings;
import ca.gc.asc_csa.apogy.addons.ui.impl.Simple3DToolWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Field Of View Entry3 DTool Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FieldOfViewEntry3DToolWizardPagesProviderImpl extends Simple3DToolWizardPagesProviderImpl implements FieldOfViewEntry3DToolWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FieldOfViewEntry3DToolWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		FieldOfViewEntry3DTool tool = ApogyAddonsSensorsFOVUIFactory.eINSTANCE.createFieldOfViewEntry3DTool();
		
		tool.setFromNodeLock(false);
		tool.setToNodeLock(false);
		tool.setFromRelativePosition(ApogyCommonMathFacade.INSTANCE.createTuple3d(0, 0, 0));
		tool.setToRelativePosition(ApogyCommonMathFacade.INSTANCE.createTuple3d(0, 0, 0));
		
		if(settings instanceof AbstractToolEClassSettings)
		{
			AbstractToolEClassSettings abstractToolEClassSettings = (AbstractToolEClassSettings) settings;
			tool.setName(abstractToolEClassSettings.getName());
			tool.setDescription(abstractToolEClassSettings.getDescription());
		}
		
		return tool;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();
		list.addAll(super.instantiateWizardPages(eObject, settings));

		FieldOfViewEntry3DTool tool = (FieldOfViewEntry3DTool) eObject;
		list.add(new FieldOfViewEntry3DToolWizardPage(tool));
		
		return list;
	}
} //FieldOfViewEntry3DToolWizardPagesProviderImpl

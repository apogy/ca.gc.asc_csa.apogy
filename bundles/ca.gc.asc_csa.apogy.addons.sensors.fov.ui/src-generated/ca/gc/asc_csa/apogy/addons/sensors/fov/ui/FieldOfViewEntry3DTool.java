/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *      Regent L'Archeveque
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui;

import ca.gc.asc_csa.apogy.addons.AbstractTwoPoints3DTool;

import ca.gc.asc_csa.apogy.addons.sensors.fov.FieldOfView;
import javax.vecmath.Color3f;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Of View Entry3 DTool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Simple 3D tool used to monitor when a selected point enters or becomes visible from a  FieldOfView.
 * The From node is the FieldOfView, the To node the point selected.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getTargetVisibility <em>Target Visibility</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getFov <em>Fov</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getFieldOfViewEntry3DToolNode <em>Field Of View Entry3 DTool Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getCurrentVectorColor <em>Current Vector Color</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getDefaultVectorColor <em>Default Vector Color</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorColorSelectionVisible <em>Vector Color Selection Visible</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorColorSelectionEntered <em>Vector Color Selection Entered</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getMaximumVectorLength <em>Maximum Vector Length</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorDiameter <em>Vector Diameter</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getCurrentVectorLength <em>Current Vector Length</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DTool()
 * @model
 * @generated
 */
public interface FieldOfViewEntry3DTool extends AbstractTwoPoints3DTool {
	/**
	 * Returns the value of the '<em><b>Target Visibility</b></em>' attribute.
	 * The literals are from the enumeration {@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolTargetVisibility}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The state of the target visibility.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Target Visibility</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolTargetVisibility
	 * @see #setTargetVisibility(FieldOfViewEntry3DToolTargetVisibility)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DTool_TargetVisibility()
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' property='Readonly'"
	 * @generated
	 */
	FieldOfViewEntry3DToolTargetVisibility getTargetVisibility();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getTargetVisibility <em>Target Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Visibility</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolTargetVisibility
	 * @see #getTargetVisibility()
	 * @generated
	 */
	void setTargetVisibility(FieldOfViewEntry3DToolTargetVisibility value);

	/**
	 * Returns the value of the '<em><b>Fov</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The  FieldOfView for which to monitor entry.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Fov</em>' reference.
	 * @see #setFov(FieldOfView)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DTool_Fov()
	 * @model transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' children='false' property='Readonly'"
	 * @generated
	 */
	FieldOfView getFov();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getFov <em>Fov</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fov</em>' reference.
	 * @see #getFov()
	 * @generated
	 */
	void setFov(FieldOfView value);

	/**
	 * Returns the value of the '<em><b>Field Of View Entry3 DTool Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode#getFieldOfViewEntry3DTool <em>Field Of View Entry3 DTool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Topology Node associated with the tool. This is the Node that represent the FieldOfViewEntry3DTool in the topology.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Field Of View Entry3 DTool Node</em>' reference.
	 * @see #setFieldOfViewEntry3DToolNode(FieldOfViewEntry3DToolNode)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DTool_FieldOfViewEntry3DToolNode()
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode#getFieldOfViewEntry3DTool
	 * @model opposite="fieldOfViewEntry3DTool" transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' children='false' property='None' propertyCategory='RULER_PROPERTIES'"
	 * @generated
	 */
	FieldOfViewEntry3DToolNode getFieldOfViewEntry3DToolNode();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getFieldOfViewEntry3DToolNode <em>Field Of View Entry3 DTool Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Field Of View Entry3 DTool Node</em>' reference.
	 * @see #getFieldOfViewEntry3DToolNode()
	 * @generated
	 */
	void setFieldOfViewEntry3DToolNode(FieldOfViewEntry3DToolNode value);

	/**
	 * Returns the value of the '<em><b>Current Vector Color</b></em>' attribute.
	 * The default value is <code>"0.0,1.0,0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The current displayed vector color.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Current Vector Color</em>' attribute.
	 * @see #setCurrentVectorColor(Color3f)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DTool_CurrentVectorColor()
	 * @model default="0.0,1.0,0.0" unique="false" dataType="ca.gc.asc_csa.apogy.addons.Color3f"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='None'"
	 * @generated
	 */
	Color3f getCurrentVectorColor();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getCurrentVectorColor <em>Current Vector Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Vector Color</em>' attribute.
	 * @see #getCurrentVectorColor()
	 * @generated
	 */
	void setCurrentVectorColor(Color3f value);

	/**
	 * Returns the value of the '<em><b>Default Vector Color</b></em>' attribute.
	 * The default value is <code>"0.0,1.0,0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The vector color when not no entry detected.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Default Vector Color</em>' attribute.
	 * @see #setDefaultVectorColor(Color3f)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DTool_DefaultVectorColor()
	 * @model default="0.0,1.0,0.0" unique="false" dataType="ca.gc.asc_csa.apogy.addons.Color3f"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Editable'"
	 * @generated
	 */
	Color3f getDefaultVectorColor();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getDefaultVectorColor <em>Default Vector Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Vector Color</em>' attribute.
	 * @see #getDefaultVectorColor()
	 * @generated
	 */
	void setDefaultVectorColor(Color3f value);

	/**
	 * Returns the value of the '<em><b>Vector Color Selection Visible</b></em>' attribute.
	 * The default value is <code>"1.0,0.0,0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The vector color when the selected point visible from the FieldOfView.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Vector Color Selection Visible</em>' attribute.
	 * @see #setVectorColorSelectionVisible(Color3f)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DTool_VectorColorSelectionVisible()
	 * @model default="1.0,0.0,0.0" unique="false" dataType="ca.gc.asc_csa.apogy.addons.Color3f"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Editable'"
	 * @generated
	 */
	Color3f getVectorColorSelectionVisible();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorColorSelectionVisible <em>Vector Color Selection Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vector Color Selection Visible</em>' attribute.
	 * @see #getVectorColorSelectionVisible()
	 * @generated
	 */
	void setVectorColorSelectionVisible(Color3f value);

	/**
	 * Returns the value of the '<em><b>Vector Color Selection Entered</b></em>' attribute.
	 * The default value is <code>"1.0,1.0,0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The vector color when the selected point in inside the volume (or area) of the FieldOfView.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Vector Color Selection Entered</em>' attribute.
	 * @see #setVectorColorSelectionEntered(Color3f)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DTool_VectorColorSelectionEntered()
	 * @model default="1.0,1.0,0.0" unique="false" dataType="ca.gc.asc_csa.apogy.addons.Color3f"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Editable'"
	 * @generated
	 */
	Color3f getVectorColorSelectionEntered();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorColorSelectionEntered <em>Vector Color Selection Entered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vector Color Selection Entered</em>' attribute.
	 * @see #getVectorColorSelectionEntered()
	 * @generated
	 */
	void setVectorColorSelectionEntered(Color3f value);

	/**
	 * Returns the value of the '<em><b>Maximum Vector Length</b></em>' attribute.
	 * The default value is <code>"10.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Maximum length of the vector. When the current length becomes smaller than this value, the actual distance to the selected point is used.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Maximum Vector Length</em>' attribute.
	 * @see #setMaximumVectorLength(double)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DTool_MaximumVectorLength()
	 * @model default="10.0" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Editable' notify='true' apogy_units='m'"
	 * @generated
	 */
	double getMaximumVectorLength();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getMaximumVectorLength <em>Maximum Vector Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maximum Vector Length</em>' attribute.
	 * @see #getMaximumVectorLength()
	 * @generated
	 */
	void setMaximumVectorLength(double value);

	/**
	 * Returns the value of the '<em><b>Vector Diameter</b></em>' attribute.
	 * The default value is <code>"0.025"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Diameter of the cylinder used to display the vector.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Vector Diameter</em>' attribute.
	 * @see #setVectorDiameter(double)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DTool_VectorDiameter()
	 * @model default="0.025" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Editable' notify='true' apogy_units='m'"
	 * @generated
	 */
	double getVectorDiameter();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getVectorDiameter <em>Vector Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vector Diameter</em>' attribute.
	 * @see #getVectorDiameter()
	 * @generated
	 */
	void setVectorDiameter(double value);

	/**
	 * Returns the value of the '<em><b>Current Vector Length</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Current length of the vector.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Current Vector Length</em>' attribute.
	 * @see #setCurrentVectorLength(double)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage#getFieldOfViewEntry3DTool_CurrentVectorLength()
	 * @model default="0" unique="false" transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='None' apogy_units='m'"
	 * @generated
	 */
	double getCurrentVectorLength();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool#getCurrentVectorLength <em>Current Vector Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Vector Length</em>' attribute.
	 * @see #getCurrentVectorLength()
	 * @generated
	 */
	void setCurrentVectorLength(double value);

} // FieldOfViewEntry3DTool

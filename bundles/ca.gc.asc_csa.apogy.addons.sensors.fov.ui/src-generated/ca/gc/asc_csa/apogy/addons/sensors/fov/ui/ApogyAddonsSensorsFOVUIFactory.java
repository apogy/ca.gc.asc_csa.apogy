package ca.gc.asc_csa.apogy.addons.sensors.fov.ui;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage
 * @generated
 */
public interface ApogyAddonsSensorsFOVUIFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyAddonsSensorsFOVUIFactory eINSTANCE = ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Field Of View Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field Of View Presentation</em>'.
	 * @generated
	 */
	FieldOfViewPresentation createFieldOfViewPresentation();

	/**
	 * Returns a new object of class '<em>Circular Sector Field Of View Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Circular Sector Field Of View Presentation</em>'.
	 * @generated
	 */
	CircularSectorFieldOfViewPresentation createCircularSectorFieldOfViewPresentation();

	/**
	 * Returns a new object of class '<em>Conical Field Of View Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conical Field Of View Presentation</em>'.
	 * @generated
	 */
	ConicalFieldOfViewPresentation createConicalFieldOfViewPresentation();

	/**
	 * Returns a new object of class '<em>Rectangular Frustrum Field Of View Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rectangular Frustrum Field Of View Presentation</em>'.
	 * @generated
	 */
	RectangularFrustrumFieldOfViewPresentation createRectangularFrustrumFieldOfViewPresentation();

	/**
	 * Returns a new object of class '<em>Field Of View Entry3 DTool</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field Of View Entry3 DTool</em>'.
	 * @generated
	 */
	FieldOfViewEntry3DTool createFieldOfViewEntry3DTool();

	/**
	 * Returns a new object of class '<em>Field Of View Entry3 DTool Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field Of View Entry3 DTool Node</em>'.
	 * @generated
	 */
	FieldOfViewEntry3DToolNode createFieldOfViewEntry3DToolNode();

	/**
	 * Returns a new object of class '<em>Circular Sector Field Of View Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return a new object of class '<em>Circular Sector Field Of View Wizard Pages Provider</em>'.
	 * @generated
	 */
	CircularSectorFieldOfViewWizardPagesProvider createCircularSectorFieldOfViewWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Conical Field Of View Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return a new object of class '<em>Conical Field Of View Wizard Pages Provider</em>'.
	 * @generated
	 */
	ConicalFieldOfViewWizardPagesProvider createConicalFieldOfViewWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Rectangular Frustrum Field Of View Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->

	 * @return a new object of class '<em>Rectangular Frustrum Field Of View Wizard Pages Provider</em>'.
	 * @generated
	 */
	RectangularFrustrumFieldOfViewWizardPagesProvider createRectangularFrustrumFieldOfViewWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Field Of View Entry3 DTool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field Of View Entry3 DTool Wizard Pages Provider</em>'.
	 * @generated
	 */
	FieldOfViewEntry3DToolWizardPagesProvider createFieldOfViewEntry3DToolWizardPagesProvider();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyAddonsSensorsFOVUIPackage getApogyAddonsSensorsFOVUIPackage();

} //ApogyAddonsSensorsFOVUIFactory

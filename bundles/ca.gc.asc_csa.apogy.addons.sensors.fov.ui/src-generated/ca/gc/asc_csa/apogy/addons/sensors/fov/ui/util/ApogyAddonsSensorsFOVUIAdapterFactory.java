package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.util;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.addons.AbstractTool;
import ca.gc.asc_csa.apogy.addons.AbstractTwoPoints3DTool;
import ca.gc.asc_csa.apogy.addons.Simple3DTool;
import ca.gc.asc_csa.apogy.addons.SimpleTool;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.*;
import ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.Node;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewPresentation;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewPresentation;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewPresentation;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import ca.gc.asc_csa.apogy.common.topology.ui.NodeWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage
 * @generated
 */
public class ApogyAddonsSensorsFOVUIAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ApogyAddonsSensorsFOVUIPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsSensorsFOVUIAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ApogyAddonsSensorsFOVUIPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApogyAddonsSensorsFOVUISwitch<Adapter> modelSwitch =
		new ApogyAddonsSensorsFOVUISwitch<Adapter>() {
			@Override
			public Adapter caseFieldOfViewPresentation(FieldOfViewPresentation object) {
				return createFieldOfViewPresentationAdapter();
			}
			@Override
			public Adapter caseCircularSectorFieldOfViewPresentation(CircularSectorFieldOfViewPresentation object) {
				return createCircularSectorFieldOfViewPresentationAdapter();
			}
			@Override
			public Adapter caseConicalFieldOfViewPresentation(ConicalFieldOfViewPresentation object) {
				return createConicalFieldOfViewPresentationAdapter();
			}
			@Override
			public Adapter caseRectangularFrustrumFieldOfViewPresentation(RectangularFrustrumFieldOfViewPresentation object) {
				return createRectangularFrustrumFieldOfViewPresentationAdapter();
			}
			@Override
			public Adapter caseFieldOfViewEntry3DTool(FieldOfViewEntry3DTool object) {
				return createFieldOfViewEntry3DToolAdapter();
			}
			@Override
			public Adapter caseFieldOfViewEntry3DToolNode(FieldOfViewEntry3DToolNode object) {
				return createFieldOfViewEntry3DToolNodeAdapter();
			}
			@Override
			public Adapter caseFieldOfViewWizardPagesProvider(FieldOfViewWizardPagesProvider object) {
				return createFieldOfViewWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseCircularSectorFieldOfViewWizardPagesProvider(CircularSectorFieldOfViewWizardPagesProvider object) {
				return createCircularSectorFieldOfViewWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseConicalFieldOfViewWizardPagesProvider(ConicalFieldOfViewWizardPagesProvider object) {
				return createConicalFieldOfViewWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseRectangularFrustrumFieldOfViewWizardPagesProvider(RectangularFrustrumFieldOfViewWizardPagesProvider object) {
				return createRectangularFrustrumFieldOfViewWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseFieldOfViewEntry3DToolWizardPagesProvider(FieldOfViewEntry3DToolWizardPagesProvider object) {
				return createFieldOfViewEntry3DToolWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseNodePresentation(NodePresentation object) {
				return createNodePresentationAdapter();
			}
			@Override
			public Adapter caseNamed(Named object) {
				return createNamedAdapter();
			}
			@Override
			public Adapter caseDescribed(Described object) {
				return createDescribedAdapter();
			}
			@Override
			public Adapter caseAbstractTool(AbstractTool object) {
				return createAbstractToolAdapter();
			}
			@Override
			public Adapter caseSimpleTool(SimpleTool object) {
				return createSimpleToolAdapter();
			}
			@Override
			public Adapter caseSimple3DTool(Simple3DTool object) {
				return createSimple3DToolAdapter();
			}
			@Override
			public Adapter caseAbstractTwoPoints3DTool(AbstractTwoPoints3DTool object) {
				return createAbstractTwoPoints3DToolAdapter();
			}
			@Override
			public Adapter caseNode(Node object) {
				return createNodeAdapter();
			}
			@Override
			public Adapter caseWizardPagesProvider(WizardPagesProvider object) {
				return createWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseNodeWizardPagesProvider(NodeWizardPagesProvider object) {
				return createNodeWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseNamedDescribedWizardPagesProvider(NamedDescribedWizardPagesProvider object) {
				return createNamedDescribedWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseAbstractToolWizardPagesProvider(AbstractToolWizardPagesProvider object) {
				return createAbstractToolWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseSimpleToolWizardPagesProvider(SimpleToolWizardPagesProvider object) {
				return createSimpleToolWizardPagesProviderAdapter();
			}
			@Override
			public Adapter caseSimple3DToolWizardPagesProvider(Simple3DToolWizardPagesProvider object) {
				return createSimple3DToolWizardPagesProviderAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation <em>Field Of View Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewPresentation
	 * @generated
	 */
	public Adapter createFieldOfViewPresentationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewPresentation <em>Circular Sector Field Of View Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewPresentation
	 * @generated
	 */
	public Adapter createCircularSectorFieldOfViewPresentationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewPresentation <em>Conical Field Of View Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewPresentation
	 * @generated
	 */
	public Adapter createConicalFieldOfViewPresentationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewPresentation <em>Rectangular Frustrum Field Of View Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewPresentation
	 * @generated
	 */
	public Adapter createRectangularFrustrumFieldOfViewPresentationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool <em>Field Of View Entry3 DTool</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool
	 * @generated
	 */
	public Adapter createFieldOfViewEntry3DToolAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode <em>Field Of View Entry3 DTool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode
	 * @generated
	 */
	public Adapter createFieldOfViewEntry3DToolNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewWizardPagesProvider <em>Field Of View Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->

	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewWizardPagesProvider
	 * @generated
	 */
	public Adapter createFieldOfViewWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewWizardPagesProvider <em>Circular Sector Field Of View Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->

	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewWizardPagesProvider
	 * @generated
	 */
	public Adapter createCircularSectorFieldOfViewWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewWizardPagesProvider <em>Conical Field Of View Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->

	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ConicalFieldOfViewWizardPagesProvider
	 * @generated
	 */
	public Adapter createConicalFieldOfViewWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewWizardPagesProvider <em>Rectangular Frustrum Field Of View Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->

	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewWizardPagesProvider
	 * @generated
	 */
	public Adapter createRectangularFrustrumFieldOfViewWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolWizardPagesProvider <em>Field Of View Entry3 DTool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolWizardPagesProvider
	 * @generated
	 */
	public Adapter createFieldOfViewEntry3DToolWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation <em>Node Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation
	 * @generated
	 */
	public Adapter createNodePresentationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.Named <em>Named</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.Named
	 * @generated
	 */
	public Adapter createNamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.Described <em>Described</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.Described
	 * @generated
	 */
	public Adapter createDescribedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.AbstractTool <em>Abstract Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.AbstractTool
	 * @generated
	 */
	public Adapter createAbstractToolAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.SimpleTool <em>Simple Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.SimpleTool
	 * @generated
	 */
	public Adapter createSimpleToolAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.Simple3DTool <em>Simple3 DTool</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.Simple3DTool
	 * @generated
	 */
	public Adapter createSimple3DToolAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.AbstractTwoPoints3DTool <em>Abstract Two Points3 DTool</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.AbstractTwoPoints3DTool
	 * @generated
	 */
	public Adapter createAbstractTwoPoints3DToolAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.topology.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.topology.Node
	 * @generated
	 */
	public Adapter createNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider <em>Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->

	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider
	 * @generated
	 */
	public Adapter createWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.topology.ui.NodeWizardPagesProvider <em>Node Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->

	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.topology.ui.NodeWizardPagesProvider
	 * @generated
	 */
	public Adapter createNodeWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider <em>Named Described Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider
	 * @generated
	 */
	public Adapter createNamedDescribedWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider <em>Abstract Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider
	 * @generated
	 */
	public Adapter createAbstractToolWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider <em>Simple Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider
	 * @generated
	 */
	public Adapter createSimpleToolWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider <em>Simple3 DTool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider
	 * @generated
	 */
	public Adapter createSimple3DToolWizardPagesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ApogyAddonsSensorsFOVUIAdapterFactory

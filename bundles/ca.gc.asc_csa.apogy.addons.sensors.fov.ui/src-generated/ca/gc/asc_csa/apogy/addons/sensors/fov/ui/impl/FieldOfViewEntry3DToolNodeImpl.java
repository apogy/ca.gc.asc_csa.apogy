/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *      Regent L'Archeveque
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl;

import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DToolNode;

import ca.gc.asc_csa.apogy.common.topology.impl.NodeImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Field Of View Entry3 DTool Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.FieldOfViewEntry3DToolNodeImpl#getFieldOfViewEntry3DTool <em>Field Of View Entry3 DTool</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FieldOfViewEntry3DToolNodeImpl extends NodeImpl implements FieldOfViewEntry3DToolNode {
	/**
	 * The cached value of the '{@link #getFieldOfViewEntry3DTool() <em>Field Of View Entry3 DTool</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFieldOfViewEntry3DTool()
	 * @generated
	 * @ordered
	 */
	protected FieldOfViewEntry3DTool fieldOfViewEntry3DTool;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FieldOfViewEntry3DToolNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsSensorsFOVUIPackage.Literals.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfViewEntry3DTool getFieldOfViewEntry3DTool() {
		if (fieldOfViewEntry3DTool != null && fieldOfViewEntry3DTool.eIsProxy()) {
			InternalEObject oldFieldOfViewEntry3DTool = (InternalEObject)fieldOfViewEntry3DTool;
			fieldOfViewEntry3DTool = (FieldOfViewEntry3DTool)eResolveProxy(oldFieldOfViewEntry3DTool);
			if (fieldOfViewEntry3DTool != oldFieldOfViewEntry3DTool) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL, oldFieldOfViewEntry3DTool, fieldOfViewEntry3DTool));
			}
		}
		return fieldOfViewEntry3DTool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldOfViewEntry3DTool basicGetFieldOfViewEntry3DTool() {
		return fieldOfViewEntry3DTool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFieldOfViewEntry3DTool(FieldOfViewEntry3DTool newFieldOfViewEntry3DTool, NotificationChain msgs) {
		FieldOfViewEntry3DTool oldFieldOfViewEntry3DTool = fieldOfViewEntry3DTool;
		fieldOfViewEntry3DTool = newFieldOfViewEntry3DTool;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL, oldFieldOfViewEntry3DTool, newFieldOfViewEntry3DTool);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFieldOfViewEntry3DTool(FieldOfViewEntry3DTool newFieldOfViewEntry3DTool) {
		if (newFieldOfViewEntry3DTool != fieldOfViewEntry3DTool) {
			NotificationChain msgs = null;
			if (fieldOfViewEntry3DTool != null)
				msgs = ((InternalEObject)fieldOfViewEntry3DTool).eInverseRemove(this, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE, FieldOfViewEntry3DTool.class, msgs);
			if (newFieldOfViewEntry3DTool != null)
				msgs = ((InternalEObject)newFieldOfViewEntry3DTool).eInverseAdd(this, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE, FieldOfViewEntry3DTool.class, msgs);
			msgs = basicSetFieldOfViewEntry3DTool(newFieldOfViewEntry3DTool, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL, newFieldOfViewEntry3DTool, newFieldOfViewEntry3DTool));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL:
				if (fieldOfViewEntry3DTool != null)
					msgs = ((InternalEObject)fieldOfViewEntry3DTool).eInverseRemove(this, ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL__FIELD_OF_VIEW_ENTRY3_DTOOL_NODE, FieldOfViewEntry3DTool.class, msgs);
				return basicSetFieldOfViewEntry3DTool((FieldOfViewEntry3DTool)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL:
				return basicSetFieldOfViewEntry3DTool(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL:
				if (resolve) return getFieldOfViewEntry3DTool();
				return basicGetFieldOfViewEntry3DTool();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL:
				setFieldOfViewEntry3DTool((FieldOfViewEntry3DTool)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL:
				setFieldOfViewEntry3DTool((FieldOfViewEntry3DTool)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyAddonsSensorsFOVUIPackage.FIELD_OF_VIEW_ENTRY3_DTOOL_NODE__FIELD_OF_VIEW_ENTRY3_DTOOL:
				return fieldOfViewEntry3DTool != null;
		}
		return super.eIsSet(featureID);
	}

} //FieldOfViewEntry3DToolNodeImpl

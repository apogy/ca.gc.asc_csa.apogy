/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.composites;

import java.text.DecimalFormat;

import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.DistanceRange;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;

public class DistanceRangeComposite extends Composite 
{
	public static String NO_VALUE_AVAILABLE_STRING = "N/A";
	
	private int LABEL_WIDTH = 200; 
	private int VALUE_WIDTH = 100; 
	private int BUTTON_WIDTH = 50; 		
	
	private DistanceRange distanceRange;
	
	private TypedElementSimpleUnitsComposite minimumDistanceComposite;
	private TypedElementSimpleUnitsComposite maximumDistanceComposite;

	
	public DistanceRangeComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(1, true));

		minimumDistanceComposite = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.00");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Minimum Distance :";
			}
		};
		minimumDistanceComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		minimumDistanceComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsSensorsFOVPackage.Literals.DISTANCE_RANGE__MINIMUM_DISTANCE), getDistanceRange());
	
		maximumDistanceComposite = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.00");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Maximum Distance :";
			}
		};
		maximumDistanceComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		maximumDistanceComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsSensorsFOVPackage.Literals.DISTANCE_RANGE__MAXIMUM_DISTANCE), getDistanceRange());
	
		layout();
	}

	public DistanceRange getDistanceRange() {
		return distanceRange;
	}

	public void setDistanceRange(DistanceRange distanceRange) 
	{
		this.distanceRange = distanceRange;
		
		if(minimumDistanceComposite != null && !minimumDistanceComposite.isDisposed())
		{
			minimumDistanceComposite.setInstance(getDistanceRange());
		}
		
		if(maximumDistanceComposite != null && !maximumDistanceComposite.isDisposed())
		{
			maximumDistanceComposite.setInstance(getDistanceRange());	
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.composites;

import java.text.DecimalFormat;

import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.fov.AngularSpan;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;

public class AngularSpanComposite extends Composite 
{
	public static String NO_VALUE_AVAILABLE_STRING = "N/A";
	
	private int LABEL_WIDTH = 200; 
	private int VALUE_WIDTH = 100; 
	private int BUTTON_WIDTH = 50; 		
	
	private AngularSpan distanceRange;
	
	private TypedElementSimpleUnitsComposite minimumAngleComposite;
	private TypedElementSimpleUnitsComposite maximumAngleComposite;

	public AngularSpanComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(1, true));

		minimumAngleComposite = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.00");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Minimum Angle :";
			}
		};
		minimumAngleComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		minimumAngleComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsSensorsFOVPackage.Literals.ANGULAR_SPAN__MINIMUM_ANGLE), getAngularSpan());
	
		maximumAngleComposite = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.00");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Maximum Angle :";
			}
		};
		maximumAngleComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		maximumAngleComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsSensorsFOVPackage.Literals.ANGULAR_SPAN__MAXIMUM_ANGLE), getAngularSpan());
	
		layout();
	}

	public AngularSpan getAngularSpan() {
		return distanceRange;
	}

	public void setAngularSpan(AngularSpan distanceRange) 
	{
		this.distanceRange = distanceRange;
		
		if(minimumAngleComposite != null && !minimumAngleComposite.isDisposed())
		{
			minimumAngleComposite.setInstance(getAngularSpan());
		}
		
		if(maximumAngleComposite != null && !maximumAngleComposite.isDisposed())
		{
			maximumAngleComposite.setInstance(getAngularSpan());	
		}
	}
}

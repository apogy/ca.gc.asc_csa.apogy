/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards;

import java.text.DecimalFormat;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.RectangularFrustrumFieldOfView;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;

public class RectangularFrustrumFieldOfViewAnglesWizardPage extends WizardPage 
{
	public static String NO_VALUE_AVAILABLE_STRING = "N/A";
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards.RectangularFrustrumFieldOfViewAnglesWizardPage";
	private int LABEL_WIDTH = 200; 
	private int VALUE_WIDTH = 100; 
	private int BUTTON_WIDTH = 50; 		

	
	private RectangularFrustrumFieldOfView rectangularFrustrumFieldOfView;
	
	private TypedElementSimpleUnitsComposite horizontalAngleComposite;
	private TypedElementSimpleUnitsComposite verticalAngleComposite;
	
	private Adapter adapter;
	
	public RectangularFrustrumFieldOfViewAnglesWizardPage(RectangularFrustrumFieldOfView rectangularFrustrumFieldOfView)
	{
		super(WIZARD_PAGE_ID);
		this.rectangularFrustrumFieldOfView = rectangularFrustrumFieldOfView;
				
		setTitle("Horizontal and Vertical field of view angles.");
		setDescription("Sets the horizontal and vertical field of view angles.");				
	}
	
	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));

		horizontalAngleComposite = new TypedElementSimpleUnitsComposite(top, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.00");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Horizontal Angle :";
			}
		};
		horizontalAngleComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		horizontalAngleComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsSensorsFOVPackage.Literals.RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW__HORIZONTAL_FIELD_OF_VIEW_ANGLE), getRectangularFrustrumFieldOfView());

		verticalAngleComposite = new TypedElementSimpleUnitsComposite(top, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.00");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Vertical Angle :";
			}
		};
		verticalAngleComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		verticalAngleComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsSensorsFOVPackage.Literals.RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW__VERTICAL_FIELD_OF_VIEW_ANGLE), getRectangularFrustrumFieldOfView());

		setControl(top);
		
		if(rectangularFrustrumFieldOfView != null)  rectangularFrustrumFieldOfView.eAdapters().add(getAdapter());
		top.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent arg0) {				
				if(rectangularFrustrumFieldOfView != null) rectangularFrustrumFieldOfView.eAdapters().remove(getAdapter());
			}
		});
				
		validate();
	}
		
	public RectangularFrustrumFieldOfView getRectangularFrustrumFieldOfView() {
		return rectangularFrustrumFieldOfView;
	}

	public void setRectangularFrustrumFieldOfView(RectangularFrustrumFieldOfView rectangularFrustrumFieldOfView) 
	{
		this.rectangularFrustrumFieldOfView = rectangularFrustrumFieldOfView;
		
		if(horizontalAngleComposite != null && !horizontalAngleComposite.isDisposed())
		{
			horizontalAngleComposite.setInstance(rectangularFrustrumFieldOfView);
		}
		
		if(verticalAngleComposite != null && !verticalAngleComposite.isDisposed())
		{
			verticalAngleComposite.setInstance(rectangularFrustrumFieldOfView);
		}
	}

	protected void validate()
	{
		setErrorMessage(null);
		
		if(rectangularFrustrumFieldOfView.getHorizontalFieldOfViewAngle() <= 0)
		{
			setErrorMessage("The specified horizontal field of view angle should greater than zero !");
		}
		
		if(rectangularFrustrumFieldOfView.getVerticalFieldOfViewAngle() <= 0)
		{
			setErrorMessage("The specified vertical field of view angle should greater than zero !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	protected Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof RectangularFrustrumFieldOfView)
					{
						validate();
					}
				}
			};
		}
		return adapter;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.fov.AngularSpan;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.composites.AngularSpanComposite;

public class AngularSpanWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards.AngularSpanWizardPage";

	private AngularSpan angularSpan;
	private AngularSpanComposite angularSpanComposite;
	
	private Adapter adapter;
	
	public AngularSpanWizardPage(AngularSpan angularSpan)
	{
		super(WIZARD_PAGE_ID);
		this.angularSpan = angularSpan;
				
		setTitle("Angular Span");
		setDescription("Sets the angular span minium and maximum angles.");				
	}
	
	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));

		angularSpanComposite = new AngularSpanComposite(top, SWT.NONE);
		angularSpanComposite.setAngularSpan(angularSpan);
		
		setControl(top);
		
		if(angularSpan != null)  angularSpan.eAdapters().add(getAdapter());
		
		top.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent arg0) {				
				if(angularSpan != null) angularSpan.eAdapters().remove(getAdapter());
			}
		});
		
		validate();
	}

	protected void validate()
	{
		setErrorMessage(null);
		
		if(angularSpan.getMaximumAngle() <= angularSpan.getMinimumAngle())
		{
			setErrorMessage("The specified maximum angle should equal or greater than the minimum angle !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	protected Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof AngularSpan)
					{
						validate();
					}
				}
			};
		}
		return adapter;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.fov.DistanceRange;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.composites.DistanceRangeComposite;

public class DistanceRangeWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards.DistanceRangeWizardPage";

	private DistanceRange distanceRange;
	private DistanceRangeComposite distanceRangeComposite;
	
	private Adapter adapter;
	
	public DistanceRangeWizardPage(DistanceRange distanceRange)
	{
		super(WIZARD_PAGE_ID);
		this.distanceRange = distanceRange;
				
		setTitle("Distance Range");
		setDescription("Sets the distance range minium and maximum distances.");				
	}
	
	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));

		distanceRangeComposite = new DistanceRangeComposite(top, SWT.NONE);
		distanceRangeComposite.setDistanceRange(distanceRange);
		
		setControl(top);
		
		if(distanceRange != null)  distanceRange.eAdapters().add(getAdapter());
		
		top.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent arg0) {				
				if(distanceRange != null) distanceRange.eAdapters().remove(getAdapter());
			}
		});
		
		validate();
	}

	protected void validate()
	{
		setErrorMessage(null);
		
		if(distanceRange.getMinimumDistance() < 0)
		{
			setErrorMessage("The specified minimum range should equal or greater than zero !");
		}
		
		if(distanceRange.getMaximumDistance() < 0)
		{
			setErrorMessage("The specified maximum range should equal or greater than zero !");
		}
		
		if(distanceRange.getMaximumDistance() <= distanceRange.getMinimumDistance())
		{
			setErrorMessage("The specified maximum range should equal or greater than the minimum range !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}

	protected Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof DistanceRange)
					{
						validate();
					}
				}
			};
		}
		return adapter;
	}
	
	
}

package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.FieldOfViewEntry3DTool;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.composites.FieldOfViewEntry3DToolComposite;

public class FieldOfViewEntry3DToolWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards.FieldOfViewEntry3DToolWizardPage";
		
	private FieldOfViewEntry3DTool fieldOfViewEntry3DTool;	
	private FieldOfViewEntry3DToolComposite fieldOfViewEntry3DToolComposite;
	
	private DataBindingContext m_bindingContext;
	
	public FieldOfViewEntry3DToolWizardPage(FieldOfViewEntry3DTool fieldOfViewEntry3DTool) 
	{
		super(WIZARD_PAGE_ID);
		this.fieldOfViewEntry3DTool = fieldOfViewEntry3DTool;
			
		setTitle("Field Of View Entry Tool.");
		setDescription("Select the Tool Field Of View.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		fieldOfViewEntry3DToolComposite = new FieldOfViewEntry3DToolComposite(top, SWT.NONE);
		fieldOfViewEntry3DToolComposite.setFieldOfViewEntry3DTool(fieldOfViewEntry3DTool);
	
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
	}	
	
	@Override
	public void dispose() 
	{	
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		super.dispose();
	}
	
	protected void validate()
	{
		setErrorMessage(null);		
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
			
		return bindingContext;
	}
	
	
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.math.ui.renderers;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.core.swt.SimpleControlSWTControlSWTRenderer;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.math.Activator;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFactory;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class Tuple3dControlRenderer extends SimpleControlSWTControlSWTRenderer {
	
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	@Inject
	public Tuple3dControlRenderer(VControl vElement, ViewModelContext viewContext, ReportService reportService,
			EMFFormsDatabinding emfFormsDatabinding, EMFFormsLabelProvider emfFormsLabelProvider,
			VTViewTemplateProvider vtViewTemplateProvider) {
		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);
	}
	
	@Override
	protected Binding[] createBindings(Control control) throws DatabindingFailedException {
		return new Binding[0];
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Control createSWTControl(Composite parent) throws DatabindingFailedException {
		/** Get the tuple*/
		Tuple3d tuple3d = (Tuple3d)getValue();
		/** Initialize the tuple if needed */
		if(tuple3d == null){
			tuple3d = ApogyCommonMathFactory.eINSTANCE.createTuple3d();
			
			getModelValue().setValue(tuple3d);
		}
		
		Composite composite = new Composite(parent, SWT.None);
		composite.setLayout(new FillLayout());
		
		/** Tuple composite */
		Section section = toolkit.createSection(composite, Section.TITLE_BAR | Section.EXPANDED);
		toolkit.adapt(section);
		section.setText("Tuple 3D");
		section.setExpanded(true);
	
		Tuple3dComposite tupleComposite = new Tuple3dComposite(section, SWT.None,
				ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(tuple3d));
		tupleComposite.setTuple3d(tuple3d);
		tupleComposite.setBackground(tupleComposite.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		section.setClient(tupleComposite);
		
		return composite;
	}

	@Override
	protected String getUnsetText() {
		return "Unset";
	}

	protected Object getValue() {
		try {
			Object obj = getModelValue().getValue();
			return obj;
		} catch (DatabindingFailedException e) {
			Logger.INSTANCE.log(Activator.PLUGIN_ID, " error setting the RGB value. ", EventSeverity.ERROR);
		}
		return null;
	}
	
	@Override
	protected void dispose() {
		toolkit.dispose();
		super.dispose();
	}
}

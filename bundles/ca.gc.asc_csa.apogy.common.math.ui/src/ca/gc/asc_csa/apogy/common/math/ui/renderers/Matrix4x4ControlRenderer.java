/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.math.ui.renderers;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.core.swt.SimpleControlSWTControlSWTRenderer;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.math.Activator;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFactory;
import ca.gc.asc_csa.apogy.common.math.Matrix4x4;
import ca.gc.asc_csa.apogy.common.math.ui.composites.Matrix4x4Composite;
import ca.gc.asc_csa.apogy.common.math.ui.composites.TransformMatrixComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class Matrix4x4ControlRenderer extends SimpleControlSWTControlSWTRenderer {
	
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	@Inject
	public Matrix4x4ControlRenderer(VControl vElement, ViewModelContext viewContext, ReportService reportService,
			EMFFormsDatabinding emfFormsDatabinding, EMFFormsLabelProvider emfFormsLabelProvider,
			VTViewTemplateProvider vtViewTemplateProvider) {
		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);
	}

	@Override
	protected Binding[] createBindings(Control control) throws DatabindingFailedException {
		return new Binding[0];
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Control createSWTControl(Composite parent) throws DatabindingFailedException {
		/** Get the matrix*/
		Matrix4x4 matrix = (Matrix4x4)getValue();
		/** Initialize the matrix if needed */
		if(matrix == null){
			matrix = ApogyCommonMathFactory.eINSTANCE.createMatrix4x4();
			
			getModelValue().setValue(matrix);
		}
		
		Composite composite = new Composite(parent, SWT.BORDER);
		composite.setLayout(GridLayoutFactory.fillDefaults().create());

		/** Transform matrix */
		TransformMatrixComposite transformComposite = new TransformMatrixComposite(composite, SWT.None,
				ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(matrix));
		transformComposite.setMatrix4x4(matrix);
		transformComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		transformComposite.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));

		/** Complete matrix */
		Section section = toolkit.createSection(composite, Section.TITLE_BAR | Section.TWISTIE);
		toolkit.adapt(section);
		section.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		section.setExpanded(false);
		section.setText("Complete");

		Matrix4x4Composite completeMatrixcomposite = new Matrix4x4Composite(section, SWT.None,
				ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(matrix));
		completeMatrixcomposite.setMatrix4x4(matrix);
		completeMatrixcomposite.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		section.setClient(completeMatrixcomposite);
		
		return composite;
	}

	@Override
	protected String getUnsetText() {
		return "Unset";
	}

	protected Object getValue() {
		try {
			Object obj = getModelValue().getValue();
			return obj;
		} catch (DatabindingFailedException e) {
			Logger.INSTANCE.log(Activator.PLUGIN_ID, " error setting the RGB value. ", EventSeverity.ERROR);
		}
		return null;
	}
	
	@Override
	protected void dispose() {
		toolkit.dispose();
		super.dispose();
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.Activator;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration;


public class AbstractWorldWindLayerListComposite extends Composite 
{
	private EarthViewConfiguration earthViewConfiguration;
	
	private boolean enableEditing = true;
	private TableViewer viewer;
	private Button btnNew;
	private Button btnDelete;	
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;

	
	public AbstractWorldWindLayerListComposite(Composite parent, int style)
	{
		this(parent, style, true);
	}
	
	public AbstractWorldWindLayerListComposite(Composite parent, int style, boolean enableEditing) 
	{
		super(parent, style);
		this.enableEditing = enableEditing;
	
		if(enableEditing)
		{
			setLayout(new GridLayout(2, false));
		}
		else
		{
			setLayout(new GridLayout(1, false));
		}
		
		viewer = new TableViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		Table table = viewer.getTable();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		table.setLayoutData(gd_tree);
		table.setLinesVisible(true);
		ColumnViewerToolTipSupport.enableFor(viewer);
		
		TableViewerColumn tableViewerColumnItem_Name = new TableViewerColumn(viewer, SWT.NONE);
		TableColumn trclmnItemName = tableViewerColumnItem_Name.getColumn();
		trclmnItemName.setWidth(300);		
		
		viewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory)
		{
			@Override
			public Object[] getElements(Object object) 
			{
				if(object instanceof EarthViewConfiguration)
				{								
					EarthViewConfiguration earthViewConfiguration = (EarthViewConfiguration) object;													
					return earthViewConfiguration.getLayers().toArray();
				}	
				return null;
			}
		});
		
		viewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		viewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newAbstractWorldWindLayerSelected((AbstractWorldWindLayer)((IStructuredSelection) event.getSelection()).getFirstElement());					
			}
		});
		
		if(enableEditing)
		{
			// Buttons.
			Composite composite = new Composite(this, SWT.NONE);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
			composite.setLayout(new GridLayout(1, false));	
			
			btnNew = new Button(composite, SWT.NONE);
			btnNew.setSize(74, 29);
			btnNew.setText("New");
			btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnNew.setEnabled(true);
			btnNew.addListener(SWT.Selection, new Listener() 
			{		
				@Override
				public void handleEvent(Event event) 
				{				
					if (event.type == SWT.Selection) 
					{					
						MapBasedEClassSettings settings = ApogyCommonEMFUIFactory.eINSTANCE.createMapBasedEClassSettings();						
						settings.getUserDataMap().put("name", ApogyCommonEMFFacade.INSTANCE.getDefaultName(getEarthViewConfiguration(), null, ApogyEarthEnvironmentUIPackage.Literals.EARTH_VIEW_CONFIGURATION__LAYERS));
						
						Wizard wizard = new ApogyEObjectWizard(ApogyEarthEnvironmentUIPackage.Literals.EARTH_VIEW_CONFIGURATION__LAYERS, getEarthViewConfiguration(), settings, ApogyEarthEnvironmentUIPackage.Literals.ABSTRACT_WORLD_WIND_LAYER); 
						WizardDialog dialog = new WizardDialog(getShell(), wizard);
						dialog.open();
						
						// Forces the viewer to refresh its input.
						if(!viewer.isBusy())
						{					
							viewer.refresh();
							
							if(getEarthViewConfiguration().getLayers().isEmpty())
							{
								viewer.setSelection(new StructuredSelection(), true);
							}
							else
							{
								int index = getEarthViewConfiguration().getLayers().size() - 1;								
								viewer.setSelection(new StructuredSelection(getEarthViewConfiguration().getLayers().get(index)), true);
							}
						}					
					}
				}
			});
					
			btnDelete = new Button(composite, SWT.NONE);
			btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnDelete.setSize(74, 29);
			btnDelete.setText("Delete");
			btnDelete.setEnabled(false);
			btnDelete.addSelectionListener(new SelectionAdapter() 
			{
				@Override
				public void widgetSelected(SelectionEvent event) 
				{
					String meshLayersToDeleteMessage = "";
	
					Iterator<AbstractWorldWindLayer> layers = getSelectedAbstractWorldWindLayers().iterator();
					while (layers.hasNext()) 
					{
						AbstractWorldWindLayer meshLayer = layers.next();
						meshLayersToDeleteMessage = meshLayersToDeleteMessage + meshLayer.getName();
	
						if (layers.hasNext()) 
						{
							meshLayersToDeleteMessage = meshLayersToDeleteMessage + ", ";
						}
					}
	
					MessageDialog dialog = new MessageDialog(null, "Delete the selected Earth View Layers", null,
							"Are you sure to delete these Earth View Layers: " + meshLayersToDeleteMessage, MessageDialog.QUESTION,
							new String[] { "Yes", "No" }, 1);
					int result = dialog.open();
					if (result == 0) 
					{
						for (AbstractWorldWindLayer layer :  getSelectedAbstractWorldWindLayers()) 
						{
							try 
							{	
								// Dispose of the layer.
								layer.dispose();
								
								// The removes the layer from the list
								ApogyCommonTransactionFacade.INSTANCE.basicDelete(getEarthViewConfiguration(), ApogyEarthEnvironmentUIPackage.Literals.EARTH_VIEW_CONFIGURATION__LAYERS, layer);																
							} 
							catch (Exception e)	
							{
								Logger.INSTANCE.log(Activator.ID,
										"Unable to delete the Earth View Layers <"+ layer.getName() + ">",
										EventSeverity.ERROR, e);
							}
						}
					}
					
					// Forces the viewer to refresh its input.
					if(!viewer.isBusy())
					{					
						viewer.refresh();
						
						if(getEarthViewConfiguration().getLayers().isEmpty())
						{
							viewer.setSelection(new StructuredSelection(), true);
						}
						else
						{
							viewer.setSelection(new StructuredSelection(getEarthViewConfiguration().getLayers().get(0)), true);
						}
					}					
				}
			});
		}
		
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public EarthViewConfiguration getEarthViewConfiguration() {
		return earthViewConfiguration;
	}

	public void setEarthViewConfiguration(EarthViewConfiguration newEarthViewConfiguration) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.earthViewConfiguration = newEarthViewConfiguration;
		
		if(newEarthViewConfiguration != null)
		{
			m_bindingContext = customInitDataBindings();
			viewer.setInput(newEarthViewConfiguration);
			
			AbstractWorldWindLayer layer = getFirstAbstractWorldWindLayer(newEarthViewConfiguration);
			if(layer != null)
			{
				viewer.setSelection(new StructuredSelection(layer), true);
			}
		}
	}	
	
	@SuppressWarnings("unchecked")
	public List<AbstractWorldWindLayer> getSelectedAbstractWorldWindLayers()
	{
		return ((IStructuredSelection) viewer.getSelection()).toList();
	}
	
	protected void newAbstractWorldWindLayerSelected(AbstractWorldWindLayer abstractWorldWindLayer)
	{		
	}
	
	
	protected AbstractWorldWindLayer getFirstAbstractWorldWindLayer(EarthViewConfiguration earthViewConfiguration)
	{		
		if(!earthViewConfiguration.getLayers().isEmpty())
		{
			return earthViewConfiguration.getLayers().get(0);
		}
		else
		{
			return null;
		}
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		if(enableEditing)
		{
			IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(viewer);
			
			/* Delete Button Enabled Binding. */
			IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
			bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
					new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
							.setConverter(new Converter(Object.class, Boolean.class) {
								@Override
								public Object convert(Object fromObject) {
									return fromObject != null;
								}
							}));
		}
		return bindingContext;
	}	
}

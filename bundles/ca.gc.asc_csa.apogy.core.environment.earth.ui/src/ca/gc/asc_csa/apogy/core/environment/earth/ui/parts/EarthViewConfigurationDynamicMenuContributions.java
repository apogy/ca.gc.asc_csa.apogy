/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.parts;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.ui.di.AboutToHide;
import org.eclipse.e4.ui.di.AboutToShow;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationList;

public class EarthViewConfigurationDynamicMenuContributions 
{	
	@Inject
	private EModelService modelService;
	
	@AboutToShow
	public void aboutToShow(List<MMenuElement> items)
	{
		// First, get the list of Earth View Configuration from the active session.
		EarthViewConfigurationList cameraViewConfigurationList = EarthUIFacade.INSTANCE.getActiveEarthViewConfigurationList();
		
		if(cameraViewConfigurationList != null)
		{
			populate(items);	
		}
	}
	
	@AboutToHide
	public void aboutToHide(List<MMenuElement> items)
	{		
	}
	
	protected void populate(List<MMenuElement> items)
	{
		// Remove existing menu items
		for (MMenuElement child : items)
		{
			child.setToBeRendered(false);
			child.setVisible(false);			
		}
		items.clear();
				
		// Gets the list of Earth View Configuration available.
		EarthViewConfigurationList cameraViewConfigurationList = EarthUIFacade.INSTANCE.getActiveEarthViewConfigurationList();
		
				
		for(EarthViewConfiguration cameraViewConfiguration : cameraViewConfigurationList.getEarthViewConfigurations())
		{
			MDirectMenuItem dynamicItem = modelService.createModelElement(MDirectMenuItem.class);
	        dynamicItem.setLabel(cameraViewConfiguration.getName());
	        dynamicItem.setContributionURI("bundleclass://ca.gc.asc_csa.apogy.core.environment.earth.ui/ca.gc.asc_csa.apogy.core.environment.earth.ui.handlers.SetActiveEarthViewConfigurationHandler");
	        dynamicItem.setContributorURI("platform:/plugin/ca.gc.asc_csa.apogy.core.environment.earth.ui");	        
	        dynamicItem.setToBeRendered(true);
	        
	        dynamicItem.getTransientData().put("earthViewConfiguration", cameraViewConfiguration);
	
	        items.add(dynamicItem);
		}		
	}		
	
	protected String getIconURIForAbstractViewPoint(AbstractViewPoint abstractViewPoint)
	{
		String uri = null;
				
		
		return uri;
	}
}

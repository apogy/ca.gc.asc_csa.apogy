/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.composites;

import org.eclipse.emf.ecp.view.spi.model.VView;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer;

/**
 * Composite that displays the details of an AbstractWorldWindLayer.
 * @author pallard
 *
 */
public class AbstractWorldWindLayerDetailsComposite extends Composite 
{
	private AbstractWorldWindLayer abstractWorldWindLayer;
	private Composite compositeDetails;
	
		
	public AbstractWorldWindLayerDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));
	}

	public AbstractWorldWindLayer getAbstractWorldWindLayer() {
		return abstractWorldWindLayer;
	}

	public void setAbstractWorldWindLayer(AbstractWorldWindLayer abstractWorldWindLayer) 
	{
		this.abstractWorldWindLayer = abstractWorldWindLayer;
				
		// Update Details EMFForm.
		if(abstractWorldWindLayer != null)
		{
			VView viewModel = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createDefaultViewModel(abstractWorldWindLayer);								
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, abstractWorldWindLayer, viewModel, "No Data"); 			
		}
	}	
}

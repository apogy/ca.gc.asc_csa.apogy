package ca.gc.asc_csa.apogy.core.environment.earth.ui.parts;

import java.util.HashMap;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIRCPConstants;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.composites.EarthViewConfigurationComposite;

public class EarthViewConfigurationPart extends AbstractEObjectSelectionPart 
{
	private EarthViewConfigurationComposite earthViewConfigurationComposite;
	
	@Override
	protected void setCompositeContents(EObject eObject) 
	{
		if(eObject instanceof EarthViewConfiguration)
		{			
			earthViewConfigurationComposite.setEarthViewConfiguration((EarthViewConfiguration) eObject);
		}
		else
		{
			earthViewConfigurationComposite.setEarthViewConfiguration(null);
		}
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{		
		earthViewConfigurationComposite = new EarthViewConfigurationComposite(parent, SWT.BORDER);	
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() 
	{
		HashMap<String, ISelectionListener> map = new HashMap<>();

		map.put(ApogyEarthEnvironmentUIRCPConstants.PART__EARTH_VIEW__ID, new ISelectionListener() 
		{
			@Override
			public void selectionChanged(MPart part, Object selection) 
			{
				if (selection instanceof EarthViewConfiguration) 
				{
					EarthViewConfiguration earthViewConfiguration = (EarthViewConfiguration) selection;										
					setEObject(earthViewConfiguration);
				}
			}
		});

		return map;
	}
}

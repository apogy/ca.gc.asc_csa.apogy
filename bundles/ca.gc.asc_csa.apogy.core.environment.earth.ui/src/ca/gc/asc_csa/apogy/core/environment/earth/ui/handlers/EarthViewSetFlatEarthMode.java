/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

import ca.gc.asc_csa.apogy.core.environment.earth.ui.composites.EarthViewComposite.EarthViewMode;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.parts.EarthViewPart;

public class EarthViewSetFlatEarthMode {

	@CanExecute
	public boolean canExecute(MPart part)
	{
		if (part.getObject() instanceof EarthViewPart)
		{			
			return true;			
		}	
		else
		{
			return false;
		}
	}
	
	@Execute
	public void execute(MPart part)
	{
		if (part.getObject() instanceof EarthViewPart)
		{			
			EarthViewPart earthViewPart = (EarthViewPart) part.getObject();
			earthViewPart.setEarthViewMode(EarthViewMode.FLAT);
		}				
	}
}
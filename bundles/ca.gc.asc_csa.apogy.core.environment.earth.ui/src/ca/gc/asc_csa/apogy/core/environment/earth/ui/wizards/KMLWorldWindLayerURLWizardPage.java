/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

/**
 * Wizard age used ton configure Geographic Coordinates.
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.URLSelectionComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayer;

public class KMLWorldWindLayerURLWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.earth.ui.wizards.KMLWorldWindLayerURLWizardPage";
	
	private KMLWorldWindLayer kmlWorldWindLayer;
	private String urlString;
	
	private URLSelectionComposite urlSelectionComposite;
	
	public KMLWorldWindLayerURLWizardPage(KMLWorldWindLayer surfacePolygonWorldWindLayer) 
	{
		super(WIZARD_PAGE_ID);
		this.kmlWorldWindLayer = surfacePolygonWorldWindLayer;
			
		setTitle("URL to the KML/KMZ file.");
		setDescription("Set the URL.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
						
		// URL Selection
		urlSelectionComposite = new URLSelectionComposite(top, SWT.None, new String[]{"*.kmz", "*.kml"}, true, true, true)
		{
			@Override
			protected void urlStringSelected(String newURLString) 
			{		
				KMLWorldWindLayerURLWizardPage.this.urlString = newURLString;
				
				validate();
				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(kmlWorldWindLayer, ApogyEarthEnvironmentUIPackage.Literals.KML_WORLD_WIND_LAYER__URL, newURLString);
			}
		};
		urlSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		if(kmlWorldWindLayer != null && kmlWorldWindLayer.getUrl() != null)
		{
			urlSelectionComposite.setUrlString(kmlWorldWindLayer.getUrl());
		}
						
		setControl(top);
		urlSelectionComposite.setFocus();					
	}
	
	protected void validate()
	{
		setErrorMessage(null);	
				
		// Checks the URL.
		boolean urlValid = (urlString!= null) && (urlString.length() > 0);		
		if(!urlValid)
		{
			setErrorMessage("Invalid URL specified !");
		}
		setPageComplete(getErrorMessage() == null);
	}
}

package ca.gc.asc_csa.apogy.core.environment.earth.ui.composites;

import java.text.DecimalFormat;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;

public class AirspaceWorldWindLayerComposite extends Composite 
{
	private static String NO_VALUE_AVAILABLE_STRING = "N/A";
	private int LABEL_WIDTH = 200; 
	private int VALUE_WIDTH = 100; 
	private int BUTTON_WIDTH = 50; 		
	
	private AirspaceWorldWindLayer airspaceWorldWindLayer;
	
	private TypedElementSimpleUnitsComposite txtLowerAltitude;
	private TypedElementSimpleUnitsComposite txtUpperAltitude;
	
	private DataBindingContext m_bindingContext;
	
	public AirspaceWorldWindLayerComposite(Composite parent, int style) 
	{
		super(parent, style);			
		setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		setLayout(new GridLayout(1, true));
		
		// Lower Altitude		
		txtLowerAltitude = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.0");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Lower Altitude Limit :";
			}
		};
		GridData gd_txtLowerAltitude = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtLowerAltitude.widthHint = 100;
		gd_txtLowerAltitude.minimumWidth = 100;
		txtLowerAltitude.setLayoutData(gd_txtLowerAltitude);
		txtLowerAltitude.setTypedElement(FeaturePath.fromList(ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE), getAirspaceWorldWindLayer());
		
		
		// Upper Altitude		
		txtUpperAltitude = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.0");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Upper Altitude Limit :";
			}
		};
		GridData gd_txtUpperAltitude = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtUpperAltitude.widthHint = 100;
		gd_txtUpperAltitude.minimumWidth = 100;
		txtUpperAltitude.setLayoutData(gd_txtUpperAltitude);
		txtUpperAltitude.setTypedElement(FeaturePath.fromList(ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE), getAirspaceWorldWindLayer());
		
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public AirspaceWorldWindLayer getAirspaceWorldWindLayer() {
		return airspaceWorldWindLayer;
	}

	public void setAirspaceWorldWindLayer(AirspaceWorldWindLayer airspaceWorldWindLayer) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.airspaceWorldWindLayer = airspaceWorldWindLayer;
		
		if(airspaceWorldWindLayer != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		txtLowerAltitude.setInstance(getAirspaceWorldWindLayer());		
		txtUpperAltitude.setInstance(getAirspaceWorldWindLayer());
										
		return bindingContext;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

/**
 * Wizard age used ton configure Geographic Coordinates.
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.composites.AirspaceWorldWindLayerComposite;

public class AirspaceWorldWindLayerWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.earth.ui.wizards.AirspaceWorldWindLayerWizardPage";
	
	private AirspaceWorldWindLayerComposite airspaceWorldWindLayerComposite;
	private AirspaceWorldWindLayer airspaceWorldWindLayer;
	
	public AirspaceWorldWindLayerWizardPage(AirspaceWorldWindLayer airspaceWorldWindLayer) 
	{
		super(WIZARD_PAGE_ID);
		this.airspaceWorldWindLayer = airspaceWorldWindLayer;
			
		setTitle("Set the lower and upper limits of the Airspace.");
		setDescription("Set the lower and upper limits.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
						
		airspaceWorldWindLayerComposite = new AirspaceWorldWindLayerComposite(top, SWT.BORDER);
		airspaceWorldWindLayerComposite.setAirspaceWorldWindLayer(airspaceWorldWindLayer);
				
		setControl(top);			
	}
	
	protected void validate()
	{
		setErrorMessage(null);			
		setPageComplete(getErrorMessage() == null);
	}
}

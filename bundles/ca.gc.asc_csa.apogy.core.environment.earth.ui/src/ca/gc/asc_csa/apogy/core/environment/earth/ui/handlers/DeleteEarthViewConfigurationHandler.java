/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationList;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.composites.EarthViewComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.parts.EarthViewPart;

public class DeleteEarthViewConfigurationHandler {

	@CanExecute
	public boolean canExecute(MPart part)
	{
		if (part.getObject() instanceof EarthViewPart){
			EarthViewPart viewPart = (EarthViewPart) part.getObject();
			
			if(viewPart.getActualComposite() instanceof EarthViewComposite)
			{
				EarthViewComposite composite = (EarthViewComposite)viewPart.getActualComposite();
				return composite.getEarthViewConfiguration() != null;
			}
		}
		return false;
	}
	
	@Execute
	public void execute(MPart part, final MToolItem item)
	{		
		if (part.getObject() instanceof EarthViewPart)
		{			
			EarthViewPart viewPart = (EarthViewPart) part.getObject();
			if(viewPart.getActualComposite() instanceof EarthViewComposite)
			{
				EarthViewComposite composite = (EarthViewComposite)viewPart.getActualComposite();
				EarthViewConfiguration earthViewConfiguration= composite.getEarthViewConfiguration();
				
				if(earthViewConfiguration != null)
				{
					EarthViewConfigurationList cameraViewConfigurationList = EarthUIFacade.INSTANCE.getActiveEarthViewConfigurationList();
					ApogyCommonTransactionFacade.INSTANCE.basicRemove(cameraViewConfigurationList, ApogyEarthEnvironmentUIPackage.Literals.EARTH_VIEW_CONFIGURATION_LIST__EARTH_VIEW_CONFIGURATIONS, earthViewConfiguration);

					composite.setEarthViewConfiguration(null);
				}				
			}					
		}		
	}
}
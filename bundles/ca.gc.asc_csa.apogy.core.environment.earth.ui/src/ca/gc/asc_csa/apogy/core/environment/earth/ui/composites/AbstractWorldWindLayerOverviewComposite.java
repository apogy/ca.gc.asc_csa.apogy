/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;

/**
 * Composite that displays an overview of an AbstractWorldWindLayer.
 * @author pallard
 *
 */
public class AbstractWorldWindLayerOverviewComposite extends Composite 
{
	private AbstractWorldWindLayer abstractWorldWindLayer;
	
	private DataBindingContext m_bindingContext;
	private Text txtNamevalue;
	private Text txtDescriptionvalue;
	private Button buttonVisible;
	
	
	public AbstractWorldWindLayerOverviewComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new GridLayout(2, false));		
		
		Label lblName = new Label(this, SWT.NONE);
		lblName.setAlignment(SWT.RIGHT);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name:");
		
		txtNamevalue = new Text(this, SWT.BORDER);
		GridData gd_txtNamevalue = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_txtNamevalue.minimumWidth = 300;
		gd_txtNamevalue.widthHint = 300;
		txtNamevalue.setLayoutData(gd_txtNamevalue);
		
		Label lblDescription = new Label(this, SWT.NONE);
		lblDescription.setAlignment(SWT.RIGHT);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDescription.setText("Description:");
		
		txtDescriptionvalue = new Text(this, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtDescriptionvalue = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_txtDescriptionvalue.minimumWidth = 300;
		gd_txtDescriptionvalue.widthHint = 300;
		gd_txtDescriptionvalue.minimumHeight = 50;
		gd_txtDescriptionvalue.heightHint = 50;
		txtDescriptionvalue.setLayoutData(gd_txtDescriptionvalue);		
		
		Label lblVisible = new Label(this, SWT.NONE);
		lblVisible.setAlignment(SWT.RIGHT);
		lblVisible.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblVisible.setText("Visible:");
		
		buttonVisible = new Button(this, SWT.CHECK);
		GridData gd_buttonVisible = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);		
		buttonVisible.setLayoutData(gd_buttonVisible);
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();				
			}
		});
	}

	public AbstractWorldWindLayer getAbstractWorldWindLayer() 
	{
		return abstractWorldWindLayer;
	}

	public void setAbstractWorldWindLayer(AbstractWorldWindLayer newAbstractWorldWindLayer) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.abstractWorldWindLayer = newAbstractWorldWindLayer;
		
		if(newAbstractWorldWindLayer != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
		
	}	
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		/* Name Value. */
		IObservableValue<String> observeName = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.NAMED__NAME)).observe(getAbstractWorldWindLayer());
		IObservableValue<String> observeNameValueText = WidgetProperties.text(SWT.Modify).observe(txtNamevalue);

		bindingContext.bindValue(observeNameValueText,
								observeName, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		/* Description Value. */
		IObservableValue<String> observeDescription = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.DESCRIBED__DESCRIPTION)).observe(getAbstractWorldWindLayer());
		IObservableValue<String> observeDescriptionText = WidgetProperties.text(SWT.Modify).observe(txtDescriptionvalue);

		bindingContext.bindValue(observeDescriptionText,
								 observeDescription, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		/* Visible value*/
		IObservableValue<Boolean> observeVisible = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  								   FeaturePath.fromList(ApogyEarthEnvironmentUIPackage.Literals.ABSTRACT_WORLD_WIND_LAYER__VISIBLE)).observe(getAbstractWorldWindLayer());

		IObservableValue<Boolean> observeVisibleButton = WidgetProperties.selection().observe(buttonVisible);
		
		bindingContext.bindValue(observeVisibleButton, observeVisible, new UpdateValueStrategy(), new UpdateValueStrategy());
		
		return bindingContext;
	}
}

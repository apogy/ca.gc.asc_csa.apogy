/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBar;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBarElement;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIRCPConstants;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.composites.EarthViewComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.parts.EarthViewPart;

public class UpdateEarthViewToolbarHandler {

	@Inject
	EModelService modelService;

	@CanExecute
	public boolean canExecute(MApplication app) 
	{
		MPart part = (MPart) modelService.find(ApogyEarthEnvironmentUIRCPConstants.PART__EARTH_VIEW__ID, app);

		if (part != null && part.getObject() instanceof EarthViewPart) 
		{
			return modelService.find(ApogyEarthEnvironmentUIRCPConstants.TOOL_BAR__EARTH_VIEW__ID, part) == null;
		}
		return false;
	}

	@Execute
	public void execute(MTrimmedWindow window) {
		MPart part = (MPart) modelService.find(ApogyEarthEnvironmentUIRCPConstants.PART__EARTH_VIEW__ID, window);

		if (part.getObject() instanceof EarthViewPart) {
			EarthViewPart viewPart = (EarthViewPart) part.getObject();
			if (viewPart.getActualComposite() instanceof EarthViewComposite) {
				EarthViewConfiguration config = ((EarthViewComposite) viewPart.getActualComposite()).getEarthViewConfiguration();

				if (config != null) {
					MToolBar toolBar = part.getToolbar();

					for (MToolBarElement element : toolBar.getChildren()) 
					{												
						if (element.getElementId().equals(ApogyEarthEnvironmentUIRCPConstants.HANDLED_TOOL_ITEM__DELETE_EARTH_VIEW_CONFIGURATION__ID)) 
						{
							((MToolItem) element).setEnabled(config != null);
						}						
					}
				}
			}
		}
	}
}
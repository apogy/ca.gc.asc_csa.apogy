/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.parts;

import java.util.Collections;

import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIRCPConstants;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationReference;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.composites.EarthViewComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.composites.EarthViewComposite.EarthViewMode;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ui.Activator;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;

@SuppressWarnings("restriction")
public class EarthViewPart extends AbstractSessionBasedPart
{
	public static final String EarthViewConfigurationID = "EarthViewConfiguration";
	protected String partName = "Earth View";

	private EarthViewConfiguration earthViewConfiguration;
	
	protected EarthViewComposite earthViewComposite;
	private EarthViewMode earthViewMode = EarthViewMode.ROUND;
	
	@Inject
	ECommandService commandService;

	@Inject
	EHandlerService handlerService;

	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{				
		earthViewComposite.setEarthViewConfigurationList(EarthUIFacade.INSTANCE.getActiveEarthViewConfigurationList());
		
		if(invocatorSession != null)
		{
			EarthViewConfigurationReference ref = (EarthViewConfigurationReference) ApogyCoreInvocatorUIFacade.INSTANCE.readFromPersistedState(mPart, EarthViewConfigurationID);
			if(ref != null)
			{
				earthViewConfiguration = ref.getEarthViewConfiguration();	
			}
			else
			{
				Logger.INSTANCE.log(Activator.ID, this, "No Earth View Configuration could be read back, setting it to null. ", EventSeverity.WARNING);
			}
											
			earthViewComposite.setEarthViewConfiguration(earthViewConfiguration);			
		}		
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{
		parent.setLayout(new GridLayout(1,false));
		earthViewComposite = new EarthViewComposite(parent, SWT.BORDER)
		{
			@Override
			protected void newEarthViewConfigurationSelected(EarthViewConfiguration newEarthViewConfiguration) 
			{
				earthViewConfiguration = newEarthViewConfiguration;
				selectionService.setSelection(newEarthViewConfiguration);
				
				if(newEarthViewConfiguration != null)
				{
					mPart.setLabel(partName + " - " + newEarthViewConfiguration.getName());
				}
				
				ParameterizedCommand command = commandService.createCommand(ApogyEarthEnvironmentUIRCPConstants.COMMAND__UPDATE_EARTH_VIEW_TOOLBAR__ID, Collections.emptyMap());
				handlerService.executeHandler(command);	
			}
		};
		GridData gd_earthViewComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		earthViewComposite.setLayoutData(gd_earthViewComposite);	
		earthViewComposite.setEarthViewMode(earthViewMode);
	}	
	
	@Override
	public void userPersistState(MPart mPart) 
	{
		try
		{			
			if(earthViewConfiguration != null)
			{		
				EarthViewConfigurationReference ref = ApogyEarthEnvironmentUIFactory.eINSTANCE.createEarthViewConfigurationReference();
				ref.setEarthViewConfiguration(earthViewConfiguration);
				ApogyCoreInvocatorUIFacade.INSTANCE.saveToPersistedState(mPart, EarthViewConfigurationID, ref);
			}		
			
			// Reset part name
			mPart.setLabel(partName);
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
	}
	
	public void setEarthViewMode(EarthViewMode mode) 
	{
		if(earthViewComposite != null && !earthViewComposite.isDisposed())
		{
			earthViewComposite.setEarthViewMode(mode);
		}
		
		this.earthViewMode = mode;
	}

	public EarthViewMode getEarthViewMode()
	{
		return earthViewMode;
	}
}

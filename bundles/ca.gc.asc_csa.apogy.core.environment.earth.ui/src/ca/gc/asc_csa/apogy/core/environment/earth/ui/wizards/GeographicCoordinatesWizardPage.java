/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

/**
 * Wizard age used ton configure Geographic Coordinates.
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.composites.GeographicCoordinatesComposite;

public class GeographicCoordinatesWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.earth.ui.wizards.GeographicCoordinatesWizardPage";
	
	private GeographicCoordinates geographicCoordinates;
	private GeographicCoordinatesComposite geographicCoordinatesComposite;
	
	public GeographicCoordinatesWizardPage(GeographicCoordinates geographicCoordinates) 
	{
		super(WIZARD_PAGE_ID);
		this.geographicCoordinates = geographicCoordinates;
			
		setTitle("Geographic Coordinates.");
		setDescription("Set the Geographic Coordinates longitude, latitude and elevation.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(1,false));
		setControl(container);
		
		geographicCoordinatesComposite = new GeographicCoordinatesComposite(container, SWT.NONE);
		geographicCoordinatesComposite.setGeographicCoordinates(geographicCoordinates);
	}
	
	protected void validate()
	{
		setErrorMessage(null);								
		setPageComplete(getErrorMessage() == null);
	}
}

package ca.gc.asc_csa.apogy.core.environment.earth.ui.composites;

import java.text.DecimalFormat;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;
import ca.gc.asc_csa.apogy.common.ui.composites.Color3fComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer;

public class SurfacePolygonWorldWindLayerComposite extends Composite 
{
	private static String NO_VALUE_AVAILABLE_STRING = "N/A";
	private static int LABEL_WIDTH = 200; 
	private static int VALUE_WIDTH = 100; 
	private static int BUTTON_WIDTH = 50; 		
	
	private SurfacePolygonWorldWindLayer surfacePolygonWorldWindLayer;
	
	private Color3fComposite layerColor;
	private TypedElementSimpleUnitsComposite opacity;
	
	private DataBindingContext m_bindingContext;
	
	public SurfacePolygonWorldWindLayerComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		setLayout(new GridLayout(2, true));
		
		// Layer Color		
		layerColor = new Color3fComposite(this, SWT.NONE, "Layer Color", true, null, ApogyEarthEnvironmentUIPackage.Literals.SURFACE_POLYGON_WORLD_WIND_LAYER__COLOR);
		GridData gd_vectorColor = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_vectorColor.widthHint = 100;
		gd_vectorColor.minimumWidth = 100;
		layerColor.setLayoutData(gd_vectorColor);
				
		opacity = new TypedElementSimpleUnitsComposite(this, SWT.NONE, true, false, false, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.0");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Opacity :";
			}
		};
		GridData gd_opacity = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_opacity.widthHint = 100;
		gd_opacity.minimumWidth = 100;
		opacity.setLayoutData(gd_opacity);
		opacity.setTypedElement(FeaturePath.fromList(ApogyEarthEnvironmentUIPackage.Literals.SURFACE_POLYGON_WORLD_WIND_LAYER__OPACITY), getSurfacePolygonWorldWindLayer());
		
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public SurfacePolygonWorldWindLayer getSurfacePolygonWorldWindLayer() 
	{
		return surfacePolygonWorldWindLayer;
	}

	public void setSurfacePolygonWorldWindLayer(SurfacePolygonWorldWindLayer surfacePolygonWorldWindLayer) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.surfacePolygonWorldWindLayer = surfacePolygonWorldWindLayer;
		
		if(surfacePolygonWorldWindLayer != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}				
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
			
		// Opacity
		opacity.setInstance(getSurfacePolygonWorldWindLayer());
		
		// Colors
		layerColor.setOwner(getSurfacePolygonWorldWindLayer());
		
		return bindingContext;
	}
}

/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Canadian Space Agency (CSA) - Initial API and implementation
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
 *           
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.jface.viewers.ISelection;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesPathWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.utils.MultiEObjectsAdapter;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.utils.WorldWindUtils;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Polyline;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Geographic Coordinates Path World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.GeographicCoordinatesPathWorldWindLayerImpl#getGeographicCoordinatesList <em>Geographic Coordinates List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GeographicCoordinatesPathWorldWindLayerImpl extends AbstractWorldWindLayerImpl implements GeographicCoordinatesPathWorldWindLayer 
{
	private MultiEObjectsAdapter geographicCoordinatesAdapter= null;
	public static final int GROUND_TRACE_STIPPLE_FACTOR = 0;
	public static final short GROUND_TRACE_STIPPLE_PATTERN = 0x00FF;
	
	/**
	 * The cached value of the '{@link #getGeographicCoordinatesList() <em>Geographic Coordinates List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeographicCoordinatesList()
	 * @generated
	 * @ordered
	 */
	protected EList<GeographicCoordinates> geographicCoordinatesList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	protected GeographicCoordinatesPathWorldWindLayerImpl() 
	{
		super();
		eAdapters().add(getGeographicCoordinatesAdapter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentUIPackage.Literals.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GeographicCoordinates> getGeographicCoordinatesList() {
		if (geographicCoordinatesList == null) {
			geographicCoordinatesList = new EObjectContainmentEList<GeographicCoordinates>(GeographicCoordinates.class, this, ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST);
		}
		return geographicCoordinatesList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void clearPath() 
	{
		if(!getGeographicCoordinatesList().isEmpty())
		{			
			ApogyCommonTransactionFacade.INSTANCE.basicClear(this, ApogyEarthEnvironmentUIPackage.Literals.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
				return ((InternalEList<?>)getGeographicCoordinatesList()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
				return getGeographicCoordinatesList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
				getGeographicCoordinatesList().clear();
				getGeographicCoordinatesList().addAll((Collection<? extends GeographicCoordinates>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
				getGeographicCoordinatesList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
				return geographicCoordinatesList != null && !geographicCoordinatesList.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER___CLEAR_PATH:
				clearPath();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	@Override
	public void selectionChanged(ISelection selection) 
	{
		// Nothing to do.
	}
	
	@Override
	public void dispose() 
	{
		eAdapters().remove(getGeographicCoordinatesAdapter());
		super.dispose();	
	}
	
	@Override
	protected void updateRenderableLayer() 
	{
		if(!isUpdating() && !isDisposed())
		{
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, true);

			RenderableLayer layer = getRenderableLayer();
			layer.removeAllRenderables();	
			
			if(isVisible())
			{
				addRenderable(layer);							
			}
			getRenderableLayer().firePropertyChange(AVKey.LAYER, null, this);
			
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, false);
		}		
	}
	
	protected void addRenderable(RenderableLayer layer)
	{
		try
		{
			if(!getGeographicCoordinatesList().isEmpty())
			{
				List<Polyline> polyLines = WorldWindUtils.createPolyLineFromGeographicCoordinatesListNoWrapAround(getGeographicCoordinatesList());
				for(Polyline polyLine : polyLines)
				{
					polyLine.setFollowTerrain(false);
					polyLine.setColor(Color.GREEN);
					polyLine.setStippleFactor(GROUND_TRACE_STIPPLE_FACTOR);
					polyLine.setStipplePattern(GROUND_TRACE_STIPPLE_PATTERN);
					
					layer.addRenderable(polyLine);
				}
			}			
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
	}
	
	protected MultiEObjectsAdapter getGeographicCoordinatesAdapter() 
	{
		if(geographicCoordinatesAdapter == null)
		{
			geographicCoordinatesAdapter = new MultiEObjectsAdapter()
			{
				@Override
				public void registerToEObject(EObject eObject) 
				{
					if(eObject instanceof GeographicCoordinates)
					{
						GeographicCoordinates newGeographicCoordinates = (GeographicCoordinates) eObject;
						newGeographicCoordinates.eAdapters().add(getGeographicCoordinatesAdapter());						
					}
					else
					{
						super.registerToEObject(eObject);
					}
				}
				
				@SuppressWarnings("unchecked")
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(!isDisposed())
					{
						if(msg.getNotifier() instanceof GeographicCoordinates)
						{
							int featureId = msg.getFeatureID(GeographicCoordinates.class);
							switch (featureId) 
							{							
								case ApogyEarthEnvironmentPackage.GEOGRAPHIC_COORDINATES__ELEVATION:
								case ApogyEarthEnvironmentPackage.GEOGRAPHIC_COORDINATES__LATITUDE:
								case ApogyEarthEnvironmentPackage.GEOGRAPHIC_COORDINATES__LONGITUDE:								
									if(isAutoUpdateEnabled()) updateRenderableLayer();								
								break;
	
								default:
								break;
							}
						}
						else if(msg.getNotifier() instanceof GeographicCoordinatesPathWorldWindLayer)
						{
							int featureId = msg.getFeatureID(GeographicCoordinatesPathWorldWindLayer.class);
							switch (featureId) 
							{														
								case ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
									
									switch (msg.getEventType()) 
									{
										case Notification.ADD:
										{
											GeographicCoordinates coord = (GeographicCoordinates) msg.getNewValue();
											coord.eAdapters().add(getGeographicCoordinatesAdapter());
										}
										break;
										
										case Notification.ADD_MANY:
										{
											Collection<GeographicCoordinates> coords = (Collection<GeographicCoordinates>) msg.getNewValue();
											for(GeographicCoordinates coord : coords)
											{
												coord.eAdapters().add(getGeographicCoordinatesAdapter());
											}
										}	
										break;
										
										case Notification.REMOVE:
										{
											GeographicCoordinates coord = (GeographicCoordinates) msg.getOldValue();
											coord.eAdapters().remove(getGeographicCoordinatesAdapter());
										}										
										break;
										
										case Notification.REMOVE_MANY:
										{
											Collection<GeographicCoordinates> coords = (Collection<GeographicCoordinates>) msg.getOldValue();
											for(GeographicCoordinates coord : coords)
											{
												coord.eAdapters().remove(getGeographicCoordinatesAdapter());
											}
										}
										break;
	
										default:
										break;
									}
									
									if(isAutoUpdateEnabled()) updateRenderableLayer();								
								break;														
	
								default:
								break;
							}
						}	
					}
				}				
			};
		}
		return geographicCoordinatesAdapter;
	}
	
} //GeographicCoordinatesPathWorldWindLayerImpl

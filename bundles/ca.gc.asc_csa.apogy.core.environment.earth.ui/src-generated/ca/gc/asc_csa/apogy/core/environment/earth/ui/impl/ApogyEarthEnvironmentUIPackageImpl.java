/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import javax.vecmath.Color3f;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.jface.viewers.ISelection;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationList;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationReference;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesPathWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.layers.RenderableLayer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyEarthEnvironmentUIPackageImpl extends EPackageImpl implements ApogyEarthEnvironmentUIPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthUIFacadeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthViewConfigurationListEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthViewConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthViewConfigurationReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractWorldWindLayerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geographicCoordinatesWorldWindLayerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthSurfaceLocationWorldWindLayerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geographicCoordinatesPathWorldWindLayerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass surfacePolygonWorldWindLayerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass airspaceWorldWindLayerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass kmlWorldWindLayerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractWorldWindLayerWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthSurfaceLocationWorldWindLayerWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass surfacePolygonWorldWindLayerWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass airspaceWorldWindLayerWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass kmlWorldWindLayerWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType color3fEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType renderableLayerEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iSelectionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType worldWindowEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyEarthEnvironmentUIPackageImpl() {
		super(eNS_URI, ApogyEarthEnvironmentUIFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyEarthEnvironmentUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyEarthEnvironmentUIPackage init() {
		if (isInited) return (ApogyEarthEnvironmentUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyEarthEnvironmentUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyEarthEnvironmentUIPackageImpl theApogyEarthEnvironmentUIPackage = (ApogyEarthEnvironmentUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyEarthEnvironmentUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyEarthEnvironmentUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyEarthEnvironmentPackage.eINSTANCE.eClass();
		ApogyCommonEMFUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyEarthEnvironmentUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyEarthEnvironmentUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyEarthEnvironmentUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyEarthEnvironmentUIPackage.eNS_URI, theApogyEarthEnvironmentUIPackage);
		return theApogyEarthEnvironmentUIPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthUIFacade() {
		return earthUIFacadeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEarthUIFacade__GetActiveEarthViewConfigurationList() {
		return earthUIFacadeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthViewConfigurationList() {
		return earthViewConfigurationListEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthViewConfigurationList_EarthViewConfigurations() {
		return (EReference)earthViewConfigurationListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthViewConfiguration() {
		return earthViewConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthViewConfiguration_Layers() {
		return (EReference)earthViewConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEarthViewConfiguration__SelectionChanged__ISelection() {
		return earthViewConfigurationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthViewConfigurationReference() {
		return earthViewConfigurationReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthViewConfigurationReference_EarthViewConfiguration() {
		return (EReference)earthViewConfigurationReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractWorldWindLayer() {
		return abstractWorldWindLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractWorldWindLayer_Disposed() {
		return (EAttribute)abstractWorldWindLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractWorldWindLayer_Visible() {
		return (EAttribute)abstractWorldWindLayerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractWorldWindLayer_Blinking() {
		return (EAttribute)abstractWorldWindLayerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractWorldWindLayer_RenderableLayer() {
		return (EAttribute)abstractWorldWindLayerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractWorldWindLayer_WorldWindow() {
		return (EAttribute)abstractWorldWindLayerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAbstractWorldWindLayer__Initialise() {
		return abstractWorldWindLayerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAbstractWorldWindLayer__Dispose() {
		return abstractWorldWindLayerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAbstractWorldWindLayer__SelectionChanged__ISelection() {
		return abstractWorldWindLayerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGeographicCoordinatesWorldWindLayer() {
		return geographicCoordinatesWorldWindLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGeographicCoordinatesWorldWindLayer_GeographicCoordinatesList() {
		return (EReference)geographicCoordinatesWorldWindLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGeographicCoordinatesWorldWindLayer_LockSelection() {
		return (EAttribute)geographicCoordinatesWorldWindLayerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGeographicCoordinatesWorldWindLayer_DisplayLocation() {
		return (EAttribute)geographicCoordinatesWorldWindLayerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGeographicCoordinatesWorldWindLayer_DisplayedRadius() {
		return (EAttribute)geographicCoordinatesWorldWindLayerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthSurfaceLocationWorldWindLayer() {
		return earthSurfaceLocationWorldWindLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthSurfaceLocationWorldWindLayer_EarthSurfaceLocation() {
		return (EReference)earthSurfaceLocationWorldWindLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEarthSurfaceLocationWorldWindLayer_TargetRadius() {
		return (EAttribute)earthSurfaceLocationWorldWindLayerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEarthSurfaceLocationWorldWindLayer_DisplayBalloon() {
		return (EAttribute)earthSurfaceLocationWorldWindLayerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEarthSurfaceLocationWorldWindLayer_DisplayLocation() {
		return (EAttribute)earthSurfaceLocationWorldWindLayerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEarthSurfaceLocationWorldWindLayer_Color() {
		return (EAttribute)earthSurfaceLocationWorldWindLayerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEarthSurfaceLocationWorldWindLayer_Opacity() {
		return (EAttribute)earthSurfaceLocationWorldWindLayerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGeographicCoordinatesPathWorldWindLayer() {
		return geographicCoordinatesPathWorldWindLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGeographicCoordinatesPathWorldWindLayer_GeographicCoordinatesList() {
		return (EReference)geographicCoordinatesPathWorldWindLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getGeographicCoordinatesPathWorldWindLayer__ClearPath() {
		return geographicCoordinatesPathWorldWindLayerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSurfacePolygonWorldWindLayer() {
		return surfacePolygonWorldWindLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSurfacePolygonWorldWindLayer_GeographicCoordinatesList() {
		return (EReference)surfacePolygonWorldWindLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSurfacePolygonWorldWindLayer_Url() {
		return (EAttribute)surfacePolygonWorldWindLayerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSurfacePolygonWorldWindLayer_Color() {
		return (EAttribute)surfacePolygonWorldWindLayerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSurfacePolygonWorldWindLayer_Opacity() {
		return (EAttribute)surfacePolygonWorldWindLayerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSurfacePolygonWorldWindLayer__IsCoordinatesInside__GeographicCoordinates() {
		return surfacePolygonWorldWindLayerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAirspaceWorldWindLayer() {
		return airspaceWorldWindLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAirspaceWorldWindLayer_LowerAltitude() {
		return (EAttribute)airspaceWorldWindLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAirspaceWorldWindLayer_UpperAltitude() {
		return (EAttribute)airspaceWorldWindLayerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKMLWorldWindLayer() {
		return kmlWorldWindLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getKMLWorldWindLayer_Url() {
		return (EAttribute)kmlWorldWindLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractWorldWindLayerWizardPagesProvider() {
		return abstractWorldWindLayerWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthSurfaceLocationWorldWindLayerWizardPagesProvider() {
		return earthSurfaceLocationWorldWindLayerWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSurfacePolygonWorldWindLayerWizardPagesProvider() {
		return surfacePolygonWorldWindLayerWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAirspaceWorldWindLayerWizardPagesProvider() {
		return airspaceWorldWindLayerWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKMLWorldWindLayerWizardPagesProvider() {
		return kmlWorldWindLayerWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getColor3f() {
		return color3fEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getRenderableLayer() {
		return renderableLayerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getISelection() {
		return iSelectionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getWorldWindow() {
		return worldWindowEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthEnvironmentUIFactory getApogyEarthEnvironmentUIFactory() {
		return (ApogyEarthEnvironmentUIFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		earthUIFacadeEClass = createEClass(EARTH_UI_FACADE);
		createEOperation(earthUIFacadeEClass, EARTH_UI_FACADE___GET_ACTIVE_EARTH_VIEW_CONFIGURATION_LIST);

		earthViewConfigurationListEClass = createEClass(EARTH_VIEW_CONFIGURATION_LIST);
		createEReference(earthViewConfigurationListEClass, EARTH_VIEW_CONFIGURATION_LIST__EARTH_VIEW_CONFIGURATIONS);

		earthViewConfigurationEClass = createEClass(EARTH_VIEW_CONFIGURATION);
		createEReference(earthViewConfigurationEClass, EARTH_VIEW_CONFIGURATION__LAYERS);
		createEOperation(earthViewConfigurationEClass, EARTH_VIEW_CONFIGURATION___SELECTION_CHANGED__ISELECTION);

		earthViewConfigurationReferenceEClass = createEClass(EARTH_VIEW_CONFIGURATION_REFERENCE);
		createEReference(earthViewConfigurationReferenceEClass, EARTH_VIEW_CONFIGURATION_REFERENCE__EARTH_VIEW_CONFIGURATION);

		abstractWorldWindLayerEClass = createEClass(ABSTRACT_WORLD_WIND_LAYER);
		createEAttribute(abstractWorldWindLayerEClass, ABSTRACT_WORLD_WIND_LAYER__DISPOSED);
		createEAttribute(abstractWorldWindLayerEClass, ABSTRACT_WORLD_WIND_LAYER__VISIBLE);
		createEAttribute(abstractWorldWindLayerEClass, ABSTRACT_WORLD_WIND_LAYER__BLINKING);
		createEAttribute(abstractWorldWindLayerEClass, ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER);
		createEAttribute(abstractWorldWindLayerEClass, ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW);
		createEOperation(abstractWorldWindLayerEClass, ABSTRACT_WORLD_WIND_LAYER___INITIALISE);
		createEOperation(abstractWorldWindLayerEClass, ABSTRACT_WORLD_WIND_LAYER___DISPOSE);
		createEOperation(abstractWorldWindLayerEClass, ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION);

		geographicCoordinatesWorldWindLayerEClass = createEClass(GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER);
		createEReference(geographicCoordinatesWorldWindLayerEClass, GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST);
		createEAttribute(geographicCoordinatesWorldWindLayerEClass, GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__LOCK_SELECTION);
		createEAttribute(geographicCoordinatesWorldWindLayerEClass, GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__DISPLAY_LOCATION);
		createEAttribute(geographicCoordinatesWorldWindLayerEClass, GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__DISPLAYED_RADIUS);

		earthSurfaceLocationWorldWindLayerEClass = createEClass(EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER);
		createEReference(earthSurfaceLocationWorldWindLayerEClass, EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION);
		createEAttribute(earthSurfaceLocationWorldWindLayerEClass, EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__TARGET_RADIUS);
		createEAttribute(earthSurfaceLocationWorldWindLayerEClass, EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DISPLAY_BALLOON);
		createEAttribute(earthSurfaceLocationWorldWindLayerEClass, EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DISPLAY_LOCATION);
		createEAttribute(earthSurfaceLocationWorldWindLayerEClass, EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__COLOR);
		createEAttribute(earthSurfaceLocationWorldWindLayerEClass, EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__OPACITY);

		geographicCoordinatesPathWorldWindLayerEClass = createEClass(GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER);
		createEReference(geographicCoordinatesPathWorldWindLayerEClass, GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST);
		createEOperation(geographicCoordinatesPathWorldWindLayerEClass, GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER___CLEAR_PATH);

		surfacePolygonWorldWindLayerEClass = createEClass(SURFACE_POLYGON_WORLD_WIND_LAYER);
		createEReference(surfacePolygonWorldWindLayerEClass, SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST);
		createEAttribute(surfacePolygonWorldWindLayerEClass, SURFACE_POLYGON_WORLD_WIND_LAYER__URL);
		createEAttribute(surfacePolygonWorldWindLayerEClass, SURFACE_POLYGON_WORLD_WIND_LAYER__COLOR);
		createEAttribute(surfacePolygonWorldWindLayerEClass, SURFACE_POLYGON_WORLD_WIND_LAYER__OPACITY);
		createEOperation(surfacePolygonWorldWindLayerEClass, SURFACE_POLYGON_WORLD_WIND_LAYER___IS_COORDINATES_INSIDE__GEOGRAPHICCOORDINATES);

		airspaceWorldWindLayerEClass = createEClass(AIRSPACE_WORLD_WIND_LAYER);
		createEAttribute(airspaceWorldWindLayerEClass, AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE);
		createEAttribute(airspaceWorldWindLayerEClass, AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE);

		kmlWorldWindLayerEClass = createEClass(KML_WORLD_WIND_LAYER);
		createEAttribute(kmlWorldWindLayerEClass, KML_WORLD_WIND_LAYER__URL);

		abstractWorldWindLayerWizardPagesProviderEClass = createEClass(ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER);

		earthSurfaceLocationWorldWindLayerWizardPagesProviderEClass = createEClass(EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER);

		surfacePolygonWorldWindLayerWizardPagesProviderEClass = createEClass(SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER);

		airspaceWorldWindLayerWizardPagesProviderEClass = createEClass(AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER);

		kmlWorldWindLayerWizardPagesProviderEClass = createEClass(KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER);

		// Create data types
		color3fEDataType = createEDataType(COLOR3F);
		renderableLayerEDataType = createEDataType(RENDERABLE_LAYER);
		iSelectionEDataType = createEDataType(ISELECTION);
		worldWindowEDataType = createEDataType(WORLD_WINDOW);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCoreInvocatorPackage theApogyCoreInvocatorPackage = (ApogyCoreInvocatorPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCoreInvocatorPackage.eNS_URI);
		ApogyCommonEMFPackage theApogyCommonEMFPackage = (ApogyCommonEMFPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFPackage.eNS_URI);
		ApogyCorePackage theApogyCorePackage = (ApogyCorePackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCorePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogyEarthEnvironmentPackage theApogyEarthEnvironmentPackage = (ApogyEarthEnvironmentPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyEarthEnvironmentPackage.eNS_URI);
		ApogyCommonEMFUIPackage theApogyCommonEMFUIPackage = (ApogyCommonEMFUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFUIPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		earthViewConfigurationListEClass.getESuperTypes().add(theApogyCoreInvocatorPackage.getAbstractToolsListContainer());
		earthViewConfigurationEClass.getESuperTypes().add(theApogyCommonEMFPackage.getNamed());
		earthViewConfigurationEClass.getESuperTypes().add(theApogyCommonEMFPackage.getDescribed());
		abstractWorldWindLayerEClass.getESuperTypes().add(theApogyCorePackage.getUpdatable());
		abstractWorldWindLayerEClass.getESuperTypes().add(theApogyCommonEMFPackage.getNamed());
		abstractWorldWindLayerEClass.getESuperTypes().add(theApogyCommonEMFPackage.getDescribed());
		geographicCoordinatesWorldWindLayerEClass.getESuperTypes().add(this.getAbstractWorldWindLayer());
		earthSurfaceLocationWorldWindLayerEClass.getESuperTypes().add(this.getAbstractWorldWindLayer());
		geographicCoordinatesPathWorldWindLayerEClass.getESuperTypes().add(this.getAbstractWorldWindLayer());
		surfacePolygonWorldWindLayerEClass.getESuperTypes().add(this.getAbstractWorldWindLayer());
		airspaceWorldWindLayerEClass.getESuperTypes().add(this.getSurfacePolygonWorldWindLayer());
		kmlWorldWindLayerEClass.getESuperTypes().add(this.getAbstractWorldWindLayer());
		abstractWorldWindLayerWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		earthSurfaceLocationWorldWindLayerWizardPagesProviderEClass.getESuperTypes().add(this.getAbstractWorldWindLayerWizardPagesProvider());
		surfacePolygonWorldWindLayerWizardPagesProviderEClass.getESuperTypes().add(this.getAbstractWorldWindLayerWizardPagesProvider());
		airspaceWorldWindLayerWizardPagesProviderEClass.getESuperTypes().add(this.getSurfacePolygonWorldWindLayerWizardPagesProvider());
		kmlWorldWindLayerWizardPagesProviderEClass.getESuperTypes().add(this.getAbstractWorldWindLayerWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(earthUIFacadeEClass, EarthUIFacade.class, "EarthUIFacade", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getEarthUIFacade__GetActiveEarthViewConfigurationList(), this.getEarthViewConfigurationList(), "getActiveEarthViewConfigurationList", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(earthViewConfigurationListEClass, EarthViewConfigurationList.class, "EarthViewConfigurationList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEarthViewConfigurationList_EarthViewConfigurations(), this.getEarthViewConfiguration(), null, "earthViewConfigurations", null, 0, -1, EarthViewConfigurationList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(earthViewConfigurationEClass, EarthViewConfiguration.class, "EarthViewConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEarthViewConfiguration_Layers(), this.getAbstractWorldWindLayer(), null, "layers", null, 0, -1, EarthViewConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getEarthViewConfiguration__SelectionChanged__ISelection(), null, "selectionChanged", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getISelection(), "selection", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(earthViewConfigurationReferenceEClass, EarthViewConfigurationReference.class, "EarthViewConfigurationReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEarthViewConfigurationReference_EarthViewConfiguration(), this.getEarthViewConfiguration(), null, "earthViewConfiguration", null, 0, 1, EarthViewConfigurationReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractWorldWindLayerEClass, AbstractWorldWindLayer.class, "AbstractWorldWindLayer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractWorldWindLayer_Disposed(), theEcorePackage.getEBoolean(), "disposed", "false", 0, 1, AbstractWorldWindLayer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractWorldWindLayer_Visible(), theEcorePackage.getEBoolean(), "visible", "true", 0, 1, AbstractWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractWorldWindLayer_Blinking(), theEcorePackage.getEBoolean(), "blinking", "false", 0, 1, AbstractWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractWorldWindLayer_RenderableLayer(), this.getRenderableLayer(), "renderableLayer", null, 0, 1, AbstractWorldWindLayer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractWorldWindLayer_WorldWindow(), this.getWorldWindow(), "worldWindow", null, 0, 1, AbstractWorldWindLayer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getAbstractWorldWindLayer__Initialise(), null, "initialise", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getAbstractWorldWindLayer__Dispose(), null, "dispose", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAbstractWorldWindLayer__SelectionChanged__ISelection(), null, "selectionChanged", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getISelection(), "selection", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(geographicCoordinatesWorldWindLayerEClass, GeographicCoordinatesWorldWindLayer.class, "GeographicCoordinatesWorldWindLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGeographicCoordinatesWorldWindLayer_GeographicCoordinatesList(), theApogyEarthEnvironmentPackage.getGeographicCoordinates(), null, "geographicCoordinatesList", null, 0, -1, GeographicCoordinatesWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGeographicCoordinatesWorldWindLayer_LockSelection(), theEcorePackage.getEBoolean(), "lockSelection", "true", 0, 1, GeographicCoordinatesWorldWindLayer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGeographicCoordinatesWorldWindLayer_DisplayLocation(), theEcorePackage.getEBoolean(), "displayLocation", "true", 0, 1, GeographicCoordinatesWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGeographicCoordinatesWorldWindLayer_DisplayedRadius(), theEcorePackage.getEDouble(), "displayedRadius", "50", 0, 1, GeographicCoordinatesWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(earthSurfaceLocationWorldWindLayerEClass, EarthSurfaceLocationWorldWindLayer.class, "EarthSurfaceLocationWorldWindLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEarthSurfaceLocationWorldWindLayer_EarthSurfaceLocation(), theApogyEarthEnvironmentPackage.getEarthSurfaceLocation(), null, "earthSurfaceLocation", null, 0, 1, EarthSurfaceLocationWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEarthSurfaceLocationWorldWindLayer_TargetRadius(), theEcorePackage.getEDouble(), "targetRadius", "50", 0, 1, EarthSurfaceLocationWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEarthSurfaceLocationWorldWindLayer_DisplayBalloon(), theEcorePackage.getEBoolean(), "displayBalloon", "true", 0, 1, EarthSurfaceLocationWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEarthSurfaceLocationWorldWindLayer_DisplayLocation(), theEcorePackage.getEBoolean(), "displayLocation", "true", 0, 1, EarthSurfaceLocationWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEarthSurfaceLocationWorldWindLayer_Color(), this.getColor3f(), "color", "0.0,1.0,0.0", 0, 1, EarthSurfaceLocationWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEarthSurfaceLocationWorldWindLayer_Opacity(), theEcorePackage.getEDouble(), "opacity", "0.2", 0, 1, EarthSurfaceLocationWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(geographicCoordinatesPathWorldWindLayerEClass, GeographicCoordinatesPathWorldWindLayer.class, "GeographicCoordinatesPathWorldWindLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGeographicCoordinatesPathWorldWindLayer_GeographicCoordinatesList(), theApogyEarthEnvironmentPackage.getGeographicCoordinates(), null, "geographicCoordinatesList", null, 0, -1, GeographicCoordinatesPathWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getGeographicCoordinatesPathWorldWindLayer__ClearPath(), null, "clearPath", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(surfacePolygonWorldWindLayerEClass, SurfacePolygonWorldWindLayer.class, "SurfacePolygonWorldWindLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSurfacePolygonWorldWindLayer_GeographicCoordinatesList(), theApogyEarthEnvironmentPackage.getGeographicCoordinates(), null, "geographicCoordinatesList", null, 0, -1, SurfacePolygonWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSurfacePolygonWorldWindLayer_Url(), theEcorePackage.getEString(), "url", null, 0, 1, SurfacePolygonWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSurfacePolygonWorldWindLayer_Color(), this.getColor3f(), "color", "0.0,1.0,0.0", 0, 1, SurfacePolygonWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSurfacePolygonWorldWindLayer_Opacity(), theEcorePackage.getEDouble(), "opacity", "1.0", 0, 1, SurfacePolygonWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getSurfacePolygonWorldWindLayer__IsCoordinatesInside__GeographicCoordinates(), theEcorePackage.getEBoolean(), "isCoordinatesInside", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyEarthEnvironmentPackage.getGeographicCoordinates(), "coordinates", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(airspaceWorldWindLayerEClass, AirspaceWorldWindLayer.class, "AirspaceWorldWindLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAirspaceWorldWindLayer_LowerAltitude(), theEcorePackage.getEDouble(), "lowerAltitude", null, 0, 1, AirspaceWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAirspaceWorldWindLayer_UpperAltitude(), theEcorePackage.getEDouble(), "upperAltitude", "10000", 0, 1, AirspaceWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(kmlWorldWindLayerEClass, KMLWorldWindLayer.class, "KMLWorldWindLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getKMLWorldWindLayer_Url(), theEcorePackage.getEString(), "url", null, 0, 1, KMLWorldWindLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractWorldWindLayerWizardPagesProviderEClass, AbstractWorldWindLayerWizardPagesProvider.class, "AbstractWorldWindLayerWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(earthSurfaceLocationWorldWindLayerWizardPagesProviderEClass, EarthSurfaceLocationWorldWindLayerWizardPagesProvider.class, "EarthSurfaceLocationWorldWindLayerWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(surfacePolygonWorldWindLayerWizardPagesProviderEClass, SurfacePolygonWorldWindLayerWizardPagesProvider.class, "SurfacePolygonWorldWindLayerWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(airspaceWorldWindLayerWizardPagesProviderEClass, AirspaceWorldWindLayerWizardPagesProvider.class, "AirspaceWorldWindLayerWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(kmlWorldWindLayerWizardPagesProviderEClass, KMLWorldWindLayerWizardPagesProvider.class, "KMLWorldWindLayerWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize data types
		initEDataType(color3fEDataType, Color3f.class, "Color3f", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(renderableLayerEDataType, RenderableLayer.class, "RenderableLayer", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iSelectionEDataType, ISelection.class, "ISelection", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(worldWindowEDataType, WorldWindow.class, "WorldWindow", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "prefix", "ApogyEarthEnvironmentUI",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyCoreEnvironmentUI",
			 "complianceLevel", "6.0",
			 "suppressGenModelAnnotations", "false",
			 "dynamicTemplates", "true",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.core.environment.earth.ui/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.core.environment.earth.ui.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.core.environment.earth"
		   });	
		addAnnotation
		  (color3fEDataType, 
		   source, 
		   new String[] {
			 "documentation", "*\nTypes definition."
		   });	
		addAnnotation
		  (getEarthUIFacade__GetActiveEarthViewConfigurationList(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns the getActiveEarthViewConfigurationList in the Active Session.\n@return The getActiveEarthViewConfigurationList in the Active Session, creates one if none is found."
		   });	
		addAnnotation
		  (earthViewConfigurationListEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA list of EarthViewConfigurations."
		   });	
		addAnnotation
		  (earthViewConfigurationEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nClass used to contain the configuration to be used for an EarthView."
		   });	
		addAnnotation
		  (getEarthViewConfiguration__SelectionChanged__ISelection(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod that notifies all AbstractWorldWindLayer of a new selection.\n@param selection The new selection made."
		   });	
		addAnnotation
		  (getEarthViewConfiguration_Layers(), 
		   source, 
		   new String[] {
			 "documentation", "*\nList of Maps being displayed.",
			 "children", "true",
			 "notify", "true",
			 "property", "None"
		   });	
		addAnnotation
		  (earthViewConfigurationReferenceEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nClass implementing a reference to a EarthViewConfiguration."
		   });	
		addAnnotation
		  (abstractWorldWindLayerEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nDefines a Layer that can be shown on the WordWindModel"
		   });	
		addAnnotation
		  (getAbstractWorldWindLayer__Initialise(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called when the layer is first rendered. Can be called multiple times."
		   });	
		addAnnotation
		  (getAbstractWorldWindLayer__Dispose(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called when the layer is not longer\nneeded (i.e. when it gets deleted.)"
		   });	
		addAnnotation
		  (getAbstractWorldWindLayer__SelectionChanged__ISelection(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called when a selection is made by the user.\nSub-classes can override this method if applicable.\n@param selection The new selection made."
		   });	
		addAnnotation
		  (getAbstractWorldWindLayer_Disposed(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not this layer has been disposed of.",
			 "notify", "false",
			 "property", "None"
		   });	
		addAnnotation
		  (getAbstractWorldWindLayer_Visible(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not to display the layer.",
			 "notify", "true",
			 "property", "Editable"
		   });	
		addAnnotation
		  (getAbstractWorldWindLayer_Blinking(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhen enabled, make the layer blink.",
			 "notify", "true",
			 "property", "Editable"
		   });	
		addAnnotation
		  (getAbstractWorldWindLayer_RenderableLayer(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe RenderableLayer associated with this Layer",
			 "children", "false",
			 "notify", "false",
			 "property", "None"
		   });	
		addAnnotation
		  (getAbstractWorldWindLayer_WorldWindow(), 
		   source, 
		   new String[] {
			 "documentation", "*\nA reference to the WorldWindow displaying the layer.",
			 "children", "false",
			 "notify", "false",
			 "property", "None"
		   });	
		addAnnotation
		  (geographicCoordinatesWorldWindLayerEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWorldWindLayer that displays a list of position on Earth."
		   });	
		addAnnotation
		  (getGeographicCoordinatesWorldWindLayer_GeographicCoordinatesList(), 
		   source, 
		   new String[] {
			 "notify", "true"
		   });	
		addAnnotation
		  (getGeographicCoordinatesWorldWindLayer_LockSelection(), 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecifies whether or not the layer will respond to new selection.",
			 "notify", "true"
		   });	
		addAnnotation
		  (getGeographicCoordinatesWorldWindLayer_DisplayLocation(), 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecified wether or not to add the lat/lon/alt as part of the displayed text.",
			 "notify", "true",
			 "propertyCategory", "VISUALS"
		   });	
		addAnnotation
		  (getGeographicCoordinatesWorldWindLayer_DisplayedRadius(), 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecifies the radius of the circle display at the target location.",
			 "propertyCategory", "VISUALS",
			 "apogy_units", "km"
		   });	
		addAnnotation
		  (earthSurfaceLocationWorldWindLayerEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn AbstractWorldWindLayer representing an location on the surface of the Earth."
		   });	
		addAnnotation
		  (getEarthSurfaceLocationWorldWindLayer_EarthSurfaceLocation(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReference to the EarthSurfaceLocation to display."
		   });	
		addAnnotation
		  (getEarthSurfaceLocationWorldWindLayer_TargetRadius(), 
		   source, 
		   new String[] {
			 "documentation", "The radius of the target.",
			 "propertyCategory", "VISUALS",
			 "apogy_units", "km"
		   });	
		addAnnotation
		  (getEarthSurfaceLocationWorldWindLayer_DisplayBalloon(), 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecified whether or not to add a balloon to display the name of the details of the surface location.",
			 "notify", "true",
			 "propertyCategory", "VISUALS"
		   });	
		addAnnotation
		  (getEarthSurfaceLocationWorldWindLayer_DisplayLocation(), 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecified whether or not to add the lat/lon/alt as part of the displayed text.",
			 "notify", "true",
			 "propertyCategory", "VISUALS"
		   });	
		addAnnotation
		  (getEarthSurfaceLocationWorldWindLayer_Color(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe color of the location."
		   });	
		addAnnotation
		  (getEarthSurfaceLocationWorldWindLayer_Opacity(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe opacity. 1 if fully opaque, 0 is fully transparent."
		   });	
		addAnnotation
		  (geographicCoordinatesPathWorldWindLayerEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\n An AbstractWorldWindLayer representing a path made out of a list of GeographicCoordinates."
		   });	
		addAnnotation
		  (getGeographicCoordinatesPathWorldWindLayer__ClearPath(), 
		   source, 
		   new String[] {
			 "documentation", "*\nRemoves all points in the path."
		   });	
		addAnnotation
		  (getGeographicCoordinatesPathWorldWindLayer_GeographicCoordinatesList(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of geographical coordinates.",
			 "notify", "true"
		   });	
		addAnnotation
		  (surfacePolygonWorldWindLayerEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\n An AbstractWorldWindLayer representing a surface area defined by a polygon on the surface."
		   });	
		addAnnotation
		  (getSurfacePolygonWorldWindLayer__IsCoordinatesInside__GeographicCoordinates(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns whether or not a specified GeographicCoordinates is inside the Airspace.\n@param coordinates The GeographicCoordinates.\n@return True if the coordinates are inside, false otherwise."
		   });	
		addAnnotation
		  (getSurfacePolygonWorldWindLayer_GeographicCoordinatesList(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of geographical coordinates representing the outline of the Airspace.",
			 "notify", "true"
		   });	
		addAnnotation
		  (getSurfacePolygonWorldWindLayer_Url(), 
		   source, 
		   new String[] {
			 "documentation", "*\nURL to a CSV formated file that contains the geographical coordinates defining the airspace contours.",
			 "notify", "true"
		   });	
		addAnnotation
		  (getSurfacePolygonWorldWindLayer_Color(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe color of the airspace."
		   });	
		addAnnotation
		  (getSurfacePolygonWorldWindLayer_Opacity(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe opacity. 1 if fully opaque, 0 is fully transparent."
		   });	
		addAnnotation
		  (airspaceWorldWindLayerEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\n An AbstractWorldWindLayer representing volume in the atmosphere."
		   });	
		addAnnotation
		  (getAirspaceWorldWindLayer_LowerAltitude(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe lower altitude limit, relative to Mean Sea Level. A negative value has the airspace follow the ground.",
			 "notify", "true",
			 "apogy_units", "m"
		   });	
		addAnnotation
		  (getAirspaceWorldWindLayer_UpperAltitude(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe upper altitude limit, relative to Mean Sea Level.",
			 "notify", "true",
			 "apogy_units", "m"
		   });	
		addAnnotation
		  (kmlWorldWindLayerEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\n An AbstractWorldWindLayer representing the content of a KML / KMZ file."
		   });	
		addAnnotation
		  (getKMLWorldWindLayer_Url(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe URL to the KML/KMZ file.",
			 "notify", "true"
		   });	
		addAnnotation
		  (abstractWorldWindLayerWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizard Support Classes"
		   });	
		addAnnotation
		  (earthSurfaceLocationWorldWindLayerWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizardPagesProvider used to create EarthSurfaceLocationWorldWindLayer."
		   });	
		addAnnotation
		  (surfacePolygonWorldWindLayerWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizardPagesProvider used to create SurfacePolygonWorldWindLayer."
		   });	
		addAnnotation
		  (airspaceWorldWindLayerWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizardPagesProvider used to create AirspaceWorldWindLayer."
		   });	
		addAnnotation
		  (kmlWorldWindLayerWizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nWizardPagesProvider used to create KMLWorldWindLayer."
		   });
	}

} //ApogyEarthEnvironmentUIPackageImpl

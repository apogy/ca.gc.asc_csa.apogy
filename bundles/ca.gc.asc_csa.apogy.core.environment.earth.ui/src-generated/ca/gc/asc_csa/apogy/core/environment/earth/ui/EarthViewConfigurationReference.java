/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earth View Configuration Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Class implementing a reference to a EarthViewConfiguration.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationReference#getEarthViewConfiguration <em>Earth View Configuration</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getEarthViewConfigurationReference()
 * @model
 * @generated
 */
public interface EarthViewConfigurationReference extends EObject {
	/**
	 * Returns the value of the '<em><b>Earth View Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Earth View Configuration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Earth View Configuration</em>' reference.
	 * @see #setEarthViewConfiguration(EarthViewConfiguration)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getEarthViewConfigurationReference_EarthViewConfiguration()
	 * @model
	 * @generated
	 */
	EarthViewConfiguration getEarthViewConfiguration();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationReference#getEarthViewConfiguration <em>Earth View Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Earth View Configuration</em>' reference.
	 * @see #getEarthViewConfiguration()
	 * @generated
	 */
	void setEarthViewConfiguration(EarthViewConfiguration value);

} // EarthViewConfigurationReference

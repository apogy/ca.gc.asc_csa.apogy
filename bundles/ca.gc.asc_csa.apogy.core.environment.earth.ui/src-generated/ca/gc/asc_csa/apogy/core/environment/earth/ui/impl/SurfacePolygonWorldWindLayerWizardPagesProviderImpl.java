/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Canadian Space Agency (CSA) - Initial API and implementation
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
 *           
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.wizards.SurfacePolygonWorldWindLayerURLWizardPage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.wizards.SurfacePolygonWorldWindLayerWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Surface Polygon World Wind Layer Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SurfacePolygonWorldWindLayerWizardPagesProviderImpl extends AbstractWorldWindLayerWizardPagesProviderImpl implements SurfacePolygonWorldWindLayerWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SurfacePolygonWorldWindLayerWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentUIPackage.Literals.SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER;
	}

	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		SurfacePolygonWorldWindLayer layer = ApogyEarthEnvironmentUIFactory.eINSTANCE.createSurfacePolygonWorldWindLayer();
		return layer;
	};
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		SurfacePolygonWorldWindLayer layer = (SurfacePolygonWorldWindLayer) eObject;
		
		SurfacePolygonWorldWindLayerURLWizardPage surfacePolygonWorldWindLayerURLWizardPage = new SurfacePolygonWorldWindLayerURLWizardPage(layer);
		list.add(surfacePolygonWorldWindLayerURLWizardPage);
		
		SurfacePolygonWorldWindLayerWizardPage surfacePolygonWorldWindLayerWizardPage = new SurfacePolygonWorldWindLayerWizardPage(layer);
		list.add(surfacePolygonWorldWindLayerWizardPage);
		
		return list;
	}
} //SurfacePolygonWorldWindLayerWizardPagesProviderImpl

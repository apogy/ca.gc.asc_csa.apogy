/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Canadian Space Agency (CSA) - Initial API and implementation
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
 *           
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import java.net.URL;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.Activator;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayer;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.ogc.kml.KMLRoot;
import gov.nasa.worldwind.ogc.kml.impl.KMLController;
import gov.nasa.worldwind.util.WWIO;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>KML World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.KMLWorldWindLayerImpl#getUrl <em>Url</em>}</li>
 * </ul>
 *
 * @generated
 */
public class KMLWorldWindLayerImpl extends AbstractWorldWindLayerImpl implements KMLWorldWindLayer 
{
	private boolean loading = false;
	private KMLController kmlController = null;

	/**
	 * The default value of the '{@link #getUrl() <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrl()
	 * @generated
	 * @ordered
	 */
	protected static final String URL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUrl() <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrl()
	 * @generated
	 * @ordered
	 */
	protected String url = URL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KMLWorldWindLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentUIPackage.Literals.KML_WORLD_WIND_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setUrl(String newUrl) 
	{
		setUrlGen(newUrl);
		
		// Load File.
		loadFile();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUrlGen(String newUrl) {
		String oldUrl = url;
		url = newUrl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.KML_WORLD_WIND_LAYER__URL, oldUrl, url));
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.KML_WORLD_WIND_LAYER__URL:
				return getUrl();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.KML_WORLD_WIND_LAYER__URL:
				setUrl((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.KML_WORLD_WIND_LAYER__URL:
				setUrl(URL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.KML_WORLD_WIND_LAYER__URL:
				return URL_EDEFAULT == null ? url != null : !URL_EDEFAULT.equals(url);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (url: ");
		result.append(url);
		result.append(')');
		return result.toString();
	}

	@Override
	public void initialise() 
	{
		loadFile();
		
		super.initialise();
	}
	
	@Override
	protected void updateRenderableLayer() 
	{
		if(!isUpdating() && !isDisposed())
		{
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, true);

			RenderableLayer layer = getRenderableLayer();
			layer.removeAllRenderables();	
			
			if(isVisible() && kmlController != null)
			{
				layer.addRenderable(kmlController);						
			}
			getRenderableLayer().firePropertyChange(AVKey.LAYER, null, this);
			
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, false);
		}			
	}

	private void loadFile()
	{
		if(!loading)
		{							
			loading = true;
			
			// Clears the layer.
			getRenderableLayer().removeAllRenderables();				
						
			String kmlSource = getUrl();			
			URL url = WWIO.makeURL(kmlSource);
			
			if (url != null)
			{			
				System.out.println("URL : " + url);
				
				Job job = new Job("Loading KML <" + url + ">")
				{
					@Override
					protected IStatus run(IProgressMonitor arg0) 
					{
						try 
						{			
							Logger.INSTANCE.log(Activator.ID, "Loading KML from <" + url.toString() + ">...", EventSeverity.INFO);
							kmlController = null;
							
							KMLRoot kmlRoot = KMLRoot.createAndParse(url);		
																				
							kmlController = new KMLController(kmlRoot);
							Logger.INSTANCE.log(Activator.ID, "KML Loaded.", EventSeverity.OK);								
							
							RenderableLayer layer = getRenderableLayer();
							layer.removeAllRenderables();	
							layer.addRenderable(kmlController);							
							
							getRenderableLayer().firePropertyChange(AVKey.LAYER, null, this);
	
						} 
						catch (Exception e) 
						{			
							Logger.INSTANCE.log(Activator.ID, "Failed to load KML from <" + url.toString() + "> !", EventSeverity.ERROR);
							e.printStackTrace();
						}
						
						loading = false;
						return Status.OK_STATUS;
					}
				};
				job.schedule();						
			}
		}			
	}	
} //KMLWorldWindLayerImpl

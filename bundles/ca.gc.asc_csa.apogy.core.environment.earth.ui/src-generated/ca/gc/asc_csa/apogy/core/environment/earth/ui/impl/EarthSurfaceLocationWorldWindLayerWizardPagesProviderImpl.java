/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.EarthSurfaceLocation;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.wizards.GeographicCoordinatesWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Surface Location World Wind Layer Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EarthSurfaceLocationWorldWindLayerWizardPagesProviderImpl extends AbstractWorldWindLayerLayerWizardPagesProviderImpl implements EarthSurfaceLocationWorldWindLayerWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EarthSurfaceLocationWorldWindLayerWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentUIPackage.Literals.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER;
	}
	
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		EarthSurfaceLocationWorldWindLayer layer = ApogyEarthEnvironmentUIFactory.eINSTANCE.createEarthSurfaceLocationWorldWindLayer();

		EarthSurfaceLocation location = ApogyEarthEnvironmentFactory.eINSTANCE.createEarthSurfaceLocation();
		location.setElevation(100);
		location.setLatitude(Math.toRadians(45.518206644445));
		location.setLongitude(Math.toRadians(-73.393904468182));
		layer.setEarthSurfaceLocation(location);
		
		return layer;
	};
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		EarthSurfaceLocationWorldWindLayer earthSurfaceLocationWorldWindLayer = (EarthSurfaceLocationWorldWindLayer) eObject;
		
		GeographicCoordinatesWizardPage geographicCoordinatesWizardPage = new GeographicCoordinatesWizardPage(earthSurfaceLocationWorldWindLayer.getEarthSurfaceLocation());
		list.add(geographicCoordinatesWizardPage);
		
		return list;
	}
} //EarthSurfaceLocationWorldWindLayerWizardPagesProviderImpl

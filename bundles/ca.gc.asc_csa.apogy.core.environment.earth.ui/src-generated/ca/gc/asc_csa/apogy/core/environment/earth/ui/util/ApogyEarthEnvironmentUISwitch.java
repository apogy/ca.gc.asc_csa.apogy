/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.core.Updatable;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.*;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationList;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationReference;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesPathWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.invocator.AbstractToolsListContainer;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage
 * @generated
 */
public class ApogyEarthEnvironmentUISwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ApogyEarthEnvironmentUIPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthEnvironmentUISwitch() {
		if (modelPackage == null) {
			modelPackage = ApogyEarthEnvironmentUIPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ApogyEarthEnvironmentUIPackage.EARTH_UI_FACADE: {
				EarthUIFacade earthUIFacade = (EarthUIFacade)theEObject;
				T result = caseEarthUIFacade(earthUIFacade);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION_LIST: {
				EarthViewConfigurationList earthViewConfigurationList = (EarthViewConfigurationList)theEObject;
				T result = caseEarthViewConfigurationList(earthViewConfigurationList);
				if (result == null) result = caseAbstractToolsListContainer(earthViewConfigurationList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION: {
				EarthViewConfiguration earthViewConfiguration = (EarthViewConfiguration)theEObject;
				T result = caseEarthViewConfiguration(earthViewConfiguration);
				if (result == null) result = caseNamed(earthViewConfiguration);
				if (result == null) result = caseDescribed(earthViewConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION_REFERENCE: {
				EarthViewConfigurationReference earthViewConfigurationReference = (EarthViewConfigurationReference)theEObject;
				T result = caseEarthViewConfigurationReference(earthViewConfigurationReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER: {
				AbstractWorldWindLayer abstractWorldWindLayer = (AbstractWorldWindLayer)theEObject;
				T result = caseAbstractWorldWindLayer(abstractWorldWindLayer);
				if (result == null) result = caseUpdatable(abstractWorldWindLayer);
				if (result == null) result = caseNamed(abstractWorldWindLayer);
				if (result == null) result = caseDescribed(abstractWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER: {
				GeographicCoordinatesWorldWindLayer geographicCoordinatesWorldWindLayer = (GeographicCoordinatesWorldWindLayer)theEObject;
				T result = caseGeographicCoordinatesWorldWindLayer(geographicCoordinatesWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(geographicCoordinatesWorldWindLayer);
				if (result == null) result = caseUpdatable(geographicCoordinatesWorldWindLayer);
				if (result == null) result = caseNamed(geographicCoordinatesWorldWindLayer);
				if (result == null) result = caseDescribed(geographicCoordinatesWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER: {
				EarthSurfaceLocationWorldWindLayer earthSurfaceLocationWorldWindLayer = (EarthSurfaceLocationWorldWindLayer)theEObject;
				T result = caseEarthSurfaceLocationWorldWindLayer(earthSurfaceLocationWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(earthSurfaceLocationWorldWindLayer);
				if (result == null) result = caseUpdatable(earthSurfaceLocationWorldWindLayer);
				if (result == null) result = caseNamed(earthSurfaceLocationWorldWindLayer);
				if (result == null) result = caseDescribed(earthSurfaceLocationWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER: {
				GeographicCoordinatesPathWorldWindLayer geographicCoordinatesPathWorldWindLayer = (GeographicCoordinatesPathWorldWindLayer)theEObject;
				T result = caseGeographicCoordinatesPathWorldWindLayer(geographicCoordinatesPathWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(geographicCoordinatesPathWorldWindLayer);
				if (result == null) result = caseUpdatable(geographicCoordinatesPathWorldWindLayer);
				if (result == null) result = caseNamed(geographicCoordinatesPathWorldWindLayer);
				if (result == null) result = caseDescribed(geographicCoordinatesPathWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER: {
				SurfacePolygonWorldWindLayer surfacePolygonWorldWindLayer = (SurfacePolygonWorldWindLayer)theEObject;
				T result = caseSurfacePolygonWorldWindLayer(surfacePolygonWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(surfacePolygonWorldWindLayer);
				if (result == null) result = caseUpdatable(surfacePolygonWorldWindLayer);
				if (result == null) result = caseNamed(surfacePolygonWorldWindLayer);
				if (result == null) result = caseDescribed(surfacePolygonWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER: {
				AirspaceWorldWindLayer airspaceWorldWindLayer = (AirspaceWorldWindLayer)theEObject;
				T result = caseAirspaceWorldWindLayer(airspaceWorldWindLayer);
				if (result == null) result = caseSurfacePolygonWorldWindLayer(airspaceWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(airspaceWorldWindLayer);
				if (result == null) result = caseUpdatable(airspaceWorldWindLayer);
				if (result == null) result = caseNamed(airspaceWorldWindLayer);
				if (result == null) result = caseDescribed(airspaceWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.KML_WORLD_WIND_LAYER: {
				KMLWorldWindLayer kmlWorldWindLayer = (KMLWorldWindLayer)theEObject;
				T result = caseKMLWorldWindLayer(kmlWorldWindLayer);
				if (result == null) result = caseAbstractWorldWindLayer(kmlWorldWindLayer);
				if (result == null) result = caseUpdatable(kmlWorldWindLayer);
				if (result == null) result = caseNamed(kmlWorldWindLayer);
				if (result == null) result = caseDescribed(kmlWorldWindLayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER: {
				AbstractWorldWindLayerWizardPagesProvider abstractWorldWindLayerWizardPagesProvider = (AbstractWorldWindLayerWizardPagesProvider)theEObject;
				T result = caseAbstractWorldWindLayerWizardPagesProvider(abstractWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(abstractWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(abstractWorldWindLayerWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER: {
				EarthSurfaceLocationWorldWindLayerWizardPagesProvider earthSurfaceLocationWorldWindLayerWizardPagesProvider = (EarthSurfaceLocationWorldWindLayerWizardPagesProvider)theEObject;
				T result = caseEarthSurfaceLocationWorldWindLayerWizardPagesProvider(earthSurfaceLocationWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseAbstractWorldWindLayerWizardPagesProvider(earthSurfaceLocationWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(earthSurfaceLocationWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(earthSurfaceLocationWorldWindLayerWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER: {
				SurfacePolygonWorldWindLayerWizardPagesProvider surfacePolygonWorldWindLayerWizardPagesProvider = (SurfacePolygonWorldWindLayerWizardPagesProvider)theEObject;
				T result = caseSurfacePolygonWorldWindLayerWizardPagesProvider(surfacePolygonWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseAbstractWorldWindLayerWizardPagesProvider(surfacePolygonWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(surfacePolygonWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(surfacePolygonWorldWindLayerWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER: {
				AirspaceWorldWindLayerWizardPagesProvider airspaceWorldWindLayerWizardPagesProvider = (AirspaceWorldWindLayerWizardPagesProvider)theEObject;
				T result = caseAirspaceWorldWindLayerWizardPagesProvider(airspaceWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseSurfacePolygonWorldWindLayerWizardPagesProvider(airspaceWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseAbstractWorldWindLayerWizardPagesProvider(airspaceWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(airspaceWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(airspaceWorldWindLayerWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthEnvironmentUIPackage.KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER: {
				KMLWorldWindLayerWizardPagesProvider kmlWorldWindLayerWizardPagesProvider = (KMLWorldWindLayerWizardPagesProvider)theEObject;
				T result = caseKMLWorldWindLayerWizardPagesProvider(kmlWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseAbstractWorldWindLayerWizardPagesProvider(kmlWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(kmlWorldWindLayerWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(kmlWorldWindLayerWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth UI Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth UI Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthUIFacade(EarthUIFacade object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth View Configuration List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth View Configuration List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthViewConfigurationList(EarthViewConfigurationList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth View Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth View Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthViewConfiguration(EarthViewConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth View Configuration Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth View Configuration Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthViewConfigurationReference(EarthViewConfigurationReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractWorldWindLayer(AbstractWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Geographic Coordinates World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Geographic Coordinates World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGeographicCoordinatesWorldWindLayer(GeographicCoordinatesWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth Surface Location World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth Surface Location World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthSurfaceLocationWorldWindLayer(EarthSurfaceLocationWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Geographic Coordinates Path World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Geographic Coordinates Path World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGeographicCoordinatesPathWorldWindLayer(GeographicCoordinatesPathWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Surface Polygon World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Surface Polygon World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSurfacePolygonWorldWindLayer(SurfacePolygonWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Airspace World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Airspace World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAirspaceWorldWindLayer(AirspaceWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>KML World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>KML World Wind Layer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKMLWorldWindLayer(KMLWorldWindLayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract World Wind Layer Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractWorldWindLayerWizardPagesProvider(AbstractWorldWindLayerWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthSurfaceLocationWorldWindLayerWizardPagesProvider(EarthSurfaceLocationWorldWindLayerWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Surface Polygon World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Surface Polygon World Wind Layer Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSurfacePolygonWorldWindLayerWizardPagesProvider(SurfacePolygonWorldWindLayerWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Airspace World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Airspace World Wind Layer Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAirspaceWorldWindLayerWizardPagesProvider(AirspaceWorldWindLayerWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>KML World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>KML World Wind Layer Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKMLWorldWindLayerWizardPagesProvider(KMLWorldWindLayerWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Tools List Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Tools List Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractToolsListContainer(AbstractToolsListContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamed(Named object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Described</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Described</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDescribed(Described object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Updatable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Updatable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUpdatable(Updatable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWizardPagesProvider(WizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedDescribedWizardPagesProvider(NamedDescribedWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ApogyEarthEnvironmentUISwitch

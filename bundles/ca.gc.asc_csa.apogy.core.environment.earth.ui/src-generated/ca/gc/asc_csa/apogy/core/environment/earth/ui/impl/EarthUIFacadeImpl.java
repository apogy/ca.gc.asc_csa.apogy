/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationList;
import ca.gc.asc_csa.apogy.core.invocator.AbstractToolsListContainer;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFactory;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ToolsList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth UI Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EarthUIFacadeImpl extends MinimalEObjectImpl.Container implements EarthUIFacade 
{
	
	private static EarthUIFacade instance = null;
	
	public static EarthUIFacade getInstance() 
	{
		if (instance == null) 
		{
			instance = new EarthUIFacadeImpl();
		}
		return instance;
	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EarthUIFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentUIPackage.Literals.EARTH_UI_FACADE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EarthViewConfigurationList getActiveEarthViewConfigurationList() 
	{
		EarthViewConfigurationList earthViewConfigurationList = null;
		
		InvocatorSession session = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();
		if(session != null)
		{
			ToolsList toolsList = session.getToolsList();
			if (toolsList == null) {
				toolsList = ApogyCoreInvocatorFactory.eINSTANCE.createToolsList();
				ApogyCommonTransactionFacade.INSTANCE.basicSet(session,
						ApogyCoreInvocatorPackage.Literals.INVOCATOR_SESSION__TOOLS_LIST, toolsList);

			}

			Iterator<AbstractToolsListContainer> it = toolsList.getToolsListContainers().iterator();
			while (earthViewConfigurationList == null && it.hasNext()) 
			{
				AbstractToolsListContainer abstractToolsListContainer = it.next();
				if (abstractToolsListContainer instanceof EarthViewConfigurationList) 
				{
					earthViewConfigurationList = (EarthViewConfigurationList) abstractToolsListContainer;
				}
			}

			// If none was found, create one.
			if (earthViewConfigurationList == null) 
			{
				earthViewConfigurationList = ApogyEarthEnvironmentUIFactory.eINSTANCE.createEarthViewConfigurationList();
				ApogyCommonTransactionFacade.INSTANCE.basicAdd(toolsList,
						ApogyCoreInvocatorPackage.Literals.TOOLS_LIST__TOOLS_LIST_CONTAINERS,
						earthViewConfigurationList);
			}

		}
		return earthViewConfigurationList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyEarthEnvironmentUIPackage.EARTH_UI_FACADE___GET_ACTIVE_EARTH_VIEW_CONFIGURATION_LIST:
				return getActiveEarthViewConfigurationList();
		}
		return super.eInvoke(operationID, arguments);
	}

} //EarthUIFacadeImpl

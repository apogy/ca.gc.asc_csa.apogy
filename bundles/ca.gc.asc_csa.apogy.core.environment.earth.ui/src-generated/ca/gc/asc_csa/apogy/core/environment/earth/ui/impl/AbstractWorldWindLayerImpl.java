/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.progress.UIJob;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.Activator;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.utils.MultiEObjectsAdapter;
import ca.gc.asc_csa.apogy.core.impl.UpdatableImpl;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.layers.RenderableLayer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl#getName <em>Name</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl#isDisposed <em>Disposed</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl#isVisible <em>Visible</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl#isBlinking <em>Blinking</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl#getRenderableLayer <em>Renderable Layer</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl#getWorldWindow <em>World Window</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractWorldWindLayerImpl extends UpdatableImpl implements AbstractWorldWindLayer 
{
	private static UIJob blinkingJob = null;
	private static Map<AbstractWorldWindLayer, Boolean> blinkingLayers = new HashMap<AbstractWorldWindLayer, Boolean>();
	
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #isDisposed() <em>Disposed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisposed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DISPOSED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDisposed() <em>Disposed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisposed()
	 * @generated
	 * @ordered
	 */
	protected boolean disposed = DISPOSED_EDEFAULT;

	private MultiEObjectsAdapter layerAdapter= null;
	
	/**
	 * The default value of the '{@link #isVisible() <em>Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVisible()
	 * @generated
	 * @ordered
	 */
	protected static final boolean VISIBLE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isVisible() <em>Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVisible()
	 * @generated
	 * @ordered
	 */
	protected boolean visible = VISIBLE_EDEFAULT;

	/**
	 * The default value of the '{@link #isBlinking() <em>Blinking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBlinking()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BLINKING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBlinking() <em>Blinking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBlinking()
	 * @generated
	 * @ordered
	 */
	protected boolean blinking = BLINKING_EDEFAULT;

	/**
	 * The default value of the '{@link #getRenderableLayer() <em>Renderable Layer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRenderableLayer()
	 * @generated
	 * @ordered
	 */
	protected static final RenderableLayer RENDERABLE_LAYER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRenderableLayer() <em>Renderable Layer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRenderableLayer()
	 * @generated
	 * @ordered
	 */
	protected RenderableLayer renderableLayer = RENDERABLE_LAYER_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorldWindow() <em>World Window</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorldWindow()
	 * @generated
	 * @ordered
	 */
	protected static final WorldWindow WORLD_WINDOW_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWorldWindow() <em>World Window</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorldWindow()
	 * @generated
	 * @ordered
	 */
	protected WorldWindow worldWindow = WORLD_WINDOW_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	protected AbstractWorldWindLayerImpl() 
	{
		super();
		eAdapters().add(getLayerAdapter());
		
		// Forces the blinking Job to start.
		getBlinkingJob();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentUIPackage.Literals.ABSTRACT_WORLD_WIND_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDisposed() {
		return disposed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisposed(boolean newDisposed) {
		boolean oldDisposed = disposed;
		disposed = newDisposed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DISPOSED, oldDisposed, disposed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setVisible(boolean newVisible) 
	{
		setVisibleGen(newVisible);		
		updateRenderableLayer();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisibleGen(boolean newVisible) {
		boolean oldVisible = visible;
		visible = newVisible;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__VISIBLE, oldVisible, visible));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBlinking() {
		return blinking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setBlinking(boolean newBlinking) 
	{
		if(newBlinking && !blinking)
		{
			// Changed to blinking.
			blinkingLayers.put(this, this.isVisible());			
		}
		else if(!newBlinking && blinking)
		{
			// Changed to not blinking.		
			Boolean originalVisibility = blinkingLayers.get(this);
			if(originalVisibility != null)
			{
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyEarthEnvironmentUIPackage.Literals.ABSTRACT_WORLD_WIND_LAYER__VISIBLE, originalVisibility, true);
			}
			
			blinkingLayers.remove(this);
		}
		
		setBlinkingGen(newBlinking);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlinkingGen(boolean newBlinking) {
		boolean oldBlinking = blinking;
		blinking = newBlinking;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__BLINKING, oldBlinking, blinking));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public RenderableLayer getRenderableLayer() 
	{
		RenderableLayer tmp = getRenderableLayerGen();
		if(tmp == null)
		{
			tmp = new RenderableLayer();
			
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyEarthEnvironmentUIPackage.Literals.ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER, tmp);			
		}
		return tmp;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RenderableLayer getRenderableLayerGen() {
		return renderableLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRenderableLayer(RenderableLayer newRenderableLayer) {
		RenderableLayer oldRenderableLayer = renderableLayer;
		renderableLayer = newRenderableLayer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER, oldRenderableLayer, renderableLayer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorldWindow getWorldWindow() {
		return worldWindow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorldWindow(WorldWindow newWorldWindow) {
		WorldWindow oldWorldWindow = worldWindow;
		worldWindow = newWorldWindow;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW, oldWorldWindow, worldWindow));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void initialise() 
	{
		// Nothing to do here.
	}

	@Override
	public void update() throws Exception 
	{
		if(!isUpdating() && !isDisposed())
		{
			try
			{			
				updateRenderableLayer();				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void dispose() 
	{	
		// Wait for the layer to finish its update.
		while(isUpdating())
		{			
			try 
			{
				Logger.INSTANCE.log(Activator.ID, "Waiting for layer to complete update...", EventSeverity.WARNING);
				Thread.sleep(200);
			} 
			catch (Exception e) 
			{					
			}
		}
		
		// Set disposed flag to true.
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyEarthEnvironmentUIPackage.Literals.ABSTRACT_WORLD_WIND_LAYER__DISPOSED, true, true);

		// Removes the layer from the display.
		RenderableLayer layer = getRenderableLayer();
		layer.removeAllRenderables();		
		getRenderableLayer().firePropertyChange(AVKey.LAYER, null, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void selectionChanged(ISelection selection) 
	{
		// Nothing to do.
	}
	
	@Override
	public boolean getDefaultAutoUpdateEnabled() 
	{
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__NAME:
				return getName();
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION:
				return getDescription();
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DISPOSED:
				return isDisposed();
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__VISIBLE:
				return isVisible();
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__BLINKING:
				return isBlinking();
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER:
				return getRenderableLayer();
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW:
				return getWorldWindow();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__NAME:
				setName((String)newValue);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DISPOSED:
				setDisposed((Boolean)newValue);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__VISIBLE:
				setVisible((Boolean)newValue);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__BLINKING:
				setBlinking((Boolean)newValue);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER:
				setRenderableLayer((RenderableLayer)newValue);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW:
				setWorldWindow((WorldWindow)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DISPOSED:
				setDisposed(DISPOSED_EDEFAULT);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__VISIBLE:
				setVisible(VISIBLE_EDEFAULT);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__BLINKING:
				setBlinking(BLINKING_EDEFAULT);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER:
				setRenderableLayer(RENDERABLE_LAYER_EDEFAULT);
				return;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW:
				setWorldWindow(WORLD_WINDOW_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DISPOSED:
				return disposed != DISPOSED_EDEFAULT;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__VISIBLE:
				return visible != VISIBLE_EDEFAULT;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__BLINKING:
				return blinking != BLINKING_EDEFAULT;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER:
				return RENDERABLE_LAYER_EDEFAULT == null ? renderableLayer != null : !RENDERABLE_LAYER_EDEFAULT.equals(renderableLayer);
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW:
				return WORLD_WINDOW_EDEFAULT == null ? worldWindow != null : !WORLD_WINDOW_EDEFAULT.equals(worldWindow);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Named.class) {
			switch (derivedFeatureID) {
				case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__NAME: return ApogyCommonEMFPackage.NAMED__NAME;
				default: return -1;
			}
		}
		if (baseClass == Described.class) {
			switch (derivedFeatureID) {
				case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION: return ApogyCommonEMFPackage.DESCRIBED__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Named.class) {
			switch (baseFeatureID) {
				case ApogyCommonEMFPackage.NAMED__NAME: return ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__NAME;
				default: return -1;
			}
		}
		if (baseClass == Described.class) {
			switch (baseFeatureID) {
				case ApogyCommonEMFPackage.DESCRIBED__DESCRIPTION: return ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___INITIALISE:
				initialise();
				return null;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___DISPOSE:
				dispose();
				return null;
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION:
				selectionChanged((ISelection)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", disposed: ");
		result.append(disposed);
		result.append(", visible: ");
		result.append(visible);
		result.append(", blinking: ");
		result.append(blinking);
		result.append(", renderableLayer: ");
		result.append(renderableLayer);
		result.append(", worldWindow: ");
		result.append(worldWindow);
		result.append(')');
		return result.toString();
	}

	/**
	 * Method that does the update of the renderable layer in World Wind.
	 */
	protected abstract void updateRenderableLayer();
	
	private MultiEObjectsAdapter getLayerAdapter() 
	{
		if(layerAdapter == null)
		{
			layerAdapter = new MultiEObjectsAdapter()
			{				
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof AbstractWorldWindLayer)
					{
						int featureId = msg.getFeatureID(AbstractWorldWindLayer.class);
						switch (featureId) 
						{
							case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER__VISIBLE:							
								if(isAutoUpdateEnabled()) updateRenderableLayer();								
							break;

							default:
							break;
						}
					}					
				}				
			};
		}
		return layerAdapter;
	}

	private static UIJob getBlinkingJob() 
	{
		if(blinkingJob == null)
		{
			blinkingJob = new UIJob("World Wind Layer Blinking") 
			{							
				private boolean visible = true;
				
				@Override
				public IStatus runInUIThread(IProgressMonitor progressMonitor) 
				{	
					List<AbstractWorldWindLayer> layers = new ArrayList<AbstractWorldWindLayer>(blinkingLayers.keySet());
					
					for(AbstractWorldWindLayer layer : layers)
					{
						if(!layer.isDisposed())
						{
							ApogyCommonTransactionFacade.INSTANCE.basicSet(layer, ApogyEarthEnvironmentUIPackage.Literals.ABSTRACT_WORLD_WIND_LAYER__VISIBLE, visible, true);
						}
					}
					
					// Toggle visibility.
					visible = !visible;
	
					if(!progressMonitor.isCanceled()) this.schedule(1000);
					return Status.OK_STATUS;
				}
			};
			
			blinkingJob.setPriority(UIJob.LONG);
			blinkingJob.schedule();
		}
		return blinkingJob;
	}	
} //AbstractWorldWindLayerImpl

/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Canadian Space Agency (CSA) - Initial API and implementation
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
 *           
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Color3f;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.utils.WorldWindUtils;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.airspaces.Airspace;
import gov.nasa.worldwind.render.airspaces.AirspaceAttributes;
import gov.nasa.worldwind.render.airspaces.BasicAirspaceAttributes;
import gov.nasa.worldwind.render.airspaces.Polygon;
import gov.nasa.worldwind.util.WWMath;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Airspace World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AirspaceWorldWindLayerImpl#getLowerAltitude <em>Lower Altitude</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AirspaceWorldWindLayerImpl#getUpperAltitude <em>Upper Altitude</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AirspaceWorldWindLayerImpl extends SurfacePolygonWorldWindLayerImpl implements AirspaceWorldWindLayer 
{	
	protected Airspace airspace;
	
	/**
	 * The default value of the '{@link #getLowerAltitude() <em>Lower Altitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerAltitude()
	 * @generated
	 * @ordered
	 */
	protected static final double LOWER_ALTITUDE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getLowerAltitude() <em>Lower Altitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerAltitude()
	 * @generated
	 * @ordered
	 */
	protected double lowerAltitude = LOWER_ALTITUDE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpperAltitude() <em>Upper Altitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperAltitude()
	 * @generated
	 * @ordered
	 */
	protected static final double UPPER_ALTITUDE_EDEFAULT = 10000.0;

	/**
	 * The cached value of the '{@link #getUpperAltitude() <em>Upper Altitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperAltitude()
	 * @generated
	 * @ordered
	 */
	protected double upperAltitude = UPPER_ALTITUDE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AirspaceWorldWindLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLowerAltitude() {
		return lowerAltitude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setLowerAltitude(double newLowerAltitude) 
	{
		setLowerAltitudeGen(newLowerAltitude);
		
		updateRenderableLayer();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLowerAltitudeGen(double newLowerAltitude) {
		double oldLowerAltitude = lowerAltitude;
		lowerAltitude = newLowerAltitude;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE, oldLowerAltitude, lowerAltitude));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getUpperAltitude() {
		return upperAltitude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setUpperAltitude(double newUpperAltitude) 
	{
		setUpperAltitudeGen(newUpperAltitude);
		
		updateRenderableLayer();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpperAltitudeGen(double newUpperAltitude) {
		double oldUpperAltitude = upperAltitude;
		upperAltitude = newUpperAltitude;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE, oldUpperAltitude, upperAltitude));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setUrl(String newUrl) {
		setUrlGen(newUrl);
		loadFile();
					
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setColor(Color3f newColor) 
	{
		setColorGen(newColor);
		updateRenderableLayer();
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setOpacity(double newOpacity) 
	{
		if(newOpacity > 1)
		{
			setOpacityGen(1);
		}
		else if(newOpacity < 0)
		{
			setOpacityGen(0);
		}
		else
		{
			setOpacityGen(newOpacity);						
		}
		
		updateRenderableLayer();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public boolean isCoordinatesInside(GeographicCoordinates coordinates) 
	{
		if(airspace != null)
		{
			if(coordinates.getElevation() >= getLowerAltitude() && 
			   coordinates.getElevation() <= getUpperAltitude())
			{
				Position position = WorldWindUtils.convertToPosition(coordinates);
				Polygon polygon = (Polygon) airspace;
				return WWMath.isLocationInside(position, polygon.getLocations());	
			}			
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE:
				return getLowerAltitude();
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE:
				return getUpperAltitude();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE:
				setLowerAltitude((Double)newValue);
				return;
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE:
				setUpperAltitude((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE:
				setLowerAltitude(LOWER_ALTITUDE_EDEFAULT);
				return;
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE:
				setUpperAltitude(UPPER_ALTITUDE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE:
				return lowerAltitude != LOWER_ALTITUDE_EDEFAULT;
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE:
				return upperAltitude != UPPER_ALTITUDE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (lowerAltitude: ");
		result.append(lowerAltitude);
		result.append(", upperAltitude: ");
		result.append(upperAltitude);
		result.append(')');
		return result.toString();
	}

	@Override
	public void initialise() 
	{
		loadFile();
		
		super.initialise();
	}
	
	@Override
	protected void updateRenderableLayer() 
	{		
		if(!isUpdating() && !isDisposed())
		{
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, true);

			RenderableLayer layer = getRenderableLayer();
			layer.removeAllRenderables();	
			
			if(isVisible())
			{
				addRenderable(layer);							
			}
			getRenderableLayer().firePropertyChange(AVKey.LAYER, null, this);
			
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, false);
		}		
	}
	
	protected void addRenderable(RenderableLayer layer)
	{
		try
		{
			if(!getGeographicCoordinatesList().isEmpty())
			{
				airspace = createAirspace(getWorldWindow());
				layer.addRenderable(airspace);
			}		
			else
			{
				airspace = null;
			}
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
	}
	
    protected Airspace createAirspace(WorldWindow wwd)
    {
        Polygon poly = new Polygon();
        
        poly.setAttributes(getDefaultAirspaceAttributes());
        poly.setValue(AVKey.DISPLAY_NAME, getName());
        poly.setAltitudes(0.0, 0.0);
        poly.setTerrainConforming(true, false);
        this.initializePolygon(wwd, poly);

        return poly;
    }
    
    protected void initializePolygon(WorldWindow wwd, Polygon polygon)
    {
    	List<Position> positions = new ArrayList<Position>();
    	for(GeographicCoordinates coords :getGeographicCoordinatesList())
    	{
    		Position position = WorldWindUtils.convertToPosition(coords);
    		positions.add(position);
    	}
    	
    	if(getLowerAltitude() <= 0)
    	{
    		polygon.setAltitudes(0, getUpperAltitude());
        	polygon.setTerrainConforming(true, false);
    	}
    	else
    	{
    		polygon.setAltitudes(getLowerAltitude(), getUpperAltitude());
        	polygon.setTerrainConforming(false, false);
    	}
    	
    	polygon.setLocations(positions);
    }

    protected AirspaceAttributes getDefaultAirspaceAttributes()
    {
 	   	AirspaceAttributes attributes = new BasicAirspaceAttributes();
        
        Color color = WorldWindUtils.convertFrom(getColor());
        
        attributes.setMaterial(new Material(color, Color.LIGHT_GRAY, Color.DARK_GRAY, Color.BLACK, 0.0f));                
        attributes.setOutlineMaterial(new Material(color, color, color, color, 0.0f));
        
        attributes.setDrawOutline(true);
        attributes.setOpacity(getOpacity());
        attributes.setOutlineOpacity(0.95);
        attributes.setOutlineWidth(2);
        return attributes;
    }
} //AirspaceWorldWindLayerImpl

/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyEarthEnvironmentUI' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' modelName='ApogyCoreEnvironmentUI' complianceLevel='6.0' suppressGenModelAnnotations='false' dynamicTemplates='true' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' modelDirectory='/ca.gc.asc_csa.apogy.core.environment.earth.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.core.environment.earth.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.core.environment.earth'"
 * @generated
 */
public interface ApogyEarthEnvironmentUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.core.environment.earth.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyEarthEnvironmentUIPackage eINSTANCE = ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthUIFacadeImpl <em>Earth UI Facade</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthUIFacadeImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthUIFacade()
	 * @generated
	 */
	int EARTH_UI_FACADE = 0;

	/**
	 * The number of structural features of the '<em>Earth UI Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_UI_FACADE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Active Earth View Configuration List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_UI_FACADE___GET_ACTIVE_EARTH_VIEW_CONFIGURATION_LIST = 0;

	/**
	 * The number of operations of the '<em>Earth UI Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_UI_FACADE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationListImpl <em>Earth View Configuration List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationListImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthViewConfigurationList()
	 * @generated
	 */
	int EARTH_VIEW_CONFIGURATION_LIST = 1;

	/**
	 * The feature id for the '<em><b>Tools List</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION_LIST__TOOLS_LIST = ApogyCoreInvocatorPackage.ABSTRACT_TOOLS_LIST_CONTAINER__TOOLS_LIST;

	/**
	 * The feature id for the '<em><b>Earth View Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION_LIST__EARTH_VIEW_CONFIGURATIONS = ApogyCoreInvocatorPackage.ABSTRACT_TOOLS_LIST_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Earth View Configuration List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION_LIST_FEATURE_COUNT = ApogyCoreInvocatorPackage.ABSTRACT_TOOLS_LIST_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Earth View Configuration List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION_LIST_OPERATION_COUNT = ApogyCoreInvocatorPackage.ABSTRACT_TOOLS_LIST_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationImpl <em>Earth View Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthViewConfiguration()
	 * @generated
	 */
	int EARTH_VIEW_CONFIGURATION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION__NAME = ApogyCommonEMFPackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION__DESCRIPTION = ApogyCommonEMFPackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Layers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION__LAYERS = ApogyCommonEMFPackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Earth View Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION_FEATURE_COUNT = ApogyCommonEMFPackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION___SELECTION_CHANGED__ISELECTION = ApogyCommonEMFPackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Earth View Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION_OPERATION_COUNT = ApogyCommonEMFPackage.NAMED_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationReferenceImpl <em>Earth View Configuration Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationReferenceImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthViewConfigurationReference()
	 * @generated
	 */
	int EARTH_VIEW_CONFIGURATION_REFERENCE = 3;

	/**
	 * The feature id for the '<em><b>Earth View Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION_REFERENCE__EARTH_VIEW_CONFIGURATION = 0;

	/**
	 * The number of structural features of the '<em>Earth View Configuration Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION_REFERENCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Earth View Configuration Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_VIEW_CONFIGURATION_REFERENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl <em>Abstract World Wind Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getAbstractWorldWindLayer()
	 * @generated
	 */
	int ABSTRACT_WORLD_WIND_LAYER = 4;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER__UPDATING = ApogyCorePackage.UPDATABLE__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ApogyCorePackage.UPDATABLE__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER__NAME = ApogyCorePackage.UPDATABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION = ApogyCorePackage.UPDATABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER__DISPOSED = ApogyCorePackage.UPDATABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER__VISIBLE = ApogyCorePackage.UPDATABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER__BLINKING = ApogyCorePackage.UPDATABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER = ApogyCorePackage.UPDATABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW = ApogyCorePackage.UPDATABLE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Abstract World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT = ApogyCorePackage.UPDATABLE_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ApogyCorePackage.UPDATABLE___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER___UPDATE = ApogyCorePackage.UPDATABLE___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER___INITIALISE = ApogyCorePackage.UPDATABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER___DISPOSE = ApogyCorePackage.UPDATABLE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ApogyCorePackage.UPDATABLE_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Abstract World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER_OPERATION_COUNT = ApogyCorePackage.UPDATABLE_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.GeographicCoordinatesWorldWindLayerImpl <em>Geographic Coordinates World Wind Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.GeographicCoordinatesWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getGeographicCoordinatesWorldWindLayer()
	 * @generated
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER = 5;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__UPDATING = ABSTRACT_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ABSTRACT_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__NAME = ABSTRACT_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__DESCRIPTION = ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__DISPOSED = ABSTRACT_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__VISIBLE = ABSTRACT_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__BLINKING = ABSTRACT_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__RENDERABLE_LAYER = ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__WORLD_WINDOW = ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Geographic Coordinates List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lock Selection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__LOCK_SELECTION = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Display Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__DISPLAY_LOCATION = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Displayed Radius</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__DISPLAYED_RADIUS = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Geographic Coordinates World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER_FEATURE_COUNT = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ABSTRACT_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER___UPDATE = ABSTRACT_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER___INITIALISE = ABSTRACT_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER___DISPOSE = ABSTRACT_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The number of operations of the '<em>Geographic Coordinates World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER_OPERATION_COUNT = ABSTRACT_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerImpl <em>Earth Surface Location World Wind Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthSurfaceLocationWorldWindLayer()
	 * @generated
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER = 6;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__UPDATING = ABSTRACT_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ABSTRACT_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__NAME = ABSTRACT_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DESCRIPTION = ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DISPOSED = ABSTRACT_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__VISIBLE = ABSTRACT_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__BLINKING = ABSTRACT_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__RENDERABLE_LAYER = ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__WORLD_WINDOW = ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Earth Surface Location</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target Radius</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__TARGET_RADIUS = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Display Balloon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DISPLAY_BALLOON = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Display Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DISPLAY_LOCATION = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__COLOR = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Opacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__OPACITY = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Earth Surface Location World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_FEATURE_COUNT = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ABSTRACT_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER___UPDATE = ABSTRACT_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER___INITIALISE = ABSTRACT_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER___DISPOSE = ABSTRACT_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The number of operations of the '<em>Earth Surface Location World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_OPERATION_COUNT = ABSTRACT_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.GeographicCoordinatesPathWorldWindLayerImpl <em>Geographic Coordinates Path World Wind Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.GeographicCoordinatesPathWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getGeographicCoordinatesPathWorldWindLayer()
	 * @generated
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER = 7;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__UPDATING = ABSTRACT_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ABSTRACT_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__NAME = ABSTRACT_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__DESCRIPTION = ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__DISPOSED = ABSTRACT_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__VISIBLE = ABSTRACT_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__BLINKING = ABSTRACT_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__RENDERABLE_LAYER = ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__WORLD_WINDOW = ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Geographic Coordinates List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Geographic Coordinates Path World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER_FEATURE_COUNT = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ABSTRACT_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER___UPDATE = ABSTRACT_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER___INITIALISE = ABSTRACT_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER___DISPOSE = ABSTRACT_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The operation id for the '<em>Clear Path</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER___CLEAR_PATH = ABSTRACT_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Geographic Coordinates Path World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER_OPERATION_COUNT = ABSTRACT_WORLD_WIND_LAYER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerImpl <em>Surface Polygon World Wind Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getSurfacePolygonWorldWindLayer()
	 * @generated
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER = 8;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__UPDATING = ABSTRACT_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ABSTRACT_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__NAME = ABSTRACT_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__DESCRIPTION = ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__DISPOSED = ABSTRACT_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__VISIBLE = ABSTRACT_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__BLINKING = ABSTRACT_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__RENDERABLE_LAYER = ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__WORLD_WINDOW = ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Geographic Coordinates List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__URL = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__COLOR = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Opacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER__OPACITY = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Surface Polygon World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_FEATURE_COUNT = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ABSTRACT_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER___UPDATE = ABSTRACT_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER___INITIALISE = ABSTRACT_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER___DISPOSE = ABSTRACT_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The operation id for the '<em>Is Coordinates Inside</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER___IS_COORDINATES_INSIDE__GEOGRAPHICCOORDINATES = ABSTRACT_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Surface Polygon World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_OPERATION_COUNT = ABSTRACT_WORLD_WIND_LAYER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AirspaceWorldWindLayerImpl <em>Airspace World Wind Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AirspaceWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getAirspaceWorldWindLayer()
	 * @generated
	 */
	int AIRSPACE_WORLD_WIND_LAYER = 9;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__UPDATING = SURFACE_POLYGON_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = SURFACE_POLYGON_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__NAME = SURFACE_POLYGON_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__DESCRIPTION = SURFACE_POLYGON_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__DISPOSED = SURFACE_POLYGON_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__VISIBLE = SURFACE_POLYGON_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__BLINKING = SURFACE_POLYGON_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__RENDERABLE_LAYER = SURFACE_POLYGON_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__WORLD_WINDOW = SURFACE_POLYGON_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Geographic Coordinates List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST = SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__URL = SURFACE_POLYGON_WORLD_WIND_LAYER__URL;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__COLOR = SURFACE_POLYGON_WORLD_WIND_LAYER__COLOR;

	/**
	 * The feature id for the '<em><b>Opacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__OPACITY = SURFACE_POLYGON_WORLD_WIND_LAYER__OPACITY;

	/**
	 * The feature id for the '<em><b>Lower Altitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE = SURFACE_POLYGON_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Upper Altitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE = SURFACE_POLYGON_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Airspace World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER_FEATURE_COUNT = SURFACE_POLYGON_WORLD_WIND_LAYER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = SURFACE_POLYGON_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER___UPDATE = SURFACE_POLYGON_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER___INITIALISE = SURFACE_POLYGON_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER___DISPOSE = SURFACE_POLYGON_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = SURFACE_POLYGON_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The operation id for the '<em>Is Coordinates Inside</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER___IS_COORDINATES_INSIDE__GEOGRAPHICCOORDINATES = SURFACE_POLYGON_WORLD_WIND_LAYER___IS_COORDINATES_INSIDE__GEOGRAPHICCOORDINATES;

	/**
	 * The number of operations of the '<em>Airspace World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER_OPERATION_COUNT = SURFACE_POLYGON_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.KMLWorldWindLayerImpl <em>KML World Wind Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.KMLWorldWindLayerImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getKMLWorldWindLayer()
	 * @generated
	 */
	int KML_WORLD_WIND_LAYER = 10;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER__UPDATING = ABSTRACT_WORLD_WIND_LAYER__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED = ABSTRACT_WORLD_WIND_LAYER__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER__NAME = ABSTRACT_WORLD_WIND_LAYER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER__DESCRIPTION = ABSTRACT_WORLD_WIND_LAYER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER__DISPOSED = ABSTRACT_WORLD_WIND_LAYER__DISPOSED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER__VISIBLE = ABSTRACT_WORLD_WIND_LAYER__VISIBLE;

	/**
	 * The feature id for the '<em><b>Blinking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER__BLINKING = ABSTRACT_WORLD_WIND_LAYER__BLINKING;

	/**
	 * The feature id for the '<em><b>Renderable Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER__RENDERABLE_LAYER = ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER;

	/**
	 * The feature id for the '<em><b>World Window</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER__WORLD_WINDOW = ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER__URL = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>KML World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER_FEATURE_COUNT = ABSTRACT_WORLD_WIND_LAYER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED = ABSTRACT_WORLD_WIND_LAYER___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER___UPDATE = ABSTRACT_WORLD_WIND_LAYER___UPDATE;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER___INITIALISE = ABSTRACT_WORLD_WIND_LAYER___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER___DISPOSE = ABSTRACT_WORLD_WIND_LAYER___DISPOSE;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION;

	/**
	 * The number of operations of the '<em>KML World Wind Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER_OPERATION_COUNT = ABSTRACT_WORLD_WIND_LAYER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerWizardPagesProviderImpl <em>Abstract World Wind Layer Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getAbstractWorldWindLayerWizardPagesProvider()
	 * @generated
	 */
	int ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = 11;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Abstract World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Abstract World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerWizardPagesProviderImpl <em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthSurfaceLocationWorldWindLayerWizardPagesProvider()
	 * @generated
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = 12;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerWizardPagesProviderImpl <em>Surface Polygon World Wind Layer Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getSurfacePolygonWorldWindLayerWizardPagesProvider()
	 * @generated
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = 13;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Surface Polygon World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Surface Polygon World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AirspaceWorldWindLayerWizardPagesProviderImpl <em>Airspace World Wind Layer Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AirspaceWorldWindLayerWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getAirspaceWorldWindLayerWizardPagesProvider()
	 * @generated
	 */
	int AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = 14;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES = SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT = SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Airspace World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Airspace World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.KMLWorldWindLayerWizardPagesProviderImpl <em>KML World Wind Layer Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.KMLWorldWindLayerWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getKMLWorldWindLayerWizardPagesProvider()
	 * @generated
	 */
	int KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = 15;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>KML World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>KML World Wind Layer Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '<em>Color3f</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see javax.vecmath.Color3f
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getColor3f()
	 * @generated
	 */
	int COLOR3F = 16;

	/**
	 * The meta object id for the '<em>Renderable Layer</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gov.nasa.worldwind.layers.RenderableLayer
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getRenderableLayer()
	 * @generated
	 */
	int RENDERABLE_LAYER = 17;

	/**
	 * The meta object id for the '<em>ISelection</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jface.viewers.ISelection
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getISelection()
	 * @generated
	 */
	int ISELECTION = 18;


	/**
	 * The meta object id for the '<em>World Window</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gov.nasa.worldwind.WorldWindow
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getWorldWindow()
	 * @generated
	 */
	int WORLD_WINDOW = 19;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade <em>Earth UI Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth UI Facade</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade
	 * @generated
	 */
	EClass getEarthUIFacade();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade#getActiveEarthViewConfigurationList() <em>Get Active Earth View Configuration List</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Active Earth View Configuration List</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade#getActiveEarthViewConfigurationList()
	 * @generated
	 */
	EOperation getEarthUIFacade__GetActiveEarthViewConfigurationList();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationList <em>Earth View Configuration List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth View Configuration List</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationList
	 * @generated
	 */
	EClass getEarthViewConfigurationList();

	/**
	 * Returns the meta object for the containment reference list '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationList#getEarthViewConfigurations <em>Earth View Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Earth View Configurations</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationList#getEarthViewConfigurations()
	 * @see #getEarthViewConfigurationList()
	 * @generated
	 */
	EReference getEarthViewConfigurationList_EarthViewConfigurations();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration <em>Earth View Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth View Configuration</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration
	 * @generated
	 */
	EClass getEarthViewConfiguration();

	/**
	 * Returns the meta object for the containment reference list '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration#getLayers <em>Layers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Layers</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration#getLayers()
	 * @see #getEarthViewConfiguration()
	 * @generated
	 */
	EReference getEarthViewConfiguration_Layers();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration#selectionChanged(org.eclipse.jface.viewers.ISelection) <em>Selection Changed</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Selection Changed</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration#selectionChanged(org.eclipse.jface.viewers.ISelection)
	 * @generated
	 */
	EOperation getEarthViewConfiguration__SelectionChanged__ISelection();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationReference <em>Earth View Configuration Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth View Configuration Reference</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationReference
	 * @generated
	 */
	EClass getEarthViewConfigurationReference();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationReference#getEarthViewConfiguration <em>Earth View Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Earth View Configuration</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationReference#getEarthViewConfiguration()
	 * @see #getEarthViewConfigurationReference()
	 * @generated
	 */
	EReference getEarthViewConfigurationReference_EarthViewConfiguration();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer <em>Abstract World Wind Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer
	 * @generated
	 */
	EClass getAbstractWorldWindLayer();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isDisposed <em>Disposed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Disposed</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isDisposed()
	 * @see #getAbstractWorldWindLayer()
	 * @generated
	 */
	EAttribute getAbstractWorldWindLayer_Disposed();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isVisible <em>Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visible</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isVisible()
	 * @see #getAbstractWorldWindLayer()
	 * @generated
	 */
	EAttribute getAbstractWorldWindLayer_Visible();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isBlinking <em>Blinking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Blinking</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isBlinking()
	 * @see #getAbstractWorldWindLayer()
	 * @generated
	 */
	EAttribute getAbstractWorldWindLayer_Blinking();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#getRenderableLayer <em>Renderable Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Renderable Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#getRenderableLayer()
	 * @see #getAbstractWorldWindLayer()
	 * @generated
	 */
	EAttribute getAbstractWorldWindLayer_RenderableLayer();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#getWorldWindow <em>World Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>World Window</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#getWorldWindow()
	 * @see #getAbstractWorldWindLayer()
	 * @generated
	 */
	EAttribute getAbstractWorldWindLayer_WorldWindow();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#initialise() <em>Initialise</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initialise</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#initialise()
	 * @generated
	 */
	EOperation getAbstractWorldWindLayer__Initialise();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#dispose() <em>Dispose</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Dispose</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#dispose()
	 * @generated
	 */
	EOperation getAbstractWorldWindLayer__Dispose();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#selectionChanged(org.eclipse.jface.viewers.ISelection) <em>Selection Changed</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Selection Changed</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#selectionChanged(org.eclipse.jface.viewers.ISelection)
	 * @generated
	 */
	EOperation getAbstractWorldWindLayer__SelectionChanged__ISelection();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer <em>Geographic Coordinates World Wind Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Geographic Coordinates World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer
	 * @generated
	 */
	EClass getGeographicCoordinatesWorldWindLayer();

	/**
	 * Returns the meta object for the containment reference list '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer#getGeographicCoordinatesList <em>Geographic Coordinates List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Geographic Coordinates List</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer#getGeographicCoordinatesList()
	 * @see #getGeographicCoordinatesWorldWindLayer()
	 * @generated
	 */
	EReference getGeographicCoordinatesWorldWindLayer_GeographicCoordinatesList();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer#isLockSelection <em>Lock Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lock Selection</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer#isLockSelection()
	 * @see #getGeographicCoordinatesWorldWindLayer()
	 * @generated
	 */
	EAttribute getGeographicCoordinatesWorldWindLayer_LockSelection();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer#isDisplayLocation <em>Display Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Location</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer#isDisplayLocation()
	 * @see #getGeographicCoordinatesWorldWindLayer()
	 * @generated
	 */
	EAttribute getGeographicCoordinatesWorldWindLayer_DisplayLocation();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer#getDisplayedRadius <em>Displayed Radius</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Displayed Radius</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer#getDisplayedRadius()
	 * @see #getGeographicCoordinatesWorldWindLayer()
	 * @generated
	 */
	EAttribute getGeographicCoordinatesWorldWindLayer_DisplayedRadius();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer <em>Earth Surface Location World Wind Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth Surface Location World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer
	 * @generated
	 */
	EClass getEarthSurfaceLocationWorldWindLayer();

	/**
	 * Returns the meta object for the containment reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#getEarthSurfaceLocation <em>Earth Surface Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Earth Surface Location</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#getEarthSurfaceLocation()
	 * @see #getEarthSurfaceLocationWorldWindLayer()
	 * @generated
	 */
	EReference getEarthSurfaceLocationWorldWindLayer_EarthSurfaceLocation();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#getTargetRadius <em>Target Radius</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Radius</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#getTargetRadius()
	 * @see #getEarthSurfaceLocationWorldWindLayer()
	 * @generated
	 */
	EAttribute getEarthSurfaceLocationWorldWindLayer_TargetRadius();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#isDisplayBalloon <em>Display Balloon</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Balloon</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#isDisplayBalloon()
	 * @see #getEarthSurfaceLocationWorldWindLayer()
	 * @generated
	 */
	EAttribute getEarthSurfaceLocationWorldWindLayer_DisplayBalloon();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#isDisplayLocation <em>Display Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Location</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#isDisplayLocation()
	 * @see #getEarthSurfaceLocationWorldWindLayer()
	 * @generated
	 */
	EAttribute getEarthSurfaceLocationWorldWindLayer_DisplayLocation();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#getColor()
	 * @see #getEarthSurfaceLocationWorldWindLayer()
	 * @generated
	 */
	EAttribute getEarthSurfaceLocationWorldWindLayer_Color();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#getOpacity <em>Opacity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opacity</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer#getOpacity()
	 * @see #getEarthSurfaceLocationWorldWindLayer()
	 * @generated
	 */
	EAttribute getEarthSurfaceLocationWorldWindLayer_Opacity();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesPathWorldWindLayer <em>Geographic Coordinates Path World Wind Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Geographic Coordinates Path World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesPathWorldWindLayer
	 * @generated
	 */
	EClass getGeographicCoordinatesPathWorldWindLayer();

	/**
	 * Returns the meta object for the containment reference list '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesPathWorldWindLayer#getGeographicCoordinatesList <em>Geographic Coordinates List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Geographic Coordinates List</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesPathWorldWindLayer#getGeographicCoordinatesList()
	 * @see #getGeographicCoordinatesPathWorldWindLayer()
	 * @generated
	 */
	EReference getGeographicCoordinatesPathWorldWindLayer_GeographicCoordinatesList();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesPathWorldWindLayer#clearPath() <em>Clear Path</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Path</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesPathWorldWindLayer#clearPath()
	 * @generated
	 */
	EOperation getGeographicCoordinatesPathWorldWindLayer__ClearPath();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer <em>Surface Polygon World Wind Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Surface Polygon World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer
	 * @generated
	 */
	EClass getSurfacePolygonWorldWindLayer();

	/**
	 * Returns the meta object for the containment reference list '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer#getGeographicCoordinatesList <em>Geographic Coordinates List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Geographic Coordinates List</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer#getGeographicCoordinatesList()
	 * @see #getSurfacePolygonWorldWindLayer()
	 * @generated
	 */
	EReference getSurfacePolygonWorldWindLayer_GeographicCoordinatesList();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer#getUrl()
	 * @see #getSurfacePolygonWorldWindLayer()
	 * @generated
	 */
	EAttribute getSurfacePolygonWorldWindLayer_Url();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer#getColor()
	 * @see #getSurfacePolygonWorldWindLayer()
	 * @generated
	 */
	EAttribute getSurfacePolygonWorldWindLayer_Color();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer#getOpacity <em>Opacity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opacity</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer#getOpacity()
	 * @see #getSurfacePolygonWorldWindLayer()
	 * @generated
	 */
	EAttribute getSurfacePolygonWorldWindLayer_Opacity();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer#isCoordinatesInside(ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates) <em>Is Coordinates Inside</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Coordinates Inside</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer#isCoordinatesInside(ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates)
	 * @generated
	 */
	EOperation getSurfacePolygonWorldWindLayer__IsCoordinatesInside__GeographicCoordinates();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer <em>Airspace World Wind Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Airspace World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer
	 * @generated
	 */
	EClass getAirspaceWorldWindLayer();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getLowerAltitude <em>Lower Altitude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower Altitude</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getLowerAltitude()
	 * @see #getAirspaceWorldWindLayer()
	 * @generated
	 */
	EAttribute getAirspaceWorldWindLayer_LowerAltitude();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getUpperAltitude <em>Upper Altitude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Altitude</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getUpperAltitude()
	 * @see #getAirspaceWorldWindLayer()
	 * @generated
	 */
	EAttribute getAirspaceWorldWindLayer_UpperAltitude();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayer <em>KML World Wind Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>KML World Wind Layer</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayer
	 * @generated
	 */
	EClass getKMLWorldWindLayer();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayer#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayer#getUrl()
	 * @see #getKMLWorldWindLayer()
	 * @generated
	 */
	EAttribute getKMLWorldWindLayer_Url();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayerWizardPagesProvider <em>Abstract World Wind Layer Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract World Wind Layer Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayerWizardPagesProvider
	 * @generated
	 */
	EClass getAbstractWorldWindLayerWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayerWizardPagesProvider <em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayerWizardPagesProvider
	 * @generated
	 */
	EClass getEarthSurfaceLocationWorldWindLayerWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayerWizardPagesProvider <em>Surface Polygon World Wind Layer Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Surface Polygon World Wind Layer Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayerWizardPagesProvider
	 * @generated
	 */
	EClass getSurfacePolygonWorldWindLayerWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayerWizardPagesProvider <em>Airspace World Wind Layer Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Airspace World Wind Layer Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayerWizardPagesProvider
	 * @generated
	 */
	EClass getAirspaceWorldWindLayerWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayerWizardPagesProvider <em>KML World Wind Layer Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>KML World Wind Layer Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayerWizardPagesProvider
	 * @generated
	 */
	EClass getKMLWorldWindLayerWizardPagesProvider();

	/**
	 * Returns the meta object for data type '{@link javax.vecmath.Color3f <em>Color3f</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Color3f</em>'.
	 * @see javax.vecmath.Color3f
	 * @model instanceClass="javax.vecmath.Color3f"
	 * @generated
	 */
	EDataType getColor3f();

	/**
	 * Returns the meta object for data type '{@link gov.nasa.worldwind.layers.RenderableLayer <em>Renderable Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Renderable Layer</em>'.
	 * @see gov.nasa.worldwind.layers.RenderableLayer
	 * @model instanceClass="gov.nasa.worldwind.layers.RenderableLayer"
	 * @generated
	 */
	EDataType getRenderableLayer();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.jface.viewers.ISelection <em>ISelection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ISelection</em>'.
	 * @see org.eclipse.jface.viewers.ISelection
	 * @model instanceClass="org.eclipse.jface.viewers.ISelection"
	 * @generated
	 */
	EDataType getISelection();

	/**
	 * Returns the meta object for data type '{@link gov.nasa.worldwind.WorldWindow <em>World Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>World Window</em>'.
	 * @see gov.nasa.worldwind.WorldWindow
	 * @model instanceClass="gov.nasa.worldwind.WorldWindow"
	 * @generated
	 */
	EDataType getWorldWindow();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyEarthEnvironmentUIFactory getApogyEarthEnvironmentUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthUIFacadeImpl <em>Earth UI Facade</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthUIFacadeImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthUIFacade()
		 * @generated
		 */
		EClass EARTH_UI_FACADE = eINSTANCE.getEarthUIFacade();

		/**
		 * The meta object literal for the '<em><b>Get Active Earth View Configuration List</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EARTH_UI_FACADE___GET_ACTIVE_EARTH_VIEW_CONFIGURATION_LIST = eINSTANCE.getEarthUIFacade__GetActiveEarthViewConfigurationList();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationListImpl <em>Earth View Configuration List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationListImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthViewConfigurationList()
		 * @generated
		 */
		EClass EARTH_VIEW_CONFIGURATION_LIST = eINSTANCE.getEarthViewConfigurationList();

		/**
		 * The meta object literal for the '<em><b>Earth View Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_VIEW_CONFIGURATION_LIST__EARTH_VIEW_CONFIGURATIONS = eINSTANCE.getEarthViewConfigurationList_EarthViewConfigurations();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationImpl <em>Earth View Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthViewConfiguration()
		 * @generated
		 */
		EClass EARTH_VIEW_CONFIGURATION = eINSTANCE.getEarthViewConfiguration();

		/**
		 * The meta object literal for the '<em><b>Layers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_VIEW_CONFIGURATION__LAYERS = eINSTANCE.getEarthViewConfiguration_Layers();

		/**
		 * The meta object literal for the '<em><b>Selection Changed</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EARTH_VIEW_CONFIGURATION___SELECTION_CHANGED__ISELECTION = eINSTANCE.getEarthViewConfiguration__SelectionChanged__ISelection();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationReferenceImpl <em>Earth View Configuration Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationReferenceImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthViewConfigurationReference()
		 * @generated
		 */
		EClass EARTH_VIEW_CONFIGURATION_REFERENCE = eINSTANCE.getEarthViewConfigurationReference();

		/**
		 * The meta object literal for the '<em><b>Earth View Configuration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_VIEW_CONFIGURATION_REFERENCE__EARTH_VIEW_CONFIGURATION = eINSTANCE.getEarthViewConfigurationReference_EarthViewConfiguration();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl <em>Abstract World Wind Layer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getAbstractWorldWindLayer()
		 * @generated
		 */
		EClass ABSTRACT_WORLD_WIND_LAYER = eINSTANCE.getAbstractWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Disposed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_WORLD_WIND_LAYER__DISPOSED = eINSTANCE.getAbstractWorldWindLayer_Disposed();

		/**
		 * The meta object literal for the '<em><b>Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_WORLD_WIND_LAYER__VISIBLE = eINSTANCE.getAbstractWorldWindLayer_Visible();

		/**
		 * The meta object literal for the '<em><b>Blinking</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_WORLD_WIND_LAYER__BLINKING = eINSTANCE.getAbstractWorldWindLayer_Blinking();

		/**
		 * The meta object literal for the '<em><b>Renderable Layer</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_WORLD_WIND_LAYER__RENDERABLE_LAYER = eINSTANCE.getAbstractWorldWindLayer_RenderableLayer();

		/**
		 * The meta object literal for the '<em><b>World Window</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_WORLD_WIND_LAYER__WORLD_WINDOW = eINSTANCE.getAbstractWorldWindLayer_WorldWindow();

		/**
		 * The meta object literal for the '<em><b>Initialise</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ABSTRACT_WORLD_WIND_LAYER___INITIALISE = eINSTANCE.getAbstractWorldWindLayer__Initialise();

		/**
		 * The meta object literal for the '<em><b>Dispose</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ABSTRACT_WORLD_WIND_LAYER___DISPOSE = eINSTANCE.getAbstractWorldWindLayer__Dispose();

		/**
		 * The meta object literal for the '<em><b>Selection Changed</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ABSTRACT_WORLD_WIND_LAYER___SELECTION_CHANGED__ISELECTION = eINSTANCE.getAbstractWorldWindLayer__SelectionChanged__ISelection();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.GeographicCoordinatesWorldWindLayerImpl <em>Geographic Coordinates World Wind Layer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.GeographicCoordinatesWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getGeographicCoordinatesWorldWindLayer()
		 * @generated
		 */
		EClass GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER = eINSTANCE.getGeographicCoordinatesWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Geographic Coordinates List</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST = eINSTANCE.getGeographicCoordinatesWorldWindLayer_GeographicCoordinatesList();

		/**
		 * The meta object literal for the '<em><b>Lock Selection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__LOCK_SELECTION = eINSTANCE.getGeographicCoordinatesWorldWindLayer_LockSelection();

		/**
		 * The meta object literal for the '<em><b>Display Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__DISPLAY_LOCATION = eINSTANCE.getGeographicCoordinatesWorldWindLayer_DisplayLocation();

		/**
		 * The meta object literal for the '<em><b>Displayed Radius</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER__DISPLAYED_RADIUS = eINSTANCE.getGeographicCoordinatesWorldWindLayer_DisplayedRadius();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerImpl <em>Earth Surface Location World Wind Layer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthSurfaceLocationWorldWindLayer()
		 * @generated
		 */
		EClass EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER = eINSTANCE.getEarthSurfaceLocationWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Earth Surface Location</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION = eINSTANCE.getEarthSurfaceLocationWorldWindLayer_EarthSurfaceLocation();

		/**
		 * The meta object literal for the '<em><b>Target Radius</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__TARGET_RADIUS = eINSTANCE.getEarthSurfaceLocationWorldWindLayer_TargetRadius();

		/**
		 * The meta object literal for the '<em><b>Display Balloon</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DISPLAY_BALLOON = eINSTANCE.getEarthSurfaceLocationWorldWindLayer_DisplayBalloon();

		/**
		 * The meta object literal for the '<em><b>Display Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__DISPLAY_LOCATION = eINSTANCE.getEarthSurfaceLocationWorldWindLayer_DisplayLocation();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__COLOR = eINSTANCE.getEarthSurfaceLocationWorldWindLayer_Color();

		/**
		 * The meta object literal for the '<em><b>Opacity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__OPACITY = eINSTANCE.getEarthSurfaceLocationWorldWindLayer_Opacity();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.GeographicCoordinatesPathWorldWindLayerImpl <em>Geographic Coordinates Path World Wind Layer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.GeographicCoordinatesPathWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getGeographicCoordinatesPathWorldWindLayer()
		 * @generated
		 */
		EClass GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER = eINSTANCE.getGeographicCoordinatesPathWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Geographic Coordinates List</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST = eINSTANCE.getGeographicCoordinatesPathWorldWindLayer_GeographicCoordinatesList();

		/**
		 * The meta object literal for the '<em><b>Clear Path</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER___CLEAR_PATH = eINSTANCE.getGeographicCoordinatesPathWorldWindLayer__ClearPath();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerImpl <em>Surface Polygon World Wind Layer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getSurfacePolygonWorldWindLayer()
		 * @generated
		 */
		EClass SURFACE_POLYGON_WORLD_WIND_LAYER = eINSTANCE.getSurfacePolygonWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Geographic Coordinates List</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST = eINSTANCE.getSurfacePolygonWorldWindLayer_GeographicCoordinatesList();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SURFACE_POLYGON_WORLD_WIND_LAYER__URL = eINSTANCE.getSurfacePolygonWorldWindLayer_Url();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SURFACE_POLYGON_WORLD_WIND_LAYER__COLOR = eINSTANCE.getSurfacePolygonWorldWindLayer_Color();

		/**
		 * The meta object literal for the '<em><b>Opacity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SURFACE_POLYGON_WORLD_WIND_LAYER__OPACITY = eINSTANCE.getSurfacePolygonWorldWindLayer_Opacity();

		/**
		 * The meta object literal for the '<em><b>Is Coordinates Inside</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SURFACE_POLYGON_WORLD_WIND_LAYER___IS_COORDINATES_INSIDE__GEOGRAPHICCOORDINATES = eINSTANCE.getSurfacePolygonWorldWindLayer__IsCoordinatesInside__GeographicCoordinates();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AirspaceWorldWindLayerImpl <em>Airspace World Wind Layer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AirspaceWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getAirspaceWorldWindLayer()
		 * @generated
		 */
		EClass AIRSPACE_WORLD_WIND_LAYER = eINSTANCE.getAirspaceWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Lower Altitude</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE = eINSTANCE.getAirspaceWorldWindLayer_LowerAltitude();

		/**
		 * The meta object literal for the '<em><b>Upper Altitude</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE = eINSTANCE.getAirspaceWorldWindLayer_UpperAltitude();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.KMLWorldWindLayerImpl <em>KML World Wind Layer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.KMLWorldWindLayerImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getKMLWorldWindLayer()
		 * @generated
		 */
		EClass KML_WORLD_WIND_LAYER = eINSTANCE.getKMLWorldWindLayer();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KML_WORLD_WIND_LAYER__URL = eINSTANCE.getKMLWorldWindLayer_Url();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerWizardPagesProviderImpl <em>Abstract World Wind Layer Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AbstractWorldWindLayerWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getAbstractWorldWindLayerWizardPagesProvider()
		 * @generated
		 */
		EClass ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = eINSTANCE.getAbstractWorldWindLayerWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerWizardPagesProviderImpl <em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getEarthSurfaceLocationWorldWindLayerWizardPagesProvider()
		 * @generated
		 */
		EClass EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = eINSTANCE.getEarthSurfaceLocationWorldWindLayerWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerWizardPagesProviderImpl <em>Surface Polygon World Wind Layer Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getSurfacePolygonWorldWindLayerWizardPagesProvider()
		 * @generated
		 */
		EClass SURFACE_POLYGON_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = eINSTANCE.getSurfacePolygonWorldWindLayerWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AirspaceWorldWindLayerWizardPagesProviderImpl <em>Airspace World Wind Layer Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.AirspaceWorldWindLayerWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getAirspaceWorldWindLayerWizardPagesProvider()
		 * @generated
		 */
		EClass AIRSPACE_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = eINSTANCE.getAirspaceWorldWindLayerWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.KMLWorldWindLayerWizardPagesProviderImpl <em>KML World Wind Layer Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.KMLWorldWindLayerWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getKMLWorldWindLayerWizardPagesProvider()
		 * @generated
		 */
		EClass KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER = eINSTANCE.getKMLWorldWindLayerWizardPagesProvider();

		/**
		 * The meta object literal for the '<em>Color3f</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see javax.vecmath.Color3f
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getColor3f()
		 * @generated
		 */
		EDataType COLOR3F = eINSTANCE.getColor3f();

		/**
		 * The meta object literal for the '<em>Renderable Layer</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gov.nasa.worldwind.layers.RenderableLayer
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getRenderableLayer()
		 * @generated
		 */
		EDataType RENDERABLE_LAYER = eINSTANCE.getRenderableLayer();

		/**
		 * The meta object literal for the '<em>ISelection</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jface.viewers.ISelection
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getISelection()
		 * @generated
		 */
		EDataType ISELECTION = eINSTANCE.getISelection();

		/**
		 * The meta object literal for the '<em>World Window</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gov.nasa.worldwind.WorldWindow
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIPackageImpl#getWorldWindow()
		 * @generated
		 */
		EDataType WORLD_WINDOW = eINSTANCE.getWorldWindow();

	}

} //ApogyEarthEnvironmentUIPackage

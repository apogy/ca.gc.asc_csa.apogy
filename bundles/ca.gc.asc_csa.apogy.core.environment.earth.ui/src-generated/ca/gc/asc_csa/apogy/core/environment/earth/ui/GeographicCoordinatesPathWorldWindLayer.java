/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Canadian Space Agency (CSA) - Initial API and implementation
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
 *           
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui;

import org.eclipse.emf.common.util.EList;

import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Geographic Coordinates Path World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 *  An AbstractWorldWindLayer representing a path made out of a list of GeographicCoordinates.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesPathWorldWindLayer#getGeographicCoordinatesList <em>Geographic Coordinates List</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getGeographicCoordinatesPathWorldWindLayer()
 * @model
 * @generated
 */
public interface GeographicCoordinatesPathWorldWindLayer extends AbstractWorldWindLayer {
	/**
	 * Returns the value of the '<em><b>Geographic Coordinates List</b></em>' containment reference list.
	 * The list contents are of type {@link ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Geographic Coordinates List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The list of geographical coordinates.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Geographic Coordinates List</em>' containment reference list.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getGeographicCoordinatesPathWorldWindLayer_GeographicCoordinatesList()
	 * @model containment="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true'"
	 * @generated
	 */
	EList<GeographicCoordinates> getGeographicCoordinatesList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Removes all points in the path.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void clearPath();

} // GeographicCoordinatesPathWorldWindLayer

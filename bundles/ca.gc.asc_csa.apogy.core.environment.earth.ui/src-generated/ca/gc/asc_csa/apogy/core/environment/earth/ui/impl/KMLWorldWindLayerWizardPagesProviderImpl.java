/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Canadian Space Agency (CSA) - Initial API and implementation
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
 *           
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.KMLWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.wizards.KMLWorldWindLayerURLWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>KML World Wind Layer Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KMLWorldWindLayerWizardPagesProviderImpl extends AbstractWorldWindLayerWizardPagesProviderImpl implements KMLWorldWindLayerWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KMLWorldWindLayerWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentUIPackage.Literals.KML_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		KMLWorldWindLayer layer = ApogyEarthEnvironmentUIFactory.eINSTANCE.createKMLWorldWindLayer();
		return layer;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
	
		KMLWorldWindLayer layer = (KMLWorldWindLayer) eObject;
		
		KMLWorldWindLayerURLWizardPage kmlWorldWindLayerURLWizardPage = new KMLWorldWindLayerURLWizardPage(layer);
		list.add(kmlWorldWindLayerURLWizardPage);
		
		return list;
	}
} //KMLWorldWindLayerWizardPagesProviderImpl

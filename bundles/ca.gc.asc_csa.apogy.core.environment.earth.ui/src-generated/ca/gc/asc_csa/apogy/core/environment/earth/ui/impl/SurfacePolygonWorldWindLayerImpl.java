/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Canadian Space Agency (CSA) - Initial API and implementation
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
 *           
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.vecmath.Color3f;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.SurfacePolygonWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.utils.WorldWindUtils;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Position.PositionList;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.ogc.kml.KMLAbstractFeature;
import gov.nasa.worldwind.ogc.kml.KMLAbstractGeometry;
import gov.nasa.worldwind.ogc.kml.KMLDocument;
import gov.nasa.worldwind.ogc.kml.KMLLinearRing;
import gov.nasa.worldwind.ogc.kml.KMLMultiGeometry;
import gov.nasa.worldwind.ogc.kml.KMLPlacemark;
import gov.nasa.worldwind.ogc.kml.KMLPolygon;
import gov.nasa.worldwind.ogc.kml.KMLRoot;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.ShapeAttributes;
import gov.nasa.worldwind.render.SurfacePolygon;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWMath;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Surface Polygon World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerImpl#getGeographicCoordinatesList <em>Geographic Coordinates List</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerImpl#getUrl <em>Url</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerImpl#getColor <em>Color</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.SurfacePolygonWorldWindLayerImpl#getOpacity <em>Opacity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SurfacePolygonWorldWindLayerImpl extends AbstractWorldWindLayerImpl implements SurfacePolygonWorldWindLayer 
{	
	protected SurfacePolygon polygon;
	
	/**
	 * The cached value of the '{@link #getGeographicCoordinatesList() <em>Geographic Coordinates List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeographicCoordinatesList()
	 * @generated
	 * @ordered
	 */
	protected EList<GeographicCoordinates> geographicCoordinatesList;

	/**
	 * The default value of the '{@link #getUrl() <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrl()
	 * @generated
	 * @ordered
	 */
	protected static final String URL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUrl() <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrl()
	 * @generated
	 * @ordered
	 */
	protected String url = URL_EDEFAULT;

	/**
	 * The default value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected static final Color3f COLOR_EDEFAULT = (Color3f)ApogyEarthEnvironmentUIFactory.eINSTANCE.createFromString(ApogyEarthEnvironmentUIPackage.eINSTANCE.getColor3f(), "0.0,1.0,0.0");

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected Color3f color = COLOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getOpacity() <em>Opacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpacity()
	 * @generated
	 * @ordered
	 */
	protected static final double OPACITY_EDEFAULT = 1.0;

	/**
	 * The cached value of the '{@link #getOpacity() <em>Opacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpacity()
	 * @generated
	 * @ordered
	 */
	protected double opacity = OPACITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SurfacePolygonWorldWindLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentUIPackage.Literals.SURFACE_POLYGON_WORLD_WIND_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GeographicCoordinates> getGeographicCoordinatesList() {
		if (geographicCoordinatesList == null) {
			geographicCoordinatesList = new EObjectContainmentEList<GeographicCoordinates>(GeographicCoordinates.class, this, ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST);
		}
		return geographicCoordinatesList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setUrl(String newUrl) {
		setUrlGen(newUrl);
		loadFile();
					
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUrlGen(String newUrl) {
		String oldUrl = url;
		url = newUrl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__URL, oldUrl, url));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Color3f getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setColor(Color3f newColor) 
	{
		setColorGen(newColor);
		updateRenderableLayer();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColorGen(Color3f newColor) {
		Color3f oldColor = color;
		color = newColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__COLOR, oldColor, color));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getOpacity() {
		return opacity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setOpacity(double newOpacity) 
	{
		if(newOpacity > 1)
		{
			setOpacityGen(1);
		}
		else if(newOpacity < 0)
		{
			setOpacityGen(0);
		}
		else
		{
			setOpacityGen(newOpacity);						
		}
		
		updateRenderableLayer();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpacityGen(double newOpacity) {
		double oldOpacity = opacity;
		opacity = newOpacity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__OPACITY, oldOpacity, opacity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public boolean isCoordinatesInside(GeographicCoordinates coordinates) 
	{
		if(polygon != null)
		{
			Position position = WorldWindUtils.convertToPosition(coordinates);			
			return WWMath.isLocationInside(position, polygon.getLocations());			
		}
		return false;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
				return ((InternalEList<?>)getGeographicCoordinatesList()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
				return getGeographicCoordinatesList();
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__URL:
				return getUrl();
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__COLOR:
				return getColor();
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__OPACITY:
				return getOpacity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
				getGeographicCoordinatesList().clear();
				getGeographicCoordinatesList().addAll((Collection<? extends GeographicCoordinates>)newValue);
				return;
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__URL:
				setUrl((String)newValue);
				return;
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__COLOR:
				setColor((Color3f)newValue);
				return;
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__OPACITY:
				setOpacity((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
				getGeographicCoordinatesList().clear();
				return;
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__URL:
				setUrl(URL_EDEFAULT);
				return;
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__COLOR:
				setColor(COLOR_EDEFAULT);
				return;
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__OPACITY:
				setOpacity(OPACITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
				return geographicCoordinatesList != null && !geographicCoordinatesList.isEmpty();
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__URL:
				return URL_EDEFAULT == null ? url != null : !URL_EDEFAULT.equals(url);
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__COLOR:
				return COLOR_EDEFAULT == null ? color != null : !COLOR_EDEFAULT.equals(color);
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER__OPACITY:
				return opacity != OPACITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyEarthEnvironmentUIPackage.SURFACE_POLYGON_WORLD_WIND_LAYER___IS_COORDINATES_INSIDE__GEOGRAPHICCOORDINATES:
				return isCoordinatesInside((GeographicCoordinates)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (url: ");
		result.append(url);
		result.append(", color: ");
		result.append(color);
		result.append(", opacity: ");
		result.append(opacity);
		result.append(')');
		return result.toString();
	}

	@Override
	public void initialise() 
	{
		loadFile();
		
		super.initialise();
	}
	
	@Override
	public boolean getDefaultAutoUpdateEnabled() 
	{
		return true;
	}
	
	protected void loadFile()
	{
		try
		{
			ApogyCommonTransactionFacade.INSTANCE.basicClear(this, ApogyEarthEnvironmentUIPackage.Literals.SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST, true);
			
			List<GeographicCoordinates> coords = new ArrayList<GeographicCoordinates>();
			
			if(getUrl() != null)
			{
				if(getUrl().endsWith("csv"))
				{
					coords = ApogyEarthFacade.INSTANCE.loadGeographicCoordinatesFromURL(getUrl());
				}
				else if(getUrl().endsWith("kmz") || getUrl().endsWith("kml"))
				{
					coords = loadGeographicCoordinatesFromKML(getUrl());
				}
			}
			ApogyCommonTransactionFacade.INSTANCE.basicAdd(this, ApogyEarthEnvironmentUIPackage.Literals.SURFACE_POLYGON_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST, coords, true);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		if(isAutoUpdateEnabled()) updateRenderableLayer();
	}
	
	@Override
	protected void updateRenderableLayer() 
	{
		if(!isUpdating() && !isDisposed())
		{
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, true);

			RenderableLayer layer = getRenderableLayer();
			layer.removeAllRenderables();	
			
			if(isVisible())
			{
				addRenderable(layer);							
			}
			getRenderableLayer().firePropertyChange(AVKey.LAYER, null, this);
			
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, false);
		}		
		
	}

	private void addRenderable(RenderableLayer layer)
	{
		try
		{
			if(!getGeographicCoordinatesList().isEmpty())
			{
				polygon = createPolygon(getWorldWindow());
				layer.addRenderable(polygon);
			}		
			else
			{
				polygon = null;
			}
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
	}
	
   protected SurfacePolygon createPolygon(WorldWindow wwd)
   {
        SurfacePolygon poly = new SurfacePolygon();
        poly.setAttributes(getDefaultAttributes());
        poly.setValue(AVKey.DISPLAY_NAME, getName());
        this.initializePolygon(wwd, poly);

        return poly;
    }
   
   protected void initializePolygon(WorldWindow wwd, SurfacePolygon polygon)
   {
	   	List<Position> positions = new ArrayList<Position>();
	   	for(GeographicCoordinates coords :getGeographicCoordinatesList())
	   	{
	   		Position position = WorldWindUtils.convertToPosition(coords);
	   		positions.add(position);
	   	}	   		      
	   	polygon.setLocations(positions);
   }
   
   protected ShapeAttributes getDefaultAttributes()
   {
	   ShapeAttributes attributes = new BasicShapeAttributes();
       
       Color color = WorldWindUtils.convertFrom(getColor());
       
       attributes.setInteriorMaterial(new Material(color, color, color, color, 0.0f));         
       attributes.setOutlineMaterial(new Material(color, color, color, color, 0.0f));
       
       attributes.setDrawOutline(true);
       attributes.setInteriorOpacity(getOpacity());
       attributes.setOutlineOpacity(0.95);
       attributes.setOutlineWidth(2);
       return attributes;
   }
   
   private List<GeographicCoordinates> loadGeographicCoordinatesFromKML(String urlString)
   {
	   List<GeographicCoordinates> coords = new ArrayList<GeographicCoordinates>();
	   
	   try
	   {
		   URL url = WWIO.makeURL(urlString);
		   KMLRoot kmlRoot = KMLRoot.createAndParse(url);
		   KMLDocument kmlDocument = (KMLDocument) kmlRoot.getFeature();
		   List<KMLAbstractFeature> features = WorldWindUtils.getAllKMLAbstractFeatureOfType(KMLPlacemark.class, kmlDocument);
		   
		   System.out.println();
		   
		   KMLPlacemark kmlPlacemark = (KMLPlacemark) features.get(0);
		   if(kmlPlacemark.getGeometry() instanceof KMLMultiGeometry)
		   {
			   KMLMultiGeometry kmlMultiGeometry = (KMLMultiGeometry) kmlPlacemark.getGeometry();
			   
			   KMLPolygon kmlPolygon = null;
			   Iterator<KMLAbstractGeometry> it = kmlMultiGeometry.getGeometries().iterator();
			   while(it.hasNext() && kmlPolygon == null)
			   {
				   KMLAbstractGeometry tmp =it.next();
				   if(tmp instanceof KMLPolygon)
				   {
					   kmlPolygon = (KMLPolygon) tmp;
				   }
			   }
			   			   
			   KMLLinearRing ring = kmlPolygon.getOuterBoundary();
			   PositionList positionList = ring.getCoordinates();
			   for(Position position : positionList.list)
			   {
					GeographicCoordinates coordinates = ApogyEarthEnvironmentFactory.eINSTANCE.createGeographicCoordinates();
					coordinates.setElevation(position.getElevation());
					coordinates.setLatitude(position.getLatitude().radians);
					coordinates.setLongitude(position.getLongitude().radians);						
					coords.add(coordinates);
			   }
		   }		   
	   }
	   catch (Exception e) 
	   {
		   e.printStackTrace();
	   }
	   
	   return coords;
   }
   
} //SurfacePolygonWorldWindLayerImpl

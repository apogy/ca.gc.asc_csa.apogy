/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Canadian Space Agency (CSA) - Initial API and implementation
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
 *           
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Airspace World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 *  An AbstractWorldWindLayer representing volume in the atmosphere.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getLowerAltitude <em>Lower Altitude</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getUpperAltitude <em>Upper Altitude</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAirspaceWorldWindLayer()
 * @model
 * @generated
 */
public interface AirspaceWorldWindLayer extends SurfacePolygonWorldWindLayer {
	/**
	 * Returns the value of the '<em><b>Lower Altitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The lower altitude limit, relative to Mean Sea Level. A negative value has the airspace follow the ground.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Lower Altitude</em>' attribute.
	 * @see #setLowerAltitude(double)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAirspaceWorldWindLayer_LowerAltitude()
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' apogy_units='m'"
	 * @generated
	 */
	double getLowerAltitude();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getLowerAltitude <em>Lower Altitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower Altitude</em>' attribute.
	 * @see #getLowerAltitude()
	 * @generated
	 */
	void setLowerAltitude(double value);

	/**
	 * Returns the value of the '<em><b>Upper Altitude</b></em>' attribute.
	 * The default value is <code>"10000"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The upper altitude limit, relative to Mean Sea Level.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Upper Altitude</em>' attribute.
	 * @see #setUpperAltitude(double)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAirspaceWorldWindLayer_UpperAltitude()
	 * @model default="10000" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' apogy_units='m'"
	 * @generated
	 */
	double getUpperAltitude();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getUpperAltitude <em>Upper Altitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Altitude</em>' attribute.
	 * @see #getUpperAltitude()
	 * @generated
	 */
	void setUpperAltitude(double value);

} // AirspaceWorldWindLayer

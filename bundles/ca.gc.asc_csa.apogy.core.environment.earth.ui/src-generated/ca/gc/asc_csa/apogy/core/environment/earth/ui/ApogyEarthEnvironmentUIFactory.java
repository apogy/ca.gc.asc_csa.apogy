/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage
 * @generated
 */
public interface ApogyEarthEnvironmentUIFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyEarthEnvironmentUIFactory eINSTANCE = ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.ApogyEarthEnvironmentUIFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Earth UI Facade</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Earth UI Facade</em>'.
	 * @generated
	 */
	EarthUIFacade createEarthUIFacade();

	/**
	 * Returns a new object of class '<em>Earth View Configuration List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Earth View Configuration List</em>'.
	 * @generated
	 */
	EarthViewConfigurationList createEarthViewConfigurationList();

	/**
	 * Returns a new object of class '<em>Earth View Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Earth View Configuration</em>'.
	 * @generated
	 */
	EarthViewConfiguration createEarthViewConfiguration();

	/**
	 * Returns a new object of class '<em>Earth View Configuration Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Earth View Configuration Reference</em>'.
	 * @generated
	 */
	EarthViewConfigurationReference createEarthViewConfigurationReference();

	/**
	 * Returns a new object of class '<em>Geographic Coordinates World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Geographic Coordinates World Wind Layer</em>'.
	 * @generated
	 */
	GeographicCoordinatesWorldWindLayer createGeographicCoordinatesWorldWindLayer();

	/**
	 * Returns a new object of class '<em>Earth Surface Location World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Earth Surface Location World Wind Layer</em>'.
	 * @generated
	 */
	EarthSurfaceLocationWorldWindLayer createEarthSurfaceLocationWorldWindLayer();

	/**
	 * Returns a new object of class '<em>Geographic Coordinates Path World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Geographic Coordinates Path World Wind Layer</em>'.
	 * @generated
	 */
	GeographicCoordinatesPathWorldWindLayer createGeographicCoordinatesPathWorldWindLayer();

	/**
	 * Returns a new object of class '<em>Surface Polygon World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Surface Polygon World Wind Layer</em>'.
	 * @generated
	 */
	SurfacePolygonWorldWindLayer createSurfacePolygonWorldWindLayer();

	/**
	 * Returns a new object of class '<em>Airspace World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Airspace World Wind Layer</em>'.
	 * @generated
	 */
	AirspaceWorldWindLayer createAirspaceWorldWindLayer();

	/**
	 * Returns a new object of class '<em>KML World Wind Layer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>KML World Wind Layer</em>'.
	 * @generated
	 */
	KMLWorldWindLayer createKMLWorldWindLayer();

	/**
	 * Returns a new object of class '<em>Abstract World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract World Wind Layer Wizard Pages Provider</em>'.
	 * @generated
	 */
	AbstractWorldWindLayerWizardPagesProvider createAbstractWorldWindLayerWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Earth Surface Location World Wind Layer Wizard Pages Provider</em>'.
	 * @generated
	 */
	EarthSurfaceLocationWorldWindLayerWizardPagesProvider createEarthSurfaceLocationWorldWindLayerWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Surface Polygon World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Surface Polygon World Wind Layer Wizard Pages Provider</em>'.
	 * @generated
	 */
	SurfacePolygonWorldWindLayerWizardPagesProvider createSurfacePolygonWorldWindLayerWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Airspace World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Airspace World Wind Layer Wizard Pages Provider</em>'.
	 * @generated
	 */
	AirspaceWorldWindLayerWizardPagesProvider createAirspaceWorldWindLayerWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>KML World Wind Layer Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>KML World Wind Layer Wizard Pages Provider</em>'.
	 * @generated
	 */
	KMLWorldWindLayerWizardPagesProvider createKMLWorldWindLayerWizardPagesProvider();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyEarthEnvironmentUIPackage getApogyEarthEnvironmentUIPackage();

} //ApogyEarthEnvironmentUIFactory

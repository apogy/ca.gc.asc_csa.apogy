/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui;

import org.eclipse.jface.viewers.ISelection;

import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.core.Updatable;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.layers.RenderableLayer;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Defines a Layer that can be shown on the WordWindModel
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isDisposed <em>Disposed</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isVisible <em>Visible</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isBlinking <em>Blinking</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#getRenderableLayer <em>Renderable Layer</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#getWorldWindow <em>World Window</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAbstractWorldWindLayer()
 * @model abstract="true"
 * @generated
 */
public interface AbstractWorldWindLayer extends Updatable, Named, Described {
	/**
	 * Returns the value of the '<em><b>Disposed</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Whether or not this layer has been disposed of.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Disposed</em>' attribute.
	 * @see #setDisposed(boolean)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAbstractWorldWindLayer_Disposed()
	 * @model default="false" unique="false" transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='false' property='None'"
	 * @generated
	 */
	boolean isDisposed();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isDisposed <em>Disposed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disposed</em>' attribute.
	 * @see #isDisposed()
	 * @generated
	 */
	void setDisposed(boolean value);

	/**
	 * Returns the value of the '<em><b>Visible</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Whether or not to display the layer.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Visible</em>' attribute.
	 * @see #setVisible(boolean)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAbstractWorldWindLayer_Visible()
	 * @model default="true" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' property='Editable'"
	 * @generated
	 */
	boolean isVisible();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isVisible <em>Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visible</em>' attribute.
	 * @see #isVisible()
	 * @generated
	 */
	void setVisible(boolean value);

	/**
	 * Returns the value of the '<em><b>Blinking</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * When enabled, make the layer blink.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Blinking</em>' attribute.
	 * @see #setBlinking(boolean)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAbstractWorldWindLayer_Blinking()
	 * @model default="false" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' property='Editable'"
	 * @generated
	 */
	boolean isBlinking();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#isBlinking <em>Blinking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Blinking</em>' attribute.
	 * @see #isBlinking()
	 * @generated
	 */
	void setBlinking(boolean value);

	/**
	 * Returns the value of the '<em><b>Renderable Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The RenderableLayer associated with this Layer
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Renderable Layer</em>' attribute.
	 * @see #setRenderableLayer(RenderableLayer)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAbstractWorldWindLayer_RenderableLayer()
	 * @model unique="false" dataType="ca.gc.asc_csa.apogy.core.environment.earth.ui.RenderableLayer" transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel children='false' notify='false' property='None'"
	 * @generated
	 */
	RenderableLayer getRenderableLayer();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#getRenderableLayer <em>Renderable Layer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Renderable Layer</em>' attribute.
	 * @see #getRenderableLayer()
	 * @generated
	 */
	void setRenderableLayer(RenderableLayer value);

	/**
	 * Returns the value of the '<em><b>World Window</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * A reference to the WorldWindow displaying the layer.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>World Window</em>' attribute.
	 * @see #setWorldWindow(WorldWindow)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAbstractWorldWindLayer_WorldWindow()
	 * @model unique="false" dataType="ca.gc.asc_csa.apogy.core.environment.earth.ui.WorldWindow" transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel children='false' notify='false' property='None'"
	 * @generated
	 */
	WorldWindow getWorldWindow();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer#getWorldWindow <em>World Window</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>World Window</em>' attribute.
	 * @see #getWorldWindow()
	 * @generated
	 */
	void setWorldWindow(WorldWindow value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method called when the layer is first rendered. Can be called multiple times.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void initialise();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method called when the layer is not longer
	 * needed (i.e. when it gets deleted.)
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void dispose();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method called when a selection is made by the user.
	 * Sub-classes can override this method if applicable.
	 * @param selection The new selection made.
	 * <!-- end-model-doc -->
	 * @model selectionDataType="ca.gc.asc_csa.apogy.core.environment.earth.ui.ISelection" selectionUnique="false"
	 * @generated
	 */
	void selectionChanged(ISelection selection);

} // AbstractWorldWindLayer

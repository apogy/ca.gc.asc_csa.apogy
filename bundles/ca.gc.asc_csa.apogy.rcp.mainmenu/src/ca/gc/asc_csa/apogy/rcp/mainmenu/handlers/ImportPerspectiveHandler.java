package ca.gc.asc_csa.apogy.rcp.mainmenu.handlers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspectiveStack;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainer;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.registry.PerspectiveRegistry;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.rcp.Activator;
import ca.gc.asc_csa.apogy.rcp.ApogyRCPConstants;

@SuppressWarnings("restriction")
public class ImportPerspectiveHandler {

	@Execute
	public void execute(Shell shell, EModelService modelService, MApplication application) {

		MWindow window = (MWindow)modelService.find(ApogyRCPConstants.MAIN_WINDOW__ID, application);
		/** Find the file. */
		FileDialog dialog = new FileDialog(shell, SWT.OPEN);
		dialog.setFilterExtensions(new String[] { "*.e4xmi" });
		String path = dialog.open();
		
		if(path != null){
			File tmpFile = new File(path);
			URI uri = URI.createURI(tmpFile.toURI().toString());

			/** Create the resource. */
			ResourceSet resourceSet = new ResourceSetImpl();
			Resource resource = resourceSet.getResource(uri, true);
			
			try{
				MPerspective perspective = (MPerspective) resource.getContents().get(0);
				MPerspectiveStack stack = (MPerspectiveStack) modelService.find(ApogyRCPConstants.PERSPECTIVE_STACK__ID, window);

				/** Add "_import" to the label and id. */
				perspective.setLabel(perspective.getLabel() + "_import");
				perspective.setElementId(perspective.getElementId() + "_import");
				
				/** Make the perspective ID unique */
				makeIDsUnique(perspective, stack);
				
				/** Add the perspective ID to all the elements of the perspective */
				List<MUIElement> partSashElementToAdd = new ArrayList<>();
				partSashElementToAdd.addAll(modelService.findElements(perspective, null, MPart.class, null));
				partSashElementToAdd.addAll(modelService.findElements(perspective, null, MPartSashContainer.class, null));
				partSashElementToAdd.addAll(modelService.findElements(perspective, null, MPartStack.class, null));
				
				for (MUIElement element : partSashElementToAdd) {
					if (!(element instanceof MPart) || !(element instanceof MPart && "bundleclass://org.eclipse.ui.workbench/org.eclipse.ui.internal.e4.compatibility.CompatibilityView".equals(((MPart) element).getContributionURI()))) {
						/** Make the IDs unique */
						element.setElementId(element.getElementId() + "_" + perspective.getElementId());
					}
				}

				/** Add the perspective to the registry to avoid orphan perspectives */
				((PerspectiveRegistry)PlatformUI.getWorkbench().getPerspectiveRegistry()).addPerspective(perspective);
				application.getSnippets().add(modelService.cloneElement(perspective, application));		
				/** Add the perspective */
				stack.getChildren().add(perspective);
				stack.setSelectedElement(perspective);
			}catch(Exception e){
				Logger.INSTANCE.log(Activator.ID, "Error opening the .e4xmi, top element is not a perspective",
						EventSeverity.ERROR);
			}
		}
	}
	
	/**
	 * Makes the id of the perspective unique in the stack.
	 */
	private void makeIDsUnique(MPerspective perspective, MPerspectiveStack stack){
		int j = 1;
		/** Find an ID that is unique */
		for (int i = 0; i < stack.getChildren().size(); i++) {
			if (stack.getChildren().get(i) instanceof MPerspective) {
				MPerspective stackedPerspective = (MPerspective) stack.getChildren().get(i);
				if (stackedPerspective.getElementId() != null && stackedPerspective.getElementId()
						.startsWith(perspective.getElementId() + "_" + Integer.toString(j))) {
					j++;
					i = 0;
				}
			}
		}
		perspective.setElementId(perspective.getElementId() + "_" + Integer.toString(j));
	}
}
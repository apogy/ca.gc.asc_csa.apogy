package ca.gc.asc_csa.apogy.rcp.mainmenu.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import ca.gc.asc_csa.apogy.rcp.mainmenu.dialogs.AddViewDialog;

public class AddViewHandler {

	@Inject
	EModelService modelService;

	@Inject
	MApplication application;

	/**
	 * Opens a wizard to add a view to the perspective
	 */
	@Execute
	public void execute(IEclipseContext context) {
		AddViewDialog dialog = ContextInjectionFactory.make(AddViewDialog.class, context);
		dialog.open();
	}

	@CanExecute
	public boolean canExecute() {
		return modelService.getActivePerspective(application.getChildren().get(0)) != null;
	}
}
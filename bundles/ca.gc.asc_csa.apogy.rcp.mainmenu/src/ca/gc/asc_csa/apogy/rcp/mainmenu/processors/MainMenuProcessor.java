package ca.gc.asc_csa.apogy.rcp.mainmenu.processors;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.menu.MMenu;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuFactory;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.rcp.mainmenu.Activator;

public class MainMenuProcessor {
	@Inject
	protected MApplication app;

	@Execute
	public void run(@Optional IEclipseContext context, MApplication app) 
	{
		/**
		 * This is used to fix the known e4 error that makes the menu dissapear
		 * when restarting the application.
		 */
		try {
			MMenu menu = app.getChildren().get(0).getMainMenu();
			if (menu == null) {
				menu = MMenuFactory.INSTANCE.createMenu();
				menu.setElementId("org.eclipse.ui.main.menu");
				app.getChildren().get(0).setMainMenu(menu);
			}
		} catch (Exception ex) {
			String message = this.getClass().getSimpleName() + " Error during the initialization of the main menu ";
			Logger.INSTANCE.log(Activator.ID, this, message, EventSeverity.ERROR);
		}
	}
}

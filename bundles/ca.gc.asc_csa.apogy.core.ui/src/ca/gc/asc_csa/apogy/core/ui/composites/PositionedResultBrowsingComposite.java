package ca.gc.asc_csa.apogy.core.ui.composites;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.core.PositionedResult;

public class PositionedResultBrowsingComposite extends Composite 
{
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
		
	private List<PositionedResult> inputs = new ArrayList<PositionedResult>();
	
	private PositionedResultSearchComposite positionedResultSearchComposite;
	private PositionedResultListComposite positionedResultListComposite;
	
	public PositionedResultBrowsingComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		// Results
		ScrolledForm scrldResults = formToolkit.createScrolledForm(this);
		scrldResults.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrldResults.setBounds(0, 0, 186, 165);
		formToolkit.paintBordersFor(scrldResults);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			scrldResults.getBody().setLayout(tableWrapLayout);
		}
		
		Section sctnFiltersAndSorters = formToolkit.createSection(scrldResults.getBody(), Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnNewSection = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 1, 1);
		twd_sctnNewSection.grabHorizontal = true;
		twd_sctnNewSection.grabVertical = true;
		twd_sctnNewSection.valign = TableWrapData.FILL;
		sctnFiltersAndSorters.setLayoutData(twd_sctnNewSection);
		sctnFiltersAndSorters.setBounds(0, 0, 112, 27);
		formToolkit.paintBordersFor(sctnFiltersAndSorters);
		sctnFiltersAndSorters.setText("Filters and Sorters");
		
		positionedResultSearchComposite = new PositionedResultSearchComposite(sctnFiltersAndSorters, SWT.NONE)
		{
			@Override
			protected void applyFilter()
			{		
				Collection<PositionedResult> filtered = positionedResultSearchComposite.applyFilter(inputs);				
				SortedSet<PositionedResult> sorted = positionedResultSearchComposite.sort(filtered);				
				positionedResultListComposite.setPositionedResultList(sorted);
			}
			
			@Override
			protected void applySorter()
			{		
				Collection<PositionedResult> filtered = positionedResultSearchComposite.applyFilter(inputs);
				SortedSet<PositionedResult> sorted = positionedResultSearchComposite.sort(filtered);
				positionedResultListComposite.setPositionedResultList(sorted);
			}
		};
		Comparator<PositionedResult> com = ApogyCommonEMFFactory.eINSTANCE.createTimedComparator();
		positionedResultSearchComposite.getCompositeComparator().getComparators().add(com);
		positionedResultSearchComposite.setCompositeComparator(positionedResultSearchComposite.getCompositeComparator());
		
		formToolkit.adapt(positionedResultSearchComposite);
		formToolkit.paintBordersFor(positionedResultSearchComposite);
		sctnFiltersAndSorters.setClient(positionedResultSearchComposite);	
		
		Section sctnResults = formToolkit.createSection(scrldResults.getBody(), Section.TITLE_BAR);
		TableWrapData twd_sctnResults = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL, 1, 1);
		twd_sctnResults.grabHorizontal = true;
		twd_sctnResults.align = TableWrapData.FILL;
		twd_sctnResults.valign = TableWrapData.FILL;
		twd_sctnResults.grabVertical = true;
		sctnResults.setLayoutData(twd_sctnResults);
		sctnResults.setBounds(0, 0, 112, 27);
		formToolkit.paintBordersFor(sctnResults);
		sctnResults.setText("Data Products");
								
		positionedResultListComposite = new PositionedResultListComposite(sctnResults, SWT.NONE)
		{
			@Override
			protected void positionedResultSelectedChanged(PositionedResult positionedResult) 
			{				
				newPositionnedResultSelected(positionedResult);
			}
		};
		
		formToolkit.adapt(positionedResultListComposite);
		formToolkit.paintBordersFor(positionedResultListComposite);
		sctnResults.setClient(positionedResultListComposite);	

	}

	public List<PositionedResult> getInputs() {
		return inputs;
	}

	public void setInputs(List<PositionedResult> newInputs) 
	{
		this.inputs.clear();
		
		if(newInputs != null)
		{
			this.inputs.addAll(newInputs);
		}
		
		Collection<PositionedResult> filtered = positionedResultSearchComposite.applyFilter(inputs);
		SortedSet<PositionedResult> sorted = positionedResultSearchComposite.sort(filtered);
		positionedResultListComposite.setPositionedResultList(sorted);		
	}	
	
	/**
	 * Method called when the user selects a PositionedResult in the list. Can be overloaded.
	 * @param positionedResult The PositionedResult selected.
	 */
	protected void newPositionnedResultSelected(PositionedResult positionedResult)
	{		
	}
	
}

package ca.gc.asc_csa.apogy.core.ui.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.PositionedResult;
import ca.gc.asc_csa.apogy.core.invocator.AbstractResult;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.DataProductsList;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallResultsList;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;
import ca.gc.asc_csa.apogy.core.ui.composites.PositionedResultBrowsingComposite;

public class PositionedResultSearchPart extends AbstractSessionBasedPart
{
	private PositionedResultBrowsingComposite positionedResultBrowsingComposite;
	
	private DataProductsList dataProductsList;
	
	private List<PositionedResult> inputs = new ArrayList<PositionedResult>();
	
	private Adapter dataProductsListAdapter;
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{		
		if(invocatorSession != null)
		{						
			updateInputs(invocatorSession.getDataProductsListContainer().getDataProductsList().get(0));					
		}
		else
		{
			updateInputs(null);
		}
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{
		parent.setLayout(new FillLayout());
		
		Composite top = new Composite(parent, SWT.BORDER);
		GridLayout gl_parent = new GridLayout(1, false);
		gl_parent.verticalSpacing = 10;
		top.setLayout(gl_parent);
		
		positionedResultBrowsingComposite = new PositionedResultBrowsingComposite(top, SWT.COLOR_WIDGET_BORDER)
		{
			@Override
			protected void newPositionnedResultSelected(PositionedResult positionedResult) 
			{
				selectionService.setSelection(positionedResult);
			}
		};
		positionedResultBrowsingComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));			
	}	
	
	private void updateInputs(final DataProductsList newDataProductsList)
	{
		if(positionedResultBrowsingComposite != null && !positionedResultBrowsingComposite.isDisposed())
		{
			positionedResultBrowsingComposite.getDisplay().asyncExec(new Runnable() 
			{			
				@Override
				public void run() 
				{				
					if(dataProductsList != null)
					{
						dataProductsList.getOperationCallResultsList().eAdapters().remove(getDataProductsListAdapter());
					}
					
					dataProductsList = newDataProductsList;
					
					inputs.clear();
					
					if(newDataProductsList != null)
					{
						OperationCallResultsList operationCallResultsList = dataProductsList.getOperationCallResultsList();
						for(AbstractResult abstractResult : operationCallResultsList.getResults())
						{
							if(abstractResult instanceof PositionedResult)
							{
								PositionedResult positionedResult = (PositionedResult) abstractResult;
								inputs.add(positionedResult);
							}
						}
					}								
					
					positionedResultBrowsingComposite.setInputs(inputs);	
					
					if(newDataProductsList != null)
					{
						dataProductsList.getOperationCallResultsList().eAdapters().add(getDataProductsListAdapter());
					}
				}
			});
		}					
	}
	
	private Adapter getDataProductsListAdapter()
	{
		if(dataProductsListAdapter == null)
		{
			dataProductsListAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{					
					if(msg.getNotifier() instanceof OperationCallResultsList)
					{
						int featureID = msg.getFeatureID(OperationCallResultsList.class);
						switch (featureID) 
						{
							case ApogyCoreInvocatorPackage.RESULTS_LIST__RESULTS:
								updateInputs(dataProductsList);
							break;

						default:
							break;
						}						
					}
					
				}
			};
		}
		
		return dataProductsListAdapter;
	}
	
	@Override
	protected void dispose() {
		if(dataProductsList != null)
		{
			dataProductsList.getOperationCallResultsList().eAdapters().remove(getDataProductsListAdapter());
		}
		super.dispose();
	}
}

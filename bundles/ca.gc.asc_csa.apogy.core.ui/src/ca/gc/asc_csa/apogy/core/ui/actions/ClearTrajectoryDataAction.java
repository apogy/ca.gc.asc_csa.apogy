package ca.gc.asc_csa.apogy.core.ui.actions;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;

import ca.gc.asc_csa.apogy.core.ui.views.MovableTrajectoryView;

public class ClearTrajectoryDataAction extends Action {

	/**
	 * The {@link TopologyView} to pin/un pin.
	 */
	private MovableTrajectoryView view;

	public ClearTrajectoryDataAction(MovableTrajectoryView view) 
	{
		super("Clear", IAction.AS_PUSH_BUTTON);

		if (view == null) {
			throw new IllegalArgumentException();
		}

		this.view = view;

		update();
	}

	@Override
	public void run() 
	{
		view.clearTrajectoryData();
	}

	public void update() {
		setChecked(view.isPinned());
	}

}

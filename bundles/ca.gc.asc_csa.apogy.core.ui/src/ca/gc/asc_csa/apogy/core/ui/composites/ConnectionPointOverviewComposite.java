package ca.gc.asc_csa.apogy.core.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.GroupNode;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.NodeSearchComposite;
import ca.gc.asc_csa.apogy.common.topology.ui.dialogs.NodeSelectionDialog;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.ConnectionPoint;

public class ConnectionPointOverviewComposite extends Composite 
{
	private ConnectionPoint connectionPoint;
	
	private DataBindingContext m_bindingContext;
	private Text txtNamevalue;
	private Text txtDescriptionvalue;
	private Text txtNodeID;
	private Button btnSelectNode;

	public ConnectionPointOverviewComposite(Composite parent, int style) 
	{
		super(parent, SWT.NO_BACKGROUND);	
		setLayout(new GridLayout(3, false));		
		
		Label lblName = new Label(this, SWT.NONE);
		lblName.setAlignment(SWT.RIGHT);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name:");
		
		txtNamevalue = new Text(this, SWT.NONE);
		GridData gd_txtNamevalue = new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1);
		gd_txtNamevalue.minimumWidth = 300;
		gd_txtNamevalue.widthHint = 300;
		txtNamevalue.setLayoutData(gd_txtNamevalue);
		
		Label lblDescription = new Label(this, SWT.NONE);
		lblDescription.setAlignment(SWT.RIGHT);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDescription.setText("Description:");
		
		txtDescriptionvalue = new Text(this, SWT.NONE | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtDescriptionvalue = new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1);
		gd_txtDescriptionvalue.minimumWidth = 300;
		gd_txtDescriptionvalue.widthHint = 300;
		gd_txtDescriptionvalue.minimumHeight = 50;
		gd_txtDescriptionvalue.heightHint = 50;
		txtDescriptionvalue.setLayoutData(gd_txtDescriptionvalue);		
		
		Label label = new Label(this, SWT.NONE);
		label.setText("Refered Node ID : ");
		
		txtNodeID = new Text(this, SWT.NONE);
		txtNodeID.setEditable(false);
		GridData gd_lblFromNodeID = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_lblFromNodeID.minimumWidth = 250;
		gd_lblFromNodeID.widthHint = 250;
		txtNodeID.setLayoutData(gd_lblFromNodeID);
		
		btnSelectNode = new Button(this, SWT.NONE);
		btnSelectNode.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnSelectNode.setText("Select");
		btnSelectNode.setToolTipText("Opens a Wizard to select the Node to which the connection point is associated.");
		btnSelectNode.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				// Opens the Node Selection Dialog.
				ApogySystem apogySystem = connectionPoint.getPointsList().getApogySystem();
				Node root = apogySystem.getTopologyRoot().getOriginNode();
				NodeSelectionDialog nodeSelectionDialog = new NodeSelectionDialog(getShell(), root)
				{
					@Override
					protected NodeSearchComposite createNodeSearchComposite(Composite parent) 
					{
						NodeSearchComposite nodeSearchComposite = new NodeSearchComposite(parent, SWT.NONE, root)
						{
							@Override
							public void nodeSelectedChanged(Node nodeSelected) 
							{
								setSelectedNode(nodeSelected);
							}
						};
						
						nodeSearchComposite.setTypeFilter(ApogyCommonTopologyPackage.Literals.GROUP_NODE);
						
						return nodeSearchComposite;
					}
				};
				
				if(nodeSelectionDialog.open() == Window.OK)		
				{
					Node fromNode = nodeSelectionDialog.getSelectedNode();
					
					if(fromNode instanceof GroupNode)
					{
						GroupNode selectedNode = (GroupNode) fromNode;
						
						if(ApogyCommonTransactionFacade.INSTANCE.areEditingDomainsValid(connectionPoint, ApogyCorePackage.Literals.CONNECTION_POINT__NODE, selectedNode, false) == ApogyCommonTransactionFacade.EXECUTE_COMMAND_ON_OWNER_DOMAIN)
						{
							ApogyCommonTransactionFacade.INSTANCE.basicSet(connectionPoint, ApogyCorePackage.Literals.CONNECTION_POINT__NODE, fromNode);							
						}
						else
						{
							connectionPoint.setNode(selectedNode);						
						}
						
						txtNodeID.setText(selectedNode.getNodeId());
					}										
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) 
			{			
			}
		});
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();				
			}
		});
	}

	public ConnectionPoint getConnectionPoint() 
	{
		return connectionPoint;
	}

	public void setConnectionPoint(ConnectionPoint map) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.connectionPoint = map;
		
		if(map != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		/* Name Value. */
		IObservableValue<Double> observeName = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.NAMED__NAME)).observe(getConnectionPoint());
		IObservableValue<String> observeNameValueText = WidgetProperties.text(SWT.Modify).observe(txtNamevalue);

		bindingContext.bindValue(observeNameValueText,
								observeName, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		/* Description Value. */
		IObservableValue<Double> observeDescription = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.DESCRIBED__DESCRIPTION)).observe(getConnectionPoint());
		IObservableValue<String> observeDescriptionText = WidgetProperties.text(SWT.Modify).observe(txtDescriptionvalue);

		bindingContext.bindValue(observeDescriptionText,
								 observeDescription, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		// TODO Add NODE.
		if(getConnectionPoint().getNode() != null)
		{
			txtNodeID.setText(getConnectionPoint().getNode().getNodeId());
		}
		
		return bindingContext;
	}
}

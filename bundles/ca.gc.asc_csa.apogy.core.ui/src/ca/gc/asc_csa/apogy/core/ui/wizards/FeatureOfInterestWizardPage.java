package ca.gc.asc_csa.apogy.core.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.math.ui.composites.TransformMatrixComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.FeatureOfInterest;

public class FeatureOfInterestWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.ui.wizards.FeatureOfInterestWizardPage";
	
	private FeatureOfInterest featureOfInterest;
	
	private TransformMatrixComposite transformMatrixComposite;
		
	public FeatureOfInterestWizardPage(FeatureOfInterest featureOfInterest) 
	{
		super(WIZARD_PAGE_ID);
		this.featureOfInterest = featureOfInterest;
		
		setTitle("Feature Of Interest Position and Orientation");
		setDescription("Set the Feature Of Interest position and orientation.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(1, false));

		transformMatrixComposite = new TransformMatrixComposite(container, SWT.NONE, ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain());
		transformMatrixComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		transformMatrixComposite.setMatrix4x4(featureOfInterest.getPose());
	
		setControl(container);			
	}	
		
	protected void validate()
	{
		setErrorMessage(null);
		setPageComplete(true);
	}
}

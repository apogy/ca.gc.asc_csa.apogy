package ca.gc.asc_csa.apogy.core.ui.commands;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import ca.gc.asc_csa.apogy.core.AbstractApogySystemPoseCorrector;
import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter;
import ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.Variable;
import ca.gc.asc_csa.apogy.core.invocator.VariableImplementation;

public class TogglePoseCorrectorEnabledCommandHandler extends AbstractHandler implements
		IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{
		Iterator<?> selections = ((IStructuredSelection) HandlerUtil.getActiveMenuSelection(event)).iterator();

		while (selections.hasNext()) 
		{
			Object selection = selections.next();

			if (selection instanceof AbstractApogySystemPoseCorrector) 
			{
				AbstractApogySystemPoseCorrector corrector = (AbstractApogySystemPoseCorrector) selection;
				
				corrector.setEnabled(!corrector.isEnabled());
			}
			else if(selection instanceof Variable)
			{
				Variable variable = (Variable) selection;
				
				AbstractTypeImplementation ati = ApogyCoreInvocatorFacade.INSTANCE.getTypeImplementation(variable);
				if(ati instanceof VariableImplementation)
				{
					VariableImplementation va = (VariableImplementation) ati;
					if(va.getAdapterInstance() instanceof ApogySystemApiAdapter)
					{
						ApogySystemApiAdapter ssaa = (ApogySystemApiAdapter) va.getAdapterInstance();
						
						AbstractApogySystemPoseCorrector corrector = ssaa.getPoseCorrector();
						if(corrector != null)
						{
							corrector.setEnabled(!corrector.isEnabled());
						}
					}
				}
			}
		}
		return null;
	}
}

package ca.gc.asc_csa.apogy.core.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.ConnectionPoint;

public class ApogySystemConnectionPointsComposite extends Composite 
{
	private ApogySystem apogySystem;
	
	private ApogySystemConnectionPointsListComposite apogySystemConnectionPointsListComposite;
	private ConnectionPointOverviewComposite connectionPointOverviewComposite;

	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	public ApogySystemConnectionPointsComposite(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(1, true));
		
		ScrolledForm scrolledForm = formToolkit.createScrolledForm(this);
		scrolledForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrolledForm);		
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.numColumns = 1;
			tableWrapLayout.makeColumnsEqualWidth = true;
			scrolledForm.getBody().setLayout(tableWrapLayout);
		}
		
		// Connections Points
		Section sctnConnectionPoints = formToolkit.createSection(scrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnDemlist = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 3, 1);
		twd_sctnDemlist.valign = TableWrapData.FILL;
		twd_sctnDemlist.grabVertical = true;
		twd_sctnDemlist.grabHorizontal = true;
		sctnConnectionPoints.setLayoutData(twd_sctnDemlist);
		formToolkit.paintBordersFor(sctnConnectionPoints);
		sctnConnectionPoints.setText("Connection Points");
		
		apogySystemConnectionPointsListComposite = new ApogySystemConnectionPointsListComposite(sctnConnectionPoints, SWT.NONE)
		{
			@Override
			protected void newConnectionPointSelected(ConnectionPoint connectionPoint) 
			{
				connectionPointOverviewComposite.setConnectionPoint(connectionPoint);
			}
		};				
		
		formToolkit.adapt(apogySystemConnectionPointsListComposite);
		formToolkit.paintBordersFor(apogySystemConnectionPointsListComposite);
		sctnConnectionPoints.setClient(apogySystemConnectionPointsListComposite);
		
		// Connections Points
		Section sctnConnectionPointOverview = formToolkit.createSection(scrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnOverview = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 3, 1);
		twd_sctnOverview.valign = TableWrapData.FILL;
		twd_sctnOverview.grabVertical = true;
		twd_sctnOverview.grabHorizontal = true;
		sctnConnectionPointOverview.setLayoutData(twd_sctnOverview);
		formToolkit.paintBordersFor(sctnConnectionPointOverview);
		sctnConnectionPointOverview.setText("Connection Point Overview");
		
		connectionPointOverviewComposite = new ConnectionPointOverviewComposite(sctnConnectionPointOverview, SWT.NONE);
		formToolkit.adapt(connectionPointOverviewComposite);
		formToolkit.paintBordersFor(connectionPointOverviewComposite);
		sctnConnectionPointOverview.setClient(connectionPointOverviewComposite);	
								
		// Do clean up upon dispose.
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent arg0) 
			{
				apogySystem = null;
			}
		});
	}

	public ApogySystem getApogySystem() {
		return apogySystem;
	}

	public void setApogySystem(ApogySystem apogySystem) 
	{
		this.apogySystem = apogySystem;
		
		if(apogySystemConnectionPointsListComposite != null && !apogySystemConnectionPointsListComposite.isDisposed())
		{
			if(apogySystem != null)
			{
				apogySystemConnectionPointsListComposite.setConnectionPointsList(apogySystem.getConnectionPointsList());
			}
			else
			{
				apogySystemConnectionPointsListComposite.setConnectionPointsList(null);
			}
		}
	}		
}

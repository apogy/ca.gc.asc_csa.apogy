package ca.gc.asc_csa.apogy.core.ui.composites;

import javax.vecmath.Point3d;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.ibm.icu.text.DecimalFormat;

import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Matrix4x4;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dComposite;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter;
import ca.gc.asc_csa.apogy.core.PoseProvider;

public class DeltaPoseComposite extends Composite 
{
	private Adapter poseAdapter;	
	private ApogySystemApiAdapter variable1ApogySystemApiAdapter;
	private ApogySystemApiAdapter variable2ApogySystemApiAdapter;
	
	private Text textDistance;
	private Tuple3dComposite deltaDistanceComposite;
	private Tuple3dComposite deltaOrientationComposite;
	
	private DecimalFormat distanceFormat = new DecimalFormat("0.000");
		
	public DeltaPoseComposite(Composite parent, int style) {
		super(parent, style);		
		setLayout(new GridLayout(2, false));
		
		Label lblDistancem = new Label(this, SWT.NONE);
		lblDistancem.setAlignment(SWT.RIGHT);
		lblDistancem.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDistancem.setText("Distance (m):");
		
		textDistance = new Text(this, SWT.BORDER);
		textDistance.setToolTipText("Distance between the variables, in meters.");
		textDistance.setEditable(false);
		textDistance.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblPositionDistancem = new Label(this, SWT.NONE);
		lblPositionDistancem.setAlignment(SWT.RIGHT);
		lblPositionDistancem.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPositionDistancem.setText("Position Delta (m):");
		
		deltaDistanceComposite = new Tuple3dComposite(this, SWT.NONE);
		deltaDistanceComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		deltaDistanceComposite.setEnableEditing(false);
		
		Label lblOrientationDeltadeg = new Label(this, SWT.NONE);
		lblOrientationDeltadeg.setAlignment(SWT.RIGHT);
		lblOrientationDeltadeg.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblOrientationDeltadeg.setText("Orientation Delta (deg):");
		
		deltaOrientationComposite = new Tuple3dComposite(this, SWT.NONE, "0.000");
		deltaOrientationComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		deltaOrientationComposite.setEnableEditing(false);
		
		setDisplaysEnabled(false);
		
		addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) {				
				if(variable1ApogySystemApiAdapter != null) variable1ApogySystemApiAdapter.eAdapters().remove(getPoseAdapter());
				if(variable2ApogySystemApiAdapter != null) variable2ApogySystemApiAdapter.eAdapters().remove(getPoseAdapter());
			}
		});
	}

	public ApogySystemApiAdapter getVariable1ApogySystemApiAdapter() {
		return variable1ApogySystemApiAdapter;
	}

	public void setVariable1ApogySystemApiAdapter(ApogySystemApiAdapter apogySystemApiAdapter) 
	{
		if(this.variable1ApogySystemApiAdapter != null) this.variable1ApogySystemApiAdapter.eAdapters().remove(getPoseAdapter());
		
		this.variable1ApogySystemApiAdapter = apogySystemApiAdapter;
		
		if(this.variable1ApogySystemApiAdapter != null) this.variable1ApogySystemApiAdapter.eAdapters().add(getPoseAdapter());
						
		updatePose();
	}
	
	public ApogySystemApiAdapter getVariable2ApogySystemApiAdapter() {
		return variable2ApogySystemApiAdapter;
	}
	
	public void setVariable2ApogySystemApiAdapter(ApogySystemApiAdapter apogySystemApiAdapter) 
	{
		if(this.variable2ApogySystemApiAdapter != null) this.variable2ApogySystemApiAdapter.eAdapters().remove(getPoseAdapter());
		
		this.variable2ApogySystemApiAdapter = apogySystemApiAdapter;
		
		if(this.variable2ApogySystemApiAdapter != null) this.variable2ApogySystemApiAdapter.eAdapters().add(getPoseAdapter());
		
		updatePose();				
	}

	protected void setDisplaysEnabled(boolean enabled)
	{
		if(textDistance != null && !textDistance.isDisposed()) textDistance.setEnabled(enabled);
		if(deltaDistanceComposite != null && !deltaDistanceComposite.isDisposed()) deltaDistanceComposite.setEnabled(enabled);
		if(deltaOrientationComposite != null && !deltaOrientationComposite.isDisposed()) deltaOrientationComposite.setEnabled(enabled);		
	}
	
	protected void updatePose()
	{
		if(!isDisposed())
		{
			// Do this on UI Thread.									
			getDisplay().asyncExec(new Runnable() 
			{									
				@Override
				public void run() 
				{				
					setDisplaysEnabled(variable1ApogySystemApiAdapter != null && variable2ApogySystemApiAdapter != null);
					
					if(variable1ApogySystemApiAdapter != null && variable2ApogySystemApiAdapter != null)
					{
						Matrix4x4 variable1Pose = variable1ApogySystemApiAdapter.getPoseTransform();
						if(variable1Pose == null) variable1Pose = ApogyCommonMathFacade.INSTANCE.createIdentityMatrix4x4();
											
						Matrix4x4 variable2Pose = variable2ApogySystemApiAdapter.getPoseTransform();
						if(variable2Pose == null) variable2Pose = ApogyCommonMathFacade.INSTANCE.createIdentityMatrix4x4();
							
						Tuple3d variable1Position = ApogyCommonMathFacade.INSTANCE.extractPosition(variable1Pose);
						Point3d p1 = new Point3d(variable1Position.asTuple3d()); 
										
						Tuple3d variable2Position = ApogyCommonMathFacade.INSTANCE.extractPosition(variable2Pose);
						Point3d p2 = new Point3d(variable2Position.asTuple3d()); 
						
						// Updates Distance.
						double distance = p1.distance(p2);					
						if(textDistance != null && !textDistance.isDisposed()) textDistance.setText(distanceFormat.format(distance));
						
						double deltaX = p1.x - p2.x;
						double deltaY = p1.y - p2.y;
						double deltaZ = p1.z - p2.z;
						
						// Updates Delta Position.
						Tuple3d deltaPosition = ApogyCommonMathFacade.INSTANCE.createTuple3d(deltaX, deltaY, deltaZ);
						if(deltaDistanceComposite !=null && !deltaDistanceComposite.isDisposed()) deltaDistanceComposite.setTuple3d(deltaPosition);
						
						// TODO Updates Delta Orientation.
					
					}					
				}
			});
		}
	}
	
	protected Adapter getPoseAdapter()
	{
		if(poseAdapter == null)
		{
			poseAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{					
					if(msg.getNotifier() instanceof PoseProvider)
					{
						int featureID = msg.getFeatureID(PoseProvider.class);
						switch (featureID) 
						{
							case ApogyCorePackage.POSE_PROVIDER__POSE_TRANSFORM:
								if(msg.getNewValue() instanceof Matrix4x4)
								{																
									updatePose();
								}																								
							break;

							default:
							break;
						}												
					}					
				}
			};
		}
		
		return poseAdapter;
	}
}

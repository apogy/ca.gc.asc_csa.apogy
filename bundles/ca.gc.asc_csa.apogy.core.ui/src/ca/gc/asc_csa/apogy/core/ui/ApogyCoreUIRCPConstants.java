package ca.gc.asc_csa.apogy.core.ui;

public class ApogyCoreUIRCPConstants 
{
	
	/**
	 * PartIds for selectionBasedParts.
	 */
	public static final String PART__POSITIONED_RESULT_SEARCH__ID = "ca.gc.asc_csa.apogy.core.ui.parts.positionedResultSearchPart";	
	public static final String PART__VARIABLE_RELATIVE_POSE__ID = "ca.gc.asc_csa.apogy.core.ui.part.variableRelativePose";			
	
	/**
	 * VariableComparativePosePart persisted states.
	 */
	public static final String PERSISTED_STATE__VARIABLE_POSE_COMPARATIVE__DISTANCE_1 = "distance1";
	public static final String PERSISTED_STATE__VARIABLE_POSE_COMPARATIVE__DISTANCE_2 = "distance2";
	public static final String PERSISTED_STATE__VARIABLE_POSE_VARIABLE_FEATURE__REFERENCE_1 = "variableFeatureReference1";
	public static final String PERSISTED_STATE__VARIABLE_POSE_VARIABLE_FEATURE__REFERENCE_2 = "variableFeatureReference2";
		
	public static final String APOGY_SYSTEM_ID = "apogySystem";
	public static final String NAME_ID = "name";
			
	
}

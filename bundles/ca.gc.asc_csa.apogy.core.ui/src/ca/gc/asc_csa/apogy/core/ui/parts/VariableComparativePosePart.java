package ca.gc.asc_csa.apogy.core.ui.parts;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFactory;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;
import ca.gc.asc_csa.apogy.core.invocator.VariablesList;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIRCPConstants;
import ca.gc.asc_csa.apogy.core.ui.composites.ApogySystemPoseComparativeComposite;

public class VariableComparativePosePart extends AbstractSessionBasedPart 
{
	private double trip1Distance = 0.0;
	private double trip2Distance = 0.0;
	
	private ApogySystemPoseComparativeComposite apogySystemPoseComposite;
	
	private VariableFeatureReference variable1FeatureReference = ApogyCoreInvocatorFactory.eINSTANCE.createVariableFeatureReference();
	private VariableFeatureReference variable2FeatureReference = ApogyCoreInvocatorFactory.eINSTANCE.createVariableFeatureReference();
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{	
		if(invocatorSession != null)
		{
			if(apogySystemPoseComposite != null)
			{
				apogySystemPoseComposite.setVariableList(invocatorSession.getEnvironment().getVariablesList());			
				
				VariableFeatureReference vfr1 = (VariableFeatureReference) ApogyCoreInvocatorUIFacade.INSTANCE.readFromPersistedState(mPart, ApogyCoreUIRCPConstants.PERSISTED_STATE__VARIABLE_POSE_VARIABLE_FEATURE__REFERENCE_1);
				if(vfr1 != null)
				{
					this.variable1FeatureReference = vfr1;
					apogySystemPoseComposite.setVariableFeatureReference1(vfr1);
				}
				
				VariableFeatureReference vfr2 = (VariableFeatureReference) ApogyCoreInvocatorUIFacade.INSTANCE.readFromPersistedState(mPart, ApogyCoreUIRCPConstants.PERSISTED_STATE__VARIABLE_POSE_VARIABLE_FEATURE__REFERENCE_2);
				if(vfr2 != null)
				{
					this.variable2FeatureReference = vfr2;
					apogySystemPoseComposite.setVariableFeatureReference2(vfr2);
				}
			}
		}
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{		
		parent.setLayout(new FillLayout());
		
		VariablesList variables = null;
		
		if(ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() != null &&
		   ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment() != null)
		{
			ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment().getVariablesList();
		}
			
		apogySystemPoseComposite = new ApogySystemPoseComparativeComposite(parent, style, variables, variable1FeatureReference, variable2FeatureReference, trip1Distance, trip2Distance);
	}
	
	@Override
	public void userPostConstruct(MPart mPart) 
	{
		try
		{
			String distance1String = mPart.getPersistedState().get(ApogyCoreUIRCPConstants.PERSISTED_STATE__VARIABLE_POSE_COMPARATIVE__DISTANCE_1);								
			if(distance1String != null) trip1Distance = Double.parseDouble(distance1String);	
									
			String distance2String = mPart.getPersistedState().get(ApogyCoreUIRCPConstants.PERSISTED_STATE__VARIABLE_POSE_COMPARATIVE__DISTANCE_2);
			if(distance2String != null) trip2Distance = Double.parseDouble(distance2String);
				
			VariableFeatureReference vfr1 =  (VariableFeatureReference) ApogyCoreInvocatorUIFacade.INSTANCE.readFromPersistedState(mPart, ApogyCoreUIRCPConstants.PERSISTED_STATE__VARIABLE_POSE_VARIABLE_FEATURE__REFERENCE_1);
			if(vfr1 != null) variable1FeatureReference = vfr1;
			
			VariableFeatureReference vfr2 = (VariableFeatureReference) ApogyCoreInvocatorUIFacade.INSTANCE.readFromPersistedState(mPart, ApogyCoreUIRCPConstants.PERSISTED_STATE__VARIABLE_POSE_VARIABLE_FEATURE__REFERENCE_2);
			if(vfr2 != null) variable2FeatureReference = vfr2;
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
	}
	
	@Override
	public void userPersistState(MPart mPart)
	{				
		if(apogySystemPoseComposite != null)
		{
			mPart.getPersistedState().put(ApogyCoreUIRCPConstants.PERSISTED_STATE__VARIABLE_POSE_COMPARATIVE__DISTANCE_1, Double.toString(apogySystemPoseComposite.getTripDistance1()));	
			mPart.getPersistedState().put(ApogyCoreUIRCPConstants.PERSISTED_STATE__VARIABLE_POSE_COMPARATIVE__DISTANCE_2, Double.toString(apogySystemPoseComposite.getTripDistance2()));	
	
			ApogyCoreInvocatorUIFacade.INSTANCE.saveToPersistedState(mPart, ApogyCoreUIRCPConstants.PERSISTED_STATE__VARIABLE_POSE_VARIABLE_FEATURE__REFERENCE_1, variable1FeatureReference);
			ApogyCoreInvocatorUIFacade.INSTANCE.saveToPersistedState(mPart, ApogyCoreUIRCPConstants.PERSISTED_STATE__VARIABLE_POSE_VARIABLE_FEATURE__REFERENCE_2, variable2FeatureReference);			
		}
	}
}

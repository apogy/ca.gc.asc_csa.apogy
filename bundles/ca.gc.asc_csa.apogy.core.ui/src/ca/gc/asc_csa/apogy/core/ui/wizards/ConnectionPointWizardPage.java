package ca.gc.asc_csa.apogy.core.ui.wizards;

import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.GroupNode;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.NodeSearchComposite;
import ca.gc.asc_csa.apogy.common.topology.ui.dialogs.NodeSelectionDialog;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.ConnectionPoint;

public class ConnectionPointWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.ui.wizards.ConnectionPointWizardPage";
	
	private ApogySystem apogySystem;
	private ConnectionPoint connectionPoint;
	
	private Text txtNodeID;
	private Button btnSelectNode;
	
	public ConnectionPointWizardPage(ApogySystem apogySystem, ConnectionPoint connectionPoint) 
	{
		super(WIZARD_PAGE_ID);
		this.apogySystem = apogySystem;
		this.connectionPoint = connectionPoint;
		
		setTitle("Connection Point refered Node");
		setDescription("Set the Connection Point refered Node in the Apogy System Topology.");				
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(3, false));

		Label label = new Label(container, SWT.NONE);
		label.setText("Refered Node ID : ");
		
		txtNodeID = new Text(container, SWT.BORDER);
		txtNodeID.setEditable(false);
		GridData gd_lblFromNodeID = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblFromNodeID.minimumWidth = 250;
		gd_lblFromNodeID.widthHint = 250;
		txtNodeID.setLayoutData(gd_lblFromNodeID);
		
		btnSelectNode = new Button(container, SWT.NONE);
		btnSelectNode.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnSelectNode.setText("Select");
		btnSelectNode.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				// Opens the Node Selection Dialog.
				Node root = apogySystem.getTopologyRoot().getOriginNode();
				NodeSelectionDialog nodeSelectionDialog = new NodeSelectionDialog(getShell(), root)
				{
					@Override
					protected NodeSearchComposite createNodeSearchComposite(Composite parent) 
					{
						NodeSearchComposite nodeSearchComposite = new NodeSearchComposite(parent, SWT.NONE, root)
						{
							@Override
							public void nodeSelectedChanged(Node nodeSelected) 
							{
								setSelectedNode(nodeSelected);
							}
						};
						
						nodeSearchComposite.setTypeFilter(ApogyCommonTopologyPackage.Literals.GROUP_NODE);
						
						return nodeSearchComposite;
					}
				};
				
				if(nodeSelectionDialog.open() == Window.OK)		
				{
					Node fromNode = nodeSelectionDialog.getSelectedNode();
					
					if(fromNode instanceof GroupNode)
					{
						GroupNode selectedNode = (GroupNode) fromNode;
						
						if(ApogyCommonTransactionFacade.INSTANCE.areEditingDomainsValid(connectionPoint, ApogyCorePackage.Literals.CONNECTION_POINT__NODE, selectedNode, false) == ApogyCommonTransactionFacade.EXECUTE_COMMAND_ON_OWNER_DOMAIN)
						{
							ApogyCommonTransactionFacade.INSTANCE.basicSet(connectionPoint, ApogyCorePackage.Literals.CONNECTION_POINT__NODE, fromNode);							
						}
						else
						{
							connectionPoint.setNode(selectedNode);						
						}
						
						setErrorMessage(null);
						txtNodeID.setText(selectedNode.getNodeId());
						txtNodeID.setToolTipText(selectedNode.getNodeId());
					}										
				}
				
				if(connectionPoint.getNode() == null) setErrorMessage("No Node Selected !");
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) 
			{			
			}
		});
	
		setControl(container);	
		
		setErrorMessage("No Node Selected !");
	}	
		
	protected void validate()
	{		
		setPageComplete(connectionPoint != null);
	}
}

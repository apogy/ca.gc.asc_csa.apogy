package ca.gc.asc_csa.apogy.core.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.FeatureOfInterest;
import ca.gc.asc_csa.apogy.core.FeatureOfInterestList;
import ca.gc.asc_csa.apogy.core.ui.Activator;


public class FeatureOfInterestListComposite extends Composite 
{	
	private FeatureOfInterestList featureOfInterestList;
	
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;	
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;
	
	public FeatureOfInterestListComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(2, false));
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_tree.widthHint = 300;
		gd_tree.minimumWidth = 300;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CompositeFilterContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newFeatureOfInterestSelected((FeatureOfInterest)((IStructuredSelection) event.getSelection()).getFirstElement());					
			}
		});
		
		// Buttons.
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(composite, SWT.NONE);
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{
				if (event.type == SWT.Selection) 
				{					
					Wizard wizard = new ApogyEObjectWizard(ApogyCorePackage.Literals.FEATURE_OF_INTEREST_LIST__FEATURES_OF_INTEREST, getFeatureOfInterestList(), null, ApogyCorePackage.Literals.FEATURE_OF_INTEREST);								
					WizardDialog dialog = new WizardDialog(getShell(), wizard);
					dialog.open();
										
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(getFeatureOfInterestList());
					}
				}		
			}
		});
				
		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				String foisToDeleteMessage = "";

				Iterator<FeatureOfInterest> foiList = getSelectedFeatureOfInterests().iterator();
				while (foiList.hasNext()) 
				{
					FeatureOfInterest foi = foiList.next();
					foisToDeleteMessage = foisToDeleteMessage + foi.getName();

					if (foiList.hasNext()) 
					{
						foisToDeleteMessage = foisToDeleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected Feature of Interests (FOI)", null,
						"Are you sure to delete these FOIs: " + foisToDeleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					for (FeatureOfInterest foi :  getSelectedFeatureOfInterests()) 
					{
						try 
						{
							ApogyCommonTransactionFacade.INSTANCE.basicRemove(foi.eContainer(), ApogyCorePackage.Literals.FEATURE_OF_INTEREST_LIST__FEATURES_OF_INTEREST, foi);
						} 
						catch (Exception e)	
						{
							Logger.INSTANCE.log(Activator.ID,
									"Unable to delete the FOI <"+ foi.getName() + ">",
									EventSeverity.ERROR, e);
						}
					}
				}
				
				// Forces the viewer to refresh its input.
				if(!treeViewer.isBusy())
				{					
					treeViewer.setInput(getFeatureOfInterestList());
				}								
			}
		});
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if(m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
	
	public FeatureOfInterestList getFeatureOfInterestList() {
		return featureOfInterestList;
	}

	public void setFeatureOfInterestList(FeatureOfInterestList newFeatureOfInterestList) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.featureOfInterestList = newFeatureOfInterestList;
		
		if(newFeatureOfInterestList != null)
		{
			m_bindingContext = customInitDataBindings();
			treeViewer.setInput(newFeatureOfInterestList);
		}
	}

	@SuppressWarnings("unchecked")
	public List<FeatureOfInterest> getSelectedFeatureOfInterests()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	protected void newFeatureOfInterestSelected(FeatureOfInterest newFeatureOfInterest)
	{		
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		return bindingContext;
	}
	
	private class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof FeatureOfInterestList)
			{								
				FeatureOfInterestList featureOfInterestList = (FeatureOfInterestList) inputElement;		
				return featureOfInterestList.getFeaturesOfInterest().toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof FeatureOfInterestList)
			{								
				FeatureOfInterestList featureOfInterestList = (FeatureOfInterestList) parentElement;		
				return featureOfInterestList.getFeaturesOfInterest().toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof FeatureOfInterestList)
			{
				FeatureOfInterestList featureOfInterestList = (FeatureOfInterestList) element;		
				return !featureOfInterestList.getFeaturesOfInterest().isEmpty();
			}		
			else
			{
				return false;
			}
		}			
	}
}

package ca.gc.asc_csa.apogy.core.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;
import ca.gc.asc_csa.apogy.core.invocator.VariablesList;

public class ApogySystemPoseComparativeComposite extends Composite 
{		
	private ApogySystemPoseComposite variable1ApogySystemPoseComposite;
	private ApogySystemPoseComposite variable2ApogySystemPoseComposite;
	
	private DeltaPoseComposite deltaPoseComposite;
	
	public ApogySystemPoseComparativeComposite(Composite parent, int style) 
	{
		this(parent, style, null, null, null, 0,0);
	}
	
	public ApogySystemPoseComparativeComposite(Composite parent, int style,  VariablesList variablesList, VariableFeatureReference variable1FeatureReference ,VariableFeatureReference variable2FeatureReference, double trip1Distance, double trip2Distance) 
	{		
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Group grpVariable1 = new Group(this, SWT.BORDER);
		grpVariable1.setText("Variable 1");
		grpVariable1.setLayout(new GridLayout(1, false));
		
		variable1ApogySystemPoseComposite = new ApogySystemPoseComposite(grpVariable1, SWT.NONE, variablesList, variable1FeatureReference, trip1Distance)
		{
			@Override
			protected void updateApogySystemApiAdapter(ApogySystemApiAdapter oldApogySystemApiAdapter, ApogySystemApiAdapter newApogySystemApiAdapter) 
			{
				super.updateApogySystemApiAdapter(oldApogySystemApiAdapter, newApogySystemApiAdapter);
				deltaPoseComposite.setVariable1ApogySystemApiAdapter(newApogySystemApiAdapter);
			}
		};
	
		Group grpVariable2 = new Group(this, SWT.BORDER);
		grpVariable2.setText("Variable 2");
		grpVariable2.setLayout(new GridLayout(1, false));
		
		variable2ApogySystemPoseComposite = new ApogySystemPoseComposite(grpVariable2, SWT.NONE, variablesList, variable2FeatureReference, trip2Distance)
		{
			@Override
			protected void updateApogySystemApiAdapter(ApogySystemApiAdapter oldApogySystemApiAdapter, ApogySystemApiAdapter newApogySystemApiAdapter) 
			{
				super.updateApogySystemApiAdapter(oldApogySystemApiAdapter, newApogySystemApiAdapter);
				deltaPoseComposite.setVariable2ApogySystemApiAdapter(newApogySystemApiAdapter);
			}
		};
		
		Group grpRelative = new Group(this, SWT.NONE);
		grpRelative.setText("Relative Pose");
		grpRelative.setLayout(new GridLayout(1, false));
	
		deltaPoseComposite = new DeltaPoseComposite(grpRelative, SWT.BORDER);
	}	
	
	public void setVariableFeatureReference1(VariableFeatureReference newVariableFeatureReference) 
	{
		if(variable1ApogySystemPoseComposite != null && !variable1ApogySystemPoseComposite.isDisposed())
		{
			variable1ApogySystemPoseComposite.setVariableFeatureReference(newVariableFeatureReference);
		}
	}

	public void setVariableFeatureReference2(VariableFeatureReference newVariableFeatureReference) 
	{
		if(variable2ApogySystemPoseComposite != null && !variable2ApogySystemPoseComposite.isDisposed())
		{
			variable2ApogySystemPoseComposite.setVariableFeatureReference(newVariableFeatureReference);
		}
	}
	
	public void setVariableList(final VariablesList variablesList) 
	{
		// Do this on UI Thread.									
		getDisplay().asyncExec(new Runnable() 
		{									
			@Override
			public void run() 
			{										
				variable1ApogySystemPoseComposite.setVariableList(variablesList);
				variable2ApogySystemPoseComposite.setVariableList(variablesList);
			}
		});		
	}	
	
	public double getTripDistance1() 
	{
		if(variable1ApogySystemPoseComposite != null && !variable1ApogySystemPoseComposite.isDisposed())
		{
			return variable1ApogySystemPoseComposite.getTripDistance();
		}
		else
		{
			return 0;
		}
	}

	public void setTripDistance1(double tripDistance) 
	{
		if(variable1ApogySystemPoseComposite != null && !variable1ApogySystemPoseComposite.isDisposed())
		{
			variable1ApogySystemPoseComposite.setTripDistance(tripDistance);
		}	
	}

	public double getTripDistance2() 
	{
		if(variable2ApogySystemPoseComposite != null && !variable2ApogySystemPoseComposite.isDisposed())
		{
			return variable2ApogySystemPoseComposite.getTripDistance();
		}
		else
		{
			return 0;
		}
	}

	public void setTripDistance2(double tripDistance) 
	{
		if(variable2ApogySystemPoseComposite != null && !variable2ApogySystemPoseComposite.isDisposed())
		{
			variable2ApogySystemPoseComposite.setTripDistance(tripDistance);
		}	
	}
	
}

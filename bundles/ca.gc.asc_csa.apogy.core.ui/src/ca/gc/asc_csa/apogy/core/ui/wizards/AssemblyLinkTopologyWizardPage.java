/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.ui.wizards;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.ui.composites.TopologyTreeComposite;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.AssemblyLink;

public class AssemblyLinkTopologyWizardPage extends AbstractAssemblyLinkWizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.ui.wizards.AssemblyLinkTopologyWizardPage";	
	private TopologyTreeComposite topologyTreeComposite;
		
	public AssemblyLinkTopologyWizardPage(ApogySystem apogySystem, AssemblyLink assemblyLink) 
	{
		super(WIZARD_PAGE_ID, apogySystem, assemblyLink);	
		
		setTitle("Assembly link topology");
		setDescription("Define the link topology.");				
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{	
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(1, true));
		setControl(container);	
		
		topologyTreeComposite = new TopologyTreeComposite(container, SWT.NONE, true);
		GridData gd_topologyTreeComposite = new GridData(SWT.FILL, SWT.FILL, true, true,1,1);
		topologyTreeComposite.setLayoutData(gd_topologyTreeComposite);
		
		topologyTreeComposite.setRoot(assemblyLink.getGeometryNode());
	}
	
	protected void validate()
	{			
		setErrorMessage(null);		
		setPageComplete(getErrorMessage() == null);
	}
}

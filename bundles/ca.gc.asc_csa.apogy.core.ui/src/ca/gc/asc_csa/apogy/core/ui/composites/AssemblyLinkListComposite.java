package ca.gc.asc_csa.apogy.core.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.AssemblyLink;
import ca.gc.asc_csa.apogy.core.AssemblyLinksList;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIRCPConstants;

public class AssemblyLinkListComposite extends Composite 
{
	private AssemblyLinksList assemblyLinksList;
	
	private Tree tree;
	private TreeViewer treeViewer;
	
	private Button btnNew;
	private Button btnDelete;	
	
	private DataBindingContext m_bindingContext;
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	public AssemblyLinkListComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, false));
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new AssemblyLinksListContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				Object object = ((IStructuredSelection) event.getSelection()).getFirstElement();
				if(object instanceof AssemblyLink)
				{
					AssemblyLink selectedAssemblyLink = (AssemblyLink) object; 
					newAssemblyLinkSelected(selectedAssemblyLink);
				}
			}
		});
		
		// Buttons.
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(composite, SWT.NONE);
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.setToolTipText("Create a new Assembly Link.");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{				
				if (event.type == SWT.Selection) 
				{					
					MapBasedEClassSettings settings = ApogyCommonEMFUIFactory.eINSTANCE.createMapBasedEClassSettings();					
					String name = ApogyCommonEMFFacade.INSTANCE.getDefaultName(getAssemblyLinksList(), null, ApogyCorePackage.Literals.ASSEMBLY_LINKS_LIST__ASSEMBLY_LINKS);
					
					settings.getUserDataMap().put(ApogyCoreUIRCPConstants.NAME_ID, name);
					ApogySystem apogySystem = (ApogySystem) getAssemblyLinksList().eContainer();
					
					settings.getUserDataMap().put(ApogyCoreUIRCPConstants.APOGY_SYSTEM_ID, apogySystem);					
											
					Wizard wizard = new ApogyEObjectWizard(ApogyCorePackage.Literals.ASSEMBLY_LINKS_LIST__ASSEMBLY_LINKS, getAssemblyLinksList(), settings, ApogyCorePackage.Literals.ASSEMBLY_LINK); 
					WizardDialog dialog = new WizardDialog(getShell(), wizard);
					dialog.open();
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.refresh();
					}					
				}
			}
		});
				
		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setToolTipText("Deletes the selected Assembly Link(s).");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				String deleteMessage = "";

				Iterator<AssemblyLink> assemblyLinkIterator = getSelectedAssemblyLinks().iterator();
				while (assemblyLinkIterator.hasNext()) 
				{
					AssemblyLink assemblyLink = assemblyLinkIterator.next();
					deleteMessage = deleteMessage + assemblyLink.getName();

					if (assemblyLinkIterator.hasNext()) 
					{
						deleteMessage = deleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected Assembly Link(s)", null,
						"Are you sure to delete these Assembly Link(s) : " + deleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					List<AssemblyLink> toRemove = getSelectedAssemblyLinks();
					ApogyCommonTransactionFacade.INSTANCE.basicRemove(getAssemblyLinksList(), ApogyCorePackage.Literals.ASSEMBLY_LINKS_LIST__ASSEMBLY_LINKS, toRemove);
				}
				
				// Forces the viewer to refresh its input.
				if(!treeViewer.isBusy())
				{					
					treeViewer.refresh();
				}					
			}
		});
	
		
		// Cleanup on dispose
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (m_bindingContext != null) m_bindingContext.dispose();			
			}
		});
	}

	public AssemblyLinksList getAssemblyLinksList() {
		return assemblyLinksList;
	}

	public void setAssemblyLinksList(AssemblyLinksList newAssemblyLinksList) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.assemblyLinksList = newAssemblyLinksList;
		
		if(newAssemblyLinksList != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}	
		
		treeViewer.setInput(newAssemblyLinksList);				
	}
	
	@SuppressWarnings("unchecked")
	public List<AssemblyLink> getSelectedAssemblyLinks()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	protected void newAssemblyLinkSelected(AssemblyLink selectedAssemblyLink)
	{		
	}	
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		return bindingContext;
	}
	
	private class AssemblyLinksListContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{						
			if(inputElement instanceof AssemblyLinksList)
			{
				AssemblyLinksList assemblyLinksList = (AssemblyLinksList) inputElement;
				return assemblyLinksList.getAssemblyLinks().toArray();
			}
				
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof AssemblyLinksList)
			{
				AssemblyLinksList assemblyLinksList = (AssemblyLinksList) parentElement;
				return assemblyLinksList.getAssemblyLinks().toArray();
			}					
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof AssemblyLinksList)
			{
				AssemblyLinksList assemblyLinksList = (AssemblyLinksList) element;
				return !assemblyLinksList.getAssemblyLinks().isEmpty();
			}			
			else
			{
				return false;
			}
		}
	}
}

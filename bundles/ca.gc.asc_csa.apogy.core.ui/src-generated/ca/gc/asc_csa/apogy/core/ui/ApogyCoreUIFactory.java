package ca.gc.asc_csa.apogy.core.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIPackage
 * @generated
 */
public interface ApogyCoreUIFactory extends EFactory
{
  /**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  ApogyCoreUIFactory eINSTANCE = ca.gc.asc_csa.apogy.core.ui.impl.ApogyCoreUIFactoryImpl.init();

  /**
	 * Returns a new object of class '<em>Facade</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Facade</em>'.
	 * @generated
	 */
	ApogyCoreUIFacade createApogyCoreUIFacade();

		/**
	 * Returns a new object of class '<em>Result Node Presentation</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @return a new object of class '<em>Result Node Presentation</em>'.
	 * @generated
	 */
  ResultNodePresentation createResultNodePresentation();

  /**
	 * Returns a new object of class '<em>Feature Of Interest UI Settings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Feature Of Interest UI Settings</em>'.
	 * @generated
	 */
	FeatureOfInterestUISettings createFeatureOfInterestUISettings();

		/**
	 * Returns a new object of class '<em>Feature Of Interest Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Feature Of Interest Wizard Pages Provider</em>'.
	 * @generated
	 */
	FeatureOfInterestWizardPagesProvider createFeatureOfInterestWizardPagesProvider();

		/**
	 * Returns a new object of class '<em>Connection Point Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Connection Point Wizard Pages Provider</em>'.
	 * @generated
	 */
	ConnectionPointWizardPagesProvider createConnectionPointWizardPagesProvider();

		/**
	 * Returns a new object of class '<em>Assembly Link Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assembly Link Wizard Pages Provider</em>'.
	 * @generated
	 */
	AssemblyLinkWizardPagesProvider createAssemblyLinkWizardPagesProvider();

		/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyCoreUIPackage getApogyCoreUIPackage();

} //ApogyCoreUIFactory

/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.core.ApogyCoreFactory;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.ConnectionPoint;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIPackage;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIRCPConstants;
import ca.gc.asc_csa.apogy.core.ui.ConnectionPointWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.ui.wizards.ConnectionPointWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connection Point Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ConnectionPointWizardPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements ConnectionPointWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionPointWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreUIPackage.Literals.CONNECTION_POINT_WIZARD_PAGES_PROVIDER;
	}

	
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		ConnectionPoint connectionPoint = ApogyCoreFactory.eINSTANCE.createConnectionPoint();		
		
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings = (MapBasedEClassSettings) settings;
			
			connectionPoint.setName((String) mapBasedEClassSettings.getUserDataMap().get(ApogyCoreUIRCPConstants.NAME_ID));
		}
		return connectionPoint;
	};
	
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		ApogySystem apogySystem = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings = (MapBasedEClassSettings) settings;
			apogySystem = (ApogySystem) mapBasedEClassSettings.getUserDataMap().get(ApogyCoreUIRCPConstants.APOGY_SYSTEM_ID);
		}
		
		ConnectionPoint connectionPoint = (ConnectionPoint) eObject;
		list.add(new ConnectionPointWizardPage(apogySystem, connectionPoint));
				
		return list;
	}
	
} //ConnectionPointWizardPagesProviderImpl

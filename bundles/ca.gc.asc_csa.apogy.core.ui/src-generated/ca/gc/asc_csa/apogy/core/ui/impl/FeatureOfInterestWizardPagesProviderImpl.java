/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.core.ApogyCoreFactory;
import ca.gc.asc_csa.apogy.core.FeatureOfInterest;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIPackage;
import ca.gc.asc_csa.apogy.core.ui.FeatureOfInterestUISettings;
import ca.gc.asc_csa.apogy.core.ui.FeatureOfInterestWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.ui.wizards.FeatureOfInterestWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Of Interest Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FeatureOfInterestWizardPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements FeatureOfInterestWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public FeatureOfInterestWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreUIPackage.Literals.FEATURE_OF_INTEREST_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		FeatureOfInterest foi = (FeatureOfInterest) eObject;
		list.add(new FeatureOfInterestWizardPage(foi));
				
		return list;
	}
	
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		FeatureOfInterest foi = ApogyCoreFactory.eINSTANCE.createFeatureOfInterest();
		foi.setPose(ApogyCommonMathFacade.INSTANCE.createIdentityMatrix4x4());
		
		if(settings instanceof FeatureOfInterestUISettings)
		{
			FeatureOfInterestUISettings featureOfInterestUISettings = (FeatureOfInterestUISettings) settings;
			foi.setName(featureOfInterestUISettings.getName());
		}
		return foi;
	};
} //FeatureOfInterestWizardPagesProviderImpl

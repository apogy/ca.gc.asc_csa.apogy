/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.topology.AggregateGroupNode;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFactory;
import ca.gc.asc_csa.apogy.core.ApogyCoreFactory;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.AssemblyLink;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIPackage;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIRCPConstants;
import ca.gc.asc_csa.apogy.core.ui.AssemblyLinkWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.ui.wizards.AssemblyLinkDestinationWizardPage;
import ca.gc.asc_csa.apogy.core.ui.wizards.AssemblyLinkSourceWizardPage;
import ca.gc.asc_csa.apogy.core.ui.wizards.AssemblyLinkTopologyWizardPage;
import ca.gc.asc_csa.apogy.core.ui.wizards.AssemblyLinkTransformWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assembly Link Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AssemblyLinkWizardPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements AssemblyLinkWizardPagesProvider 
{
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public AssemblyLinkWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreUIPackage.Literals.ASSEMBLY_LINK_WIZARD_PAGES_PROVIDER;
	}
	
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		AssemblyLink assemblyLink = ApogyCoreFactory.eINSTANCE.createAssemblyLink();
		assemblyLink.setTransformationMatrix(ApogyCommonMathFacade.INSTANCE.createIdentityMatrix4x4());
		
		AggregateGroupNode node = ApogyCommonTopologyFactory.eINSTANCE.createAggregateGroupNode();		
		assemblyLink.setGeometryNode(node);
		
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings = (MapBasedEClassSettings) settings;
			
			assemblyLink.setName((String) mapBasedEClassSettings.getUserDataMap().get(ApogyCoreUIRCPConstants.NAME_ID));
		}
		return assemblyLink;
	};
	
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		ApogySystem apogySystem = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings = (MapBasedEClassSettings) settings;
			apogySystem = (ApogySystem) mapBasedEClassSettings.getUserDataMap().get(ApogyCoreUIRCPConstants.APOGY_SYSTEM_ID);
		}
		
		AssemblyLink assemblyLink = (AssemblyLink) eObject;
		
		// Add other pages.
		list.add(new AssemblyLinkSourceWizardPage(apogySystem, assemblyLink));
		list.add(new AssemblyLinkDestinationWizardPage(apogySystem, assemblyLink));	
		list.add(new AssemblyLinkTransformWizardPage(apogySystem, assemblyLink));
		list.add(new AssemblyLinkTopologyWizardPage(apogySystem, assemblyLink));
		
		return list;
	}
} //AssemblyLinkWizardPagesProviderImpl

package ca.gc.asc_csa.apogy.core.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.core.FeatureOfInterest;
import ca.gc.asc_csa.apogy.core.FeatureOfInterestNode;
import ca.gc.asc_csa.apogy.core.ui.impl.ApogyCoreUIFacadeImpl;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Facade</b></em>'. <!-- end-user-doc --> *
 *
 * @see ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIPackage#getApogyCoreUIFacade()
 * @model
 * @generated
 */
public interface ApogyCoreUIFacade extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * Returns the FeatureOfInterestNode associated with a specified FeatureOfInterest.
	 * @param featureOfInterest The specified FeatureOfInterest.
	 * @return The FeatureOfInterestNode associated with the specified FeatureOfInterest, null is none is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" featureOfInterestUnique="false"
	 * @generated
	 */
	FeatureOfInterestNode getFeatureOfInterestNode(FeatureOfInterest featureOfInterest);

	/**
	 * @generated_NOT
	 */
	public static ApogyCoreUIFacade INSTANCE = ApogyCoreUIFacadeImpl
			.getInstance();
} // ApogyCoreUIFacade

package ca.gc.asc_csa.apogy.common.geometry.data3d;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import ca.gc.asc_csa.apogy.common.geometry.data.MeshPolygonShapesSampler;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cartesian Triangular Mesh Polygon Sampler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A MeshPolygonShapesSampler used to sample CartesianPositionCoordinates triangular meshes.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage#getCartesianTriangularMeshPolygonSampler()
 * @model
 * @generated
 */
public interface CartesianTriangularMeshPolygonSampler extends MeshPolygonShapesSampler<CartesianPositionCoordinates, CartesianTriangle> {
} // CartesianTriangularMeshPolygonSampler

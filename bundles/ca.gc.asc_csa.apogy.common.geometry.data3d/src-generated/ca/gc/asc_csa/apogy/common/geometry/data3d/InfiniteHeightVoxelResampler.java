/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.geometry.data3d;

import ca.gc.asc_csa.apogy.common.processors.Processor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Infinite Height Voxel Resampler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Voxel resampler that uses cell of infinite height. Cells are defined in the XY plane and are square. The height is defined along Z.
 * This resampler includes two zones (short and long range) defined by two radius.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getFilterType <em>Filter Type</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getMinimumNumberOfPointPerVoxel <em>Minimum Number Of Point Per Voxel</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getShortRangeLimit <em>Short Range Limit</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getShortRangeResolution <em>Short Range Resolution</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getLongRangeLimit <em>Long Range Limit</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getLongRangeResolution <em>Long Range Resolution</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage#getInfiniteHeightVoxelResampler()
 * @model
 * @generated
 */
public interface InfiniteHeightVoxelResampler extends Processor<CartesianCoordinatesSet, CartesianCoordinatesSet> {
	/**
	 * Returns the value of the '<em><b>Filter Type</b></em>' attribute.
	 * The literals are from the enumeration {@link ca.gc.asc_csa.apogy.common.geometry.data3d.VoxelFilterType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The filtering type to be used.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Filter Type</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.VoxelFilterType
	 * @see #setFilterType(VoxelFilterType)
	 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage#getInfiniteHeightVoxelResampler_FilterType()
	 * @model unique="false"
	 * @generated
	 */
	VoxelFilterType getFilterType();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getFilterType <em>Filter Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter Type</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.VoxelFilterType
	 * @see #getFilterType()
	 * @generated
	 */
	void setFilterType(VoxelFilterType value);

	/**
	 * Returns the value of the '<em><b>Minimum Number Of Point Per Voxel</b></em>' attribute.
	 * The default value is <code>"2"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Minimum number of points in a voxel to be valid.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Minimum Number Of Point Per Voxel</em>' attribute.
	 * @see #setMinimumNumberOfPointPerVoxel(int)
	 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage#getInfiniteHeightVoxelResampler_MinimumNumberOfPointPerVoxel()
	 * @model default="2" unique="false"
	 * @generated
	 */
	int getMinimumNumberOfPointPerVoxel();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getMinimumNumberOfPointPerVoxel <em>Minimum Number Of Point Per Voxel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minimum Number Of Point Per Voxel</em>' attribute.
	 * @see #getMinimumNumberOfPointPerVoxel()
	 * @generated
	 */
	void setMinimumNumberOfPointPerVoxel(int value);

	/**
	 * Returns the value of the '<em><b>Short Range Limit</b></em>' attribute.
	 * The default value is <code>"4.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The maximum distance in the short range."
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Short Range Limit</em>' attribute.
	 * @see #setShortRangeLimit(double)
	 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage#getInfiniteHeightVoxelResampler_ShortRangeLimit()
	 * @model default="4.0" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'"
	 * @generated
	 */
	double getShortRangeLimit();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getShortRangeLimit <em>Short Range Limit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Short Range Limit</em>' attribute.
	 * @see #getShortRangeLimit()
	 * @generated
	 */
	void setShortRangeLimit(double value);

	/**
	 * Returns the value of the '<em><b>Short Range Resolution</b></em>' attribute.
	 * The default value is <code>"0.05"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The voxel size in the short range.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Short Range Resolution</em>' attribute.
	 * @see #setShortRangeResolution(double)
	 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage#getInfiniteHeightVoxelResampler_ShortRangeResolution()
	 * @model default="0.05" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'"
	 * @generated
	 */
	double getShortRangeResolution();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getShortRangeResolution <em>Short Range Resolution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Short Range Resolution</em>' attribute.
	 * @see #getShortRangeResolution()
	 * @generated
	 */
	void setShortRangeResolution(double value);

	/**
	 * Returns the value of the '<em><b>Long Range Limit</b></em>' attribute.
	 * The default value is <code>"4.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The maximum distance in the long range.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Long Range Limit</em>' attribute.
	 * @see #setLongRangeLimit(double)
	 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage#getInfiniteHeightVoxelResampler_LongRangeLimit()
	 * @model default="4.0" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'"
	 * @generated
	 */
	double getLongRangeLimit();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getLongRangeLimit <em>Long Range Limit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Long Range Limit</em>' attribute.
	 * @see #getLongRangeLimit()
	 * @generated
	 */
	void setLongRangeLimit(double value);

	/**
	 * Returns the value of the '<em><b>Long Range Resolution</b></em>' attribute.
	 * The default value is <code>"0.25"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The voxel size in the long range.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Long Range Resolution</em>' attribute.
	 * @see #setLongRangeResolution(double)
	 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage#getInfiniteHeightVoxelResampler_LongRangeResolution()
	 * @model default="0.25" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'"
	 * @generated
	 */
	double getLongRangeResolution();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler#getLongRangeResolution <em>Long Range Resolution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Long Range Resolution</em>' attribute.
	 * @see #getLongRangeResolution()
	 * @generated
	 */
	void setLongRangeResolution(double value);

} // InfiniteHeightVoxelResampler

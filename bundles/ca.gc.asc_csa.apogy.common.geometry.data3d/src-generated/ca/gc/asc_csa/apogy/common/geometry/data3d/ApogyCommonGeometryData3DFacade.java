package ca.gc.asc_csa.apogy.common.geometry.data3d;
/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/

import java.util.List;

import javax.vecmath.Matrix4d;
import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.common.geometry.data3d.impl.ApogyCommonGeometryData3DFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data3d Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage#getApogyCommonGeometryData3DFacade()
 * @model
 * @generated
 */
public interface ApogyCommonGeometryData3DFacade extends EObject {
	
	public static ApogyCommonGeometryData3DFacade INSTANCE = ApogyCommonGeometryData3DFacadeImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianPositionCoordinates for given coordinates.
	 * @param x The x coordinates value.
	 * @param y The y coordinates value.
	 * @param z The z coordinates value.
	 * @return The CartesianPositionCoordinates created.
	 * <!-- end-model-doc -->
	 * @model unique="false" xUnique="false" yUnique="false" zUnique="false"
	 * @generated
	 */
	CartesianPositionCoordinates createCartesianPositionCoordinates(double x, double y, double z);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a ColoredCartesianPositionCoordinates for given coordinates and color.
	 * @param x The x coordinates value.
	 * @param y The y coordinates value.
	 * @param z The z coordinates value.
	 * @param red The value of the red color component, between 0 and 255.
	 * @param green The value of the green color component, between 0 and 255.
	 * @param blue The value of the blue color component, between 0 and 255.
	 * @return The ColoredCartesianPositionCoordinates created.
	 * <!-- end-model-doc -->
	 * @model unique="false" xUnique="false" yUnique="false" zUnique="false" redUnique="false" greenUnique="false" blueUnique="false"
	 * @generated
	 */
	ColoredCartesianPositionCoordinates createColoredCartesianPositionCoordinates(double x, double y, double z, short red, short green, short blue);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a ColoredCartesianPositionCoordinates for given coordinates and color, including alpha.
	 * @param x The x coordinates value.
	 * @param y The y coordinates value.
	 * @param z The z coordinates value.
	 * @param alpha The alpha value, between 0 (transparent) to 255 (opaque).
	 * @param red The value of the red color component, between 0 and 255.
	 * @param green The value of the green color component, between 0 and 255.
	 * @param blue The value of the blue color component, between 0 and 255.
	 * @return The ColoredCartesianPositionCoordinates created.
	 * <!-- end-model-doc -->
	 * @model unique="false" xUnique="false" yUnique="false" zUnique="false" alphaUnique="false" redUnique="false" greenUnique="false" blueUnique="false"
	 * @generated
	 */
	ColoredCartesianPositionCoordinates createColoredCartesianPositionCoordinates(double x, double y, double z, short alpha, short red, short green, short blue);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates an RGBAColor given color components, including alpha.
	 * @param alpha The alpha value, between 0 (transparent) to 255 (opaque).
	 * @param red The value of the red color component, between 0 and 255.
	 * @param green The value of the green color component, between 0 and 255.
	 * @param blue The value of the blue color component, between 0 and 255.
	 * @return The RGBAColor.
	 * <!-- end-model-doc -->
	 * @model unique="false" alphaUnique="false" redUnique="false" greenUnique="false" blueUnique="false"
	 * @generated
	 */
	RGBAColor createRGBAColor(short alpha, short red, short green, short blue);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates an CartesianOrientationCoordinates given rotation values.
	 * @param xRotation Rotation about X, in radians.
	 * @param yRotation Rotation about Y, in radians.
	 * @param zRotation Rotation about Z, in radians.
	 * @return The CartesianOrientationCoordinates.
	 * <!-- end-model-doc -->
	 * @model unique="false" xRotationUnique="false"
	 *        xRotationAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" yRotationUnique="false"
	 *        yRotationAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" zRotationUnique="false"
	 *        zRotationAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'"
	 * @generated
	 */
	CartesianOrientationCoordinates createCartesianOrientationCoordinates(double xRotation, double yRotation, double zRotation);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a SphericalCoordinates given its coordinates.
	 * @param phi The azimuth angle, in radians.
	 * @param theta The elevation angle, in radians.
	 * @param r The radius, in meters.
	 * @return SphericalCoordinates
	 * @see https://en.wikipedia.org/wiki/Spherical_coordinate_system
	 * <!-- end-model-doc -->
	 * @model unique="false" phiUnique="false"
	 *        phiAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" thetaUnique="false"
	 *        thetaAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" rUnique="false"
	 *        rAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'"
	 * @generated
	 */
	SphericalCoordinates createSphericalCoordinates(double phi, double theta, double r);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Pose given its position and orientation coordinates.
	 * @param x The x coordinates value.
	 * @param y The y coordinates value.
	 * @param z The z coordinates value.
	 * @param xRotation Rotation about X, in radians.
	 * @param yRotation Rotation about Y, in radians.
	 * @param zRotation Rotation about Z, in radians.
	 * @return The Pose.
	 * <!-- end-model-doc -->
	 * @model unique="false" xUnique="false"
	 *        xAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" yUnique="false"
	 *        yAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" zUnique="false"
	 *        zAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'" xRotationUnique="false"
	 *        xRotationAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" yRotationUnique="false"
	 *        yRotationAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" zRotationUnique="false"
	 *        zRotationAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'"
	 * @generated
	 */
	Pose createPose(double x, double y, double z, double xRotation, double yRotation, double zRotation);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Pose from a original Pose.
	 * @param pose The original Pose. Not modified by this method.
	 * @return The Pose.
	 * <!-- end-model-doc -->
	 * @model unique="false" poseUnique="false"
	 * @generated
	 */
	Pose createPose(Pose pose);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a Pose given its position and orientation coordinates.
	 * @param position The position.
	 * @param orientation The orientation.
	 * @return The Pose.
	 * <!-- end-model-doc -->
	 * @model unique="false" positionUnique="false" orientationUnique="false"
	 * @generated
	 */
	Pose createPose(CartesianPositionCoordinates position, CartesianOrientationCoordinates orientation);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a DigitalElevationMap from a CartesianCoordinatesSet. The resulting DigitalElevationMap is defined in the XY plane.
	 * @param coordinatesSet The CartesianCoordinatesSet. Not modified by this method.
	 * @return The DigitalElevationMap.
	 * <!-- end-model-doc -->
	 * @model unique="false" coordinatesSetUnique="false"
	 * @generated
	 */
	DigitalElevationMap createDigitalElevationMap(CartesianCoordinatesSet coordinatesSet);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianPolygon given 3 vertices.
	 * @param v1 The first vertex.
	 * @param v2 The first vertex.
	 * @param v3 The first vertex.
	 * @return The CartesianPolygon.
	 * <!-- end-model-doc -->
	 * @model unique="false" v1Unique="false" v2Unique="false" v3Unique="false"
	 * @generated
	 */
	CartesianPolygon createCartesianPolygon(CartesianPositionCoordinates v1, CartesianPositionCoordinates v2, CartesianPositionCoordinates v3);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianPositionCoordinates using the coordinates from an original CartesianPositionCoordinates.
	 * @param coordinates The original CartesianPositionCoordinates. Not modified by this method.
	 * @return The CartesianPositionCoordinates.
	 * <!-- end-model-doc -->
	 * @model unique="false" coordinatesUnique="false"
	 * @generated
	 */
	CartesianPositionCoordinates createCartesianPositionCoordinates(CartesianPositionCoordinates coordinates);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianOrientationCoordinates using the coordinates from an original CartesianOrientationCoordinates.
	 * @param coordinates The original CartesianOrientationCoordinates. Not modified by this method.
	 * @return The CartesianOrientationCoordinates.
	 * <!-- end-model-doc -->
	 * @model unique="false" coordinatesUnique="false"
	 * @generated
	 */
	CartesianOrientationCoordinates createCartesianOrientationCoordinates(CartesianOrientationCoordinates coordinates);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianCoordinatesMesh by making copies of the points and polygons from an original mesh.
	 * @param cartesianCoordinatesMesh The original CartesianCoordinatesMesh. Not modified by this method.
	 * @return The CartesianCoordinatesMesh.
	 * <!-- end-model-doc -->
	 * @model unique="false" cartesianCoordinatesMeshUnique="false"
	 * @generated
	 */
	CartesianCoordinatesMesh createCartesianCoordinatesMesh(CartesianCoordinatesMesh cartesianCoordinatesMesh);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianTriangle given 3 vertices.
	 * @param v1 The first vertex.
	 * @param v2 The first vertex.
	 * @param v3 The first vertex.
	 * @return The CartesianTriangle.
	 * <!-- end-model-doc -->
	 * @model unique="false" v1Unique="false" v2Unique="false" v3Unique="false"
	 * @generated
	 */
	CartesianTriangle createCartesianTriangle(CartesianPositionCoordinates v1, CartesianPositionCoordinates v2, CartesianPositionCoordinates v3);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianTriangle given a CartesianPolygon.
	 * @param polygon The polygon. Not modified by this method.
	 * @return The CartesianTriangle.
	 * @throws An exception of the given polygon does not contain exactly 3 vertices.
	 * <!-- end-model-doc -->
	 * @model unique="false" exceptions="ca.gc.asc_csa.apogy.common.geometry.data3d.IllegalArgumentException" polygonUnique="false"
	 * @generated
	 */
	<T extends CartesianPolygon> CartesianTriangle createCartesianTriangle(T polygon) throws IllegalArgumentException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianCoordinatesMesh given a list of Polygons.
	 * @param polygons The list of Polygons. Not modified by this method.
	 * @return The CartesianCoordinatesMesh. The polygons and vertices in this mesh are copies of the ones found in the list of polygons.
	 * <!-- end-model-doc -->
	 * @model unique="false" polygonsDataType="ca.gc.asc_csa.apogy.common.geometry.data3d.List<ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPolygon>" polygonsUnique="false" polygonsMany="false"
	 * @generated
	 */
	CartesianCoordinatesMesh createCartesianCoordinatesMesh(List<CartesianPolygon> polygons);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianCoordinatesMesh given a CartesianTriangularMesh.
	 * @param cartesianCoordinatesMesh The CartesianTriangularMesh.	Not modified by this method.
	 * @return The CartesianCoordinatesMesh. The polygons and vertices in this mesh are copies of the ones found in the original.
	 * <!-- end-model-doc -->
	 * @model unique="false" cartesianCoordinatesMeshUnique="false"
	 * @generated
	 */
	CartesianCoordinatesMesh createCartesianCoordinatesMesh(CartesianTriangularMesh cartesianCoordinatesMesh);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianTriangularMesh given a list of Cartesian Triangle.
	 * @param polygons The list of Polygons. Not modified by this method.
	 * @return The CartesianTriangularMesh. The polygons and vertices in this mesh are copies of the ones found in the list of triangles.
	 * <!-- end-model-doc -->
	 * @model unique="false" polygonsDataType="ca.gc.asc_csa.apogy.common.geometry.data3d.List<ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangle>" polygonsUnique="false" polygonsMany="false"
	 * @generated
	 */
	CartesianTriangularMesh createCartesianTriangularMesh(List<CartesianTriangle> polygons);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianTriangularMesh given a CartesianTriangularMesh.
	 * @param cartesianCoordinatesMesh The CartesianTriangularMesh.	Not modified by this method.
	 * @return The CartesianTriangularMesh. The polygons and vertices in this mesh are copies of the ones found in the original.
	 * <!-- end-model-doc -->
	 * @model unique="false" cartesianCoordinatesMeshUnique="false"
	 * @generated
	 */
	CartesianTriangularMesh createCartesianTriangularMesh(CartesianTriangularMesh cartesianCoordinatesMesh);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianCoordinatesSet from an original CartesianCoordinatesSet and applies a specified transform to each of its point.
	 * @param points The original CartesianCoordinatesSet. Not modified by this method.
	 * @param trMatrix The transform matrix to apply.
	 * @return The CartesianCoordinatesSet.
	 * <!-- end-model-doc -->
	 * @model unique="false" pointsUnique="false" trMatrixDataType="ca.gc.asc_csa.apogy.common.geometry.data3d.Matrix4d" trMatrixUnique="false"
	 * @generated
	 */
	CartesianCoordinatesSet applyTransform(CartesianCoordinatesSet points, Matrix4d trMatrix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a list of CartesianPositionCoordinates from an original list of CartesianPositionCoordinates and applies a specified transform to each of its point.
	 * @param points The original list of CartesianPositionCoordinates. Not modified by this method.
	 * @param trMatrix The transform matrix to apply.
	 * @return The list of CartesianPositionCoordinates.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.geometry.data3d.List<ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPositionCoordinates>" unique="false" many="false" pointsDataType="ca.gc.asc_csa.apogy.common.geometry.data3d.List<ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPositionCoordinates>" pointsUnique="false" pointsMany="false" trMatrixDataType="ca.gc.asc_csa.apogy.common.geometry.data3d.Matrix4d" trMatrixUnique="false"
	 * @generated
	 */
	List<CartesianPositionCoordinates> applyTransform(List<CartesianPositionCoordinates> points, Matrix4d trMatrix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a CartesianTriangularMesh from an original CartesianTriangularMesh and applies a specified transform to each of its points.
	 * @param mesh The original CartesianTriangularMesh. Not modified by this method.
	 * @param trMatrix The transform matrix to apply.
	 * @return The CartesianTriangularMesh.
	 * <!-- end-model-doc -->
	 * @model unique="false" meshUnique="false" trMatrixDataType="ca.gc.asc_csa.apogy.common.geometry.data3d.Matrix4d" trMatrixUnique="false"
	 * @generated
	 */
	CartesianTriangularMesh createTransformedMesh(CartesianTriangularMesh mesh, Matrix4d trMatrix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Applies a specified transform to each of the points of a CartesianTriangularMesh.
	 * @param mesh The CartesianTriangularMesh. Point position will be modified.
	 * @param trMatrix The transform matrix to apply.
	 * <!-- end-model-doc -->
	 * @model meshUnique="false" trMatrixDataType="ca.gc.asc_csa.apogy.common.geometry.data3d.Matrix4d" trMatrixUnique="false"
	 * @generated
	 */
	void applyTransform(CartesianTriangularMesh mesh, Matrix4d trMatrix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Creates a NormalPointCloud from an original NormalPointCloud and applies a specified transform to each of its points.
	 * @param points The original NormalPointCloud. Not modified by this method.
	 * @param trMatrix The transform matrix to apply.
	 * @return The NormalPointCloud.
	 * <!-- end-model-doc -->
	 * @model unique="false" pointsUnique="false" trMatrixDataType="ca.gc.asc_csa.apogy.common.geometry.data3d.Matrix4d" trMatrixUnique="false"
	 * @generated
	 */
	NormalPointCloud applyTransform(NormalPointCloud points, Matrix4d trMatrix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Updates the coordinates of each point of a CartesianCoordinatesSet using a the values specified in an array. If the array
	 * contains more pointe than the original CartesianCoordinatesSet, the extra point will be added.
	 * @param cartesianCoordinatesSet The CartesianCoordinatesSet to update.
	 * @param xyzData The array of n points organize as [0..n][x,y,z].
	 * <!-- end-model-doc -->
	 * @model cartesianCoordinatesSetUnique="false" xyzDataDataType="ca.gc.asc_csa.apogy.common.geometry.data3d.DoubleArrayOfArray" xyzDataUnique="false"
	 * @generated
	 */
	void updateCartesianCoordinatesSet(CartesianCoordinatesSet cartesianCoordinatesSet, double[][] xyzData);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Concatenate multiple mesh into one. Note that no duplicate checking and no merging is applied.
	 * @param listOfTriangularMeshes The list of mesh to merge. The mesh will not be modified by this method.
	 * @return The CartesianTriangularMesh containing copies of the originals meshes point and polygon.
	 * <!-- end-model-doc -->
	 * @model unique="false" listOfTriangularMeshesDataType="ca.gc.asc_csa.apogy.common.geometry.data3d.List<ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangularMesh>" listOfTriangularMeshesUnique="false" listOfTriangularMeshesMany="false"
	 * @generated
	 */
	CartesianTriangularMesh concatenateTriangularMeshes(List<CartesianTriangularMesh> listOfTriangularMeshes);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Generate a point cloud from a CartesianTriangularMesh at a given resolution.
	 * @param cartesianCoordinatesMesh The Cartesian Triangular Mesh.
	 * @param resolution The target resolution of the point cloud.
	 * @return A CartesianCoordinatesSet containing the points.
	 * <!-- end-model-doc -->
	 * @model unique="false" cartesianCoordinatesMeshUnique="false" resolutionUnique="false"
	 * @generated
	 */
	CartesianCoordinatesSet generatePointCloud(CartesianTriangularMesh cartesianCoordinatesMesh, double resolution);

} // ApogyCommonGeometryData3DFacade

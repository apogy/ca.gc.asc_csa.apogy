/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.geometry.data3d.converters;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ca.gc.asc_csa.apogy.common.converters.FileExporterUtilities;
import ca.gc.asc_csa.apogy.common.converters.IFileExporter;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianCoordinatesSet;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianCoordinatesSetExtent;
import ca.gc.asc_csa.apogy.common.geometry.data3d.Data3DIO;

public class CartesianCoordinatesSetFileExporter implements IFileExporter
{
	public static final String XYZ_FILE_EXTENSION = "xyz";

	@Override
	public Class<?> getOutputType() 
	{		
		return File.class;
	}

	@Override
	public Class<?> getInputType() 
	{		
		return CartesianCoordinatesSet.class;
	}

	@Override
	public boolean canConvert(Object input) 
	{
		if(input instanceof CartesianCoordinatesSet)
		{
			CartesianCoordinatesSet points = (CartesianCoordinatesSet) input;
			return points.getPoints().size() > 0;
		}
		
		return false;
	}

	@Override
	public Object convert(Object input) throws Exception 
	{
		CartesianCoordinatesSet points = (CartesianCoordinatesSet) input;

		// Save as .xyz
		String tmpFolder = System.getProperty("user.home") + File.separator + System.getProperty("java.io.tmpdir");
		Date now = new Date();				
		
		String fileName = null;
		try
		{			
			fileName = tmpFolder + File.separator + now.getTime() + "." + XYZ_FILE_EXTENSION;											
			Data3DIO.INSTANCE.saveCoordinatesSetToXYZ(points, fileName);						
			return new File(fileName);	
		}
		catch(Exception e)
		{
			File toDelete = new File(fileName);
			toDelete.delete();			
			e.printStackTrace();
			return null;
		}		
	}

	@Override
	public void exportToFile(Object input, String filePath, List<String> extensions) throws Exception 
	{			
		CartesianCoordinatesSet points = (CartesianCoordinatesSet) input;
		for(String extension : extensions)
		{
			String fullPathString = filePath + "." + extension;			
			if(extension.equalsIgnoreCase(XYZ_FILE_EXTENSION))
			{								
				if(!fullPathString.endsWith("." + XYZ_FILE_EXTENSION)) fullPathString += "." + XYZ_FILE_EXTENSION;
				Data3DIO.INSTANCE.saveCoordinatesSetToXYZ(points, fullPathString);			
			}
			else if(extension.equalsIgnoreCase(METADATA_FILE_EXTENSION))
			{								
				FileExporterUtilities.saveMetaDataToFile(fullPathString, getMetaData(points));
			}
		}
	}

	@Override
	public List<String> getSupportedFileExtensions() 
	{
		List<String> extensions = new ArrayList<String>();
		extensions.add(XYZ_FILE_EXTENSION);
		extensions.add(METADATA_FILE_EXTENSION);
		return extensions;
	}

	@Override
	public String getDescription(String fileExtension) 
	{
		if(fileExtension.contains(XYZ_FILE_EXTENSION))
		{
			return "The mesh vertices in a xyz file format.";
		}
		else if(fileExtension.contains(METADATA_FILE_EXTENSION))
		{
			return "The mesh metadata.";
		}
		return null;	
	}

	/**
	 * Gets the metadata as a String for a given AbstractEImage.
	 * @param eImage The AbstractEImage.
	 * @return The metadata string.
	 */
	public static String getMetaData(CartesianCoordinatesSet points)
	{
		String metadata = "";
		
		if(points != null)
		{
			metadata += "Mesh number of points = " + points.getPoints().size() + "\n";
			
			CartesianCoordinatesSetExtent extent = points.getExtent();
			metadata += "Mesh minimum X = " + extent.getXMin() + "\n";
			metadata += "Mesh maximum X = " + extent.getXMax() + "\n";
			metadata += "Mesh minimum Y = " + extent.getYMin() + "\n";
			metadata += "Mesh maximum Y = " + extent.getYMax() + "\n";
			metadata += "Mesh minimum Z = " + extent.getZMin() + "\n";
			metadata += "Mesh maximum Z = " + extent.getZMax() + "\n";			
		}
		
		return metadata;
	}
	
}

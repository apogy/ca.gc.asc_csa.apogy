/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.geometry.data3d.converters;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ca.gc.asc_csa.apogy.common.converters.FileExporterUtilities;
import ca.gc.asc_csa.apogy.common.converters.IFileExporter;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangularMesh;
import ca.gc.asc_csa.apogy.common.geometry.data3d.Data3DIO;

public class CartesianTriangularMeshFileExporter implements IFileExporter
{
	public static final String TRIANGLE_FILE_EXTENSION = "tri";

	@Override
	public Class<?> getOutputType() 
	{		
		return File.class;
	}

	@Override
	public Class<?> getInputType() 
	{		
		return CartesianTriangularMesh.class;
	}

	@Override
	public boolean canConvert(Object input) 
	{
		if(input instanceof CartesianTriangularMesh)
		{
			CartesianTriangularMesh mesh = (CartesianTriangularMesh) input;
			return mesh.getPoints().size() > 0;
		}
		
		return false;
	}

	@Override
	public Object convert(Object input) throws Exception 
	{
		CartesianTriangularMesh mesh = (CartesianTriangularMesh) input;

		// Save as .tri
		String tmpFolder = System.getProperty("user.home") + File.separator + System.getProperty("java.io.tmpdir");
		Date now = new Date();				
		
		String fileName = null;
		try
		{			
			fileName = tmpFolder + File.separator + now.getTime() + "." + TRIANGLE_FILE_EXTENSION;											
			Data3DIO.INSTANCE.saveCoordinatesSetToXYZ(mesh, fileName);		
			
			return new File(fileName);	
		}
		catch(Exception e)
		{
			File toDelete = new File(fileName);
			toDelete.delete();			
			e.printStackTrace();
			return null;
		}		
	}

	@Override
	public void exportToFile(Object input, String filePath, List<String> extensions) throws Exception 
	{	
		boolean xyzExported = false;
		CartesianTriangularMesh mesh = (CartesianTriangularMesh) input;
		for(String extension : extensions)
		{
			String fullPathString = filePath;			
			
			if(extension.equalsIgnoreCase(TRIANGLE_FILE_EXTENSION))
			{	
				// Export the triangle file. This exports the xyz file at the same time.
				Data3DIO.INSTANCE.saveTriangularMeshAsASCII(mesh, fullPathString);
				xyzExported = true;
			}
			else if(extension.equalsIgnoreCase(CartesianCoordinatesSetFileExporter.XYZ_FILE_EXTENSION))
			{											
				if(!xyzExported)
				{
					if(!fullPathString.endsWith("." + CartesianCoordinatesSetFileExporter.XYZ_FILE_EXTENSION)) fullPathString += "." + CartesianCoordinatesSetFileExporter.XYZ_FILE_EXTENSION;
					Data3DIO.INSTANCE.saveCoordinatesSetToXYZ(mesh, fullPathString);
					xyzExported = true;
				}				
			}
			else if(extension.equalsIgnoreCase(METADATA_FILE_EXTENSION))
			{								
				FileExporterUtilities.saveMetaDataToFile(fullPathString, getMetaData(mesh));
			}
		}
	}

	@Override
	public List<String> getSupportedFileExtensions() 
	{
		List<String> extensions = new ArrayList<String>();
		extensions.add(CartesianCoordinatesSetFileExporter.XYZ_FILE_EXTENSION);
		extensions.add(TRIANGLE_FILE_EXTENSION);
		extensions.add(METADATA_FILE_EXTENSION);
		return extensions;
	}

	@Override
	public String getDescription(String fileExtension) 
	{
		if(fileExtension.contains(CartesianCoordinatesSetFileExporter.XYZ_FILE_EXTENSION))
		{
			return "The mesh vertices in a xyz file format.";
		}
		else if(fileExtension.contains(TRIANGLE_FILE_EXTENSION))
		{
			return "The mesh triangles in a .tri file format. Exporting this file also exports the .xyz file.";
		}
		else if(fileExtension.contains(METADATA_FILE_EXTENSION))
		{
			return "The mesh metadata.";
		}
		return null;	
	}

	/**
	 * Gets the metadata as a String for a given AbstractEImage.
	 * @param eImage The AbstractEImage.
	 * @return The metadata string.
	 */
	public static String getMetaData(CartesianTriangularMesh mesh)
	{
		String metadata = "";
		
		if(mesh != null)
		{
			metadata += "Mesh number of triangles = " + mesh.getPolygons().size() + "\n";
			metadata += "Mesh surface = " + mesh.getSurface() + "\n";
			metadata += CartesianCoordinatesSetFileExporter.getMetaData(mesh);		
		}
		
		return metadata;
	}
	
}

package ca.gc.asc_csa.apogy.common.math.graphs.tests;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;

import ca.gc.asc_csa.apogy.common.math.graphs.KDTree;
import edu.wlu.cs.levy.CG.KeyDuplicateException;
import edu.wlu.cs.levy.CG.KeySizeException;


public class OriginalKDTree implements KDTree
{
	protected  edu.wlu.cs.levy.CG.KDTree orginalKDTRee = null;
	private int dimension = 0;
	
	@Override
	public EClass eClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Resource eResource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EObject eContainer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EStructuralFeature eContainingFeature() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EReference eContainmentFeature() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EList<EObject> eContents() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TreeIterator<EObject> eAllContents() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eIsProxy() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public EList<EObject> eCrossReferences() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object eGet(EStructuralFeature feature) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object eGet(EStructuralFeature feature, boolean resolve) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eSet(EStructuralFeature feature, Object newValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean eIsSet(EStructuralFeature feature) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void eUnset(EStructuralFeature feature) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object eInvoke(EOperation operation, EList<?> arguments) throws InvocationTargetException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EList<Adapter> eAdapters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eDeliver() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void eSetDeliver(boolean deliver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eNotify(Notification notification) {
		// TODO Auto-generated method stub
		
	}

	public OriginalKDTree()
	{
		
	}
	
	@Override
	public int getDimension() 
	{
		return dimension;
	}

	@Override
	public void setDimension(int value) {
		orginalKDTRee = new edu.wlu.cs.levy.CG.KDTree(value);
		dimension = value;
	}

	@Override
	public boolean addNode(double[] key, Object value) throws Exception {
		try
		{
			orginalKDTRee.insert(key, value);
		}
		catch (KeySizeException e) 
		{
			throw e;
		}
		catch (KeyDuplicateException e) {
			return false;
		}
		
		return true;
	}

	@Override
	public Object search(double[] key) throws Exception 
	{
		return orginalKDTRee.search(key);
	}

	@Override
	public boolean removeNode(double[] key) throws Exception 
	{
		try
		{
			orginalKDTRee.delete(key);
			return true;
		}
		catch (KeySizeException e) 
		{
			throw e;
		}
		catch (Exception ex) {
			return false;
		}
	}

	@Override
	public Object findNearest(double[] key) throws Exception 
	{
		return orginalKDTRee.nearest(key);
	}

	@Override
	public List<Object> findNearest(double[] key, int numberOfNodes) throws Exception 
	{
		Object[] objects = orginalKDTRee.nearest(key, numberOfNodes);
		
		List<Object> list = new ArrayList<Object>();
		for(int i = 0; i < objects.length; i++)
		{
			list.add(objects[i]);
		}
		
		return list;
	}

	@Override
	public List<Object> performRangeSearch(double[] lowerKey, double[] upperKey) throws Exception 
	{
		Object[] objects = orginalKDTRee.range(lowerKey, upperKey);
		
		List<Object> list = new ArrayList<Object>();
		for(int i = 0; i < objects.length; i++)
		{
			list.add(objects[i]);
		}
		
		return list;
	}

	@Override
	public void initializeTree(List<double[]> keys, List<Object> values) throws Exception 
	{				
		orginalKDTRee = new edu.wlu.cs.levy.CG.KDTree(dimension);
				
		for(int i = 0; i < keys.size(); i++)
		{
			double[] key = keys.get(i);
			Object value = values.get(i);			
			orginalKDTRee.insert(key, value);		
		}
	}

}
